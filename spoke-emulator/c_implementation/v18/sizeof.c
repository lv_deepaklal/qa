#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>

int main(int argc, char** argv) {

    printf("CHAR_BIT    :   %d\n", CHAR_BIT);
    printf("CHAR_MAX    :   %d\n", CHAR_MAX);
    printf("CHAR_MIN    :   %d\n", CHAR_MIN);
    printf("INT_MAX     :   %d\n", INT_MAX);
    printf("INT_MIN     :   %d\n", INT_MIN);
    printf("LONG_MAX    :   %ld\n", (long) LONG_MAX);
    printf("LONG_MIN    :   %ld\n", (long) LONG_MIN);
    printf("SCHAR_MAX   :   %d\n", SCHAR_MAX);
    printf("SCHAR_MIN   :   %d\n", SCHAR_MIN);
    printf("SHRT_MAX    :   %d\n", SHRT_MAX);
    printf("SHRT_MIN    :   %d\n", SHRT_MIN);
    printf("UCHAR_MAX   :   %d\n", UCHAR_MAX);
    printf("UINT_MAX    :   %u\n", (unsigned int) UINT_MAX);
    printf("ULONG_MAX   :   %lu\n", (unsigned long) ULONG_MAX);
    printf("USHRT_MAX   :   %d\n", (unsigned short) USHRT_MAX);

    printf("CHAR_BIT    :   %ld\n", sizeof(CHAR_BIT));
    printf("CHAR_MAX    :   %ld\n", sizeof(CHAR_MAX));
    printf("CHAR_MIN    :   %ld\n", sizeof(CHAR_MIN));
    printf("INT_MAX     :   %ld\n", sizeof(INT_MAX));
    printf("INT_MIN     :   %ld\n", sizeof(INT_MIN));
    printf("LONG_MAX    :   %ld\n", sizeof((long) LONG_MAX));
    printf("LONG_MIN    :   %ld\n", sizeof((long) LONG_MIN));
    printf("SCHAR_MAX   :   %ld\n", sizeof(SCHAR_MAX));
    printf("SCHAR_MIN   :   %ld\n", sizeof(SCHAR_MIN));
    printf("SHRT_MAX    :   %ld\n", sizeof(SHRT_MAX));
    printf("SHRT_MIN    :   %ld\n", sizeof(SHRT_MIN));
    printf("UCHAR_MAX   :   %ld\n", sizeof(UCHAR_MAX));
    printf("UINT_MAX    :   %ld\n", sizeof((unsigned int) UINT_MAX));
    printf("ULONG_MAX   :   %ld\n", sizeof((unsigned long) ULONG_MAX));
    printf("USHRT_MAX   :   %ld\n", sizeof((unsigned short) USHRT_MAX));
    return 0;
}

