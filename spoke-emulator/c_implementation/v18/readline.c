#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>

int main ( void )
{
   static const char filename[] = "clid.txt";
   FILE *file = fopen ( filename, "r" );
   if ( file != NULL )
   {
      char line [ 128 ]; /* or other suitable maximum line size */
      while ( fgets ( line, sizeof line, file ) != NULL ) /* read a line */
      {
         //fputs ( line, stdout ); /* write the line */
	 //printf("%s", line);
	 //printf("%d\n", (unsigned int)atoi(line));
	 printf("%x\n", (unsigned int)atoi(line));
	 //printf("%x\n", (unsigned int)atoi(line)+1);
	 unsigned int ab = (unsigned int)atoi(line);
	 printf("sizeof ab %ld\n",sizeof(ab));

	 unsigned char x1 = (ab >> 24) & 255;
	 unsigned char x2 = (ab >> 16) & 255;
	 unsigned char x3 = (ab >> 8) & 255;
	 unsigned char x4 = (ab >> 0) & 255;
	 printf("%02x %02x %02x %02x\n", x1,x2,x3,x4);

      }
      fclose ( file );
   }
   else
   {
      perror ( filename ); /* why didn't the file open? */
   }
   return 0;
}

