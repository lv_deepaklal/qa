#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <linux/if_ether.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <net/if.h>

// for sleep
#include <unistd.h>
//For ethernet struct
#include <netinet/ether.h>

#include <pthread.h>
#include<signal.h>
#include<errno.h>

#include "udp.h"


#define LN_DATA_LEN 186
#define MAX_DATA_SIZE 100
#define FOO ""
#define DEFAULT_IF      "eth1"


int raw_sock;
extern int errno;
int STOP=0;
int NO_OF_NODES = 350;
struct sockaddr_in dst_addr;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void hexdump(unsigned char *data, unsigned int data_bytes)
{
    int bin_p, ascii_p;

    bin_p = ascii_p = 0;

    while(bin_p < data_bytes){
        int j;
        int whitespaces;
        for(j = 0; j < 8 && bin_p < data_bytes; j++){
            printf("%02x ", data[bin_p++]);
        }

        whitespaces = (8 - j) * 3;
        for(j = 0; j < whitespaces; j++){
            printf(" ");
        }

        for(j = 0; j < 8 && ascii_p < data_bytes; j++){
            if(isprint(data[ascii_p])){
                printf("%c", data[ascii_p++]);
            }else{
                printf(".");
                ascii_p++;
            }
        }

        printf("\n");
    }
}


int HBEAT_SRCCLID_POSN = 44; 
int HBEATACK_SRCCLID_POSN = 44; 
int RTTPROBE_SRCCLID_POSN = 44; 
int HSHAKE_SRCCLID_POSN =44;

void get_source_clids(FILE *file, uint8_t *heart_beat_data, uint8_t *rtt_probe_data, uint8_t *handshake_data, uint8_t *heart_beat_ack_data) {

    //int src_clid = 4;
    if ( file != NULL ) {
      char line [ 128 ]; /* or other suitable maximum line size */

      int node_idx = 0;
      while ( fgets ( line, sizeof line, file ) != NULL && node_idx < NO_OF_NODES ) {
         unsigned int ab = (unsigned int)atoi(line);

	 *((heart_beat_data+node_idx*MAX_DATA_SIZE)+ HBEAT_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((heart_beat_data+node_idx*MAX_DATA_SIZE)+ HBEAT_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((heart_beat_data+node_idx*MAX_DATA_SIZE)+ HBEAT_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((heart_beat_data+node_idx*MAX_DATA_SIZE)+ HBEAT_SRCCLID_POSN+3) = (ab >> 24) & 255;

         ab = (unsigned int)atoi(line);
	 *((rtt_probe_data+node_idx*MAX_DATA_SIZE)+ RTTPROBE_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((rtt_probe_data+node_idx*MAX_DATA_SIZE)+ RTTPROBE_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((rtt_probe_data+node_idx*MAX_DATA_SIZE)+ RTTPROBE_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((rtt_probe_data+node_idx*MAX_DATA_SIZE)+ RTTPROBE_SRCCLID_POSN+3) = (ab >> 24) & 255;

         ab = (unsigned int)atoi(line);
	 *((handshake_data+node_idx*MAX_DATA_SIZE)+ HSHAKE_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((handshake_data+node_idx*MAX_DATA_SIZE)+ HSHAKE_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((handshake_data+node_idx*MAX_DATA_SIZE)+ HSHAKE_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((handshake_data+node_idx*MAX_DATA_SIZE)+ HSHAKE_SRCCLID_POSN+3) = (ab >> 24) & 255;

         ab = (unsigned int)atoi(line);
	 *((heart_beat_ack_data+node_idx*MAX_DATA_SIZE)+ HBEATACK_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((heart_beat_ack_data+node_idx*MAX_DATA_SIZE)+ HBEATACK_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((heart_beat_ack_data+node_idx*MAX_DATA_SIZE)+ HBEATACK_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((heart_beat_ack_data+node_idx*MAX_DATA_SIZE)+ HBEATACK_SRCCLID_POSN+3) = (ab >> 24) & 255;
	 node_idx++;
      }
      //fclose ( file );
    } else {
      printf("Failed to open the file\n");
    }
}

char ** get_ips_from_file(FILE *fp, char **localhost ) {
   //fp = fopen(file_name, "r"); // read mode
 
   if (fp == NULL)
   {
      perror("Error while opening the file.\n");
      exit(EXIT_FAILURE);
   }
 
    localhost = (char  **) malloc (NO_OF_NODES * sizeof (char *));
    for (int i=0; i< NO_OF_NODES; i++) {
        localhost[i] = (char *) malloc ( 16 * sizeof(char));
    }
 
   int i=0;
   char ch;
   int start = 0;
   int end = 0;
   int ip_idx = 0;
   for (int i =0; i<NO_OF_NODES; i++) {
	   for (int j=0; j<16; j++)
		   localhost[i][j]='\0';
   }

   char ip[16];
   int per_ip_idx = 0;
   while((ch = fgetc(fp)) != EOF && ip_idx < NO_OF_NODES && per_ip_idx < 16) {
      //printf("%c", ch);
      if (ch=='\n') {
          ip[per_ip_idx++] = '\0';
          per_ip_idx=0;
          strcpy(localhost[ip_idx++], ip);
      } else {
          ip[per_ip_idx++] = ch;
      }
   }
   return localhost;
}


struct spoke_set {
    //int no_of_nodes;
    struct sockaddr_ll *socket_address;
    int no_of_spokes_per_bunch;
    unsigned int hs_packet_size;
    unsigned int pb_packet_size;
    unsigned int hb_packet_size;
    unsigned int hback_packet_size;
    uint8_t *hb_frame;
    uint8_t *hs_frame;  
    uint8_t *pb_frame;  
    uint8_t *hback_frame;  
};

void * start_spoke_bunch(void *args) {

	struct spoke_set *sp_set = (struct spoke_set *) args;
        int c=0;
        int s1=0, s2=0, s3=0, s4=0;

	while (!STOP) {
	    clock_t t1 = clock();
            //time_t start = time(NULL);
	    printf("t1 %ld sp_set->no_of_spokes_per_bunch %d\n",t1, sp_set->no_of_spokes_per_bunch);
            for (int i=0; i<sp_set->no_of_spokes_per_bunch; i++) {
		    printf("i=%d c=%d\n",i, c);
                s1 = sendto(raw_sock, sp_set->hb_frame + (i*sp_set->hb_packet_size) , sp_set->hb_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));
                s2 = sendto(raw_sock, sp_set->pb_frame + (i*sp_set->pb_packet_size), sp_set->pb_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));
                s4 = sendto(raw_sock, sp_set->hback_frame + (i*sp_set->hback_packet_size), sp_set->hback_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));
 	        if (c%5==0) {
                  s3 = sendto(raw_sock, sp_set->hs_frame + (i*sp_set->hs_packet_size), sp_set->hs_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));
 	          c=0;
 	        }
            }
 	    c++;
	    clock_t t2 = clock();
	    double diff_us = (double)(t2 - t1)*1000000/CLOCKS_PER_SEC;
            //long int wait = 1000000 - (int)diff/1000000.0;
	    long int wait_us = (int)(1000000.0 - diff_us);
	    printf("t1 = %ld, t2=%ld absdiff %ld, diff=%f CLOCKS_PER_SEC %ld, wait %ld\n", t1, t2, t2-t1, diff_us, CLOCKS_PER_SEC, wait_us);
	    if (wait_us > 0) {
                //usleep(1000000 - (int)diff/1000000.0);
                usleep(wait_us);
	    }
            //unsigned int diff = (unsigned int)round(difftime(time(NULL), begin));

	    
	    //nanosleep();
	}
}

int main(void)
{
    uint8_t frame[NO_OF_NODES][LN_DATA_LEN+14];
    uint8_t packet[NO_OF_NODES][LN_DATA_LEN];
    uint8_t udp_packet[NO_OF_NODES][LN_DATA_LEN];

    uint8_t hs_frame[NO_OF_NODES][LN_DATA_LEN+14];
    uint8_t hs_packet[NO_OF_NODES][LN_DATA_LEN];
    uint8_t hs_udp_packet[NO_OF_NODES][LN_DATA_LEN];
    uint8_t pb_frame[NO_OF_NODES][LN_DATA_LEN+14];
    uint8_t pb_packet[NO_OF_NODES][LN_DATA_LEN];
    uint8_t pb_udp_packet[NO_OF_NODES][LN_DATA_LEN];
    uint8_t hb_frame[NO_OF_NODES][LN_DATA_LEN+14];
    uint8_t hb_packet[NO_OF_NODES][LN_DATA_LEN];
    uint8_t hb_udp_packet[NO_OF_NODES][LN_DATA_LEN];
    uint8_t hback_frame[NO_OF_NODES][LN_DATA_LEN+14];
    uint8_t hback_packet[NO_OF_NODES][LN_DATA_LEN];
    uint8_t hback_udp_packet[NO_OF_NODES][LN_DATA_LEN];

    uint8_t data[NO_OF_NODES][MAX_DATA_SIZE];
    uint8_t probe[NO_OF_NODES][MAX_DATA_SIZE];
    uint8_t hshake[NO_OF_NODES][MAX_DATA_SIZE];
    uint8_t hback[NO_OF_NODES][MAX_DATA_SIZE];

    char **localhost=NULL;
    char *dsthost = "192.168.0.1";
    unsigned int packet_size;
    unsigned int hs_packet_size;
    unsigned int hb_packet_size;
    unsigned int pb_packet_size;
    unsigned int hback_packet_size;
    unsigned int data_size;
    struct sockaddr_in src_addr[NO_OF_NODES];
    struct sockaddr_ll socket_address;

    char *file_name = "ip.txt";
    char *file_name2 = "clid.txt";

    FILE *fp, *fp2;
    fp = fopen(file_name, "r");
    fp2 = fopen(file_name2, "r");
    localhost = get_ips_from_file(fp, localhost);
    fclose(fp);

    int print_ips = 0;
    if (print_ips) {
        for (int i =0; i<NO_OF_NODES; i++) {
          printf("s=%s\n", localhost[i]);
        }
    }

    dst_addr.sin_family = AF_INET;
    dst_addr.sin_port =  htons(7007);
    inet_aton(dsthost, &dst_addr.sin_addr);

    //unsigned char heartbeat[] = { 0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2c, 0x3b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x3a, 0x30, 0x00, 0x00, 0x03, 0x68, 0x01, 0x80, 0x02, 0x48, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4c, 0x76, 0x45, 0x72, 0x43, 0x6f, 0x4d, 0x70, 0x10 };
    unsigned char heartbeat[] = { 0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x13, 0x2e, 0x00, 0x00, 0x01, 0xe8, 0x02, 0x80, 0x01, 0x68, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    unsigned char rttProbe[] = { 0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0xc5, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x48, 0x01, 0x80, 0x03, 0x68, 0x01, 0x80, 0xc5, 0x70, 0xc7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x42, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x0f, 0x42, 0x40, 0x00, 0x00, 0x00, 0x01  };

    unsigned char handshake[] = {0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x75, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x07, 0xba, 0x01, 0x01, 0xfe, 0xb9, 0x01, 0x01, 0xfe, 0x02, 0x48, 0x01, 0x80, 0x03, 0x68, 0x01, 0x80, 0x1b, 0x5f, 0x1b, 0x5f, 0x00, 0x00, 0x00, 0x00};

    //unsigned char heartbeatack[] = {0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x75, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x07, 0xba, 0x01, 0x01, 0xfe, 0xb9, 0x01, 0x01, 0xfe, 0x02, 0x48, 0x01, 0x80, 0x03, 0x68, 0x01, 0x80, 0x1b, 0x5f, 0x1b, 0x5f, 0x00, 0x00, 0x00, 0x00};

    unsigned char heartbeatack[] = {0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xcf, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x13, 0x2e, 0x00, 0x00, 0x01, 0x68, 0x01, 0x80, 0x01, 0xe8, 0x02, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4c, 0x76, 0x45, 0x72, 0x43, 0x6f, 0x4d, 0x70, 0x10};
    unsigned int heartbeat_data_size = sizeof(heartbeat);
    unsigned int handshake_data_size = sizeof(handshake);
    unsigned int rtt_probe_data_size = sizeof(rttProbe);
    unsigned int heartbeatack_data_size = sizeof(heartbeatack);



    /* */
        struct ifreq if_idx;
        struct ifreq if_mac;
        char ifName[IFNAMSIZ];
        //sock_fd = *raw_sock;
        strcpy(ifName, DEFAULT_IF);
        /* Open RAW socket to send on */
        if ((raw_sock = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
            perror("socket");
        }

        /* Get the index of the interface to send on */
        memset(&if_idx, 0, sizeof(struct ifreq));
        strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
        if (ioctl(raw_sock, SIOCGIFINDEX, &if_idx) < 0)
            perror("SIOCGIFINDEX");

        /* Get the MAC address of the interface to send on */
        memset(&if_mac, 0, sizeof(struct ifreq));
        strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
        if (ioctl(raw_sock, SIOCGIFHWADDR, &if_mac) < 0)
            perror("SIOCGIFHWADDR");


    for (int i=0; i<NO_OF_NODES; i++) {

        src_addr[i].sin_family = AF_INET;
        src_addr[i].sin_port = htons(31013);
        inet_aton(localhost[i], &src_addr[i].sin_addr);

        memset(data[i], 0, MAX_DATA_SIZE);
        memcpy(data[i], heartbeat, sizeof(heartbeat));

        memset(probe[i], 0, MAX_DATA_SIZE);
        memcpy(probe[i], rttProbe, sizeof(rttProbe));

        memset(hshake[i], 0, MAX_DATA_SIZE);
        memcpy(hshake[i], handshake, sizeof(handshake));

        memset(hback[i], 0, MAX_DATA_SIZE);
        memcpy(hback[i], heartbeatack, sizeof(heartbeatack));
    }

    get_source_clids(fp2, (uint8_t *)data, (uint8_t *)probe, (uint8_t *)hshake, (uint8_t *)hback);

    for (int i=0; i<NO_OF_NODES; i++) {

	// Heartbeat
        packet_size = build_udp_packet(src_addr[i], dst_addr, hb_udp_packet[i], data[i], heartbeat_data_size);
        packet_size = build_ip_packet(src_addr[i].sin_addr, dst_addr.sin_addr, IPPROTO_UDP, hb_packet[i], hb_udp_packet[i], packet_size);
        hb_packet_size = build_ethernet_frame(&raw_sock, hb_frame[i], hb_packet[i], packet_size, &socket_address, if_idx, if_mac);
	
	// probe
        packet_size = build_udp_packet(src_addr[i], dst_addr, pb_udp_packet[i], probe[i], rtt_probe_data_size);
        packet_size = build_ip_packet(src_addr[i].sin_addr, dst_addr.sin_addr, IPPROTO_UDP, pb_packet[i], pb_udp_packet[i], packet_size);
        pb_packet_size = build_ethernet_frame(&raw_sock, pb_frame[i], pb_packet[i], packet_size, &socket_address, if_idx, if_mac);
	
	// Handsahke
        packet_size = build_udp_packet(src_addr[i], dst_addr, hs_udp_packet[i], hshake[i], handshake_data_size);
        packet_size = build_ip_packet(src_addr[i].sin_addr, dst_addr.sin_addr, IPPROTO_UDP, hs_packet[i], hs_udp_packet[i], packet_size);
        hs_packet_size = build_ethernet_frame(&raw_sock, hs_frame[i], hs_packet[i], packet_size, &socket_address, if_idx, if_mac);

	// HeartbeatAck
        packet_size = build_udp_packet(src_addr[i], dst_addr, hback_udp_packet[i], hback[i], heartbeatack_data_size);
        packet_size = build_ip_packet(src_addr[i].sin_addr, dst_addr.sin_addr, IPPROTO_UDP, hback_packet[i], hback_udp_packet[i], packet_size);
        hback_packet_size = build_ethernet_frame(&raw_sock, hback_frame[i], hback_packet[i], packet_size, &socket_address, if_idx, if_mac);
    }

    //for (int i=0; i<NO_OF_NODES; i++) {
    //    hexdump(frame[i], packet_size);
    //}

    fclose(fp2);
    int NO_OF_BUNCHES=1;
    int NO_OF_SPOKES_PER_BUNCH=350;

    pthread_t spokes_bunch[NO_OF_BUNCHES];
    struct spoke_set spoke_sets[NO_OF_BUNCHES];
    int thr_status;

    for (int i=0; i<NO_OF_BUNCHES; i++) {
	spoke_sets[i].no_of_spokes_per_bunch = NO_OF_SPOKES_PER_BUNCH;
	spoke_sets[i].socket_address = &socket_address; 

	//spoke_sets[i].hb_packet_size = hb_packet_size; 
	//spoke_sets[i].hs_packet_size = hs_packet_size;
	//spoke_sets[i].pb_packet_size = pb_packet_size;
	//spoke_sets[i].hback_packet_size = hback_packet_size;
	spoke_sets[i].hb_packet_size = 200;
	spoke_sets[i].hs_packet_size = 200;
	spoke_sets[i].pb_packet_size = 200;
	spoke_sets[i].hback_packet_size = 200;

	printf("hb_packet_size %d hs_packet_size %d pb_packet_size %d hback_packet_size %d\n", hb_packet_size, hs_packet_size, pb_packet_size, hback_packet_size);
	spoke_sets[i].hb_frame = &hb_frame[i][0];
	spoke_sets[i].hback_frame = &hback_frame[i][0];
	spoke_sets[i].hs_frame = &hs_frame[i][0];
	spoke_sets[i].pb_frame = &pb_frame[i][0];

        thr_status = pthread_create(&spokes_bunch[i], NULL, start_spoke_bunch, &spoke_sets[i]);
    }


    while (1) {
    }
    return 0;
}

