#ifndef UDP_H
#define UDP_H

#include <stdint.h>
#include <arpa/inet.h>

#define UDP_HDR_SIZE 8

#define INET_HDR_LEN 5

unsigned int build_ethernet_frame(int *sockfd, uint8_t *frame,  unsigned int data_size, struct sockaddr_ll *socket_address, struct ifreq if_idx, struct ifreq if_mac, uint8_t dstmac_hex[6]);

unsigned int build_udp_packet(struct sockaddr_in src_addr, struct sockaddr_in dst_addr, uint8_t *udp_packet, unsigned int data_size);

unsigned int build_ip_packet(struct in_addr src_addr, struct in_addr dst_addr, uint8_t protocol, uint8_t *ip_packet, unsigned int data_size);

#endif

