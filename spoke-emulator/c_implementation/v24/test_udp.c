#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <linux/if_ether.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <net/if.h>

// for sleep
#include <unistd.h>
//For ethernet struct
#include <netinet/ether.h>

#include <pthread.h>
#include<signal.h>
#include<errno.h>

#include "udp.h"

#include "ini.h"


#define LN_DATA_LEN 186
#define MAX_DATA_SIZE 100
#define FOO ""
#define DEFAULT_IF      "br0"


#define LNP_HB_DATA_SIZE 85
#define LNP_HS_DATA_SIZE 60
#define LNP_RTT_DATA_SIZE 88
#define LNP_HBACK_DATA_SIZE 85 
#define LN_IP_HDR_SIZE 20
#define LN_UDP_HDR_SIZE 8
#define LN_ETH_HDR_SIZE 14

#define LNP_HB_PKT_SIZE     LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HB_DATA_SIZE 
#define LNP_HS_PKT_SIZE     LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HS_DATA_SIZE 
#define LNP_RTT_PKT_SIZE    LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_RTT_DATA_SIZE
#define LNP_HBACK_PKT_SIZE  LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HBACK_DATA_SIZE

#define NO_OF_PATHS 1

int HB_PKT_SIZE    = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HB_DATA_SIZE; 
int HS_PKT_SIZE    = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HS_DATA_SIZE ;
int RTT_PKT_SIZE   = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_RTT_DATA_SIZE;
int HBACK_PKT_SIZE = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HBACK_DATA_SIZE;

int raw_sock;
extern int errno;
int STOP=0;
int NO_OF_NODES = NO_OF_PATHS;
struct sockaddr_in dst_addr;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

typedef struct {
   const char *iface;
   const char *dest_ip;
   const char *dest_mac;
   uint32_t dest_clid;
} user_input;

static int user_input_handler(void* user, const char* section, const char* name, const char* value) {


    user_input *input = (user_input *)user;
    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    if (MATCH("general", "iface")) {
        input->iface = strdup(value);
    } else if (MATCH("general", "dest_ip")) {
        input->dest_ip = strdup(value);
    } else if (MATCH("general", "dest_mac")) {
        input->dest_mac = strdup(value);
    } else if (MATCH("general", "dest_clid")) {
        input->dest_clid = (uint32_t)htonl(atol(value));
    } else {
        return 0;
    }
    return 1;
}


/* Prints the hexdump */
void hexdump(unsigned char *data, unsigned int data_bytes)
{
    int bin_p, ascii_p;

    bin_p = ascii_p = 0;

    while(bin_p < data_bytes){
        int j;
        int whitespaces;
        for(j = 0; j < 8 && bin_p < data_bytes; j++){
            printf("%02x ", data[bin_p++]);
        }

        whitespaces = (8 - j) * 3;
        for(j = 0; j < whitespaces; j++){
            printf(" ");
        }

        for(j = 0; j < 8 && ascii_p < data_bytes; j++){
            if(isprint(data[ascii_p])){
                printf("%c", data[ascii_p++]);
            }else{
                printf(".");
                ascii_p++;
            }
        }

        printf("\n");
    }
}


int HBEAT_SRCCLID_POSN = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+44; 
int HBEATACK_SRCCLID_POSN = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+44; 
int RTTPROBE_SRCCLID_POSN = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+44; 
int HSHAKE_SRCCLID_POSN =LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+44;

/* Modify the data with Source CLID */
void get_source_clids(FILE *file, uint8_t *heart_beat_data, uint8_t *rtt_probe_data, uint8_t *handshake_data, uint8_t *heart_beat_ack_data) {

    //int src_clid = 4;
    if ( file != NULL ) {
      char line [ 128 ]; /* or other suitable maximum line size */

      int node_idx = 0;
      while ( fgets ( line, sizeof line, file ) != NULL && node_idx < NO_OF_NODES ) {
         unsigned int ab = (unsigned int)atoi(line);


	 *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_SRCCLID_POSN+3) = (ab >> 24) & 255;

         ab = (unsigned int)atoi(line);
	 *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_SRCCLID_POSN+3) = (ab >> 24) & 255;

         ab = (unsigned int)atoi(line);
	 *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_SRCCLID_POSN+3) = (ab >> 24) & 255;

         ab = (unsigned int)atoi(line);
	 *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_SRCCLID_POSN+3) = (ab >> 24) & 255;
	 node_idx++;
      }
      //fclose ( file );
    } else {
      printf("Failed to open the file\n");
    }
}

/* Read the file and get Source IP */
char * get_ips_from_file(FILE *fp, char *localhost ) {
   //fp = fopen(file_name, "r"); // read mode
 
   if (fp == NULL)
   {
      perror("Error while opening the file.\n");
      exit(EXIT_FAILURE);
   }
 
    //printf("loalhsot%p\n", localhost);
    //localhost = (char  **) malloc (NO_OF_NODES * sizeof (char *));
    //for (int i=0; i< NO_OF_NODES; i++) {
    //    localhost[i] = (char *) malloc ( 16 * sizeof(char));
    //}
 
   int i=0;
   char ch;
   int start = 0;
   int end = 0;
   int ip_idx = 0;
   //for (int i =0; i<NO_OF_NODES; i++) {
   //}
//	   for (int j=0; j<16; j++)
//		   localhost[i]='\0';

   char ip[16];
   int per_ip_idx = 0;
   while((ch = fgetc(fp)) != EOF && per_ip_idx < 16) {
      //printf("%c", ch);
      if (ch=='\n') {
          localhost[per_ip_idx++] = '\0';
          //ip[per_ip_idx++] = '\0';
          //per_ip_idx=0;
	  break;
          //strcpy(localhost[ip_idx++], ip);
      } else {
          //ip[per_ip_idx++] = ch;
          localhost[per_ip_idx++] = ch;
      }
   }
   return localhost;
}


struct spoke_set {
    //int no_of_nodes;
    struct sockaddr_ll *socket_address;
    int no_of_spokes_per_bunch;
    unsigned int hs_packet_size;
    unsigned int pb_packet_size;
    unsigned int hb_packet_size;
    unsigned int hback_packet_size;
    uint8_t *hb_frame;
    uint8_t *hs_frame;  
    uint8_t *pb_frame;  
    uint8_t *hback_frame;  
};

long int BUFFER_TIME_US = 5000; //1ms buffer
void * start_spoke_bunch(void *args) {

	struct spoke_set *sp_set = (struct spoke_set *) args;
        int c=0;
        int s1=0, s2=0, s3=0, s4=0;

	long start, end;
	struct timeval t1, t2;
	gettimeofday(&t1, NULL);
	while (!STOP) {
	    //clock_t t1 = clock();
	    //long double t1 = (long double)clock();
            //time_t start = time(NULL);
	    //printf("t1 %ld sp_set->no_of_spokes_per_bunch %d\n",t1, sp_set->no_of_spokes_per_bunch);
	    start = (long)t1.tv_sec * 1000000 + (long)t1.tv_usec ;
            for (int i=0; i<sp_set->no_of_spokes_per_bunch; i++) {
		   // printf("i=%d c=%d\n",i, c);
                s1 = sendto(raw_sock, sp_set->hb_frame + (i*sp_set->hb_packet_size) , sp_set->hb_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));
                s2 = sendto(raw_sock, sp_set->pb_frame + (i*sp_set->pb_packet_size), sp_set->pb_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));
                s4 = sendto(raw_sock, sp_set->hback_frame + (i*sp_set->hback_packet_size), sp_set->hback_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));

 	        if (c%5==0) {
		  /* Send this only once in 5sec*/
                  s3 = sendto(raw_sock, sp_set->hs_frame + (i*sp_set->hs_packet_size), sp_set->hs_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));
 	          c=0;
 	        }
            }
 	    c++;
	    if (c==5) {
		    //STOP=1;
	    }

	    //clock_t t2 = clock();
	    //long double t2 = (long double)clock();
	    //long double diff_us = (long double)(t2 - t1)*1000000/CLOCKS_PER_SEC;
	    //long int wait_us = (long int)(1000000.0 - diff_us);
	    //printf("t1 = %Lf, t2=%Lf absdiff %Lf, diff=%Lf CLOCKS_PER_SEC %ld, wait %ld\n", t1, t2, t2-t1, diff_us, CLOCKS_PER_SEC, wait_us);
	    //printf("end - start = %.2f\n", (double)time(NULL)-start);
	    gettimeofday(&t2, NULL);
	    end = (long)t2.tv_sec * 1000000 + (long)t2.tv_usec ;
	    printf("%ld useconds elapsed\n", (end - start));
	    long int wait_us = (long int)(1000000 - (end - start) - BUFFER_TIME_US);  
	    if (wait_us > 0) {
                usleep(wait_us);
	    }
	    gettimeofday(&t1, NULL);
	}
}

//static    uint8_t hs_frame[80000][LNP_HS_PKT_SIZE];
//static    uint8_t pb_frame[80000][LNP_RTT_PKT_SIZE];
//static    uint8_t hb_frame[80000][LNP_HB_PKT_SIZE];
//static    uint8_t hback_frame[80000][LNP_HBACK_PKT_SIZE];
uint8_t hs_frame[NO_OF_PATHS][LNP_HS_PKT_SIZE];
uint8_t pb_frame[NO_OF_PATHS][LNP_RTT_PKT_SIZE];
uint8_t hb_frame[NO_OF_PATHS][LNP_HB_PKT_SIZE];
uint8_t hback_frame[NO_OF_PATHS][LNP_HBACK_PKT_SIZE];
     
int main(void)
{


    //char **localhost=NULL;
    char srchost[16];
    //const char *dsthost = "192.168.0.1";
    //char *dsthost =NULL;
    char dsthost[16];
    //strcpy(dsthost, "192.168.0.1");
    //char dsthost[16] = "192.168.0.1";
    unsigned int packet_size;
    unsigned int hs_packet_size;
    unsigned int hb_packet_size;
    unsigned int pb_packet_size;
    unsigned int hback_packet_size;
    unsigned int data_size;
    //struct sockaddr_in src_addr[NO_OF_NODES];
    struct sockaddr_in src_addr;
    struct sockaddr_ll socket_address;

    char *file_name = "ip.txt";
    char *file_name2 = "clid.txt";

    FILE *fp, *fp2;
    fp = fopen(file_name, "r");
    fp2 = fopen(file_name2, "r");
    uint8_t dstmac_hex[6];
    uint8_t dstmac[17];

    user_input input;


    if (ini_parse("config.ini", user_input_handler, &input) < 0) {
        printf("Can't load 'config.ini'\n");
        return 1;
    }

    strcpy(dsthost, input.dest_ip);
    unsigned char *dest_clid = (uint8_t *)&(input.dest_clid);
    strcpy(dstmac, input.dest_mac);
    sscanf(dstmac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx\n", &dstmac_hex[0], &dstmac_hex[1], &dstmac_hex[2], &dstmac_hex[3], &dstmac_hex[4], &dstmac_hex[5]);

    //unsigned char dest_clid[] = { 0x80, 0x0b, 0x98, 0x01 };

    dst_addr.sin_family = AF_INET;
    dst_addr.sin_port =  htons(7007);
    inet_aton(dsthost, &dst_addr.sin_addr);
 

     unsigned char heartbeat[] = { 0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4b, 0x6f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x18, 0xe3, 0x00, 0x00, 0x01, 0x08, 0x00, 0x80, dest_clid[3], dest_clid[2], dest_clid[1], dest_clid[0], 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x42, 0x0f, 0x00, 0x40, 0x42, 0x0f, 0x00, 0x40, 0x42, 0x0f, 0x00, 0xfe, 0x41, 0x0f, 0x00, 0x4c, 0x76, 0x45, 0x72, 0x43, 0x6f, 0x4d, 0x70, 0x10 }; 
//     printf("%0x %0x %0x %0x\n", heartbeat[44], heartbeat[45], heartbeat[46], heartbeat[47]);
//     unsigned char heartbeat[] = { 0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x13, 0x2e, 0x00, 0x00, 0x01, 0xe8, 0x02, 0x80, dest_clid[3], dest_clid[2], dest_clid[1], dest_clid[0], 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };


    unsigned char rttProbe[] = { 0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0xc5, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x48, 0x01, 0x80, dest_clid[3], dest_clid[2], dest_clid[1], dest_clid[0], 0xc5, 0x70, 0xc7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x42, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x0f, 0x42, 0x40, 0x00, 0x00, 0x00, 0x01  };

    unsigned char handshake[] = {0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x75, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x07, 0xba, 0x01, 0x01, 0xfe, 0xb9, 0x01, 0x01, 0xfe, 0x02, 0x48, 0x01, 0x80, dest_clid[3], dest_clid[2], dest_clid[1], dest_clid[0], 0x1b, 0x5f, 0x1b, 0x5f, 0x00, 0x00, 0x00, 0x00};


    unsigned char heartbeatack[] = {0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xcf, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x13, 0x2e, 0x00, 0x00, 0x01, 0x68, 0x01, 0x80, dest_clid[3], dest_clid[2], dest_clid[1], dest_clid[0], 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4c, 0x76, 0x45, 0x72, 0x43, 0x6f, 0x4d, 0x70, 0x10};


    unsigned int heartbeat_data_size = sizeof(heartbeat);
    unsigned int handshake_data_size = sizeof(handshake);
    unsigned int rtt_probe_data_size = sizeof(rttProbe);
    unsigned int heartbeatack_data_size = sizeof(heartbeatack);
    printf("\n hb %d hs %d pb %d hback %d\n", sizeof(heartbeat), sizeof(handshake) ,sizeof(rttProbe), sizeof(heartbeatack));



    /* */
        struct ifreq if_idx;
        struct ifreq if_mac;
        char ifName[IFNAMSIZ];
        strcpy(ifName, input.iface);
        //sock_fd = *raw_sock;
        //strcpy(ifName, DEFAULT_IF);
        /* Open RAW socket to send on */
        if ((raw_sock = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
            perror("socket");
        }
	printf("ifname %s\n", ifName);

        /* Get the index of the interface to send on */
        memset(&if_idx, 0, sizeof(struct ifreq));
        strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
        if (ioctl(raw_sock, SIOCGIFINDEX, &if_idx) < 0)
            perror("SIOCGIFINDEX");

        /* Get the MAC address of the interface to send on */
        memset(&if_mac, 0, sizeof(struct ifreq));
        strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
        if (ioctl(raw_sock, SIOCGIFHWADDR, &if_mac) < 0)
            perror("SIOCGIFHWADDR");

	 printf("mac %s\n", if_mac.ifr_name);

    for (int i=0; i<NO_OF_NODES; i++) {

	/*Initialise to 0 */
        memset(hb_frame[i], 0, LNP_HB_PKT_SIZE);
        memset(pb_frame[i], 0, LNP_RTT_PKT_SIZE);
        memset(hs_frame[i], 0, LNP_HS_PKT_SIZE);
        memset(hback_frame[i], 0, LNP_HBACK_PKT_SIZE);


	/* Copy the data into the array */
	int data_start_posn = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE;
        memcpy(hb_frame[i]+data_start_posn, heartbeat, sizeof(heartbeat));
        memcpy(pb_frame[i]+data_start_posn, rttProbe, sizeof(rttProbe));
        memcpy(hs_frame[i]+data_start_posn, handshake, sizeof(handshake));
        memcpy(hback_frame[i]+data_start_posn, heartbeatack, sizeof(heartbeatack));

    }


    /* Modify the Source CLID in the data */
    get_source_clids(fp2, (uint8_t *)hb_frame, (uint8_t *)pb_frame, (uint8_t *)hs_frame, (uint8_t *)hback_frame);

    src_addr.sin_port =  htons(13013);
    for (int i=0; i<NO_OF_NODES; i++) {

        get_ips_from_file(fp, srchost);
	//printf("%s\n", srchost);
        inet_aton(srchost, &src_addr.sin_addr);

	/* Fill the Ethernet, IP and UDP header for  Heartbeat */
        packet_size = build_udp_packet(src_addr, dst_addr, hb_frame[i]+34, heartbeat_data_size);
        packet_size = build_ip_packet(src_addr.sin_addr, dst_addr.sin_addr, IPPROTO_UDP, hb_frame[i]+14,  packet_size);
        hb_packet_size = build_ethernet_frame(&raw_sock, hb_frame[i], packet_size, &socket_address, if_idx, if_mac, dstmac_hex);
	
	/* Fill the Ethernet, IP and UDP header for probe */
        packet_size = build_udp_packet(src_addr, dst_addr, pb_frame[i]+34,  rtt_probe_data_size);
        packet_size = build_ip_packet(src_addr.sin_addr, dst_addr.sin_addr, IPPROTO_UDP, pb_frame[i]+14,  packet_size);
        pb_packet_size = build_ethernet_frame(&raw_sock, pb_frame[i],  packet_size, &socket_address, if_idx, if_mac, dstmac_hex);
	
	/* Fill the Ethernet, IP and UDP header for  Handsahke */
        packet_size = build_udp_packet(src_addr, dst_addr, hs_frame[i]+34,  handshake_data_size);
        packet_size = build_ip_packet(src_addr.sin_addr, dst_addr.sin_addr, IPPROTO_UDP, hs_frame[i]+14,  packet_size);
        hs_packet_size = build_ethernet_frame(&raw_sock, hs_frame[i], packet_size, &socket_address, if_idx, if_mac, dstmac_hex);

	/* Fill the Ethernet, IP and UDP header for HeartbeatAck */
        packet_size = build_udp_packet(src_addr, dst_addr, hback_frame[i]+34, heartbeatack_data_size);
        packet_size = build_ip_packet(src_addr.sin_addr, dst_addr.sin_addr, IPPROTO_UDP, hback_frame[i]+14,  packet_size);
        hback_packet_size = build_ethernet_frame(&raw_sock, hback_frame[i], packet_size, &socket_address, if_idx, if_mac, dstmac_hex);
    }

    fclose(fp);
    /* Print the hexdump 
    for (int i=0; i<NO_OF_NODES; i++) {
        hexdump(frame[i], packet_size);
    } 
    */

    fclose(fp2);
    int NO_OF_BUNCHES=1;
    int NO_OF_SPOKES_PER_BUNCH=NO_OF_PATHS;

    pthread_t spokes_bunch[NO_OF_BUNCHES];
    struct spoke_set spoke_sets[NO_OF_BUNCHES];
    int thr_status;

    for (int i=0; i<NO_OF_BUNCHES; i++) {
	int bunch_idx = i;    
	//bunch_idx=0; // Remove this line when actual bunches are there
	spoke_sets[i].no_of_spokes_per_bunch = NO_OF_SPOKES_PER_BUNCH;
	spoke_sets[i].socket_address = &socket_address; 

	spoke_sets[i].hb_packet_size = LNP_HB_PKT_SIZE;
	spoke_sets[i].hs_packet_size = LNP_HS_PKT_SIZE;
	spoke_sets[i].pb_packet_size = LNP_RTT_PKT_SIZE;
	spoke_sets[i].hback_packet_size = LNP_HBACK_PKT_SIZE;

	spoke_sets[i].hb_frame = &hb_frame[bunch_idx][0];
	spoke_sets[i].hback_frame = &hback_frame[bunch_idx][0];
	spoke_sets[i].hs_frame = &hs_frame[bunch_idx][0];
	spoke_sets[i].pb_frame = &pb_frame[bunch_idx][0];

        thr_status = pthread_create(&spokes_bunch[i], NULL, start_spoke_bunch, &spoke_sets[i]);
    }

    /*
    for (int i = 0; i< 1; i++) {
	    uint8_t *hb_test = hb_frame[i];
	    for (int j=0; j<LNP_HB_PKT_SIZE; j++) {
		    printf("%0x ", (*hb_test++));
	    }
    } */

    /* Wait for thread to proceed continously */
    while (1) {
    }

    /* Handle Signal interupts */

    return 0;
}

