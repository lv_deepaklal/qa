
from random import getrandbits
from ipaddress import IPv4Address, IPv6Address
def randip():
    v=4
    if v == 4:
        bits = getrandbits(32) # generates an integer with 32 random bits
        addr = IPv4Address(bits) # instances an IPv4Address object from those bits
        addr_str = str(addr) # get the IPv4Address object's string representation
    elif v == 6:
        bits = getrandbits(128) # generates an integer with 128 random bits
        addr = IPv6Address(bits) # instances an IPv6Address object from those bits
        # .compressed contains the short version of the IPv6 address
        # str(addr) always returns the short address
        # .exploded is the opposite of this, always returning the full address with all-zero groups and so on
        addr_str = addr.compressed

    #print(addr_str)
    return addr_str


ips = 80000
iplist = []
while len(iplist) <= 80000:
    ip = randip()
    if ip not in iplist :
        iplist.append(ip)


f=open('w.txt', 'w')
for ip in iplist:
    f.write(ip+'\n')

f.close()

