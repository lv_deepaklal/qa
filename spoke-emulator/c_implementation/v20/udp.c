#include <stdlib.h> 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <linux/if_ether.h>

// for struct ether_header
#include <netinet/ether.h>
// for struct ifreq
#include <net/if.h>

#include <linux/if_packet.h>
#include <sys/ioctl.h>

#include "udp.h"

#define MY_DEST_MAC0	0x00
#define MY_DEST_MAC1	0x11
#define MY_DEST_MAC2	0x11
#define MY_DEST_MAC3	0x11
#define MY_DEST_MAC4	0x11
#define MY_DEST_MAC5	0x11
//#define DEFAULT_IF	"eth1"

// For little endian
struct pseudo_iphdr
{
    uint32_t source_addr;
    uint32_t dest_addr;
    uint8_t zeros;
    uint8_t prot;
    uint16_t length;
};


uint16_t checksum(uint8_t *data, unsigned int size)
{
    int i;
    int sum = 0;
    uint16_t *p = (uint16_t *)data;

    for(i = 0; i < size; i += 2){
        sum += *(p++);
    }

    uint16_t carry = sum >> 16;
    uint16_t tmp = 0x0000ffff & sum;
    uint16_t res = ~(tmp + carry);

    return res;
}


unsigned int build_ethernet_frame(int *sockfd, uint8_t *frame,  unsigned int data_size, struct sockaddr_ll *socket_address, struct ifreq if_idx, struct ifreq if_mac)
{
        struct ether_header *eh;

        eh = (struct ether_header *)frame;
	/* Ethernet header */
	eh->ether_shost[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
	eh->ether_shost[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
	eh->ether_shost[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
	eh->ether_shost[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
	eh->ether_shost[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
	eh->ether_shost[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];
	eh->ether_dhost[0] = MY_DEST_MAC0;
	eh->ether_dhost[1] = MY_DEST_MAC1;
	eh->ether_dhost[2] = MY_DEST_MAC2;
	eh->ether_dhost[3] = MY_DEST_MAC3;
	eh->ether_dhost[4] = MY_DEST_MAC4;
	eh->ether_dhost[5] = MY_DEST_MAC5;
	/* Ethertype field */
	eh->ether_type = htons(ETH_P_IP);

		/* Index of the network device */
	socket_address->sll_ifindex = if_idx.ifr_ifindex;
	/* Address length*/
	socket_address->sll_halen = ETH_ALEN;
	/* Destination MAC */
	socket_address->sll_addr[0] = MY_DEST_MAC0;
	socket_address->sll_addr[1] = MY_DEST_MAC1;
	socket_address->sll_addr[2] = MY_DEST_MAC2;
	socket_address->sll_addr[3] = MY_DEST_MAC3;
	socket_address->sll_addr[4] = MY_DEST_MAC4;
	socket_address->sll_addr[5] = MY_DEST_MAC5;

        //memcpy(frame + sizeof(struct ether_header), data, data_size);

        return sizeof(struct ether_header) + data_size;
}

unsigned int build_ip_packet(struct in_addr src_addr, struct in_addr dst_addr, uint8_t protocol, uint8_t *ip_packet, unsigned int data_size)
{
    struct iphdr *ip_header;

    ip_header = (struct iphdr *)ip_packet;
    ip_header->version = 4;
    ip_header->ihl = INET_HDR_LEN;
    ip_header->tos = 0;
    ip_header->tot_len = htons(INET_HDR_LEN * 4 + data_size);
    ip_header->id = 0; // Filled in automatically
    ip_header->frag_off = 0;
    ip_header->ttl = 64;
    ip_header->protocol = protocol;
    ip_header->check = 0; // Filled in automatically
    ip_header->saddr = src_addr.s_addr;
    ip_header->daddr = dst_addr.s_addr;

    //memcpy(ip_packet + sizeof(struct iphdr), data, data_size);

    return sizeof(struct iphdr) + data_size;
}


#define MAX_PSEUDO_PKT_SIZE 1024

unsigned int build_udp_packet(struct sockaddr_in src_addr, struct sockaddr_in dst_addr, uint8_t *udp_packet,  unsigned int data_size)
{
    //uint8_t pseudo_packet[MAX_PSEUDO_PKT_SIZE];
    uint8_t *pseudo_packet = (uint8_t *)calloc(MAX_PSEUDO_PKT_SIZE , sizeof(uint8_t));
    uint16_t length;
    struct udphdr *udph;
    struct pseudo_iphdr *p_iphdr = (struct pseudo_iphdr *)pseudo_packet;

    length = UDP_HDR_SIZE + data_size;
    udph = (struct udphdr *)udp_packet;
    udph->source = src_addr.sin_port;
    //udph->source = sin_port;
    udph->dest = dst_addr.sin_port;
    udph->len = htons(length);
    //memcpy(udp_packet + UDP_HDR_SIZE, data, data_size);

    //if(length + sizeof(struct pseudo_iphdr) > MAX_PSEUDO_PKT_SIZE){
    //    fprintf(stderr, "Buffer size not enough");
    //    exit(1);
    //}

    // Calculate checksum with pseudo ip header
    p_iphdr->source_addr = src_addr.sin_addr.s_addr;
    //p_iphdr->source_addr = s_addr;
    p_iphdr->dest_addr = dst_addr.sin_addr.s_addr;
    p_iphdr->zeros = 0;
    p_iphdr->prot = IPPROTO_UDP; //udp
    p_iphdr->length = udph->len;

    // Do NOT use udph->len instead of length.
    // udph->len is in big endian
    memcpy(pseudo_packet + sizeof(struct pseudo_iphdr), udph, length);
    udph->check = checksum(pseudo_packet, sizeof(struct pseudo_iphdr) + length);
    free(pseudo_packet);

    return length;
}

