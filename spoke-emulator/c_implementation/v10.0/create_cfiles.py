
import json

json_file = 'nw_json.json'
no_of_spokes = 20000
spoke_counts = 20000
no_of_wans = 3
#no of spokes - to emulate; spoke counts - actual data present
no_of_iters = no_of_spokes/spoke_counts
if no_of_spokes%spoke_counts: no_of_iters=no_of_iters+1 
def create():
    with open(json_file, 'r') as f:
        spoke_info = json.loads(f.read())

    file_iplist = 'ip.txt'
    file_clidlist = 'clid.txt'

    wrote_count=0

    CLIDS = {}
    CLIDS_HEX = {}
    SPOKES = []
    IPS = []
    CLIDS = []

    sorted_spokes = spoke_info.keys()
    sorted_spokes.sort()
    for spoke_name in sorted_spokes:
        for iface_info in spoke_info[spoke_name]:
            if iface_info.get("role", None)=="wan" and iface_info.get("mpls", None)=="no" and iface_info.get("state", None)=="up":
                for iface_indiv in iface_info.get("ip", []):
                     #IPS.add ( iface_indiv["ip"] )
                     SPOKES.append( str( spoke_name ) )
                     IPS.append ( str(iface_indiv["ip"]) )
                     #CLIDS.append ( iface_info.get("clid", 0) )
                     #CLIDS_HEX.append ( "%08x"% iface_info.get("clid", 0) )
                     #CLIDS[str(spoke_name)] =  iface_info.get("clid", 0)
                     CLIDS.append (iface_info.get("clid", 0)) 
                     CLIDS_HEX[str(spoke_name)] ="%08x"% iface_info.get("clid", 0)
                     #break #breaking on 1st element
        
                #break

    f_ip = open(file_iplist, 'w')
    f_clid = open(file_clidlist, 'w')

    print(len(IPS), len(SPOKES))
    total_wrote_count=0
    for i in range(no_of_iters):
        wrote_count=0
        while wrote_count < spoke_counts and total_wrote_count<no_of_spokes:
                f_ip.write(str(IPS[wrote_count])+'\n')
                spoke_name = SPOKES[wrote_count]
                #print(CLIDS[spoke_name])
                #f_clid.write(str(CLIDS[spoke_name])+'\n')
                f_clid.write(str(CLIDS[wrote_count])+'\n')
                wrote_count=wrote_count+1
                total_wrote_count=total_wrote_count+1

    f_ip.close()
    f_clid.close()

if __name__ == "__main__":
    create()

