import json
import netaddr

nw_json = "nw_json.json"
resp_json_file = "ln_response.json"
resp_json ={}

with open(nw_json, 'r') as f: 
    info = json.loads(f.read())

                              
spoke_ids = info.keys()       
spoke_ids.sort()              

print(len(spoke_ids))
#for s in spoke_ids:
#    print(s)
                              

                              
resp_json = []                            

for spoke_id in spoke_ids:    
    iface_info_list = info[spoke_id]
    lan_infos = []
    wan_infos = []
    print(len(iface_info_list))
    for iface_info in iface_info_list:
        if iface_info['role'] == 'lan':
            lanip = iface_info['ip'][0]['ip']
            lanmask = iface_info['ip'][0]['mask']
            lan_nw = netaddr.IPNetwork('%s/%s'%(lanip, lanmask)) 
            lan_nw = '%s/%s' %(str(lan_nw.network), str(lan_nw.prefixlen))
            lan_infos.append({
                           "resolved": "yes",
                           "nwgrp": "0",
                           "dest_network": lan_nw, 
                           "resolve": "%s/32" %(lanip) 
                             })
        #print(lan_infos)
        if iface_info['role'] == 'wan':
            wanip = iface_info['ip'][0]['ip']
            clid  = str(iface_info['clid'])
            wan_type = "internet" if iface_info['mpls']=="no" else "mpls"
            mpls_ids = ["1"] if iface_info['mpls']=="no" else [ iface_info['mpls'] ]
            wan_infos.append({
                    "heartbeat-enable": "1",
                    "handshake-enable": "1",
                    "wan-type": wan_type,
                    "mpls-ids": mpls_ids,
                    "is_dyn_ip": "1",
                    "ip": wanip,
                    "clid": clid
                    })

    #print(len(lan_infos))
    #print(len(wan_infos))
    #print(lan_infos)
    #print(wan_infos)
    for lan_info in lan_infos:
        lan_info.update( {"nexthop_info": wan_infos } )
        resp_json.append(lan_info)


    
                              
#print(resp_json)
                              
with open(resp_json_file, 'w') as f:
    #f.write(json.dumps(resp_json, indent=5, sort_keys=False))
    f.write(json.dumps(resp_json))
                                                                 
