#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <linux/if_ether.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <net/if.h>

// for sleep
#include <unistd.h>
//For ethernet struct
#include <netinet/ether.h>

#include <pthread.h>
#include<signal.h>
#include<errno.h>

#include "udp.h"

#include "ini.h"


#define LN_DATA_LEN 186
#define MAX_DATA_SIZE 100
#define FOO ""


#define LNP_HB_DATA_SIZE 85
#define LNP_HS_DATA_SIZE 60
#define LNP_RTT_DATA_SIZE 88
#define LNP_HBACK_DATA_SIZE 85 
#define LN_IP_HDR_SIZE 20
#define LN_UDP_HDR_SIZE 8
#define LN_ETH_HDR_SIZE 14

#define LNP_HB_PKT_SIZE     LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HB_DATA_SIZE 
#define LNP_HS_PKT_SIZE     LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HS_DATA_SIZE 
#define LNP_RTT_PKT_SIZE    LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_RTT_DATA_SIZE
#define LNP_HBACK_PKT_SIZE  LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HBACK_DATA_SIZE

#define NO_OF_PATHS 20000
#define IFACES  4

int HB_PKT_SIZE    = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HB_DATA_SIZE; 
int HS_PKT_SIZE    = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HS_DATA_SIZE ;
int RTT_PKT_SIZE   = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_RTT_DATA_SIZE;
int HBACK_PKT_SIZE = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+ LNP_HBACK_DATA_SIZE;

int raw_sock[IFACES];
extern int errno;
int STOP=0;
struct sockaddr_in dst_addr[IFACES];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

typedef struct {
   const char *ifaces;
   int no_of_ifaces;
   int no_of_paths;

   const char *iface_tag[IFACES];
   const char *iface[IFACES];
   const char *dest_ip[IFACES];
   const char *dest_mac[IFACES];
   uint32_t   dest_clid[IFACES];
   const char *fp_ip[IFACES];
   const char *fp_clid[IFACES];

} user_input;


void wait_here() {
	while (1) {
	}
}


int get_no_of_ifaces(const char *ifaces, char *iface[IFACES]) {

    int iface_count = 0;
    int char_idx = 0;
    int perifaceidx = 0;

    char *p  = calloc(10, sizeof(char));

    while (ifaces[char_idx] != '\0') {
        if (ifaces[char_idx] == ',') {
		*(p+perifaceidx) = '\0';
                iface[iface_count] = p;

		iface_count++;
                p  = calloc(10, sizeof(char));
		//printf("ifacec outn %d %p \n", iface_count, p);
		perifaceidx=0;
	} else {
            *(p+perifaceidx) = ifaces[char_idx];
	    //printf("%p %c\n", p+perifaceidx, *(p+perifaceidx));
	    perifaceidx++;
	}

        char_idx++;
    }

    //printf("perifaceifx %d \n",  perifaceidx);
    if (perifaceidx) {
		*(p+perifaceidx) = '\0';
                iface[iface_count] = p;

		iface_count++;
    }
//printf("ifacec outn end %d char_idx %d\n", iface_count, char_idx);

    /* Limit to max ifaces supported */
    if (iface_count > IFACES) {
        return IFACES;
    } else {
        return iface_count;
    }
}


int get_iface_position( const char *section, char *iface[IFACES], int *posn) {

	//printf("getiface section %s %d\n", section, *posn);
	for (int i=0; i<IFACES; i++) {
	    //printf("iface[%d] = %p %s\n",i, iface[i], iface[i]);
		if (iface[i] != NULL) {
			//printf("stcmp %d\n",strcmp(section, iface[i]));
			if (strcmp(section, iface[i])==0) {
			    *posn = i;
                            return 1;
	                }
		}
		//return 0;
	}
	return 0;
}

   static  int validity_counter = 0;
static int user_input_handler(void* user, const char* section, const char* name, const char* value) {

   /*static  int validity_counter = 0;
   static  int per_iface_args = 6;
   static  int general_args = 2;
   static  int get_further_info = 0;
   static  int no_of_ifaces = 0;
   static int valid_counter_value = general_args +  (per_iface_args * no_of_ifaces); */
   int iface_posn = -1;

    user_input *input = (user_input *)user;
    //printf("userinfos=> %p %s %s %s\n", user, section, name, value);
    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0


    if (MATCH("general", "ifaces")) {
        input->ifaces = strdup(value);
	//printf("ifaces are %s\n", input->ifaces );
	//printf("len = %d", strlen(input->ifaces));
        input->no_of_ifaces = get_no_of_ifaces(input->ifaces, input->iface_tag);
	//printf("no if ifaces %d\n", input->no_of_ifaces);
	validity_counter++;
    } else if (MATCH("general", "no_of_paths")) {
        input->no_of_paths = (int)(atoi(value));
	//printf("input->no_of_paths %d\n", input->no_of_paths);
	if (input->no_of_paths > NO_OF_PATHS) {
		input->no_of_paths = NO_OF_PATHS;
	}
	//printf("input->no_of_paths %d\n", input->no_of_paths);
	validity_counter++;
    } else if (get_iface_position(section, input->iface_tag, &iface_posn))  {

	    //printf("==iface itag posn %p\n", input->iface_tag[iface_posn]);
	    //printf("match %d\n", MATCH(input->iface_tag[iface_posn], "iface"));
	    //printf("strcmp %d\n", strcmp(section, input->iface_tag[iface_posn])); 
	    //printf("strcmp %d\n", strcmp(name, "iface"));
                if (MATCH(input->iface_tag[iface_posn], "iface")) {
			//printf("  section %s iface\n ", section);
			input->iface[iface_posn] = strdup(value);
		} else if (MATCH(input->iface_tag[iface_posn], "dest_ip")) {
			//printf("  section %s dest ip\n ", section);
			input->dest_ip[iface_posn] = strdup(value);
			//printf("-------------%s\n", input->dest_ip[iface_posn]);
	                validity_counter++;
		} else if (MATCH(input->iface_tag[iface_posn], "dest_mac")) {
			input->dest_mac[iface_posn] = strdup(value);
	                validity_counter++;
		} else if (MATCH(input->iface_tag[iface_posn], "dest_clid")) {
			//printf("  section %s dest clid\n ", section);
			input->dest_clid[iface_posn] = (uint32_t)htonl(atol(value));
			//printf("destclid[%d] %ld\n", iface_posn, input->dest_clid[iface_posn]);
	                validity_counter++;
		} else if (MATCH(input->iface_tag[iface_posn], "ips_list")) {
			//printf("  section %s ipslist\n ", section);
			input->fp_ip[iface_posn] = strdup(value);
	                validity_counter++;
		} else if (MATCH(input->iface_tag[iface_posn], "clids_list")) {
			input->fp_clid[iface_posn] = strdup(value);
	                validity_counter++;
		} else {
			return 0;
		}
    } else {
	    return 0;
    }

    return 1;
}


/* Prints the hexdump */
void hexdump(unsigned char *data, unsigned int data_bytes)
{
    int bin_p, ascii_p;

    bin_p = ascii_p = 0;

    while(bin_p < data_bytes){
        int j;
        int whitespaces;
        for(j = 0; j < 8 && bin_p < data_bytes; j++){
            printf("%02x ", data[bin_p++]);
        }

        whitespaces = (8 - j) * 3;
        for(j = 0; j < whitespaces; j++){
            printf(" ");
        }

        for(j = 0; j < 8 && ascii_p < data_bytes; j++){
            if(isprint(data[ascii_p])){
                printf("%c", data[ascii_p++]);
            }else{
                printf(".");
                ascii_p++;
            }
        }

        printf("\n");
    }
}


int HBEAT_SRCCLID_POSN = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+44; 
int HBEATACK_SRCCLID_POSN = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+44; 
int RTTPROBE_SRCCLID_POSN = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+44; 
int HSHAKE_SRCCLID_POSN =LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+44;

int HBEAT_DESTCLID_POSN = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+48; 
int HBEATACK_DESTCLID_POSN = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+48; 
int RTTPROBE_DESTCLID_POSN = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+48; 
int HSHAKE_DESTCLID_POSN =LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE+48;

void get_dest_clids(int no_of_paths, char *dest_clid, uint8_t *heart_beat_data, uint8_t *rtt_probe_data, uint8_t *handshake_data, uint8_t *heart_beat_ack_data) {

	for (int node_idx=0; node_idx<no_of_paths; node_idx++) {
	   *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_DESTCLID_POSN)   = *(dest_clid + 3);
	   *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_DESTCLID_POSN+1) = *(dest_clid + 2);
	   *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_DESTCLID_POSN+2) = *(dest_clid + 1);
	   *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_DESTCLID_POSN+3) = *(dest_clid + 0);
           
	   *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_DESTCLID_POSN)   = *(dest_clid + 3);
	   *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_DESTCLID_POSN+1) = *(dest_clid + 2);
	   *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_DESTCLID_POSN+2) = *(dest_clid + 1);
	   *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_DESTCLID_POSN+3) = *(dest_clid + 0);
           
	   *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_DESTCLID_POSN)   =  *(dest_clid + 3);
	   *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_DESTCLID_POSN+1) =  *(dest_clid + 2);
	   *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_DESTCLID_POSN+2) =  *(dest_clid + 1);
	   *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_DESTCLID_POSN+3) =  *(dest_clid + 0);
           
	   *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_DESTCLID_POSN)   =  *(dest_clid + 3);
	   *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_DESTCLID_POSN+1) =  *(dest_clid + 2);
	   *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_DESTCLID_POSN+2) =  *(dest_clid + 1);
	   *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_DESTCLID_POSN+3) =  *(dest_clid + 0);
	}
}


/* Modify the data with Source CLID */
void get_source_clids(int no_of_paths, FILE *file, uint8_t *heart_beat_data, uint8_t *rtt_probe_data, uint8_t *handshake_data, uint8_t *heart_beat_ack_data) {

    //int src_clid = 4;
    if ( file != NULL ) {
      char line [ 128 ]; /* or other suitable maximum line size */

      int node_idx = 0;
      while ( fgets ( line, sizeof line, file ) != NULL && node_idx < no_of_paths ) {
         unsigned int ab = (unsigned int)atoi(line);


	 *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((heart_beat_data+node_idx*HB_PKT_SIZE)+ HBEAT_SRCCLID_POSN+3) = (ab >> 24) & 255;

         ab = (unsigned int)atoi(line);
	 *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((rtt_probe_data+node_idx*RTT_PKT_SIZE)+ RTTPROBE_SRCCLID_POSN+3) = (ab >> 24) & 255;

         ab = (unsigned int)atoi(line);
	 *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((handshake_data+node_idx*HS_PKT_SIZE)+ HSHAKE_SRCCLID_POSN+3) = (ab >> 24) & 255;

         ab = (unsigned int)atoi(line);
	 *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_SRCCLID_POSN) = (ab >> 0) & 255;
	 *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_SRCCLID_POSN+1) = (ab >> 8) & 255;
	 *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_SRCCLID_POSN+2) = (ab >> 16) & 255;
	 *((heart_beat_ack_data+node_idx*HBACK_PKT_SIZE)+ HBEATACK_SRCCLID_POSN+3) = (ab >> 24) & 255;
	 node_idx++;
      }
      //fclose ( file );
    } else {
      printf("Failed to open the file\n");
    }
}

/* Read the file and get Source IP */
char * get_ips_from_file(FILE *fp, char *localhost ) {
   //fp = fopen(file_name, "r"); // read mode
 
   if (fp == NULL)
   {
      perror("Error while opening the file.\n");
      exit(EXIT_FAILURE);
   }
 
    //printf("loalhsot%p\n", localhost);
    //localhost = (char  **) malloc (NO_OF_NODES * sizeof (char *));
    //for (int i=0; i< NO_OF_NODES; i++) {
    //    localhost[i] = (char *) malloc ( 16 * sizeof(char));
    //}
 
   int i=0;
   char ch;
   int start = 0;
   int end = 0;
   int ip_idx = 0;
   //for (int i =0; i<NO_OF_NODES; i++) {
   //}
//	   for (int j=0; j<16; j++)
//		   localhost[i]='\0';

   char ip[16];
   int per_ip_idx = 0;
   while((ch = fgetc(fp)) != EOF && per_ip_idx < 16) {
      //printf("%c", ch);
      if (ch=='\n') {
          localhost[per_ip_idx++] = '\0';
          //ip[per_ip_idx++] = '\0';
          //per_ip_idx=0;
	  break;
          //strcpy(localhost[ip_idx++], ip);
      } else {
          //ip[per_ip_idx++] = ch;
          localhost[per_ip_idx++] = ch;
      }
   }
   return localhost;
}


struct spoke_set {
    //int no_of_nodes;
    int raw_sock_idx;
    struct sockaddr_ll *socket_address;
    unsigned int no_of_paths_per_bunch;
    unsigned int hs_packet_size;
    unsigned int pb_packet_size;
    unsigned int hb_packet_size;
    unsigned int hback_packet_size;
    uint8_t *hb_frame;
    uint8_t *hs_frame;  
    uint8_t *pb_frame;  
    uint8_t *hback_frame;  
};

long int BUFFER_TIME_US = 5000; //1ms buffer
void * start_spoke_bunch(void *args) {

	struct spoke_set *sp_set = (struct spoke_set *) args;
        int c=0;
        int s1=0, s2=0, s3=0, s4=0;

	long start, end;
	struct timeval t1, t2;
	gettimeofday(&t1, NULL);
	while (!STOP) {

	    start = (long)t1.tv_sec * 1000000 + (long)t1.tv_usec ;
            for (int i=0; i<sp_set->no_of_paths_per_bunch; i++) {
		   // printf("i=%d c=%d\n",i, c);
		   //printf("raw sockidx %d\n", sp_set->raw_sock_idx);
                s1 = sendto(raw_sock[sp_set->raw_sock_idx], sp_set->hb_frame + (i*sp_set->hb_packet_size) , sp_set->hb_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));
                s2 = sendto(raw_sock[sp_set->raw_sock_idx], sp_set->pb_frame + (i*sp_set->pb_packet_size), sp_set->pb_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));
                s4 = sendto(raw_sock[sp_set->raw_sock_idx], sp_set->hback_frame + (i*sp_set->hback_packet_size), sp_set->hback_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));

                if (c%5==0) {
		  /* Send this only once in 5sec*/
                  s3 = sendto(raw_sock[sp_set->raw_sock_idx], sp_set->hs_frame + (i*sp_set->hs_packet_size), sp_set->hs_packet_size, 0, (struct sockaddr *)sp_set->socket_address, sizeof(struct sockaddr_ll));
 	          c=0;
 	        }
            }
 	    c++;
	    if (c==5) {
		    //STOP=1;
	    }

	    long int interval_us = 1000000; 
	    gettimeofday(&t2, NULL);
	    end = (long)t2.tv_sec * 1000000 + (long)t2.tv_usec ;
	    //printf("%ld useconds elapsed\n", (end - start));
	    printf("Sent successfully %d spokes data...\n", sp_set->no_of_paths_per_bunch);
	    long int wait_us = (long int)(interval_us - (end - start) - BUFFER_TIME_US);  
	    if (wait_us > 0) {
                usleep(wait_us);
	    }
	    gettimeofday(&t1, NULL);
	}
}

//static    uint8_t hs_frame[80000][LNP_HS_PKT_SIZE];
//static    uint8_t pb_frame[80000][LNP_RTT_PKT_SIZE];
//static    uint8_t hb_frame[80000][LNP_HB_PKT_SIZE];
//static    uint8_t hback_frame[80000][LNP_HBACK_PKT_SIZE];
uint8_t hs_frame[IFACES][NO_OF_PATHS][LNP_HS_PKT_SIZE];
uint8_t pb_frame[IFACES][NO_OF_PATHS][LNP_RTT_PKT_SIZE];
uint8_t hb_frame[IFACES][NO_OF_PATHS][LNP_HB_PKT_SIZE];
uint8_t hback_frame[IFACES][NO_OF_PATHS][LNP_HBACK_PKT_SIZE];
     
int main(void)
{


    //char **localhost=NULL;
    char srchost[16];
    //const char *dsthost = "192.168.0.1";
    //char *dsthost =NULL;
    char dsthost[IFACES][16];
    //strcpy(dsthost, "192.168.0.1");
    //char dsthost[16] = "192.168.0.1";
    unsigned int packet_size;
    unsigned int hs_packet_size;
    unsigned int hb_packet_size;
    unsigned int pb_packet_size;
    unsigned int hback_packet_size;
    unsigned int data_size;
    //struct sockaddr_in src_addr[NO_OF_NODES];
    struct sockaddr_in src_addr;
    struct sockaddr_ll socket_address[IFACES];

    //char *file_name = "ip.txt";
    //char *file_name2 = "clid.txt";

    //FILE *fp_ip, *fp_clid;
    //fp_ip = fopen(file_name, "r");

    FILE *fp_emu_ips[IFACES], *fp_emu_clids[IFACES];
    //fp_clid = fopen(file_name2, "r");
    uint8_t dstmac_hex[IFACES][6];
    uint8_t dstmac[17];

    user_input input;
    input.ifaces = calloc( 64, sizeof(char));
    strcpy(input.ifaces, "");
    input.no_of_ifaces = 0;
    input.no_of_paths = 0;

    for (int i=0; i<IFACES; i++) {
	    input.iface_tag[i] = NULL;
    } 


    if (ini_parse("config.ini", user_input_handler, &input) < 0) {
        printf("Can't load 'config.ini'\n");
        return 1;
    }

    for (int i=0; i<input.no_of_ifaces; i++) {
        fp_emu_ips[i]   = fopen(input.fp_ip[i], "r");
        fp_emu_clids[i] = fopen(input.fp_clid[i], "r");
    }
    //printf("no of ifaces %d no f paths %d ifaces %s\n", input.no_of_ifaces, input.no_of_paths, input.ifaces);
    //for (int i=0; i<input.no_of_ifaces; i++) {
    //	    printf("ifaece %s\n", input.iface[i]);
    //	    printf("destip %s\n", input.dest_ip[i]);
    //	    printf("destmac %s\n", input.dest_mac[i]);
    //	    printf("destclid %d\n", input.dest_clid[i]);
    //	    printf("fpip %s\n", input.fp_ip[i]);
    //	    printf("fp clid %s\n", input.fp_clid[i]);
    //}
    


    for (int i=0; i<input.no_of_ifaces; i++) {
        strcpy(dsthost[i], input.dest_ip[i]);
    }
    //for (int i=0; i<input.no_of_ifaces; i++) {
    //   printf("dsthost[%d] %s\n", i, dsthost[i]);
    //}

    //input.no_of_ifaces = 0;

    unsigned char *dest_clid[IFACES];

    for (int i=0; i<input.no_of_ifaces; i++) {
        dest_clid[i]  = (uint8_t *)&(input.dest_clid[i]);
        strcpy(dstmac, input.dest_mac[i]);
        sscanf(dstmac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx\n", &dstmac_hex[i][0], &dstmac_hex[i][1], &dstmac_hex[i][2], &dstmac_hex[i][3], &dstmac_hex[i][4], &dstmac_hex[i][5]);
    }

    //unsigned char dest_clid[] = { 0x80, 0x0b, 0x98, 0x01 };

    for (int i=0; i<input.no_of_ifaces; i++) {
        dst_addr[i].sin_family = AF_INET;
        dst_addr[i].sin_port =  htons(7007);
        inet_aton(dsthost[i], &dst_addr[i].sin_addr);
    }
 

    /*
    unsigned char heartbeat[]   =  {} ;
    unsigned char rttProbe[]  =  {} ;
    unsigned char handshake[] =  {} ;
    unsigned char  heartbeatack[]  =  {} ;
    */
     unsigned char heartbeat[] =   { 0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4b, 0x6f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x18, 0xe3, 0x00, 0x00, 0x01, 0x08, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x42, 0x0f, 0x00, 0x40, 0x42, 0x0f, 0x00, 0x40, 0x42, 0x0f, 0x00, 0xfe, 0x41, 0x0f, 0x00, 0x4c, 0x76, 0x45, 0x72, 0x43, 0x6f, 0x4d, 0x70, 0x10 }; 

    unsigned char rttProbe[] =     { 0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0xc5, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x48, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0xc5, 0x70, 0xc7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x42, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x0f, 0x42, 0x40, 0x00, 0x00, 0x00, 0x01  };

    unsigned char handshake[] =    { 0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x75, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x07, 0xba, 0x01, 0x01, 0xfe, 0xb9, 0x01, 0x01, 0xfe, 0x02, 0x48, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x1b, 0x5f, 0x1b, 0x5f, 0x00, 0x00, 0x00, 0x00};


    unsigned char heartbeatack[] = { 0x00, 0x00, 0x81, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xcf, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x13, 0x2e, 0x00, 0x00, 0x01, 0x68, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4c, 0x76, 0x45, 0x72, 0x43, 0x6f, 0x4d, 0x70, 0x10};
     /*
      */


    unsigned int heartbeat_data_size = sizeof(heartbeat);
    unsigned int handshake_data_size = sizeof(handshake);
    unsigned int rtt_probe_data_size = sizeof(rttProbe);
    unsigned int heartbeatack_data_size = sizeof(heartbeatack);
    //printf("\n hb %d hs %d pb %d hback %d\n", sizeof(heartbeat), sizeof(handshake) ,sizeof(rttProbe), sizeof(heartbeatack));



    /* */
        struct ifreq if_idx[IFACES];
        struct ifreq if_mac[IFACES];
        char ifName[IFACES][IFNAMSIZ];
        for (int i=0; i<input.no_of_ifaces; i++) {
            strcpy(ifName[i], input.iface[i]);
        }
        /* Open RAW socket to send on */

    for (int i=0; i<input.no_of_ifaces; i++) {
        if ((raw_sock[i] = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
            perror("socket");
        }

        /* Get the index of the interface to send on */
        memset(&if_idx[i], 0, sizeof(struct ifreq));
        strncpy(if_idx[i].ifr_name, ifName[i], IFNAMSIZ-1);
        if (ioctl(raw_sock[i], SIOCGIFINDEX, &if_idx[i]) < 0)
            perror("SIOCGIFINDEX");

        /* Get the MAC address of the interface to send on */
        memset(&if_mac[i], 0, sizeof(struct ifreq));
        strncpy(if_mac[i].ifr_name, ifName[i], IFNAMSIZ-1);
        if (ioctl(raw_sock[i], SIOCGIFHWADDR, &if_mac[i]) < 0)
            perror("SIOCGIFHWADDR");

	 //printf("mac %s\n", if_mac.ifr_name);
     }

    for (int j=0; j<input.no_of_ifaces; j++) {

        for (int i=0; i<input.no_of_paths; i++) {

		/*Initialise to 0 */
                memset(hb_frame[j][i], 0, LNP_HB_PKT_SIZE);
                memset(pb_frame[j][i], 0, LNP_RTT_PKT_SIZE);
                memset(hs_frame[j][i], 0, LNP_HS_PKT_SIZE);
                memset(hback_frame[j][i], 0, LNP_HBACK_PKT_SIZE);
        
        
		/* Copy the data into the array */
		int data_start_posn = LN_ETH_HDR_SIZE+LN_IP_HDR_SIZE+LN_UDP_HDR_SIZE;
                memcpy(hb_frame[j][i]+data_start_posn, heartbeat, sizeof(heartbeat));
                memcpy(pb_frame[j][i]+data_start_posn, rttProbe, sizeof(rttProbe));
                memcpy(hs_frame[j][i]+data_start_posn, handshake, sizeof(handshake));
                memcpy(hback_frame[j][i]+data_start_posn, heartbeatack, sizeof(heartbeatack));
        }

    }


    /* Modify the Source CLID in the data */
    for (int j=0; j<input.no_of_ifaces; j++) {
        get_source_clids(input.no_of_paths, fp_emu_clids[j], (uint8_t *)hb_frame[j], (uint8_t *)pb_frame[j], (uint8_t *)hs_frame[j], (uint8_t *)hback_frame[j]);
        get_dest_clids(input.no_of_paths, dest_clid[j], (uint8_t *)hb_frame[j], (uint8_t *)pb_frame[j], (uint8_t *)hs_frame[j], (uint8_t *)hback_frame[j]);
    }

    src_addr.sin_port =  htons(13013);
    for (int j=0; j<input.no_of_ifaces; j++) {
        for (int i=0; i<input.no_of_paths; i++) {
        
         get_ips_from_file(fp_emu_ips[j], srchost);
         //printf("%s\n", srchost);
         inet_aton(srchost, &src_addr.sin_addr);
        
         /* Fill the Ethernet, IP and UDP header for  Heartbeat */
         packet_size = build_udp_packet(src_addr, dst_addr[j], hb_frame[j][i]+34, heartbeat_data_size);
         packet_size = build_ip_packet(src_addr.sin_addr, dst_addr[j].sin_addr, IPPROTO_UDP, hb_frame[j][i]+14,  packet_size);
         hb_packet_size = build_ethernet_frame(&raw_sock[j], hb_frame[j][i], packet_size, &socket_address[j], if_idx[j], if_mac[j], dstmac_hex[j]);
         
         /* Fill the Ethernet, IP and UDP header for probe */
         packet_size = build_udp_packet(src_addr, dst_addr[j], pb_frame[j][i]+34,  rtt_probe_data_size);
         packet_size = build_ip_packet(src_addr.sin_addr, dst_addr[j].sin_addr, IPPROTO_UDP, pb_frame[j][i]+14,  packet_size);
         pb_packet_size = build_ethernet_frame(&raw_sock[j], pb_frame[j][i],  packet_size, &socket_address[j], if_idx[j], if_mac[j], dstmac_hex[j]);
         
         /* Fill the Ethernet, IP and UDP header for  Handsahke */
         packet_size = build_udp_packet(src_addr, dst_addr[j], hs_frame[j][i]+34,  handshake_data_size);
         packet_size = build_ip_packet(src_addr.sin_addr, dst_addr[j].sin_addr, IPPROTO_UDP, hs_frame[j][i]+14,  packet_size);
         hs_packet_size = build_ethernet_frame(&raw_sock[j], hs_frame[j][i], packet_size, &socket_address[j], if_idx[j], if_mac[j], dstmac_hex[j]);
        
         /* Fill the Ethernet, IP and UDP header for HeartbeatAck */
         packet_size = build_udp_packet(src_addr, dst_addr[j], hback_frame[j][i]+34, heartbeatack_data_size);
         packet_size = build_ip_packet(src_addr.sin_addr, dst_addr[j].sin_addr, IPPROTO_UDP, hback_frame[j][i]+14,  packet_size);
         hback_packet_size = build_ethernet_frame(&raw_sock[j], hback_frame[j][i], packet_size, &socket_address[j], if_idx[j], if_mac[j], dstmac_hex[j]);

        }
    }

    //fclose(fp_ip);
    /* Print the hexdump 
    for (int i=0; i<NO_OF_NODES; i++) {
        hexdump(frame[i], packet_size);
    } 
    */

    for (int i=0; i<input.no_of_ifaces; i++) {
	fclose(fp_emu_ips[i]);
	fclose(fp_emu_clids[i]);
    }

    //fclose(fp_clid);
    int NO_OF_BUNCHES=1;
    //int NO_OF_PATHS_PER_BUNCH=NO_OF_PATHS/NO_OF_BUNCHES;
    int NO_OF_PATHS_PER_BUNCH=(unsigned int)input.no_of_paths/NO_OF_BUNCHES;
    NO_OF_PATHS_PER_BUNCH=NO_OF_PATHS_PER_BUNCH + ((unsigned int)input.no_of_paths%NO_OF_BUNCHES);

    pthread_t spokes_bunch[IFACES][NO_OF_BUNCHES];
    struct spoke_set spoke_sets[IFACES][NO_OF_BUNCHES];
    int thr_status;

    printf("NO_OF_BUNCHES %d\n", NO_OF_BUNCHES);
    //printf("input.no_of_paths %d\n", input.no_of_paths);
    for (int j=0; j<input.no_of_ifaces; j++) {
        for (int i=0; i<NO_OF_BUNCHES; i++) {
//    printf("hi..\n");
    //printf("input.no_of_paths %d\n", input.no_of_paths);
            int bunch_idx = i;    
            //bunch_idx=0; // Remove this line when actual bunches are there
	    spoke_sets[j][i].raw_sock_idx = j;

            spoke_sets[j][i].no_of_paths_per_bunch = (unsigned int)input.no_of_paths;
            spoke_sets[j][i].socket_address = &socket_address[j]; 
        
            spoke_sets[j][i].hb_packet_size = LNP_HB_PKT_SIZE;
            spoke_sets[j][i].hs_packet_size = LNP_HS_PKT_SIZE;
            spoke_sets[j][i].pb_packet_size = LNP_RTT_PKT_SIZE;
            spoke_sets[j][i].hback_packet_size = LNP_HBACK_PKT_SIZE;
        
            spoke_sets[j][i].hb_frame = &hb_frame[j][bunch_idx][0];
            spoke_sets[j][i].hback_frame = &hback_frame[j][bunch_idx][0];
            spoke_sets[j][i].hs_frame = &hs_frame[j][bunch_idx][0];
            spoke_sets[j][i].pb_frame = &pb_frame[j][bunch_idx][0]; 
            /*
	    */
        
        }
    }
    //wait_here();

    for (int j=0; j<input.no_of_ifaces; j++) {
        for (int i=0; i<NO_OF_BUNCHES; i++) {
            int bunch_idx = i;    
        
            thr_status = pthread_create(&spokes_bunch[j][i], NULL, start_spoke_bunch, &spoke_sets[j][i]);
        }
    }

    /*
    for (int i = 0; i< 1; i++) {
	    uint8_t *hb_test = hb_frame[i];
	    for (int j=0; j<LNP_HB_PKT_SIZE; j++) {
		    printf("%0x ", (*hb_test++));
	    }
    } */

    /* Wait for thread to proceed continously */
    while (1) {
    }

    /* Handle Signal interupts */

    return 0;
}

