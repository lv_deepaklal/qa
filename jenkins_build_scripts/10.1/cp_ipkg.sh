#!/bin/bash

set -e

RELVER=$1
RELDIR=$2

CP_DIR=${RELDIR}/'CloudPort'${RELVER}
CP_ARCHIVE_NAME=${RELDIR}/CloudPort-${RELVER}.tgz

abort()
{
    echo -e "\nAborting CP Package Build!!\n"
    exit 1
}

if [ X$RELVER = X"" ]; then
    echo -e "\nRelease version is requried\n"
    abort
fi

exit_if_file_not_found()
{
    fname=$1
    if [ ! -f "$fname" ]; then
        echo -e "\nFile $fname not found!\n"
        abort
    fi
}

build_tar_gpg_archive()
{
    echo "Creating CloudPort GPG tar package"
    cd $HOME
    python create_cloudport_package.py ~/packaging.pub ~/packaging.priv ${CP_DIR}
    echo "Created CloudPort GPG tar package successfully"
}

compute_checksum()
{
    echo "Computing Checksums"
    cd $RELDIR
    md5sum * 2>/dev/null > $HOME/MD5SUMS
    sha256sum * 2>/dev/null > $HOME/SHA256SUMS

    sync
    cp $HOME/MD5SUMS $RELDIR
    cp $HOME/SHA256SUMS $RELDIR
}

set -e
build_tar_gpg_archive

set +e
compute_checksum
sync

echo "Deleting directory $CP_DIR"
rm -rf $CP_DIR
sync

