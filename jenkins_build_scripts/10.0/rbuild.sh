#!/bin/bash

RELVER=`echo $1 | cut -d "v" -f 2`

MAJR=`echo $RELVER | cut -d '.' -f 1`
MINR=`echo $RELVER | cut -d '.' -f 2`
PTCH=`echo $RELVER | cut -d '.' -f 2`

BASVER=${MAJR}.${MINR}.0
BASE_RELVER=${MAJR}.${MINR}
export GOHOME=/var/lib/jenkins
export GOROOT=$GOHOME/golang/go
export GOPATH=$GOHOME/go
export PATH=$PATH:$GOROOT/bin

PKG_HOME=$HOME
BUILD_HOME='/var/lib/jenkins'
WORKSPC_HOME=$BUILD_HOME'/workspace'
RELDIR=$PKG_HOME'/v'$RELVER
CP_DIR=$PKG_HOME'/v'$RELVER'/CloudPort'$RELVER
SRCDIR=$WORKSPC_HOME'/'$JOB_NAME
BLDCMD=$SRCDIR'/scripts/release-build.sh'
PKGDIR=$SRCDIR'/build/package/'
BASE_RELVER_DIR=$PKG_HOME'/userContent/releases/v'$BASE_RELVER
RELVER_DIR=${BASE_RELVER_DIR}'/'${RELVER}

FOSSDIR=$WORKSPC_HOME'/foss/everest'

abort()
{
    echo -e "\nAborting Package Build!!\n"
    exit 1
}

if [ X$RELVER = X"" ]; then
	echo -e "\nRelease version is requried.\n"
	abort
fi

exit_if_file_not_found()
{
    fname=$1
    if [ ! -f "$fname" ]; then
        echo -e "\nFile $fname not found!\n"
        abort
    fi
}

foss_repo_files=( "${FOSSDIR}/src/lnpkg/cloudport-dependencies_${BASVER}.tgz"
                  "${FOSSDIR}/src/statsbeat/statsbeat_5.4.2-1.lnpkg"
                  "${FOSSDIR}/src/filebeat/filebeat_5.1.1.lnpkg" )

repo_file_check()
{
    for i in "${foss_repo_files[@]}"
    do
        exit_if_file_not_found $i
    done
}

repo_file_copy()
{
    for i in "${foss_repo_files[@]}"
    do
        cp $i $CP_DIR
	sleep 2
    done
}

repo_file_check

# make target release directory where the images will be copied
build_and_copy_images ()
{
    mkdir -p $RELDIR
    rm -rf $RELDIR/*
    mkdir -p $CP_DIR
    cd $SRCDIR
    mkdir -p $RELVER_DIR

    $BLDCMD -p --prod
    cp $PKGDIR/cloud-port_$RELVER'_R_'* $CP_DIR
    sleep 2

    $BLDCMD -lb --prod
    cp $PKGDIR/lavelle-linux-bintools_$RELVER'_R_'* $CP_DIR
    sleep 2

    $BLDCMD -es --prod
    cp $PKGDIR/cloud-edge-station_$RELVER'R'* $RELDIR
    sleep 2

    $BLDCMD -st --prod
    cp $PKGDIR/cloud-stats_$RELVER'R'* $RELDIR
    sleep 2

    $BLDCMD -n --prod
    cp $PKGDIR/cloud-notifications_$RELVER'R'* $RELDIR
    sleep 2

    $BLDCMD -lps --prod
    cp $PKGDIR/cloud-lp-server_$RELVER'R'* $RELDIR
    sleep 2

    # Copy FOSS repo images
    repo_file_copy

    #cp $FOSSDIR/src/urlf-db/site-categories-v2.tgz ${RELDIR}/site-categories.tgz
    sleep 2
    sync
}

build_and_copy_images

/home/lavelle/cp_rpkg.sh $RELVER $RELDIR 

echo "Copying contents of ${RELDIR} to ${RELVER_DIR}"
cp -R ${RELDIR}/* ${RELVER_DIR}
sync

echo "Deleting directory ${RELDIR}"
rm -rf ${RELDIR}
sync

cd ${BASE_RELVER_DIR}
rm latest
ln -s ${RELVER}/ latest

