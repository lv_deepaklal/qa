#!/usr/bin/env python2


import os
import sys
import copy
import json
import time
import subprocess


TMP_DIR = "/var/tmp/{0}"
UPGRADE_CONF = "upgrade_conf.json"
#CLOUDPORT_PACKAGE_TAR = "CloudPort-{0}.tgz"
CLOUDPORT_PACKAGE_TAR = "CloudPort-{0}-R.tgz"
CLOUDPORT_PACKAGE_GPG = "{0}.gpg".format(CLOUDPORT_PACKAGE_TAR)
PACKAGE_VERSION = ""
WANTED_PACKAGES = ("cloud-port", "lavelle-linux-bintools", "filebeat",
                   "statsbeat")
PACKAGE_TYPE_MAPPING = {
    "cloud-port": "service",
    "lavelle-linux-bintools": "system",
    "filebeat": "third-party",
    "statsbeat": "third-party"
}
PACKAGE_DESCRIPTION_MAPPING = {
    "cloud-port": "CloudPort Service Package",
    "lavelle-linux-bintools": "CloudPort System Package",
    "filebeat": "Logs Exporter Package",
    "statsbeat": "Stats Exporter Package"
}
DEPENDENCY_PACKAGE_MAPPING = {
    "cloud-port": "cloudport-dependencies"
}
PACKAGE_STRUCTURE = {
    "archive-name": "",
    "checksum": "",
    "package-type": "",
    "version": "",
    "dependency-package": "",
    "dependency-package-checksum": "",
    "package-description": ""
}


def execute(command):
    output = None
    try:
        output = subprocess.check_output(command, shell=True,
                                         stderr=subprocess.STDOUT,
                                         executable="/bin/bash",
                                         close_fds=True)
    except subprocess.CalledProcessError as exception:
        print("Command returned a non-zero exit status.\nCommand ::: {0}"
              "\nOutput ::: {1}".format(command, exception.output))
        return False
    except Exception as exception:
        print("Exception Occurred ::: {0}".format(exception))
        return False
    return output if output else True


def _valid_arguments(args):
    return all([os.path.exists(arg) for arg in args])


def _get_checksum(directory, file):
    path = os.path.join(directory, file)
    cksum_command = "cksum '{0}'".format(path) + " | awk '{print $1}'"
    return execute(cksum_command).strip()


def _get_dependency_package(package_directory, package_name):
    dependency_package_start = DEPENDENCY_PACKAGE_MAPPING.get(package_name)
    if not dependency_package_start:
        return ""
    dependency_packages = [package for package in os.listdir(package_directory)
                           if package.startswith(dependency_package_start)]
    if dependency_packages:
        return dependency_packages[0]
    return ""


def _generate_upgrade_configuration(package_directory):
    global PACKAGE_VERSION
    packages = sorted(
        [package for package in os.listdir(package_directory)
        if package.lower().startswith(WANTED_PACKAGES)])
    upgrade_conf = {
        "package-list": [],
        "packages": {}
    }
    for package in packages:
        package_name = package.replace(".lnpkg", "")
        key = package_name.split("_")[0]
        package_dict = copy.deepcopy(PACKAGE_STRUCTURE)
        version_fields = package_name.split("_")[1:]
        if key == "cloud-port":
            PACKAGE_VERSION = version_fields[0]
        package_dict['archive-name'] = package
        package_dict['package-type'] = PACKAGE_TYPE_MAPPING[key]
        package_dict['version'] = "_".join(version_fields)
        package_dict['package-description'] = PACKAGE_DESCRIPTION_MAPPING[key]
        package_checksum = _get_checksum(package_directory, package)
        if not package_checksum:
            print("Checksum of {0} could not be calculated".format(package))
            sys.exit(1)
        package_dict['checksum'] = str(package_checksum)
        dependency_package = _get_dependency_package(package_directory, key)
        if dependency_package:
            package_dict['dependency-package'] = dependency_package
            dependency_package_checksum = \
                _get_checksum(package_directory, dependency_package)
            if not dependency_package_checksum:
                print("Checksum of {0} could not be calculated"
                      .format(dependency_package))
                sys.exit(1)
            package_dict['dependency-package-checksum'] = \
                str(dependency_package_checksum)
        upgrade_conf['package-list'].append(key)
        upgrade_conf['packages'][key] = package_dict
    with open(os.path.join(package_directory, UPGRADE_CONF),
              "w") as upgrade_conf_file:
        json.dump(upgrade_conf, upgrade_conf_file, indent=4, sort_keys=True)
    parent_dir = os.path.normpath(os.path.join(package_directory, os.pardir))
    return os.path.join(parent_dir,
                        CLOUDPORT_PACKAGE_TAR.format(PACKAGE_VERSION))


def _create_tar(package_directory, package_tar_path):
    tar_command = "cd '{0}' && tar -czf '{1}' * && cd -".format(
        package_directory, package_tar_path)
    tar_result = execute(tar_command)
    if tar_result:
        parent_dir = os.path.normpath(os.path.join(package_directory,
                                                   os.pardir))
        return os.path.join(parent_dir,
                            CLOUDPORT_PACKAGE_GPG.format(PACKAGE_VERSION))
    return ""


def _encrypt_tar_gpg(package_tar_path, package_gpg_path, public_key_file,
                     private_key_file):
    gpg_homedir = TMP_DIR.format(int(time.time()))
    gpg_encrypt_command = \
        "mkdir -p '{0}' && gpg --homedir '{0}' --yes --import '{1}' && "\
        "gpg --homedir '{0}' --yes --import '{2}' && gpg --homedir '{0}' " \
        " --yes --encrypt --sign --recipient 'Lavelle Packaging' " \
        "--trust-model 'always' --output '{3}' '{4}' && rm -rf '{0}'".format(
            gpg_homedir, public_key_file, private_key_file, package_gpg_path,
            package_tar_path)
    return execute(gpg_encrypt_command)


def main():
    if len(sys.argv) != 4 or not _valid_arguments(sys.argv[1:]):
        print("Invalid Arguments")
        print("Usage:\n\tpython create_cloudport_package.py <PUBLIC_KEY> "
              "<PRIVATE_KEY> <PACKAGE_DIRECTORY>")
        sys.exit(1)

    public_key_file = sys.argv[1]
    private_key_file = sys.argv[2]
    package_directory = sys.argv[3]

    package_tar_path = _generate_upgrade_configuration(package_directory)
    if not package_tar_path:
        print("Failed to generate upgrade configuration")
        return
    package_gpg_path = _create_tar(package_directory, package_tar_path)
    if not package_gpg_path:
        print("Failed to package tar")
        return
    gpg_encryption_result = _encrypt_tar_gpg(package_tar_path,
                                             package_gpg_path, public_key_file,
                                             private_key_file)
    if not gpg_encryption_result:
        print("Failed to encrypt tar")
        return
    print("Encrypted Successfully : {0}".format(package_gpg_path))


if __name__ == "__main__":
    main()
