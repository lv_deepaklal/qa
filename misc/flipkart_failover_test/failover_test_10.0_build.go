package main

import (
            "fmt"
            "time"
            "strings"
            "io/ioutil"
            "encoding/json"
            "os"
            "log"
            "os/exec"
            "bytes"
	    "strconv"
	    scp "github.com/bramvdbogaerde/go-scp"
	    "github.com/bramvdbogaerde/go-scp/auth"
	    "golang.org/x/crypto/ssh"
       )

type TestInputs struct {
    Url string `json:"cs_url"`
    SshPort int `json:"cs_ssh_port"`
    SshKeyPath string `json:"ssh_key_path"`
    Ip string `json:"ip_to_ping"`
    Debug bool `json:"debug"`
}

type sysconfInf struct {
    Name        string  `json:"name"`
    Proto       string  `json:"proto"`
    Swport      string  `json:"swport"`
    Role        string  `json:"role"`
    IsMpls      string  `json:"mpls"`
    IsMgmt      string  `json:"mgmt-only"`
    VlanId      string  `json:"vid"`
    IsPvid      string  `json:"is_pvid"`
    wanType     string
}

type sysconfSwitch struct {
    Name        string `json:"name"`
    Ports       []string `json:"ports"`
}

type bondPort struct {
    Name        string   `json:"name"`
    MemberPorts []string `json:"memports"`
}

type sysinfo struct {
    Cs              struct {
            Url     string `json:"url"`
    }`json:"cs"`
    Uuid            string          `json:"uuid"`
    Infs            []sysconfInf    `json:"interfaces"`
    Switches        []sysconfSwitch `json:"switches"`
    Bondports       []bondPort      `json:"bondports"`
    WebuiUrl        string          `json:"webui_url"`
    SpeedtestUrl    string          `json:"speedtest_url"`
    InsightsUrl     string          `json:"insights_url"`
    infToSwportMap  map[string]string
    swportToInfsMap map[string][]string
    swportToBondPortMap map[string] * bondPort
    infMap          map[string] * sysconfInf
    infNames        []string
    swportNames     []string
    bondPortNames   []string
    lanInfs         []string
    wanInfs         []string
    bbWanInfs       []string
    mplsWanInfs     []string
    allInfs         []string
    rtTables        []string
    nsNatFwInfs     []string
    nsPsuLanInfs    []string
}

type deviceinfo struct {
    Lavelle_code    string    `json:"LAVELLE_CODE"`
    Manufac_code    string    `json:"MANUFACTURER_CODE"`
    Manufac_sku     string    `json:"MANUFACTURER_SKU"`
    Manufac_year    string    `json:"MANUFACTURE_YEAR"`
    Manufac_week    string    `json:"MANUFACTURE_WEEK"`
    Instance_type   string    `json:"INSTANCE_TYPE"`
    Serial_no       string    `json:"SERIAL_NUMBER"`
}

type deviceInfoInt struct {
    Lavelle_code    int    `json:"LAVELLE_CODE"`
    Manufac_code    int    `json:"MANUFACTURER_CODE"`
    Manufac_sku     int    `json:"MANUFACTURER_SKU"`
    Manufac_year    int    `json:"MANUFACTURE_YEAR"`
    Manufac_week    int    `json:"MANUFACTURE_WEEK"`
    Instance_type   int    `json:"INSTANCE_TYPE"`
    Serial_no       int    `json:"SERIAL_NUMBER"`
}

const   (
            SYSCONF_FILE        = "/factory/sysconf.json"
            DEVICE_INFO_FILE    = "/factory/device_info.json"
            NS_NATFW_NAME       = "ns-svc-natfw"
            NS_PSEUDO_LAN_NAME  = "ns-svc-lan"
        )

var (
        sysInfo         sysinfo
        DeviceInfo      deviceinfo
        DeviceInfoInt   deviceInfoInt
        PingOutput      []byte
        Ppelog          []byte
        Syslog          []byte
	err             error

        logger *log.Logger
	cs_url string
	cs_ssh_port int = 22
	cs_ssh_key_path string = ""
	ip_to_ping string  = ""
	Debug bool = false
    )

func getCommandOutput (cmd string, fstr ...string) ([]string, error) {
    res := []string{}
    for i, _ := range fstr {
        cmd += " | egrep --color " + fstr[i]
    }
    if cmd != "bash" {
        cmd += " 2>/dev/null"
    }
    //fmt.Printf("[DEBUG]: rCO : [%s]\n", cmd)
    output, err := exec.Command("bash","-c", cmd).Output()
    if err != nil {
        return nil, err
    }
    bb := bytes.NewBuffer(output)
    s, err := bb.ReadString(byte('\n'))
    for err == nil {
        res = append(res, s)
        s, err = bb.ReadString(byte('\n'))
    }
    return res, nil
}


func fileExists(path string) (bool, error) {
    _, err := os.Stat(path)
    if err == nil { return true, nil }
    if os.IsNotExist(err) { return false, nil }
    return true, err
}

func readSysFile (fname string) string {
    bb, err := ioutil.ReadFile(fname)
    if err == nil {
        return strings.Trim(string(bb), " \n")
    }
    logger.Printf("failed to read file %s : %s\n", fname, err.Error())
    return "error-occured"
}

func getInfIPaddr (inf string) string {
    cmd := "ip addr show dev " + inf + " |grep 'inet ' |awk '{print $2}'"
    o, e := getCommandOutput(cmd)
    if e == nil {
        for i, _ := range o {
            o[i] = strings.Trim(o[i], " \n")
        }
        return strings.Join(o, ", ")
    }
    return "error-occurred"
}

func isPortLinkUp (swport string) bool {
    if "up" == readSysFile("/sys/class/net/"+swport+"/operstate") {
        return true
    }
    return false
}

/*
func isInfLinkUp (ifname string) bool {
    sysi := &sysInfo
    if _, ok := sysi.infMap[ifname]; ok {
        return "up" == readSysFile("/sys/class/net/"+ifname+"/operstate") && isPortLinkUp(sysi.infToSwportMap[ifname])
    }
    return false
}
*/

func getDHCPLinks(ifname []string) []string{
    sysi := &sysInfo
    dhcpLinks := make([]string, 0, 6)
    for _, link := range ifname {
        if "dhcp" == sysi.infMap[link].Proto {
            dhcpLinks = append(dhcpLinks, link)
        }
    }
    return dhcpLinks
}

func isInfLinkUp (ifname string) bool {
    sysi := &sysInfo
    if _, ok := sysi.infMap[ifname]; ok {
        if "up" == readSysFile("/sys/class/net/"+ifname+"/operstate") && isPortLinkUp(sysi.infToSwportMap[ifname]) {
            output, _ := getCommandOutput("/home/lavelle/appctl path/show | grep " + ifname + " | grep 0x0000 | grep -v '8.8.8.8' | wc -l")
	    if output2, err := strconv.Atoi( strings.TrimSuffix(output[0], "\n")); err == nil && output2 > 0 {
	        return true
	    }
	    return false
        }
        return false
    }
    return false
}

func getActiveLink() []string{
    sysi := &sysInfo
    var activeLink []string
    for _, wanInf := range sysi.wanInfs {
        if isInfLinkUp(wanInf) {
            activeLink = append(activeLink, wanInf)
        }
    }
    return activeLink
}

func devInfo () error {
    data, err := ioutil.ReadFile(DEVICE_INFO_FILE)
    if err != nil {
        fmt.Println(err)
        return err
    }
    devinfo := &DeviceInfo
    if err = json.Unmarshal(data, devinfo); err != nil {
        log.Println(err)
        devinfoint := &DeviceInfoInt
        if err = json.Unmarshal(data, devinfoint); err == nil {
            devinfo.Lavelle_code    = strconv.Itoa(devinfoint.Lavelle_code)
            devinfo.Manufac_code    = strconv.Itoa(devinfoint.Manufac_code)
            devinfo.Manufac_sku     = strconv.Itoa(devinfoint.Manufac_sku)
            devinfo.Manufac_year    = strconv.Itoa(devinfoint.Manufac_year)
            devinfo.Manufac_week    = strconv.Itoa(devinfoint.Manufac_week)
            devinfo.Instance_type   = strconv.Itoa(devinfoint.Instance_type)
            devinfo.Serial_no       = strconv.Itoa(devinfoint.Serial_no)
        } else {
            log.Println(err)
            return err
        }
    }
    fmt.Println(data, devinfo)
    return nil
}

func initSysInfo () error {
    data, err := ioutil.ReadFile(SYSCONF_FILE)
    if err != nil {
        return err
    }
    sysi := &sysInfo
    if err = json.Unmarshal(data, sysi); err != nil {
        return err
    }
    /*
     * create three mappings
     * - a map of inf-name to @sysconfInf struct
     * - a map of inf-name to swport-name
     * - a map of swport-name to inf-name(s)
     */
    sysi.infMap = make(map[string] * sysconfInf)
    sysi.infToSwportMap = make(map[string]string)
    sysi.swportToInfsMap = make(map[string][]string)
    sysi.swportToBondPortMap = make(map[string] * bondPort)
    for i , _ := range sysi.Infs {
        inf := &sysi.Infs[i]
        sysi.infMap[inf.Name] = inf
        sysi.infToSwportMap[inf.Name] = inf.Swport
        sysi.swportToInfsMap[inf.Swport] =
                    append(sysi.swportToInfsMap[inf.Swport], inf.Name)
        if inf.VlanId == "" {
            inf.VlanId = "n/a"
        }
        if inf.IsPvid != "" && strings.EqualFold(inf.IsPvid, "yes") {
            inf.VlanId = "n/a"
        }
        if inf.Role == "lan" {
            sysi.lanInfs = append(sysi.lanInfs, inf.Name)
        } else {
            if inf.IsMgmt == "" {
                inf.IsMgmt = "no"
            }
            if inf.IsMpls == "yes" {
                inf.wanType = "MPLS"
                sysi.mplsWanInfs = append(sysi.mplsWanInfs, inf.Name)
            } else {
                inf.wanType = "BroadBand"
                sysi.bbWanInfs = append(sysi.bbWanInfs, inf.Name)
            }
        }
    }
    sysi.wanInfs = append(sysi.bbWanInfs, sysi.mplsWanInfs...)
    sysi.allInfs = append(sysi.wanInfs, sysi.lanInfs...)
    for k, _ := range sysi.infToSwportMap {
        sysi.infNames = append(sysi.infNames, k)
    }
    for i, _ := range sysi.Switches {
        for _, p := range sysi.Switches[i].Ports {
            sysi.swportNames = append(sysi.swportNames, p)
        }
    }
    /* add support for bondports */
    for i, _ := range sysi.Bondports {
        sysi.bondPortNames = append(sysi.bondPortNames, sysi.Bondports[i].Name)
        for _, p := range sysi.Bondports[i].MemberPorts {
            sysi.swportToBondPortMap[p] = &sysi.Bondports[i]
        }
    }

    /* initialize route table names */
    for i, _ := range sysi.wanInfs {
        sysi.rtTables = append(sysi.rtTables, "rt_"+sysi.wanInfs[i])
    }
    sysi.rtTables = append(sysi.rtTables, "main")

    /* create the directory if doesn't exist */
    //os.MkdirAll(FileServerLocation, 0755)

    /* psuedo LAN interfaces */
    for _, ns := range []string{NS_PSEUDO_LAN_NAME, NS_NATFW_NAME} {
        cmd := "ip netns exec " + ns +
               " cat /proc/net/dev | grep \":\" | grep -v \"lo:\" |"+
               " awk -F: '{print $1}'"
        o, e := getCommandOutput(cmd)
        if e == nil {
            for i, _ := range o {
                switch ns {
                    case NS_PSEUDO_LAN_NAME:
                        sysi.nsPsuLanInfs = append(sysi.nsPsuLanInfs,
                                                   strings.Trim(o[i], " \n"))
                    case NS_NATFW_NAME:
                        sysi.nsNatFwInfs = append(sysi.nsNatFwInfs,
                                                   strings.Trim(o[i], " \n"))
                }
            }
        } else {
            logger.Printf("failed to get interfaces of %s\n", ns)
            return fmt.Errorf("failed to get interfaces of %s\n", ns)
        }
    }

    return nil
}

func startPPELog() {
    cmd := "tail -f /var/log/upstart/lavelle-ppe.log > /home/lavelle/ppe.log"
    Ppelog, err = exec.Command("bash", "-c", cmd).Output()
    if err != nil {
        fmt.Println(err)
        return
    }
}

func startSyslog() {
    cmd := "tail -f /var/log/syslog > /home/lavelle/syslog.log"
    Syslog, err = exec.Command("bash", "-c", cmd).Output()
    if err != nil {
        fmt.Println(err)
        return
    }
}

func initiatePing (count int, ip_addr string) {
    cmd := "ip netns exec ns-svc-lan /bin/ping -s 1000 -c " + strconv.Itoa(count) + " " + ip_addr + " > /home/lavelle/ping_content.log 2>&1"
    fmt.Println(cmd)
    PingOutput, err = exec.Command("bash", "-c", cmd).Output()
    if err != nil {
        fmt.Println(err)
        return
    }
}


func copyFilesToCs(files []string) {

	username := "lavelle"
	password := "lavelle@3163"
	var clientConfig ssh.ClientConfig
	if cs_ssh_key_path == "" {
	    clientConfig, _ = auth.PasswordKey(username, password, ssh.InsecureIgnoreHostKey())
	} else {
            clientConfig, _ = auth.PrivateKey(username, cs_ssh_key_path, ssh.InsecureIgnoreHostKey())
	}


	socket := fmt.Sprintf("%s:%d", cs_url, cs_ssh_port)
	client := scp.NewClient(socket, &clientConfig)

	err := client.Connect()
	if err != nil {
            logger.Println("Couldn't establish a connection to the remote server ", err)
            return
	}
	defer client.Close()

	for _, filename := range files {
	   f, _ := os.Open(filename)
	   defer f.Close()
	   err = client.CopyFile(f, "/home/lavelle/failover/"+filename, "0666")
	   if err != nil {
	          logger.Println("Error while copying file ", err)
	   }
	}

}


func initInputs() {
  jsonFile, err := os.Open("failovertest.json")
  if err != nil {
     logger.Println(err)
  }
  defer jsonFile.Close()

  byteValue, _ := ioutil.ReadAll(jsonFile)

  // we initialize our Users array
  var testinputs TestInputs

  // we unmarshal our byteArray which contains our
  // jsonFile's content into 'users' which we defined above
  if err = json.Unmarshal(byteValue, &testinputs); err != nil {
	  panic(err)
  }


  cs_url = strings.TrimSpace(testinputs.Url)
  cs_ssh_port = testinputs.SshPort
  cs_ssh_key_path = strings.TrimSpace(testinputs.SshKeyPath)
  ip_to_ping = strings.TrimSpace(testinputs.Ip)
  Debug = testinputs.Debug

  if cs_url == "" || cs_ssh_port  < 0 || cs_ssh_port > 65535 || ip_to_ping == "" {
	  logger.Println("Invalid inputs")
	  os.Exit(1)
  }

}


func main() {

    devInfo()
    t1 := time.Now()
    devinfo := &DeviceInfo

    fmt.Println(devinfo)
    //serial_no := strconv.Itoa(devinfo.Serial_no)
    file, err := os.OpenFile(devinfo.Serial_no + "_debug_" + fmt.Sprintf("%s.log", t1.Format("20060102_150405")), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
    if err != nil {
        log.Fatal(err)
    }
    logger = log.New(file, "DEBUG: ", log.Ldate|log.Ltime|log.Lshortfile)
    initInputs()
    initSysInfo()
    activeLinks := getActiveLink()
    dhcpLinks := getDHCPLinks(activeLinks)
    logger.Println(activeLinks)
    logger.Println(dhcpLinks)
    //ppelog, syslog, pingoutput := "", "", ""
    if len(activeLinks) >=2 {
        go startPPELog()
	time.Sleep(1 * time.Second) // just for safety
        go startSyslog()
	time.Sleep(1 * time.Second) // just for safety
        go initiatePing(60, ip_to_ping)
        t1 = time.Now()
        time.Sleep(5 * time.Second)
        for _, activeLink := range activeLinks {
            cmd := "ifconfig " + activeLink + " down"
            getCommandOutput(cmd)
            time.Sleep(10 * time.Second)
            cmd = "ifconfig " + activeLink + " up"
            getCommandOutput(cmd)
            time.Sleep(10 * time.Second)
        }

	for _, link := range dhcpLinks {
            cmd = "dhclient " + link
            getCommandOutput(cmd)
        }

        diff := time.Now().Sub(t1)
        if int64(diff.Seconds()) < 72 {
            time.Sleep(time.Duration(72 - int64(diff.Seconds())) * time.Second)
        }
    } else {
        getCommandOutput("echo 'Device only has single interface' > /home/lavelle/ping_content.log")
        logger.Println("Device only has single interface")
    }
    Ppelog, err := ioutil.ReadFile("/home/lavelle/ppe.log")
    if err == nil {
        ppelog := strings.Trim(string(Ppelog), " \n")
        logger.Println(ppelog)
    } else {
        logger.Println(err)
    }
    Syslog, err := ioutil.ReadFile("/home/lavelle/syslog.log")
    if err == nil {
        syslog := strings.Trim(string(Syslog), " \n")
        logger.Println(syslog)
    }

    PingOutput, err := ioutil.ReadFile("/home/lavelle/ping_content.log")
    if err == nil {
        pingoutput := strings.Trim(string(PingOutput), " \n")
        fmt.Println(pingoutput)
        logger.Println(pingoutput)
    }

    files_to_copy := []string{}
    PingFilename := devinfo.Serial_no + "_ping_" + fmt.Sprintf("%s.log", t1.Format("20060102_150405"))
    PpeLogFilename := devinfo.Serial_no + "_ppe_" + fmt.Sprintf("%s.log", t1.Format("20060102_150405"))
    SyslogFilename := devinfo.Serial_no + "_syslog_" + fmt.Sprintf("%s.log", t1.Format("20060102_150405"))
    scp_file := devinfo.Serial_no + "_failover_test_" + fmt.Sprintf("%s.log", t1.Format("20060102_150405"))

    fileObj, err := os.OpenFile(scp_file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
    if err != nil {
        log.Fatal(err)
    }

    heading := []byte("--------------------------------------------------------------------------------------\n")
    if err = ioutil.WriteFile(PingFilename, PingOutput, 0666); err == nil {
        if _, err = fileObj.Write(heading); err != nil {
            logger.Println(err)
        } else {
            _, err = fileObj.Write([]byte("Ping Output\n"))
            _, err = fileObj.Write(heading)
            _, err = fileObj.Write(PingOutput)
            _, err = fileObj.Write(heading)
        }
        files_to_copy = append(files_to_copy, scp_file)
        files_to_copy = append(files_to_copy, PingFilename)
    } else {
        logger.Println("Error writing Ping file")
    }

    if err = ioutil.WriteFile(PpeLogFilename, Ppelog, 0666); err == nil {
        if _, err = fileObj.Write(heading); err != nil {
            logger.Println(err)
        } else {
            _, err = fileObj.Write([]byte("PPE Log Output\n"))
            _, err = fileObj.Write(heading)
            _, err = fileObj.Write(Ppelog)
            _, err = fileObj.Write(heading)
        }
        //files_to_copy = append(files_to_copy, PpeLogFilename)
    } else {
        logger.Println("Error writing ppelog file")
    }

    if err = ioutil.WriteFile(SyslogFilename, Syslog, 0666); err  == nil {
        if _, err = fileObj.Write(heading); err != nil {
            logger.Println(err)
        } else {
            _, err = fileObj.Write([]byte("SysLog Output\n"))
            _, err = fileObj.Write(heading)
            _, err = fileObj.Write(Syslog)
            _, err = fileObj.Write(heading)
        }
        //files_to_copy = append(files_to_copy, SyslogFilename)
    } else {
        logger.Println("Error writing syslog file")
    }

    fileObj.Close()
    if len(files_to_copy) == 0 {
        logger.Println("No files to copy to CS")
    } else {
        copyFilesToCs(files_to_copy)
    }
}
