package lvnlog

import (
	"fmt"
	"os"
	"io"
	"log"
	os_user "os/user"
	//"time"
	//"errors"
	//"runtime"
	//"strings"
	utils "utils"
       )



func CheckFileExists(filename string) (bool) {

    if _, err := os.Stat(filename); os.IsNotExist(err) {
        return false
    } else {
        return true
    }
}

func getLogPath() (logpath string) {
    home_dir, _ := os_user.Current()
    logpath  = fmt.Sprintf("%s/lvn_auto_logs", home_dir.HomeDir )
    //fmt.Println(logpath)
    //panic("")
    return
}

/*
type lavelleLogger struct {

    debug *log.Logger
    info *log.Logger
    warn *log.Logger
    err  *log.Logger

}
*/

/*
var (
  Debug = Black
  Info  = Black
  Warn  = Yellow
  Err   = Red
)

var (
  Black   = Color("\033[1;30m%s\033[0m")
  Red     = Color("\033[1;31m%s\033[0m")
  Green   = Color("\033[1;32m%s\033[0m")
  Yellow  = Color("\033[1;33m%s\033[0m")
  Purple  = Color("\033[1;34m%s\033[0m")
  Magenta = Color("\033[1;35m%s\033[0m")
  Teal    = Color("\033[1;36m%s\033[0m")
  White   = Color("\033[1;37m%s\033[0m")
)

func Color(colorString string) func(...interface{}) string {
  sprint := func(args ...interface{}) string {
    return fmt.Sprintf(colorString,
      fmt.Sprint(args...))
  }
  return sprint
}
fmt.Println(Info("hello, world!"))
*/

var (
    Debug *log.Logger
    Info *log.Logger
    Warn *log.Logger
    Err  *log.Logger
    Log_date string
    LogFilename string
    LVN_LOGPATH string
)

func get_datetime_log_format() (datetime string){

    //datetime = time.Now().Format("2006-01-02-15-04-05")
    datetime = utils.GetCurrentDateTimeForFile()
    return datetime
}


func createlogfile(filename string) (*os.File, error) {

    return os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)

}

//func New(filename string) (logger lavelleLogger, err error) {
//func New(filename string) (err error) {
//func init() {
//	New()
//}
//func New() (err error) {
func initialise() {
	fmt.Println("..........................")
	New()
}

func New(filenames ...string) (err error) {

    //var err_msg error
    //if filename, err_msg = utils.Get_testcase_name(); err_msg!=nil {
   //	    fmt.Printf("Unabel to get testcase name")
//	 return err_msg
  //  }

  /* enable_debug = false, logs debug info in debug log only
                           debug info will not get logged in normal logfile
                  - true, logs the debug info both in debug log and 
		          normal log
     enable_console = false, log info will not be showed in console
                    = true, log info will be shown in console
     enable_console_debug = true, when enable_debug=true, it will log debug
                            info in console
   */


    enable_debug   := false
    enable_console := true
    enable_console_debug := false
    //disable_log    := false

    var debuglogTargets, infologTargets, warnlogTargets, errlogTargets []io.Writer
    var filename string

    if len(filenames) > 0 {
        filename = filenames[0]
	LogFilename = filenames[0]
        LVN_LOGPATH = getLogPath()

        if !CheckFileExists(LVN_LOGPATH) {
                os.MkdirAll(LVN_LOGPATH, 0777)
        }

        Log_date        = get_datetime_log_format()
        case_log       := fmt.Sprintf("%s/%s_%s.log", LVN_LOGPATH, filename, Log_date)
        case_debug_log := fmt.Sprintf("%s/%s_%s.debug.log", LVN_LOGPATH, filename, Log_date)

        case_log_f, err1 := createlogfile(case_log)
        case_debug_log_f, err2 := createlogfile(case_debug_log)

        if err1 != nil {
                fmt.Printf("Failed to create logfile %s\n", case_log)
        }
        if err2 != nil {
                fmt.Printf("Failed to create logfile %s\n", case_debug_log)
        }

        /*
        debuglogTargets := []*os.File{case_debug_log_f}
        infologTargets  := []*os.File{case_log_f}
        warnlogTargets  := []*os.File{case_log_f}
        errlogTargets   := []*os.File{case_log_f}
*/

        //if disable_log {

        //}

        debuglogTargets = []io.Writer{case_debug_log_f}
        infologTargets  = []io.Writer{case_debug_log_f, case_log_f}
        warnlogTargets  = []io.Writer{case_debug_log_f, case_log_f}
        errlogTargets   = []io.Writer{case_debug_log_f, case_log_f}

        if enable_debug {
            debuglogTargets  = append(debuglogTargets, case_log_f)
            if enable_console_debug {
                debuglogTargets  = append(debuglogTargets, os.Stdout)
            }
        }

    } else {
	    enable_console = true
    }



    if enable_console {
        infologTargets   = append(infologTargets, os.Stdout)
        warnlogTargets   = append(warnlogTargets, os.Stdout)
        errlogTargets    = append(errlogTargets, os.Stdout)
    }


    debuglogWriter  := io.MultiWriter(debuglogTargets...)
    infologWriter   := io.MultiWriter(infologTargets...)
    warnlogWriter   := io.MultiWriter(warnlogTargets...)
    errlogWriter    := io.MultiWriter(errlogTargets...)
    /*
    debuglogWriter  := io.MultiWriter(case_debug_log_f, os.Stdout, case_log_f)
    infologWriter   := io.MultiWriter(case_log_f,os.Stdout)
    warnlogWriter   := io.MultiWriter(case_log_f,os.Stdout)
    errlogWriter    := io.MultiWriter(case_log_f,os.Stdout)

*/
    //Debug = log.New(debuglogWriter, "[DEBUG] ", log.Ldate | log.Ltime | log.Lshortfile)
    //Info  = log.New(infologWriter,  "[INFO]  ", log.Ldate | log.Ltime | log.Lshortfile)
    //Warn  = log.New(warnlogWriter,  "[WARN]  ", log.Ldate | log.Ltime | log.Lshortfile)
    //Err   = log.New(errlogWriter,   "[ERROR] ", log.Ldate | log.Ltime | log.Lshortfile)

    Debug = log.New(debuglogWriter, "[DEBUG] ", log.Ldate | log.Ltime)
    Info  = log.New(infologWriter,  "[INFO]  ", log.Ldate | log.Ltime)
    Warn  = log.New(warnlogWriter,  "[WARN]  ", log.Ldate | log.Ltime)
    Err   = log.New(errlogWriter,   "[ERROR] ", log.Ldate | log.Ltime)
    //fmt.Println(Debug, Info, Warn, Err)

    /*
    logger = lavelleLogger{debug: Debug,
                            info : Info,
		            warn : Warn,
		            err  : Error}
     */
    //return logger, errors.New("") 
    return
}
