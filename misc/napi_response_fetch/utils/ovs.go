package utils

import (
	"fmt"
	"regexp"
	"strings"
	"strconv"
)

type OVSTable struct {
        cookie   string
        duration float64
        table    int
        nPackets int
        nBytes   int
        idleAge  int
        hardAge  int
        priority int
        flowDir  int
        srcNwgrp int
        dstNwgrp int
        appCat   *string
        appId    *int
        //ip       string
        nwSrc    *string
        nwDst    *string
	srcPort  *int
	dstPort  *int
        actions  string
	proto    *string
	ct_state *int

	Rule     map[string]struct{}
}

func NewOVSTable() *OVSTable {
    o := OVSTable{}
    return &o
}

func (o *OVSTable) SetTable(table int) {
	fmt.Println("set table", o.table)
    o.table = table
    fmt.Println("setted table", o.table)
}

func (o *OVSTable) SetPriority(p int) {
    o.priority = p
}


func (o *OVSTable) SetFlowDir(flow int) {
    o.flowDir = flow
}


func (o *OVSTable) SetSrcNwGrp(nw_grp_id int) {
    o.srcNwgrp = nw_grp_id
}

func (o *OVSTable) SetDstNwGrp(nw_grp_id int) {
    o.dstNwgrp = nw_grp_id
}

func (o *OVSTable) SetAppCat(appcat int) {
    appcat_str := fmt.Sprintf("%d", appcat)
    if appcat == 128 {
        appcat_str = appcat_str+"/0x80"
    }
    o.appCat = &appcat_str
}

func (o *OVSTable) SetAppId(appid int) {
    o.appId = &appid
}

func (o *OVSTable) SetSrcNw(nw string) {
    o.nwSrc = &nw
}

func (o *OVSTable) SetDstNw(nw string) {
    o.nwDst = &nw
}

func (o *OVSTable) SetDstPort(port int) {
    o.dstPort = &port
}

func (o *OVSTable) SetSrcPort(port int) {
    o.srcPort = &port
}

func (o *OVSTable) SetActions(action  string) {
    if strings.ToLower(strings.TrimSpace(action)) == "deny" {
        action = "drop"
    }
    o.actions = action
}

func (o *OVSTable) SetProto(proto string) {
    o.proto = &proto
}

var ct_state_map map[string]int = map[string]int{
    "new" : 1,
}

func (o *OVSTable) SetConnectionState(ct_state string) {

    ct_state_int := ct_state_map[strings.ToLower(ct_state)]
    o.ct_state = &ct_state_int
}

func (o *OVSTable) CreateMap() map[string]struct{} {
// cookie=0x0, duration=10850.359s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=128/0x80,app_id=103,ct_state=1,tcp,nw_src=192.168.100.0/24,nw_dst=192.168.100.0/24,tp_src=10001,tp_dst=12001 actions=drop
    key := fmt.Sprintf("table_%d-priority_%d-flow_dir_%d-src_nwgrp_%d-dst_nwgrp_%d-", o.table, o.priority, o.flowDir, o.srcNwgrp, o.dstNwgrp)

    if o.appCat != nil {
       key = key + fmt.Sprintf("app_cat_%s-", *o.appCat)
    }

    if o.appId != nil {
       key = key + fmt.Sprintf("app_id_%d-", *o.appId)
    }

    if o.nwSrc != nil {
       key = key + fmt.Sprintf("src_nw_%s-", *o.nwSrc)
    }

    if o.nwDst != nil {
       key = key + fmt.Sprintf("dst_nw_%s-", *o.nwDst)
    }

    if o.srcPort != nil {
       key = key + fmt.Sprintf("src_port_%d-", *o.srcPort)
    }

    if o.dstPort != nil {
       key = key + fmt.Sprintf("dst_port_%d-", *o.dstPort)
    }

    if o.proto != nil {
       key = key + fmt.Sprintf("proto_%s-", *o.proto)
    }

    if o.ct_state != nil {
       key = key + fmt.Sprintf("connection_state_%d-", *o.ct_state)
    }

    key = key + fmt.Sprintf("action_%s", o.actions)

    return map[string]struct{}{
        key: struct{}{},
    }
}

func ParseOvsTable(out string) []OVSTable  {

       ovsentries := []OVSTable{}

       /*
        out := `cookie=0x0, duration=10850.360s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=128/0x80,app_id=103,ct_state=1,tcp,nw_src=192.168.100.0/24,nw_dst=192.168.18.0/24,tp_src=10001,tp_dst=12001 actions=drop
 cookie=0x0, duration=10850.360s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=128/0x80,app_id=103,ct_state=1,tcp,nw_src=192.168.100.0/24,nw_dst=192.168.18.0/24,tp_src=10001,tp_dst=12002 actions=drop
 cookie=0x0, duration=10850.359s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=128/0x80,app_id=103,ct_state=1,tcp,nw_src=192.168.100.0/24,nw_dst=192.168.100.0/24,tp_src=10001,tp_dst=12001 actions=drop
 cookie=0x0, duration=10850.359s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=128/0x80,app_id=103,ct_state=1,tcp,nw_src=192.168.100.0/24,nw_dst=192.168.100.0/24,tp_src=10001,tp_dst=12002 actions=drop
 cookie=0x0, duration=10850.359s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=128/0x80,app_id=103,ct_state=1,tcp,nw_src=192.168.100.0/24,nw_dst=192.168.18.0/24,tp_src=10002,tp_dst=12001 actions=drop
 cookie=0x0, duration=10850.359s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=128/0x80,app_id=103,ct_state=1,tcp,nw_src=192.168.100.0/24,nw_dst=192.168.18.0/24,tp_src=10002,tp_dst=12002 actions=drop
 cookie=0x0, duration=10850.359s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=128/0x80,app_id=103,ct_state=1,tcp,nw_src=192.168.100.0/24,nw_dst=192.168.100.0/24,tp_src=10002,tp_dst=12001 actions=drop
 cookie=0x0, duration=10850.359s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=128/0x80,app_id=103,ct_state=1,tcp,nw_src=192.168.100.0/24,nw_dst=192.168.100.0/24,tp_src=10002,tp_dst=12002 actions=drop
 cookie=0x0, duration=10850.358s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=16,app_id=83,ct_state=1,icmp,nw_src=192.168.18.0/24,nw_dst=192.168.18.0/24 actions=drop
 cookie=0x0, duration=10850.358s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=16,app_id=83,ct_state=1,icmp,nw_src=192.168.18.0/24,nw_dst=192.168.100.0/24 actions=drop
 cookie=0x0, duration=10850.357s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=16,app_id=83,ct_state=1,icmp,nw_src=192.168.100.0/24,nw_dst=192.168.18.0/24 actions=drop
 cookie=0x0, duration=10850.357s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=16,app_id=83,ct_state=1,icmp,nw_src=192.168.100.0/24,nw_dst=192.168.100.0/24 actions=drop
 cookie=0x0, duration=10850.354s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=16,app_id=103,ct_state=1,icmp,nw_src=192.168.18.0/24,nw_dst=192.168.18.0/24 actions=drop
 cookie=0x0, duration=10850.354s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=16,app_id=103,ct_state=1,icmp,nw_src=192.168.18.0/24,nw_dst=192.168.100.0/24 actions=drop
 cookie=0x0, duration=10850.353s, table=1, n_packets=0, n_bytes=0, idle_age=10850, priority=4010,flow_dir=1,src_nwgrp=0,dst_nwgrp=4095,app_cat=16,app_id=103,ct_state=1,icmp,nw_src=192.168.100.0/24,nw_dst=192.168.18.0/24 actions=drop`
 */
	//re := regexp.MustCompile(`(cookie=[\w]+),\s(duration=\d+.\d+)s,\s(table=\d),\s(n_packets=\d+),\s(n_bytes=\d+),\s(idle_age=\d+),\s(hard_age=\d+)?.?.?(priority=\d+),(flow_dir=\d+)?,?(src_nwgrp=\d+)?,?(dst_nwgrp=\d+)?,?(app_cat=[\d\/x]+)?,?(app_id=\d+)?,?(\w+)?,?(nw_src=[\d\.\/]+)?,?(nw_dst=[\d\.\/]+)?,?(tp_src=\d+)?,?(tp_dst=\d+)? (actions=[\w:\)\(,]+)`)
        //fmt.Println(re)
        //fmt.Println(out)re := regexp.MustCompile(`(cookie=[\w]+)..(duration=\d+.\d+)...(table=\d)..(n_packets=\d+)..(n_bytes=\d+)..(idle_age=\d+)..(hard_age=\d+)?.?.?(priority=\d+),(flow_dir=\d+)?,?(src_nwgrp=\d+)?,?(dst_nwgrp=\d+)?,?(app_cat=[\d\/x]+)?,?(app_id=\d+)?,?(ip)?,?(nw_src=[\d\.\/]+)?,?(nw_dst=[\d\.\/]+)? (actions=[\w:\)\(,]+)`)

re := regexp.MustCompile(`(cookie=[\w]+)..(duration=\d+.\d+)...(table=\d+)..(n_packets=\d+)..(n_bytes=\d+)..(idle_age=\d+)..(hard_age=\d+)?.?.?(priority=\d+),(flow_dir=\d+)?,?(src_nwgrp=\d+)?,?(dst_nwgrp=\d+)?,?(app_cat=[\d\/x]+)?,?(app_id=\d+)?,?(ct_state=\d+)?,?(\w+)?,?(nw_src=[\d\.\/]+)?,?(nw_dst=[\d\.\/]+)?,?(tp_src=\d+)?,?(tp_dst=\d+)? (actions=[\w:\)\(,]+)`)
   res := re.FindAllSubmatch([]byte(out), -1)

//fmt.Println(len(res))

for _, y := range res {
      ovs := OVSTable{}
  //  fmt.Println( len(y))
     fmt.Println("=======================================================")
    for i := 1; i < len(y); i++ {
      if strings.TrimSpace(string(y[i])) == "" {
          continue
      }
    //fmt.Println(strings.TrimSpace(string(y[i])))
    splitted := strings.Split(strings.TrimSpace(string(y[i])), "=")
    if len(splitted) == 2 {
      // fmt.Println(i, "2", splitted[0], splitted[1])
       assigner(&ovs, splitted[0], splitted[1])
    } else if len(splitted) == 1 {

	if i == 15 {
            assigner(&ovs, "proto", splitted[0])
	}
        //fmt.Println(i, "1", splitted[0])
    } else {
    }

 }
    ovsentries = append(ovsentries, ovs)
}

fmt.Println(len(ovsentries))

//for _, x := range ovsentries {
//    fmt.Println(x)
//   fmt.Println( x.CreateMap() )
//}

    return ovsentries
}

func assigner(o *OVSTable, var_name string, value string) {

    switch var_name {

        case "cookie":
            o.cookie = value

        case "duration":
            o.duration, _ = strconv.ParseFloat(value, 64)

        case "table":
            o.table, _ = strconv.Atoi(value)

        case "n_packets":
            o.nPackets, _ = strconv.Atoi(value)

        case "n_bytes":
            o.nBytes, _ = strconv.Atoi(value)

        case "idle_age":
            o.idleAge, _ = strconv.Atoi(value)

        case "hard_age":
            o.hardAge, _ = strconv.Atoi(value)

        case "priority":
            o.priority, _ = strconv.Atoi(value)

        case "flow_dir":
            o.flowDir, _ = strconv.Atoi(value)

        case "src_nwgrp":
            o.srcNwgrp, _ = strconv.Atoi(value)

        case "dst_nwgrp":
            o.dstNwgrp, _ = strconv.Atoi(value)

        case "app_cat":
            o.appCat = &value

        case "app_id":
            app_id, _ := strconv.Atoi(value)
	    o.appId = &app_id

        case "ct_state":
            ct_state, _ := strconv.Atoi(value)
            o.ct_state = &ct_state

        case "nw_src":
            o.nwSrc = &value

        case "nw_dst":
            o.nwDst = &value

        case "tp_src":
            src_port, _ := strconv.Atoi(value)
	    o.srcPort = &src_port

        case "tp_dst":
            dst_port, _ := strconv.Atoi(value)
            o.dstPort = &dst_port

        case "actions":
            o.actions = value

        case "proto":
            o.proto = &value

	default:
            // do nothing
    }
}
