package main

import (
	"fmt"
	log "lvnlog"
	//rest_service "restservice"
	emu "cpectrlplaneemu"
	//"time"
	//"utils"
	//"strings"
	"os"
	"io/ioutil"
	"encoding/json"
	//"strconv"
)

var (
	err              error
	res              bool
	error_reason     string
	testcase_name string = "get_rules"
)


type GetFunc func() (bool, error, map[string]interface{})

func (g GetFunc) WriteToFile(filename string) {

    if _ ,ok, nw_info := g(); ok != nil {
	log.Err.Printf("Failed to get %T info\n", g)
	return
    } else {
	if nw_info_bytes, err2 := json.MarshalIndent(nw_info, "", "  "); err2 == nil {
		if err := ioutil.WriteFile(filename,  nw_info_bytes, 0644); err != nil {
			fmt.Printf("eror wring file %s %s\n", filename, err.Error())
		}
        } else {
			fmt.Printf("wrote indenting\n")
        }
    }
}

func main() {

	var cpe_sl_no int
	var emu_url string
	var url string
	var uuid_provided bool = false
	log.New(testcase_name)
	foldername := log.LogFilename + "_" + log.Log_date

	arg_index := map[string]int{
		"url":        1,
		"uuid":       2,
		"foldername": 3,
	}

	no_of_args_reqd := 1 + len(arg_index) // 1 is scriptname

	if len(os.Args) == no_of_args_reqd {
            url = os.Args[arg_index["url"]]
	    //emu_url = strings.Split(url, "-webui")[0]+ strings.Trim(strings.Split(url, "-webui")[1], "/") + ":443"
	    emu_url = url + ":443"
	    if len(os.Args[arg_index["uuid"]]) == 47 {
            // UUID provided
	    cpe_sl_no = 1
	    uuid_provided = true
	    } else {
            //cpe_sl_no, _ = strconv.Atoi(os.Args[2])
		fmt.Println("UUID length wrong")
		return
	    }
	    foldername = os.Args[arg_index["foldername"]]
	} else {
		fmt.Println("Argument reqd: url, UUID, foldername")
		fmt.Println("<scriptname> https://deepakr10.cloudstation.io 00:00:00:00:00:00:00:00:00:00:01:11:00:00:03:ea 1002") 
		return
	}

	//emu_url    := "https://csperf.cloudstation.io:443"
	//emu_url    := "https://local-rg-webui.cloudstation.i:443"
	emu.Initialise_urls(emu_url)
	//cpe_sl_no := 5184
	cpestruct := emu.NewCloudport(cpe_sl_no, log.Info)
	if uuid_provided {
	    cpestruct.SetUUID(os.Args[arg_index["uuid"]])
        }
	cpe := &cpestruct

	res, err = cpe.Login()
	if !res {
	    fmt.Printf("error sent http login for %d\n", cpe_sl_no)
	    return
	}

        os.MkdirAll(foldername, 0666)
	GetFunc(cpe.GetAcl).WriteToFile(foldername+"/acl.json")
	GetFunc(cpe.GetQos).WriteToFile(foldername+"/qos.json")
	GetFunc(cpe.GetDns).WriteToFile(foldername+"/dns.json")
	GetFunc(cpe.GetTrl).WriteToFile(foldername+"/trl.json")
	GetFunc(cpe.GetLb).WriteToFile(foldername+"/lb.json")
	GetFunc(cpe.GetNat).WriteToFile(foldername+"/nat.json")
	GetFunc(cpe.GetMpls).WriteToFile(foldername+"/mpls.json")
	GetFunc(cpe.GetVpn).WriteToFile(foldername+"/vpn.json")
	GetFunc(cpe.GetSecurity).WriteToFile(foldername+"/security.json")
	GetFunc(cpe.GetFwdRules).WriteToFile(foldername+"/fwd.json")
	GetFunc(cpe.GetNwgroups).WriteToFile(foldername+"/nwgrp.json")
	GetFunc(cpe.GetFwdRulesV5).WriteToFile(foldername + "/fwdv5.json")
	GetFunc(cpe.GetNwgroupsV5).WriteToFile(foldername + "/nwgrpv5.json")
	GetFunc(cpe.GetForwardExt).WriteToFile(foldername + "/fwdext.json")
}
