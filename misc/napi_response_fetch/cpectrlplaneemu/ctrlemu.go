package cpectrlplaneemu

import (
	"compress/gzip"
	"encoding/json"
	"errors"
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var (
	CS_rest_API              map[string]string
	cs_api_duration_map_secs map[string]int64
	err error

	Headers_v1 = map[string]string{
		"Accept":          "application/json",
		"Content-Type":    "application/x-www-form-urlencoded",
		"Accept-Encoding": "gzip",
		"AUTHORIZATION":   "X-LAVELLE-AUTH sessionid=",
	}

	Headers_v2 = map[string]string{
		"Accept":          "application/json",
		"Content-Type":    "application/x-www-form-urlencoded",
		"Accept-Encoding": "gzip",
		"VERSION-COMPAT":  "v2",
		"AUTHORIZATION":   "X-LAVELLE-AUTH sessionid=",
	}

	Headers_v3 = map[string]string{
		"Accept":          "application/json",
		"Content-Type":    "application/x-www-form-urlencoded",
		"Accept-Encoding": "gzip",
		"VERSION-COMPAT":  "v3",
		"AUTHORIZATION":   "X-LAVELLE-AUTH sessionid=",
	}
	defaultCookie     []*http.Cookie         = []*http.Cookie{}

)

func Initialise_urls(CS_url string) {
	CS_rest_API = map[string]string{

		"login":                      strings.Join([]string{CS_url, "/api/v2/login/"}, ""),                     // 200
		"get_global_config":          strings.Join([]string{CS_url, "/api/v2/global_config_pull_req/"}, ""),    //30s 200
		"prov_config":                strings.Join([]string{CS_url, "/api/v4/provision/config/"}, ""),          //60s 200
		"post_iface_update":          strings.Join([]string{CS_url, "/api/v3/route/interface_update/"}, ""),    //30s
		"post_peerstate_updates":     strings.Join([]string{CS_url, "/api/v2/route/peering_state_update"}, ""), //30s 200
		"keyclient":                  strings.Join([]string{CS_url, "/api/v2/rules/vpn_keys"}, ""),             //35s 200
		"post_syncdate":              strings.Join([]string{CS_url, "/api/v2/syncdate/send_date/"}, ""),        //60s 200
		"post_system_update":         strings.Join([]string{CS_url, "/api/v2/system_update/"}, ""),             //30s 200
		"get_dns_proxy":              strings.Join([]string{CS_url, "/api/v2/rules/dns_proxy/"}, ""),           //25s 200
		"rules_t1":                   strings.Join([]string{CS_url, "/api/v4/rules/acls/"}, ""),                //10mins 200
		"rules_t2":                   strings.Join([]string{CS_url, "/api/v4/rules/qos/"}, ""),                 // 10mins 200
		"rules_t3":                   strings.Join([]string{CS_url, "/api/v4/rules/policers/"}, ""),            //10mins 200
		"rules_t4":                   strings.Join([]string{CS_url, "/api/v4/rules/lb_policy/"}, ""),           //10mins 200
		"get_fwd_rules":              strings.Join([]string{CS_url, "/api/v4/rules/forward/"}, ""),             //10mins 200
		"nat_info_v4":                strings.Join([]string{CS_url, "/api/v4/rules/nat_policy/"}, ""),          //10mins 200
		"nw_grps":                    strings.Join([]string{CS_url, "/api/v4/rules/network_groups/"}, ""),      //10mins 200
		"post_discovered_subnets_v3": strings.Join([]string{CS_url, "/api/v3/route/discovered_subnets/"}, ""),  //17s
		"get_syncdate":               strings.Join([]string{CS_url, "/api/v2/syncdate/sync_date/"}, ""),
		"get_resolve_req":            strings.Join([]string{CS_url, "/api/v4/rules/resolve/resolve/"}, ""),
		"get_url_filtering":          strings.Join([]string{CS_url, "/api/v2/rules/url_filtering/"}, ""),
		"post_lndpi_rules":           strings.Join([]string{CS_url, "/api/v3/lndpi/rules/"}, ""),
		"get_lndpi_signature":        strings.Join([]string{CS_url, "/api/v3/lndpi/signatures/"}, ""),
		"get_lndpi_checksum":         strings.Join([]string{CS_url, "/api/v3/lndpi/checksums/"}, ""),
		"mpls_profiles":              strings.Join([]string{CS_url, "/api/v2/rules/mpls_profiles/"}, ""),       //10mins 200
		"security_policy":            strings.Join([]string{CS_url, "/api/v4/rules/security_policy/"}, ""),     //10mins 200
		"get_ha_config":              strings.Join([]string{CS_url, "/api/v1/ha_config/"}, ""),                 //10mins 200
		"get_dp_service_config":      strings.Join([]string{CS_url, "/api/v1/dataplane_service_config/"}, ""),  //10mins 200

		"nw_grps_v5":                 strings.Join([]string{CS_url, "/api/v5/rules/network_groups/"}, ""),      //10mins 200
		"get_fwd_rules_v5":           strings.Join([]string{CS_url, "/api/v5/rules/forward/"}, ""),             //10mins 200
		"forward_ext":                strings.Join([]string{CS_url, "/api/v1/rules/forward_ext/"}, ""),         //10mins 200
	}

	cs_api_duration_map_secs = map[string]int64{
		"post_discovered_subnets_v3": 15, //17s
		"get_ha_config":              15,

		"get_global_config":      30, //30s 200
		"post_system_update":     30, //30s
		"get_dns_proxy":          30, //25s 200
		"iface_update":           30, //30s
		"post_peerstate_updates": 30, //30s
		"keyclient":              30,

		"prov_config":           60,
		"post_syncdate":         60, //60s cpe also getting 500
		"get_dp_service_config": 60,

		"rules_t1":        600, //10mins 200
		"rules_t2":        600, // 10mins 200
		"rules_t3":        600, //10mins 200
		"rules_t4":        600, //10mins 200
		"get_fwd_rules":   600, //10mins 200
		"nat_info_v4":     600, //10mins 200
		"nw_grps":         600, //10mins 200
		"mpls_profiles":   600, //10mins 200
		"security_policy": 600, //10mins 200

		"login": 1500, // 200

		"nw_grps_v5":        600,
		"get_fwd_rules_v5":     600,
		"forward_ext":          600,

	}

}

type gzreadCloser struct {
	*gzip.Reader
	io.Closer
}

func (gz gzreadCloser) Close() error {
	return gz.Closer.Close()
}

type CloudPort struct {
	uuid                           string
	name                           string
	serial_no                      int
	login_status                   bool
	resp_time                      map[string][]int64
	headers_v1                     map[string]string
	headers_v2                     map[string]string
	headers_v3                     map[string]string
	Log                            *log.Logger
}

func NewCloudport(sl_no int, log *log.Logger) CloudPort {

	cpe := CloudPort{
		serial_no:                      sl_no,
		name:                           fmt.Sprintf("Device-%d", sl_no),
		uuid:                           generate_uuid(sl_no),
		headers_v1:                     map[string]string{},
		headers_v2:                     map[string]string{},
		headers_v3:                     map[string]string{},
	        Log:                            log,
	}

	for k, v := range Headers_v1 {
		cpe.headers_v1[k] = v
	}
	for k, v := range Headers_v2 {
		cpe.headers_v2[k] = v
	}
	for k, v := range Headers_v3 {
		cpe.headers_v3[k] = v
	}

	return cpe
}

/*To convert the serial number into UUID, thus making the CPE simulation easier*/
func generate_uuid(serial_number int) string {
	uuid := 0
	uuid |= serial_number //make to UUID start with serial number
	uuid |= 1 << (128 - 96)
	uuid |= 1 << (128 - 92)
	uuid |= 1 << (128 - 88)

	// UUID is a 32byte Hexa string, thus converting the integer to hexadecimal
	string_uuid := fmt.Sprintf("%0.32x", uuid)

	result := []string{}
	// Converting the 32 hexadecimal integer into colon separated string
	for x := 0; x < len(string_uuid); x += 2 {
		result = append(result, string_uuid[x:x+2])
	}
	return strings.Join(result, ":")
}

func convert_struct_to_string_via_json(v interface{}) (bool, string) {
	var json_payload string
	bytesRepresentation, err := json.Marshal(v)
	if err != nil {
		return false, json_payload
	}
	if len(bytesRepresentation) > 0 {
		json_payload = string(bytesRepresentation)
		return true, json_payload
	}
	return false, json_payload
}

func http_request(method string, providedUrl string, queryParams map[string]interface{}, header map[string]string, data map[string]string, cookies []*http.Cookie, log *log.Logger) (result *http.Response, response_time *int64, err error) {

	providedUrlSplitted := strings.Split(providedUrl, "/")

	uri := strings.Join(providedUrlSplitted[0:3], "/")
	apiPath := "/" + strings.Join(providedUrlSplitted[3:], "/")

	baseurl, _ := url.Parse(uri)
	baseurl.Path += apiPath
	params := url.Values{}

	for queryParamKey, queryParamValue := range queryParams {
		//params.Add(queryParamKey, string(queryParamValue))
		params.Add(queryParamKey, fmt.Sprintf("%v", queryParamValue))
	}

	baseurl.RawQuery = params.Encode()

	completeUrl := baseurl.String()

	var request *http.Request

	json_datas := url.Values{}
	for k, v := range data {
		json_datas.Add(k, v)
	}
	json_datas_encoded := json_datas.Encode()

	bytesRepresentation, err1 := json.Marshal(data)

	if err1 != nil {
		panic(err1)
	}

	var json_payload string
	if len(bytesRepresentation) > 0 {
		json_payload = string(bytesRepresentation)
		request, err = http.NewRequest(method, completeUrl, strings.NewReader(json_datas_encoded))
	} else {
		json_payload = "{}"
		request, err = http.NewRequest(method, completeUrl, nil)
	}
	//}

	// reuses the connection, check if server is closing or not.
	// if server is closing, set this to true, so that http goclient will create
	// new connection instead of resusing old ones
	request.Close = true

	if err != nil {
		//fmt.Println("error in New request creation")
		fmt.Printf(" Error in http request creation\n")
		return
	}

	session_id := ""
	/* Setting the http header values*/
	for hdr_name, hdr_value := range header {
		if hdr_name == "AUTHORIZATION" {
			session_id = hdr_value
		}
		if _, ok := request.Header[hdr_name]; ok {
			request.Header.Set(hdr_name, hdr_value)
		} else {
			request.Header.Add(hdr_name, hdr_value)
		}
	}

	/* 2. Creating client*/
	var timeout time.Duration = 50

	for _, cookie := range cookies {
		request.AddCookie(cookie)
	}

	SkipSecurityVerification := true
	    transCfg := &http.Transport{
                TLSClientConfig: &tls.Config{InsecureSkipVerify: SkipSecurityVerification}, // ignore expired SSL certificates
            }
	    client := &http.Client{Transport: transCfg, Timeout: time.Second * timeout}

	t1 := time.Now()
	result, err = client.Do(request)
	t2 := time.Now()

	//	defer result.Body.Close()

	response_duration := t2.Sub(t1).Milliseconds()
	response_time = &response_duration
	if err != nil {

		log.Printf("[ERROR] HTTP request %s %s =|%s|= %s \n", method, completeUrl, session_id, err.Error())
		return result, response_time, err
	}

	var _display_payload_data bool = false

	if _display_payload_data {
		log.Printf(" [INFO] HTTP request %s %s =|%s|= Json: %s response %d Duration %dms\n", method, completeUrl, session_id, json_payload, result.StatusCode, response_duration)
	} else {
		log.Printf(" [INFO] HTTP request %s %s =|%s|= response %d Duration %dms\n", method, completeUrl, session_id, result.StatusCode, response_duration)
	}

	resp := result

	if resp.Header.Get("Content-Encoding") == "gzip" {
		resp.Header.Del("Content-Length")
		zr, err2 := gzip.NewReader(resp.Body)
		if err2 != nil {
			fmt.Println(err)
			return
		}
		resp.Body = gzreadCloser{zr, resp.Body}
	}
	return result, response_time, err
}

func (cpe *CloudPort) Login() (bool, error) {

	key := "login"
	//interval := cs_api_duration_map_secs[key] * 1000
	password := "45c146bb10332edbefb89e2784775d99"
	response_data := map[string]interface{}{}

	var login_data struct {
		Uuid     string `json:"uuid"`
		Username string `json:"username"`
		Password string `json:"password"`
	}

	login_data.Uuid = cpe.uuid
	login_data.Username = "lavelle"
	login_data.Password = password

	resp, resp_time, err := http_request("GET", CS_rest_API[key], map[string]interface{}{"uuid": cpe.uuid}, map[string]string{}, map[string]string{
		"uuid":     cpe.uuid,
		"username": "lavelle",
		"password": password,
	}, defaultCookie, cpe.Log)
	if err != nil {
		cpe.Log.Printf("[ERROR] login failed %s\n", cpe.name)
		return false,  err
	}

	response_data = validate_response(key, resp, cpe, resp_time, err)

	var session_id interface{}
	var ok bool = false
	if session_id, ok = response_data["session_id"]; !ok {
		cpe.Log.Println("[ERROR] JSON Parse ")
		return false, errors.New("[ERROR] JSON Parse")
	}

	if resp.StatusCode == 200 {
		cpe.login_status = true
		cpe.headers_v1["AUTHORIZATION"] = fmt.Sprintf("X-LAVELLE-AUTH sessionid=%s", session_id.(string))
                cpe.headers_v2["AUTHORIZATION"] = fmt.Sprintf("X-LAVELLE-AUTH sessionid=%s", session_id.(string))
                cpe.headers_v3["AUTHORIZATION"] = fmt.Sprintf("X-LAVELLE-AUTH sessionid=%s", session_id.(string))
	} else {
		fmt.Println("Error logging in please retry")
		cpe.login_status = false
	}
        return true, nil
}

func (cpe *CloudPort) GetFwdRules() (bool, error, map[string]interface{}) {

	key := "get_fwd_rules"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
}

func (cpe *CloudPort) GetNwgroups() (bool, error, map[string]interface{}) {

	key := "nw_grps"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v1)
}

func (cpe *CloudPort) GetFwdRulesV5() (bool, error, map[string]interface{}) {

	key := "get_fwd_rules_v5"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
}

func (cpe *CloudPort) GetNwgroupsV5() (bool, error, map[string]interface{}) {

	key := "nw_grps_v5"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v1)
}

func (cpe *CloudPort) GetForwardExt() (bool, error, map[string]interface{}) {

	key := "forward_ext"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
}

func (cpe *CloudPort) GetAcl() (bool, error, map[string]interface{}) {

	key := "rules_t1"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
	//cpe.GetApiRespStats(key, interval, 2, &apiStruct.http_rules_acl, lpCtr.count_rules_acl)
}

func (cpe *CloudPort) GetSyncDate() (bool, error, map[string]interface{}) {

	key := "get_syncdate"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
	//cpe.GetApiRespStats(key, interval, 2, &apiStruct.http_rules_acl, lpCtr.count_rules_acl)
}

func (cpe *CloudPort) GetProvConfig() (bool, error, map[string]interface{}) {

	key := "prov_config"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
	//cpe.GetApiRespStats(key, interval, 2, &apiStruct.http_rules_acl, lpCtr.count_rules_acl)
}

func (cpe *CloudPort) GetLndpiSignature() (bool, error, map[string]interface{}) {

	key := "get_lndpi_signature"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
	//cpe.GetApiRespStats(key, interval, 2, &apiStruct.http_rules_acl, lpCtr.count_rules_acl)
}

func (cpe *CloudPort) GetLndpiChecksum() (bool, error, map[string]interface{}) {

	key := "get_lndpi_checksum"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
	//cpe.GetApiRespStats(key, interval, 2, &apiStruct.http_rules_acl, lpCtr.count_rules_acl)
}

func (cpe *CloudPort) GetHaConfig() (bool, error, map[string]interface{}) {

	key := "get_ha_config"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
	//cpe.GetApiRespStats(key, interval, 2, &apiStruct.http_rules_acl, lpCtr.count_rules_acl)
}

func (cpe *CloudPort) GetDPServiceConfig() (bool, error, map[string]interface{}) {

	key := "get_dp_service_config"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
	//cpe.GetApiRespStats(key, interval, 2, &apiStruct.http_rules_acl, lpCtr.count_rules_acl)
}

func (cpe *CloudPort) GetURLFiltering() (bool, error, map[string]interface{}) {

	key := "get_url_filtering"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
	//cpe.GetApiRespStats(key, interval, 2, &apiStruct.http_rules_acl, lpCtr.count_rules_acl)
}

func (cpe *CloudPort) GetResolve() (bool, error, map[string]interface{}) {

	key := "get_resolve_req"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
	//cpe.GetApiRespStats(key, interval, 2, &apiStruct.http_rules_acl, lpCtr.count_rules_acl)
}

func (cpe *CloudPort) GetGlobalConfig() (bool, error, map[string]interface{}) {

	key := "get_global_config"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
	//cpe.GetApiRespStats(key, interval, 2, &apiStruct.http_rules_acl, lpCtr.count_rules_acl)
}

func (cpe *CloudPort) GetQos() (bool, error, map[string]interface{})  {

	key := "rules_t2"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v1)
	//cpe.GetApiRespStats(key, interval, 1, &apiStruct.http_rules_qos, lpCtr.count_rules_qos)
}

func (cpe *CloudPort) GetTrl() (bool, error, map[string]interface{}) {

	key := "rules_t3"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v1)
	//cpe.GetApiRespStats(key, interval, 1, &apiStruct.http_rules_trl, lpCtr.count_rules_trl)
}

func (cpe *CloudPort) GetLb() (bool, error, map[string]interface{}) {

	key := "rules_t4"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v1)
	//cpe.GetApiRespStats(key, interval, 1, &apiStruct.http_rules_lb, lpCtr.count_rules_lb)
}

func (cpe *CloudPort) GetNat() (bool, error, map[string]interface{}) {

	key := "nat_info_v4"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v1)
	//cpe.GetApiRespStats(key, interval, 1, &apiStruct.http_nat_info, lpCtr.count_nat_info)
}

func (cpe *CloudPort) GetMpls() (bool, error, map[string]interface{}) {

	key := "mpls_profiles"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v1)
	//cpe.GetApiRespStats(key, interval, 1, &apiStruct.http_mpls_profile, lpCtr.count_mpls_profile)
}

func (cpe *CloudPort) GetSecurity()  (bool, error, map[string]interface{}) {

	key := "security_policy"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v1)
	//cpe.GetApiRespStats(key, interval, 1, &apiStruct.http_security_policy, lpCtr.count_security_policy)
}

func (cpe *CloudPort) GetVpn()  (bool, error, map[string]interface{}) {

	key := "keyclient"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v1)
	//cpe.GetApiRespStats(key, interval, 1, &apiStruct.http_get_keyclient, lpCtr.count_get_keyclient)
}

func (cpe *CloudPort) GetDns() (bool, error, map[string]interface{}) {

	key := "get_dns_proxy"
	interval := cs_api_duration_map_secs[key] * 1000
	return cpe.GetApiRespStats(key, interval, cpe.headers_v2)
	//cpe.GetApiRespStats(key, interval, 2, &apiStruct.http_get_dns_proxy, lpCtr.count_get_dns_proxy)
}

func (cpe *CloudPort) GetApiRespStats(key string, interval int64, header map[string]string) (bool, error, map[string]interface{}) {

	res, resp_time, err := http_request("GET", CS_rest_API[key], map[string]interface{}{"uuid": cpe.uuid}, header, map[string]string{"uuid": cpe.uuid}, defaultCookie, cpe.Log)

	if err != nil {
	    return false, err, map[string]interface{}{}
	}
	response_data := validate_response(key, res, cpe, resp_time, err)
	return true, nil,  response_data
}

func (cpe *CloudPort) SetUUID(uuid string) {
	cpe.uuid = uuid
}

func validate_response(res_name string, resp *http.Response, cpe *CloudPort, delay *int64, err error) (response_data map[string]interface{}) {

	if cpe == nil {
		panic(errors.New("CPE itself not addressed..."))
	}
	if resp == nil {

		return
	}

	if err != nil {
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		err12 := json.NewDecoder(resp.Body).Decode(&response_data)
		if err12 != nil {
			cpe.Log.Println(err12.Error())
			return
		}

		if ok, json_data := convert_struct_to_string_via_json(response_data); ok {
			cpe.Log.Printf(" [INFO] RESPONSE: %s: %s\n", res_name, json_data)
		}

	}
	return
}
