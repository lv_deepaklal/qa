namespace :bulk_cpe_entry do
  desc "TODO"
  task :write, [:user_email] => :environment do |t, args|

    user_email = args[:user_email]
    user = User.find_by(email: user_email)
    if not user.nil?
      puts "Starting Bulk Insertion. User: #{user.email}"

      model_params = "LN00030015"
      profile = Profile.where(customer_id: user.customer_id).first

      PublicActivity.enabled = false

      ActiveRecord::Base.transaction do
        if File.file?("/tmp/locations.json")
          locations_json = File.read("/tmp/locations.json")
        else
          locations_json = "{}"
        end
        locations = JSON.parse locations_json

        full_data = CSV.read("/opt/lavelle/web/lib/tasks/cloudports.csv")

        full_data.each do |arr|
          cpe_name = "#{arr[0]}-#{arr[3]}-#{arr[4]}"
          puts "Creating CloudPort : #{cpe_name}"

          serial_no = arr[1]
          uuid = arr[2]

          location = arr[3].strip.downcase
          place_id = locations[location]

          if uuid.length == 47
            #ok
          else
              raise "Error while creating CloudPort #{cpe_name}: UUID length mismatch"
          end

          if place_id.nil?

            puts "City #{location} was not found in Locations"

            place_id = update_location(location)
            if place_id.empty?
              raise "Error while creating CloudPort #{cpe_name}: Failed to get Location"
            end

            puts "City: #{location}, Place ID: #{place_id}"

            locations[location] = place_id
          end

          site = Site.create!(
            name: arr[0],
            user_id: user.id,
            customer_id: user.customer_id,
            is_hub: false,
            place_id: place_id,
            location: location,
            auto_upgrade: false,
            profile_id: profile.id
          )

          if site and site.errors.messages.empty?
            cloudport = Cloudport.create!(
              name: cpe_name,
              serial_no: serial_no,
              s_number: uuid,
              model: model_params,
              site_id: site.id,
              user_id: site.user_id,
              customer_id: site.customer_id,
            )
            if cloudport.errors.messages.empty?
              puts "CloudPort #{cpe_name} has been created"
            else
              puts "There was an error in the creation of CloudPort #{cpe_name}"
            end
          else
            puts "No entry for Site #{arr[0]}"
          end
        end

        File.write("/tmp/locations.json", locations.to_json)
      end

    else
      puts "No user selected"
    end

  end
end

def update_location location
  begin
    res_location = RestClient.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + location + "&key=AIzaSyA6x86F5kIBcOCcVaIiVSKSR_mNJOQ262E")
    return JSON.parse(res_location.body)["results"][0]["place_id"]
  rescue => e
    puts e
  end
  return ""
end

