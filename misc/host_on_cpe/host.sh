#!/bin/bash
if [ $# -ne 4 ];
    then echo "illegal number of parameters"
    echo "Usage: $0 <host-ns-name> <vid> <host-ip-with-mask> <host-default-gw>"
    echo "   Ex: $0 host1 100 10.4.5.10/24 10.4.5.1"
   exit
fi

hostname=$1
vid=$2
host_ip=$3
gw_ip=$4

switchname="sw0"
host_side_iface="${1}.lan"
switch_side_iface="${1}.sw"

ip link add $host_side_iface type veth peer name $switch_side_iface
ip netns add $hostname
ip link set $host_side_iface netns $hostname
ip netns exec $hostname ip link set $host_side_iface up
ip netns exec $hostname ip link set lo up
ip link set $switch_side_iface master $switchname
bridge vlan add dev $switch_side_iface vid $vid pvid untagged
ip link set $switch_side_iface up
ip netns exec $hostname ifconfig $host_side_iface $host_ip
ip netns exec $hostname ip route add default via $gw_ip
