#!/usr/bin/env python2


import filecmp
import os
import sys
import tarfile
import shutil


TMP_PATH = "/tmp"
SOURCES_DIR = "sources"


def taropen(file, pos=None):
    tar = tarfile.open(file, "r:*")
    if pos is not None:
        tar.seek(pos)
    return tar


def get_extract_path(package):
    return "{0}/{1}".format(TMP_PATH, os.path.basename(
        package.name).replace(".lnpkg", ""))


def _get_recursive_file_listing(path):
    return [os.path.relpath(os.path.join(dirpath, file), path)
            for (dirpath, dirnames, filenames) in os.walk(path)
            for file in filenames]


def lndiff(source_path, destination_path):
    old_sources_path = os.path.join(source_path, SOURCES_DIR)
    new_sources_path = os.path.join(destination_path, SOURCES_DIR)
    old_files = set(_get_recursive_file_listing(old_sources_path))
    new_files = set(_get_recursive_file_listing(new_sources_path))

    combined = old_files | new_files

    result = {
        "added": [],
        "modified": [],
        "deleted": []
    }

    for file in sorted(combined):
        if file in old_files and file in new_files:
            same = filecmp.cmp(os.path.join(old_sources_path, file),
                               os.path.join(new_sources_path, file),
                               shallow=False)
            if not same:
                result['modified'].append(file)
        elif file in old_files:
            result['deleted'].append(file)
            # print("Only in %s: %s" % (a, fn))
        elif file in new_files:
            result['added'].append(file)
            # print("Only in %s: %s" % (b, fn))
        else:
            print("Skipping : {0}. Not a file".format(file))

    return result


if __name__ == '__main__':
    lndiff(*sys.argv[1:])
