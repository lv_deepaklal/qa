#!/usr/bin/env python2


import os
import sys
import copy
import json
import traceback
import subprocess
import collections
import lntar


METADATA_FILE = "metadata.json"
PATCH_NAME = "{0}_{1}.{2}.{3}_{4}_{5}"
PATCH_PATH = "build/package/{0}".format(PATCH_NAME)

SERVICE_MAPPING = {
    "cloud-port": [
        ("bin/lndpi", ["lavelle-lndpi"]),
        ("bin/lpclnt", ["lavelle-link-probe"]),
        ("bin/lpqprobe", ["lavelle-link-probe"]),
        ("bin/modem_mgr", ["lavelle-modem-mgr"]),
        ("bin/ppe", ["lavelle-ppe"]),
        ("bin/ln-transport", ["lavelle-lntransport"]),
        ("bin/pathrdr", ["lavelle-pathrdr"]),
        ("bin/revtunnel", ["lavelle-revtunnel"]),
        ("bin/uflowrdr", ["lavelle-uflowrdr"]),
        ("bin/fileserver/fileserver", ["lavelle-file-server"]),
        ("prebuilt/bin/ovs", ["lavelle-ppe"]),
        ("scripts/cp", ["lavelle-cp"]),
        ("scripts/web", ["lavelle-webui"]),
        ("scripts/ppe_setup.py", ["lavelle-ppe"]),
    ],
    "lavelle-linux-bintools": [
        ("bin/ln_ipt_rules_mgmt.sh", ["lavelle-linkutil"]),
    ]
}
EXCLUDE_FILES = ["metadata.json", "version"]


def _execute(command):
    output = None
    try:
        output = subprocess.check_output(command, shell=True,
                                         stderr=sys.stdout,
                                         executable="/bin/bash",
                                         close_fds=True)
    except subprocess.CalledProcessError as exception:
        print("Command returned a non-zero exit status.\nCommand ::: {0}"
              "\nOutput ::: {1}".format(command, exception.output))
        raise exception
    except Exception:
        print("Exception Occurred :::")
        return False
    return output if output else True


def _mkdir_p(path):
    try:
        os.makedirs(path)
        return True
    except OSError:
        if os.path.isdir(path):
            return True
        raise


def _get_version_string(version):
    return "{0}.{1}.{2}".format(version['major'], version['minor'],
                                version['patch'])


def _check_patch_feasibility(source_metadata, destination_metadata):
    source_version = source_metadata['Version']
    destination_version = destination_metadata['Version']
    if source_version.get("major") == destination_version.get("major") and \
            source_version.get("minor") == destination_version.get("minor"):
        return True
    return False


def _copy_with_parents(source, destination, lead=0):
    stripped_source = "/".join(source.split("/")[lead:])
    source_dir = os.path.dirname(stripped_source)
    _mkdir_p(os.path.join(destination, source_dir))
    lntar.shutil.copy(source, os.path.join(destination, stripped_source))
    return True


def _get_affected_services(package, patch_files):
    affected_services = set()
    path_service_list = SERVICE_MAPPING[package]
    for file in patch_files:
        if os.path.basename(file) in EXCLUDE_FILES:
            continue
        affected_service_list = []
        for path_service in path_service_list:
            if path_service[0] in file:
                affected_service_list = path_service[1]
                break
        if "system" in affected_service_list:
            return ["system"]
        affected_services.update(affected_service_list)
    return sorted(affected_services)


def _prepare_metadata(source_metadata, destination_metadata, patch_folder,
                      patch_files):
    final_metadata = copy.deepcopy(destination_metadata)

    # Files
    final_metadata['Files'] = patch_files

    patch_files = patch_files['deleted'] + patch_files['added'] + \
        patch_files['modified']
    # Services
    final_metadata['Affected Services'] = _get_affected_services(
        destination_metadata.get("Package", ""), patch_files)

    # Patch Size
    patch_size = _execute(
        "du -s {0}".format(patch_folder)).split()[0].decode("utf-8")
    final_metadata['Extracted Patch Size'] = patch_size

    # Restart Lavelle Services
    final_metadata['Extracted Patch Size'] = patch_size

    # Base Package
    final_metadata['Base Package Version'] = source_metadata['Version']

    # Type
    final_metadata['Type'] = "patch"

    # Write to metadata.json
    with open("{0}/{1}".format(patch_folder, METADATA_FILE), "w") as \
            metadata_file:
        json.dump(final_metadata, metadata_file, indent=4)

    return _get_version_string(source_metadata['Version'])


def _get_patch_dir(metadata):
    version = metadata.get("Version", collections.OrderedDict())
    # Create patch folder
    patch_folder = PATCH_PATH.format(
        metadata.get("Package", ""),
        version.get("major", "0"),
        version.get("minor", "0"),
        version.get("patch", "0"),
        version.get("_type", "I"),
        version.get("_commit", "0")
    )
    _mkdir_p(patch_folder)
    return patch_folder


def _create_patch(build_path):
    result = _execute(
        "GZIP_OPT=-9 tar --atime-preserve -C {0} -czf {0}.patch .".format(
            build_path))
    return result


def main():
    try:
        destination_tar = lntar.taropen(DESTINATION)
        destination_path = lntar.get_extract_path(destination_tar)
        destination_tar.extractall(path=destination_path)
        destination_metadata_path = os.path.join(destination_path,
                                                 METADATA_FILE)
        destination_metadata = json.load(
            open(destination_metadata_path),
            object_pairs_hook=collections.OrderedDict)
        source_tar = lntar.taropen(SOURCE)
        source_path = lntar.get_extract_path(source_tar)
        source_tar.extractall(path=source_path)
        source_metadata_path = os.path.join(source_path, METADATA_FILE)
        source_metadata = json.load(
            open(source_metadata_path),
            object_pairs_hook=collections.OrderedDict)
        if not _check_patch_feasibility(source_metadata, destination_metadata):
            print("Incompatible Source & Destination Versions")
            return 1
        patch_files = lntar.lndiff(source_path, destination_path)
        if any([files for modification_type, files in patch_files.items()]):
            patch_dir = _get_patch_dir(destination_metadata)
            for modification_type in ["added", "modified"]:
                for file in patch_files.get(modification_type, []):
                    file_path = os.path.join(destination_path,
                                             lntar.SOURCES_DIR, file)
                    _copy_with_parents(file_path, patch_dir, lead=3)
            base_version = _prepare_metadata(
                source_metadata, destination_metadata, patch_dir, patch_files)
            result = _create_patch(patch_dir)
            if result:
                print("Patch created successfully")
                print("{0},{1}".format(
                    base_version,
                    os.path.basename("{0}.patch".format(patch_dir))))
                lntar.shutil.rmtree(patch_dir)
                exit_code = 0
            else:
                print("Failed to create Patch. \nOutput: {0}".format(result))
                exit_code = 1
        else:
            print("Cannot Create Patch Package."
                  "Identical Source & Destination Packages.")
            exit_code = 1
        lntar.shutil.rmtree(source_path)
        lntar.shutil.rmtree(destination_path)
        return exit_code
    except Exception:
        print("Exception Occurred :::")
        traceback.print_exc()
        return -1


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Invalid Arguments")
        sys.exit(1)
    SOURCE = sys.argv[1]
    DESTINATION = sys.argv[2]
    sys.exit(main())
