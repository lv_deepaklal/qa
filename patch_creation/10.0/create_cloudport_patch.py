#!/usr/bin/env python2


import os
import sys
import copy
import json
import time
import subprocess


TMP_DIR = "/var/tmp/{0}"
UPGRADE_CONF = "upgrade_conf.json"
PATCH_CREATION_SCRIPT = "create_patch.py"
CLOUDPORT_PACKAGE_TAR = "CloudPort-{0}-R.tgz"
CLOUDPORT_PACKAGE_GPG = "{0}.gpg".format(CLOUDPORT_PACKAGE_TAR)
PACKAGE_VERSION = ""
PACKAGE_TYPE_MAPPING = {
    "cloud-port": "service",
    "lavelle-linux-bintools": "system"
}
PACKAGE_DESCRIPTION_MAPPING = {
    "cloud-port": "CloudPort Service Package",
    "lavelle-linux-bintools": "CloudPort System Package"
}
PACKAGE_STRUCTURE = {
    "archive-name": "",
    "checksum": "",
    "package-type": "",
    "version": "",
    "base-version": "",
    "package-description": ""
}


def _execute(command):
    output = None
    try:
        output = subprocess.check_output(
            command, shell=True, stderr=subprocess.STDOUT,
            executable="/bin/bash", close_fds=True)
    except subprocess.CalledProcessError as exception:
        print("Command returned a non-zero exit status.\nCommand ::: {0}"
              "\nOutput ::: {1}".format(command, exception.output))
        return False
    except Exception as exception:
        print("Exception Occurred ::: {0}".format(exception))
        return False
    return output if output else True


def _create_patch(directory, packages):
    packages_string = " ".join(packages)
    command = "{0} {1}".format(os.path.join(".", PATCH_CREATION_SCRIPT),
                               packages_string)
    output = _execute(command)
    if not output:
        print("Failed to create patch for packages : {0}"
              .format(packages_string))
        return "", ""
    version, patch_package = output.split()[-1].strip().split(",")
    os.rename(os.path.join("build/package", patch_package),
              os.path.join(directory, patch_package))
    return version, patch_package


def _create_patches(directory, cloudport_packages, bintools_packages):
    if len(cloudport_packages) not in [0, 2] or \
            len(bintools_packages) not in [0, 2]:
        print("Invalid Number of Package Files")
        print(cloudport_packages, bintools_packages)
        return False
    base_version_patch_mapping = {}
    if len(cloudport_packages) == 2:
        cloudport_base_version, cloudport_patch = _create_patch(
            directory, cloudport_packages)
        if not cloudport_base_version or not cloudport_patch:
            return False
        base_version_patch_mapping["cloud-port"] = [cloudport_base_version,
                                                    cloudport_patch]
    time.sleep(2)
    if len(bintools_packages) == 2:
        bintools_base_version, bintools_patch = _create_patch(
            directory, bintools_packages)
        base_version_patch_mapping["lavelle-linux-bintools"] = [
            bintools_base_version, bintools_patch]
    return base_version_patch_mapping


def _valid_arguments(args):
    return all([os.path.exists(arg) for arg in args])


def _get_checksum(directory, file):
    path = os.path.join(directory, file)
    cksum_command = "cksum '{0}'".format(path) + " | awk '{print $1}'"
    return _execute(cksum_command).strip()


def _generate_upgrade_configuration(package_directory, patch_mapping):
    global PACKAGE_VERSION
    upgrade_conf = {
        "package-list": [],
        "packages": {},
        "type": "patch"
    }
    for package, package_dict  in patch_mapping.items():
        base_version, patch_package = package_dict
        package_name = patch_package.replace(".patch", "")
        package_dict = copy.deepcopy(PACKAGE_STRUCTURE)
        version_fields = package_name.split("_")[1:]
        if package == "cloud-port":
            PACKAGE_VERSION = version_fields[0]
        package_dict['archive-name'] = patch_package
        package_dict['package-type'] = PACKAGE_TYPE_MAPPING[package]
        package_dict['version'] = "_".join(version_fields)
        package_dict['base-version'] = base_version
        package_dict['package-description'] = PACKAGE_DESCRIPTION_MAPPING[
            package]
        package_checksum = _get_checksum(package_directory, patch_package)
        if not package_checksum:
            print("Checksum of {0} could not be calculated".format(
                patch_package))
            sys.exit(1)
        package_dict['checksum'] = str(package_checksum)
        upgrade_conf['package-list'].append(package)
        upgrade_conf['packages'][package] = package_dict
    with open(os.path.join(package_directory, UPGRADE_CONF),
              "w") as upgrade_conf_file:
        json.dump(upgrade_conf, upgrade_conf_file, indent=4, sort_keys=True)
    parent_dir = os.path.normpath(os.path.join(package_directory, os.pardir))
    return os.path.join(package_directory,
                        CLOUDPORT_PACKAGE_TAR.format(PACKAGE_VERSION))


def _create_tar(package_directory, package_tar_path):
    tar_command = "cd '{0}' && tar -czf '{1}' * && cd -".format(
        package_directory, CLOUDPORT_PACKAGE_TAR.format(PACKAGE_VERSION))
    tar_result = _execute(tar_command)
    if tar_result:
        parent_dir = os.path.normpath(os.path.join(package_directory,
                                                   os.pardir))
        return os.path.join(package_directory,
                            CLOUDPORT_PACKAGE_GPG.format(PACKAGE_VERSION))
    return ""


def _encrypt_tar_gpg(package_tar_path, package_gpg_path, public_key_file,
                     private_key_file):
    gpg_homedir = TMP_DIR.format(int(time.time()))
    gpg_encrypt_command = \
        "mkdir -p '{0}' && gpg --homedir '{0}' --yes --import '{1}' && "\
        "gpg --homedir '{0}' --yes --import '{2}' && gpg --homedir '{0}' " \
        " --yes --encrypt --sign --recipient 'Lavelle Packaging' " \
        "--trust-model 'always' --output '{3}' '{4}' && rm -rf '{0}'".format(
            gpg_homedir, public_key_file, private_key_file, package_gpg_path,
            package_tar_path)
    return _execute(gpg_encrypt_command)


def main():
    if len(sys.argv) != 6 or not _valid_arguments(sys.argv[1:4]):
        print("Invalid Arguments")
        print("Usage:\n\tpython create_cloudport_package.py <PUBLIC_KEY> "
              "<PRIVATE_KEY> <PACKAGE_DIRECTORY> <CLOUDPORT_PACKAGES> "
              "<BINTOOLS_PACKAGES>")
        sys.exit(1)

    public_key_file = sys.argv[1]
    private_key_file = sys.argv[2]
    package_directory = sys.argv[3]
    cloudport_packages = list(filter(None, sys.argv[4].split(",")))
    bintools_packages = list(filter(None, sys.argv[5].split(",")))

    patch_create_result = _create_patches(
        package_directory, cloudport_packages, bintools_packages)
    if not patch_create_result:
        print("Failed to create patches")
        return

    package_tar_path = _generate_upgrade_configuration(package_directory,
                                                       patch_create_result)
    if not package_tar_path:
        print("Failed to generate upgrade configuration")
        return

    package_gpg_path = _create_tar(package_directory, package_tar_path)
    if not package_gpg_path:
        print("Failed to package tar")
        return

    gpg_encryption_result = _encrypt_tar_gpg(package_tar_path,
                                             package_gpg_path, public_key_file,
                                             private_key_file)
    if not gpg_encryption_result:
        print("Failed to encrypt tar")
        return

    print("Encrypted Successfully : {0}".format(package_gpg_path))


if __name__ == "__main__":
    main()
