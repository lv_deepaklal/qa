#Pre-Requisites:
LNPKG Version : 3.0
LN Upgrade Service Version : 3.0
CS Version : 10.x

#Creation
Get the 2 lnpkg packages for which you want create a patch package.
Sample Packages:
 Source: cloud-port_8.1.1_I_5bcfa2d.lnpkg
 Destination: cloud-port_8.1.2_I_5bcfa2d.lnpkg
 This can have both cloud-port & bintools packages as long as the source & destination versions
are the same.

Copy the 2 files to a new folder
Copy the following scripts to the same folder:
    - create_cloudport_patch.py
    - create_patch.py
    - lntar.py
    - packaging.priv
    - packaging.pub
Create an empty directory in that folder for the target version of the patch, like "8.1.2"

#Command for creation:
python2 create_cloudport_patch.py packaging.pub packaging.priv ~/10_patches/8.1.2
cloud-port_8.1.1_I_5bcfa2d.lnpkg,cloud-port_8.1.2_I_5bcfa2d.lnpkg ""
NOTE: The last empty string in the above command is to indicate the missing bintools packages for
patch creation.


