#!/usr/bin/python2

# UUID is a 16 byte identifier. Below is the format of UUID
#
# <---------------------------- 128 bits ----------------------------------->
# <--------- Upper 64 bits --------->
# |============================|====|========|====|==|==|=================|
# |                A           |  B |    C   |  D | E| F|        G        |
# |============================|====|========|====|==|==|=================|
#                                   <---------- Lower 64 bits ----------->
#
# =============================== Description ==================================
#
# Upper 64 bits.
# (Only used for instances in cloud. Set to 0's for other instances.)
#
# | A | 58 bits | Not Used.
# | B | 8 bits  | Cloud provider type. See the below table for the possible values
# |   |         | Set to 0's in Hardware devices.
# ------------------------------------------------------------------------------
#
# Lower 64 bits
#
# | C | 16 bits | Reserved for future. Set to 0's
# | D | 8 bits  | Model Number. Set to 0's in ACE and FCE
# | E | 4 bits  | Node Category. See table below for the possible values
# | F | 4 bits  | Instance Type. See table below for the possible values
# | G | 32 bits | Serial Number

# Cloud Provider Table
# | Provider | Value
# | AWS      | 1
# | SFL      | 2
# | Azure    | 3
#
#
# Node Category Table
# | CPE      | 1
# | ACE      | 2
# | FCE      | 3
# | HE       | 4
#
#
# Instance Type Table
# | HW-USFF | 1
# | HW-SVR  | 2
# | HW-PC   | 3
# | VM      | 4
#
#
# Model Numbers table
# | LN00030015 | 1 | LAVELLE_LANNER
# | LN01030015 | 2 | SHORETEL LANNER
# | LN00010015 | 3 | SHORETEL NETGATE
# | LN00200000 | 4 | VCPE
#
#

import sys
import argparse


CLOUD_PROVIDER_POSITION = [57, 64]
MODEL_NUMBER_POSITION   = [80, 88]
NODE_CATEGORY_POSITION  = [89, 92]
INSTANCE_TYPE_POSITION  = [93, 96]
SERIAL_NUMBER_POSITION  = [97, 128]

CLOUD_PROVIDERS = {
    'aws': 1,
    'sfl': 2,
    'azure': 3
}

NODE_CATEGORIES = {
    'CPE': 1,
    'ACE': 2,
    'FCE': 3,
    'HE': 4
}

INSTANCE_TYPES = {
    'HW-USFF': 1,
    'HW-SVR': 2,
    'HW-PC': 3,
    'VM': 4
}

MODEL_NUMBERS = {
    'LN00030015': 1, # LAVELLE_LANNER
    'LN01030015': 2, # SHORETEL LANNER
    'LN00010015': 3, # SHORETEL NETGATE
    'LN00200000': 4, # VCPE
    'RPI3MB'    : 5  # RaspberryPi 3 Model B
}


def _validate_arg_range(arg_name, args, position):
    # max value is (2^n) - 1. Where is number of available bits
    max_value = (2 ** (position[1] - position[0] + 1)) - 1

    # Check if the argument value can be casted to int
    try:
        arg_value = int(args[arg_name])
    except ValueError:
        raise ValueError(
            "{0}={1} is not valid number"
        )

    if (arg_value <= 0) or (arg_value > max_value):
        raise ValueError(
            "{0}={1} is not valid. Exceeds maximum range of serial number".format(
                arg_name, arg_value
            )
        )


def _validate_arg_option(arg_name, args, dictionary):
    arg_value = args[arg_name]
    if arg_value not in dictionary.keys():
        raise ValueError(
            "{0}={1} is not valid. Value must be in {2}".format(
                arg_name, arg_value, ", ".join(dictionary.keys())
            )
        )


def generate_uuid(serial_number):

    uuid = 0
    uuid |= serial_number << 128 - 128
    uuid |= 1 << 128 - 96
    uuid |= 1 << 128 - 92
    uuid |= 1 << 128 - 88

    uuid = format(uuid, 'x').zfill(32)

    return ':'.join(uuid[i]+uuid[i+1] for i in range(0, len(uuid)-1, 2))

def generate_uuid(**kwargs):
    REQUIRED_PARAMS = ['model_number', 'node_category', 'instance_type', 'serial_number']

    # Check ig all the mandatory arguments are present. This represents the
    # lowest possible case of required arguments.
    for arg in REQUIRED_PARAMS:
        if arg not in kwargs:
            raise ValueError("{0} missing in arguments".format(arg))

    model_number  = kwargs['model_number']
    node_category = kwargs['node_category']
    instance_type = kwargs['instance_type']
    serial_number = kwargs['serial_number']

    # Check if the value of the argument is valid. Else raises an exception
    _validate_arg_option('model_number', kwargs, MODEL_NUMBERS)
    _validate_arg_option('node_category', kwargs, NODE_CATEGORIES)
    _validate_arg_option('instance_type', kwargs, INSTANCE_TYPES)
    _validate_arg_range('serial_number', kwargs, SERIAL_NUMBER_POSITION)

    uuid = 0
    uuid |= serial_number << 128 - SERIAL_NUMBER_POSITION[1]
    uuid |= INSTANCE_TYPES[instance_type] << 128 - INSTANCE_TYPE_POSITION[1]
    uuid |= NODE_CATEGORIES[node_category] << 128 - NODE_CATEGORY_POSITION[1]
    uuid |= MODEL_NUMBERS[model_number] << 128 - MODEL_NUMBER_POSITION[1]

    # Cloud Providers is only valid in case of VMs
    if instance_type == 'VM':
        if 'cloud_provider' not in kwargs:
            raise ValueError("{0} missing in arguments".format(arg))

        cloud_provider = kwargs['cloud_provider']
        _validate_arg_option('cloud_provider', kwargs, CLOUD_PROVIDERS)
        uuid |= CLOUD_PROVIDERS[cloud_provider] << 128 - CLOUD_PROVIDER_POSITION[1]

    # format(integer, 'x') unlike hex() returns hexadeciamal representation
    # without any prefix '0x' or suffix 'L'.
    uuid = format(uuid, 'x').zfill(32)

    return ':'.join(uuid[i]+uuid[i+1] for i in range(0, len(uuid)-1, 2))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(prog='gen_uuid', usage='%(prog)s [options]')

    parser.add_argument(
        '--serial', metavar='', required=True, type=int, help='serial number'
    )
    parser.add_argument(
        '--model', metavar='', required=True, type=str, help='model number'
    )
    parser.add_argument(
        '--node-category', metavar='', required=False, type=str, help='node category'
    )
    parser.add_argument(
        '--instance-type', metavar='', required=False, type=str, help='instance type'
    )
    parser.add_argument(
        '--cloud-provider', metavar='', required=False, type=str, help='cloud provider'
    )

    args = parser.parse_args()

    model_number   = args.model
    node_category  = args.node_category
    instance_type  = args.instance_type
    serial_number  = args.serial
    cloud_provider = args.cloud_provider

    if node_category is None:
        node_category = 'CPE'

    if instance_type is None:
        instance_type = 'HW-USFF'

    try:
        uuid = generate_uuid(
            serial_number=serial_number, model_number=model_number,
            node_category=node_category, instance_type=instance_type,
            cloud_provider=cloud_provider
        )
        print(uuid)
    except ValueError as e:
        print(str(e))
        sys.exit(1)
