from time import strftime, localtime
from logging import basicConfig, DEBUG, INFO, getLogger
import os
import unittest
import sys
import xmlrunner
import signal
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

Logger = getLogger(__name__)
xml_timestamp = log_dir = file_basename = None


class ScriptTimeoutException(Exception):
    pass


def _write_to_console(msg):
    # NOTE: xmlrunner hijacks sys.stdout, so to *actually* print to console
    # requires using __stdout__ (which is the original stdout setting)
    sys.__stdout__.write(msg)

    # Flush to force the message to be written immediately, else since it's
    # buffered, it could end up getting printed later and out of order.
    sys.__stdout__.flush()


def _get_xml_timestamp(time_now=None):
    if not time_now:
        time_now = localtime()
    local_xml_timestamp = strftime("%Y%m%d%H%M%S", time_now)
    return local_xml_timestamp


# Defaulting script timeout to 20 minutes
def setup_module_helper(file, alarm_timeout=3600):
    """
    Takes care of creating the "Logs" directory and configuring the logger object.
    Intended to be invoked in setUpModule() of the individual test scripts.
    """
    BasePath = os.path.abspath(os.path.dirname(file))
    time_now = localtime()
    log_timestamp = strftime("%Y%m%d_%H%M%S", time_now)
    global xml_timestamp
    xml_timestamp = _get_xml_timestamp(time_now)

    global log_dir
    log_dir = os.path.join(BasePath, 'Logs')
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    global file_basename
    file_basename = os.path.basename(file)

    log_file = os.path.join(log_dir, file_basename + '_' + log_timestamp + '.log')

    # Write to console the path of the log file, so it's easy for the user
    # to view.
    # NOTE: xmlrunner hijacks sys.stdout, so to *actually* print to console
    # requires using __stdout__ (which is the original stdout setting)
    _write_to_console("Log file: {0}\n".format(log_file))

    basicConfig(
        filename=log_file,
        level=DEBUG,
        format='%(asctime)-4s [%(filename)s:%(lineno)s:%(funcName)-4s] %(msg)s'
    )
    Logger.info('TEST SCRIPT: {0}\n'.format(file))

    def _signal_handler(signal, frame):
        raise ScriptTimeoutException("SIGALRM: Script did not complete within timeout of "
                                     "{0} seconds".format(alarm_timeout))

    signal.signal(signal.SIGALRM, _signal_handler)
    signal.alarm(alarm_timeout)

    return Logger


class CustomXmlTestRunner(xmlrunner.XMLTestRunner):
    """
    A custom test-runner that builds on top of xmlrunner, which additionally
    writes a report of all test-cases to the end of the test's log file.

    Note that xmlrunner itself is built on top of unittest and writes
    the test results to a JUNIT XML file in addition to the typical
    functionality provided by unittest.
    """

    def __init__(self, **kwargs):
        final_kwargs = kwargs
        # Set to highest verbosity level
        final_kwargs['verbosity'] = 2
        final_kwargs['output'] = 'Logs'
        final_kwargs['outsuffix'] = _get_xml_timestamp()
        super(CustomXmlTestRunner, self).__init__(**final_kwargs)

    def run(self, *args, **kwargs):
        """
        Overridden implementation of the run() method (which is what actually
        runs the test-suite), that additionally adds a table of results of
        all of the test-cases within the test script.
        """

        # If super() failed, then we would not have access to the result object,
        # which is necessary for writing the summary to the logs and for
        # writing the JUNIT XML files. So we install a decorator to get access
        # to the result object even if an exception was thrown.
        def _get_result_handle(func):
            def _inner(result_obj, *args, **kwargs):
                func(result_obj, *args, **kwargs)
                self._result_obj = result_obj

            return _inner

        xmlrunner.__init__ = _get_result_handle(xmlrunner.__init__)

        # Handle a script timeout here outsuffix
        try:
            result = super(CustomXmlTestRunner, self).run(*args, **kwargs)
        except ScriptTimeoutException as e:
            result = self._result_obj
            _write_to_console(str(e) + '\n')
            # Write the JUNIT XML
            result.generate_reports(self)

        # Might need to access some of these to write a sane result if setUpClass/
        # tearDownClass/setUpModule fail.
        # sys.__stdout__.write(str(result.__dict__.keys()) + "\n")
        # sys.__stdout__.write(str(result._previousTestClass._classSetupFailed) + '\n')
        # sys.__stdout__.write(str(result._previousTestClass) + '\n')
        # sys.__stdout__.write(str(result._previousTestClass._junit_xml_path) + '\n')

        Logger.info('')
        Logger.info('*' * 80)
        format_str = '{0:<30} {1:<30} {2:<30}'
        Logger.info(format_str.format('TESTCASE', 'TIME TAKEN (s)', 'RESULT'))
        Logger.info('-' * 80)

        # The "id" is <module>.<classname>.<testcase_name>
        # We need only the test-case name, so split on "." and return the
        # last element
        def _extract_testcase_from_id(id):
            elems = id.split('.')
            return elems[-1]

        for success in result.successes:
            Logger.info(format_str.format(_extract_testcase_from_id(success.test_id),
                                          success.elapsed_time, 'PASS'))

        # TODO: Might need to look for errors as well, not just failures
        all_failures = result.failures + result.errors
        for failure_tuple in all_failures:
            # "failures" is a 2-element tuple. The first element is the
            # details of the test-case, the second is the traceback message.
            failure = failure_tuple[0]
            Logger.info(format_str.format(_extract_testcase_from_id(failure.test_id),
                                          failure.elapsed_time, 'FAIL'))

        Logger.info('*' * 80)
        return result


class CustomUnitTestCase(unittest.TestCase):
    """
    Provides an overridden version of unittest.TestCase that:
    * Prints a nice banner at the beginning of each test-case with test-case name
      and description.
    * Logs the result of each test-case
    * Provides some convenience methods, which are currently limited to logging.
    """

    # Helper for creating object from classname. Useful for
    # classmethods like setUpClass, tearDownClass to access instance methods
    @classmethod
    def _create_obj_from_cls(cls):
        self = cls()
        self.setUp()
        return self

    @classmethod
    def setUpClass(cls):
        module = cls.__module__
        junit_xml_file = ''

        if module == '__main__':
            # Run as a script, so module is set to "__main__". In this case,
            # the JUNIT XML file created does not have the module name
            junit_xml_file = 'TEST_{cls_name}_{timestamp}.xml'. \
                format(cls_name=cls.__name__, timestamp=xml_timestamp)
        else:
            # JUNIT XML file format: TEST-<module>-<class_name>-<timestamp>.xml
            junit_xml_file = 'TEST_{module}.{cls_name}_{timestamp}.xml'. \
                format(module=module, cls_name=cls.__name__, timestamp=xml_timestamp)

        junit_xml_path = os.path.join(log_dir, junit_xml_file)
        cls._junit_xml_path = junit_xml_path

        _write_to_console('JUNIT XML path: {0}\n\n'.format(junit_xml_path))

        # Install a decorator so we know when ScriptTimeoutException was raised.
        # The base class implementation of run() would have caught it, so
        # we cannot use try-except in our code. Instead, we set
        # cls._timeout_exception if/when this exception is thrown, so we know
        # this exception was raised if this attribute is set.
        def _timeout_decorate(func):
            def _inner(timeout_obj, *args, **kwargs):
                func(timeout_obj, *args, **kwargs)
                cls._timeout_exception = timeout_obj

            return _inner

        ScriptTimeoutException.__init__ = _timeout_decorate(ScriptTimeoutException.__init__)

        return cls._create_obj_from_cls()

    @classmethod
    def tearDownClass(cls):
        return cls._create_obj_from_cls()

    def setUp(self):
        # Reset variables for each test-case
        self._step_num = 1
        self._previous_step_time = None

    def run(self, result=None):
        # First print the test-case + description
        description = self.shortDescription()
        if description:
            self.write_description(description)

        # Then run the test-case
        super(CustomUnitTestCase, self).run(result)

        # And finally write the result
        self.write_result(result)
        self.write_log('')

        # Raise an exception to break out without executing the other test-cases
        if getattr(self, '_timeout_exception', None):
            alarm_err_str = '! ' + str(self._timeout_exception) + ' !'
            self.write_log('!' * len(alarm_err_str))
            self.write_log(alarm_err_str)
            self.write_log('!' * len(alarm_err_str))
            raise self._timeout_exception

    def write_log(self, msg):
        """Write a message to the log"""
        Logger.info(msg)

    def write_description(self, description, testcase=None):
        """
        Outputs a banner for each test-case, like:

            ##################################
            TESTCASE: my_test_case
            DESCRIPTION: This tests something
            ##################################

        This typically gets called implicitly (by reading the test-case's
        docstring).
        """
        self.write_log('#' * 60)
        testcase = testcase or self._testMethodName
        self.write_log('TESTCASE: {0}'.format(testcase))
        self.write_log('DESCRIPTION: {0}'.format(description))
        self.write_log('#' * 60)

    def write_result(self, result):
        """
        Write the result of the test-case, like:

            -----------------------
            TESTCASE: my_test_case
            RESULT: PASS
            -----------------------

        This too is implicitly called at the end of the test-case.
        """
        self.write_log('-' * 60)
        self.write_log('TESTCASE: {0}'.format(self._testMethodName))

        if result.wasSuccessful():
            self.write_log('RESULT: PASS')
        else:
            self.write_log('RESULT: FAIL')

            # The failure details are either in "failures" or "errors"
            failure_tuple = result.failures + result.errors
            failure_traceback_elems = failure_tuple[0][1].split('\n')
            for elem in failure_traceback_elems:
                self.write_log(elem)

        self.write_log('-' * 60)

    def write_step(self, msg):
        """
        Write a test step to the logs. This will end up logging something like:

            STEP 2 (+2s): This is step 2

        This includes the step number, the time taken by the previous step,
        and the step message.
        """
        import time
        self.write_log('=' * 60)
        step_time_msg = ''
        if self._previous_step_time:
            step_time_msg = ' (+' + str(time.time() - self._previous_step_time) + 's)'
        self.write_log('STEP {0}{1}: {2}'.format(self._step_num, step_time_msg, msg))
        self.write_log('=' * 60)
        self._step_num += 1
        self._previous_step_time = time.time()
