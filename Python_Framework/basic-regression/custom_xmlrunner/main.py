from .custom_unittest_runner import CustomXmlTestRunner
import unittest

# Provide a modularized main() implementation, so that this can be used by scripts,
# instead of the scripts needing to repeat all these options.
def main(**kwargs):
    unittest.main(
        testRunner=CustomXmlTestRunner,
        # see issue #59 of xmlrunner
        failfast=False, catchbreak=False, buffer=False, **kwargs)
