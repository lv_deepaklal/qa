# __main__.py allows invoking scripts similar to how unittest-based
# scripts are invoked, like:
# python3 -m custom_xmlrunner my_script

import unittest
from .main import main
import sys

if sys.argv[0].endswith("__main__.py"):
    import os.path
    # We change sys.argv[0] to make help message more useful
    # use executable without path, unquoted
    # (it's just a hint anyway)
    # (if you have spaces in your executable you get what you deserve!)
    executable = os.path.basename(sys.executable)
    sys.argv[0] = executable + " -m custom_xmlrunner"
    del os

__unittest = True

main(module=None)