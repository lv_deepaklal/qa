import os
import time
import json
from urllib import parse

import requests
import imaplib,quopri
from logging import getLogger
try:
    from json.decoder import JSONDecodeError
except ImportError:
    JSONDecodeError = ValueError

Logger = getLogger(name=__name__)

class EntryExit(object):

    def __init__(self, f):
        self.f = f

    def __call__(self):
        Logger.info ("Entering", self.f.__name__)
        self.f()
        Logger.info("Exited", self.f.__name__)


class ConfigManager(object):

    access_token = None

    def _retry_on_exception(func,max_tries=3, retry_sleep=0.3):
        """Mthod to retry on Exception
        :param max_tries: Integer , 3 - by default
        :return:
        """

        def wrapper(*args,**kwargs):
            for i in range(max_tries):
                try:
                    return func(*args, **kwargs)
                    break
                except Exception as err:
                    msg = "Got Exception:{}, retrying attempt: {}".format(err,i+1)
                    print(msg)
                    if i+1 == max_tries:
                        raise Exception("Got Exception in the last attempt: {}".format(err))
                    else:
                        time.sleep(retry_sleep)
                        continue
        return wrapper

    def add_credentials(f):

        def inner(self, *args, **kwargs):
            params = dict()
            headers = dict()

            # make changes befoer push
            if "headers" in kwargs.keys():
                headers = kwargs['headers']
                del kwargs['headers']

            if 'Authorization' not in headers:
                headers['Authorization'] = "Bearer {token}".format(token=self.access_token)

            if 'Content-type' not in headers:
                headers['Content-type'] = 'application/json'

            kwargs['headers'] = headers

            return f(self, *args, **kwargs)

        return inner

    def __init__(self, cloudstation_details,act_as='net_admin'):
        '''

        :param cloudstation_details:
        :param act_as: helps to create access token based on the user_type
                        1] net_admin
                        2] sys_admin
                        3] master
        '''


        self.cloudstation = cloudstation_details

        [ setattr(self, "_%s"%k, v) for k,v in self.cloudstation.items() ]


        if act_as == 'master':
            self.setUsername(self._master_username)
            self.setPassword(self._master_password)

        if act_as == 'net_admin':

            self.setUsername(self._net_admin_username)
            self.setPassword(self._net_admin_password)

        if act_as == 'sys_admin':
            self.setUsername(self._sys_admin_username)
            self.setPassword(self._sys_admin_password)

        #make changes before push
        self.access_token = None

        if self.access_token is None:
            self.setAccess_token()

    def setAccess_token(self):
        self.access_token = self._set_clodstation_acces_token()

    def setUsername(self, name):
        self.__username = name

    def getUsername(self):
        return self.__username

    def setPassword(self, pwd):
        self.__password = pwd

    def getPassword(self):
        return self.__password

    def _set_clodstation_acces_token(self):
        """The local function to create the active harmony session
        :return: session object
        """
        url = self.base_url + 'sessions/login/'
        common_headers = {"Content-Type":"application/x-www-form-urlencoded"}

        self.payload = {
            'username': self.getUsername(),
            'password': self.getPassword()
        }
        payload = parse.urlencode(self.payload)
        Logger.info(payload, url)
        response = requests.post(url=url,headers=common_headers,data=payload).json()
        Logger.info("The access token  is : {id}".format(id = response['access_token']))
        return response['access_token']

    def get_user_details(self):
        """
        Function to get user/customer details of the CS
        :return: user details
        """
        url = self.base_url + 'sessions/login/'
        common_headers = {"Content-Type": "application/x-www-form-urlencoded"}

        self.payload = {
            'username': self.getUsername(),
            'password': self.getPassword()
        }

        payload = parse.urlencode(self.payload)

        response = requests.post(url=url, headers=common_headers, data=payload).json()

        return response

    @property
    def base_url(self):
        return self._base_url


    @add_credentials
    def post(self, *args, **kwargs):
        return requests.post(*args, **kwargs)

    @add_credentials
    def get(self, *args, **kwargs):
        return requests.get(*args, **kwargs)

    @add_credentials
    def put(self, *args, **kwargs):
        return requests.put(*args, **kwargs)

    @add_credentials
    def delete(self, *args, **kwargs):
        return requests.delete(*args, **kwargs)



if __name__=="__main__":
    basepath = os.path.join(os.path.dirname(__file__), '../../data_bags/')
    bp = os.path.join(basepath, "cloudstation.json")

    with open(bp) as fp:
        linux_hash = json.load(fp)

    a = ConfigManager(linux_hash,act_as='sys_admin')



