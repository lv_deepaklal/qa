import os, sys, time, re
import json
import datetime
from datetime import timedelta
import requests
import sys
sys.path.append('../')
from modules.StationAPI.api import ConfigManager
from modules.sshlib.ssh import ConnectionHandler
from custom_xmlrunner import setup_module_helper, main, CustomUnitTestCase
from logging import getLogger
from utils import ln_gen_uuid
Logger = getLogger(name=__name__)

class EntryExit(object):

    def __init__(self, f):
        self.f = f

    def __call__(self):
        Logger.info ("Entering", self.f.__name__)
        self.f()
        Logger.info("Exited", self.f.__name__)



class databag(object):

    def __init__(self, auth_info="../data_bags/script_data_bags/cloudoperations.json"):
        if not auth_info.startswith("/"):
            auth_info=os.path.abspath(
                os.path.join(os.path.dirname(__file__), auth_info))

        with open(auth_info, 'r') as f:
            self.cloudOperation = json.load(f)

class CloudOperations(ConfigManager):

    data_map = {}

    def __init__(self,cloudstation_details='../data_bags/cloudstation.json',act_as='net_admin'):

        cs_json = self._read_cloudstation_json(path=cloudstation_details)
        ConfigManager.__init__(self,cloudstation_details=cs_json,act_as=act_as)
        self.setup_param()

    @classmethod
    def _read_cloudstation_json(self,path):
        with open(path)as fp:
            self.cloudstation = json.load(fp)

        return self.cloudstation
    @classmethod
    def _change_data(cls, json_data):
        """utility method to change/replace values in the json_data
        :param json_data: json dict to modify
        :return:
        """
        for k, v in cls.data_map.items():
            json_data = json_data.replace(str(k), str(v))
        return json_data

    def setup_param(self):
        global stationbag
        a = databag()
        stationbag = a.cloudOperation

    def create_customer(self,**kwargs):
        """
        Function to create a customer
        :param kwargs: required param (customer_name and url_prefix) others are taken by default if not passed
        :return: customer_details
        """

        if 'sys_email' not in kwargs.keys():
            kwargs['sys_email'] = 'sys@lavellenetworks.com'

        if 'net_email' not in kwargs.keys():
            kwargs['net_email'] = 'net@lavellenetworks.com'

        if 'manager_name' not in kwargs.keys():
            kwargs['manager_name'] = 'Vengadesh'

        if 'manager_email' not in kwargs.keys():
            kwargs['manager_email'] = 'vengadesh@lavellenetworks.com'

        if 'license_name' not in kwargs.keys():
            kwargs['license_name'] = 'cloudstation_pro'

        if 'domain' not in kwargs.keys():
            kwargs['domain'] = 'cloudstation.io'

        url_prefix = kwargs.get('url_prefix')
        url = url_prefix + '.' + kwargs.get('domain')
        self.data_map['__customer-name__'] = kwargs.get('customer_name')
        self.data_map['__url-prefix__'] = kwargs.get('url_prefix')

        self.data_map['__cloudstation-url__'] = url
        self.data_map['__sys-email__'] = kwargs.get('sys_email')
        self.data_map['__net-email__'] = kwargs.get('net_email')
        self.data_map['__manager-name__'] = kwargs.get('manager_name')
        self.data_map['__manager-email__'] = kwargs.get('manager_email')
        self.data_map['__license-name__'] = kwargs.get('license_name')
        self.data_map['__domain__'] = kwargs.get('domain')

        url = self._base_url + 'customers/'
        payload = self._change_data(json.dumps(stationbag['customer']['payload']))


        res = self.post(url = url, data = payload)
        if res.status_code != 200:
            Logger.info(res.json())
            raise Exception('customer creation failed : {} '.format(res.json()))

        Logger.info('Customer created succesfully \n Response {} '.format(res.json()))
        return res.json()

    def create_network(self,**kwargs):
        """
        Function to create a network location cloud-station
        :param kwargs: location -> where to create a network, is_hub -> true/false user_id -> user_id
        :return:
        """

        if "location" not in kwargs.keys():
            kwargs['location'] = "Hassan Karnataka India"

        self.data_map['__network-location-name__'] = kwargs.get('network_location_name')

        if kwargs.get('is_hub'):
            self.data_map['__is-hub__'] = 'true'
        else:
            self.data_map['__is-hub__'] = 'false'

        self.data_map['__user-id__'] = kwargs.get('user_id')
        self.data_map['__location__'] = kwargs.get('location')

        url = self._network_location
        payload = self._change_data(json.dumps(stationbag['network_location']['payload']))

        Logger.info('Creating network as follows {} '.format(payload))
        response = self.post(url = url, data = payload)

        if response.status_code != 200:
            Logger.info(response.json())
            raise Exception('Failed to create a network location : {}'.format(response.json()))

        Logger.info('Response {} '.format(response.json()))
        return response.json()

    def create_network_appliance(self,name,site_id,uuid):
        """
        Function to create a network appliance for network location
        :param name: Name of the network appliance
        :param site_id: where to attach the network appliance
        :param uuid: 12 digit uuid of CPE
        :return: CPE details
        """

        self.data_map['__cp-name__'] = name
        self.data_map['__site-id__'] = site_id
        self.data_map['__serial-number__'] = uuid
        payload = self._change_data(json.dumps(stationbag['cloudport']['payload']))
        url = self._cloud_port

        Logger.info('Creating network appliance as follows {}'.format(payload))

        response = self.post(url = url, data = payload)

        if response.status_code != 200:
            Logger.info('Failed to add network applicance {} '.format(response.json()))
            raise Exception('Failed to create network appliance {}'.format(response.json()))

        Logger.info('Response : {} '.format(response.json()))
        return response.json()

    def create_lan_config(self,force=True,**kwargs):
        """
        Creates a lan configure in user specified port and user specified cloudport
        :param force: verifies if given subnet clashes with other subnet
        :param kwargs: port-name -> which port to attach lan | config lan_ip -> lan ip | prefix_length -> subnet prefix
        :return:
        """
        #take care of adding multiple address in one go
        # Logger.info()
        if 'port_name' not in kwargs.keys():
            kwargs['port_name'] = "lan0"
        self.data_map['__port-name__'] = kwargs.get('port_name')
        self.data_map['__cp-link-id__'] = kwargs.get('cloudport_linkid')

        payload = stationbag['lan-config']['payload']



        self.data_map['__lan-ip__'] = kwargs.get('lan_ip')
        self.data_map['__prefix-length__'] = kwargs.get('prefix_length')

        payload = self._change_data(json.dumps(payload))
        url = self._cloud_port + '{}/interfaces'.format(kwargs.get('cloudport_id'))


        response = self.post(url = url,data = payload)
        return response

    def get_cloudport_linkid(self,cloudport_id,port):
        """
        Function to get a cloudport link id associated with respect to that port
        :param cloudport_id: cloudport id
        :param port: which port cloudport id user needs
        :return: cloudport link id
        """
        url = self._cloud_port + '{}/cloudport_links'.format(cloudport_id)
        response = self.get(url=url)
        if response.status_code == 200:
            data = response.json()
            Logger.info(data)
            for cloudport_link in data['cloudport_links']:
                if cloudport_link['name'] == port:
                    Logger.info('cloudport link id w.r.t  given cloudport {} is {}'.format(cloudport_id,cloudport_link['id']))
                    return cloudport_link['id']
        else:
            raise Exception('Failed to retrive cloudport link id for the port {} : reason {}'.format(port,response.json()))

    def get_app_classifier_details(self, app_names):

        self.applications = {
            "application_category":{},
            "application":{},
            "custom": {},
            "to":{
                "lan_subnets":[],
                "customs":[]
            },
            "from":{
                "lan_subnets":[],
                "customs":[]
            }
        }

        url = self._base_url + 'policies/policy_objects'
        res = self.get(url).json()

        if "application_categories" in app_names.keys():
            for app in app_names['application_categories']:
                for app_cate in res['policy_objects']['application_categories']:
                    if app_cate['name'] == app:
                        self.applications['application_category'][app_cate['name']] = app_cate


        if "application" in app_names.keys():
            for app_name in app_names['application']:
                for application in res['policy_objects']['applications']:
                    if (application['name'] == app_name):
                        self.applications['application'][app_name] = application

        if "custom" in app_names.keys():
            for custom in app_names['custom']:
                d = { "addition_type": "OR", "object": {"value": custom, "type": "CustomProtocol"} }
                self.applications['custom'][custom] = d

        if 'to' in app_names.keys():
            if 'lan_subnets' in app_names['to']:
                pass

            if 'custom' in app_names['to']:
                for ip in app_names['to']['custom']:
                    temp = { "value": ip, "type": "CustomIp"}
                    d = {'addition_type': 'OR', 'object': temp}
                    self.applications['to']['customs'].append(d)


        return self.applications

    def get_site_id(self, serial_number):
        """
        Function to return a site-id that respective cloud-port is attached to
        :param serial_number: cp-serial number
        :return: site-id
        """
        Logger.info('getting a site info ')
        url = self._cloudstation_url + '/api/v2/lavelle/roles/get_permit_data?obj=cloudports'
        res = self.get(url)
        if res.status_code == 200:
            res = res.json()
            Logger.info(res)
            for cloudport in res['data']['cloudports']:
                if cloudport['serial_no'] == str(serial_number):
                    Logger.info('site-id of serial number {} is {}'.format(serial_number,cloudport['site_id']))
                    return cloudport['site_id']

        else:
            return False

    def get_network_group_details(self):
        """
        Function to get network id details of default network group
        :return: details network along with network ID
        """
        url = self._base_url + 'policies/policy_objects'
        res = self.get(url).json()
        details = dict()
        for each in res['policy_objects']['network_groups']:
            if 'Default_NetworkGroup' in each['name']:
                details['Default-network-gp-id'] = each['id']
            if each['name'] == 'Internet':
                details['Internet'] = each['id']
            if 'Enterprise Network' in each['name']:
                details['Enterprise Network'] = each['id']

        return details

    def get_network_group_info(self,id):
        """
        Function to get the details of the network group
        :param:ID: id of the network-group
        :return:Network-group details like id,hex-id,name...
        """
        url = self._cloudstation_url + '/api/v2/lavelle/roles/get_permit_data?obj=network_groups'
        res = self.get(url=url).json()
        for network_group in res['data']['network_groups']:
            if network_group['id'] == id:
                return network_group

    def verify_gateway_status(self):
        """
        Function to verify default network-group network status
        :return: true/false
        """
        url = self._base_url + 'network_groups/summary'
        res = self.get(url).json()
        Logger.info(res)

        for data in res['data']:
            if data['cp_status'] != 'online':
                Logger.info('CPE status not online re-checking after 30 sec')
                raise Exception
            else:
                pass
        Logger.info('CPE status online verified!!!')
        return True

    def get_cp_uuid(self,serial_number,model_number='LN00030015',node_category='CPE',instance_type='HW-USFF'):
        """
        Function to generate uuid of CPE based on serial number
        :param serial_number: 6 digits serial numer
        :return: 12 digit CP uuid
        """
        return ln_gen_uuid.generate_uuid(
            serial_number=serial_number, model_number = model_number,
            node_category=node_category,instance_type=instance_type)

    def get_interface_details(self,cloudport_id,interface_name):
        """
        function to return the wan interface ID
        :param cloudport_id: network-appliance/cloudport ID
        :param interface_name: interface name (wan0/wan1/lan0)
        :return: interface-details
        """
        url = self._base_url + 'cloudports/{}/interfaces'.format(cloudport_id)
        response = self.get(url).json()
        for interface in response['interfaces']:
            if interface['name'] == interface_name:
                return interface

        return False

    def get_cloudport_details(self,serial_number):
        """
        Function to get the details of cloudport (network-appliance)
        :param serial_number: 6-digit serial number associated with CP while creating the network appliance
        :return: cloudport details/network appliance details
        """
        url = self._cloudstation_url + '/api/v2/lavelle/roles/get_permit_data?obj=cloudports'
        response = self.get(url)
        if response.status_code == 200:
            data = response.json()
            for cp in data['data']['cloudports']:
                if cp['serial_no'] == str(serial_number):
                    return cp
            Logger.info('network appliance w.r.t serial number {} not found '.format(serial_number))
            return False
        else:
            Logger.info('could not hit 200 response code reason {}'.format(response.json()))
            return False

    def get_default_network_proxy_id(self):
        """
        Function to get a default Network proxy Internet
        :return: default network proxy ID
        """
        Logger.info('Getting the default ID of network proxy internet')
        url = self._cloudstation_url + '/api/v2/lavelle/dataplane_feature'
        res = self.get(url=url).json()
        Logger.info(res)
        for each in res['data']['dataplane_feature_profiles']:
            if 'Default' in each['name']:
                return each['id']

    def get_default_threat_protection_id(self):
        """
        Function to get a default threat protection profile
        :return: default threat protection profile id
        """
        Logger.info('getting the default threat protection profile id')
        url = self._base_url +'security_profiles'
        res = self.get(url=url).json()
        Logger.info(res)
        for each in res['security_profiles']:
            if 'Default' in each['name']:
                return each['id']

    def get_default_vpn_profile_id(self):
        """
        Function to get a default VPN profile ID
        :return:  default vpn profile ID
        """
        Logger.info('getting a default VPN profile ID')
        url = self._base_url +'vpn_profiles'
        res = self.get(url=url).json()
        Logger.info(res)
        for each in res['vpn_profiles']:
            if 'Default' in each['name']:
                return each['id']

    def get_subnet_id(self,ip_names):
        """
        Function to get a subnet id available in the CS based on the allocated ip-address
        :param ip_names: list of ip-names/address to match the subnet
        :return: subnet-ID
        """
        Logger.info('Getting available subnets ')
        url = self._base_url +'network_groups/available_lan_subnets'
        res = self.get(url=url).json()
        Logger.info(res)
        subnet_id = []

        for subnet in res['available_lan_subnets']:
            for ip in ip_names:
                if subnet['subnet'] == ip:
                    subnet_id.append(subnet['id'])

        return subnet_id

    def get_default_network_location_profile_id(self):
        """
        Funtion to get a default network location profile ID
        :return: default network location ID
        """
        Logger.info('Getting a default network location ID')
        url = self._cloudstation_url + '/api/v2/lavelle/roles/get_permit_data?obj=profiles'
        res = self.get(url=url).json()
        Logger.info(res)
        for each in res['data']['profiles']:
            if 'Default' in each['name']:
                return each

    def update_network_location_details(self,payload):
        """
        Function to update the network location
        :param id: network-location id
        :param payload: altered data to be update
        :return: True/False
        """
        Logger.info('Updating the network-location template')
        Logger.info('altering paylaod ', payload)
        url = self._base_url + 'profile/{}'.format(payload['id'])

        res = self.put(url = url,data = payload)

        if res.status_code != 200:
            Logger.info('Failed to update network location ',id)
            Logger.info(res.json())
            return False

        Logger.info('Updated networ location succesfully')
        Logger.info(res.json())
        return True

    def create_network_group(self,**network_details):
        """
        Function to create a network group in CS
        :param network_details:
        :return:
        """
        if 'next_hop' not in network_details.keys():
            network_details['next_hop'] = 'hub'

        if 'encapsulation' not in network_details.keys():
            network_details['encapsulation'] = 'lntun'

        if 'network_proxy_id' not in network_details.keys():
            network_details['network_proxy_id'] = self.get_default_network_proxy_id()

        if 'threat_protection_profile' not in network_details.keys():
            network_details['threat_protection_profile'] = self.get_default_threat_protection_id()

        if 'vpn_profile_id' not in network_details.keys():
            network_details['vpn_profile_id'] = self.get_default_vpn_profile_id()

        self.data_map['__ng-name__'] = network_details.get('ng_name')
        self.data_map['__encap-method__'] = network_details.get('encapsulation')
        self.data_map['__next-hop__'] = network_details.get('next_hop')
        self.data_map[1111] = network_details.get('network_proxy_id')
        self.data_map[2222] = network_details.get('threat_protection_profile')
        self.data_map[3333] = network_details.get('vpn_profile_id')
        payload = stationbag['network-group']['payload']
        payload['user'] = self.get_user_details()
        payload['subnets'] = network_details.get('addresses')

        payload = self._change_data(json.dumps(payload))
        url = self._base_url + 'network_groups/'

        response = self.post(url=url,data = payload)

        if response.status_code != 200:
            err_msg = 'Failed to create a new network group {} '.format(response.json())
            Logger.info(err_msg)
            return False
        else:
            Logger.info('New network group {} created successfully '.format(network_details.get('ng_name')))
            Logger.info(response.json())
            return response.json()

    def create_dns_proxy(self,**dns_details):
        """
        Function to add DNS entries under network protocol templates in CS
        :param dns_details: dns-details
        :return:
        """
        Logger.info('Adding DNS deatils in cloudstation')
        self.data_map['__name__'] = dns_details.get('name')
        self.data_map['__primary-server__'] = dns_details.get('primary_server')
        self.data_map['__secondary-server__'] = dns_details.get('secondary_server')

        payload = stationbag['dns-proxies']['payload']
        for server in dns_details.get('server_details'):
            payload['dns_proxy_entries'].append(server)

        payload = self._change_data(json.dumps(payload))
        url = self._base_url + 'dns_proxies'

        response = self.post(url=url, data = payload)

        if response.status_code != 200:
            Logger.info('Failed to add dns-proxy entries to cloud-station')
            Logger.info(response.json())
            return False

        else:
            Logger.info('Added DNS-proxies to cloud-station')
            Logger.info(response.json())
            return response.json()

    def create_ssh_object(self,ip,username='lavelle',password='lavelle@3163'):
        """
        Function to create a ssh object for any machine
        :param ip: str - machine-ip address
        :param username: str - username - default=lavelle
        :param password: str - password - default=lavelle@3163
        :return: ssh object
        """
        device = {
            "ip": ip,
            "username": username,
            "password": password
        }
        return ConnectionHandler(**device)

if __name__=="__main__":
    basepath = os.path.join(os.path.dirname(__file__), '../data_bags/')
    bp = os.path.join(basepath, "cloudstation.json")

    with open(bp) as fp:
        linux_hash = json.load(fp)

    a = CloudOperations(linux_hash, act_as='master')
    customer_details = {
        "customer_name": "one",
        "url_prefix": "qa-team",
    }
    a.create_customer(**customer_details)
