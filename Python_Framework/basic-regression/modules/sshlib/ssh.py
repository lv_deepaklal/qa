from netmiko import ConnectHandler
import time
import sys
sys.path.append('../')
from datetime import datetime
from logging import getLogger
from netmiko import BaseConnection
import re

Logger = getLogger(name=__name__)


def _retry_on_exception(func, max_tries=3, retry_sleep=30):
    """Mthod to retry on Exception
    :param max_tries: Integer , 3 - by default
    :return:
    """

    def wrapper(*args, **kwargs):
        for i in range(max_tries):
            try:
                return func(*args, **kwargs)
                break
            except Exception as err:
                msg = "Got Exception:{}, retrying attempt: {}".format(err, i + 1)
                print("RETRYING", msg)
                if i + 1 == max_tries:
                    raise Exception("Got Exception in the last attempt: {}".format(err))
                else:
                    time.sleep(retry_sleep)
                    continue

    return wrapper

class ConnectionHandler(BaseConnection):

    def __init__(self,**kwargs):
        self.required_params = dict()
        self.required_params = kwargs
        data = self._verify_params(**kwargs)
        Logger.info(data)

        
        super(ConnectionHandler,self).__init__(**data)
        self.set_base_prompt(alt_prompt_terminator="$")


    def _verify_params(self, **kwargs):
        """
        Private function to fetch all the required params for the ssh connections
        :param kwargs:
        :return: True/False
        """
        if 'device_type' not in kwargs:
            kwargs['device_type'] = 'linux'
        if 'ip' not in kwargs:
            Logger.fatal('ip address missing in header to connect')
            raise Exception('ip address missing to connect')
        if 'username' not in kwargs:
            Logger.fatal('username missing in header to connect')
            raise Exception('username missing')
        if 'password' not in kwargs:
            Logger.fatal('password required to connect')
            raise Exception('password missing')
        if 'secret' not in kwargs:
            kwargs['secret'] = kwargs.get('password')

        return kwargs

    def _retry_on_exception(func, max_tries=3, retry_sleep=30):
        """Mthod to retry on Exception
        :param max_tries: Integer , 3 - by default
        :return:
        """

        def wrapper(*args, **kwargs):
            for i in range(max_tries):
                try:
                    return func(*args, **kwargs)
                    break
                except Exception as err:
                    msg = "Got Exception:{}, retrying attempt: {}".format(err, i + 1)
                    print("RETRYING",msg)
                    if i + 1 == max_tries:
                        raise Exception("Got Exception in the last attempt: {}".format(err))
                    else:
                        time.sleep(retry_sleep)
                        continue

        return wrapper

    def run_cmd(self, command, expect_string=None, reset_base_prompt=True):
        """
        Use this function to run your command at one go.
        :param command: command to be executed
        :param expect_string:expected prompt terminus after command execution (optional)
        :param reset_base_prompt: set True to call set_base_prompt after cmd.
        :return:
        """

        output = super(ConnectionHandler, self).send_command(command,expect_string,reset_base_prompt)
        return output

    def switch_to_root(self):
        """
        Function to switch to root user of a system
        :return:
        """
        self.run_cmd(command='sudo su', expect_string='password')
        output = self.run_cmd(self.required_params.get('password'))
        return output

    def verify_incoming_traffic(self,interface_name):
        """
        Function to get the incoming lan traffic in the specified port
        :param interface_name: port_name/interface-name
        :return: String list of 5 incoming traffic rate
        """
        cmd = 'ifstat -b -i {} 1 5| tail -5'.format(interface_name) + '|awk \'{print $1}\' '
        output = self.run_cmd(cmd)
        return str(output).split("\n")

    def send_traffic(self, client_ip, rate, time_limit):
        """
        Function to send traffic via iperf
        :param client_ip: client ip address
        :param rate: transfer rate in MBPS ()
        :param time_limit: how long the traafic in seconds
        :return:
        """
        cmd = 'nohup iperf -c {} -b{}m -i1 -t{} &'.format(client_ip,rate,time_limit)
        Logger.info(cmd)
        self.run_cmd(cmd)

    def get_tcp_dump_data(self,cmd,number_of_packets):
        """
        Function to get a tcp dump logs inside a device
        :param cmd: TCP dump command to fire in a machine
        :param INT number_of_packets: Number of packets wish to trace
        :return: Logs of tcpdump based on number_of_packets mentioned
        """
        if number_of_packets < 15:
            command = cmd + ' -c {}'.format(number_of_packets)
            output = self.run_cmd(command)
            capture = re.search("capture size \d+ bytes\s(.*)\d+ packets captured", str(output),re.DOTALL)
            Logger.info(capture)
            return capture.group(1)
        else:
            Logger.info('Requested number of packets is greater. maximum packets can be traced is 15 given is {}'.format(number_of_packets))
            return False

    @_retry_on_exception
    def verify_nat_config_pushed(self,verify_str):
        """
        Function to verify whether nat policy is pushed to CP
        :param verify_str: string to verify in the output
        :return: True/False
        """
        cmd = 'ip netns exec ns-svc-natfw iptables -S -t nat'
        output = self.run_cmd(cmd)
        Logger.info(output)
        if verify_str in output:
            return True
        else:
            raise Exception('config not pushed to cp yet')