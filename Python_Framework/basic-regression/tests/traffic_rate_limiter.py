import os, sys, time, re
import json
import datetime
from datetime import timedelta, datetime
from functools import wraps

import random
import sys

sys.path.append('../')
from modules.sshlib.ssh import ConnectionHandler
from custom_xmlrunner import setup_module_helper, main, CustomUnitTestCase
from modules.CloudOperations import CloudOperations



def setUpModule():
    global BasePath, Logger, resource_json_hash, cloudObj, resource_json_hash

    global machine_hash, cloudstation, trl_policy_json

    BasePath = os.path.abspath(os.path.dirname(__file__))
    Logger = setup_module_helper(__file__, alarm_timeout=2400)

    BasePath = os.path.join(os.path.dirname(__file__), '../data_bags/')

    resource_json_hash = {
        "cloudstation-config": os.path.join(BasePath, 'cloudstation.json'),
        "machine-config": os.path.join(BasePath, 'machine-config.json')
    }

    with open(resource_json_hash['machine-config']) as fp:
        machine_hash = json.load(fp)

    with open(resource_json_hash['cloudstation-config']) as fp:
        cloudstation = json.load(fp)

    bp = os.path.join(os.path.dirname(__file__), '../data_bags/script_data_bags/')
    resource_json_hash = {
        "trl_policy": os.path.join(bp, "trl_policy.json"),
    }
    with open(resource_json_hash['trl_policy'], 'r') as fr:
        trl_policy_json = json.load(fr)

    _populate_other_params()


def _populate_other_params():
    global cp_object, cloudObj
    device = {
        'device_type': 'linux',
        'ip': '172.11.35.21',
        'username': 'lavelle',
        'password': 'lavelle@3163',
    }
    cp_object = ConnectionHandler(**device)
    Logger.info(cp_object)
    cloudObj = CloudOperations()


class TRL_policy(CustomUnitTestCase):
    data_map = {}

    def setup(self, json_file=None, data=None):
        pass

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        # Define policy configuartion data
        self = super(TRL_policy, cls).tearDownClass()
        pass

    @classmethod
    def _change_data(cls, json_data):
        """utility method to change/replace values in the json_data
        :param json_data: json dict to modify
        :return:
        """
        for k, v in cls.data_map.items():
            json_data = json_data.replace(str(k), str(v))
        return json_data

    @classmethod
    def timeit(func):
        """This decorator prints the execution time for the decorated function."""

        @wraps(func)
        def wrapper(*args, **kwargs):
            hash = random.getrandbits(128)
            start = time.time()
            Logger.debug("{} : {} start: at {}".format(hash, func.__name__, datetime.datetime.now()))
            result = func(*args, **kwargs)
            Logger.debug("{} : {} End: at {}".format(hash, func.__name__, datetime.datetime.now()))
            end = time.time()
            Logger.debug("{} : {} ran in {}s".format(hash, func.__name__, round(end - start, 2)))
            return result

        return wrapper

    def _retry_on_exception(func, max_tries=3, retry_sleep=0.3):
        """Mthod to retry on Exception
        :param max_tries: Integer , 3 - by default
        :return:
        """

        def wrapper(*args, **kwargs):
            for i in range(max_tries):
                try:
                    return func(*args, **kwargs)
                    break
                except Exception as err:
                    msg = "Got Exception:{}, retrying attempt: {}".format(err, i + 1)
                    print(msg)
                    if i + 1 == max_tries:
                        raise Exception("Got Exception in the last attempt: {}".format(err))
                    else:
                        time.sleep(retry_sleep)
                        continue

        return wrapper


    def generate_lb_payload(self,policy_details,**kwargs):
        """
        Function to generate lb policy payload data
        :param policy_details: similar to policy-details as in CS_UI
        :param action_details: similar to action-details as in CS-UI
        :param kwargs: application_classifier details in dict format
        :return: generated lb_policy payload
        """
        payload = trl_policy_json
        if 'application_category' in kwargs.keys():
            for app in kwargs['application_category'].values():
                d = {'addition_type': 'OR', 'object': app}
                payload['both']['application_categories'].append(d)

        if 'application' in kwargs.keys():
            for app in kwargs['application'].values():
                d = {'addition_type': 'OR', 'object': app}
                payload['both']['applications'].append(d)

        if 'custom' in kwargs.keys():
            for app in kwargs['custom'].values():
                payload['both']['customs'].append(app)

        if 'from' in kwargs.keys():
            pass

        if 'to' in kwargs.keys():
            if 'lan_subnets' in kwargs['to']:
                pass

            if 'custom' in kwargs['to']:
                for ip in kwargs['to']['custom']:
                    temp = { "value": ip, "type": "CustomIp"}
                    d = {'addition_type': 'OR', 'object': temp}
                    payload['to']['customs'].append(d)

        self.data_map[9999] = policy_details.get('destination_id')
        self.data_map[8888] = policy_details.get('source_id')
        self.data_map['__policy-name__'] = policy_details.get('policy_name')
        self.data_map[1111] = policy_details.get('rate')

        payload = self._change_data(json.dumps(payload))

        return payload

    def add_trl_policy(self,policy_details, **application_classifiers):
        """
        Fuction to add lb policy to cloudstation
        :param payload:
        :return:
        """
        data = cloudObj.get_app_classifier_details(app_names=application_classifiers)

        payload = self.generate_lb_payload(policy_details, **data)


        url = cloudObj._base_url + 'traffic_rate_limiter'

        response = cloudObj.post(url=url,data = payload)

        if response.status_code != 200:
            err_msg = 'Failed to add TRL policy to cloud-station: API response {}'.format(response.json())
            Logger.info(err_msg)
            raise Exception(err_msg)

        Logger.info('Policy added {} '.format(response.json()))
        return response.json()

    def attach_policy(self,policy_id,site_id):
        """
        Function to attach lb policy to cloud-station
        :param policy_id:
        :return:
        """
        url = cloudObj._base_url + 'traffic_rate_limiter/{}/attach_policy'.format(policy_id)
        payload = {
            "attached_to_type":"Site",
            "attached_to_id":str(site_id)
        }

        response = cloudObj.post(url=url,data=json.dumps(payload))
        if response.status_code != 200:
            err_msg='Failed to attach policy Reason {}'.format(response.json())
            Logger.fatal(err_msg)
            return False
        Logger.info('Attached policy {}'.format(response.json()))
        return response.json()

    def de_attach_policy(self,policy_id,site_id):
        """
        Function to de-attach the trl policy form cloudport
        :param policy_id: policy-id
        :param site_id: site_id-id
        :return: de-attachs policy
        """
        url = cloudObj._base_url + 'traffic_rate_limiter/{}/traffic_rate_limiter_relations'.format(policy_id)
        payload = {
            "object_type": "Site",
            "object_id": str(site_id)
        }
        Logger.info(url)
        Logger.info(payload)
        response = cloudObj.post(url=url, data=json.dumps(payload))
        if response.status_code != 200:
            err_msg = 'Failed to de-attach policy Reason {}'.format(response.json())
            Logger.fatal(err_msg)
            return False
        Logger.info('De-attached policy {}'.format(response.json()))
        return response.json()

    def delete_trl_policy(self,policy_id):
        """
        Function to delete the trl policy
        :param policy_id: policy_id
        :return: True/false
        """
        Logger.info('Deleting the trl policy ')
        url = cloudObj._base_url + 'traffic_rate_limiter/{}'.format(policy_id)
        res = cloudObj.delete(url = url)
        Logger.info(res.json())
        if res.status_code!=200:
            Logger.info('failed to delete the policy')
            return False
        Logger.info('Deleted policy')
        return True

    def test_trl_policy_on_hub(self):
        """
        Testcase to attach trl policy on hub and verify traffic
        :return:
        """
        network_id = cloudObj.get_network_group_details()
        serial_number = machine_hash['cp-details'][0]['serial-number']
        policy_details = {
            "policy_name": 'trl_policy_demo_{}'.format(random.randint(1, 100)),
            "destination_id": int(network_id['Default-network-gp-id']),
            "source_id": int(network_id['Default-network-gp-id']),
            "rate": 1000
        }

        #custom ip address to be taken dynamically from master json
        ip = '10.117.51.11'
        application_classifiers = {
            "to": {
                'custom': ["10.117.51.11"]
            }
        }

        self.write_step('Adding new Traffic rate limiter policy ')
        response = self.add_trl_policy(policy_details, **application_classifiers)

        site_id = cloudObj.get_site_id(serial_number=serial_number)

        self.write_step('Attaching new policy to cloud-station')
        res = self.attach_policy(policy_id=response['data']['traffic_rate_limiter']['id'], site_id=site_id)
        Logger.info('waiting for 120 sec to push config to CP')
        time.sleep(120)
        try:

            if res['message'] == 'success':
                # send traffic and verify traffic
                self.write_step('Sending traffic from hub to spoke')
                cp_object.send_traffic(client_ip=ip, rate=2, time_limit=120)
                time.sleep(5)
                self.write_step('verifying incoming traffic in spoke')
                device = {
                    'device_type': 'linux',
                    'ip': '172.11.35.22',
                    'username': 'lavelle',
                    'password': 'lavelle@3163',
                }
                cp_host_object = ConnectionHandler(**device)
                data = cp_host_object.verify_incoming_traffic(interface_name='eth0')
                Logger.info(
                    'Last 5 reading of incoming traffic in device {} on port {} '.format(device['device_type'], 'eth0'))
                for kb in data:
                    if float(kb) > policy_details['rate']:
                        err_msg = 'Traffic rate is more than {} '.format(policy_details['rate'])
                        Logger.info(err_msg)
                        raise Exception(err_msg)

            else:
                err_msg = 'Failed to attach traffic rate policy to site'
                Logger.info(err_msg)
                raise Exception(err_msg)
        finally:
            Logger.info('Cleaning up ')
            Logger.info('De-attaching the policy')
            self.de_attach_policy(policy_id=response['data']['traffic_rate_limiter']['id'], site_id=site_id)
            Logger.info('Deleting the policy')
            self.delete_trl_policy(policy_id=response['data']['traffic_rate_limiter']['id'])


if __name__ == '__main__':
    main()