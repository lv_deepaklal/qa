#!/bin/bash

SANITY="yes"
echo "SANITY is ${SANITY}"

echo "###################################"
echo "current working dir is "
echo `pwd`
echo "###################################"

#setup="python3 setup.py "
#echo ${setup}
#${setup}
#if [ $? -ne 0 ]
#then
#    echo '[FAILED] setup failed so exiting...'
#    exit $?
#fi
#onboard="python3 onboard.py"
#
#echo ${onboard}
#${onboard}
#if [ $? -ne 0 ]
#then
#    echo '[FAILED] onboard failed so exiting...(after running offboard.py)'
#    exit $?
#fi

if [[ $SANITY == "yes" ]];
then
    echo "\n ###\n##### Running SANITY TestSuite\n###\n"



    #python3 thunder_cleanup_upgrade.py
    #python3 registration.py TestRegister.test_SequenceRegisterAndConfig

else
    echo "\n ###\n##### Running Regression TestSuite\n###\n"
    ../custom_xmlrunner/suite_runner --suite_as_string firewall.py,lbpolicy.py,traffic_rate_limiter.py,nat_policy.py,forwarding.py,inter_network_policy.py
fi
