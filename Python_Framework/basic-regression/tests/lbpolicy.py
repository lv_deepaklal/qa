import os, sys, time, re
import json
import datetime
from datetime import timedelta, datetime
from functools import wraps

import requests
import random
import sys

sys.path.append('../')
from modules.StationAPI.api import ConfigManager
from modules.sshlib.ssh import ConnectionHandler
from custom_xmlrunner import setup_module_helper, main, CustomUnitTestCase
from modules.CloudOperations import CloudOperations
from utils import ln_gen_uuid
# from requests_toolbelt.utils import dump


def setUpModule():
    global BasePath, Logger, resource_json_hash, cloudObj, resource_json_hash

    global machine_hash, cloudstation, lbpolicy_json

    BasePath = os.path.abspath(os.path.dirname(__file__))
    Logger = setup_module_helper(__file__, alarm_timeout=2400)

    BasePath = os.path.join(os.path.dirname(__file__), '../data_bags/')

    resource_json_hash = {
        "cloudstation-config": os.path.join(BasePath, 'cloudstation.json'),
        "machine-config": os.path.join(BasePath, 'machine-config.json')
    }

    with open(resource_json_hash['machine-config']) as fp:
        machine_hash = json.load(fp)

    with open(resource_json_hash['cloudstation-config']) as fp:
        cloudstation = json.load(fp)

    bp = os.path.join(os.path.dirname(__file__), '../data_bags/script_data_bags/')
    resource_json_hash = {
        "lbpolicy": os.path.join(bp, "lbpolicy.json"),
    }
    with open(resource_json_hash['lbpolicy'], 'r') as fr:
        lbpolicy_json = json.load(fr)

    _populate_other_params()


def _populate_other_params():
    global cp_object, cloudObj
    device = {
        'device_type': 'linux',
        'ip': '172.11.35.21',
        'username': 'lavelle',
        'password': 'lavelle@3163',
    }
    cp_object = ConnectionHandler(**device)
    Logger.info(cp_object)
    cloudObj = CloudOperations()


class Lbpolicy(CustomUnitTestCase):
    data_map = {}

    def setup(self, json_file=None, data=None):
        pass

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        # Define policy configuartion data
        self = super(Lbpolicy, cls).tearDownClass()
        pass

    @classmethod
    def _change_data(cls, json_data):
        """utility method to change/replace values in the json_data
        :param json_data: json dict to modify
        :return:
        """
        for k, v in cls.data_map.items():
            json_data = json_data.replace(str(k), str(v))
        return json_data

    @classmethod
    def timeit(func):
        """This decorator prints the execution time for the decorated function."""

        @wraps(func)
        def wrapper(*args, **kwargs):
            hash = random.getrandbits(128)
            start = time.time()
            Logger.debug("{} : {} start: at {}".format(hash, func.__name__, datetime.datetime.now()))
            result = func(*args, **kwargs)
            Logger.debug("{} : {} End: at {}".format(hash, func.__name__, datetime.datetime.now()))
            end = time.time()
            Logger.debug("{} : {} ran in {}s".format(hash, func.__name__, round(end - start, 2)))
            return result

        return wrapper

    def _retry_on_exception(func, max_tries=3, retry_sleep=0.3):
        """Mthod to retry on Exception
        :param max_tries: Integer , 3 - by default
        :return:
        """

        def wrapper(*args, **kwargs):
            for i in range(max_tries):
                try:
                    return func(*args, **kwargs)
                    break
                except Exception as err:
                    msg = "Got Exception:{}, retrying attempt: {}".format(err, i + 1)
                    print(msg)
                    if i + 1 == max_tries:
                        raise Exception("Got Exception in the last attempt: {}".format(err))
                    else:
                        time.sleep(retry_sleep)
                        continue

        return wrapper

    def verify_wan_interfaces(self,cp_id):
        """
        Function to verify available wan interfaces are more than 1 and to verify if wan interference are in good state
        :param cp_id: cloudport_id
        :return: True/false
        """
        url = cloudObj._base_url + 'cloudports/{}/interfaces'.format(cp_id)
        response = cloudObj.get(url)
        if response.status_code == 200:
            response = response.json()
            Logger.info("interface details are",response)
            a = [ 'wan' for interface in response['interfaces'] if interface['role'] == 'wan']
            if len(a) < 2:
                Logger.info('Not enough wan interfaces configured on given cloudport cannot proceed further')
                return False
            else:
                for interface in response['interfaces']:
                    Logger.info('interface details are {}'.format(interface))
                    if interface['role'] == 'wan':
                        if interface['status'] != 'up' or interface['cloudport_link']['enable'] != True:
                            Logger.info('{} interface status down or its not enabled , check logs '.format(interface['']))
                            return False

            return True
        else:
            err_msg = 'Failed to get interface details for cloud-port ID {} : reason {} '.format(cp_id,response.json())
            raise Exception(err_msg)

    def get_cloudport_details(self,serial_number):
        """
        Function to get the details of cloudport (network-appliance)
        :param serial_number: 6-digit serial number associated with CP while creating the network appliance
        :return: cloudport details/network appliance details
        """
        url = cloudObj._cloudstation_url + '/api/v2/lavelle/roles/get_permit_data?obj=cloudports'
        response = cloudObj.get(url)
        if response.status_code == 200:
            data = response.json()
            for cp in data['data']['cloudports']:
                if cp['serial_no'] == str(serial_number):
                    return cp
            Logger.info('network appliance w.r.t serial number {} not found '.format(serial_number))
            return False
        else:
            Logger.info('could not hit 200 response code reason {}'.format(response.json()))
            return False

    def get_wan_interface_id(self,cloudport_id,interface_name):
        """
        function to return the wan interface ID
        :param cloudport_id: network-appliance/cloudport ID
        :param interface_name: interface name (wan0/wan1)
        :return: interface-id
        """
        url = cloudObj._base_url + 'cloudports/{}/interfaces'.format(cloudport_id)
        response = cloudObj.get(url).json()
        for interface in response['interfaces']:
            if interface['name'] == interface_name:
                return interface


    def generate_lb_payload(self,policy_details,action_details,**kwargs):
        """
        Function to generate lb policy payload data
        :param policy_details: similar to policy-details as in CS_UI
        :param action_details: similar to action-details as in CS-UI
        :param kwargs: application_classifier details in dict format
        :return: generated lb_policy payload
        """
        payload = lbpolicy_json
        if 'application_category' in kwargs.keys():
            for app in kwargs['application_category'].values():
                d = {'addition_type': 'OR', 'object': app}
                payload['both']['application_categories'].append(d)

        if 'application' in kwargs.keys():
            for app in kwargs['application'].values():
                d = {'addition_type': 'OR', 'object': app}
                payload['both']['applications'].append(d)

        if 'custom' in kwargs.keys():
            for app in kwargs['custom'].values():
                payload['both']['customs'].append(app)

        cloudport_details = action_details.get('cp_details')
        for wan in action_details.get('wan_interfaces'):
            interface = self.get_wan_interface_id(cloudport_details['id'],wan)
            d = {
                "interface_id": interface['id'],
                "interface": {
                    "name": wan,
                    "cpe_name": cloudport_details['name']
                },
                "enabled": True,
                "priority": random.randint(1,10)
            }
            payload['lb_action'].append(d)

        self.data_map[9999] = policy_details.get('destination_id')
        self.data_map[8888] = policy_details.get('source_id')
        self.data_map['__policy-name__'] = policy_details.get('policy_name')
        self.data_map['__cloud-port-id__'] = cloudport_details['id']

        payload = self._change_data(json.dumps(payload))

        return payload

    def add_lb_policy(self,policy_details, action_details, **application_classifiers):
        """
        Fuction to add lb policy to cloudstation
        :param payload:
        :return:
        """
        data = cloudObj.get_app_classifier_details(app_names=application_classifiers)

        payload = self.generate_lb_payload(policy_details, action_details, **data)

        url = cloudObj._base_url + 'lb_policy'

        response = cloudObj.post(url=url,data = payload)

        if response.status_code != 200:
            err_msg = 'Failed to add lb policy to cloud-station: API response {}'.format(response.json())
            Logger.info(err_msg)
            raise Exception(err_msg)

        Logger.info('Policy added {} '.format(response.json()))
        return response.json()

    def attach_policy(self,policy_id,cloudport_id):
        """
        Function to attach lb policy to cloud-station
        :param policy_id:
        :return:
        """
        url = cloudObj._base_url + 'lb_policy/{}/attach_policy'.format(policy_id)
        payload = {
            "attached_to_type":"Cloudport",
            "attached_to_id":str(cloudport_id)
        }
        Logger.info(url)
        Logger.info(payload)
        response = cloudObj.post(url=url,data=json.dumps(payload))
        if response.status_code != 200:
            err_msg='Failed to attach policy Reason {}'.format(response.json())
            Logger.fatal(err_msg)
            return False
        Logger.info('Attached policy {}'.format(response.json()))
        return response.json()

    def de_attach_policy(self,policy_id,cloudport_id):
        """
        Function to de-attach the lb policy form cloudport
        :param policy_id: policy-id
        :param cloudport_id: cloudport-id
        :return: de-attachs policy
        """
        url = cloudObj._base_url + 'lb_policy/{}/lb_policy_relations'.format(policy_id)
        payload = {
            "object_type": "Cloudport",
            "object_id": str(cloudport_id)
        }
        Logger.info(url)
        Logger.info(payload)
        response = cloudObj.post(url=url, data=json.dumps(payload))
        if response.status_code != 200:
            err_msg = 'Failed to de-attach policy Reason {}'.format(response.json())
            Logger.fatal(err_msg)
            return False
        Logger.info('De-attached policy {}'.format(response.json()))
        return response.json()

    def delete_lb_policy(self,policy_id):
        """
        Function to delete a lb_policy
        :param policy_id: policy_id
        :return: True/false
        """
        Logger.info('Deleting a policy {}'.format(policy_id))
        url =  cloudObj._base_url + 'lb_policy/{}'.format(policy_id)
        data = cloudObj.delete(url=url)
        Logger.info(data.json())
        if data.status_code !=200:
            return False
        return True

    def get_lb_policy_details(self,policy_id):
        """
        Function to get the LB policy details
        :param policy_id: id of the existing policy
        :return: policy_details
        """
        url = cloudObj._base_url + 'lb_policy/{}'.format(policy_id)
        response = cloudObj.get(url)
        Logger.info(response.json())

        if response.status_code == 200:
            return response.json()

        return False

    def get_wan_link_details(self,policy_details,cp_id):
        """
        Function to get the wan ip and wan interface details based on policy
        :param policy_details: attached policy details
        :return: dict(wan-ip,interface_name)
        """
        high_priority = max([policy['priority'] for policy in policy_details['lb_policy_actions']])
        a = {
            "wan-ip": "",
            "wan-interface": ""
        }
        for policy in policy_details['lb_policy_actions']:
            if policy['priority'] == high_priority:
                a['wan-interface'] = policy['interface']['name']

        data = self.get_wan_interface_id(cp_id,a['wan-interface'])
        Logger.info(data)
        a['wan-ip'] = data['interface_addresses'][0]['ip']

        return a

    def send_and_verify_traffic(self,wan_ip,wan_interface):

        cp_obj = cloudObj.create_ssh_object(ip=wan_ip)

        cmd = 'ifconfig {}'.format(wan_interface) + ' | awk \'/TX packets/ {print $2}\' '
        Logger.info('Running command : {}'.format(cmd))
        start = time.time()
        TX_before = cp_obj.run_cmd(cmd)
        TX_before = str(TX_before).split(":")[1]


        Logger.info('Running traffic')
        output = cp_object.run_cmd('wget https://youtube.com')
        Logger.info(output)

        TX_after = cp_obj.run_cmd(cmd)
        TX_after = str(TX_after).split(":")[1]

        done = time.time()
        elapsed = done - start

        result = (int(TX_after)-(9*int(elapsed) + 0.8*int(elapsed))) - int(TX_before)
        Logger.info('packets sent Approx = {} '.format(result))
        if (result > 30):
            return True
        else:
            return False

    def test_lb_policy(self):
        """
        Testcase to attach lb policy to hub, send and verify link traffic
        """

        serial_number = machine_hash['cp-details'][0]['serial-number']
        Logger.info('Getting cloud-port details of hub')
        cp_details = cloudObj.get_cloudport_details(serial_number=serial_number)
        Logger.info('CP details are {}'.format(cp_details))
        self.write_step('Verifying more than one wan interfaces exists')
        if self.verify_wan_interfaces(cp_details['id']):
            Logger.info('verify passed more than one wan interface exists')
            network_id = cloudObj.get_network_group_details()
            policy_details = {
                "policy_name": 'lb_policy_demo_{}'.format(random.randint(1,100)),
                "destination_id": int(network_id['Internet']),
                "source_id": int(network_id['Default-network-gp-id'])
            }

            application_classifiers = {
                "application": ["youtube"]
            }

            action_details = {
                "cp_details" : cp_details,
                "wan_interfaces": ["wan0","wan1"]
            }
            self.write_step('Adding new LB policy ')
            response = self.add_lb_policy(policy_details, action_details, **application_classifiers)

            try:
                self.write_step('Attaching LB policy to cloud-port')
                self.attach_policy(policy_id=response['data']['lb_policy']['id'],cloudport_id=cp_details['id'])

                Logger.info('Get high priority wan interface details')
                data = self.get_lb_policy_details(policy_id=response['data']['lb_policy']['id'])
                a = self.get_wan_link_details(data,cp_details['id'])

                self.write_step('Sending and Verifying the traffic')
                status = self.send_and_verify_traffic(wan_ip=a['wan-ip'],wan_interface=a['wan-interface'])
                self.assertEqual(True,status)
            finally:
                Logger.info('Feature clean-up ')
                Logger.info('De-attaching policy')
                self.de_attach_policy(policy_id=response['data']['lb_policy']['id'],cloudport_id=cp_details['id'])
                Logger.info('Deleting policy')
                self.delete_lb_policy(policy_id=response['data']['lb_policy']['id'])
        else:
            err_msg = 'Network-appliance or cloud-port does not have min. required wan interfaces failing testcase'
            Logger.info(err_msg)
            raise Exception(err_msg)



if __name__ == '__main__':
    main()