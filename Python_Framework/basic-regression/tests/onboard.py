import os, sys, time, re
import json
import datetime
from datetime import timedelta
import requests
import sys
sys.path.append('../')
from modules.StationAPI.api import ConfigManager
from modules.CloudOperations import CloudOperations
from modules.sshlib.ssh import ConnectionHandler
from custom_xmlrunner import setup_module_helper, main, CustomUnitTestCase
from utils import ln_gen_uuid
# from requests_toolbelt.utils import dump

'''modules ssh cs and cpe '''

def setUpModule():
      global BasePath, Logger, resource_json_hash, cloudObj, resource_onboard_json_hash

      global machine_hash, cloudstation, onboard_json, onboard_response

      BasePath = os.path.abspath(os.path.dirname(__file__))
      Logger = setup_module_helper(__file__, alarm_timeout=2400)
      BasePath = os.path.join(os.path.dirname(__file__), '../data_bags/')
      resource_json_hash = {
         "cloudstation-config": os.path.join(BasePath, 'cloudstation.json'),
         "machine-config": os.path.join(BasePath, 'machine-config.json')
      }

      with open(resource_json_hash['machine-config']) as fp:
          machine_hash = json.load(fp)

      with open(resource_json_hash['cloudstation-config']) as fp:
          cloudstation = json.load(fp)


      bp = os.path.join(os.path.dirname(__file__), '../data_bags/script_data_bags/onboard/')

      resource_onboard_json_hash = {
          "onboard": os.path.join(bp, "onboard.json"),
          "onboard_response": os.path.join(bp, "response.json"),
      }
      with open(resource_onboard_json_hash['onboard'], 'r') as fr:
          onboard_json = json.load(fr)

      with open(resource_onboard_json_hash['onboard_response'], 'r') as fs:
          onboard_response = json.load(fs)

      cloudObj = CloudOperations(act_as='master')
      _populate_other_params()


def _populate_other_params():
    pass

class Onboard(CustomUnitTestCase):

    data_map = {}
    customer_name = 'team-qa'

    def setup(self, json_file=None, data=None):
        pass

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        # Define policy configuartion data
        self = super(Onboard, cls).tearDownClass()
        pass
    @classmethod
    def _change_data(cls, json_data):
        """utility method to change/replace values in the json_data
        :param json_data: json dict to modify
        :return:
        """
        for k, v in cls.data_map.items():
            json_data = json_data.replace(str(k), str(v))
        return json_data

    def _retry_on_exception(func, max_tries=3, retry_sleep=0.3):
        """Mthod to retry on Exception
        :param max_tries: Integer , 3 - by default
        :return:
        """

        def wrapper(*args, **kwargs):
            for i in range(max_tries):
                try:
                    return func(*args, **kwargs)
                    break
                except Exception as err:
                    msg = "Got Exception:{}, retrying attempt: {}".format(err, i + 1)
                    print(msg)
                    if i + 1 == max_tries:
                        raise Exception("Got Exception in the last attempt: {}".format(err))
                    else:
                        time.sleep(retry_sleep)
                        continue

        return wrapper


    def write_json(self,data,filename):

        with open(filename, 'w') as fp:
            json.dump(data, fp, indent=2)

        return None

    def generate_traffic(self):

        device = {
            'device-type': 'linux',
            'ip': '172.11.35.20',
            'username': 'lavelle',
            'password': 'lavelle@3163',
        }
        Logger.info('Connecting to device {}'.format(device['ip']))
        a = ConnectionHandler(**device)
        # net_connect.find_prompt()
        Logger.info('Sending traffic to youtube application')
        output = a.run_cmd("wget https://youtube.com")
        Logger.info(output)
        if('200 OK' in output):
            return True
        else:
            raise Exception('Failed to send Traffic : {}'.format(output))

    def verify_traffic(self,start_time,end_time,user_id):
        """

        :param start_time:
        :param end_time:
        :param cnid:
        :return:
        """

        url = cloudObj._cloudstation_url + '/api/v2/lavelle/top_apps?from={}&to={}&scope_type=User&scope_id={}'.format(
            start_time,end_time,user_id )
        Logger.info('Traffic url {} '.format(url))


        self.flag = False
        res = cloudObj.get(url).json()
        Logger.info(res)

        for app in res['data']['APP_B_IN']:
            if app['name'] == 'youtube':
                self.flag = True
                break
        if self.flag:
            return True
        else:
            raise Exception('Traffic verify failed check logs')


    def test__create_customer(self):
        """
                Testcase does the following functions
                1] Create a customer using master credentials
                2] writes sys and net admin credentials into cloudstation.json
                3] writes customer details, sys_admin details and net_admin details to onboard_response.json
                :return: None
        """
        try:

            self.write_step('Creating customer')
            customer_details = {
                "customer_name": "three",
                "url_prefix": "qa-raksi"
            }
            Logger.info('customer details : {} '.format(customer_details))
            data = cloudObj.create_customer(**customer_details)
            Logger.info(data)

            #updating cloudstation.json with sys and net admin
            Logger.info('Updating the cloudstation.json with sys and net admin credentials')

            cloudstation['sys_admin_username'] = data['data']['sys_admin']['email']
            cloudstation['sys_admin_password'] = data['data']['sys_admin']['password']

            cloudstation['net_admin_username'] = data['data']['net_admin']['email']
            cloudstation['net_admin_password'] = data['data']['net_admin']['password']

            self.write_json(data=cloudstation, filename=resource_json_hash['cloudstation-config'])

            #writing customer, net-admin, sys-admin details into response.json file
            Logger.info('Writing the customer, sys-admin, net-admin details into response.json for future testcases')
            temp = str(self.customer_name)
            response_hash = {
                temp:{}
            }

            response_hash[temp]['customer'] = data['data']['customer']
            response_hash[temp]['net_admin'] = data['data']['net_admin_user']
            response_hash[temp]['sys_admin'] = data['data']['sys_admin_user']

            self.write_json(data=response_hash,filename=resource_onboard_json_hash['onboard_response'])

        except Exception as er:
            Logger.info('Failed to create a customer, check logs skipping remaining testcases')
            # testcase skip code here
            raise Exception(er)

    def test_connect_cpe(self):
        """
        Testcase does the following 1] create a network location as hub/spoke  2] Attach cp as hub/spoke 3] create lan config for cpe and validate the CPE status
        :return:
        """
        global cloudObj
        cloudObj = CloudOperations()

        with open(resource_onboard_json_hash['onboard_response'], 'r') as fu:
            onboard_response = json.load(fu)

        self.write_step('Creating Network location and attaching network appliance for the following')
        temp = str(self.customer_name)
        user_id = onboard_response[temp]['net_admin']['id']

        i = 1
        for each_cp in machine_hash['cp-details']:
            network_location = {
             "network_location_name" : 'network-location-{}'.format(i),
             "is_hub": each_cp['hub'],
             "location": each_cp['location'],
             "user_id" : user_id
            }

            Logger.info('Creating network location')
            site_info = cloudObj.create_network(**network_location)

            uuid = cloudObj.get_cp_uuid(each_cp['serial-number'])
            Logger.info('12 digit uuid for serial number {} is {}'.format(each_cp['serial-number'], uuid))
            Logger.info('Creating network applicance for the network location {} '.format(network_location['network_location_name']))
            cp_info = cloudObj.create_network_appliance(
                name= '-cp-{}'.format(i),
                site_id=site_info['site']['id'],
                uuid=uuid
            )
            time.sleep(60)

            cloudport_id = cp_info['data']['cloudport']['id']
            port = each_cp['interface']['lan']['port']

            Logger.info('Retreving cloudport link id associated to given cloudport {}'.format(cloudport_id))
            cloudport_linkid = cloudObj.get_cloudport_linkid(cloudport_id,port)

            lan_config = {
                "port_name": "lan0",
                "cloudport_id": cloudport_id,
                "cloudport_linkid": cloudport_linkid,
                "lan_ip": each_cp['interface']['lan']['addresses'][0]['ip'],
                "prefix_length":each_cp['interface']['lan']['addresses'][0]['prefix_length']
            }
            Logger.info('Creating lan_config for appliance {}'.format('-cp-{}'.format(i)))
            response = cloudObj.create_lan_config(**lan_config)

            i=i+1

        time.sleep(30)
        self.write_step('Verifying cloudport Gateway status ')
        cloudObj.verify_gateway_status()

        self.write_step('Generating and verifying traffic with CP')
        d = datetime.datetime.utcnow() - timedelta(minutes=5)
        start_time = '{}T{}Z'.format(d.strftime("%Y-%m-%d"), d.strftime("%H:%M:%S"))

        self.generate_traffic()
        time.sleep(50)

        current_utc = datetime.datetime.utcnow()
        end_time = '{}T{}Z'.format(current_utc.strftime("%Y-%m-%d"), current_utc.strftime("%H:%M:%S"))

        self.verify_traffic(start_time, end_time, user_id)


if __name__ == '__main__':
    main()
