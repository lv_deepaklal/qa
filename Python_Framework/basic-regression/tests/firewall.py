import os, sys, time, re
import json
import datetime
import random
from datetime import timedelta
import requests
import sys

sys.path.append('../')
from modules.StationAPI.api import ConfigManager
from modules.sshlib.ssh import ConnectionHandler
from custom_xmlrunner import setup_module_helper, main, CustomUnitTestCase
from modules.CloudOperations import CloudOperations
from utils import ln_gen_uuid
# from requests_toolbelt.utils import dump


def setUpModule():
    global BasePath, Logger, resource_json_hash, cloudObj, resource_json_hash

    global machine_hash, cloudstation, firewall_json

    BasePath = os.path.abspath(os.path.dirname(__file__))
    Logger = setup_module_helper(__file__, alarm_timeout=2400)

    BasePath = os.path.join(os.path.dirname(__file__), '../data_bags/')

    resource_json_hash = {
        "cloudstation-config": os.path.join(BasePath, 'cloudstation.json'),
        "machine-config": os.path.join(BasePath, 'machine-config.json')
    }

    with open(resource_json_hash['machine-config']) as fp:
        machine_hash = json.load(fp)

    with open(resource_json_hash['cloudstation-config']) as fp:
        cloudstation = json.load(fp)

    bp = os.path.join(os.path.dirname(__file__), '../data_bags/script_data_bags/')
    resource_json_hash = {
        "firewall": os.path.join(bp, "firewall.json"),
    }
    with open(resource_json_hash['firewall'], 'r') as fr:
        firewall_json = json.load(fr)

    _populate_other_params()


def _populate_other_params():
    global cp_object, cloudObj
    device = {
        'device_type': 'linux',
        'ip': '172.11.35.21',
        'username': 'lavelle',
        'password': 'lavelle@3163',
    }
    cp_object = ConnectionHandler(**device)
    Logger.info(cp_object)
    cloudObj = CloudOperations()


class Firewall(CustomUnitTestCase):
    data_map = {}

    def setup(self, json_file=None, data=None):
        pass

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        # Define policy configuartion data
        self = super(Firewall, cls).tearDownClass()
        pass

    @classmethod
    def _change_data(cls, json_data):
        """utility method to change/replace values in the json_data
        :param json_data: json dict to modify
        :return:
        """
        for k, v in cls.data_map.items():
            json_data = json_data.replace(str(k), str(v))
        return json_data

    def _retry_on_exception(func, max_tries=3, retry_sleep=0.3):
        """Mthod to retry on Exception
        :param max_tries: Integer , 3 - by default
        :return:
        """

        def wrapper(*args, **kwargs):
            for i in range(max_tries):
                try:
                    return func(*args, **kwargs)
                    break
                except Exception as err:
                    msg = "Got Exception:{}, retrying attempt: {}".format(err, i + 1)
                    print(msg)
                    if i + 1 == max_tries:
                        raise Exception("Got Exception in the last attempt: {}".format(err))
                    else:
                        time.sleep(retry_sleep)
                        continue

        return wrapper

    def get_network_group_details(self):
        url = cloudObj._base_url + 'policies/policy_objects'
        res = cloudObj.get(url).json()
        details = dict()
        for each in res['policy_objects']['network_groups']:
            if 'Default_NetworkGroup' in each['name']:
                details['Default-network-gp-id'] = each['id']
            if each['name'] == 'Internet':
                details['Internet'] = each['id']
            if 'Enterprise Network' in each['name']:
                details['Enterprise Network'] = each['id']

        return details


    def attach_policy(self, hex_id, site_id):
        """
        Function to attach a firewall policy to cs
        :param hex_id: policy hex_id
        :param site_id: site_id where you want to attach the policy
        :return: True/False
        """
        url = cloudObj._base_url + 'acls/{}/attach_policy'.format(hex_id)
        self.data_map['__site-id__'] = site_id
        payload = self._change_data(json.dumps(firewall_json['attach_payload']))
        print(payload)
        res = cloudObj.post(url=url, data=payload)
        Logger.info(res.json())
        if res.status_code == 200:
            Logger.info('Attached policy to given site')
            return True
        else:
            Logger.info('Failed to attach the firewall policy')
            return False


    def deattach_policy(self, policy_id, site_id):
        """
        Function to De-attach the policy from CS
        :param policy_id: policy-id
        :param site_id: site_id
        :return: True/False
        """
        url = cloudObj._base_url + '/acls/{}/acls_relations'.format(policy_id)
        payload = {"object_id": str(site_id), "object_type": "Site"}
        Logger.info(url)
        Logger.info(payload)
        res = cloudObj.post(url, json.dumps(payload)).json()
        Logger.info(res)

    def generate_policy_payload(self,**kwargs):

        payload = firewall_json['firewall_payload']

        if 'application_category' in kwargs.keys():
            for app in kwargs['application_category'].values():
                d = {'addition_type': 'OR', 'object':app}
                payload['both']['application_categories'].append(d)

        if 'application' in kwargs.keys():
            for app in kwargs['application'].values():
                d = {'addition_type': 'OR', 'object':app}
                payload['both']['applications'].append(d)

        if 'custom' in kwargs.keys():
            for app in kwargs['custom'].values():
                payload['both']['customs'].append(app)

        return payload



    def add_firewall_policy(self,app_classifier,**kwargs):
        """
        Function to add firewall policy w.r.t to application( youtube, facebook)
        :param app_classifier: same as application classifier details as in cs-UI
        :param kwargs: policy_name='abc', action=allow/deny, internet_id=24 ...
        :return: True/False
        """


        self.data_map['__policy-name__'] = kwargs.get('policy_name')
        self.data_map['__action__'] = kwargs.get('action')
        self.data_map[9999] = kwargs.get('destination_id') #int(network_id['Internet'])
        self.data_map[8888] = kwargs.get('source_id') #int(network_id['Default-network-gp-id'])

        payload = self.generate_policy_payload(**app_classifier)
        payload = self._change_data(json.dumps(payload))


        url = cloudObj._base_url + 'acls'
        response = cloudObj.post(url=url,data = payload).json()
        Logger.info("API response for adding policy {}".format(response))
        return response

    def delete_acl_policy(self,policy_hex_id):
        """
        Function to delete a firewall policy
        :param policy_hex_id: policy_hex_id -12 digits
        :return: True/False
        """

        Logger.info('Deleting the firewall policy')
        url = cloudObj._base_url + 'acls/{}'.format(policy_hex_id)
        res = cloudObj.delete(url=url)
        Logger.info(res.json())

        if res.status_code!=200:

            Logger.info('Failed to delete the policy ')
            return False
        return True
    def get_app_classifier_details(self, app_names):

        self.applications = {
            "application_category":{},
            "application":{},
            "custom": {}
        }

        url = cloudObj._base_url + 'policies/policy_objects'
        res = cloudObj.get(url).json()


        if "application_categories" in app_names.keys():
            for app in app_names['application_categories']:
                for app_cate in res['policy_objects']['application_categories']:
                    if app_cate['name'] == app:
                        self.applications['application_category'][app_cate['name']] = app_cate


        if "application" in app_names.keys():
            for app_name in app_names['application']:
                for application in res['policy_objects']['applications']:
                    if (application['name'] == app_name):
                        self.applications['application'][app_name] = application

        if "custom" in app_names.keys():
            for custom in app_names['custom']:
                d = { "addition_type": "OR", "object": {"value": custom, "type": "CustomProtocol"} }
                self.applications['custom'][custom] = d

        return self.applications

    def test_block_application_traffic_and_verify(self):

        network_id = cloudObj.get_network_group_details()
        policy_details = {
            "policy_name": 'demo_block_{}'.format(random.randint(1, 100)),
            "action": "Deny",
            "destination_id": int(network_id['Internet']),
            "source_id": int(network_id['Default-network-gp-id'])
        }

        application_classifiers = {
            "application": ["youtube","facebook"],
            "application_categories": ["Web"],

        }


        data = self.get_app_classifier_details(app_names=application_classifiers)

        self.write_step('Adding a firewall policy to cs')
        response = self.add_firewall_policy(app_classifier=data,**policy_details)

        # attaching acl policy to hub
        serial_number = machine_hash['cp-details'][0]['serial-number']
        site_id = cloudObj.get_site_id(serial_number=serial_number)
        try:


            self.write_step('Attaching firewall policy to hub')
            if self.attach_policy(hex_id=response['data']['acl']['hex_id'], site_id=site_id):

                """
                TODO:
                Take care of sleep part here or verify if config are pushed
                """
                Logger.info('waiting for 100 sec to push config to CP')
                time.sleep(100)

                self.write_step('Sending traffic to verify traffic is blocked')

                output = cp_object.run_cmd('wget https://youtube.com')
                Logger.info(output)
                if ('200 OK' not in output):
                    Logger.info('Traffic is not flowing to youtube')
                else:
                    err_msg = 'Able to send traffic to youtube after adding acl policy'
                    Logger.info(err_msg)
                    raise Exception(err_msg)

                # unblocking policy and verifying the traffic
                self.write_step('De-Attaching policy from site and verify the traffic is flowing')
                self.deattach_policy(response['data']['acl']['id'], site_id)
                Logger.info('sleeping for 100 sec to reflect policy de-attach')
                time.sleep(100)
                output = cp_object.run_cmd('wget https://youtube.com')
                Logger.info(output)
                if ('200 OK' in output):
                    Logger.info('Traffic is flowing to youtube post de-attaching the policy')
                else:
                    err_msg = 'Not able to send the traffic to youtube post de-attaching the policy'
                    Logger.info(err_msg)
                    raise Exception(err_msg)
            else:
                raise Exception('Failed to attach firewall policy to site ')
        finally:
            Logger.info('Feature cleanup')
            Logger.info('De-attaching policy from cs')
            self.deattach_policy(response['data']['acl']['id'], site_id)
            Logger.info('Deleting firewall policy from cs')
            self.delete_acl_policy(response['data']['acl']['hex_id'])


if __name__ == '__main__':
    main()
