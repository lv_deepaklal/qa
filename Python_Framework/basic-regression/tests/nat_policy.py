import os, sys, time, re
import json
import datetime
from datetime import timedelta, datetime
from functools import wraps

import random
import sys

sys.path.append('../')
from modules.sshlib.ssh import ConnectionHandler
from custom_xmlrunner import setup_module_helper, main, CustomUnitTestCase
from modules.CloudOperations import CloudOperations



def setUpModule():
    global BasePath, Logger, resource_json_hash, cloudObj, resource_json_hash

    global machine_hash, cloudstation, nat_policy_json

    BasePath = os.path.abspath(os.path.dirname(__file__))
    Logger = setup_module_helper(__file__, alarm_timeout=2400)

    BasePath = os.path.join(os.path.dirname(__file__), '../data_bags/')

    resource_json_hash = {
        "cloudstation-config": os.path.join(BasePath, 'cloudstation.json'),
        "machine-config": os.path.join(BasePath, 'machine-config.json')
    }

    with open(resource_json_hash['machine-config']) as fp:
        machine_hash = json.load(fp)

    with open(resource_json_hash['cloudstation-config']) as fp:
        cloudstation = json.load(fp)

    bp = os.path.join(os.path.dirname(__file__), '../data_bags/script_data_bags/')
    resource_json_hash = {
        "nat_policy": os.path.join(bp, "nat_policy.json"),
    }
    with open(resource_json_hash['nat_policy'], 'r') as fr:
        nat_policy_json = json.load(fr)

    _populate_other_params()


def _populate_other_params():
    global cp_object, cloudObj
    device = {
        'ip': '172.11.35.21',
        'username': 'lavelle',
        'password': 'lavelle@3163',
    }
    cp_object = ConnectionHandler(**device)
    Logger.info(cp_object)
    cloudObj = CloudOperations()


class NAT_policy(CustomUnitTestCase):
    data_map = {}

    def setup(self, json_file=None, data=None):
        pass

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        # Define policy configuartion data
        self = super(NAT_policy, cls).tearDownClass()
        pass

    @classmethod
    def _change_data(cls, json_data):
        """utility method to change/replace values in the json_data
        :param json_data: json dict to modify
        :return:
        """
        for k, v in cls.data_map.items():
            json_data = json_data.replace(str(k), str(v))
        return json_data

    @classmethod
    def timeit(func):
        """This decorator prints the execution time for the decorated function."""

        @wraps(func)
        def wrapper(*args, **kwargs):
            hash = random.getrandbits(128)
            start = time.time()
            Logger.debug("{} : {} start: at {}".format(hash, func.__name__, datetime.datetime.now()))
            result = func(*args, **kwargs)
            Logger.debug("{} : {} End: at {}".format(hash, func.__name__, datetime.datetime.now()))
            end = time.time()
            Logger.debug("{} : {} ran in {}s".format(hash, func.__name__, round(end - start, 2)))
            return result

        return wrapper

    def _retry_on_exception(func, max_tries=3, retry_sleep=0.3):
        """Mthod to retry on Exception
        :param max_tries: Integer , 3 - by default
        :return:
        """

        def wrapper(*args, **kwargs):
            for i in range(max_tries):
                try:
                    return func(*args, **kwargs)
                    break
                except Exception as err:
                    msg = "Got Exception:{}, retrying attempt: {}".format(err, i + 1)
                    print(msg)
                    if i + 1 == max_tries:
                        raise Exception("Got Exception in the last attempt: {}".format(err))
                    else:
                        time.sleep(retry_sleep)
                        continue

        return wrapper



    def generate_nat_payload(self,policy_details,**kwargs):
        """
        Function to generate lb policy payload data
        :param policy_details: similar to policy-details as in CS_UI
        :param action_details: similar to action-details as in CS-UI
        :param kwargs: application_classifier details in dict format
        :return: generated lb_policy payload
        """
        payload = nat_policy_json
        if 'application_category' in kwargs.keys():
            for app in kwargs['application_category'].values():
                d = {'addition_type': 'OR', 'object': app}
                payload['both']['application_categories'].append(d)

        if 'application' in kwargs.keys():
            for app in kwargs['application'].values():
                d = {'addition_type': 'OR', 'object': app}
                payload['both']['applications'].append(d)

        if 'custom' in kwargs.keys():
            for app in kwargs['custom'].values():
                payload['both']['customs'].append(app)

        if 'from' in kwargs.keys():
            if 'lan_subnets' in kwargs['to']:
                pass

            if 'custom' in kwargs['from']:
                for ip in kwargs['from']['custom']:
                    temp = {"value": ip, "type": "CustomIp"}
                    d = {'addition_type': 'OR', 'object': temp}
                    payload['from']['customs'].append(d)

        if 'to' in kwargs.keys():
            if 'lan_subnets' in kwargs['to']:
                pass

            if 'custom' in kwargs['to']:
                for ip in kwargs['to']['custom']:
                    temp = { "value": ip, "type": "CustomIp"}
                    d = {'addition_type': 'OR', 'object': temp}
                    payload['to']['customs'].append(d)


        self.data_map[9999] = policy_details.get('destination_id')
        self.data_map[8888] = policy_details.get('source_id')
        self.data_map['__policy-name__'] = policy_details.get('policy_name')
        self.data_map['__cloud-port-id__'] = policy_details.get('owner_id')
        self.data_map['__nat-type__'] = policy_details.get('nat_type')
        self.data_map['__interface-id__'] = policy_details.get('interface_id')
        self.data_map['__ip-poll-range__'] = policy_details.get('pool')


        payload = self._change_data(json.dumps(payload))

        return payload

    def add_nat_policy(self,policy_details, **application_classifiers):
        """
        Fuction to add lb policy to cloudstation
        :param payload:
        :return:
        """
        data = cloudObj.get_app_classifier_details(app_names=application_classifiers)

        data['to'] = application_classifiers['to']
        data['from'] = application_classifiers['from']
        payload = self.generate_nat_payload(policy_details, **data)

        Logger.info('Policy payload : {} '.format(payload))

        url = cloudObj._base_url + 'nat_policies'

        response = cloudObj.post(url=url,data = payload)



        if response.status_code != 200:
            err_msg = 'Failed to add NAT policy to cloud-station: API response {}'.format(response.json())
            Logger.info(err_msg)
            raise Exception(err_msg)

        Logger.info('Policy added {} '.format(response.json()))
        return response.json()

    def attach_nat_policy(self,cloudport_id,policy_id):
        """
        Function to attach nat policy to network appliance
        :param cloudport_id: network-appliance id/cp-id
        :return: True/False
        """

        url = cloudObj._base_url + 'nat_policies/{}/attach_policy'.format(policy_id)
        payload = {
                "attached_to_type": "Cloudport",
                "attached_to_id": str(cloudport_id)
            }
        check = cloudObj.post(url,json.dumps(payload))
        if check.status_code != 200:
            err_msg = 'Failed to attach policy {} '.format(check.json())
            Logger.info(err_msg)
            return False

        Logger.info(check.json())
        return True

    def create_ssh_object(self,ip,username='lavelle',pwd='lavelle@3163'):
        """
        Function to create a SSH object of given IP which can be used to perform ssh operations
        :param ip: Device IP wish to connect
        :param username: device username
        :param pwd: device password
        :return: ssh object of device
        """
        device_details = {
            'ip': ip,
            'username': username,
            'password': pwd,
        }
        Logger.info(device_details)
        return ConnectionHandler(**device_details)
    def modify_wan_port_on_cp(self,cp_id,wan_id,action):
        """
        Function to modify the wan interface on specified cloudport
        :param cp_id: cloudport ID
        :param wan_name: wan interface name
        :param action: action = up/down
        :return: True or False
        """
        payload_url  = cloudObj._base_url + 'cloudports/{}/cloudport_links/{}'.format(cp_id,wan_id)
        payload = cloudObj.get(url=payload_url).json()


        payload = payload['cloudport_links']
        payload['enable'] = action
        Logger.info(payload)
        url = payload_url+'/update_port'
        response = cloudObj.post(url=url,data = json.dumps(payload))
        if response.status_code != 200:
            Logger.info('Failed to modify( action = {} ) the physical port : {}'.format(action,response.json()))
            return False

        Logger.info(response.json())
        return True

    def deattach_policy(self,policy_details,cp_id):
        """
        Helps to de-attach the nat policy
        :param policy_details: created policy details
        :param cp_id: cp_id
        :return:
        """
        url = cloudObj._base_url + 'nat_policies/{}/nat_policy_relations'.format(policy_details['data']['nat_policies']['id'])
        payload = {

                "object_id": str(cp_id),
                "object_type": "Cloudport"
        }
        r = cloudObj.post(url=url,data = json.dumps(payload))
        if r.status_code == 200:
            Logger.info('Policy de attached successfully {} '.format(r.json()))
            return True

    def delete_nat_policy(self,policy_id):
        """
        Function to delete the nat_policy
        :param policy_id: policy-id
        :return:
        """
        url = cloudObj._base_url + 'nat_policies/{}'.format(policy_id)
        r = cloudObj.delete(url=url)

        if r.status_code == 200:
            Logger.info('Policy deleted {} '.format(r.json()))

    def cleanUP(self,policy_details,cp_id,wan_id):
        """
        Function to deattach and delete the policy
        :param policy_details: created policy details
        :param cp_id: cp_id
        :return: True/false
        """
        Logger.info('De-attaching the policy')
        self.deattach_policy(policy_details,cp_id)
        Logger.info('Deleting teh policy')
        self.delete_nat_policy(policy_details['data']['nat_policies']['id'])
        Logger.info('ENABLING WAN1 INTERFACE')
        self.modify_wan_port_on_cp(wan_id=wan_id, cp_id=cp_id, action='true')

    def test_nat_policy_with_static_nat(self):
        """
        Testcase to attach static-nat policy and verify
        :return:
        """

        network_id = cloudObj.get_network_group_details()
        serial_number = machine_hash['cp-details'][0]['serial-number']
        Logger.info('Getting cloud-port details of hub')
        cp_details = cloudObj.get_cloudport_details(serial_number=serial_number)

        Logger.info('DISABLING WAN1 INTERFACE')
        wan1_details = cloudObj.get_interface_details(cp_details['id'],'wan1')
        self.modify_wan_port_on_cp(wan_id=wan1_details['cloudport_link']['id'],cp_id=cp_details['id'],action='false')


        interface_details = cloudObj.get_interface_details(cp_details['id'],'wan0')
        Logger.info(interface_details)
        policy_details = {
            "policy_name": 'nat_policy_demo_{}'.format(random.randint(1, 100)),
            "destination_id": int(network_id['Internet']),
            "source_id": int(network_id['Default-network-gp-id']),
            "owner_id" : str(cp_details['id']),
            "nat_type": "static-nat",
            "interface_id": interface_details['id'],
            "pool": "10.117.50.15"  #change ip dynamically from master json

        }

        #custom ip address to be taken dynamically from master json

        application_classifiers = {
            "from": {
                'custom': ["10.117.50.11"]
            },
            "to": {
                'custom': ["8.8.4.4"]
            }
        }

        self.write_step('Adding new NAT  policy ')
        policy_response = self.add_nat_policy(policy_details, **application_classifiers)

        try:
            self.write_step('Attaching policy to Cloud-port')
            response = self.attach_nat_policy(policy_id=policy_response['data']['nat_policies']['id'],cloudport_id=cp_details['id'])

            if response:

                #verify traffic in cp using wan IP:
                cp_ssh = self.create_ssh_object(ip=interface_details['interface_addresses'][0]['ip'])
                cp_ssh.switch_to_root()
                cp_ssh.verify_nat_config_pushed('10.117.50.15')


                self.write_step('Sending Traffic to custom IP')
                cp_object.send_traffic(client_ip='8.8.4.4', rate=1, time_limit=120)

                self.write_step('Verifying traffic in cloudport via tcpdump')
                logs = cp_ssh.get_tcp_dump_data(cmd='tcpdump -ni wan0 -Qout host 10.117.50.15 ', number_of_packets=5)

                Logger.info(logs)
                if '10.117.50.15' not  in logs:
                    raise Exception ('Failed to verify NAT policy ')

                self.write_step('Cleaning testcase: De-attaching and deleting the policy')
                self.cleanUP(policy_details=policy_response,cp_id=cp_details['id'],wan_id=wan1_details['cloudport_link']['id'])
        except Exception as e:
            Logger.info('Exception found  {} cleaning up, De-attaching and deleting policy if added '.format(e))
            self.cleanUP(policy_details=policy_response,cp_id=cp_details['id'],wan_id=wan1_details['cloudport_link']['id'])
            raise Exception(e)



if __name__ == '__main__':
    main()