import os, sys, time, re
import json
import datetime
from datetime import timedelta
import requests
import sys
sys.path.append('../')
from modules.StationAPI.api import ConfigManager
from modules.sshlib.ssh import ConnectionHandler
from custom_xmlrunner import setup_module_helper, main, CustomUnitTestCase
from utils import ln_gen_uuid
from requests_toolbelt.utils import dump
def setUpModule():

    global BasePath, Logger, resource_json_hash, cloudObj, resource_json_hash

    global machine_hash, cloudstation, firewall_json

    BasePath = os.path.abspath(os.path.dirname(__file__))
    Logger = setup_module_helper(__file__, alarm_timeout=2400)

    BasePath = os.path.join(os.path.dirname(__file__), '../data_bags/')

    resource_json_hash = {
        "cloudstation-config": os.path.join(BasePath, 'cloudstation.json'),
        "machine-config": os.path.join(BasePath, 'machine-config.json')
    }


    with open(resource_json_hash['machine-config']) as fp:
        machine_hash = json.load(fp)

    with open(resource_json_hash['cloudstation-config']) as fp:
        cloudstation = json.load(fp)

    bp = os.path.join(os.path.dirname(__file__), '../data_bags/script_data_bags/')
    resource_json_hash = {
        "firewall": os.path.join(bp, "firewall.json"),
    }
    with open(resource_json_hash['firewall'], 'r') as fr:
        firewall_json = json.load(fr)

    cloudObj = ConfigManager(cloudstation)
    _populate_other_params()

def _populate_other_params():
    global cp_object
    device = {
        'device_type': 'linux',
        'ip': '172.11.35.20',
        'username': 'lavelle',
        'password': 'lavelle@3163',
    }
    cp_object = ConnectionHandler(**device)

class Firewall(CustomUnitTestCase):

    data_map = {}

    def setup(self, json_file=None, data=None):
        pass

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        # Define policy configuartion data
        self = super(Firewall, cls).tearDownClass()
        pass
    @classmethod
    def _change_data(cls, json_data):
        """utility method to change/replace values in the json_data
        :param json_data: json dict to modify
        :return:
        """
        for k, v in cls.data_map.items():
            json_data = json_data.replace(str(k), str(v))
        return json_data

    def _retry_on_exception(func, max_tries=3, retry_sleep=0.3):
        """Mthod to retry on Exception
        :param max_tries: Integer , 3 - by default
        :return:
        """
        def wrapper(*args, **kwargs):
            for i in range(max_tries):
                try:
                    return func(*args, **kwargs)
                    break
                except Exception as err:
                    msg = "Got Exception:{}, retrying attempt: {}".format(err, i + 1)
                    print(msg)
                    if i + 1 == max_tries:
                        raise Exception("Got Exception in the last attempt: {}".format(err))
                    else:
                        time.sleep(retry_sleep)
                        continue

        return wrapper

    def get_app_details(self,app_names):
        url = cloudObj._base_url + 'policies/policy_objects'
        res = cloudObj.get(url).json()
        self.applications = dict()
        for app_name in app_names:
            for application in res['policy_objects']['applications']:
                if(application['name'] == app_name):
                    self.applications[app_name] = application
        return self.applications

    def get_network_group_details(self):
        url = cloudObj._base_url + 'policies/policy_objects'
        res = cloudObj.get(url).json()
        details = dict()
        for each in res['policy_objects']['network_groups']:
            if 'Default_NetworkGroup' in each['name']:
                details['Default-network-gp-id'] = each['id']
            if each['name'] == 'Internet':
                details['Internet'] = each['id']
            if 'Enterprise Network' in each['name']:
                details['Enterprise Network'] = each['id']

        return details

    def add_firewall_policy(self,app_names):
        app_info = self.get_app_details(app_names=app_names)
        payload = firewall_json['firewall_payload']
        for app in app_info.values():
            d = {'addition_type': 'OR', 'object':app}
            payload['both']['applications'].append(d)

        payload = self._change_data(json.dumps(payload))
        url = cloudObj._base_url + 'acls'
        response = cloudObj.post(url=url,data = payload).json()
        return response

    def get_site_id(self,role):
        url = cloudObj._cloudstation_url + '/api/v2/lavelle/roles/get_permit_data?obj=sites'
        res = cloudObj.get(url).json()
        for site in res['data']['sites']:
            if role == 'hub':
                if site['is_hub'] == True:
                    return site['id']
            if role == 'spoke':
                if site['is_hub'] == 'false':
                    return site['id']

    def attach_policy(self,hex_id,site_id):
        url = cloudObj._base_url + '/acls/{}/attach_policy'.format(hex_id)
        self.data_map['__site-id__'] = site_id
        payload = self._change_data(json.dumps(firewall_json['attach_payload']))
        res = cloudObj.post(url=url,data = payload).json()
        return res



    def get_policy_uuid(self,site_id,policy_name):
        url = cloudObj._cloudstation_url + '/api/v2/lavelle/roles/get_scoped_policies?policy_type=acl_rules&obj_type=sites&obj_id={}&type=attach'.format(site_id)
        res = cloudObj.get(url).json()

        for policy in res['data']['policies']:
            if policy['name'] == policy_name:
                return policy

        return False

    def deattach_policy(self,policy_id,site_id):
        url = cloudObj._base_url + '/acls/{}/acls_relations'.format(policy_id)
        payload = {"object_id":str(site_id),"object_type":"Site"}
        res = cloudObj.post(url,json.dumps(payload)).json()
        print(res)


    def test_block_application_traffic_and_verify(self):

        network_id = self.get_network_group_details()
        policy_name = 'Youtube_block'
        self.data_map['__policy-name__'] = policy_name
        self.data_map['__action__'] = 'Deny'
        self.data_map[9999] = int(network_id['Internet'])
        self.data_map[8888] = int(network_id['Default-network-gp-id'])

        app_names = ['youtube','facebook']
        data = self.add_firewall_policy(app_names=app_names)

        # #attaching acl policy to hub
        site_id = self.get_site_id(role='hub')

        self.attach_policy(hex_id=data['data']['acl']['hex_id'],site_id=site_id)
        print('sleeping')
        time.sleep(30)

        #
        #
        # #send traffic
        # print("run command")
        output = cp_object.run_cmd('wget https://youtube.com')
        print(output.raw_line)

        if( '200 OK' not in output):
            print('success')
        else:
            raise Exception('Able to send traffic after adding acl policy')

        #unblocking policy and verifying the traffic
        self.deattach_policy(data['data']['acl']['id'], site_id)
        print('sleeping')
        time.sleep(60)
        output = cp_object.run_cmd('wget https://youtube.com')
        print(output.raw_line)
        if ('200 OK' in output):
            print('success')
        else:
            raise Exception('Able to send traffic after adding acl policy')



if __name__ == '__main__':
    main()