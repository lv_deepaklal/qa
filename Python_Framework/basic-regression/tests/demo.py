import os
import glob
import sys
sys.path.append('../')
#print (sys.path)
import re
# import env
import subprocess

from optparse import OptionParser
import json
from custom_xmlrunner import setup_module_helper
from collections import OrderedDict
from urllib.parse import urlparse

Logger = setup_module_helper(__file__)

station_hash=dict()
scheme='https://'

class SetUp():

    def __init__(self,linux_hash = {},machine_hash = {},config_hash = {}):

        self.basepath = os.path.join(os.path.dirname(__file__), '../data_bags/')
        basepath = self.basepath
        self.resource_json_hash = {
            "cloudstation": os.path.join(basepath, "cloudstation.json"),
            "machine-config": os.path.join(basepath, 'machine-config.json'),
            "test-bed": os.path.join(basepath, 'test-bed.json'),
            "config": os.path.join(basepath, 'config.json')
        }
        # print(self.resource_json_hash)

        self.linux_hash = linux_hash
        self.machine_hash = machine_hash
        self.config_hash = config_hash



        with open(self.resource_json_hash['test-bed']) as fp:
            self.linux_hash = json.load(fp)

        with open(self.resource_json_hash['machine-config']) as fp:
            self.machine_hash = json.load(fp)

        with open(self.resource_json_hash['config']) as fp:
            self.config_hash = json.load(fp)

    def validate(self):
        """Perform the CLI params validation passed to this script
        :return: None
        """
        #print("Inside validation")

        RequiredArgs = [
            'domain_name',
            'url_prefix',
            'master_username',
            'master_password',
            'cloud-station',
            'cp-details'
        ]
        exception_list = []
        for attr, value in self.config_hash.items():

            if attr in RequiredArgs and value is None:
                exception_list.append(attr)


        if exception_list:
            raise Exception("\nExecution failed, "
                            "Following required params are missing: {er}".format(
                er=exception_list
            ))

    def json_config(self):
        """
        Does the following
        1] Loads the machine config json based on the test-bed ip parameter
        2] Loads the cloud-station json based on the cli params
        :return:
        """
        self.load_machine_config_json()
        self.load_cloud_station_json()

    def load_machine_config_json(self):
        data = dict()

        data['cloud-station'] = self.config_hash['cloud-station']
        data['insights'] = self.config_hash['insights']
        data['cp-details'] = self.config_hash['cp-details']


        with open(self.resource_json_hash['machine-config'], 'w') as fp:
            json.dump(data, fp, indent=2)

    def load_cloud_station_json(self):

        with open(self.resource_json_hash['cloudstation']) as fp:
            station_hash = json.load(fp, object_pairs_hook=OrderedDict)

        station_hash['master_username'] = self.config_hash['master_username']
        station_hash['master_password'] = self.config_hash['master_password']
        station_hash['domain'] = self.config_hash['domain']
        station_hash['url_prefix'] = self.config_hash['url_prefix']
        station_hash['cloudstation_url'] = scheme + self.config_hash['url_prefix'] + "-webui." + self.config_hash['domain']
        station_hash['base_url'] = station_hash['cloudstation_url'] + "/api/v1/lavelle/"

        station_hash['network_location'] = station_hash['base_url'] + 'sites/'
        station_hash['cloud_port'] = station_hash['base_url'] + 'cloudports/'
        station_hash['cloud_port_location'] = station_hash['base_url'] + 'cloudports/locations/'
        station_hash['sys_admin_username'] = ''
        station_hash['sys_admin_password'] = ''
        station_hash['net_admin_username'] = ''
        station_hash['net_admin_password'] = ''

        with open(self.resource_json_hash['cloudstation'], 'w') as fp:
            json.dump(station_hash, fp, indent=2)

if __name__ == "__main__":
    a = SetUp()
    a.validate()
    a.json_config()

