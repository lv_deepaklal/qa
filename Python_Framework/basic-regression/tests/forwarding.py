import os, sys, time, re
import json
import datetime
from datetime import timedelta, datetime
from functools import wraps

import random
import sys

sys.path.append('../')
from modules.sshlib.ssh import ConnectionHandler
from custom_xmlrunner import setup_module_helper, main, CustomUnitTestCase
from modules.CloudOperations import CloudOperations



def setUpModule():
    global BasePath, Logger, resource_json_hash, cloudObj, resource_json_hash

    global machine_hash, cloudstation, forward_policy_json

    BasePath = os.path.abspath(os.path.dirname(__file__))
    Logger = setup_module_helper(__file__, alarm_timeout=2400)

    BasePath = os.path.join(os.path.dirname(__file__), '../data_bags/')

    resource_json_hash = {
        "cloudstation-config": os.path.join(BasePath, 'cloudstation.json'),
        "machine-config": os.path.join(BasePath, 'machine-config.json')
    }

    with open(resource_json_hash['machine-config']) as fp:
        machine_hash = json.load(fp)

    with open(resource_json_hash['cloudstation-config']) as fp:
        cloudstation = json.load(fp)

    bp = os.path.join(os.path.dirname(__file__), '../data_bags/script_data_bags/')
    resource_json_hash = {
        "forward_policy": os.path.join(bp, "forwarding.json"),
    }
    with open(resource_json_hash['forward_policy'], 'r') as fr:
        forward_policy_json = json.load(fr)

    _populate_other_params()


def _populate_other_params():
    global cp_object, cloudObj, host_obj
    devices = [
        {
            "name": "hub",
            "ip": "172.11.35.21",
            "username": "lavelle",
            "password": "lavelle@3163"
        },
        {
            "name": "spoke-1",
            "ip": "172.11.35.22",
            "username": "lavelle",
            "password": "lavelle@3163"
        },
        {
            "name": "spoke-2",
            "ip": "172.11.35.23",
            "username": "lavelle",
            "password": "lavelle@3163"
        }
    ]
    temp_devices = devices
    host_obj = dict()

    for device in temp_devices:
        name = device.pop('name')
        host_obj[name] = ConnectionHandler(**device)


    Logger.info(host_obj)
    cloudObj = CloudOperations()


class Forward_policy(CustomUnitTestCase):
    data_map = {}

    def setup(self, json_file=None, data=None):
        pass

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        # Define policy configuartion data
        self = super(Forward_policy, cls).tearDownClass()
        pass

    @classmethod
    def _change_data(cls, json_data):
        """utility method to change/replace values in the json_data
        :param json_data: json dict to modify
        :return:
        """
        for k, v in cls.data_map.items():
            json_data = json_data.replace(str(k), str(v))
        return json_data

    @classmethod
    def timeit(func):
        """This decorator prints the execution time for the decorated function."""

        @wraps(func)
        def wrapper(*args, **kwargs):
            hash = random.getrandbits(128)
            start = time.time()
            Logger.debug("{} : {} start: at {}".format(hash, func.__name__, datetime.datetime.now()))
            result = func(*args, **kwargs)
            Logger.debug("{} : {} End: at {}".format(hash, func.__name__, datetime.datetime.now()))
            end = time.time()
            Logger.debug("{} : {} ran in {}s".format(hash, func.__name__, round(end - start, 2)))
            return result

        return wrapper

    def _retry_on_exception(func, max_tries=3, retry_sleep=0.3):
        """Mthod to retry on Exception
        :param max_tries: Integer , 3 - by default
        :return:
        """

        def wrapper(*args, **kwargs):
            for i in range(max_tries):
                try:
                    return func(*args, **kwargs)
                    break
                except Exception as err:
                    msg = "Got Exception:{}, retrying attempt: {}".format(err, i + 1)
                    print(msg)
                    if i + 1 == max_tries:
                        raise Exception("Got Exception in the last attempt: {}".format(err))
                    else:
                        time.sleep(retry_sleep)
                        continue

        return wrapper


    def generate_payload(self,policy_details,**kwargs):
        """
        Function to generate lb policy payload data
        :param policy_details: similar to policy-details as in CS_UI
        :param action_details: similar to action-details as in CS-UI
        :param kwargs: application_classifier details in dict format
        :return: generated lb_policy payload
        """
        payload = forward_policy_json
        if 'application_category' in kwargs.keys():
            for app in kwargs['application_category'].values():
                d = {'addition_type': 'OR', 'object': app}
                payload['both']['application_categories'].append(d)

        if 'application' in kwargs.keys():
            for app in kwargs['application'].values():
                d = {'addition_type': 'OR', 'object': app}
                payload['both']['applications'].append(d)

        if 'custom' in kwargs.keys():
            for app in kwargs['custom'].values():
                payload['both']['customs'].append(app)

        if 'from' in kwargs.keys():
            pass

        if 'to' in kwargs.keys():
            if 'lan_subnets' in kwargs['to']:
                pass

            if 'custom' in kwargs['to']:
                for ip in kwargs['to']['custom']:
                    temp = { "value": ip, "type": "CustomIp"}
                    d = {'addition_type': 'OR', 'object': temp}
                    payload['to']['customs'].append(d)

        self.data_map[9999] = policy_details.get('destination_id')
        self.data_map[8888] = policy_details.get('source_id')
        self.data_map['__policy-name__'] = policy_details.get('policy_name')
        self.data_map['__direction__'] = policy_details.get('direction')
        self.data_map['__next-hop__'] = policy_details.get('next_hop')
        self.data_map['__encapsulation__'] = policy_details.get('encapsulation')

        payload = self._change_data(json.dumps(payload))

        return payload

    def add_forwarding_policy(self,policy_details, **application_classifiers):
        """
        Fuction to add forwarding policy to cloudstation
        :param payload:
        :return:
        """
        Logger.info('getting the app-classifier data')
        data = cloudObj.get_app_classifier_details(app_names=application_classifiers)
        Logger.info(data)
        payload = self.generate_payload(policy_details, **data)

        url = cloudObj._base_url + 'wan_policies'

        Logger.info('{} {}'.format(url,payload))
        response = cloudObj.post(url=url,data = payload)


        if response.status_code != 200:
            err_msg = 'Failed to add forwarding policy to cloud-station: API response {}'.format(response.json())
            Logger.info(err_msg)
            raise Exception(err_msg)

        Logger.info('Policy added {} '.format(response.json()))
        return response.json()

    # def attach_policy(self,policy_id,ng_id):
    #     """
    #     Function to attach lb policy to cloud-station
    #     :param policy_id:
    #     :return:
    #     """
    #
    #     url =  cloudObj._base_url + 'wan_policies/{}/attach_policy'.format(policy_id)
    #     payload = {
    #           "attached_to_type": "NetworkGroup",
    #           "attached_to_id": ng_id
    #     }
    #
    #     response = cloudObj.post(url=url,data=json.dumps(payload))
    #     if response.status_code != 200:
    #         err_msg='Failed to attach policy Reason {}'.format(response.json())
    #         Logger.fatal(err_msg)
    #         return False
    #     Logger.info('Attached policy {}'.format(response.json()))
    #     return response.json()  #

    def attach_policy(self,policy_hex_id,site_id):
        """
        Function to attach lb policy to cloud-station
        :param policy_id:
        :return:
        """
        url = cloudObj._base_url + 'wan_policies/{}/attach_policy'.format(policy_hex_id)
        payload = {
            "attached_to_type":"Site",
            "attached_to_id":str(site_id)
        }

        response = cloudObj.post(url=url,data=json.dumps(payload))
        if response.status_code != 200:
            err_msg='Failed to attach policy Reason {}'.format(response.json())
            Logger.fatal(err_msg)
            return False
        Logger.info('Attached policy {}'.format(response.json()))
        return response.json()

    def de_attach_policy(self,policy_id,site_id):
        """
        Function to de-attach the forwarding policy form cs
        :param policy_id: policy-id
        :param site_id: site_id-id
        :return: de-attach's policy
        """
        url = cloudObj._base_url + 'wan_policies/{}/wan_policy_relations'.format(policy_id)
        payload = {
            "object_type": "Site",
            "object_id": str(site_id)
        }
        Logger.info(url)
        Logger.info(payload)
        response = cloudObj.post(url=url, data=json.dumps(payload))
        if response.status_code != 200:
            err_msg = 'Failed to de-attach policy Reason {}'.format(response.json())
            Logger.fatal(err_msg)
            return False
        Logger.info('De-attached policy {}'.format(response.json()))
        return response.json()

    def delete_forwarding_policy(self,policy_hex_id):
        """
        Function to delete the trl policy
        :param policy_hex_id: policy_hex_id
        :return: True/false
        """
        Logger.info('Deleting the trl policy ')
        url = cloudObj._base_url + 'wan_policies/{}'.format(policy_hex_id)
        res = cloudObj.delete(url = url)
        Logger.info(res.json())
        if res.status_code!=200:
            Logger.info('failed to delete the policy')
            return False
        Logger.info(' policy deleted')
        return True

    def atest_forward_policy_using_custom(self):
        """
        Testcase to attach forwarding policy on spoke and verify traffic from hub
        :return:
        """
        network_id = cloudObj.get_network_group_details()
        policy_details = {
            "policy_name": 'forwarding_policy_demo_{}'.format(random.randint(1, 100)),
            "destination_id": int(network_id['Internet']),
            "source_id": int(network_id['Default-network-gp-id']),
            "direction": "wan",
            "next_hop": "local",

        }
        #custom ip address to be taken dynamically from master json

        application_classifiers = {
            "application": ["dns"],
            "custom":["tcp","udp"],
            "to": {
                'custom': ["8.8.8.8","8.8.4.4"]
            }
        }

        self.write_step('Adding new Forwarding policy to CS')
        response = self.add_forwarding_policy(policy_details, **application_classifiers)

        self.write_step('Attaching policy to default network-group')
        data = cloudObj.get_network_group_info(id=int(network_id['Default-network-gp-id']))
        self.attach_policy(policy_id=response['data']['wan_policy']['hex_id'], ng_id=data['hex_id'])

        self.write_step('Adding DNS proxies to the cloud-station')
        dns_details = {
            "name" : 'dns_entry_{}'.format(random.randint(1, 100)),
            "primary_server": '8.8.8.8',
            "secondary_server": '8.8.4.4',
            "server_details":[
                {
                    "name":"one",
                    "fqdn": "qa-lvn.com",
                    "public_ip": "172.11.35.88",
                    "private_ip": "10.117.50.120"
                },
                {
                    "name":"two",
                    "fqdn": "qa-lvn1.com",
                    "public_ip": "172.11.35.89",
                    "private_ip": "10.117.50.121"
                }
            ]
        }

        res = cloudObj.create_dns_proxy(**dns_details)

        self.write_step('Adding newly added DNS to default profile ID')
        res = cloudObj.get_default_network_location_profile_id()
        res['dns_proxy_enable'] = True
        res['dns_proxy_id'] = res['id']

        payload = {
            "id" : res['id'],
        }
        res.pop('id')
        payload['profile'] = res

        cloudObj.update_network_location_details(payload=payload)

    def test_forward_policy_using_hub(self):
        """
        Testcase to attach forwarding policy on spoke and verify traffic from hub
        :return:
        """
        network_id = cloudObj.get_network_group_details()
        serial_number = machine_hash['cp-details'][1]['serial-number']
        Logger.info('serial number is {}'.format(serial_number))
        policy_details = {
            "policy_name": 'forwarding_policy_demo_{}'.format(random.randint(1, 100)),
            "destination_id": int(network_id['Internet']),
            "source_id": int(network_id['Default-network-gp-id']),
            "direction": "wan",
            "next_hop": "hub",
            "encapsulation": "lntun",

        }
        #custom ip address to be taken dynamically from master json

        application_classifiers = {
            'to':{
                'custom':['8.8.4.4']
            }
        }

        self.write_step('Adding new Forwarding policy to CS')
        response = self.add_forwarding_policy(policy_details, **application_classifiers)
        self.write_step('Attaching policy to site')
        site_id = cloudObj.get_site_id(serial_number=serial_number)
        try:

            self.attach_policy(policy_hex_id=response['data']['wan_policy']['hex_id'], site_id=site_id)

            Logger.info('Sleeping for 120 sec to push the config from cs to cp')
            time.sleep(120)
            self.write_step('Send traffic from spoke to internet')
            Logger.info('sending traffic from spoke host to internet ')
            host_obj['spoke-1'].send_traffic(client_ip='8.8.4.4', rate=2, time_limit=120)

            self.write_step('Verifying traffic from hub cp')

            Logger.info('creating a ssh object for hub CP')
            Logger.info('getting wan ip of the hub cp')
            serial_number = machine_hash['cp-details'][0]['serial-number']
            cp_details = cloudObj.get_cloudport_details(serial_number=serial_number)

            tem = cloudObj.get_interface_details(cloudport_id=cp_details['id'], interface_name='wan0')
            hubobj = cloudObj.create_ssh_object(ip=tem['interface_addresses'][0]['ip'])
            hubobj.switch_to_root()
            cmd = 'tcpdump -ni any -Qin host 8.8.4.4 -tttt '

            data = hubobj.get_tcp_dump_data(cmd=cmd, number_of_packets=5)
            if tem['interface_addresses'][0]['ip'] in data:
                self.assertEqual(True, True)

        finally:
            Logger.info('Feature cleanup')
            Logger.info('De-attaching policy')
            self.de_attach_policy(policy_id=response['data']['wan_policy']['id'], site_id=site_id)
            Logger.info('Deleting the policy')
            self.delete_forwarding_policy(policy_hex_id=response['data']['wan_policy']['hex_id'])







if __name__ == '__main__':
    main()