import os, sys, time, re
import json
import datetime
from datetime import timedelta, datetime
from functools import wraps

import random
import sys

sys.path.append('../')
from modules.sshlib.ssh import ConnectionHandler
from custom_xmlrunner import setup_module_helper, main, CustomUnitTestCase
from modules.CloudOperations import CloudOperations



def setUpModule():
    global BasePath, Logger, resource_json_hash, cloudObj, resource_json_hash

    global machine_hash, cloudstation, inet_policy_json

    BasePath = os.path.abspath(os.path.dirname(__file__))
    Logger = setup_module_helper(__file__, alarm_timeout=2400)

    BasePath = os.path.join(os.path.dirname(__file__), '../data_bags/')

    resource_json_hash = {
        "cloudstation-config": os.path.join(BasePath, 'cloudstation.json'),
        "machine-config": os.path.join(BasePath, 'machine-config.json')
    }

    with open(resource_json_hash['machine-config']) as fp:
        machine_hash = json.load(fp)

    with open(resource_json_hash['cloudstation-config']) as fp:
        cloudstation = json.load(fp)

    bp = os.path.join(os.path.dirname(__file__), '../data_bags/script_data_bags/')
    resource_json_hash = {
        "inet_policy": os.path.join(bp, "inter_network_policy.json"),
    }
    with open(resource_json_hash['inet_policy'], 'r') as fr:
        inet_policy_json = json.load(fr)

    _populate_other_params()


def _populate_other_params():
    global cp_object, cloudObj, host_obj
    devices = [
        {
            "name": "hub",
            "ip": "172.11.35.21",
            "username": "lavelle",
            "password": "lavelle@3163"
        },
        {
            "name": "spoke-1",
            "ip": "172.11.35.22",
            "username": "lavelle",
            "password": "lavelle@3163"
        },
        {
            "name": "spoke-2",
            "ip": "172.11.35.23",
            "username": "lavelle",
            "password": "lavelle@3163"
        }
    ]
    temp_devices = devices
    host_obj = dict()

    for device in temp_devices:
        name = device.pop('name')
        host_obj[name] = ConnectionHandler(**device)


    Logger.info(host_obj)
    cloudObj = CloudOperations()


class Inet_policy(CustomUnitTestCase):
    data_map = {}

    def setup(self, json_file=None, data=None):
        pass

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        # Define policy configuartion data
        self = super(Inet_policy, cls).tearDownClass()
        pass

    @classmethod
    def _change_data(cls, json_data):
        """utility method to change/replace values in the json_data
        :param json_data: json dict to modify
        :return:
        """
        for k, v in cls.data_map.items():
            json_data = json_data.replace(str(k), str(v))
        return json_data


    def _retry_on_exception(func, max_tries=3, retry_sleep=0.3):
        """Mthod to retry on Exception
        :param max_tries: Integer , 3 - by default
        :return:
        """

        def wrapper(*args, **kwargs):
            for i in range(max_tries):
                try:
                    return func(*args, **kwargs)
                    break
                except Exception as err:
                    msg = "Got Exception:{}, retrying attempt: {}".format(err, i + 1)
                    print(msg)
                    if i + 1 == max_tries:
                        raise Exception("Got Exception in the last attempt: {}".format(err))
                    else:
                        time.sleep(retry_sleep)
                        continue

        return wrapper

    def prepare_other_param(self,serial_number,**interface_details):
        """
        This function helps to edit the existing lan interface details
        :param serial_number: CP serial number
        :return:
        """

        Logger.info('Getting cloud-port details of hub')
        cp_details = cloudObj.get_cloudport_details(serial_number=serial_number)
        payload = cloudObj.get_interface_details(cloudport_id=cp_details['id'], interface_name='lan0')

        payload.pop("cloudport_link")
        payload['addresses'] = payload.pop('interface_addresses')

        payload['addresses'].append(interface_details)

        url = cloudObj._base_url + 'cloudports/{}/interfaces/{}'.format(cp_details['id'],payload['id'])

        Logger.info(url,payload)
        response = cloudObj.put(url = url, data = json.dumps(payload))

        Logger.info(response)
        if response.status_code != 200:
            Logger.fatal('Failed to add lan interface to the CPE')
            return False
        return True

    def create_i_net_policy(self,**policy_details):
        """
        Function to create a inter-network policy in the CS
        :return:
        """
        Logger.info('creating a new inter_network_policy in CS')
        payload = inet_policy_json

        self.data_map['__policy-name__'] = policy_details.get('policy_name')
        self.data_map[8888] = policy_details.get('source_id')
        self.data_map[9999] = policy_details.get('destination_id')
        self.data_map[3333] = policy_details.get('vpn_profile_id')
        self.data_map['__encapsulation__'] = policy_details.get('encapsulation')

        payload = self._change_data(json.dumps(payload))

        url = cloudObj._base_url + 'inter_network_policies'

        response = cloudObj.post(url=url, data = payload)

        if response.status_code != 200:
            err_msg = 'Failed to attach inter-network policy : {}'.format(response.json())
            Logger.info(err_msg)
            return False
        else:
            Logger.info('Created a new inter-network policy')
            Logger.info(response.json())
            return response.json()

    def attach_inet_policy(self,profile_id,policy_id):
        """
        Function to attach a policy to  network-location profile
        :param profile_id: which site to attach the policy to
        :param policy_id: inet policy id to be attached
        :return: True/False
        """
        payload = {
            "attached_to_type": "Profile",
            "attached_to_id": str(profile_id)
        }
        url = cloudObj._base_url + 'inter_network_policies/{}/attach_policy'.format(policy_id)

        res = cloudObj.post(url=url, data = json.dumps(payload))
        Logger.info('Attaching inter-network-policy to location response : {}'.format(res.json()))
        if res.status_code != 200:
            Logger.info('Failed to attach the policy to profile')
            return False
        Logger.info('Attached policy to profile location ')
        return True

    def de_attach_policy(self,policy_id,profile_id):
        """
        Function to de-attach the forwarding policy form cs
        :param policy_id: policy-id
        :param site_id: site_id-id
        :return: de-attach's policy
        """
        url = cloudObj._base_url + 'inter_network_policies/{}/inter_network_policy_relations'.format(policy_id)
        payload = {
            "object_type": "Profile",
            "object_id": str(profile_id)
        }
        Logger.info(url)
        Logger.info(payload)

        response = cloudObj.post(url=url, data=json.dumps(payload))
        if response.status_code != 200:
            err_msg = 'Failed to de-attach policy Reason {}'.format(response.json())
            Logger.fatal(err_msg)
            return False
        Logger.info('De-attached policy {}'.format(response.json()))
        return response.json()

    def delete_inet_policy(self,policy_id):
        """
        Function to delete the policy from cs

        :return: True/False
        """
        url = cloudObj._base_url + 'inter_network_policies/{}'.format(policy_id)
        Logger.info(url)
        res = cloudObj.delete(url=url)
        Logger.info(res.json())
        if res.status_code != 200:
            Logger.info('failed to delete the inet policy')
            return False
        Logger.info('Policy deleted')
        return True

    def delete_network_group(self,ng_id):
        """
        Function to delete a network group
        :param ng_id: network_group ID
        :return: True/False
        """
        Logger.info('Deleting the network group, id {}'.format(ng_id))
        url = cloudObj._base_url + 'network_groups/{}'.format(ng_id)
        Logger.info(url)
        res = cloudObj.delete(url=url)
        Logger.info('API response {} '.format(res.json()))
        if res.status_code != 200:
            Logger.info('Failed to delete the network-group')
            return False
        Logger.info('Network group deleted successfully')
        return True



    def cleanUp(self,policy_id,profile_id,ng_id):
        """
        Function to clean-up the created config from policy feature
        :return:
        """
        Logger.info('!!De-attaching the inet-policy!!')
        self.de_attach_policy(policy_id=policy_id,profile_id=profile_id)
        Logger.info('Deleting the policy')
        self.delete_inet_policy(policy_id=policy_id)

        Logger.info('Deleting the created network-group')
        self.delete_network_group(ng_id=ng_id)


    def test_inter_networking_policy(self):
        """
               Testcase to test the functionality of inter-networking policy
        """
        serial_number = machine_hash['cp-details'][0]['serial-number']
        interface_details = {
            "ip": "10.117.53.1",
            "prefix_length": "24",
            "is_primary": "no"
        }

        self.write_step('Preparing the pre-requsites required')
        if self.prepare_other_param(serial_number,**interface_details): #uncomment the line

            details = {
                "ng_name": 'network_group_{}'.format(random.randint(1, 100)),
                "addresses": cloudObj.get_subnet_id(ip_names=['10.117.53.1','10.117.52.1'])
            }
            self.write_step('Creating new network group')
            ng_details = cloudObj.create_network_group(**details)

            self.write_step('Creating new inter-networking policy')
            network_id = cloudObj.get_network_group_details()
            policy_details = {
                "policy_name" : 'inet_policy_{}'.format(random.randint(1, 100)),
                "destination_id": ng_details['data']['network_group']['id'],
                "source_id": int(network_id['Default-network-gp-id']),
                "vpn_profile_id": int(cloudObj.get_default_vpn_profile_id()),
                "encapsulation": "lntun"
            }
            inet_policy = self.create_i_net_policy(**policy_details)
            res = cloudObj.get_default_network_location_profile_id()

            try:
                self.write_step('Attach the inter-networking policy')

                data = self.attach_inet_policy(profile_id=res['id'],
                                               policy_id=inet_policy['data']['inter_network_policy']['id'])
                time.sleep(120)

                self.write_step('Sending traffic ')
                host_obj['spoke-2'].send_traffic(client_ip='10.117.51.11', rate=2, time_limit=120)

                self.write_step('Verifying traffic from other side')
                host_obj['spoke-1'].switch_to_root()
                result = host_obj['spoke-1'].get_tcp_dump_data(cmd='tcpdump -ni any -Qin host 10.117.52.11',
                                                               number_of_packets=5)
                if '10.117.52.11' not in result:
                    raise Exception('FAILED')
            finally:
                Logger.info('cleanup feature')
                self.cleanUp(policy_id=inet_policy['data']['inter_network_policy']['id'],profile_id=res['id'], ng_id=ng_details['data']['network_group']['id'])







if __name__ == '__main__':
    main()