import sys, os, re, time, datetime
import requests
from multiprocessing import Process
import sched
import threading
import json


f=open('nw_json.json', 'r')
nw_data = json.loads(f.read())
f.close()



#CS_URL = 'https://lavelle-ce.devcloudstation.io:443'
CS_URL = 'https://ce-lab.devcloudstation.io:443'
CS_REST_API = {
        'login': "".join([CS_URL, '/api/v1/login/']),
        'nw_grps': "".join([CS_URL, '/rules/network_groups/']),
        'rules_t1': "".join([CS_URL, '/rules/t1/']),
        'rules_t2': "".join([CS_URL, '/rules/t2/']),
        'rules_t3': "".join([CS_URL, '/rules/t3/']),
        'rules_t4': "".join([CS_URL, '/rules/t4/']),
        'mpls_profiles': "".join([CS_URL, '/rules/mpls_profiles/']),
        'nat_info': "".join([CS_URL, '/rules/nat_info/']),
        'prov_event': "".join([CS_URL, '/api/v1/provision/event/']),
        'prov_config': "".join([CS_URL, '/api/v3/provision/config/']),
        'hello': "".join([CS_URL, '/route/hello/']),
        'keyclient': "".join([CS_URL, '/rules/vpn_keys']),
        'pkg_list': "".join([CS_URL, '/api/v1/upgrade/']),
        'iface_update': "".join([CS_URL, '/api/v2/route/interface_update/']),
        'get_dns_proxy': "".join([CS_URL, '/api/v2/rules/dns_proxy/']),
        'post_peerstate_updates': "".join([CS_URL, '/api/v2/route/peering_state_update']),
        'post_system_update': "".join([CS_URL, '/api/v2/system_update/']),
        'get_global_config': "".join([CS_URL, '/api/v2/global_config_pull_req/']),
        'post_syncdate': "".join([CS_URL, '/api/v2/syncdate/send_date/']),
        'post_discovered_subnets': "".join([CS_URL, '/api/v2/route/discovered_subnets/']),
        'get_acls': "".join([CS_URL, '/api/v2/rules/acls/']),
        'get_qos': "".join([CS_URL, '/api/v2/rules/qos/']),
        'get_policers': "".join([CS_URL, '/api/v2/rules/policers/']),
        'get_lbpolicy': "".join([CS_URL, '/api/v2/rules/lb_policy/']),
        'get_fwd_rules': "".join([CS_URL, '/api/v3/rules/forward/']),
}


def generate_uuid(serial_number):

    uuid = 0
    uuid |= serial_number << 128 - 128
    uuid |= 1 << 128 - 96
    uuid |= 1 << 128 - 92
    uuid |= 1 << 128 - 88

    uuid = format(uuid, 'x').zfill(32)

    return ':'.join(uuid[i]+uuid[i+1] for i in xrange(0, len(uuid)-1, 2))


class ControlPlaneProcess(Process):

    def __init__(self, name=None, serial_number=None, *args, **kwargs):
        Process.__init__(self, name=name, args=args, kwargs=kwargs)  # Required by base class
        self.args = args
        self.kwargs = kwargs
        self.serial_number = serial_number
        self.uuid = generate_uuid(serial_number)
        print(self.uuid)
        self.session_id=None
        self.verify_ssl = False
        self.login_retry = 3
        self.attempts = 0
        self.loggedin = False
        self.headers = {
            'Accept': 'application/json',
            'Accept-Encoding': 'gzip'
        }
        self.headers_v2 = {
            'Accept': 'application/json',
            'Accept-Encoding': 'gzip',
            'VERSION-COMPAT': 'v2'
        }
        self.body = {
            'uuid': self.uuid 
        }
        self.url = CS_URL
        self.apis =CS_REST_API
        """
        self.url = 'https://lavelle-ce.devcloudstation.io:443'
        self.apis = {
                'login': '/api/v1/login/',
                'nw_grps': '/rules/network_groups/',
                'rules_t1': '/rules/t1/',
                'rules_t2': '/rules/t2/',
                'rules_t3': '/rules/t3/',
                'rules_t4': '/rules/t4/',
                'mpls_profiles': '/rules/mpls_profiles/',
                'nat_info': '/rules/nat_info/',
                'prov_event': '/api/v1/provision/event/',
                'prov_config': '/api/v3/provision/config/',
                'hello': '/route/hello/',
                'keyclient': '/rules/vpn_keys',
                'pkg_list': '/api/v1/upgrade/',
                'iface_update': '/api/v2/route/interface_update/',
                'get_dns_proxy': '/api/v2/rules/dns_proxy/',
                'post_peerstate_updates': '/api/v2/route/peering_state_update',
                'post_system_update': '/api/v2/system_update/',
                'get_global_config': '/api/v2/global_config_pull_req/',
                'post_syncdate': '/api/v2/syncdate/send_date/',
                'post_discovered_subnets': '/api/v2/route/discovered_subnets/',

                'get_acls': '/api/v2/rules/acls/',
                'get_qos': '/api/v2/rules/qos/',
                'get_policers': '/api/v2/rules/policers/',
                'get_lbpolicy': '/api/v2/rules/lb_policy/',
                'get_fwd_rules': '/api/v3/rules/forward/',
        }
        """
        self.session_id_locked = False

        #resp = requests.get(url, params = body, headers=headers, timeout = REQUEST_TIMEOUT, verify=verify_ssl())
        #resp = requests.post(url, data = body, headers=headers, timeout = REQUEST_TIMEOUT, verify=verify_ssl())


    def thread_1500s(self):

        timer = 30
        sent_time =  time.time()
        while 1:
            sent_time, prev_sent_time = time.time(), sent_time
            if timer - (sent_time - prev_sent_time) > 0: 
                try:
                    resp = requests.get(self.apis['login'], params=self.body, timeout=5, verify=self.verify_ssl)
                    #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                    if resp.status_code == 200:
                        timer=1500
                        if not self.session_id_locked:
                            self.session_id_locked=True
                            self.session_id = resp.json()['session_id']
                            self.headers.update({'authorization': 'x-lavelle-auth sessionid=%s'%self.session_id})
                            self.headers_v2.update({'authorization': 'x-lavelle-auth sessionid=%s'%self.session_id})
                            self.loggedin =True
                            self.session_id_locked=False
                            self.attempts=0
                            #return
                        else:
                            time.sleep(1)
                            #return self.login()
                            continue
                    else:
                        self.loggedin =False
                        #print('Failed to login')
                        #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                        if self.attempts < self.login_retry: 
                            time.sleep(1)
                            self.attempts=self.attempts+1
                            #return self.login()
                            continue
                        else:
                            self.attempts=0
                except Exception, e:
                    print("Exception - ReadTimeout: %s" %e)
                time.sleep(timer - (time.time() - sent_time))

    def thread_60s(self):
        timer = 60
        #print('60 ', self.loggedin)
        while 1:
          sent_time =  time.time()
          if self.loggedin:
            sent_time, prev_sent_time = time.time(), sent_time
            if timer - (sent_time - prev_sent_time) > 0: 
                # post sync date
                try:
                    resp=requests.post(self.apis['post_syncdate'], data={'uuid': self.uuid, 'date': datetime.datetime.utcnow().strftime('%a %b %d %H:%M:%S UTC %Y\n')}, 
                                       headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '========')
                #if not resp.status_code ==200: 
                    #print('error hello')
                    #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '========')

                # oftable 1
                    resp=requests.get(self.apis['rules_t1'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error of table 1')

                # oftable 2
                    resp=requests.get(self.apis['rules_t2'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error oftable 2')

                 # oftable 3
                    resp=requests.get(self.apis['rules_t3'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error of table 3')

                # oftable 4 
                    resp=requests.get(self.apis['rules_t4'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error of table 4')

                # nat info
                    resp=requests.get(self.apis['nat_info'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error natinfo')

                # mpls profile
                    resp=requests.get(self.apis['mpls_profiles'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error mplsprofile')

                # nw grp
                    resp=requests.get(self.apis['nw_grps'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error nw_grps')

                # get fwd rules
                    resp=requests.get(self.apis['get_fwd_rules'], params={'uuid': self.uuid}, headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error nw_grps')

                # get lbpolicy
                    resp=requests.get(self.apis['get_lbpolicy'], params={'uuid': self.uuid}, headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error nw_grps')

                # get policers
                    resp=requests.get(self.apis['get_policers'], params={'uuid': self.uuid}, headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error nw_grps')

                # get qos 
                    resp=requests.get(self.apis['get_qos'], params={'uuid': self.uuid}, headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error nw_grps')

                #get acls
                    resp=requests.get(self.apis['get_acls'], params={'uuid': self.uuid}, headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error nw_grps')

                # post discovnered subnets
                    resp=requests.post(self.apis['post_discovered_subnets'], data={'uuid': self.uuid, 'routes': ['[]']}, headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '========')
                #if not resp.status_code ==200: 
                    #print('error hello')
                    #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '========')
                except Exception, e:
                    print("Exception: %s" %e)

                time.sleep(timer - (time.time() - sent_time))

    def thread_30s(self):
        #print('30 ', self.loggedin)
        timer = 30

        while 1:
          sent_time =  time.time()
          if self.loggedin:
            sent_time, prev_sent_time = time.time(), sent_time
            if timer - (sent_time - prev_sent_time) > 0: 

                # post system update
                try:
                    #print('posting') 
                    resp=requests.post(self.apis['post_system_update'], data={'uuid': self.uuid,
                            'system': json.dumps( {"version": {"failover-device-state": None, "sys-pkg-version": "9.0.0_R_8b3580b",
                            "svc-pkg-version": "9.0.0_R_8b3580b"}} )}, headers=self.headers_v2, timeout=1, verify=True)
                #resp=requests.post(self.apis['post_system_update'], data=self.post_system_update_data, headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '========')
                #if not resp.status_code ==200: 
                    #print('error hello')
                    #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '========')


                # post peerstate updates

                    route_peering_status = json.dumps( {'wan': [], 'lan': []} )
                    resp=requests.post(self.apis['post_peerstate_updates'], data={'uuid': self.uuid,
                         'route_peering_status': route_peering_status, 'current_unixtime': [str(int(time.time()))] },
                          headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '========')
                #if not resp.status_code ==200: 
                    #print('error hello')
                    #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '========')

                # get global config
                    resp = requests.get(self.apis['get_global_config'], params={'uuid': self.uuid,
                        'api_list': """{"/api/v2/rules/policers/": "v2", "/api/v2/rules/lb_policy/": "v2", "/api/v2/rules/network_groups/": "v2", "/api/v2/rules/dns_proxy/": "v2", "/api/v3/provision/config/": "v3","/api/v2/rules/mpls_profiles/": "v2", "/api/v3/rules/forward/": "v3", "/api/v2/rules/qos/": "v2", "/api/v2/rules/nat_info/": "v2", "/api/v2/rules/acls/": "v2"}""" }, 
                        headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error mplsprofile')
           
                # keyclient
                    resp=requests.get(self.apis['keyclient'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error keyclient')

                # iface update
                    parameter_pass = {
                        'uuid': self.uuid,
                        'system': [json.dumps({
                        "interfaces" : nw_data[str(self.serial_number)], 
                        "ports": [
                         {"name": "eth0", "duplexity":  "full", "auto_neg": "yes", "state": "up", "switch": "sw0", "speed": "1000"}, 
                         {"name": "eth1", "duplexity": "unknown", "auto_neg": "yes", "state": "down", "switch": "sw0", "speed": "0"}, 
                         {"name": "eth2", "duplexity": "full", "auto_neg": "yes", "state": "up", "switch": "sw0", "speed": "100"},
                         {"name": "eth3", "duplexity": "full", "auto_neg": "yes", "state": "up", "switch": "sw0", "speed": "1000"},
                         {"name": "usb-2", "duplexity": "unknown", "auto_neg": "unknown", "state": "unknown", "switch": "sw0", "speed": "0"},
                         {"name": "usb-3", "duplexity": "unknown", "auto_neg": "unknown", "state": "unknown", "switch": "sw0", "speed": "0"}
                            ],  
                        })] 
                    }
                #print(parameter_pass)
                #resp=requests.post(self.apis['iface_update'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                    resp=requests.post(self.apis['iface_update'], data=parameter_pass, headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp, resp.json(), resp.headers)
                    if not resp.status_code ==200: 
                        print('error iface_update')



                # event provision
                    resp=requests.get(self.apis['prov_event'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error event prov')

                # config provision
                    resp=requests.get(self.apis['prov_config'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error config prov')


                # get dns proxy
                    resp=requests.get(self.apis['get_dns_proxy'], params={'uuid': self.uuid}, headers=self.headers_v2, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error get_dns_proxy')

                # get pkg list
                    resp=requests.get(self.apis['pkg_list'], params={'uuid': self.uuid}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '===========')
                #print(sys._getframe().f_code.co_name, time.time(), resp)
                    if not resp.status_code ==200: 
                        print('error pkglist')
                except Exception, e:
                    print("Exception: %s" %e)
                time.sleep(timer - (time.time() - sent_time))

    def thread_120s(self):
        timer = 120
        #print('120 ', self.loggedin)
        while 1:
          sent_time =  time.time()
          if self.loggedin:
            sent_time, prev_sent_time = time.time(), sent_time
            if timer - (sent_time - prev_sent_time) > 0: 

                # hello
                try:
                    resp=requests.post(self.apis['hello'], params={'uuid': self.uuid, 'txid': str(time.time())}, headers=self.headers, timeout=1, verify=True)
                #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '========')
                #if not resp.status_code ==200: 
                    #print('error hello')
                    #print(self.serial_number, sys._getframe().f_code.co_name, time.time(), resp, resp.text, '========')
                except Exception, e:
                    print("Exception - ConnectTimeout: %s" %e)
                time.sleep(timer - (time.time() - sent_time))

    def run(self):
        #    'authorization': 'x-lavelle-auth sessionid=%s'%self.session_id
        thr_1500 = threading.Thread(target = self.thread_1500s)
        thr_30 = threading.Thread(target = self.thread_30s)
        thr_60 = threading.Thread(target = self.thread_60s)
        thr_120 = threading.Thread(target = self.thread_120s)

        thr_1500.start()
        time.sleep(2)
        thr_30.start()
        thr_60.start()
        thr_120.start()
        while 1:pass


#for sl in range(6050, 6070):
#import subprocess
#x=subprocess.Popen('sh delete_process.sh', shell=True)
#x.communicate()


skip = [6006, 6010]
processes = []
#for sl in xrange(6001, 6351):
for sl in xrange(6001, 6003):
    c= ControlPlaneProcess('ctrl-%s'%sl, sl)
    c.start()
    time.sleep(1)
    if not sl%10: time.sleep(2)

while 1: pass
"""
def main():
    skip = [6006, 6010]
    processes = []
    for sl in xrange(6001, 6002):
        if sl in skip: continue
        #sl = 6003
        #processes.append( ControlPlaneProcess('ctrl-%s'%sl, sl) )
        #c.start()

        c= ControlPlaneProcess('ctrl-%s'%sl, sl)
        c.start()
        #time.sleep(10)
        #if not sl%10: time.sleep(2)

    print(processes)
    #map(start_process, processes)
    #while True:
    #    pass

main()
"""
