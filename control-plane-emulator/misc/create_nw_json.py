import json
import netaddr
import sys
import re


cpe_start = 1539
cpe_counts = 1
lan_start_addr = '173.11.12.0/24'
inet_start_addr = '192.168.20.1/24'
mpls_start_addr = '30.30.33.0/24'
nws = []


f=open('cnid.txt', 'r')
data = f.read()
f.close()

cnid = {}
cnid_datas = data.split('\n')
for cnid_data in cnid_datas:
    z= re.search('(\d+)\s+(([0-9a-fA-F]{2}\:){3}[0-9a-fA-F]{2})', cnid_data)
    if z: cnid[z.group(1)] = int(re.sub(':', '', z.group(2)), 16)

for k, v in cnid.iteritems():
    print(k, v)

#################
#  LAN
################
nw = netaddr.IPNetwork(lan_start_addr)
mask=nw.prefixlen

nws.append((str(netaddr.IPAddress(nw.first+1)), str(nw.netmask)))
for i in range(cpe_counts-1):
    next_net = netaddr.IPNetwork( '%s/%s'%( str(netaddr.IPAddress(nw.last+1)), mask))
    nws.append((str(netaddr.IPAddress(next_net.first+1)), str(next_net.netmask)))
    del nw
    nw = next_net

#for nw in nws:
#   print(nw)


#################
#  INET
################
inets = []
inet = netaddr.IPNetwork(inet_start_addr)
mask = inet.prefixlen
#for
count=1
while count <= cpe_counts:
    gw = 0
    for host in inet.iter_hosts():
        if gw==0: 
            gw_addr = host
            gw=gw+1
            continue
        inets.append((str(host), str(inet.netmask), str(gw_addr)))
        count=count+1
    
    next_net = netaddr.IPNetwork( '%s/%s'%( str(netaddr.IPAddress(inet.last+1)), mask))
    del inet
    inet = next_net

for inet in inets:
    print(inet)

#for i in range(cpe_counts-1):
#    next_net = netaddr.IPNetwork( '%s/%s'%( str(netaddr.IPAddress(nw.last+1)), mask))
#    nws.append((str(next_net.network), str(next_net.netmask)))
#    del nw
#    nw = next_net

#################
#  MPLS
################

mpls_nets = []
mpls_net = netaddr.IPNetwork(mpls_start_addr)
mask = mpls_net.prefixlen
#for
count=1
while count <= cpe_counts:
    gw=0
    for host in mpls_net.iter_hosts():
        if gw==0: 
            gw_addr = host
            gw=gw+1
            continue
        if str(host)==str(mpls_net.first): continue
        mpls_nets.append((str(host), str(mpls_net.netmask), str(gw_addr)))
        count=count+1
    
    next_net = netaddr.IPNetwork( '%s/%s'%( str(netaddr.IPAddress(mpls_net.last+1)), mask))
    del mpls_net
    mpls_net = next_net

for inet in mpls_nets:
    print(inet)



nw_dict = {}
for i, nw in enumerate(range(cpe_counts), 0):

    lan0 = {"protocol": "Static", "name": "lan0", "ip": [{"ip": nws[i][0], "mask": nws[i][1]}], "vlan": "0", "state": "up", "role": "lan", "port": "eth0"}
    wan0 = {"protocol": "DHCP", "name": "wan0", "gw_wrstate": "unreachable", "ip": [], "vlan": "0", "mpls": "no", "clid": cnid[str(i+cpe_start)]+1, "endpt_wrstate": "unreachable", "state": "down", "wrstate": "unreachable", "role": "wan", "port_type": "Ethernet", "dns": "", "apn": "", "gateway": "0.0.0.0", "port": "eth1", "cs_wrstate": "unreachable"}
    wan1 = {"protocol": "Static", "name": "wan1", "gw_wrstate": "disabled", "ip": [{"ip": mpls_nets[i][0], "mask": mpls_nets[i][1]}], "vlan": "0", "mpls": "yes", "clid": cnid[str(i+cpe_start)]+2, "endpt_wrstate": "unreachable", "state": "up", "wrstate": "unreachable", "role": "wan", "port_type": "Ethernet", "dns": "8.8.8.8", "apn": "", "gateway": mpls_nets[i][2], "port": "eth2", "cs_wrstate": "error"}
    wan2 = {"protocol": "DHCP", "name": "wan2", "gw_wrstate": "disabled", "ip": [{"ip": inets[i][0], "mask": inets[i][1]}], "vlan": "0", "mpls": "no", "clid": cnid[str(i+cpe_start)]+3, "endpt_wrstate": "reachable", "state": "up", "wrstate": "reachable", "role": "wan", "port_type": "Ethernet", "dns": "8.8.8.8", "apn": "", "gateway": inets[i][2], "port": "eth3", "cs_wrstate": "reachable"}

    nw_dict.update({'%s'%(i+cpe_start): [ lan0, wan0, wan1, wan2 ] })
    
    
f=open('nw_json.json', 'w+')
f.write(json.dumps(nw_dict))
f.close()



