// To Do
// 1. Need to the change the API header version dynamic by moving to a 
//    config files
// 2. Need to combine v2/v1 and other version header, so that when the
//    new version comes it will be easy to add new one
// 3. Need to post dynamic routes from the CPE to CS
// 4. Need to post mulitple new interface
// 5. Dynamically identify the current supported version for each API

package main
import (
    "fmt"
    "strings"
    "net/http"
    "net/http/httputil"
    "net/url"
    "time"
    "sync"
    "bytes"
    "encoding/json"
    "io/ioutil"
    //"runtime"
)

const (
    //CS_url = "https://ce-lab.devcloudstation.io:443"
    //CS_url = "https://scale-demo.cloudstation.io:443"
    CS_url = "https://bulk-code-test.devcloudstation.io:443"
)

var (
    failure = 0
    client = &http.Client{}
    CS_rest_API = map[string]string {
        "login": strings.Join([]string{CS_url, "/api/v2/login/"}, ""),
        "nw_grps": strings.Join([]string{CS_url, "/api/v4/rules/network_groups/"}, ""),
        "rules_t1": strings.Join([]string{CS_url, "/api/v4/rules/acls/"}, ""),
        "rules_t2": strings.Join([]string{CS_url, "/api/v4/rules/qos/"}, ""),
        "rules_t3": strings.Join([]string{CS_url, "/api/v4/rules/policers/"}, ""),
        "rules_t4": strings.Join([]string{CS_url, "/api/v4/rules/lb_policy/"}, ""),
        "mpls_profiles": strings.Join([]string{CS_url, "/api/v2/rules/mpls_profiles/"}, ""),
        "nat_info": strings.Join([]string{CS_url, "/api/v2/rules/nat_policy/"}, ""),
        "prov_event": strings.Join([]string{CS_url, "/api/v1/provision/event/"}, ""),
        "prov_config": strings.Join([]string{CS_url, "/api/v3/provision/config/"}, ""),
        "hello": strings.Join([]string{CS_url, "/route/hello/"}, ""),
        "keyclient": strings.Join([]string{CS_url, "/api/v2/rules/vpn_keys"}, ""),
        "pkg_list": strings.Join([]string{CS_url, "/api/v1/upgrade/"}, ""),
        "iface_update": strings.Join([]string{CS_url, "/api/v2/route/interface_update/"}, ""),
        "get_dns_proxy": strings.Join([]string{CS_url, "/api/v2/rules/dns_proxy/"}, ""),
        "post_peerstate_updates": strings.Join([]string{CS_url, "/api/v2/route/peering_state_update"}, ""),
        "post_system_update": strings.Join([]string{CS_url, "/api/v2/system_update/"}, ""),
        "get_global_config": strings.Join([]string{CS_url, "/api/v2/global_config_pull_req/"}, ""),
        "post_syncdate": strings.Join([]string{CS_url, "/api/v2/syncdate/send_date/"}, ""),
        "post_discovered_subnets": strings.Join([]string{CS_url, "/api/v2/route/discovered_subnets/"}, ""),
        "get_acls": strings.Join([]string{CS_url, "/api/v2/rules/acls/"}, ""),
        "get_qos": strings.Join([]string{CS_url, "/api/v2/rules/qos/"}, ""),
        "get_policers": strings.Join([]string{CS_url, "/api/v2/rules/policers/"}, ""),
        "get_lbpolicy": strings.Join([]string{CS_url, "/api/v2/rules/lb_policy/"}, ""),
        "get_fwd_rules": strings.Join([]string{CS_url, "/api/v4/rules/forward/"}, ""),
        }
    Headers_v1 = map[string]string{
            "Accept":          "application/json",
            "Accept-Encoding": "gzip",
            "AUTHORIZATION":   "x-lavelle-auth sessionid=",
        }
    Headers_v2 = map[string]string{
            "Accept": "application/json",
            "Accept-Encoding": "gzip",
            "VERSION-COMPAT": "v2",
            "AUTHORIZATION":   "X-LAVELLE-AUTH sessionid=",
        }
    spoke_count int
    start_serial_no int
    cloud_port_detail = map[int]*CloudPort{}
    Dictionary map[string][]string
    file, _ = ioutil.ReadFile("nw_json.json")
    err = json.Unmarshal(file, &Dictionary)
)

type (
    CloudPort struct {
        uuid          string
        serial_no     int
        login_status  bool
        failure_count map[string]int
        headers_v1    map[string]string
        headers_v2    map[string]string
    }
)

/*To convert the serial number into UUID, thus making the CPE simulation easier*/
func generate_uuid(serial_number int)(string){
    uuid := 0
    uuid |= serial_number //make to UUID start with serial number
    uuid |= 1 << (128 - 96)
    uuid |= 1 << (128 - 92)
    uuid |= 1 << (128 - 88)

    // UUID is a 32byte Hexa string, thus converting the integer to hexadecimal 
    string_uuid := fmt.Sprintf("%0.32x", uuid)

    result := []string{}
    // Converting the 32 hexadecimal integer into colon separated string
    for x := 0; x < len(string_uuid); x += 2 {
        result = append(result, string_uuid[x:x+2])
    }
    return strings.Join(result, ":")
}

func http_request(req_type string, req_url string, params map[string]string, header map[string]string)(*http.Response, error){
    // Creating a new HTTP(S) GET request
    fmt.Println(req_url)
    request, _ := http.NewRequest(req_type, req_url, nil)
    q := url.Values{}

    // Adding the parameters value to the HTTP(S) GET request
    for key, value := range params {
        q.Add(key, value)
    }
    if len(header) != 0 {
        // Adding the header value to the HTTP(S) GET request
        for key, value := range header{
            request.Header.Add(key, value)
        }
    }
    request.URL.RawQuery = q.Encode()
    bytes, _ := httputil.DumpRequest(request, true)
    fmt.Println(string(bytes))
    response, err := client.Do(request)
    return response, err
}

func validate_response(res_name string, res *http.Response, cpe *CloudPort){
    if res.StatusCode != 200 {
        if _, ok := cpe.failure_count[res_name]; ok {
            cpe.failure_count[res_name] += 1
        } else {
            cpe.failure_count = make(map[string]int)
            cpe.failure_count[res_name] = 0
        }
        fmt.Println(res)
    } else {
        fmt.Println("SUCCESS")
        fmt.Println(res)
    }
}

func sent_http_syncdate(cpe *CloudPort){
    res, _ := http_request("POST", CS_rest_API["post_syncdate"],
                             map[string]string{"uuid": cpe.uuid, "date": time.Now().Format("Mon Jan 02 15:04:05 UTC 2006\\n")},
                             cpe.headers_v2)
    validate_response("post_syncdate", res, cpe)
}

func sent_http_rules_acl(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["rules_t1"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v1)
    validate_response("rules_t1", res, cpe)
}

func sent_http_rules_qos(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["rules_t2"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v1)
    validate_response("rules_t2", res, cpe)
}

func sent_http_rules_trl(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["rules_t3"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v1)
    validate_response("rules_t3", res, cpe)
}

func sent_http_rules_lb(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["rules_t4"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v1)
    validate_response("rules_t4", res, cpe)
}

func sent_http_nat_info(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["nat_info"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v1)
    validate_response("nat_info", res, cpe)
}

func sent_http_mpls_profile(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["mpls_profiles"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v1)
    validate_response("mpls_profiles", res, cpe)
}

func sent_http_nw_grps(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["nw_grps"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v1)
    validate_response("nw_grps", res, cpe)
}

func sent_http_get_fwd_rules(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["get_fwd_rules"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v2)
    validate_response("get_fws_rules", res, cpe)
}

func sent_http_get_lbpolicy(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["get_lbpolicy"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v2)
    validate_response("get_lbpolicy", res, cpe)
}

func sent_http_get_policer(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["get_policers"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v2)
    validate_response("get_policers", res, cpe)
}

func sent_http_get_qos(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["get_qos"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v2)
    validate_response("get_qos", res, cpe)
}

func sent_http_get_acls(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["get_acls"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v2)
    validate_response("get_acls", res, cpe)
}

func sent_http_post_discovered_subnets(cpe *CloudPort){
    res, _ := http_request("POST", CS_rest_API["post_discovered_subnets"], map[string]string{"uuid": cpe.uuid, "routes": "[]"},
                              cpe.headers_v2)
    validate_response("post_discovered_subnets", res, cpe)
}

func sent_http_post_system_updates(cpe *CloudPort){
    value, _ := json.Marshal(map[string]map[string]string{"version": map[string]string{"failover-device-state": "null",
                             "sys-pkg-version": "10.0.1_R_7e50d82", "svc-pkg-version": "10.0.1_R_7e50d82"}})
    res, _ := http_request("POST", CS_rest_API["post_system_update"], map[string]string{"uuid": cpe.uuid,
                             "system": string(value)}, cpe.headers_v2)
    validate_response("post_system_updates", res, cpe)
}

func sent_http_post_peerstate_updates(cpe *CloudPort){
    value, _ := json.Marshal(map[string][]string{"wan": []string{}, "lan": []string{},
                             "current_unixtime": []string{string(time.Now().Unix())}})
    res, _ := http_request("POST", CS_rest_API["post_peerstate_updates"], map[string]string{"uuid": cpe.uuid,
                           "route_peering_status": string(value)}, cpe.headers_v2)
    validate_response("post_system_updates", res, cpe)
}

func sent_http_get_global_config(cpe *CloudPort){

    values, _ := json.Marshal(map[string]string{"/api/v4/rules/network_groups/": "v4", "/api/v4/rules/security_policy/": "v4",
                                                "/api/v2/rules/dns_proxy/": "v2", "/api/v2/rules/mpls_profiles/": "v2",
                                                "/api/v2/rules/nat_policy/": "v2", "/api/v4/rules/forward/": "v4",
                                                "/api/v4/rules/acls/": "v4", "/api/v4/rules/qos/": "v4", "/api/v4/rules/policers/": "v4",
                                                "/api/v4/provision/config/": "v4", "/api/v4/rules/lb_policy/": "v4"})
    res, _ := http_request("GET", CS_rest_API["get_global_config"], map[string]string{"uuid": cpe.uuid,
                             "api_list": string(values)}, cpe.headers_v2)
    validate_response("get_global_config", res, cpe)
}

func sent_http_get_keyclient(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["keyclient"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v1)
    validate_response("get_keyclient", res, cpe)
}

func sent_http_post_iface_update(cpe *CloudPort){
    value, _ := json.Marshal(map[string]interface{}{
                    "interfaces" : Dictionary[string(cpe.serial_no)],
                    "ports": []map[string]string{
                     map[string]string{"name": "eth0", "duplexity":  "full", "auto_neg": "yes", "state": "up", "switch": "sw0", "speed": "1000"},
                     map[string]string{"name": "eth1", "duplexity": "unknown", "auto_neg": "yes", "state": "down", "switch": "sw0", "speed": "0"},
                     map[string]string{"name": "eth2", "duplexity": "full", "auto_neg": "yes", "state": "up", "switch": "sw0", "speed": "100"},
                     map[string]string{"name": "eth3", "duplexity": "full", "auto_neg": "yes", "state": "up", "switch": "sw0", "speed": "1000"},
                     map[string]string{"name": "usb-2", "duplexity": "unknown", "auto_neg": "unknown", "state": "unknown", "switch": "sw0", "speed": "0"},
                     map[string]string{"name": "usb-3", "duplexity": "unknown", "auto_neg": "unknown", "state": "unknown", "switch": "sw0", "speed": "0"}}})
    //var parameter_pass = map[string]string{}
    res, _ := http_request("POST", CS_rest_API["iface_update"], map[string]string{"uuid": cpe.uuid, "system": string(value)}, cpe.headers_v2)
    validate_response("get_iface_update", res, cpe)
}

func sent_http_get_prov_event(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["prov_event"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v1)
    validate_response("get_prov_event", res, cpe)
}

func sent_http_get_prov_config(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["prov_config"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v1)
    validate_response("get_prov_config", res, cpe)
}

func sent_http_get_dns_proxy(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["get_dns_proxy"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v2)
    validate_response("get_prov_config", res, cpe)
}

func sent_http_get_pkg_list(cpe *CloudPort){
    res, _ := http_request("GET", CS_rest_API["pkg_list"], map[string]string{"uuid": cpe.uuid}, cpe.headers_v1)
    validate_response("get_prov_config", res, cpe)
}

func sent_http_hello_msg(cpe *CloudPort){
    res, _ := http_request("POST", CS_rest_API["hello"], map[string]string{"uuid": cpe.uuid, "txid": string(time.Now().Unix())}, cpe.headers_v1)
    validate_response("get_prov_config", res, cpe)
}

func sent_request_every_30s(cpe *CloudPort){
    sent_http_post_system_updates(cpe)
    //sent_http_post_peerstate_updates(cpe)
    //sent_http_get_global_config(cpe)
    //sent_http_get_keyclient(cpe)
    //sent_http_post_iface_update(cpe)
    //sent_http_get_prov_event(cpe) //404 Page not found
    //sent_http_get_prov_config(cpe)
    //sent_http_get_dns_proxy(cpe)
    //sent_http_get_pkg_list(cpe) //404 Page not found
}

func sent_request_every_60s(cpe *CloudPort){
    //sent_http_syncdate(cpe)
    //sent_http_rules_acl(cpe)
    //sent_http_rules_qos(cpe)
    //sent_http_rules_trl(cpe)
    //sent_http_rules_lb(cpe)
    //sent_http_nat_info(cpe)
    //sent_http_mpls_profile(cpe)
    //sent_http_nw_grps(cpe)
    //sent_http_get_fwd_rules(cpe)
    //sent_http_get_lbpolicy(cpe)
    //sent_http_get_policer(cpe)
    //sent_http_get_qos(cpe)
    //sent_http_get_acls(cpe)
    //sent_http_post_discovered_subnets(cpe)
}

func sent_request_every_120s(cpe *CloudPort){
    sent_http_hello_msg(cpe) //404 Page not found
}

func sent_request_every_1500s(cpe *CloudPort){
    sent_http_login(cpe)
}

func sent_http_login(cpe *CloudPort)(bool){
    for retry_count := 0; retry_count < 10 ; retry_count++ {
        res, _ := http_request("GET", CS_rest_API["login"], map[string]string{"uuid": cpe.uuid}, map[string]string{})
        fmt.Println(res)

        defer res.Body.Close()
        buffer := new(bytes.Buffer)
        buffer.ReadFrom(res.Body)
        /*
        buf.off := 0
        fmt.Println(buffer.buf[0:2])
        reader :=strings.NewReader("")
        n, err := reader.Read(buf)
        fmt.Println(string(buf[n]))
        fmt.Println("jj")
        fmt.Println(buffer)
        */
        newStr := buffer.String()
        newStr = strings.Replace(strings.Replace(strings.Replace(newStr, "{", "", -1), "}", "", -1), "\"", "", -1)
        newStr_list := strings.Split(newStr, ",")
        newStr_map := make(map[string]string)
        for i := 0; i < len(newStr_list); i ++ {
            newStr_map[strings.Trim(strings.SplitN(newStr_list[i], ":", 2)[0], " ")] = strings.Trim(strings.SplitN(newStr_list[i], ":", 2)[1], " ")
        }
        //m := make(map[string]string)
        //err := json.Unmarshal(buf, &m)
        //fmt.Println(m, err)
        //fmt.Println(json.NewDecoder(newStr))
        if res.StatusCode == 200{
            cpe.login_status = true
            cpe.headers_v1 = Headers_v1
            cpe.headers_v2 = Headers_v2
            cpe.headers_v1["AUTHORIZATION"] += newStr_map["session_id"]
            cpe.headers_v2["AUTHORIZATION"] += newStr_map["session_id"]
            return true
        }
        time.Sleep(1 * time.Second)
    }
    return false
}

func sent_http(wg *sync.WaitGroup, cpe *CloudPort){
    if !sent_http_login(cpe){
        fmt.Printf("Unable to login for the device %v. Timeout error\n", cpe.uuid)
    }
    for i:= 0; i < 10; i++ {
        sent_request_every_30s(cpe)
        //sent_request_every_60s(cpe)
        //sent_request_every_120s(cpe)
        //sent_request_every_1500s(cpe)
    }
    wg.Done()
}

func main(){
    start := time.Now()
    spoke_count = 1
    start_serial_no = 9000
    var wg sync.WaitGroup
    for i := 0; i < spoke_count; i++ {
        cpe := new(CloudPort)
        cpe.serial_no = start_serial_no + i
        cpe.uuid = generate_uuid(start_serial_no + i)
        //fmt.Print(cpe.uuid)
        cloud_port_detail[start_serial_no + i] = cpe
        wg.Add(1)
        //go sent_http(&wg, cpe)
        sent_http(&wg, cpe)
        if i % 50 == 0 {
            time.Sleep(1 * time.Second)
        }
    }
    //fmt.Println(runtime.NumGoroutine())
    wg.Wait()
    fmt.Println(time.Since(start), failure)
}
