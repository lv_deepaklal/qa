import requests
import re
from abc import ABC, abstractmethod

import test_libs.cloudstation.rest_service.place as place

STATUS_OK = 200
def runner():
    print('runner')

class CloudstationBase(ABC):

    def __init__(self, url):

        self.url = url
        self.role = None
        self.id = None
        self.access_token = None
        self.customer_id = None
        self.hex_id = None
        self.customer_hex_id = None
        self.logged_in = False
        self.cookies = None
        self.initialize()
        self.headers = {
                       'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                       'Accept-Encoding': 'gzip, deflate, br',
                       'Accept-Language': 'en-US,en;q=0.9',
                       'Accept': '*/*',
                       'Authorization': None,
                       'Connection': 'keep-alive',
                      }

    def initialize(self):

       self.apis = {
                     'login': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/sessions/login',
                                            'version': 'HTTP/1.1',
                                            'method': 'post',
                                           },
                               'headers': {
                                           'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                                           'Accept-Encoding': 'gzip, deflate, br',
                                           'Accept-Language': 'en-US,en;q=0.9',
                                           'Accept': '*/*',
                                          },
                               'params': {
                                           'otp_secret': '',
                                           'username': '',
                                           'password': '',
                                         },
                               'body': None,
                              },
                     'logout': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/sessions/logout',
                                            'version': 'HTTP/1.1',
                                            'method': 'post',
                                           },
                               'headers': {
                                           'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                                           'Accept-Encoding': 'gzip, deflate, br',
                                           'Accept-Language': 'en-US,en;q=0.9',
                                           'Accept': 'application/json, text/javascript',
                                          },
                              },

                     'get_customers': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/customers',
                                            'version': 'HTTP/1.1',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Content-Type': 'application/json; charset=UTF-8',
                                           'Accept': 'application/json, text/plain, */*',
                                           'Host': 'deepak-webui.devcloudstation.io',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
                                          },
                               'body':    None,
                              },
                     'create_site': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/sites',
                                            'version': 'HTTP/1.1',
                                            'method': 'post',
                                           },
                               'headers': {
                                           'Content-Type': 'application/json; charset=UTF-8',
                                           'Accept': 'application/json, text/javascript',
                                           'Host': 'deepak-webui.devcloudstation.io',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
                                          },
                               'body':    {
                                            'name': None,
                                            'location': 'Chennai, Tamil Nadu, India',
                                            'tags': '',
                                            'is_hub': False,
                                            'profile_id': '',
                                            'user_id': None,
                                            'dns_proxy_id': '',
                                            'dns_proxy_enable': False,
                                            'snmp_mapping_id': '',
                                            'snmp_config_enable': False,
                                            'auto_upgrade': False,
                                            'place_id': 'ChIJYTN9T-plUjoRM9RjaAunYW4',
                                            'place': place.chennai,
                                          },
                              },
                     'get_users': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/users',
                                            'version': 'HTTP/1.1',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Host': 'deepak-webui.devcloudstation.io',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
                                          },
                               'body':    None,
                              },
                     'get_user_info': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/users/<hex_id>',
                                            'version': 'HTTP/1.1',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Host': 'deepak-webui.devcloudstation.io',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
                                          },
                               'body':    None,
                              },
                     'get_cloudports': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports',
                                            'version': 'HTTP/1.1',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Host': 'deepak-webui.devcloudstation.io',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
                                          },
                               'body':    None,
                              },

                     'get_cloudport_ifaces': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/interfaces',
                                            'version': 'HTTP/1.1',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Host': 'deepak-webui.devcloudstation.io',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
                                          },
                               'body':    None,
                              },

                     'get_vpn_profiles': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/vpn_profiles',
                                            'version': 'HTTP/1.1',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Host': 'deepak-webui.devcloudstation.io',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
                                          },
                               'body':    None,
                              },

                     'get_lan_subnets': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/network_groups/available_lan_subnets',
                                            'version': 'HTTP/1.1',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Host': 'deepak-webui.devcloudstation.io',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
                                          },
                               'body':    None,
                              },

                     'get_nw_groups': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/network_groups',
                                            'version': 'HTTP/1.1',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Host': 'deepak-webui.devcloudstation.io',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
                                          },
                               'body':    None,
                              },
                   }


    ##########################
    ## python request specific
    ##########################

    def construct_url(self, key, **kwargs):

       url = self.url + self.apis[key]['request']['uri']

       for k, v in kwargs.items():
           url = re.sub('<%s>'%k, v, url)


       params = self.apis[key].get('params', None) 
       body = self.apis[key].get('body', None)
       headers = self.headers 
       headers.update( self.apis[key].get('headers', {}) )
       cookies = self.cookies

       method = self.apis[key]['request']['method']  #Mandatory, so dont use get 
       #getattr(requests, method)()
       return getattr(requests, method), {'url':url, 'params': params, 'data': body, 'headers': headers, 'cookies': cookies} 

    def status(self, response):
        return response.status_code
    
    def data(self, response):
        return response.json()
    
    ##########################
    ## callable codes
    ##########################
    def login(self, login, password):
    
        """
        Working
        """


        key = 'login'
    
        self.apis[key]['params'].update( {'username': login, 'password': password} )
    
        method, kwargs = self.construct_url(key)
        kwargs['headers'].pop('Authorization', None)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        if status==STATUS_OK:

            # Getting JSON data
            self.id = data['id']
            self.role = data['role']
            self.access_token = data['access_token']
            self.customer_id = data['customer_id']
            self.hex_id = data['hex_id']
            self.customer_hex_id = data['customer_hex_id']

            # Updating local vars
            self.headers['Authorization'] = 'Bearer {0}'.format( self.access_token )
            self.logged_in = True
            self.cookies = response.cookies
            
            print('logged in successfully')
        #print(status, data)

    def get_customers(self):

        key = 'get_customers'
        method, kwargs = self.construct_url(key)
        print(self.cookies)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def create_site(self, site_name, user_id, is_hub=False): 

        key = 'create_site'
        self.apis[key]['body'].update( {'name': site_name, 'is_hub': is_hub, 'user_id': user_id} )

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_cloudports(self):
        """
        """
        key = 'get_cloudports'

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_vpn_profiles(self):
        """
        Working
        """
        key = 'get_vpn_profiles'

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_lan_subnets(self):
        """
        """
        key = 'get_lan_subnets'

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_cloudport_ifaces(self, cloudport_id ):
        """
        Working
        """
        key = 'get_cloudport_ifaces'

        method, kwargs = self.construct_url(key, cloudport_id=str(cloudport_id))
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_user_info(self, hex_id):
        """
        working
        """
        key = 'get_user_info'

        method, kwargs = self.construct_url(key, hex_id=hex_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def logout(self):
        key = 'logout'

        method, kwargs = self.construct_url(key)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_users(self):
        """
        Working
        """
        key = 'get_users'

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_nw_groups(self):
        """
        Working
        """
        key = 'get_nw_groups'

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

class Cloudstation(CloudstationBase):

    def printer(self):
        print(self.url, self.user_type)
        print("hi.,,")

