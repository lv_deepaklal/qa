import pexpect
import sys


class connectLinux():

    def __init__(self, ip, username, password, prompt, port=22, timeout=10):
        self.ip = ip
        self.username = username
        self.password = password
        self.port = port
        self.prompt = prompt
        self.session = None
        self.timeout = timeout
        self.b = None
        self.a = None

    def send(self, command, timeout=30, logout=False):
        self.session.send(command+'\n')
        res, reason = self.expect(logout=logout)
        return res, reason

    def expect(self, logout=False):

        outputs = [pexpect.EOF, pexpect.TIMEOUT, 'password:', self.prompt, 'closed', '\[sudo\] password for lavelle']

        for i in range(3):
            res = self.session.expect(outputs)
            self.b, self.a = self.session.before, self.session.after
            print('bef  ==%s====af==%s===== res %s##' % (self.b, self.a, res))
            #print('i===',i)

            if res == 0:
                return False, 'EOF'
            elif res == 1:
                return False, 'Timeout'
            elif res == 2:
                self.session.send(self.password+'\n')
                #return self.expect()
            elif res == 3:
                return True, None
            elif res == 4:
                if logout:
                   return True, None
                else:
                   return False, 'False lgout'
            elif res == 5:
                print('matched 5======', self.password)
                self.session.send(self.password+'\n')
            else:
                return False, 'No Match'

        return True, None
        
    def connect(self):

        command = 'ssh {0}@{1} -p {2} -o StrictHostKeyChecking=no'.format( self.username, self.ip, self.port)
        self.session = pexpect.spawn(command, timeout=self.timeout, encoding='utf-8')
        #self.session.logfile = sys.stdout.buffer
        self.session.logfile = sys.stdout
        res, reason = self.expect()
        print(res, reason)
        

    def set_prompt(self, prompt):
        self.prompt = prompt

    def get_prompt(self):
        return self.prompt


    def flush(self):
        flushedStuff = ''
        while not self.session.expect(r'.+', timeout=5):
            flushedStuff += self.session.match.group(0)

        return flushedStuff

    def logout(self):
        self.send('exit', logout=True)
        #self.expect(logout=True)
        if self.session.isalive():
            return False
        else:
            return True
    

newinfo = {"system": {
       "firewall": {
           "blacklist": [],
           "loopback-whitelist": [],
           "whitelist": [
               "192.168.10.234",
               "192.168.11.14"
           ]
       }
   } 
   }
import json
import pickle
import re
import time

c=connectLinux('172.11.12.149', 'lavelle', 'lavelle@3163', 'lavelle@deepak-cs')
c.connect()
actual_prompt=c.get_prompt()
c.set_prompt('lavelle@lavelle')
c.send('ssh lavelle@192.168.13.31 -o StrictHostKeyChecking=no')
c.a, c.b=None, None
c.send('cat /factory/sysconf.json')
c.send('cat /factory/sysconf.json')
#print(c.flush())
#c.send('cat /factory/sysconf.json')
res =str(c.b)
x = re.search('{.*}', res, re.DOTALL) 
#print('x========', x)
if x:
 print(x.group(0))
 res = x.group(0)
 res = json.loads(res)
 res.update(newinfo)
#res =str(c.b)
#res = str(c.a)
#print('=======\n'+res+'\n===========')
#f=open('sys.json', 'w')
#print(f, f.buffer)
#pickle.dump(str(res) , f)
#f.write(json.dumps(res, indent=4))
#f.close()
c.send('sudo bash')
#c.send('sudo bash')
print('sleeping;...')
#time.sleep(10)
c.send('echo \'{0}\' > /factory/sysconf-new.json'.format( json.dumps(res, indent=4)) )
c.send('exit')
c.set_prompt(actual_prompt)
c.send('exit')
c.logout()
