import requests
import re
from abc import ABC, abstractmethod

import test_libs.cloudstation.rest_service.place as place

STATUS_OK = 200
def runner():
    print('runner')

class CloudstationBase(ABC):

    def __init__(self, url):

        self.url = url
        self.host = None
        url_match = re.search('[a-z]+://(.*)', self.url, re.DOTALL)
        if url_match:
            self.host = url_match.group(1)
        self.role = None
        self.id = None
        self.access_token = None
        self.customer_id = None
        self.hex_id = None
        self.customer_hex_id = None
        self.logged_in = False
        self.cookies = None
        self.initialize()
        self.request = {
                         'version': 'HTTP/1.1',
                       }
        self.headers = {
                       'Content-Type': 'application/json; charset=UTF-8',
                       'Accept-Encoding': 'gzip, deflate, br',
                       'Accept-Language': 'en-US,en;q=0.9',
                       'Accept': 'application/json',
                       'Authorization': None,
                       'Connection': 'keep-alive',
                       'Host': self.host,
                       'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
                       'Referer': None,
                       'Origin': self.url,
                      }

    def initialize(self):

       self.apis = {
                     'login': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/sessions/login',
                                            'method': 'post',
                                           },
                               'headers': {
                                           'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                                           'Accept-Encoding': 'gzip, deflate, br',
                                           'Accept-Language': 'en-US,en;q=0.9',
                                           'Accept': '*/*',
                                          },
                               'params': {
                                           'otp_secret': '',
                                           'username': '',
                                           'password': '',
                                         },
                               'body': None,
                              },
                     'logout': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/sessions/logout',
                                            'method': 'post',
                                           },
                               'headers': {
                                           'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                                           'Accept-Encoding': 'gzip, deflate, br',
                                           'Accept-Language': 'en-US,en;q=0.9',
                                           'Accept': 'application/json, text/javascript',
                                          },
                              },

                     'get_default_applications': {
                               'request' : {
                                            'uri': '/api/v2/lavelle/applications',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json',
                                          },
                               'params': {
                                           'app_type': 'default',
                                           'key': '',
                                           'page': '1',
                                           'per_page': '10',
                                           'sort': '',
                                         },
                              },
                     'get_dhcp_options_for_cp': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/dhcp_options/',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Content-Type': 'application/json; charset=UTF-8',
                                           'Accept': 'application/json, text/plain, */*',
                                          },
                               'body':    None,
                              },

                     'delete_static_route_on_cp': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/routes/<route_id>',
                                            'method': 'delete',
                                           },
                               'headers': {
                                           'Accept': 'application/json',
                                           'Content-Type': 'application/json; charset=UTF-8',
                                          },
                              },
                     'set_static_route_on_cp': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/routes/',
                                            'method': 'post',
                                           },
                               'headers': {
                                           'Accept': 'application/json',
                                           'Content-Type': 'application/json; charset=UTF-8',
                                          },
                               'body':    {
                                            'interface_id': None,
                                            'prefix': None,
                                            'route_type': '',
                                            'nexthop': None,
                                          },
                              },

                     'set_mac_ip_binding_for_cp': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/mac_ip_bindings/',
                                            'method': 'post',
                                           },
                               'headers': {
                                           'Accept': 'application/json',
                                           'Content-Type': 'application/json; charset=UTF-8',
                                          },
                               'body':    {
                                            'host': None,
                                            'ip': None,
                                            'mac': None,
                                          },
                              },
                     'get_mac_ip_bindings_for_cp': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/mac_ip_bindings/',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json',
                                          },
                               'body':    None,
                              },


                     'get_dhcp_ifaces': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/dhcp_interface_ip/<cloudport_id>',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Content-Type': 'application/json; charset=UTF-8',
                                           'Accept': 'application/json, text/plain, */*',
                                          },
                               'body':    None,
                              },

                     'get_dhcp_server': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/dhcp_servers',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Content-Type': 'application/json; charset=UTF-8',
                                           'Accept': 'application/json, text/plain, */*',
                                          },
                               'body':    None,
                              },

                     'delete_dhcp_server': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/dhcp_servers/<dhcp_server_id>/',
                                            'method': 'delete',
                                           },
                               'headers': {
                                           'Accept': 'application/json',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                          },
                              },

                     'set_dhcp_server': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/dhcp_servers/',
                                            'method': 'post',
                                           },
                               'headers': {
                                           'Content-Type': 'application/json; charset=UTF-8',
                                           'Accept': 'application/json, text/javascript',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                          },
                               'body':    {
                                           'interface_address_id': None,
                                           'start_ip'            : None,
                                           'stop_ip'             : None,
                                           'dns'                 : None
                                          }
                              },
                     'get_customers': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/customers',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Content-Type': 'application/json; charset=UTF-8',
                                           'Accept': 'application/json, text/plain, */*',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                          },
                               'body':    None,
                              },
                     'create_site': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/sites',
                                            'method': 'post',
                                           },
                               'headers': {
                                           'Content-Type': 'application/json; charset=UTF-8',
                                           'Accept': 'application/json, text/javascript',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                          },
                               'body':    {
                                            'name': None,
                                            'location': 'Chennai, Tamil Nadu, India',
                                            'tags': '',
                                            'is_hub': False,
                                            'profile_id': '',
                                            'user_id': None,
                                            'dns_proxy_id': '',
                                            'dns_proxy_enable': False,
                                            'snmp_mapping_id': '',
                                            'snmp_config_enable': False,
                                            'auto_upgrade': False,
                                            'place_id': 'ChIJYTN9T-plUjoRM9RjaAunYW4',
                                            'place': place.chennai,
                                          },
                              },
                     'get_users': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/users',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                          },
                               'body':    None,
                              },
                     'get_user_info': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/users/<hex_id>',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                          },
                               'body':    None,
                              },
                     'get_cloudports': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                          },
                               'body':    None,
                              },

                     'get_cloudport_ifaces': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/interfaces',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                          },
                               'body':    None,
                              },

                     'get_vpn_profiles': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/vpn_profiles',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                          },
                               'body':    None,
                              },

                     'get_lan_subnets': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/network_groups/available_lan_subnets',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                          },
                               'body':    None,
                              },

                     'get_routes_on_cp': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/routes/',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json',
                                          },
                               'body':    None,
                              },
                     'delete_dynamic_route_on_cp': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/dynamic_routings/<route_id>',
                                            'method': 'delete',
                                           },
                               'headers': {
                                           'Accept': 'application/json',
                                          },
                              },
                     'set_dynamic_route_on_cp': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/cloudports/<cloudport_id>/dynamic_routings/',
                                            'method': 'post',
                                           },
                               'headers': {
                                           'Accept': 'application/json',
                                           'Content-Type': 'application/json; charset=UTF-8',
                                          },
                               'body':    {
                                            'interface_id': None,
                                            'protocol': None,
                                            'mode': 'direct',
                                            'neigh_ip': '',
                                            'neigh_as': '',
                                            'local_as': '',
                                            'area'    : '',
                                            'cost'    : '10',
                                            'hello'   : '10',
                                            'poll'    : '20',
                                            'retransmit': '5',
                                            'priority': '1',
                                            'stub':  '',
                                            'rfc1583compat' : '',
                                            'bfd': '',
                                            'wait': '40',
                                            'dead': '50',
                                            'dead_count': '5',
                                            'authentication': 'none',
                                            'password': '',
                                            'auth_id': '',
                                            'algorithm': '',
                                            }
                              },
                     'get_nw_groups': {
                               'request' : {
                                            'uri': '/api/v1/lavelle/network_groups',
                                            'method': 'get',
                                           },
                               'headers': {
                                           'Accept': 'application/json, text/javascript',
                                           'Referer': 'https://deepak-webui.devcloudstation.io/?cid=456e6620de5d14188f9ddd4cf526e708',
                                          },
                               'body':    None,
                              },
                   }


    ##########################
    ## python request specific
    ##########################

    def construct_url(self, key, **kwargs):

       url = self.url + self.apis[key]['request']['uri']

       for k, v in kwargs.items():
           url = re.sub('<%s>'%k, v, url)


       params = self.apis[key].get('params', None) 
       body = self.apis[key].get('body', None)
       headers = self.headers 
       headers.update( self.apis[key].get('headers', {}) )
       cookies = self.cookies

       method = self.apis[key]['request']['method']  #Mandatory, so dont use get 
       #getattr(requests, method)()
       return getattr(requests, method), {'url':url, 'params': params, 'data': body, 'headers': headers, 'cookies': cookies} 

    def status(self, response):
        return response.status_code
    
    def data(self, response):
        return response.json()
    
    ##########################
    ## callable codes
    ##########################
    def login(self, login, password):
    
        """
        Working
        """


        key = 'login'
    
        self.apis[key]['params'].update( {'username': login, 'password': password} )
    
        method, kwargs = self.construct_url(key)
        kwargs['headers'].pop('Authorization', None)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        if status==STATUS_OK:

            # Getting JSON data
            self.id = data['id']
            self.role = data['role']
            self.access_token = data['access_token']
            self.customer_id = data['customer_id']
            self.hex_id = data['hex_id']
            self.customer_hex_id = data['customer_hex_id']

            # Updating local vars
            self.headers['Authorization'] = 'Bearer {0}'.format( self.access_token )
            self.headers['Referer'] = '{0}/?cid={1}'.format( self.url, self.customer_hex_id)
            self.logged_in = True
            self.cookies = response.cookies
            
            print('logged in successfully')
        print(status, data)

    def get_customers(self):

        key = 'get_customers'
        method, kwargs = self.construct_url(key)
        print(self.cookies)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def create_site(self, site_name, user_id, is_hub=False): 

        key = 'create_site'
        self.apis[key]['body'].update( {'name': site_name, 'is_hub': is_hub, 'user_id': user_id} )

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_dhcp_ifaces_for_cp(self, cloudport_id):
        # working
        key = 'get_dhcp_ifaces'
        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def delete_dhcp_server_on_cp(self, cloudport_id, dhcp_server_id):

        # working
        key = 'delete_dhcp_server'
        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id, dhcp_server_id=dhcp_server_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def set_mac_ip_binding_for_cp(self, cloudport_id, ip, mac, host=None): 

        #not working
        key = 'set_mac_ip_binding_for_cp'
        self.apis[key]['body'].update( {'ip': ip, 'mac': mac, 'host': host if host else 'host-%s'%ip } )
        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_mac_ip_bindings_for_cp(self, cloudport_id): 

        #working
        key = 'get_mac_ip_bindings_for_cp'

        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_dhcp_options_for_cp(self, cloudport_id): 

        # working

        key = 'get_dhcp_options_for_cp'

        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_dhcp_server_for_cp(self, cloudport_id ): 

        # working
        key = 'get_dhcp_server'

        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def set_dhcp_server_for_cp(self, cloudport_id, iface_id, start_ip, stop_ip, dns):

        #not working

        key = 'set_dhcp_server'
        self.apis[key]['body'].update( {"interface_address_id":int(iface_id),
                                        "start_ip":start_ip,
                                        "stop_ip":stop_ip,
                                        "dns":dns} )

        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_cloudports(self):
        """
        """
        key = 'get_cloudports'

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_vpn_profiles(self):
        """
        Working
        """
        key = 'get_vpn_profiles'

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_lan_subnets(self):
        """
        """
        key = 'get_lan_subnets'

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_cloudport_ifaces(self, cloudport_id ):
        """
        Working
        """
        key = 'get_cloudport_ifaces'

        method, kwargs = self.construct_url(key, cloudport_id=str(cloudport_id))
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_user_info(self, hex_id):
        """
        working
        """
        key = 'get_user_info'

        method, kwargs = self.construct_url(key, hex_id=hex_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def logout(self):
        key = 'logout'

        method, kwargs = self.construct_url(key)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_users(self):
        """
        Working
        """
        key = 'get_users'

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_nw_groups(self):
        """
        Working
        """
        key = 'get_nw_groups'

        method, kwargs = self.construct_url(key)
        print(kwargs['headers'])
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def delete_dynamic_route_on_cp(self, cloudport_id, route_id):

        # working
        key = 'delete_dynamic_route_on_cp'
        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id, route_id=route_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def set_ospf_on_cp(self, cloudport_id, iface_id, area, cost='10', hello='10', poll='20', authentication='none', password = '', auth_id = '', retx='5', priority='1', rfc1583compat=False, stub=False, wait='40', dead='50', dead_count='5', bfd=False):

        key = 'dynamic_route_on_cp'

        mode = mode.strip().lower() 
        authentication = authentication.strip().lower()

        self.apis[key]['body'].update( {
                                            'interface_id': int(iface_id),
                                            'protocol': 'ospf',
                                            'mode': 'direct',
                                            'area'    : area,
                                            'cost'    : cost,
                                            'hello'   : hello,
                                            'poll'    : poll,
                                            'retransmit': retx,
                                            'priority': priority,
                                            'stub':  'yes' if stub else '',
                                            'rfc1583compat' : 'yes' if rfc1583compat else '',
                                            'bfd': 'yes' if bfd else '',
                                            'wait': wait,
                                            'dead': dead,
                                            'dead_count': dead_count,
                                            'authentication': authentication if authentication in ['none', 'simple', 'cryptographic'] else 'none',
                                            'password': password,
                                            'auth_id': auth_id,
                                            'algorithm': '',

                                           })

        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass
    def set_bgp_on_cp(self, cloudport_id, iface_id, neigh_ip, neigh_as, local_as, mode='direct'):

        key = 'dynamic_route_on_cp'

        mode = mode.strip().lower() 

        self.apis[key]['body'].update( {
                                            'interface_id': int(iface_id),
                                            'protocol': 'bgp',
                                            'mode': mode if mode in ['direct', 'multihop'] else 'direct',
                                            'neigh_ip': neigh_ip, 
                                            'neigh_as': neigh_as,
                                            'local_as': local_as,
                                           })

        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def delete_static_route_on_cp(self, cloudport_id, route_id):
        # working
        key = 'delete_static_route_on_cp'

        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id, route_id=route_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def set_static_route_on_cp(self, cloudport_id, iface_id, prefix, next_hop):
        # not working

        key = 'set_static_route_on_cp'
        self.apis[key]['body'].update( {
                                            'interface_id': int(iface_id),
                                            'prefix': prefix ,
                                            'nexthop': next_hop,
                                           })

        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id)


        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass
    def get_routes_on_cp(self, cloudport_id):
        # working

        key = 'get_routes_on_cp'

        method, kwargs = self.construct_url(key, cloudport_id=cloudport_id)
        response  = method(**kwargs)
        status = self.status(response)
        data = self.data(response)

        print(status)
        print(data)
        if status==STATUS_OK:
            #print(data)
            pass

    def get_default_applications(self):

        #
        key = 'get_default_applications'

        apps = []
        fetched_apps_count = 0
        total_apps_count = 0
        do_iteration = 1
        page = 1
        per_page = 90
        print(fetched_apps_count, total_apps_count)

        while do_iteration: 

            self.apis[key]['params'].update( {
                                              'page': str(page),
                                              'per_page': str(per_page),
                                             })

            method, kwargs = self.construct_url(key)

            response  = method(**kwargs)
            status = self.status(response)
            data = self.data(response)

            print(status)
            #print(data)

            if status==STATUS_OK:
                total_apps_count = int(data['total_count'])
                fetched_apps = data['data']['applications']
                print(len(fetched_apps), total_apps_count)
                #apps.update(set(fetched_apps))
                apps.extend(fetched_apps)
                fetched_apps_count = fetched_apps_count + len( data['data']['applications'] )
                page=page+1

            if fetched_apps_count >= total_apps_count: break
            if status != STATUS_OK: break
        print(fetched_apps_count, total_apps_count, len(apps))

    def create_wan_policy(self):
        x = {"name":"Deepak",
         "from_object":"",
         "from_type":"",
         "from_name":"",
         "both_type":"",
         "both_name":"",
         "both_object":"",
         "to_type":"",
         "to_name":"",
         "to_object":"",
         "acl_action":"",
         "owner_id":"",
         "owner_type":"",
         "disabled":false,
         "src_network_group_id":1,
         "dst_network_group_id":4095,
         "secured_data":false,
         "priority":3,
         "direction":"lan_to_wan",
         "output_direction":"wan",
         "tag":"",
         "wan_actions":[
                {"encapsulation":"none",
                 "next_hop":"hub",
                 "secured_data":false,
                 "vpn_profile_id":"",
                 "wan_type":"any",
                 "next_hop_object_value":"",
                 "gre_key":""
                 }],
         "both":{
                 "lan_subnets":[],
                 "applications":[
                                  {"object":{
                                      "id":3,
                                      "name":"icmp",
                                      "protocol":"1",
                                      "signature":null,
                                      "created_at":"2019-05-21T07:52:39.000Z",
                                      "updated_at":"2019-05-21T07:52:39.000Z",
                                      "tag":null,
                                      "dscp":null,
                                      "user_id":5,
                                      "customer_id":2,
                                      "hex_id":"0ceba2d2163bffc391af268da2039892",
                                      "ip":null,
                                      "hostname":null,
                                      "default":true,
                                      "application_category_id":null,
                                      "tcp_port":null,
                                      "udp_port":null,
                                      "app_id":2,
                                      "src_ip":null,
                                      "dst_ip":null,
                                      "src_port":null,
                                      "dst_port":null,
                                      "port":null,
                                      "ticked":true
                                     },
                                     "addition_type":"OR"
                                    },
                                  {"object":{
                                      "id":4,
                                      "name":"igmp",
                                      "protocol":"2",
                                      "signature":null,
                                      "created_at":"2019-05-21T07:52:39.000Z",
                                      "updated_at":"2019-05-21T07:52:39.000Z",
                                      "tag":null,
                                      "dscp":null,
                                      "user_id":5,
                                      "customer_id":2,
                                      "hex_id":"749f6ec85490da0b33b7943dc8bec16e",
                                      "ip":null,
                                      "hostname":null,
                                      "default":true,
                                      "application_category_id":null,
                                      "tcp_port":null,
                                      "udp_port":null,
                                      "app_id":3,
                                      "src_ip":null,
                                      "dst_ip":null,
                                      "src_port":null,
                                      "dst_port":null,
                                      "port":null,
                                      "ticked":true
                                      },
                                      "addition_type":"OR"
                                    }
                                 ],
                 "application_categories":[{"object":{
                                              "id":4,
                                              "name":"Web-ddos",
                                              "created_at":"2019-05-21T07:52:38.000Z",
                                              "updated_at":"2019-05-21T07:52:38.000Z",
                                              "user_id":5,
                                              "customer_id":2,
                                              "category":null,
                                              "app_category_id":129,
                                              "ticked":true
                                              },
                                              "addition_type":"OR"
                                             }
                                            ],
                 "customs":[]
                },
         "from":{
                  "lan_subnets":[{"object":{"id":6,"subnet":"10.0.2.1","subnet_mask":"255.255.255.0","vlan":0,"site_id":2,"subnet_name":"10295__10.0.2.1__255.255.255.0__0","site_name":"10295","ticked":true},"addition_type":"OR"},{"object":{"id":7,"subnet":"10.0.3.1","subnet_mask":"255.255.255.0","vlan":0,"site_id":3,"subnet_name":"5078__10.0.3.1__255.255.255.0__0","site_name":"5078","ticked":true},"addition_type":"OR"}],
                  "applications":[],
                  "application_categories":[],"customs":[{"object":{"value":"45.4.4.5/32","type":"CustomIp"},"addition_type":"OR"}]
                 },
         "to":{
                "lan_subnets":[{"object":{"id":9,"subnet":"10.0.10.1","subnet_mask":"255.255.255.0","vlan":0,"site_id":5,"subnet_name":"6001__10.0.10.1__255.255.255.0__0","site_name":"6001","spc_kkQvy":0,"idx_kkQvy":3,"spc_NGeuL":0,"idx_NGeuL":3,"ticked":true},"addition_type":"OR"},{"object":{"id":10,"subnet":"10.0.12.0","subnet_mask":"255.255.255.0","vlan":0,"site_id":2,"subnet_name":"10295__10.0.12.0__255.255.255.0__0","site_name":"10295","spc_kkQvy":0,"idx_kkQvy":4,"spc_NGeuL":0,"idx_NGeuL":4,"ticked":true},"addition_type":"OR"}],
                "applications":[],
                "application_categories":[],
                "customs":[{
                    "object":{ 
                              "value":"8.9.2.3/32",
                              "type":"CustomIp"
                             },
                    "addition_type":"OR"}]
               }
        }


class Cloudstation(CloudstationBase):

    def printer(self):
        print(self.url, self.user_type)
        print("hi.,,")

