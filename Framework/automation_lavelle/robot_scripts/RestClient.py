'''
Created on Nov 17, 2018

@author: Prasanta
'''
import requests

requests.packages.urllib3.disable_warnings()
# from requests import urllib3
# urllib3.disable_warnings()
import json


class RestClient(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    def __init__(self, server, user, passwd):
        '''
        Constructor
        '''
        self.server = server
        self.user = user
        self.passwd = passwd
        self.session = requests.Session()

    def create_access_token(self):
        ''' 
        docs
        '''
        URI = 'https://' + self.server + '/api/v1/lavelle/sessions/login'
        header = {'Content-type': 'application/x-www-form-urlencoded'}
        payload = {'otp_secret': '', 'g_captcha': '', 'username': self.user, 'password': self.passwd}

        r = self.session.post(url=URI, headers=header, data=payload, verify=False)
        try:
            if r.status_code == 200:
                self.access_token = json.loads(r.text)['access_token']
                print "AccessToken: " + self.access_token
            else:
                self.access_token = None
        except:
            print "Incorrect status code: " + r.status_code

    def get_site_count(self):
        URI = 'https://' + self.server + '/api/v1/lavelle/nodes/get_count'

        if self.access_token:
            header = {'Accept': 'application/json', 'Authorization': 'Bearer ' + self.access_token}
            print "Request Header--",header
            r = self.session.get(url=URI, headers=header, verify=False)
            if r.status_code == 200:
                print r.status_code
                print r.text
            return r.text
        else:
            print "Access token is not defined"
            return False


    def send_get(self, url, header=None, params=None, **kwargs):
        BASE_URI='https://' + self.server
        URI = BASE_URI+''+url
        if self.access_token:
            if header is None:
                header = {'Accept': 'application/json', 'Authorization': 'Bearer ' + self.access_token}
            print "Request Header--", header
            r = self.session.get(url=URI, headers=header, params=params, verify=False)
            if r.status_code == 200:
                print r.status_code
                print r.url
                print r.text

            return r.text
        else:
            print "Access token is not defined"
            return False

    def send_post(self, url, header=None, params=None, data=None, **kwargs):
        BASE_URI='https://' + self.server
        URI = BASE_URI+''+url
        if self.access_token:
            if header is None:
                header = {'Content-type': 'application/json; charset=UTF-8', 'Authorization': 'Bearer ' + self.access_token}
            print "Request Header--", header
            print "URI--", URI
            print "POST BODY --",data

            r = self.session.post(url=URI, headers=header, data=data, params=params, verify=False)
            if r.status_code == 200:
                print r.status_code
                print r.url
                print r.text

            return r.text
        else:
            print "Access token is not defined"
            return False

    def send_delete(self, url, header=None, params=None, data=None, **kwargs):
        BASE_URI='https://' + self.server
        URI = BASE_URI+''+url
        if self.access_token:
            if header is None:
                header = {'Content-type': 'application/json; charset=UTF-8', 'Authorization': 'Bearer ' + self.access_token}
            print "Request Header--", header
            print "URI--", URI
            print "POST BODY --",data

            r = self.session.delete(url=URI, headers=header, data=data, params=params, verify=False)
            if r.status_code == 200:
                print r.status_code
                print r.url
                print r.text

            return r.text
        else:
            print "Access token is not defined"
            return False

    def logout(self):
        URI = 'https://' + self.server + '/api/v1/lavelle/sessions/logout'
        if self.access_token:
            header = {'Accept': 'application/json', 'Authorization': 'Bearer ' + self.access_token}
            print "Request Header--",header
            print "URI--", URI
            r = self.session.post(url=URI, headers=header, verify=False)
            if r.status_code == 200:
                print r.status_code
                print r.text
            return r.status_code
        else:
            print "Access token is not defined"
            return False

if __name__ == "__main__":
    obj = RestClient('training-webui.xpedition.io', 'training-net-admin@lavellenetworks.com', 'training@1456#')
    obj.create_access_token()
  #  obj.get_site_count()
    #obj.send_get('/api/v1/lavelle/nodes/get_count')
    obj.send_get('/api/v1/lavelle/sites',params={'role': 'net-admin'})
