package lvnreports

import (
	"fmt"
	"strings"
	"io/ioutil"
        "bytes"
	"net/smtp"
	"github.com/onsi/ginkgo/config"
	"github.com/onsi/ginkgo/types"
	"lvnautomation/lvnmail"
	//tc_utils "lvnautomation/testcaseutils"
)

type cwReporter struct {
	// keep track of nested describes using map
	specs map[int]string
}

func New() *cwReporter {
	return &cwReporter{
		specs: make(map[int]string),
	}
}

var report_info bytes.Buffer = bytes.Buffer{}
var testcase_slno int=1
const SPEC_NAME_INDEX = 3

func send_report(smtp_server string, from_login string, password string, to_address []string, message string) {


    // Choose auth method and set it up
    //auth := smtp.PlainAuth("", "piotr@mailtrap.io", "extremely_secret_pass", "smtp.mailtrap.io")
    auth := smtp.PlainAuth("", from_login, password, smtp_server)
    // Here we do it all: connect to our server, set up a message and send it
    //to := []string{"billy@microsoft.com"}
    /*msg := []byte("To: billy@microsoft.com\r\n" +
    "Subject: Why are you not using Mailtrap yet?\r\n" +
    "\r\n" +
    "Here’s the space for our great sales pitch\r\n") */

    msg := "From: " + from_login + "\n" + "To: " + strings.Join(to_address, ",") + "\n" + "Subject: " + "Report\n\n" + message
    //err := smtp.SendMail("smtp.mailtrap.io:25", auth, "piotr@mailtrap.io", to, msg)

    err := smtp.SendMail(smtp_server+":587", auth, from_login, to_address, []byte(msg))
    if err != nil {
    fmt.Println("error sending mail")
    }

}
var html_info string
var console_report [][]string

func (r *cwReporter) SpecSuiteWillBegin(config config.GinkgoConfigType, summary *types.SuiteSummary) {
   //err := ioutil.WriteFile("testwrite.txt", []b
   //if err != nil{
   //    //log.Fatal(err)
   //    fmt.Prinltn(err)
   //}
   console_report = [][]string{[]string{"Sl.No", "Testcase", "Result", "Reason"}}
   csv_heading:="Sl.No,Testcase name,Result,Reason\n"
   report_info.WriteString(csv_heading)
   html_info = `<html>
    <body>
      <table border="1" style="width:100%">
      <col width="5%">
      <col width="20%">
      <col width="10%">
      <col width="60%">
      <tr>
        <th>Sl.No</th>
        <th>Testcase Name</th>
        <th>Result</th>
        <th>Reason</th>
      </tr>`
}

func (r *cwReporter) BeforeSuiteDidRun(setupSummary *types.SetupSummary) {
	fmt.Printf("%+v\n",setupSummary)
}

func (r *cwReporter) AfterSuiteDidRun(setupSummary *types.SetupSummary) {
    err := ioutil.WriteFile("test.txt", []byte(report_info.String()), 0777)
    if err != nil {
        fmt.Println()
    }
    length :=  5
    // above 5 is five | pipes
    length = length + 5 + 50 + 10 + 50
    if len(console_report) > 1 {
        fmt.Println(strings.Repeat("-", length))

        for index, test_info := range console_report {
            fmt.Printf("|%-5s|%-50s|%-10s|%-50s|\n", test_info[0], test_info[1], test_info[2], test_info[3])
	    if 0==index {
                fmt.Println(strings.Repeat("-", length))
	    }
        }
        fmt.Println(strings.Repeat("-", length))
    }

    //mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n";
    html_info = html_info + "</table></body></html>"
    password:=""
    m := lvnmail.NewEmail()
    m.Subject = ""
    m.MimeVersion = "1.0"
    m.ContentType = "text/html; charset=\"UTF-8\""
    m.Message = html_info
    fmt.Println(m.Subject, password)
    //lvnmail.SendMail(m, password)
}

func (r *cwReporter) SpecWillRun(spec *types.SpecSummary) {
	/*n := len(spec.ComponentTexts)
	for i := 1; i+1 < n; i++ {
		v := spec.ComponentTexts[i]
		// append LineNumer to support multiple test cases with same name
		id := fmt.Sprintf("%s:%d", v, spec.ComponentCodeLocations[i].LineNumber)
		s, ok := r.specs[i]
		if !ok || s != id {
			if ok {
				m := len(r.specs)
				for j := i; j <= m; j++ {
					fmt.Println("\n<COMPLETEDIN::>")
					delete(r.specs, j)
				}
			}
			r.specs[i] = id
			fmt.Printf("\n<DESCRIBE::>%s\n", escape(v))
		}
	}
	fmt.Printf("\n<IT::>%s\n", escape(spec.ComponentTexts[n-1])) */
}

var test_result_bg_color map[string]string = map[string]string{
	"PASS"  : "green",
	"FAIL"  : "red",
	"ABORT" : "blue",
	"SKIP"  : "yellow",
}
func (r *cwReporter) SpecDidComplete(spec *types.SpecSummary) {
	var test_result, test_reason, testcase_name string
	//fmt.Printf("%+v\n",spec)
	//fmt.Printf("=====>%s\n",spec.ComponentTexts[3])
	testcase_name = spec.ComponentTexts[SPEC_NAME_INDEX]
	switch spec.State {
	case types.SpecStatePassed:
		test_result = "PASS"
		test_reason = ""
		//test_reason = escape(spec.Failure.Message)
	case types.SpecStateFailed:
		test_result = "FAIL"
		test_reason = escape(spec.Failure.Message)
	case types.SpecStatePanicked:
		//fmt.Printf("\n<ERROR::>%s\n", escape(spec.Failure.Message))
		//fmt.Printf("\n<LOG::Panic>%s\n", escape(spec.Failure.ForwardedPanic))
		//fmt.Printf("\n<TAB::Stack Trace>%s\n", escape(spec.Failure.Location.FullStackTrace))
		test_result = "ABORT"
		test_reason = escape(spec.Failure.Message)
	case types.SpecStateTimedOut:
		test_result = "ABORT"
		test_reason = escape(spec.Failure.Message)
	case types.SpecStateSkipped:
		test_result = "SKIP"
		test_reason = escape(spec.Failure.Message)
	case types.SpecStatePending:
		test_result = "SKIP"
		test_reason = escape(spec.Failure.Message)
	}
	//fmt.Printf("\n<COMPLETEDIN::>%.4f\n", spec.RunTime.Seconds()*1000)
        spec_info:=fmt.Sprintf("%d,%s,%s,%s\n", testcase_slno, testcase_name, test_result, test_reason)
        report_info.WriteString(spec_info)
        //console_report = [][]string{[]string{"Sl.No", "Testcase", "Result", "Reason"}}
        console_report = append(console_report, []string{fmt.Sprintf("%d", testcase_slno), testcase_name, test_result, test_reason})

	html_info =  fmt.Sprintf("%s<tr><td>%d</td> <td>%s</td> <td bgcolor=\"%s\" align=\"center\">%s</td><td>%s</td></tr>", html_info, testcase_slno, testcase_name, test_result_bg_color[strings.ToUpper(test_result)], test_result, test_reason)
	testcase_slno++
}

func (r *cwReporter) SpecSuiteDidEnd(summary *types.SuiteSummary) {
}

func escape(s string) string {
	//return strings.Replace(s, "\n", "<:LF:>", -1)
	return s
}


