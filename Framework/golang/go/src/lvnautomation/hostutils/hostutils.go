package hostutils

import (
	"fmt"
	"github.com/sfreiberg/simplessh"
	"strings"
	"regexp"
	"strconv"
	"errors"
	utils "lvnautomation/utils"
	log "lvnautomation/lvnlog"
	"sync"

)

type BirdRoute struct {
    Network string
    Mask string
    Gateway string
}

type Netns struct {
	NamespaceName string
	Ifaces []string
}

type Credentials struct{
	Host string
	Port string
	User string
	Pswd string
	Persistent bool
	client *simplessh.Client
	connected bool
	stop_loop bool
	mutex sync.Mutex
	reconnect int
	Namespace *Netns
}


func NewHost(Host string, Port string, User string, Pswd string) *Credentials {
	var c Credentials
	c.Host = Host
	c.Port = Port
	c.User = User
	c.Pswd = Pswd
	c.Persistent = false
	c.connected = false
	c.stop_loop = false
	c.reconnect = 0
	return &c
}

func NewNetns(ifaces []string) *Netns {
    return &Netns{NamespaceName : "lvn_auto"}
}


func (sshh *Credentials) SetNetns(ns *Netns) {
	sshh.Namespace = ns
}

func (sshh *Credentials) UnsetNetns(ns *Netns) {
	sshh.Namespace = nil
}

func (sshh *Credentials) Close() {
	if sshh.connected {
		sshh.client.Close()
		sshh.connected = false
	}
}

func (sshh *Credentials) Connect() (string , error) {

	//log.Debug.Println(utils.GetFuncName())

	Hostport := sshh.Host+":"+sshh.Port
	var err error
	var client *simplessh.Client
	if !sshh.connected {

		//mt.Println("executing login...")
	        log.Debug.Printf("Executing login for %s with username %s\n", Hostport, sshh.User)
		client, err = simplessh.ConnectWithPassword(Hostport, sshh.User, sshh.Pswd)
		sshh.client = client
		if err != nil {
			log.Debug.Printf("Received from execute err %s\n", err)
			return "" ,err
		}
		sshh.connected = true
	} else {
	        //log.Debug.Printf("Skipping the login for %s with username %s since already connected\n", Hostport, sshh.User)
		//client = sshh.client
		//fmt.Println("skipped login...")
	}
	return "", nil
}

func (sshh *Credentials) ExecCommandFromHost(cmd string, flag int) (string , error) {

	//log.Debug.Println(utils.GetFuncName())

	var msg string
	var err error
	Hostport := sshh.Host+":"+sshh.Port

	if msg, err = sshh.Connect(); err!= nil {
		return msg, err
	}
	log.Debug.Printf("-------------------------------\n")
        log.Debug.Printf("Executing command: %s on %s\n", cmd, Hostport)
	client := sshh.client

	/*
	Hostport := sshh.Host+":"+sshh.Port
	var err error
	var client *simplessh.Client
	if !sshh.connected {

		fmt.Println("executing login...")
		log.Debug.Printf("Executing command: %s on %s\n", cmd, Hostport)
		client, err = simplessh.ConnectWithPassword(Hostport, sshh.User, sshh.Pswd)
		sshh.client = client
		if err != nil {
			log.Debug.Printf("Received from execute err %s\n", err)
			return "" ,err
		}
		sshh.connected = true
	} else {
		client = sshh.client
		fmt.Println("skipped login...")
	}*/

	if !sshh.Persistent {
		//fmt.Println("deferrrig close()")
                //log.Debug.Printf("Deferring close() for host %s\n", Hostport)
		defer func(sshh *Credentials) { sshh.connected = false }(sshh)
	        defer client.Close()
	} else {
		//fmt.Println("skipped deferrring close()...")
                //log.Debug.Printf("Skipped deferring close() for host %s\n", Hostport)
	}

	var Output1 string
	var output []byte

	if flag == 1 {
		output, err = client.ExecSudo(cmd, sshh.Pswd)
	} else {
		output, err = client.Exec(cmd)
	}

	if err != nil {
            if err.Error() == "ssh: rejected: administratively prohibited (open failed)" {
            //log.Debug.Printf("session closed reconnecting...")
	    sshh.mutex.Lock()
	    if sshh.reconnect == 0 {
	        sshh.connected = false
	        sshh.Connect()
		sshh.reconnect = sshh.reconnect + 1
	    }
	    sshh.mutex.Unlock()
            //log.Debug.Printf("Received from execute err %s\n", err)
            return "", err
	   }
        }

	Output1 = string(output)

	/*
	if flag == 1 {
		if err != nil {
			if output != nil {
				log.Debug.Printf("Received from execute err %s\n", err)
				return string(output) ,err
			}
			log.Debug.Printf("Received from execute err %s\n", err)
			return "",err
		}
	Output1 = string(output)
	} else {
		if err != nil {
			if output != nil {
				log.Debug.Printf("Received from execute err %s\n", err)
				return string(output) ,err
			}
			log.Debug.Printf("Received from execute err %s\n", err)
			return "",err
		}
		Output1 = string(output)
	}
	//val := strings.Fields(cmd)
         */
	//log.Debug.Printf("Output of the comman: %s %s\n", Output1)
	log.Debug.Printf("Output of the command: %s\n", cmd)
	log.Debug.Printf("%s\n", Output1)
	log.Debug.Printf("-------------------------------\n")

	//fmt.Printf("Uptime: %s\n", output)
	return Output1, nil
}

func (sshh *Credentials) ExecIfconfigFromHost(iface string) (string, string, error) {

	//log.Debug.Println(utils.GetFuncName())

	sudo := 0       // To Execute without Sudo

    log.Debug.Println(" Parameters: iface %s",iface)
 	cmd := "ifconfig "+iface
 	op, err := sshh.ExecCommandFromHost(cmd, sudo)
    log.Debug.Println(" Parameters: iface %s",op)

 	if err != nil {
		return op, "", err
		log.Debug.Printf("Received from execute err %s", err)
	}
	log.Debug.Printf("Received from execute output %s", op)

	re := regexp.MustCompile(`inet([0-9])? (addr:)?([0-9]+(\.[0-9]+)+)`)
	iptemp := re.FindAllSubmatch([]byte(op), -1)
	//ip := strings.Split(string(iptemp), " ")
	ip := string(iptemp[0][3])

	re = regexp.MustCompile(`ether [a-fA-F0-9:]{17}|[a-fA-F0-9]{12}`)
	mactemp := re.Find([]byte(op))

	//ip := strings.Split(string(iptemp), " ")
	mac := strings.Split(string(mactemp), " ")
	log.Debug.Println(ip, mac)

	//log.Debug.Printf("Returning output %s, ip %s, mac %s", op, ip[1],mac[1])
    log.Debug.Printf("Exiting ExecIfconfigFromHost functon...")

	//return ip[1], mac[1], nil 
	return ip, "", nil

}

func (sshh *Credentials) ExecPingFromHost(ip string, count int) (map[string]interface{}, error) {

    //log.Debug.Println(utils.GetFuncName())

        ping_op := make(map[string]interface{})
	cmd :="ping "+ip+" -c "+strconv.Itoa(count)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		//fmt.Println(op)
		return ping_op, err
	}

	status, tx, rx, loss, time, rtt_min, rtt_avg, rtt_max, rtt_mdev := utils.ParsePingOutput(op)
	if !status {
		return ping_op, errors.New("Failed to parse Ping output")
	}
        //log.Debug.Printf("Received from Ping output %s\n", op)
	//re := regexp.MustCompile(`(\d+) packets transmitted, (\d+) received, (\+\d+ duplicates, )?(\d+)% packet loss, time (\d+)ms(\nrtt min\/avg\/max\/mdev = ([\d\.]+)\/([\d\.]+)\/([\d\.]+)\/([\d\.]+))?`)
	//outt := re.FindAllStringSubmatch(op, -1)
	//fmt.Println(outt)
	//out := string(outt)
	ping_op["tx"] = tx
	ping_op["rx"] = rx
	ping_op["loss"] = loss
	ping_op["time"] = time
	ping_op["rtt_min"] = rtt_min
	ping_op["rtt_avg"] = rtt_avg
	ping_op["rtt_max"] = rtt_max
	ping_op["rtt_mdev"] = rtt_mdev

	return ping_op, nil
}

func (sshh *Credentials) IsReachableFromNamespace(namespace string, ip string) (bool, error) {

        //log.Debug.Println(utils.GetFuncName())
        count := 5
        res, err, output := sshh.PingFromNamespace(namespace, ip, count)
	//fmt.Println("IsReachableFromNamespace res", res)
        if !res {
                return res, err
        }

        res2, _, _, loss_percent, _, _, _, _, _  := utils.ParsePingOutput(output)
	//fmt.Println("IsReachableFromNamespace res2", res2)
        if !res2  {
                return res2, nil
        }

	//fmt.Println("IsReachableFromNamespace loss_percent", loss_percent)
        if loss_percent > 1 {
                return false, errors.New("Ping loss percent > 1%")
        }
        return true, nil
}

func (sshh *Credentials) PingFromNamespace(namespace string, ip string, count int) (bool, error, string) {

        //log.Debug.Println(utils.GetFuncName())
        sudo := 1
        cmd := fmt.Sprintf("ip netns exec %s ping -c %d %s", namespace, count, ip)
        op, err := sshh.ExecCommandFromHost(cmd, sudo)

        //fmt.Println(op)
        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return false, err, op
        }

        /*
        res, _, _, loss_percent, _, _, _, _, _  := utils.ParsePingOutput(op)

        re :=regexp.MustCompile(`(\d+)%\s+packet loss`)
        res := re.FindStringSubmatch(op, -1)
        if len(res) == 2  {
                if loss, err := strconv.Atoi(res[1]); err != nil {
                        return false, nil, op
                }
        }
        */
        return true, nil, op
}




func (sshh *Credentials) ExecShowRouteFromHost() ([]map[string]string, error) { 

	//log.Debug.Println(utils.GetFuncName())
  	sudo := 0       // To Execute without Sudo

 	cmd := "route -n"
 	op, err := sshh.ExecCommandFromHost(cmd, sudo)

 	r := []map[string]string{}

 	if err != nil {
		return r, err
		log.Debug.Printf("Received from execute err %s", err)
	}
//    log.Debug.Printf("Received from execute output %s", op)
	re := regexp.MustCompile(`(\d+\.\d+\.\d+\.\d+)\s+(\d+\.\d+\.\d+\.\d+)\s+(\d+\.\d+\.\d+\.\d+)`)
	result := re.FindAllStringSubmatch(op, -1)
//	fmt.Println(result)
	var RouteInfo = []map[string]string{}
        for _, route_entry := range result {
		//fmt.Println(route_entry[1], route_entry[2], route_entry[3])
		RouteInfo = append(RouteInfo, map[string]string{
			"network": route_entry[1],
			"gateway": route_entry[2],
			"netmask": route_entry[3],
		})
	}
    /*
	re := regexp.MustCompile(`[0-9]+(\.[0-9]+)+`)
	result := re.FindAllString(op, -1)
	length := len(result)
	//l := int(length / 3)
	var RouteInfo = []map[string]string{}
	j := 0
	for i := 0; i < length/3; i++ {
		x := map[string]string{
			"network": "",
			"netmask": "",
			"gateway": "",
	 	}
		x["network"] = result[j]
		x["netmask"] = result[j+1]
		x["gateway"] = result[j+2]
		j = j + 3
		RouteInfo = append(RouteInfo, x)
	}	*/
	log.Debug.Printf("Returning output routeinfo %s", RouteInfo)
	log.Debug.Printf("Exiting ExecShowRouteFromHost functon...")
	return RouteInfo, nil
}


func (sshh *Credentials) ExecCopyFromHost(source string, dest string) (bool, error, string) {

	//log.Debug.Println(utils.GetFuncName())

	sudo := 1
       cmd := fmt.Sprintf("cp -rf %s %s", source, dest)

	op, err := sshh.ExecCommandFromHost(cmd, sudo)

        if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return false, err, op
	}
	return true, nil, op
}

func (sshh *Credentials) ExecWgetFromHost(url string, folder string, time_out ...int) (bool, error, string) {
 
 	//log.Debug.Println(utils.GetFuncName())

 	sudo := 0       // To Execute without Sudo

 	time := strconv.Itoa(time_out[0])
 	var cmd string
	// -t 1 is retries
 	if time == "900" {
 		cmd = "wget -t 1 "+url+" -P "+folder
 	} else {
 		cmd = "wget --timeout "+time+" -t 1 "+url+" -P "+folder
 	}
 	op, err := sshh.ExecCommandFromHost(cmd, sudo)

 	if err != nil {
		return false, err, op
		log.Debug.Printf("Received from execute err %s", err)
	}
        //log.Debug.Printf("Received from execute output %s", op)
	//log.Debug.Printf("Exiting ExecWgetFromHost functon...")

        var result []string
        re := regexp.MustCompile(`HTTP request sent, awaiting response....(\d{3})`)
        res := re.FindAllSubmatch([]byte(op), -1)
	result = append(result, string(res[len(res)-1][len(res[0])-1]))
	if string(res[len(res)-1][len(res[0])-1]) == "200" {
	    log.Debug.Printf("Status Code: %s", string(res[len(res)-1][len(res[0])-1]))
	    return true, nil, op
        }
	return false, nil, op

}

func (sshh *Credentials) Chmod(filename string, mode string) (string, error) {

    //log.Debug.Println(utils.GetFuncName())
    sudo := 1

    cmd := fmt.Sprintf("chmod %s %s", mode, filename)
    op, err := sshh.ExecCommandFromHost(cmd, sudo)
    if err != nil {
		return op, err
		log.Debug.Printf("Received from execute err %s", err)
	}

    return op, nil
}

func (sshh *Credentials) ExecEchoFromHost(text string, folder string) (string, error) {

 	//log.Debug.Println(utils.GetFuncName())
 
  	sudo := 0       // To Execute without Sudo

 	cmd := "echo "+text+" >> "+folder
 	op, err := sshh.ExecCommandFromHost(cmd, sudo)

 	if err != nil {
		return op, err
		log.Debug.Printf("Received from execute err %s", err)
	}
    //log.Debug.Printf("Received from echo output %s", op)
	log.Debug.Printf("Exiting ExecEchoFromHost functon...")

	return op, nil

}

func (sshh *Credentials) ExecCatFromHost(folder string) (string, error) { 

      //log.Debug.Println(utils.GetFuncName())

	sudo := 0       // To Execute without Sudo

      cmd := "cat "+folder
      op, err := sshh.ExecCommandFromHost(cmd, sudo)

      if err != nil {
log.Debug.Printf("Received from execute err %s", err)
return op, err
      }
  //log.Debug.Printf("Received from cat output %s", op)
//	log.Debug.Printf("Exiting ExecCatFromHost functon...")

	return op, nil

}

func (sshh *Credentials) ExecRemoveIpFromHost(iface string) (string, error) { 

	//log.Debug.Println(utils.GetFuncName())

	cmd :="ifconfig "+iface+" 0.0.0.0"
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		return op, err
		log.Debug.Printf("Received from execute err %s", err)
	}
 //   log.Debug.Printf("Received from RemoveIP output %s", op)
	log.Debug.Printf("Exiting ExecRemoveIpFromHost functon...")

	return op, nil

}

func (sshh *Credentials) ExecAddIpFromHost(iface string, ip string, netmask string) (string, error) { 

	//log.Debug.Println(utils.GetFuncName())
	sudo := 1       // To Execute with Sudo
	cmd :="ifconfig "+iface+" "+ip+" netmask "+netmask+" up"
	op, err := sshh.ExecCommandFromHost(cmd, sudo)

	if err != nil {
		return op, err
		log.Debug.Printf("Received from execute err %s", err)
	}
  //  log.Debug.Printf("Received from AddIP output %s", op)
	log.Debug.Printf("Exiting ExecAddIpFromHost functon...")

	return op, nil

}

func (sshh *Credentials) ExecCreateBgpConfFromHost(folder string, routeid string, tablename string, localas string, remoterouteid string, remoteas string, BirdRo []BirdRoute) (string, error) {

	sudo := 0       // To Execute without Sudo

	//log.Debug.Println(utils.GetFuncName())

	var br,s string
	for i:=0 ; i<len(BirdRo); i++{
		s="    route "+BirdRo[i].Network+":"+BirdRo[i].Mask+" via "+BirdRo[i].Gateway+";"
		br=br+"\n"+s
	}
	br="{"+br

	str := `
log syslog { debug, trace, info, remote, warning, error, auth, fatal, bug };
log stderr all;

debug protocols  { routes, filters, events };

timeformat protocol "%s";
timeformat route "%s";


protocol kernel {
	learn;			# Learn all alien routes from the kernel
	persist;		# Don't remove routes on bird shutdown
	scan time 20;		# Scan kernel routing table every 20 seconds
	import none;		# Default is import all
	export all;		# Default is export none
}

protocol device {
	scan time 10;		# Scan interfaces every 10 seconds
}
`
str1 := `;

protocol static `

str2 := `
}

protocol bgp {
    import all;
    export where proto = "`

str3 := `"; 
    local `
str4 := `;
    router id 10.0.2.4;
    direct;
}`

str3 = str2+tablename+str3+routeid+" as "+localas+";"+"\n"+"\tneighbour "+remoterouteid+" as "+remoteas+str4


strlast := `
EOL`	


	str = str+"\n"+"route id "+routeid +str1+tablename+br+"\n"+str3+strlast
	//fmt.Printf(str)
	
 	cmd := "cat "+" > "+folder+" <<EOL"+str
 	op, err := sshh.ExecCommandFromHost(cmd, sudo)

 	if err != nil {
		return op, err
		log.Debug.Printf("Received from execute err %s", err)
	}
  //  log.Debug.Printf("Received from createBgpConf output %s", op)
	log.Debug.Printf("Exiting ExecCreateBgpConfFromHost functon...")

	return op, nil

}

func (sshh *Credentials) ExecRemoveDhcpIpFromHost(iface string) (string, error) { 

 	//log.Debug.Println(utils.GetFuncName())
 
 	cmd :="dhclient -r "+iface
 	sudo := 1		// To Execute with Sudo
 	op, err := sshh.ExecCommandFromHost(cmd, sudo)
 	if err != nil {
		return op, err
		log.Debug.Printf("Received from execute err %s", err)
	}
//    log.Debug.Printf("Received from dhclient output %s", op)
	log.Debug.Printf("Exiting ExecDhClientFromHost functon...")

	return op, nil

}

func (sshh *Credentials) ExecDhClientFromHost(iface string) (string, error) { 

 	//log.Debug.Println(utils.GetFuncName())
 
 	cmd :="dhclient "+iface
 	sudo := 1		// To Execute with Sudo
 	op, err := sshh.ExecCommandFromHost(cmd, sudo)
 	if err != nil {
		return op, err
		log.Debug.Printf("Received from execute err %s", err)
	}
//    log.Debug.Printf("Received from dhclient output %s", op)
	log.Debug.Printf("Exiting ExecDhClientFromHost functon...")

	return op, nil

}

type iperfServerConfig struct {

	BandwidthReportInterval  int
        ReportFormat string
	OutputReportFile string
	ListenPort int
        UdpMode bool
	//Bandwidth int

}

type iperfClientConfig struct {

	BandwidthReportInterval  int
        ReportFormat string
	OutputReportFile string
	ListenPort int
        UdpMode bool
	Bandwidth int

	BidirectionalDualtest bool
	TimeSeconds int
        ParallelSessions int
	//BytesToTransmit int
}

func NewIperfTest(PortNo int, IsUdpMode bool, duration_sec int, no_of_sessions int, bandwidth int) (*iperfServerConfig, *iperfClientConfig) {

	bw_report_interval := 5
	report_format      := "k"
	report_file        := fmt.Sprintf("/home/lavelle/iperf-%s.txt", log.Log_date)

	var iperf_server_config iperfServerConfig = iperfServerConfig{
		BandwidthReportInterval: bw_report_interval,
		ReportFormat           : report_format,
		OutputReportFile       : report_file,
		ListenPort             : PortNo,
		UdpMode                : IsUdpMode,
		//Bandwidth              : bandwidth,
	}

	bidirectional_dual_test := true

	var iperf_client_config iperfClientConfig = iperfClientConfig{
		BandwidthReportInterval: bw_report_interval,
		ReportFormat           : report_format,
		OutputReportFile       : report_file,
		ListenPort             : PortNo,
		UdpMode                : IsUdpMode,
		Bandwidth              : bandwidth,
		BidirectionalDualtest  : bidirectional_dual_test,
		TimeSeconds            : duration_sec,
		ParallelSessions       : no_of_sessions,
	}

	return &iperf_server_config, &iperf_client_config
}

var iperf_client_output_file string = "/home/lavelle/abc.deepak.txt"
func (sshh *Credentials) StartIperfServer(iperf_server_config *iperfServerConfig) (string, error) {

	//log.Debug.Println(utils.GetFuncName())
	sudo := 1		// To Execute with Sudo

	cmd := "iperf -s"
	if iperf_server_config.UdpMode {
            cmd = cmd + " -u"
	}
	cmd = cmd + fmt.Sprintf(" -p %d", iperf_server_config.ListenPort)
	//cmd = cmd + fmt.Sprintf(" -b %dk", iperf_server_config.Bandwidth)
	cmd = cmd + fmt.Sprintf(" -i %d", iperf_server_config.BandwidthReportInterval)
	cmd = cmd + fmt.Sprintf(" -f %s", iperf_server_config.ReportFormat)
	cmd = cmd + fmt.Sprintf(" -o %s", iperf_server_config.OutputReportFile)
	//cmd = cmd + " -D" //run as daemon, but daemon mode not running properly


	var op string
	var err error
	go func() {
		op, err = sshh.ExecCommandFromHost(cmd, sudo)
		//fmt.Println(op)
		if err != nil {
			log.Debug.Printf("Received from execute err %s", err)
			//return op, err
		}
	}()
	//op, err = sshh.ExecCommandFromHost(cmd, sudo)
	//fmt.Println(op, err)

	return op, nil
}

func (sshh *Credentials) StopIperfServer() (bool) {

	//log.Debug.Println(utils.GetFuncName())

	//var op string
	var err error
	if _, err = sshh.KillAllProcess("iperf"); err!= nil {
		return false
	}
	//fmt.Println("killed: ", op)
	return true
}

func (sshh *Credentials) StopIperfClient() (float32, bool) {

	//log.Debug.Println(utils.GetFuncName())

	var average_rate float32
	var op string
	var err error

	if _, err = sshh.KillAllProcess("iperf"); err!= nil {
		return average_rate, false
	}

	if op, err = sshh.ExecCatFromHost(iperf_client_output_file); err!= nil {
		return average_rate, false
	}

        //re := regexp.MustCompile(`Sent.*([0-9.]+)\s+Kbits/sec.*Server Report.*([0-9.]+)\s+Kbits/sec`)
	//fmt.Println("op====%s=====", op)
        re := regexp.MustCompile(`([0-9.]+)\s+Kbits/sec`)
	matches := re.FindAllStringSubmatch(op, -1)
	//fmt.Println(matches)
	var sum_rate int = 0

	if len(matches) > 0 {
		for _, match := range matches {
			//fmt.Printf("-%s-\n",match[1])
		    if rate_int, err := strconv.Atoi(match[1]); err != nil {
			    fmt.Println(err)
			    return average_rate, false
		    } else {
                        sum_rate = sum_rate +  rate_int
			//fmt.Println(sum_rate)
		    }
		}
		average_rate = float32(sum_rate)/float32(len(matches))
		//fmt.Println(average_rate)
		return average_rate, true
	} else {
	    return average_rate, false
	}
	//fmt.Println("matches: ", matches)
}


func (sshh *Credentials) StartIperfClient(server_ip string, iperf_client_config *iperfClientConfig) (string, error) {

	//log.Debug.Println(utils.GetFuncName())
	sshh.Remove(iperf_client_output_file)
	sudo := 1		// To Execute with Sudo
	//iperf -s -u -p 1234

	cmd := fmt.Sprintf("nohup iperf -c %s", server_ip)
	if iperf_client_config.UdpMode {
            cmd = cmd + " -u"
	}
	cmd = cmd + fmt.Sprintf(" -p %d", iperf_client_config.ListenPort)
	cmd = cmd + fmt.Sprintf(" -b %dk", iperf_client_config.Bandwidth)
	cmd = cmd + fmt.Sprintf(" -i %d", iperf_client_config.BandwidthReportInterval)
	cmd = cmd + fmt.Sprintf(" -f %s", iperf_client_config.ReportFormat)
	cmd = cmd + fmt.Sprintf(" -o %s", iperf_client_config.OutputReportFile)

	if iperf_client_config.BidirectionalDualtest {
	    cmd = cmd + " -d"
	}
	cmd = cmd + fmt.Sprintf(" -t %d", iperf_client_config.TimeSeconds)
	cmd = cmd + fmt.Sprintf(" -P %d", iperf_client_config.ParallelSessions)

	cmd = cmd + fmt.Sprintf(" > %s", iperf_client_output_file)

	//fmt.Println(cmd)
	//cmd = fmt.Sprintf("nohup iperf -c %s -t %d", server_ip, iperf_client_config.TimeSeconds)

	var op string
	var err error
	//op, err = sshh.ExecCommandFromHost(cmd, sudo)
	go func() {
		op, err = sshh.ExecCommandFromHost(cmd, sudo)
		//fmt.Println(op)
		if err != nil {
			log.Debug.Printf("Received from execute err %s", err)
			//return op, err
		}
	}()
	//fmt.Println(op)
	//if err != nil {
	//    log.Debug.Printf("Received from execute err %s", err)
	//    return op, err
	//}

	return op, nil
}

func (sshh *Credentials) FlushFile(filename string) (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf(">> %s", filename)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil

}

/*
func (sshh *Credentials) Mtr(ip string) (bool, []string, []string) {

        nxt_hop_list := []string{}
	loss_list := []string{}
	//log.Debug.Println(utils.GetFuncName())
	mtr -nC 192.168.12.100
	cmd := fmt.Sprintf("mtr -nC %s", ip)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return false, nxt_hop_list, loss_list
	}

	op = string.Split(string.TrimSpace(op), "\n")
	if len(op) <= 1 {
		return false, nxt_hop_list, loss_list
	}


	loss_occured := false

        for i:= 1; i< len(op); i++ {
		mtr_res_list := strings.Split(mtr_res, ",")
		nxt_hop_list = append(nxt_hop_list,  mtr_res_list[5])
		loss_list = append(loss_list, mtr_res_list[6])
		loss_prcnt, ok := strconv.ParseFloat(mtr_res_list[6], 32); ok != nil {
		    return false, nxt_hop_list, loss_list
		}

		if  loss_prcnt > 40.0 {
			loss_occured = true
		}
	}

	return !loss_occured, namespaces, nil
}
*/

func (sshh *Credentials) ShowNamespaces() ([]string, error) {

	//log.Debug.Println(utils.GetFuncName())
	namespaces := []string{}

	cmd := fmt.Sprintf("ip netns ls")
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return namespaces, err
	}

	namespaces = strings.Split(strings.TrimSpace(op), "\n")
	return namespaces, nil
}

func (sshh *Credentials) KillAllProcessOnNamespace(namespace_name string, process_name string) (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf("ip netns exec %s killall -9 %s", namespace_name, process_name)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	//fmt.Println("=============\n")
	//fmt.Println(op)
	//fmt.Println("=============\n")
	//sshh.ExecCommandFromHost(cmd, sudo)

	return op, nil
}

func (sshh *Credentials) GetProcessInfo(filter string) (string, error) {

	//log.Debug.Println(utils.GetFuncName())
	cmd := fmt.Sprintf("ps -aef")
	filter = strings.TrimSpace(filter)

	if filter != "" {
		cmd = cmd + fmt.Sprintf(` | grep "%s"`, filter)
	}

	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}

func (sshh *Credentials) KillProcessId(process_id int) (string, error) {

	//log.Debug.Println(utils.GetFuncName())
	cmd := fmt.Sprintf("kill -9 %d", process_id)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}

func (sshh *Credentials) KillAllProcess(process_name string) (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf("killall -9 %s", process_name)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	//fmt.Println("=============\n")
	//fmt.Println(op)
	//fmt.Println("=============\n")
	//sshh.ExecCommandFromHost(cmd, sudo)

	return op, nil
}

func (sshh *Credentials) DisableRootSSH() (string, error) {
	if _, err := sshh.ChangePermitRootLoginSSHDConfigNo(); err != nil {
		return "", err
	}

	if op, err := sshh.RestartSSHService(); err != nil {
		return "", err
	} else {
            return op, nil
        }
}

func (sshh *Credentials) EnableRootSSH() (string, error) {
	if _, err := sshh.ChangePermitRootLoginSSHDConfigYes(); err != nil {
		return "", err
	}

	if op, err := sshh.RestartSSHService(); err != nil {
		return "", err
	} else {
            return op, nil
        }
}

func (sshh *Credentials) ChangePermitRootLoginSSHDConfigNo() (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf(`sed -i 's/PermitRootLogin without-password/PermitRootLogin no/g; s/PermitRootLogin yes/PermitRootLogin no/g; s/PermitRootLogin forced-commands-only/PermitRootLogin no/g' /etc/ssh/sshd_config`)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	fmt.Println(op)
	return op, nil
}

func (sshh *Credentials) ChangePermitRootLoginSSHDConfigWithoutPasswd() (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf(`sed -i 's/PermitRootLogin yes/PermitRootLogin without-password/g; s/PermitRootLogin no/PermitRootLogin without-password/g; s/PermitRootLogin forced-commands-only/PermitRootLogin without-password/g' /etc/ssh/sshd_config`)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	fmt.Println(op)
	return op, nil
}

func (sshh *Credentials) ChangePermitRootLoginSSHDConfigYes() (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf(`sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/g; s/PermitRootLogin no/PermitRootLogin yes/g; s/PermitRootLogin forced-commands-only/PermitRootLogin yes/g' /etc/ssh/sshd_config`)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	fmt.Println(op)
	return op, nil
}

func (sshh *Credentials) RestartSSHService() (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf("service ssh restart")
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	fmt.Println(op)
	return op, nil
}


func (sshh *Credentials) Remove(filename string) (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf("rm -rf %s", filename)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	fmt.Println(op)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}

func (sshh *Credentials) AptgetInstall(package_name string) (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf("apt-get install -y %s", package_name)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}

func (sshh *Credentials) DeleteDefaultRoute() (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf("ip route del default")
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}

func (sshh *Credentials) AddDefaultRoute(gw string) (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf("ip route add default via %s", gw)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}

func (sshh *Credentials) IpRouteShow() (map[string]string, map[string]string, map[string]string, error) {

	//log.Debug.Println(utils.GetFuncName())
	default_route := map[string]string{}
	onlink_routes := map[string]string{}
	routes := map[string]string{}

	cmd := fmt.Sprintf("ip route show")
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return default_route, onlink_routes, routes, err
	}

        default_route, onlink_routes, routes = utils.ParseIpRouteShowOutput(op)
	return default_route, onlink_routes, routes, nil

}

func (sshh *Credentials) AddRoute(network_addr string, mask_length int, gw string) (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf("ip route add %s/%d via %s", network_addr, mask_length, gw)
	fmt.Println(cmd)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}


func (sshh *Credentials) SetInterfaceModeStatic(iface string, ip_addr string, mask string, gateway ...string) (bool, error) {

    var op string
    var err error

    nw_file := "/etc/network/interfaces"
    nw_file_bkp := "/etc/network/interfaces.bkp"

    if res, err, _ := sshh.ExecCopyFromHost(nw_file, nw_file_bkp); !res {
	    return  false, err
    }

    if op, err = sshh.ExecCatFromHost(nw_file); err != nil {
	    return false, nil
    }

    new_op := []string{}
    re := regexp.MustCompile(`^\s*#`)
    for _, line := range strings.Split(op, "\n") {
	    if !re.MatchString(line) {
		new_op = append(new_op, line)
	    }
    }


    new_op2 := strings.Join(new_op, "\n")
    fmt.Println(new_op2)

    re2 := regexp.MustCompile(fmt.Sprintf(`iface\s+%s\s+inet\s+(\w+).*?\n\s*(auto|iface)?`, iface))

    res := re2.FindAllStringSubmatch(new_op2, -1)
    fmt.Println(res[0][1])


    return true, nil
}

func (sshh *Credentials) DeleteRoute(network_addr string, mask_length int, gw string) (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf("ip route delete %s/%d via %s", network_addr, mask_length, gw)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}

func (sshh *Credentials) DisableIPFwding() (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf("sysctl -w net.ipv4.ip_forward=0 && sysctl -p /etc/sysctl.conf")
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}

func (sshh *Credentials) CreateNamespace(namespace_name string) (string, error) {

	cmd := fmt.Sprintf("ip netns add %s", namespace_name)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}

func (sshh *Credentials) DeleteNamespace(namespace_name string) (string, error) {

	cmd := fmt.Sprintf("ip netns del %s", namespace_name)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}

func (sshh *Credentials) DnsResolveHost(host_name string) (string, error) {

	ip := ""
	cmd := fmt.Sprintf("host %s", host_name)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return ip, err
	}

	op = strings.TrimSpace(op)

	re := regexp.MustCompile(`has address\s+(\d+\.\d+\.\d+\.\d+)`)
	res := re.FindAllStringSubmatch(op, -1)
	if len(res) > 0 {
		if len(res[0]) == 2 {
			ip = res[0][1]
	                return ip, nil
		}
	}
	return ip, errors.New(fmt.Sprintf("No IP address found for %s", host_name))
}


func (sshh *Credentials) ListNamespaces() ([]string, error) {

	cmd := fmt.Sprintf("ip netns ls")
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return []string{}, err
	}

	op = strings.TrimSpace(op)
	return strings.Split(op, "\n"), nil
}

func (sshh *Credentials) MoveIfaceToNamespace(namespace_name string, iface_name string) (string, error) {

	cmd := fmt.Sprintf("ip link set %s netns %s", iface_name, namespace_name)
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}

	return op, nil
}

func (sshh *Credentials) EnableIPFwding() (string, error) {

	//log.Debug.Println(utils.GetFuncName())

	cmd := fmt.Sprintf("sysctl -w net.ipv4.ip_forward=1 && sysctl -p /etc/sysctl.conf")
	sudo := 1		// To Execute with Sudo
	op, err := sshh.ExecCommandFromHost(cmd, sudo)
	if err != nil {
		log.Debug.Printf("Received from execute err %s", err)
		return op, err
	}
	return op, nil
}

func (sshh *Credentials) Download(remote, local string) error {

	//log.Debug.Println(utils.GetFuncName())

	if _, err := sshh.Connect(); err!= nil {
		return err
	}

	client := sshh.client

	if !sshh.Persistent {
		//fmt.Println("deferrrig close()")
		defer func(sshh *Credentials) { sshh.connected = false }(sshh)
	        defer client.Close()
	} else {
		//fmt.Println("skipped deferrring close()...")
	}

	return sshh.client.Download(remote, local)
}

func (sshh *Credentials) Upload(local, remote string) error {

	//log.Debug.Println(utils.GetFuncName())

	if _, err := sshh.Connect(); err!= nil {
		return err
	}

	client := sshh.client

	if !sshh.Persistent {
		//fmt.Println("deferrrig close()")
		defer func(sshh *Credentials) { sshh.connected = false }(sshh)
	        defer client.Close()
	} else {
		fmt.Println("skipped deferrring close()...")
	}

	return sshh.client.Upload(local, remote)
}
