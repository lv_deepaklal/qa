package hostutils

import (
	utils "lvnautomation/utils"
	log "lvnautomation/lvnlog"
	"fmt"
	"strings"
	"regexp"
	"time"
	"errors"
	"strconv"
	"sync"
)

type Tcpdump struct {
	Iface string
	interval int
	Filter string
	Write_to_file string
	RedirectToFile bool
	WriteToPcap bool
	cmd string
	Namespace string
	CapturedPkts []string
}

var TCPDUMP_INTERVAL_SEC int = 600

func (t *Tcpdump) SetFilter(filter_info string) {
	filter_info =  strings.TrimSpace(filter_info)
	if filter_info != "" {
		t.Filter = filter_info
	}
}

func (t *Tcpdump) SetInterval(i int) {

    if i <= 0 || i > 1800 {
	    log.Err.Printf("Tcpdump interval provided: %d sec. Supported value 1 - 1800 sec. Setting it to default value: %d sec\n", i, TCPDUMP_INTERVAL_SEC)
	    t.interval = TCPDUMP_INTERVAL_SEC
    } else {
	    t.interval = i
    }
}

func NewTcpDump() *Tcpdump {

    var t Tcpdump
    t.SetInterval(TCPDUMP_INTERVAL_SEC)
    t.Iface = "any"
    t.Write_to_file = "tcpdump_" + utils.GetCurrentDateTimeForFile()
    t.RedirectToFile = false
    t.WriteToPcap = true
    t.Namespace = ""
    return &t
}

func (t *Tcpdump) EnableRedirectToFile() {
    t.RedirectToFile = true
    t.WriteToPcap = false
}

func (sshh *Credentials) IsReachableFromLanNamespace(ip string) (bool, error) {

	//log.Debug.Println(utils.GetFuncName())
	namespace := "ns-svc-lan"
	return sshh.IsReachableFromNamespace(namespace, ip)

}

/*
func (sshh *Credentials) IsReachableFromNamespace(namespace string, ip string) (bool, error) {

	//log.Debug.Println(utils.GetFuncName())
	count := 5
	res, err, output := sshh.PingFromNamespace(namespace, ip, count)
	if !res {
		return res, err
	}

	res2, _, _, loss_percent, _, _, _, _, _  := utils.ParsePingOutput(output)
	if !res2  {
		return res2, nil
	}

	if loss_percent > 1 {
		return false, errors.New("Ping loss percent > 1%")
	}
	return true, nil
}
*/

/*
func (sshh *Credentials) PingFromNamespace(namespace string, ip string, count int) (bool, error, string) {

	//log.Debug.Println(utils.GetFuncName())
        sudo := 1
	cmd := fmt.Sprintf("ip netns exec %s ping -c %d %s", namespace, count, ip)
        op, err := sshh.ExecCommandFromHost(cmd, sudo)

	//fmt.Println(op)
        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return false, err, op
        }

	
	//res, _, _, loss_percent, _, _, _, _, _  := utils.ParsePingOutput(op)

	//re :=regexp.MustCompile(`(\d+)%\s+packet loss`)
	//res := re.FindStringSubmatch(op, -1)
	//if len(res) == 2  {
	//	if loss, err := strconv.Atoi(res[1]); err != nil {
	//		return false, nil, op
	//	}
	//}
        return true, nil, op
}
*/

func (sshh *Credentials) ExecTcpdumpOnNamespace(namespace string, t *Tcpdump) (bool, error, string) {

        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	//cmd := fmt.Sprintf("tcpdump -i %s -G %d -W 1 -w %s.pcap", t.Iface, t.interval, t.Write_to_file)
	hexprint := ""
	write_info := ""

	if t.RedirectToFile && t.WriteToPcap {
            t.RedirectToFile =  false
	}

	if !(t.RedirectToFile || t.WriteToPcap) {
		t.WriteToPcap = true
	}

	if t.RedirectToFile {
            write_info = fmt.Sprintf("> %s", t.Write_to_file)
	    hexprint = "-xx"
	}

	if t.WriteToPcap {
            write_info = fmt.Sprintf("-w %s.pcap", t.Write_to_file)
	}

	cmd := fmt.Sprintf("nohup ip netns exec %s tcpdump %s -ni %s %s", namespace, hexprint, t.Iface, write_info)
	t.cmd = fmt.Sprintf("nohup ip netns exec %s tcpdump %s -ni %s", namespace, hexprint, t.Iface)
	//cmd := fmt.Sprintf("sh tcpdump.sh")
        op, err := sshh.ExecCommandFromHost(cmd, sudo)

	//fmt.Println(op)
        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return false, err, op
        }

        return false, nil, op
}

func (sshh *Credentials) ExecTcpdump(t *Tcpdump) (bool, error, string) {

        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	//cmd := fmt.Sprintf("tcpdump -i %s -G %d -W 1 -w %s.pcap", t.Iface, t.interval, t.Write_to_file)
	hexprint := ""
	write_info := ""
	namespace_info := ""
	filter_info := ""
	if t.RedirectToFile && t.WriteToPcap {
            t.RedirectToFile =  false
	}

	if !(t.RedirectToFile || t.WriteToPcap) {
		t.WriteToPcap = true
	}

	if t.RedirectToFile {
            write_info = fmt.Sprintf("> %s", t.Write_to_file)
	    hexprint = "-xx"
	}

	if t.WriteToPcap {
            write_info = fmt.Sprintf("-w %s.pcap", t.Write_to_file)
	}

	if strings.TrimSpace(t.Namespace) != "" {
            namespace_info = fmt.Sprintf("ip netns exec %s", t.Namespace)
	}

	t.Filter = strings.TrimSpace(t.Filter)
	if t.Filter != "" {
           filter_info = t.Filter
	}


	cmd := fmt.Sprintf("nohup %s tcpdump %s -ni %s %s %s", namespace_info, hexprint, t.Iface, filter_info, write_info)
	t.cmd = fmt.Sprintf("nohup %s tcpdump %s -ni %s", namespace_info, hexprint, t.Iface, filter_info)

        op, err := sshh.ExecCommandFromHost(cmd, sudo)
	//fmt.Println(op)
        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return false, err, op
        }

        return false, nil, op
}

type LavelleAppsStatus struct {
	Service string
	IsRunning bool
	ProcessId string
}

var service_status_map map[string]bool = map[string]bool {
    "start/running": true,
    "stop/waiting" : false,
}

func (sshh *Credentials) GetLavelleServiceStatus() (bool, error, map[string]LavelleAppsStatus) {

        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	cmd := fmt.Sprintf("service lavelle-apps status")
        op, err := sshh.ExecCommandFromHost(cmd, sudo)

	lavelle_apps_stats := make(map[string]LavelleAppsStatus)
        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return false, err, lavelle_apps_stats
        }

	process_info_list := strings.Split(strings.Trim(op, "\n"), "\n")
	for _, process_info := range process_info_list {
            //re := regexp.MustCompile(`([a-z_]+)\s+([a-z/]+)?(,\s+ process (\d+))`)
            re := regexp.MustCompile(`([a-z-]+)\s+([a-z/]+)($|,\s+process\s+(\d+))`)
	    x := re.FindStringSubmatch(process_info)

	    var is_running, ok bool
	    var process_id string
	    if is_running, ok = service_status_map[x[2]]; ok {
		    process_id = x[4]
	    } else {
                   is_running = false
		   process_id = ""
	    }
	    lavelle_apps_stats[x[1]] = LavelleAppsStatus{Service: x[1], IsRunning: is_running, ProcessId: process_id}
	    //fmt.Println(len(x), x[1], x[2], x[4])
	}
        return true, nil, lavelle_apps_stats
}

func (sshh *Credentials) GetCpeVersion() (bool, error, map[string]string) {

        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	cmd := fmt.Sprintf("lnpkg list")
        op, err := sshh.ExecCommandFromHost(cmd, sudo)

	version_info := make(map[string]string)
        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return false, err, version_info
        }
/*	root@hub-5078:/# lnpkg list
filebeat        5.1.1
statsbeat       5.4.2-1
lavelle-linux-bintools  10.0.1_R_dcdf951
cloud-port      10.0.1_R_dcdf951
*/
//fmt.Println(op)
        //re := regexp.MustCompile(`filebeat\s+[\d\.-]+\nstatsbeat\s+[\d\.-]\nlavelle-linux-bintools\s+[a-zA-Z\._\d]+\ncloud-port\s+[a-zA-Z\._\d]+`)
        re := regexp.MustCompile(`(filebeat)\s+([\d\.-]+)\n(statsbeat)\s+([\d\.-]+)\n(lavelle-linux-bintools)\s+([a-zA-Z\._\d]+)\n(cloud-port)\s+([a-zA-Z\._\d]+)`)
	x := re.FindStringSubmatch(op)
	for i := 1; i < len(x); i=i+2 {
             version_info[x[i]] = x[i+1]
	}
	//fmt.Println(version_info)
	return true, nil, version_info
}

func (sshh *Credentials) FlushLavellePathRecLog() bool {
	filename := "lavelle-pathrec.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}

func (sshh *Credentials) FlushLavelleAlertLog() bool {
	filename := "lavelle-alert.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleWebuiLog() bool {
	filename := "lavelle-webui.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleKrtLog() bool {
	filename := "lavelle-krt.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleFileServerLog() bool {
	filename := "lavelle-file-server.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleModemManagerLog() bool {
	filename := "lavelle-modem-mgr.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleLndpiAppLog() bool {
	filename := "lavelle-lndpi-app.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavellFwdStateLog() bool {
	filename := "lavelle-fwdstate.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleLndpiLog() bool {
	filename := "lavelle-lndpi.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleLinkStateLog() bool {
	filename := "lavelle-linkstate.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleLnTransportLog() bool {
	filename := "lavelle-lntransport.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleReachabilityLog() bool {
	filename := "lavelle-reachability.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleDiagCmdsLog() bool {
	filename := "lavelle-diag-cmds.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleLinkUtilLog() bool {
	filename := "lavelle-linkutil.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}

func (sshh *Credentials) FlushLavelleCnidLog() bool {
	filename := "lavelle-cnid.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleRevtunnelLog() bool {
	filename := "lavelle-revtunnel.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleUflowLog() bool {
	filename := "lavelle-uflow.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleSyncDateServiceLog() bool {
	filename := "lavelle-syncdate-service.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleUflowReaderLog() bool {
	filename := "lavelle-uflowrdr.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleUpgradeServiceLog() bool {
	filename := "lavelle-upgrade-service.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleServiceCronJobLog() bool {
	filename := "lavelle-service-cronjobs.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleIpv6SlaacMonitorLog() bool {
	filename := "lavelle-ipv6-slaac-monitor.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleEbrLog() bool {
	filename := "lavelle-ebr.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleSysFwLog() bool {
	filename := "lavelle-sysfw.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleIpsLog() bool {
	filename := "lavelle-ips.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleIpsNamespaceLog() bool {
	filename := "lavelle-ips-ns-svc.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleSysdefrouteLog() bool {
	filename := "lavelle-sysdefroute.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleLndpiServiceLog() bool {
	filename := "lavelle-lndpi-svc.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavellePpeLog() bool {
	filename := "lavelle-ppe.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavellePathDrLog() bool {
	filename := "lavelle-pathrdr.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleLinkProbeLog() bool {
	filename := "lavelle-link-probe.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleNatFwLog() bool {
	filename := "lavelle-natfw-svc.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}
func (sshh *Credentials) FlushLavelleCpLog() bool {
	filename := "lavelle-cp.log"
	if _, err := sshh.FlushFile(fmt.Sprintf("/var/log/upstart/%s", filename)); err != nil {
		return false
	}
	return true
}

func (sshh *Credentials) FlushAllCpeLogs() (bool) {

    return true
}

func (sshh *Credentials) GetIptablesInfo() (string, error) {

        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	//cmd := "ip netns exec ns-svc-natfw iptables -L -n"
	cmd := "iptables -L -n"
	op, err := sshh.ExecCommandFromHost(cmd, sudo)

        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return op, err
        }

	//log.Debug.Printf("CPE %s: %s", sshh.Host, op)
        return op, nil
}

func (sshh *Credentials) GetIptablesInfoFromNatNamespace() (string, error) {

        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	cmd := "ip netns exec ns-svc-natfw iptables -L -n"
	op, err := sshh.ExecCommandFromHost(cmd, sudo)

        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return op, err
        }

	//log.Debug.Printf("CPE %s: %s", sshh.Host, op)
        return op, nil
}

func (sshh *Credentials) StopOvsDumpFlows() (string, error) {
	sshh.stop_loop = true
	return "", nil
	//op, err := sshh.ExecCatFromHost("/home/lavelle/ovs.txt")
	//return op, err
}

func (sshh *Credentials) GetOvsDumpFlowsEvery(duration int) (map[string][]string, bool) {

        //log.Debug.Println(utils.GetFuncName())

	//nohup tcpdump -i eth0 -w test.cap >/dev/null 2>&1 &
	//cmd := fmt.Sprintf("tcpdump -i %s -G %d -W 1 -w %s.pcap", t.Iface, t.interval, t.Write_to_file)
	//cmd := "> /home/lavelle/ovs.txt"
        //sshh.ExecCommandFromHost(cmd, sudo)

	var op map[string][]string
	var err error

         for {

            op, err = sshh.GetOvsDumpFlows()
            if err != nil {
                    log.Debug.Printf("Received from execute err %s", err)
                    return op, false
            }

	    time.Sleep(time.Duration(duration) * time.Second)
	    if sshh.stop_loop {
		    break
	    }
                 return op, true
	}
	//fmt.Println(op)
                    return op, false
}


func (sshh *Credentials) GetOvsDumpFlowsForTableNo(table_no int) ([]string, error) {

        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	cmd := fmt.Sprintf("/home/lavelle/ofctl dump-flows br0 table=%d", table_no)

	var op string
	var err error

        op, err = sshh.ExecCommandFromHost(cmd, sudo)
            if err != nil {
		    log.Debug.Printf("CPE %s: Received from execute err %s", sshh.Host, err)
                    return []string{},  err
            }

        //log.Debug.Printf("CPE %s: %s", sshh.Host, op)
	rules_list := strings.Split(strings.TrimSpace(op), "\n")
	//fmt.Println(rules_list)
	rules_list = rules_list[1:]
	log.Debug.Printf("table no :%d, no_of_rules: %d\n", table_no, len(rules_list))

	//for i, rule := range rules_list {
	//	fmt.Printf("Rule no: %d, Rule: %s\n", i, rule)
	//}
	//    return []string{}, nil
        return rules_list, nil
}

func (sshh *Credentials) GetOvsDumpFlowsForAcl() ([]string, error) {

        //log.Debug.Println(utils.GetFuncName())

	sshh.CatAclJson()
	table_no := 1
	return sshh.GetOvsDumpFlowsForTableNo(table_no)

}

func (sshh *Credentials) GetOvsDumpFlowsForQos() ([]string, error) {

        //log.Debug.Println(utils.GetFuncName())
	table_no := 2
	return sshh.GetOvsDumpFlowsForTableNo(table_no)

}

func (sshh *Credentials) GetOvsDumpFlowsForTrl() ([]string, error) {

        //log.Debug.Println(utils.GetFuncName())
	table_no := 3
	return sshh.GetOvsDumpFlowsForTableNo(table_no)

}

func (sshh *Credentials) GetOvsDumpFlowsForLoadBalance() ([]string, error) {

        //log.Debug.Println(utils.GetFuncName())
	table_no := 4
	return sshh.GetOvsDumpFlowsForTableNo(table_no)

}

func (sshh *Credentials) GetOvsDumpFlowsForForwarding() ([]string, error) {

        //log.Debug.Println(utils.GetFuncName())
	table_no := 8
	return sshh.GetOvsDumpFlowsForTableNo(table_no)

}

func (sshh *Credentials) GetOvsDumpFlows() (map[string][]string, error) {

        //log.Debug.Println(utils.GetFuncName())
	table_rule_map := make(map[string][]string)

	if acl, err := sshh.GetOvsDumpFlowsForAcl(); err == nil {
	    table_rule_map["acl"] = acl
	} else {
            return table_rule_map, errors.New("Unable to get dump flows data for ACL.")
	}

	if qos, err := sshh.GetOvsDumpFlowsForQos(); err == nil {
	    table_rule_map["qos"] = qos
	} else {
            return table_rule_map, errors.New("Unable to get dump flows data for QOS.")
	}

	if trl, err := sshh.GetOvsDumpFlowsForTrl(); err == nil {
	    table_rule_map["trl"] = trl
	} else {
            return table_rule_map, errors.New("Unable to get dump flows data for TRL.")
	}

	if lb, err := sshh.GetOvsDumpFlowsForLoadBalance(); err == nil {
	    table_rule_map["lb"] = lb
	} else {
            return table_rule_map, errors.New("Unable to get dump flows data for LB.")
	}

	if fwd, err := sshh.GetOvsDumpFlowsForForwarding(); err == nil {
	    table_rule_map["fwd"] = fwd
	} else {
            return table_rule_map, errors.New("Unable to get dump flows data for FWD.")
	}

	return table_rule_map, nil
}


func (sshh *Credentials) CatAclJson() (string, error) {

	filename := "/opt/lavelle/cloud-port/etc/table_1.json"
	return sshh.ExecCatFromHost(filename)
}


func (sshh *Credentials) Tailf(filename string, output_file string) (bool) {

        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	//nohup tcpdump -i eth0 -w test.cap >/dev/null 2>&1 &
	//cmd := fmt.Sprintf("tcpdump -i %s -G %d -W 1 -w %s.pcap", t.Iface, t.interval, t.Write_to_file)
	cmd := fmt.Sprintf("nohup tail -f %s > %s", filename, output_file)
	//cmd := fmt.Sprintf("nohup sh /home/lavelle/tcpdump.sh & ")
	//fmt.Println(cmd)
        //_, _= sshh.ExecCommandFromHost(fmt.Sprintf("nohup tail -f %s > %s", filename, output_file), sudo)
        _, err := sshh.ExecCommandFromHost(cmd, sudo)
	//fmt.Println(op)

        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return false
        }

        return true
}

func (sshh *Credentials) TcpdumpLndpi() (bool, error, string) {

        //log.Debug.Println(utils.GetFuncName())
	ifaces := []string{"2.in", "2.out", "s2.in", "s2.out"}

	var t *Tcpdump
	var i []*Tcpdump

	for _, iface := range ifaces {

            t= NewTcpDump()
            t.Iface = iface
            t.Write_to_file = fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s", log.LogFilename, iface, log.Log_date)
	    i = append(i, t)

	}

        for _, tcpdump_config :=  range i {
		go sshh.ExecTcpdump(tcpdump_config)
	}

        return false, nil, ""
}

func (sshh *Credentials) TcpdumpIps() (bool, error, string) {

        //log.Debug.Println(utils.GetFuncName())
	ifaces := []string{"s3.ipsl", "s3.ipsw", "3.ipsl", "3.ipsw"}
	namespace := []string{"", "", "ns-svc-ips", "ns-svc-ips"}
	namespace_bool := []bool{false, false, true, true}

	var t *Tcpdump
	var i []*Tcpdump

	for _, iface := range ifaces {

            t= NewTcpDump()
            t.Iface = iface
            t.Write_to_file = fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s", log.LogFilename, iface, log.Log_date)
	    i = append(i, t)

	}

        for idx, tcpdump_config :=  range i {
		if namespace_bool[idx] {
		    go sshh.ExecTcpdumpOnNamespace(namespace[idx], tcpdump_config)
		} else {
		    go sshh.ExecTcpdump(tcpdump_config)
		}
	}

        return false, nil, ""
}

func (sshh *Credentials) TcpdumpWanIface(waniface string) (bool, error, string) {

	waniface = strings.ToLower(waniface)

        //log.Debug.Println(utils.GetFuncName())
	ifaces := []string{waniface, fmt.Sprintf("s0.%s", waniface), fmt.Sprintf("sw0.%s", waniface), fmt.Sprintf("0.%s", waniface), fmt.Sprintf("w0.%s", waniface)}
	namespace := []string{"", "", "", "ns-svc-natfw", "ns-svc-natfw"}
	namespace_bool := []bool{false, false, false, true, true}

	var t *Tcpdump
	var i []*Tcpdump

	for _, iface := range ifaces {

            t= NewTcpDump()
            t.Iface = iface
            t.Write_to_file = fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s", log.LogFilename, iface, log.Log_date)
	    i = append(i, t)

	}

        for idx, tcpdump_config :=  range i {
		if namespace_bool[idx] {
		    go sshh.ExecTcpdumpOnNamespace(namespace[idx], tcpdump_config)
		} else {
		    go sshh.ExecTcpdump(tcpdump_config)
		}
	}

        return false, nil, ""
}

func (sshh *Credentials) TcpdumpLanIface(laniface string) (bool, error, string) {

	laniface = strings.ToLower(laniface)

        //log.Debug.Println(utils.GetFuncName())
	ifaces := []string{laniface, fmt.Sprintf("s0.%s", laniface), fmt.Sprintf("0.%s", laniface)}
	namespace := []string{"", "", "ns-svc-natfw"}

	namespace_bool := []bool{false, false, true}

	var t *Tcpdump
	var i []*Tcpdump

	for _, iface := range ifaces {

            t= NewTcpDump()
            t.Iface = iface
            t.Write_to_file = fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s", log.LogFilename, iface, log.Log_date)
	    i = append(i, t)

	}

        for idx, tcpdump_config :=  range i {
		if namespace_bool[idx] {
		    go sshh.ExecTcpdumpOnNamespace(namespace[idx], tcpdump_config)
		} else {
		    go sshh.ExecTcpdump(tcpdump_config)
		}
	}

        return false, nil, ""
}

func (sshh *Credentials) TcpdumpLanDp() (bool, error, string) {

        //log.Debug.Println(utils.GetFuncName())
	ifaces := []string{"s1.lan-dp", "s.lan-sys", "1.lan-dp", "lan-sys"}
	namespace := []string{"", "", "ns-svc-lan", "ns-svc-lan"}

	namespace_bool := []bool{false, false, true, true}

	var t *Tcpdump
	var i []*Tcpdump

	for _, iface := range ifaces {

            t= NewTcpDump()
            t.Iface = iface
            t.Write_to_file = fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s", log.LogFilename, iface, log.Log_date)
	    i = append(i, t)

	}

        for idx, tcpdump_config :=  range i {
		if namespace_bool[idx] {
		    go sshh.ExecTcpdumpOnNamespace(namespace[idx], tcpdump_config)
		} else {
		    go sshh.ExecTcpdump(tcpdump_config)
		}
	}

        return false, nil, ""
}

func (sshh *Credentials) Zip(zipname string, files ...string) bool {

        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	//zip_filename = fmt.Sprintf("/home/lavelle/file-server-files/%s_pcaps_%s.zip", log.LogFilename, log.Log_date)
	//files =  fmt.Sprintf("/home/lavelle/file-server-files/%s*%s.pcap", log.LogFilename, log.Log_date)
	cmd := fmt.Sprintf("zip -r %s %s", zipname, strings.Join(files, " "))
	//fmt.Println(cmd)
        _, err := sshh.ExecCommandFromHost(cmd, sudo)

        if err != nil {
		fmt.Println(err.Error())
                log.Debug.Printf("Received from execute err %s", err)
                return false
        }
        //log.Debug.Printf("op %s\n", op)
	return true
}

func (sshh *Credentials) ZipAllPcaps() bool {

        //log.Debug.Println(utils.GetFuncName())
	zip_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_pcaps_%s.zip", log.LogFilename, log.Log_date)
	pcap_files := fmt.Sprintf("/home/lavelle/file-server-files/%s_*_%s*.pcap", log.LogFilename, log.Log_date)
	return sshh.Zip(zip_filename, pcap_files)
}

func (sshh *Credentials) TailfLavellePathRecLog() bool {
	filename := "lavelle-pathrec.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleAlertLog() bool {
	filename := "lavelle-alert.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}
func (sshh *Credentials) TailfLavelleWebuiLog() bool {
	filename := "lavelle-webui.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleKrtLog() bool {
	filename := "lavelle-krt.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleFileServerLog() bool {
	filename := "lavelle-file-server.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleModemManagerLog() bool {
	filename := "lavelle-modem-mgr.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleLndpiAppLog() bool {
	filename := "lavelle-lndpi-app.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavellFwdStateLog() bool {
	filename := "lavelle-fwdstate.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleLndpiLog() bool {
	filename := "lavelle-lndpi.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleLinkStateLog() bool {
	filename := "lavelle-linkstate.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleLnTransportLog() bool {
	filename := "lavelle-lntransport.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleReachabilityLog() bool {
	filename := "lavelle-reachability.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleDiagCmdsLog() bool {
	filename := "lavelle-diag-cmds.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleLinkUtilLog() bool {
	filename := "lavelle-linkutil.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleCnidLog() bool {
	filename := "lavelle-cnid.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleRevtunnelLog() bool {
	filename := "lavelle-revtunnel.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleUflowLog() bool {
	filename := "lavelle-uflow.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleSyncDateServiceLog() bool {
	filename := "lavelle-syncdate-service.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleUflowReaderLog() bool {
	filename := "lavelle-uflowrdr.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleUpgradeServiceLog() bool {
	filename := "lavelle-upgrade-service.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleServiceCronJobLog() bool {
	filename := "lavelle-service-cronjobs.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleIpv6SlaacMonitorLog() bool {
	filename := "lavelle-ipv6-slaac-monitor.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleEbrLog() bool {
	filename := "lavelle-ebr.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleSysFwLog() bool {
	filename := "lavelle-sysfw.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleIpsLog() bool {
	filename := "lavelle-ips.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleIpsNamespaceLog() bool {
	filename := "lavelle-ips-ns-svc.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleSysdefrouteLog() bool {
	filename := "lavelle-sysdefroute.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleLndpiServiceLog() bool {
	filename := "lavelle-lndpi-svc.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavellePpeLog() bool {
	filename := "lavelle-ppe.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavellePathDrLog() bool {
	filename := "lavelle-pathrdr.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleLinkProbeLog() bool {
	filename := "lavelle-link-probe.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleNatFwLog() bool {
	filename := "lavelle-natfw-svc.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}

func (sshh *Credentials) TailfLavelleCpLog() bool {
	filename := "lavelle-cp.log"
	output_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s.log", log.LogFilename, strings.Split(filename, ".")[0], log.Log_date)
	return sshh.Tailf(fmt.Sprintf("/var/log/upstart/%s", filename), output_filename)
}



func (sshh *Credentials) TailfLavelleLogs() {
	go sshh.TailfLavelleCpLog()
	go sshh.TailfLavelleNatFwLog()
	go sshh.TailfLavellePpeLog()
	go sshh.TailfLavelleLndpiAppLog()
	go sshh.TailfLavelleLndpiServiceLog()
	go sshh.TailfLavelleLndpiLog()
	go sshh.TailfLavelleUflowLog()
	go sshh.TailfLavellFwdStateLog()
	go sshh.TailfLavelleUflowReaderLog()
	go sshh.TailfLavelleIpsLog()
	go sshh.TailfLavelleIpsNamespaceLog()
}

func (sshh *Credentials) KillAllTailf() {
	sshh.KillAllProcess("tail")
}

func (sshh *Credentials) KillAllTcpdump() {
	sshh.KillAllProcess("tcpdump")
}

func (sshh *Credentials) ZipAllLogs() bool {

        //log.Debug.Println(utils.GetFuncName())

	zip_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_logs_%s.zip", log.LogFilename, log.Log_date)
	pcap_files := fmt.Sprintf("/home/lavelle/file-server-files/%s_*_%s*.log", log.LogFilename, log.Log_date)
	return sshh.Zip(zip_filename, pcap_files)

}


func (sshh *Credentials) DownloadAllLogs() {

    zip_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_logs_%s.zip", log.LogFilename, log.Log_date)
    zip_filename_local := fmt.Sprintf("%s/%s_logs_%s_%s.zip", log.LVN_LOGPATH, log.LogFilename, log.Log_date, utils.GetCurrentDateTimeForFile())
    sshh.Download(zip_filename, zip_filename_local)
}

func (sshh *Credentials) DownloadAllPcaps() {

    zip_filename := fmt.Sprintf("/home/lavelle/file-server-files/%s_pcaps_%s.zip", log.LogFilename, log.Log_date)
    zip_filename_local := fmt.Sprintf("%s/%s_pcaps_%s_%s.zip", log.LVN_LOGPATH, log.LogFilename, log.Log_date, utils.GetCurrentDateTimeForFile())
    sshh.Download(zip_filename, zip_filename_local)

}

func (sshh *Credentials) GetWanIfaces() []string {
        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	//cmd := fmt.Sprintf("tcpdump -i %s -G %d -W 1 -w %s.pcap", t.Iface, t.interval, t.Write_to_file)
	//ifconfig | grep "^lan[0-9]"
	cmd := fmt.Sprintf(`ifconfig | grep "^wan[0-9]"`)
	//cmd := fmt.Sprintf("sh tcpdump.sh")
        op, err := sshh.ExecCommandFromHost(cmd, sudo)

	//fmt.Println(op)
        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return []string{}
        }

	op_list := strings.Split(strings.TrimSpace(op), "\n")
	iface_list := []string{}
	for _, op := range op_list {
		iface_list = append(iface_list, strings.TrimSpace(strings.Split(op, " ")[0]))
	}

        return iface_list
}

func (sshh *Credentials) GetLanIfaces() []string {
        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	//cmd := fmt.Sprintf("tcpdump -i %s -G %d -W 1 -w %s.pcap", t.Iface, t.interval, t.Write_to_file)
	//ifconfig | grep "^lan[0-9]"
	cmd := fmt.Sprintf(`ifconfig | grep "^lan[0-9]"`)
	//cmd := fmt.Sprintf("sh tcpdump.sh")
        op, err := sshh.ExecCommandFromHost(cmd, sudo)

	//fmt.Println(op)
        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return []string{}
        }

	op_list := strings.Split(strings.TrimSpace(op), "\n")
        iface_list := []string{}
	for _, op := range op_list {
		iface_list = append(iface_list, strings.TrimSpace(strings.Split(op, " ")[0]))
	}

        return iface_list
}

func (sshh *Credentials) TcpdumpOnCpe(lan_ifaces []string, wan_ifaces []string, Lndpi bool,  LanDp bool, Ips bool) {
	for _, lan_iface := range lan_ifaces {
		sshh.TcpdumpLanIface(lan_iface)
	}

	for _, wan_iface := range wan_ifaces {
		sshh.TcpdumpLanIface(wan_iface)
	}

	if Lndpi {
	   sshh.TcpdumpLndpi()
	}

	if LanDp {
	    sshh.TcpdumpLanDp()
	}

	if Ips {
	    sshh.TcpdumpIps()
	}
}

func (sshh *Credentials) CleanupCpeDebugFiles() {
	sshh.Remove("/home/lavelle/file-server-files/*.zip")
	sshh.Remove("/home/lavelle/file-server-files/*.log")
	sshh.Remove("/home/lavelle/file-server-files/*.pcap")
}

func (sshh *Credentials) StartDebugOnCpe(lan_ifaces []string, wan_ifaces []string, Lndpi bool,  LanDp bool, Ips bool) {
	/*
	1. Start pcaps
	2. Start tailf on log
	*/

	if len(lan_ifaces) == 0 {
		lan_ifaces = sshh.GetLanIfaces()
	}
	if len(wan_ifaces) == 0 {
		wan_ifaces = sshh.GetWanIfaces()
	}
	//fmt.Println(lan_ifaces, wan_ifaces)

	log.Debug.Printf("==============================================================\n")
	log.Debug.Printf("Collecting debug info on CPE %s\n", sshh.Host)
	sshh.KillAllTcpdump()
	sshh.CleanupCpeDebugFiles()
	sshh.GetCpeVersion()
	sshh.GetLavelleServiceStatus()
	sshh.ShowNamespaces()
	//sshh.AptgetInstall("zip")
	sshh.TailfLavelleLogs()
	sshh.TcpdumpOnCpe(lan_ifaces, wan_ifaces, Lndpi, LanDp, Ips)
	sshh.GetOvsDumpFlows()
	sshh.GetIptablesInfo()
	sshh.GetIptablesInfoFromNatNamespace()
	log.Debug.Printf("Completed Collecting debug info on CPE %s\n", sshh.Host)
	log.Debug.Printf("==============================================================\n")
}

func (sshh *Credentials) StartTcpdump(t *Tcpdump ) {

	//defer wg.Done()
	sshh.Remove(t.Write_to_file)
	sshh.Remove(fmt.Sprintf("%s.pcap", t.Write_to_file))
	go sshh.ExecTcpdump(t)
	time.Sleep(3 * time.Second)
}

func (sshh *Credentials) ReadPktContentsFromFileAfterTcpDump(t *Tcpdump) {

	if t.RedirectToFile {
		content, err := sshh.ExecCatFromHost(t.Write_to_file)
		if err == nil {
		     t.CapturedPkts, _ = utils.ConvertStringToPktList(content)
		}
	}
}

func (sshh *Credentials) StopTcpdump(t *Tcpdump, wg *sync.WaitGroup) (bool, error) {

	//pkts := []string{}
	/* defer stack dont change the order */
	defer wg.Done()

	op, err := sshh.GetProcessInfo("tcpdump")
	if err != nil {
		log.Debug.Printf("get process info: %s", err.Error())
		return false, err
	}

	re :=  regexp.MustCompile(fmt.Sprintf(`(\d+)\s+\d+.*%s`, t.cmd))
	pid_str := re.FindStringSubmatch(op)
	//log.Debug.Printf("... %s", re.String())

	if len(pid_str) > 0 && false {
       log.Debug.Println("len of pidsts: %d", len(pid_str))
       for i, x := range pid_str {
	       log.Debug.Printf("leng of x %d, x is %s pid+_str[%d]", len(x), x, i)
	       if len(x) >  0 {
		       for j, y := range x {
			       fmt.Printf("y = %s pid_str[%d][%s]", y, i, j)
		       }
	       }
       }
	}

        //log.Debug.Println("pid str ", pid_str, "pidstr[0][0]", pid_str[0][0], "pidstr[1]", pid_str[1])
	if len(pid_str) > 0 {
           var pid int
           if pid, err = strconv.Atoi(pid_str[1]); err != nil {
             //  log.Debug.Printf("strconv atoi pid_str: %s ", err.Error())
	       return false, err
           }

	   time.Sleep(10 * time.Second)
           op, err = sshh.KillProcessId(pid)
           if err != nil {
              log.Debug.Printf("killing process id %d : %s ", pid, err.Error())
	      return false, err
           }
	}

	time.Sleep(10 * time.Second)
	sshh.ReadPktContentsFromFileAfterTcpDump(t)
	return true, nil
}

func (sshh *Credentials) CollectDebugInfoFromCpe() {
	/*
	1. Collect pcaps
	2. Collect tailf log files
	*/

        log.Debug.Printf("Collecting debug info...\n")
	sshh.KillAllTailf()
	sshh.ZipAllLogs()
	sshh.DownloadAllLogs()

	sshh.KillAllTcpdump()
	sshh.KillAllProcessOnNamespace("ns-svc-lan", "tcpdump")
	sshh.KillAllProcessOnNamespace("ns-svc-natfw", "tcpdump")
	sshh.ZipAllPcaps()
	sshh.DownloadAllPcaps()
	sshh.GetOvsDumpFlows()
	sshh.GetIptablesInfo()
	sshh.GetIptablesInfoFromNatNamespace()
        log.Debug.Printf("Finished collecting debug info...\n")
}

func (sshh *Credentials) AppctlPathShow() (bool, error, string) {

        //log.Debug.Println(utils.GetFuncName())

        sudo := 1
	cmd := fmt.Sprintf("/home/lavelle/appctl path/show")
        op, err := sshh.ExecCommandFromHost(cmd, sudo)

        if err != nil {
                log.Debug.Printf("Received from execute err %s", err)
                return false, err, op
        }

        return true, nil, op
}

func (sshh *Credentials) IsPathsUp(src_ips []string, dest_ips []string) (bool, [][]string, [][]string) {

        //log.Debug.Println(utils.GetFuncName())

	no_of_paths := len(src_ips) * len(dest_ips)

	up_paths := make([][]string, 0, no_of_paths)
	down_paths := make([][]string, 0, no_of_paths)

	if no_of_paths == 0 {
		return false, up_paths, down_paths
	}

	res, _, op :=  sshh.AppctlPathShow()

	if !res {
		return res, up_paths, down_paths
	}

	//re := regexp.MustCompile(`(\d+)\s+\d+\s+(\d+\.\d+\.\d+\.\d+)\s+(\d+\.\d+\.\d+\.\d+)\s+[a-zA-Z0-9]+\s+0x[a-fA-F0-9]+\s+0x[a-fA-F0-9]+\s+[0-9]+\s+[0-9]+\s+0x[a-fA-F0-9]+\s+(0x[a-fA-F0-9]+)\s+\d+\s+\d+\s+\d+\s*$`)
	src_ip_pattern := strings.Join(src_ips, "|")
	dst_ip_pattern := strings.Join(dest_ips, "|")
	//pattern := fmt.Sprintf(`(\d+)\s+\d+\s+(%s)\s+(%s)\s+[a-zA-Z0-9]+\s+0x[a-fA-F0-9]+\s+0x[a-fA-F0-9]+\s+[0-9]+\s+[0-9]+\s+0x[a-fA-F0-9]+\s+(0x[a-fA-F0-9]+)\s+\d+\s+\d+\s+\d+\s*$`, src_ip_pattern, dst_ip_pattern)
	pattern := fmt.Sprintf(`([0-9]+).*(%s)\s+(%s).*(0x[a-fA-F0-9]+)\s+\d+\s+\d+\s+\d+\s*`, src_ip_pattern, dst_ip_pattern)
	//log.Debug.Println(pattern)
	//re := regexp.MustCompile(fmt.Sprintf(`(\d+)\s+\d+\s+(%s)\s+(%s)\s+[a-zA-Z0-9]+\s+0x[a-fA-F0-9]+\s+0x[a-fA-F0-9]+\s+[0-9]+\s+[0-9]+\s+0x[a-fA-F0-9]+\s+(0x[a-fA-F0-9]+)\s+\d+\s+\d+\s+\d+\s*$`, src_ip_pattern, dst_ip_pattern))
	re := regexp.MustCompile(pattern)
	matches := re.FindAllStringSubmatch(op, -1)
	//log.Debug.Printf(matches)


	for _, match := range matches {
		//path_id := match[1]
		src_ip  := match[2]
		dst_ip  := match[3]
		status  := match[4]
		log.Debug.Printf("src_ip %s, dst_ip %s, status %s\n", src_ip, dst_ip, status)
		if status == "0x0000" {
			up_paths = append(up_paths,  []string{src_ip, dst_ip})
		} else {
			down_paths = append(down_paths,  []string{src_ip, dst_ip})
		}
	}

	log.Debug.Printf("len(matches) %d, no_of_paths %d, len(down_paths) %d\n", len(matches), no_of_paths, len(down_paths))
	//log.Debug.Println(down_paths)
	if (len(matches) != no_of_paths) || (len(down_paths) > 0) {
		return false, up_paths, down_paths
		//"Few paths are not formed as expected..."
	}
        return true, up_paths, down_paths
}
