package lvnmail

import (
    "fmt"
    "log"
    "net"
    "strings"
    //"net/mail"
    "net/smtp"
    "crypto/tls"
)

// StartTLS Email Example
type email struct {
	Smtp_server string
	Smtp_port int
	From_addr string
	To_addr []string
	Subject string
	Message string
	ContentType string
	MimeVersion string
}

func NewEmail() (*email) {

	//to_addr := []string{"deepak.lal@lavellenetworks.com"}
	to_addr := []string{"deepak.lal@lavellenetworks.com"}
	var m email = email{
             Smtp_server: "smtp.gmail.com",
             Smtp_port: 587,
	     From_addr: "deepak.lal@lavellenetworks.com",
	     To_addr  : to_addr,
	     Subject  : "",
	     Message  : "",
	     ContentType: "",
	     MimeVersion: "",
	}
	return &m

}

func SendMail(m *email, password string) {

    //from := mail.Address{"", m.from_addr}
    //to   := mail.Address{"", "deepak.lal@lavellenetworks.com"}
    //from := m.From_addr

    //subj := m.Subject
    body := m.Message

    // Setup headers
    headers := make(map[string]string)
    headers["From"] = m.From_addr
    headers["To"] = strings.Join(m.To_addr, ",")
    headers["Subject"] = m.Subject

    if m.ContentType != "" {
        headers["Content-Type"] = m.ContentType
    }

    if m.MimeVersion != "" {
	    headers["MIME-version"] = m.MimeVersion
    }
    //mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"

     // Setup message
     message := ""
     for k,v := range headers {
         message += fmt.Sprintf("%s: %s\r\n", k, v)
     }
     message += "\r\n" + body

    //fmt.Println(message)

    // Connect to the SMTP Server
    servername := fmt.Sprintf("%s:%d", m.Smtp_server, m.Smtp_port)
    host, _, _ := net.SplitHostPort(servername)
    auth := smtp.PlainAuth("", m.From_addr, password, host)

     // TLS config
    tlsconfig := &tls.Config {
        InsecureSkipVerify: false,
        ServerName: host,
    }

    c, err := smtp.Dial(servername)
    if err != nil {
       log.Panic(err)
    }

    c.StartTLS(tlsconfig)

    // Auth
    if err = c.Auth(auth); err != nil {
                log.Panic(err)
    }

   // To && From
    if err = c.Mail(m.From_addr); err != nil {
        log.Panic(err)
    }

    for _, to_addr := range m.To_addr {
        if err = c.Rcpt(to_addr); err != nil {
             log.Panic(err)
        }
    }

    // Data
    w, err := c.Data()
    if err != nil {
         log.Panic(err)
    }
    _, err = w.Write([]byte(message))
    if err != nil {
        log.Panic(err)
     }

    err = w.Close()
    if err != nil {
        log.Panic(err)
    }
    c.Quit()
}

