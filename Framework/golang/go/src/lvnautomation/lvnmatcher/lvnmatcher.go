package lvnmatcher

import (
	"fmt"
	//"reflect"

	//"github.com/onsi/gomega/format"
	"github.com/onsi/gomega/types"
)

func LvnAutoMatcherObj(expected interface{}) types.GomegaMatcher {
    return &LvnAutoMatcher{
        Expected: expected,
    }
}

type LvnAutoMatcher struct {
	Expected interface{}
}

func (matcher *LvnAutoMatcher) Match(actual interface{}) (success bool, err error) {

    if actual == nil && matcher.Expected == nil {
                return false, fmt.Errorf("Refusing to compare <nil> to <nil>.\nBe explicit and use BeNil() instead.  This is to avoid mistakes where both sides of an assertion are erroneously uninitialized.")
        } else if matcher.Expected == nil {
                return false, fmt.Errorf("Refusing to compare type to <nil>.\nBe explicit and use BeNil() instead.  This is to avoid mistakes where both sides of an assertion are erroneously uninitialized.")
        } else if actual == nil {
                return false, nil
        }

        //actualType := reflect.TypeOf(actual)
        //expectedType := reflect.TypeOf(matcher.Expected)

        //return actualType.AssignableTo(expectedType), nil

	exp := fmt.Sprintf("%v", matcher.Expected)
	act := fmt.Sprintf("%v", actual)
	//fmt.Println(exp, act, (exp==act))
	return (exp == act), nil
}

func (matcher *LvnAutoMatcher) FailureMessage(actual interface{}) (message string) {
	//fmt.Println("Failure message..")
	return fmt.Sprintf("%v", actual)
}

func (matcher *LvnAutoMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	//fmt.Println("Negated Failure message..")
	//return format.Message(actual, fmt.Sprintf("not to panic, but panicked with\n%s", format.Object(matcher.object, 1)))
	return fmt.Sprintf("%v", actual)
}
