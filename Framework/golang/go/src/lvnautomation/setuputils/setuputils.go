package setuputils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	host_utils "lvnautomation/hostutils"
)

//to get device name

type dev struct {
	Name string `json:"name"`
}

//to get UN and PWD

type Userpwd struct {
	Username string `json:"username"`
	Password string `json:password"`
}

//hub information used in sites

type CPEInfo struct {
	Name    string   `json:"name"`
	Devices []dev    `json:"devices"`
	Hosts   []string `json:"hosts"`
}

type HubInfo struct {
	//Name       string   `json:"name"`
	//Devices    []dev    `json:"devices"`
	Dc_Dr_role string   `json:"dc-dr-role"`
	//Hosts      []string `json:"hosts"`
	CPEInfo
}

//spoke information used in sites

type SpokeInfo struct {
	//Name    string   `json:"name"`
	//Devices []dev    `json:"devices"`
	//Hosts   []string `json:"hosts"`
	CPEInfo
}

// host information used in customer

type Hostinfo struct {
	Name     string `json:"name"`
	Mgmt_ip  string `json:"mgmt_ip"`
	Ssh_UN   string `json:"ssh_username"`
	Ssh_pwd  string `json:"ssh_password"`
	Ssh_port string `json:"ssh_port"`
}

// device information used in devices

type Deviceinfo struct {
	Name           string `json:"name"`
	HA_procxy_role string `json:"ha-procxy-role"`
}

// interface map used in devices

type Ifacemap struct {
	Cpe        string `json:"cpe"`
	Host       string `json:"host"`
	Host_iface string `json:"host_iface"`
}

//main structure
type CustomerInfo struct {
			Name       string `json:"name"`
			Url         string `json:"url"`
			NetworkGroup string `json:"network_group"`
			Credentials struct {
				Sysadmin []Userpwd `json:"sysadmin"`
				Netadmin []Userpwd `json:"netadmin"`
				User     []Userpwd `json:"user"`
			} `json:"credentials"`

			Sites struct {
				Hub   []HubInfo `json:"hub"`
				Spoke struct {
					S1 SpokeInfo `json:"S1"`
					S2 SpokeInfo `json:"S2"`
				} `json:"spoke"`
			} `json:"sites"`
			Hosts   []Hostinfo `json:"hosts"`
			Devices []struct {
				Deviceinfo
				IfaceMap []Ifacemap `json:"ifacemap"`
			} `json:"devices"`
}

type Setup001 struct {
	Cloudstation struct {
		Base_url string  `json:"base_url"`
		Master   Userpwd `json:"master"`
		Customer []CustomerInfo `json:"customer"`
	} `json:"cloudstation"`
}


func (s *Setup001) GetCustomers() []CustomerInfo {
	return s.Cloudstation.Customer
}

func (s *Setup001) GetCustomer(index int) CustomerInfo {
	return s.Cloudstation.Customer[index]
}

func (s *Setup001) GetCustomerName(index int) string {
	return s.Cloudstation.Customer[index].Name
}

func (c *CustomerInfo) GetURL() string {
	return c.Url
}

func (c *CustomerInfo) NetworkGroupName() string {
	return c.NetworkGroup
}

func (c *CustomerInfo) GetNetAdminCredentials() []Userpwd {
	return c.Credentials.Netadmin
}

func (c *CustomerInfo) GetSysAdminCredentials() []Userpwd {
	return c.Credentials.Sysadmin
}

func (c *CustomerInfo) GetHubs() []HubInfo {
	return c.Sites.Hub
}

func (c *CustomerInfo) GetDCHub() (*HubInfo) {
	for _, hub := range c.GetHubs() {
            if hub.Dc_Dr_role == "dc" {
		    return &hub
	    }
	}
	return nil
}

func (c *CustomerInfo) GetDRHub() (*HubInfo) {
	for _, hub := range c.GetHubs() {
            if hub.Dc_Dr_role == "dr" {
		    return &hub
	    }
	}
	return nil
}

func (c *CustomerInfo) GetS1Spoke() SpokeInfo {
	return c.Sites.Spoke.S1
}

func (c *CustomerInfo) GetS2Spoke() SpokeInfo {
	return c.Sites.Spoke.S2
}

func (c *CustomerInfo) GetHosts() []Hostinfo {
	return c.Hosts
}

func (c *CustomerInfo) GetDevices() []Deviceinfo {
	var d []Deviceinfo
	for _, device := range c.Devices {
            d = append(d, device.Deviceinfo)
	}
	return d
}

func (c *CustomerInfo) GetDevicesAtDCHub() []Deviceinfo {
	var h []Deviceinfo
	dc_hub := c.GetDCHub()
	for _, device_info := range dc_hub.CPEInfo.Devices {
		for _, device := range c.GetDevices() {
			if device.Name == device_info.Name {
		           h = append(h, device)
			}
		}
	}
	return h
}

func (c *CustomerInfo) GetDevicesAtS1Spoke() []Deviceinfo {
	var h []Deviceinfo
	s1 := c.GetS1Spoke()
	for _, device_info := range s1.CPEInfo.Devices {
		for _, device := range c.GetDevices() {
			if device.Name == device_info.Name {
		           h = append(h, device)
			}
		}
	}
	return h
}

func (c *CustomerInfo) GetDevicesAtS2Spoke() []Deviceinfo {
	var h []Deviceinfo
	s2 := c.GetS2Spoke()
	for _, device_info := range s2.CPEInfo.Devices {
		for _, device := range c.GetDevices() {
			if device.Name == device_info.Name {
		           h = append(h, device)
			}
		}
	}
	return h
}

func (c *CustomerInfo) GetActiveDeviceAtS1Spoke() *Deviceinfo {
	var h Deviceinfo
	devices_at_s1 := c.GetDevicesAtS1Spoke()
	for _, device_info := range devices_at_s1 {
		if device_info.HA_procxy_role == "active" {
			h = device_info
		}
	}
	return &h
}

func (c *CustomerInfo) GetActiveDeviceAtS2Spoke() *Deviceinfo {
	var h Deviceinfo
	devices_at_s2 := c.GetDevicesAtS2Spoke()
	for _, device_info := range devices_at_s2 {
		if device_info.HA_procxy_role == "active" {
			h = device_info
		}
	}
	return &h
}

func (c *CustomerInfo) GetActiveDeviceAtDCHub() *Deviceinfo {
	var h Deviceinfo
	for _, device_at_dc_hub := range c.GetDevicesAtDCHub() {
		if device_at_dc_hub.HA_procxy_role == "active" {
			h = device_at_dc_hub
			break
		}
	}
	return &h
}

func (c *CustomerInfo) GetHostsAtDCHub() []Hostinfo {
	var h []Hostinfo
	dc_hub := c.GetDCHub()
	for _, hostname := range dc_hub.Hosts {
		for _, hosts := range c.GetHosts() {
			if hosts.Name == hostname {
		           h = append(h, hosts)
			   break
			}
		}
	}
	return h
}

func (c *CustomerInfo) GetHostsAtDRHub() []Hostinfo {
	var h []Hostinfo
	dr_hub := c.GetDRHub()
	for _, hostname := range dr_hub.Hosts {
		for _, hosts := range c.GetHosts() {
			if hosts.Name == hostname {
		           h = append(h, hosts)
			   break
			}
		}
	}
	return h
}

func (c *CustomerInfo) GetHostsAtS2Spoke() []Hostinfo {
	var h []Hostinfo
	for _, hostname := range c.Sites.Spoke.S2.Hosts {
		for _, hosts := range c.GetHosts() {
			if hosts.Name == hostname {
		           h = append(h, hosts)
			   break
			}
		}
	}
	return h
}

func (c *CustomerInfo) GetHostsAtS1Spoke() []Hostinfo {
	var h []Hostinfo
	for _, hostname := range c.Sites.Spoke.S1.Hosts {
		for _, hosts := range c.GetHosts() {
			if hosts.Name == hostname {
		           h = append(h, hosts)
			   break
			}
		}
	}
	return h
}

func (d *Deviceinfo) GetName() string {
	return d.Name
}

func (h *HubInfo) GetName() string {
	return h.Name
}

func (s *SpokeInfo) GetName() string {
    return s.Name
}

/*
func (s *Setup001) GetCustomerIndex(customer_name string) (bool, int) {

	var customer, customer_info CustomerInfo
	var idx int
	for idx, customer_info = range s.Cloudstation.Customer {
		if customer_info.Name == customer_name {
			customer = customer_info
			break
		}
	}

	if customer == nil {
		return false, 0
	}
	return true, idx
}

func (s *Setup001) GetHubInfo(customer_name string) (bool, []HubInfo){

	
	//var customer CustomerInfo
	//for _, customer_info := range s.Cloudstation.Customer {
	//	if customer_info.Name == customer_name {
	//		customer = customer_info
	//		break
	//	}
	//}

	//if customer == nil {
	//	return false, []HubInfo{}
	//}

	var customer CustomerInfo
	if res, idx := s.GetCustomerIndex(customer_name); res {
		customer = s.Cloudstation.Customer[idx]
	} else {
		return false, []HubInfo{}
	}


	hubs := customer.Sites.Hub
	return true, hubs
	//for _, hub := range hubs {
	//}
}

func (s *Setup001) GetDeviceMap(customer_index int) map[string]int {

    var devicemap map[string]int

    devices := s.Cloudstation.Customer[customer_index].Devices
    for i, d := range devices {
	    devicemap[d.Name] = i
    }
    return devicemap
}

func (s *Setup001) GetBackupCpe(c CPEInfo) (bool, string) {
	var cpename string
	var res bool
	for _, d := range c.Devices {
		//if d.HA_procxy_role == "backup" {
                 //  return true, d.Name
		//}
	}
	return false, cpename
}

func (s *Setup001) GetActiveCpe(customer_idx int, c CPEInfo) (bool, string) {
	var cpename string
	var res bool
	devicemap := s.GetDeviceMap(customer_idx)

	device_list := s.Cloudstation.Customer[customer_index].Devices

	for _, d := range c.Devices {

                devicemap[d.Name]
		//if d.HA_procxy_role == "active" {
                //   return true, d.Name
		//}
	}

	return false, cpename
}

func (s *Setup001) GetDRHubInfo(customer_name string) (bool, HubInfo) {
	return s.GetHubInfoBasedOnDCDRRole(customer_name, "dr")
}

func (s *Setup001) GetDCHubInfo(customer_name string) (bool, HubInfo) {
	return s.GetHubInfoBasedOnDCDRRole(customer_name, "dc")
}

func (s *Setup001) GetHubInfoBasedOnDCDRRole(customer_name string, dc_dr_role string) (bool, HubInfo) {
	var res bool
	var hubs []HubInfo
	var hub HubInfo

	if res, hubs = s.GetHubInfo(customer_name); !res {
		return false, hub
	}

	for _, hub = range hubs {
		if hub.Dc_Dr_role == dc_dr_role {
			break
		}
	}

	return true, hub
}

func (s *Setup001) GetHostIndexMap(customer_index) map[string]int {
	var hostmap map[string]int
        hosts := s.Cloudstation.CustomerInfo[customer_index].Hosts
	for i, h := range hosts {
          hostmap[h.Name] = i
	}
	return hostmap
}

func (s *Setup001) GetHubSideInfo(customer_name) {

	var res bool
	var hubinfo HubInfo
	var site_name, cpe_name string
	var customer_idx int

        if res, customer_idx = s.GetCustomerIndex(customer_name); res {
                customer = s.Cloudstation.Customer[customer_idx]
        }

	if res, hubinfo := s.GetDCHubInfo(customer_name); !res {

	}

	site_name = hubinfo.Name
        res, cpe_name = s.GetActiveCpe(customer_idx, hubinfo.CPEInfo)
	if !res {
	}


	host_map := s.GetHostIndexMap(customer_idx)
	host_cred := []*host_utils.Credentials

	for _, h := range  hubinfo.Hosts {
	    host_cred = append( host_cred, s.GetHost(customer_idx, host_map[h.Name]))
	}
	return site_name, cpe_name, host_cred

}
*/

func (s *Setup001) GetHost(customer_index int, host_index int) (*host_utils.Credentials) {

	//n  :=  s.Cloudstation.Customer[customer_index].Hosts[host_index].Name
	ip  := s.Cloudstation.Customer[customer_index].Hosts[host_index].Mgmt_ip
	un  := s.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_UN
	pwd := s.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_pwd
	port := s.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_port
        fmt.Println(ip, un, pwd, port)

	h := host_utils.NewHost(ip, port, un, pwd)
	return h
	//return n, ip, port, un, pwd
}

func NewSetup001(setup_file string) (bool, *Setup001) {
	var setup Setup001
	file, _ := ioutil.ReadFile(setup_file)
	json.Unmarshal([]byte(file), &setup)
	return true, &setup
}

//fetching the data from json file to the structure
func Setup(setup_file string) {

	var ex Setup001
	//set path for the json file
	file, _ := ioutil.ReadFile(setup_file)

	//fetching data from json file
	json.Unmarshal([]byte(file), &ex)

	//printing statement
	fmt.Println("Base_url---", ex.Cloudstation.Base_url)
	fmt.Println("Master.Username---: ", ex.Cloudstation.Master.Username)
	fmt.Println("Master.Password---", ex.Cloudstation.Master.Password)

	//looping the number of customers
	for i := 0; i < len(ex.Cloudstation.Customer); i++ {
		fmt.Println("Customer.Url---", ex.Cloudstation.Customer[i].Url)

		//for each customer looping number of sysadmins
		for j := 0; j < len(ex.Cloudstation.Customer[i].Credentials.Sysadmin); j++ {
			fmt.Println("Customer.Credentials.Sysadmin.Username---", ex.Cloudstation.Customer[i].Credentials.Sysadmin[j].Username)
			fmt.Println("Customer.Credentials.Sysadmin.Password---", ex.Cloudstation.Customer[i].Credentials.Sysadmin[j].Password)
		}

		//for each customer looping number of netadmins
		for k := 0; k < len(ex.Cloudstation.Customer[i].Credentials.Netadmin); k++ {
			fmt.Println("customer.credentials.Netadmin.Username---", ex.Cloudstation.Customer[i].Credentials.Netadmin[k].Username)
			fmt.Println("customer.credentials.Netadmin.Password---", ex.Cloudstation.Customer[i].Credentials.Netadmin[k].Password)
		}

		//for each customer looping number of user
		for l := 0; l < len(ex.Cloudstation.Customer[i].Credentials.User); l++ {
			fmt.Println("customer.credentials.User.Username---", ex.Cloudstation.Customer[i].Credentials.User[l].Username)
			fmt.Println("customer.credentials.User.Password---", ex.Cloudstation.Customer[i].Credentials.User[l].Password)
		}

		//for each customer looping number of hubs
		for m := 0; m < len(ex.Cloudstation.Customer[i].Sites.Hub); m++ {
			fmt.Println("customer.sites.Hub.Name---", ex.Cloudstation.Customer[i].Sites.Hub[m].Name)
			fmt.Println("customer.sites.Hub.Devices---", ex.Cloudstation.Customer[i].Sites.Hub[m].Devices)
			fmt.Println("customer.sites.Hub.Dc_Dr_role---", ex.Cloudstation.Customer[i].Sites.Hub[m].Dc_Dr_role)
			fmt.Println("customer.sites.Hub.Hosts---", ex.Cloudstation.Customer[i].Sites.Hub[m].Hosts)
		}

		//printing the spoke statements not looping because it is present in the customer
		fmt.Println("customer.sites.spoke.S1.Name---", ex.Cloudstation.Customer[i].Sites.Spoke.S1.Name)
		fmt.Println("customer.sites.spoke.S1.Devices---", ex.Cloudstation.Customer[i].Sites.Spoke.S1.Devices)
		fmt.Println("customer.sites.spoke.S1.Hosts---", ex.Cloudstation.Customer[i].Sites.Spoke.S1.Hosts)
		fmt.Println("customer.sites.spoke.S2.Name---", ex.Cloudstation.Customer[i].Sites.Spoke.S2.Name)
		fmt.Println("customer.sites.spoke.S2.Devices---", ex.Cloudstation.Customer[i].Sites.Spoke.S2.Devices)
		fmt.Println("customer.sites.spoke.S2.Hosts---", ex.Cloudstation.Customer[i].Sites.Spoke.S2.Hosts)

		//for each customer looping number of hosts
		for n := 0; n < len(ex.Cloudstation.Customer[i].Hosts); n++ {
			fmt.Println("customer.Hosts.Name---", ex.Cloudstation.Customer[i].Hosts[n].Name)
			fmt.Println("customer.Hosts.Mgmt_ip---", ex.Cloudstation.Customer[i].Hosts[n].Mgmt_ip)
			fmt.Println("customer.Hosts.Ssh_UN---", ex.Cloudstation.Customer[i].Hosts[n].Ssh_UN)
			fmt.Println("customer.Hosts.Ssh_pwd---", ex.Cloudstation.Customer[i].Hosts[n].Ssh_pwd)
			fmt.Println("customer.Hosts.Ssh_port---", ex.Cloudstation.Customer[i].Hosts[n].Ssh_port)
		}

		//for each customer looping number of devices
		for p := 0; p < len(ex.Cloudstation.Customer[i].Devices); p++ {
			fmt.Println("customer.devices.Device.Name---", ex.Cloudstation.Customer[i].Devices[p].Name)
			fmt.Println("customer.devices.Device.HA_procxy_role---", ex.Cloudstation.Customer[i].Devices[p].HA_procxy_role)

			//for each devices ifacemap
			for q := 0; q < len(ex.Cloudstation.Customer[i].Devices[p].IfaceMap); q++ {
				fmt.Println("customer.devices.Device.Ifacemap.Cpe---", ex.Cloudstation.Customer[i].Devices[p].IfaceMap[q].Cpe)
				fmt.Println("customer.devices.Device.Ifacemap.Host---", ex.Cloudstation.Customer[i].Devices[p].IfaceMap[q].Host)
				fmt.Println("customer.devices.Ifacemap.Host_iface---", ex.Cloudstation.Customer[i].Devices[p].IfaceMap[q].Host_iface)
			}
		}
	}

	//printing the number of customers,hubs,spoke,dr-dc,ha-procxy
	var y int

	//set first customer as 0
	z := 0

	//printing the number of customer
	Customer_count := Count(ex)
	fmt.Println("number of Customer present : ", Customer_count)

	//to get the length of the customer (customer id)
	for x := 0; x < len(ex.Cloudstation.Customer); x++ {
		Sys_count := Count_Sys(ex, x)
		Net_count := Count_Net(ex, x)
		User_count := Count_User(ex, x)
		fmt.Println("number of sysadmin for customer", x, ":", Sys_count)
		fmt.Println("number of netadmin for customer", x, ":", Net_count)
		fmt.Println("number of user for customer", x, ":", User_count)

		//check the hub wheather dr or dc(fetching the data from ex main structure,x is to create the hub that many number of customer,c number of hubs)
		for c := 0; c < len(ex.Cloudstation.Customer[x].Sites.Hub); c++ {
			Drdc := Dr_Dc(ex, x, c)
			fmt.Println("the hub is:", Drdc)
		}

		//check the device is haprocxy=active or backup(ex,x,d number of ha procxy)
		for d := 0; d < len(ex.Cloudstation.Customer[x].Devices); d++ {
			HA := HA_procxy(ex, x, d)
			fmt.Println("The Ha-procxy-role:", HA)
		}

	}

	//user input
	fmt.Println("Enter the Customer_ID")
	fmt.Scanln(&y)

	//checking condition wheather it is less customer value
	if y >= len(ex.Cloudstation.Customer) {
		fmt.Println("Invalid Input")
		z = 1
	}

	var res1, res2, res3 bool
	if z == 0 {

		//y is here customer id and ex is the variable to fetch the main structure
		res1 = Hub_Present(y, ex)
		res2 = Spoke1_Present(y, ex)
		res3 = Spoke2_Present(y, ex)
	}

	//printing the statements
	if res1 == true {
		fmt.Println("Hub is present")
	} else {
		fmt.Println("Hub is not present")
	}

	if res2 == true {
		fmt.Println("Spoke S1 is present")
	} else {
		fmt.Println("Spoke S1 is not present")
	}

	if res3 == true {
		fmt.Println("Spoke S2 is present")
	} else {
		fmt.Println("Spoke S2 is not present")
	}

}

//to count number of customer
func Count(ex Setup001) int {
	return (len(ex.Cloudstation.Customer))
}

//counting of number of sys,net,user are there in each customer
func Count_Sys(ex Setup001, x int) int {
	return (len(ex.Cloudstation.Customer[x].Credentials.Sysadmin))
}

func Count_Net(ex Setup001, x int) int {
	return (len(ex.Cloudstation.Customer[x].Credentials.Netadmin))
}

func Count_User(ex Setup001, x int) int {
	return (len(ex.Cloudstation.Customer[x].Credentials.User))
}

//returning hub present or not by checking hub count is zero or not
func Hub_Present(y int, ex Setup001) bool {
	Hub_count := len(ex.Cloudstation.Customer[y].Sites.Hub)
	if Hub_count == 0 {
		return false
	} else {
		return true
	}

}

//returning spoke present or not by checking the Spoke1 name present or not
func Spoke1_Present(y int, ex Setup001) bool {

	if ex.Cloudstation.Customer[y].Sites.Spoke.S1.Name == "" {
		return false
	} else {
		return true
	}
}
func Spoke2_Present(y int, ex Setup001) bool {

	if ex.Cloudstation.Customer[y].Sites.Spoke.S2.Name == "" {
		return false
	} else {
		return true
	}

}

//returning Dc-Dr value
func Dr_Dc(ex Setup001, x int, c int) string {

	return ex.Cloudstation.Customer[x].Sites.Hub[c].Dc_Dr_role
}

//returning the HA_procxy value
func HA_procxy(ex Setup001, x int, d int) string {

	return ex.Cloudstation.Customer[x].Devices[d].HA_procxy_role
}
