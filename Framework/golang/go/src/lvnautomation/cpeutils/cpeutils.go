package cpe_utils

import (
	"fmt"
	lvn_log "lvnautomation/lvnlog"
	utils "lvnautomation/utils"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"
)

//utils "lvn_automation/utils"

type GjefParam struct {
	CsUrl            string
	NetAdminId       string
	NetAdminPassword string
	typeOfEntity     string
	Command          string
	CpeList          []string
	Delay            int
	TimeOut          int
}
type GjefOutput struct {
	UUID   string
	Stdout string
	Stderr string
}

func (GjefInfo *GjefParam) Work(cmd []string) ([]string, error) {

	//lvn_log.Debug.Printf("Enter in %s with cs_url: %s, net_admin_id: %s, password: %s, etype: %s, cpe_list: %s, cmd_list: %s, delay: %d, timeout: %d", utils.GetFuncName(), cs_url, net_admin, psswd, etype, cpe_names, cmd, delay, tout)
	//outCreate,errCreate :=createCpsFile()
	var empty []string
	GjefInfo.Command = makeCmd(cmd)
	lvn_log.Debug.Printf("Recieving %s", GjefInfo.Command)
	outGjef, errGjef := GjefInfo.makeRequest()
	lvn_log.Err.Println(errGjef)
	//fmt.Print(outGjef, errGjef)
	JobID := getJobID(outGjef)
	lvn_log.Debug.Printf("Recieved value from %s : %s", utils.GetFuncName(), JobID)
	//fmt.Println(JobID)
	outResult, errResult := GjefInfo.getReply(JobID)
	//fmt.Println(outResult, errResult)
	fmt.Printf("%q", outResult)
	return empty, errResult
}
func NewGjef() (bool, *GjefParam) {
	var x GjefParam
	x.CsUrl = ""
	x.NetAdminId = ""
	x.NetAdminPassword = ""
	x.Command = ""
	x.Delay = 10
	x.TimeOut = 5
	x.CpeList = []string{""}
	x.SetTypeOfEntity("cloudport")
	return true, &x
}
func (GjefInfo *GjefParam) Read(csurl string, netid string, psswd string, cpe_names []string, etype string, delay int, tout int) bool {
	GjefInfo.CsUrl = csurl
	GjefInfo.NetAdminId = netid
	GjefInfo.NetAdminPassword = psswd
	GjefInfo.CpeList = cpe_names
	str := checkEtype(etype)
	if str == "" {
		return false
	}
	GjefInfo.SetTypeOfEntity(etype)
	GjefInfo.Delay = delay
	GjefInfo.TimeOut = tout
	return true
}
func (GjefInfo *GjefParam) SetTypeOfEntity(etype string) {
	GjefInfo.typeOfEntity = etype
}
func (GjefInfo *GjefParam) GetTypeOfEntity() string {
	return GjefInfo.typeOfEntity
}
func getJobID(outGjef string) string {
	lvn_log.Debug.Printf("Entering %s with Paramter as %s", utils.GetFuncName(), outGjef)
	re := regexp.MustCompile(`Job ID: [0-9]+`)
	JobExp := re.Find([]byte(outGjef))
	jobExpList := strings.Split(string(JobExp), " ")
	lvn_log.Debug.Printf("Returing from %s with value: %s", utils.GetFuncName(), jobExpList[2])
	return jobExpList[2]
}
func isSuccess(outGjefProgress string, prevOut string) bool {
	//lvn_log.Debug.Printf("Entering %s with Paramter as %s", utils.GetFuncName(), outGjefProgress)
	//res := (strings.Contains(outGjefProgress, "Scheduled") || strings.Contains(outGjefProgress, "Unknown"))
	//lvn_log.Debug.Printf("Returing from %s with value: %t", utils.GetFuncName(), res)
	if outGjefProgress != prevOut {
		lvn_log.Debug.Println(outGjefProgress)
	}

	res1 := strings.Contains(outGjefProgress, "Successfull") && !strings.Contains(outGjefProgress, "Unknown") && !strings.Contains(outGjefProgress, "Scheduled")
	return res1
}
func status(outGjefProgress string) string {
	re := regexp.MustCompile(`(Progress [\s\S]*)`)
	jobStatus := re.Find([]byte(outGjefProgress))
	return string(jobStatus)
}
func getOutput(outGjefShow string) []GjefOutput {
	re := regexp.MustCompile(`(UUID[\s\S\w\W]+?)stdout output\s-------------\s([\s\S\w\W]+?)stderr output\n-------------\n([\s\W\S\w]*?[^=]+)`)
	JobExp := re.FindAllSubmatch([]byte(outGjefShow), -1)
	var Out []GjefOutput
	for i, _ := range JobExp {
		OutSingle := GjefOutput{
			UUID:   string(JobExp[i][1]),
			Stdout: string(JobExp[i][2]),
			Stderr: string(JobExp[i][3]),
		}
		Out = append(Out, OutSingle)
	}
	//fmt.Println(Out)
	return Out
}

func execute(name string, args []string) (string, error) {
	lvn_log.Debug.Printf("Entering %s with parameter as name: %s, args: %s", utils.GetFuncName(), name, args)
	cmd := exec.Command(name, "")
	cmd.Args = args
	out, err2 := cmd.CombinedOutput()
	if err2 != nil {
		lvn_log.Err.Println(string(out), err2)
		return string(out), err2
	}
	lvn_log.Debug.Printf("Return from %s with value: %s", utils.GetFuncName(), string(out))
	return string(out), nil
}

/*
func (GjefInfo *GjefParam) createCpsFile() error {
	var CpeSerialNumberList []string
	for _, v := range GjefInfo.CpeList {
		cpe_serial_num, err := GetCpeSerialNumber(v)
		if err != nil {
			fmt.Println("Unable to fetch cpe serial number")
			return err
		}
		CpeSerialNumberList = append(CpeSerialNumberList)
	}
	cpe_serial_string = strings.Join(cpe_serial_num, "\n")
	form_cmd := fmt.Sprintf("echo %s >> /tmp/cps", cpe_serial_num)
	make_args = append(make_args, "/bin/sh", "-c", form_cmd)
	out_make, err_make := execute("/bin/sh", make_args)
	if err_make != nil {
		return err_make
	}
	return err_make
}
*/
func checkEtype(etype string) string {

	str := strings.ToLower(etype)
	if strings.Compare(str, "cloudport") == 0 {
		return str
	} else if strings.Compare(str, "customer") == 0 {
		return str
	} else {
		lvn_log.Err.Println("Invalid Entity type [cloudport/customer]")
		//fmt.Println("Invalid Entity type [cloudport/customer]")
		return ""
	}

}
func makeCmd(cmd []string) string {
	lvn_log.Debug.Printf("Entered %s with input %s", utils.GetFuncName(), cmd)
	length := len(cmd)
	command := cmd[0]
	if length == 1 {
		lvn_log.Debug.Printf("Returning from  %s with return_value %s", utils.GetFuncName(), command)
		return command
	} else {
		for i := 1; i < length; i++ {
			command = command + ";" + cmd[i]
		}
	}
	lvn_log.Debug.Printf("Returning from  %s with return_value %s", utils.GetFuncName(), command)
	return command
}

func checkforfailure(outGjefProgress string) bool {
	return strings.Contains(outGjefProgress, "Failed") && !strings.Contains(outGjefProgress, "Scheduled") && !strings.Contains(outGjefProgress, "Unknown")
}
func (GjefInfo *GjefParam) makeRequest() (string, error) {
	var makeGjef []string
	var empty string
	makeGjef = append(makeGjef, "python", "run_job.py", "-u", GjefInfo.CsUrl, "-U", GjefInfo.NetAdminId, "-P", GjefInfo.NetAdminPassword, "-t", GjefInfo.GetTypeOfEntity(), "-f", "/tmp/cps", "-i", GjefInfo.Command)
	lvn_log.Debug.Printf("Request Command: %s", makeGjef)
	//fmt.Println(makeGjef)
	outGjef, errGjef := execute("python", makeGjef)
	if errGjef != nil {
		lvn_log.Err.Println(outGjef, errGjef)
		//fmt.Println(outGjef)
		return empty, errGjef
	}
	return outGjef, errGjef
}

func (GjefInfo *GjefParam) getReply(JobID string) ([]GjefOutput, error) {
	var empty []GjefOutput
	lvn_log.Debug.Printf("Enter %s with JobId: %s and returning values in [][][]byte, error format", utils.GetFuncName, JobID)

	var makeGjefShow []string
	var makeGjefProgress []string
	makeGjefProgress = append(makeGjefProgress, "python", "run_job.py", "-u", GjefInfo.CsUrl, "-U", GjefInfo.NetAdminId, "-P", GjefInfo.NetAdminPassword, "-a", "progress", "-j", JobID)
	//----------------------------------------------------------------------------------------
	//if GjefInfo.TimeOut < GjefInfo.Delay {
	//	GjefInfo.TimeOut, GjefInfo.Delay = GjefInfo.Delay, GjefInfo.TimeOut
	//}
	lvn_log.Debug.Printf("Forming Progress Command: %s", makeGjefProgress)
	prevOut := ""
	start := time.Now()
	for true {
		outGjefProgress, errGjefProgress := execute("python", makeGjefProgress)
		if errGjefProgress != nil {
			lvn_log.Err.Println("Error in execute() \n%s inside getReply() while executing PROGRESS", errGjefProgress)
			return empty, errGjefProgress
		}
		lvn_log.Debug.Println(status(outGjefProgress))
		//		fmt.Println(status(outGjefProgress))
		if isSuccess(outGjefProgress, prevOut) {
			lvn_log.Debug.Println("Success", "Breaking the loop")
			break
		}
		if checkforfailure(outGjefProgress) {
			lvn_log.Debug.Println("Request Failed", "Breaking the loop")
			break
		}
		prevOut = outGjefProgress
		x := strconv.Itoa(GjefInfo.TimeOut)
		x = x + "m"
		duration, _ := time.ParseDuration(x)
		//fmt.Println(x, duration, time.Since(start))
		if time.Since(start) > duration {
			//			fmt.Println("timeout")
			lvn_log.Err.Println("Timeout occured")
			return empty, nil
		}
		lvn_log.Info.Printf("Delaying loop by %d seconds\n", GjefInfo.Delay)
		time.Sleep(time.Duration(GjefInfo.Delay) * 1000 * time.Millisecond)
	}
	//----------------------------------------------------------------------------------------
	makeGjefShow = append(makeGjefShow, "python", "run_job.py", "-u", GjefInfo.CsUrl, "-U", GjefInfo.NetAdminId, "-P", GjefInfo.NetAdminPassword, "-a", "show", "-j", JobID)
	lvn_log.Debug.Printf("Forming Show Command: %s", makeGjefShow)
	outGjefShow, errGjefShow := execute("python", makeGjefShow)
	if errGjefShow != nil {
		//fmt.Println(outGjefShow)
		lvn_log.Err.Println("Error in execute() \n%s inside getReply() while executing SHOW", outGjefShow, errGjefShow)
		return empty, errGjefShow
	}
	extractOutput := getOutput(outGjefShow)

	var removeArgs []string
	removeArgs = append(removeArgs, "rm", "-f", "/tmp/cps")
	lvn_log.Debug.Println("Deleting /tmp/cps")
	_, errRemove := execute("rm", removeArgs)
	if errRemove != nil {
		lvn_log.Err.Println("Unable to delete /tmp/cps ", errRemove)
		//fmt.Println("Unable to delete /tmp/cps", errRemove)
	}
	lvn_log.Debug.Printf("Returning from %s", utils.GetFuncName())
	return extractOutput, errGjefShow
}

type OVSTable struct {
	cookie   string
	duration float64
	table    int
	nPackets int
	nBytes   int
	idleAge  int
	hardAge  int
	priority int
	flowDir  int
	srcNwgrp int
	dstNwgrp int
	appCat   string
	appId    int
	ip       string
	nwSrc    string
	nwDst    string
	actions  string
}

func (GjefInfo *GjefParam) GetOvsTableInfo(cmd []string) ([][]OVSTable, []string, error) {
	lvn_log.Debug.Printf("Enter %s with parameter %s", utils.GetFuncName(), cmd)
	var ovsTableResultTotal [][]OVSTable
	var errorList []string
	//outCreate,errCreate :=createCpsFile()
	if len(cmd) == 0 {
		cmd = append(cmd, "cd /home/lavelle", "./ofctl dump-flows br0")
		GjefInfo.Command = makeCmd(cmd)
	} else {
		cmd = append([]string{"cd /home/lavelle"}, cmd...)
		alpha := []string{"cd /home/lavelle"}
		for i := 1; i < len(cmd); i++ {
			alpha = append(alpha, fmt.Sprintf("./ofctl dump-flows br0 table=%s", cmd[i]))
		}
		GjefInfo.Command = makeCmd(alpha)
	}
	lvn_log.Debug.Printf("Recieving %s", GjefInfo.Command)
	outGjef, errGjef := GjefInfo.makeRequest()
	if errGjef != nil {
		lvn_log.Err.Println("Error from makeRequest()", errGjef)
	}

	JobID := getJobID(outGjef)
	lvn_log.Debug.Printf("Job ID: %s", JobID)

	outResult, errResult := GjefInfo.getReply(JobID)
	if errResult != nil {
		lvn_log.Err.Println("Error:", errResult)
		return ovsTableResultTotal, errorList, errResult
	}
	for k := 0; k < len(outResult); k++ {
		str := strings.TrimSpace(outResult[k].Stderr)
		if strings.Compare(str, "") == 0 {
			errorList = append(errorList, fmt.Sprintf("Success : %s", outResult[k].UUID))
			lvn_log.Info.Printf("Success : %s", outResult[k].UUID)

			re := regexp.MustCompile(`(cookie=[\w]+)..(duration=\d+.\d+)...(table=\d)..(n_packets=\d+)..(n_bytes=\d+)..(idle_age=\d+)..(hard_age=\d+)?.?.?(priority=\d+),(flow_dir=\d+)?,?(src_nwgrp=\d+)?,?(dst_nwgrp=\d+)?,?(app_cat=[\d\/x]+)?,?(app_id=\d+)?,?(ip)?,?(nw_src=[\d\.\/]+)?,?(nw_dst=[\d\.\/]+)? (actions=[\w:\)\(,]+)`)
			res := re.FindAllSubmatch([]byte(outResult[k].Stdout), -1)

			var ovsTableResult []OVSTable

			for i := 0; i < len(res); i++ {

				var x []string
				for j := 1; j < len(res[i]); j++ {

					a := strings.Split(string(res[i][j]), "=")
					if len(a) == 2 {
						x = append(x, a[1])
					} else {
						x = append(x, a[0])
					}
				}

				dur, _ := strconv.ParseFloat(x[1], 64)
				tab, _ := strconv.Atoi(x[2])
				np, _ := strconv.Atoi(x[3])
				nb, _ := strconv.Atoi(x[4])
				iage, _ := strconv.Atoi(x[5])
				hage, _ := strconv.Atoi(x[6])
				prio, _ := strconv.Atoi(x[7])
				fd, _ := strconv.Atoi(x[8])
				srcnw, _ := strconv.Atoi(x[9])
				dstnw, _ := strconv.Atoi(x[10])
				aid, _ := strconv.Atoi(x[12])
				y := OVSTable{
					cookie:   x[0],
					duration: dur,
					table:    tab,
					nPackets: np,
					nBytes:   nb,
					idleAge:  iage,
					hardAge:  hage,
					priority: prio,
					flowDir:  fd,
					srcNwgrp: srcnw,
					dstNwgrp: dstnw,
					appCat:   x[11],
					appId:    aid,
					ip:       x[13],
					nwSrc:    x[14],
					nwDst:    x[15],
					actions:  x[16],
				}
				ovsTableResult = append(ovsTableResult, y)

			}
			ovsTableResultTotal = append(ovsTableResultTotal, ovsTableResult)
		} else {
			errorList = append(errorList, fmt.Sprintf("%s:\t%s", outResult[k].UUID, outResult[k].Stderr))
			lvn_log.Err.Printf("Failed : %s", outResult[k].UUID)
		}

	}
	lvn_log.Debug.Println("Returning from ", utils.GetFuncName(), "with values", ovsTableResultTotal, errorList, errResult)
	lvn_log.Info.Println("Success")
	return ovsTableResultTotal, errorList, errResult
}

func (GjefInfo *GjefParam) WgetFromLan(wget_link string, tout ...int) ([]string, []string, error) {
	lvn_log.Debug.Printf("Enter %s with parameter %s, ", utils.GetFuncName(), wget_link)

	var errorList []string
	//outCreate,errCreate :=createCpsFile()
	var cmd []string
	if len(tout) == 0 {
		cmd = append(cmd, "ip netns exec ns-svc-lan", fmt.Sprintf("wget --timeout=%d -t 2 -P /home/lavelle/file-server-files %s", GjefInfo.TimeOut, wget_link))
	} else {
		cmd = append(cmd, "ip netns exec ns-svc-lan", fmt.Sprintf("wget --timeout=%d -t 2 -P /home/lavelle/file-server-files %s", tout[0], wget_link))
	}
	GjefInfo.Command = makeCmd(cmd)
	lvn_log.Debug.Printf("Recieving %s", GjefInfo.Command)
	outGjef, errGjef := GjefInfo.makeRequest()
	if errGjef != nil {
		lvn_log.Err.Println("Error from makeRequest()", errGjef)
	}

	JobID := getJobID(outGjef)
	lvn_log.Debug.Printf("Job ID: %s", JobID)
	var result []string
	outResult, errResult := GjefInfo.getReply(JobID)
	for i := range outResult {
		errorList = append(errorList, outResult[i].Stderr)
		re := regexp.MustCompile(`HTTP request sent, awaiting response....(\d{3})`)
		res := re.FindAllSubmatch([]byte(outResult[i].Stderr), -1)
		result = append(result, string(res[len(res)-1][len(res[0])-1]))
		if string(res[len(res)-1][len(res[0])-1]) == "200" {
			lvn_log.Debug.Printf("Status Code: %s", string(res[len(res)-1][len(res[0])-1]))
		}
	}
	lvn_log.Debug.Printf("Return from %s with valuse %s\n%s\n%s", utils.GetFuncName(), result, errorList, errResult)
	for j := range result {
		if result[j] == "200" {
			lvn_log.Debug.Printf("Success for %s", outResult[j].UUID)
		}
		if strings.Contains(outResult[j].Stderr, "Giving up") {
			lvn_log.Err.Printf("Failure for %s", outResult[j].UUID)
		} else if strings.Contains(outResult[j].Stderr, "unable to resolve host address") {
			lvn_log.Err.Printf("Unable to resolve host for %s", outResult[j].UUID)
		}
	}
	//return empty, errorList, errResult
	return result, errorList, errResult
}

type IfconfigOutput struct {
	IpAddress        string
	Netmask          string
	BroadcastAddress string
}

func (GjefInfo *GjefParam) Ifconfig(ifaceName string) ([][]IfconfigOutput, []string, error) {
	lvn_log.Debug.Printf("Enter %s with parameter %s, ", utils.GetFuncName(), ifaceName)
	var IfconfigOutputResult [][]IfconfigOutput
	var errorList []string
	//outCreate,errCreate :=createCpsFile()
	var cmd []string
	cmd = append(cmd, fmt.Sprintf("ifconfig %s", ifaceName))
	GjefInfo.Command = makeCmd(cmd)
	lvn_log.Debug.Printf("Recieving %s", GjefInfo.Command)
	outGjef, errGjef := GjefInfo.makeRequest()
	if errGjef != nil {
		lvn_log.Err.Println("Error from makeRequest()", errGjef)
	}

	JobID := getJobID(outGjef)
	lvn_log.Debug.Printf("Job ID: %s", JobID)
	outResult, errResult := GjefInfo.getReply(JobID)
	for k, _ := range outResult {
		str := strings.TrimSpace(outResult[k].Stderr)
		if strings.Compare(str, "") == 0 {
			errorList = append(errorList, fmt.Sprintf("Success : %s", outResult[k].UUID))
			lvn_log.Info.Printf("Success : %s", outResult[k].UUID)

			re := regexp.MustCompile(`inet addr:([\d\.]*)..Bcast:([\d\.]*)..Mask:([\d\.]*)`)
			res := re.FindAllSubmatch([]byte(outResult[k].Stdout), -1)

			var ifconfdum []IfconfigOutput

			for i := 0; i < len(res); i++ {
				y := IfconfigOutput{
					IpAddress:        string(res[i][1]),
					BroadcastAddress: string(res[i][2]),
					Netmask:          string(res[i][3]),
				}
				ifconfdum = append(ifconfdum, y)

			}
			IfconfigOutputResult = append(IfconfigOutputResult, ifconfdum)
		} else {
			errorList = append(errorList, fmt.Sprintf("%s:\t%s", outResult[k].UUID, outResult[k].Stderr))
			lvn_log.Err.Printf("Failed : %s", outResult[k].UUID)
		}

	}
	lvn_log.Debug.Println("Returning from ", utils.GetFuncName(), "with values", IfconfigOutputResult, errorList, errResult)
	return IfconfigOutputResult, errorList, errResult
}

func (GjefInfo *GjefParam) TcpDump(ifaceName string, filename string, interval int) error {

	lvn_log.Debug.Printf("Enter %s with parameter %s, ", utils.GetFuncName(), ifaceName)
	//var IfconfigOutputResult [][]IfconfigOutput
	var errorList []string
	//outCreate,errCreate :=createCpsFile()
	var cmd []string
	cmd = append(cmd, fmt.Sprintf("tcpdump -i %s -G %d -W 1 -w /home/lavelle/file-server-files/%s.pcap", ifaceName, interval, filename))
	//fmt.Println(cmd)
	GjefInfo.Command = makeCmd(cmd)
	lvn_log.Debug.Printf("Recieving %s", GjefInfo.Command)
	outGjef, errGjef := GjefInfo.makeRequest()
	if errGjef != nil {
		lvn_log.Err.Println("Error from makeRequest()", errGjef)
	}

	JobID := getJobID(outGjef)
	lvn_log.Debug.Printf("Job ID: %s", JobID)
	outResult, errResult := GjefInfo.getReply(JobID)
	for k := range outResult {
		errorList = append(errorList, outResult[k].Stderr)
		lvn_log.Debug.Println(string(outResult[k].Stderr))
	}
	return errResult
}

type Ping struct {
	PacketTransmitted int
	PacketRecieved    int
	PacketLost        int
	TotalTime         int
	RttMin            int
	RttAvg            int
	RttMax            int
	StanderdDev       int
}

func (GjefInfo *GjefParam) PingFromLan(ip string, interval int, count int) ([]Ping, []string, error) {
	lvn_log.Debug.Printf("Enter %s with parameter %s,interval %d , count %d, ", utils.GetFuncName(), ip, interval, count)
	var PingOut []Ping
	var errorList []string
	//outCreate,errCreate :=createCpsFile()
	isValidIp, errValidate := utils.ValidateIP(ip)
	if isValidIp == false {
		lvn_log.Err.Printf("Invalid IP: %s\n", ip)
		return PingOut, errorList, errValidate
	}

	var cmd []string
	cmd = append(cmd, "ip netns exec ns-svc-lan", fmt.Sprintf("ping -i %d -c %d %s", interval, count, ip))
	GjefInfo.Command = makeCmd(cmd)
	lvn_log.Debug.Printf("Recieving %s", GjefInfo.Command)
	outGjef, errGjef := GjefInfo.makeRequest()
	if errGjef != nil {
		lvn_log.Err.Println("Error from makeRequest()", errGjef)
	}

	JobID := getJobID(outGjef)
	lvn_log.Debug.Printf("Job ID: %s", JobID)
	outResult, errResult := GjefInfo.getReply(JobID)
	for k := 0; k < len(outResult); k++ {
		str := strings.TrimSpace(outResult[k].Stderr)
		if strings.Compare(str, "") == 0 {
			errorList = append(errorList, fmt.Sprintf("Success : %s", outResult[k].UUID))
			lvn_log.Info.Printf("Success : %s", outResult[k].UUID)

			re := regexp.MustCompile(`(\d+) packets transmitted, (\d+) received, (\+\d+ duplicates, )?(\d+)% packet loss, time (\d+)ms\nrtt min\/avg\/max\/mdev = ([\d\.]+)\/([\d\.]+)\/([\d\.]+)\/([\d\.]+)`)
			res := re.FindSubmatch([]byte(outResult[k].Stdout))
			//fmt.Printf("%s", res)
			Pt, _ := strconv.Atoi(string(res[1]))
			Pr, _ := strconv.Atoi(string(res[2]))
			Pl, _ := strconv.Atoi(string(res[4]))
			Tt, _ := strconv.Atoi(string(res[5]))
			Rmi, _ := strconv.Atoi(string(res[6]))
			Ra, _ := strconv.Atoi(string(res[7]))
			Rma, _ := strconv.Atoi(string(res[8]))
			Sd, _ := strconv.Atoi(string(res[9]))
			x := Ping{
				PacketTransmitted: Pt,
				PacketRecieved:    Pr,
				PacketLost:        Pl,
				TotalTime:         Tt,
				RttMin:            Rmi,
				RttMax:            Rma,
				RttAvg:            Ra,
				StanderdDev:       Sd,
			}
			PingOut = append(PingOut, x)
		} else {
			errorList = append(errorList, fmt.Sprintf("%s:\t%s", outResult[k].UUID, outResult[k].Stderr))
			lvn_log.Err.Printf("Failed : %s", outResult[k].UUID)
		}
	}
	lvn_log.Debug.Println("Returning from ", utils.GetFuncName(), "with values", PingOut, errorList, errResult)
	lvn_log.Info.Println("Success")
	return PingOut, errorList, errResult
}

type DiskSpace struct {
	FileSystem  string
	Size        string
	Used        string
	UserPercent int
	MountedOn   string
}

func (GjefInfo *GjefParam) CheckDiskSpace() ([][]DiskSpace, []string, error) {
	lvn_log.Debug.Printf("Enter %s ", utils.GetFuncName())
	var Disk [][]DiskSpace
	//outCreate,errCreate :=createCpsFile()
	var cmd []string
	cmd = append(cmd, "df -h | awk '{print $1, $2, $3, $5, $6}'")
	GjefInfo.Command = makeCmd(cmd)
	lvn_log.Debug.Printf("Recieving %s", GjefInfo.Command)
	//fmt.Println(cmd)
	outGjef, errGjef := GjefInfo.makeRequest()
	if errGjef != nil {
		lvn_log.Err.Println("Error from makeRequest()", errGjef)
	}
	JobID := getJobID(outGjef)
	lvn_log.Debug.Printf("Job ID: %s", JobID)
	var errorList []string
	outResult, errResult := GjefInfo.getReply(JobID)
	if errResult != nil {
		lvn_log.Err.Println("Error:", errResult)
		return Disk, errorList, errResult
	}
	for k := 0; k < len(outResult); k++ {
		str := strings.TrimSpace(outResult[k].Stderr)
		if strings.Compare(str, "") == 0 {
			errorList = append(errorList, fmt.Sprintf("Success : %s", outResult[k].UUID))
			lvn_log.Info.Printf("Success : %s", outResult[k].UUID)
			re := regexp.MustCompile(`([\w\/]+) ([\d\.]+[GMK]?) ([\d\.]+[GMK]?) ([\d]+)% ([\w\/\-\:]+)`)
			res := re.FindAllSubmatch([]byte(outResult[k].Stdout), -1)
			fmt.Printf("%s", res)
			var DiskInfo []DiskSpace

			for i := 0; i < len(res); i++ {

				Fs := string(res[i][1])
				S := string(res[i][2])
				U := string(res[i][3])
				Up, _ := strconv.Atoi(string(res[i][4]))
				M := string(res[i][5])
				y := DiskSpace{
					FileSystem:  Fs,
					Size:        S,
					Used:        U,
					UserPercent: Up,
					MountedOn:   M,
				}

				DiskInfo = append(DiskInfo, y)

			}
			Disk = append(Disk, DiskInfo)
		} else {
			errorList = append(errorList, fmt.Sprintf("%s:\t%s", outResult[k].UUID, outResult[k].Stderr))
			lvn_log.Err.Printf("Failed : %s", outResult[k].UUID)
		}

	}
	lvn_log.Debug.Println("Returning from ", utils.GetFuncName(), "with values", Disk, errorList, errResult)
	lvn_log.Info.Println("Success")
	return Disk, errorList, errResult

}

type CpuInfo struct {
	Command  string
	CpuUsage float64
}

func (GjefInfo *GjefParam) CheckCpuUsage() ([][]CpuInfo, []string, error) {
	lvn_log.Debug.Printf("Enter %s", utils.GetFuncName())
	var Cpu [][]CpuInfo
	//outCreate,errCreate :=createCpsFile()
	var cmd []string
	cmd = append(cmd, "top -b -n 1 | awk '{print $9, $12}'")
	GjefInfo.Command = makeCmd(cmd)
	lvn_log.Debug.Printf("Recieving %s", GjefInfo.Command)
	//fmt.Println(cmd)
	outGjef, errGjef := GjefInfo.makeRequest()
	if errGjef != nil {
		lvn_log.Err.Println("Error from makeRequest()", errGjef)
	}
	JobID := getJobID(outGjef)
	lvn_log.Debug.Printf("Job ID: %s", JobID)
	var errorList []string
	outResult, errResult := GjefInfo.getReply(JobID)
	if errResult != nil {
		lvn_log.Err.Println("Error:", errResult)
		return Cpu, errorList, errResult
	}
	for k := 0; k < len(outResult); k++ {
		str := strings.TrimSpace(outResult[k].Stderr)
		if strings.Compare(str, "") == 0 {
			errorList = append(errorList, fmt.Sprintf("Success : %s", outResult[k].UUID))
			lvn_log.Info.Printf("Success : %s", outResult[k].UUID)

			re := regexp.MustCompile(`([\d\.]+) ([\w\/\-\:\(\)]+)`)
			res := re.FindAllSubmatch([]byte(outResult[k].Stdout), -1)
			//fmt.Printf("%s", res)
			var CpuDetail []CpuInfo

			for i := 0; i < len(res); i++ {

				Cm := string(res[i][2])
				Cp, _ := strconv.ParseFloat(string(res[i][1]), 64)
				y := CpuInfo{
					Command:  Cm,
					CpuUsage: Cp,
				}

				CpuDetail = append(CpuDetail, y)

			}
			Cpu = append(Cpu, CpuDetail)
		} else {
			errorList = append(errorList, fmt.Sprintf("%s:\t%s", outResult[k].UUID, outResult[k].Stderr))
			lvn_log.Err.Printf("Failed : %s", outResult[k].UUID)
		}

	}
	lvn_log.Debug.Println("Returning from ", utils.GetFuncName(), "with values", Cpu, errorList, errResult)
	return Cpu, errorList, errResult

}
func (GjefInfo *GjefParam) DeleteFile(FilePath string) (string, []string, error) {
	lvn_log.Debug.Printf("Enter %s with filePath %s", utils.GetFuncName(), FilePath)
	//outCreate,errCreate :=createCpsFile()
	var cmd []string
	cmd = append(cmd, fmt.Sprintf("rm -rf %s", FilePath))
	GjefInfo.Command = makeCmd(cmd)
	lvn_log.Debug.Printf("Recieving %s", GjefInfo.Command)
	outGjef, errGjef := GjefInfo.makeRequest()
	if errGjef != nil {
		lvn_log.Err.Println("Error from makeRequest()", errGjef)
	}
	JobID := getJobID(outGjef)
	lvn_log.Debug.Printf("Job ID: %s", JobID)
	var errorList []string
	outResult, errResult := GjefInfo.getReply(JobID)
	if errResult != nil {
		lvn_log.Err.Println("Error:", errResult)
		return "", errorList, errResult
	}
	for k := 0; k < len(outResult); k++ {
		str := strings.TrimSpace(outResult[k].Stderr)
		if strings.Compare(str, "") == 0 {
			errorList = append(errorList, fmt.Sprintf("Success : %s", outResult[k].UUID))
			lvn_log.Info.Printf("Success : %s", outResult[k].UUID)
		} else {
			errorList = append(errorList, fmt.Sprintf("%s:\t%s", outResult[k].UUID, outResult[k].Stderr))
			lvn_log.Err.Printf("Failed : %s", outResult[k].UUID)
		}
	}
	lvn_log.Debug.Println("Returning from ", utils.GetFuncName(), "with values", errorList, errResult)
	return "", errorList, errResult
}

type LvnService struct {
	ServiceName string
	Status      string
	ProcessId   int
}

func (GjefInfo *GjefParam) CheckLavelleService() ([][]LvnService, []string, error) {
	lvn_log.Debug.Printf("Enter %s ", utils.GetFuncName())
	var LvnServiceList [][]LvnService
	//outCreate,errCreate :=createCpsFile()
	var cmd []string
	cmd = append(cmd, "service lavelle-apps status")
	GjefInfo.Command = makeCmd(cmd)
	lvn_log.Debug.Printf("Recieving %s", GjefInfo.Command)
	//fmt.Println(cmd)
	outGjef, errGjef := GjefInfo.makeRequest()
	if errGjef != nil {
		lvn_log.Err.Println("Error from makeRequest()", errGjef)
	}
	JobID := getJobID(outGjef)
	lvn_log.Debug.Printf("Job ID: %s", JobID)
	var errorList []string
	outResult, errResult := GjefInfo.getReply(JobID)
	if errResult != nil {
		lvn_log.Err.Println("Error:", errResult)
		return LvnServiceList, errorList, errResult
	}
	for k := 0; k < len(outResult); k++ {
		str := strings.TrimSpace(outResult[k].Stderr)
		if strings.Compare(str, "") == 0 {
			errorList = append(errorList, fmt.Sprintf("Success : %s", outResult[k].UUID))
			lvn_log.Info.Printf("Success : %s", outResult[k].UUID)

			re := regexp.MustCompile(`(lavelle-[\w\W]+?) ([\w]+\/[\w]+),? ?(process ([\d]+))?`)
			res := re.FindAllSubmatch([]byte(outResult[k].Stdout), -1)
			var LvnServiceInfo []LvnService
			for i := 0; i < len(res); i++ {

				Sn := string(res[i][1])
				St := string(res[i][2])
				var Pid int
				if string(res[i][4]) == "" {
					Pid = 0
				} else {
					Pid, _ = strconv.Atoi(string(res[i][4]))
				}

				y := LvnService{
					ServiceName: Sn,
					Status:      St,
					ProcessId:   Pid,
				}

				LvnServiceInfo = append(LvnServiceInfo, y)

			}
			LvnServiceList = append(LvnServiceList, LvnServiceInfo)
		} else {
			errorList = append(errorList, fmt.Sprintf("%s:\t%s", outResult[k].UUID, outResult[k].Stderr))
			lvn_log.Err.Printf("Failed : %s", outResult[k].UUID)
		}

	}

	lvn_log.Debug.Println("Returning from ", utils.GetFuncName(), "with values", LvnServiceList, errorList, errResult)
	return LvnServiceList, errorList, errResult
}

//type GenericFn func(v interface...) (string, []string, error)
//func (Gjef *GjefParam) Execute(GenericFn, v interface...) {
//	GenericFn(...v)
//}

func (GjefInfo *GjefParam) WhitelistIp(listOfIp []string) (string, []string, error) {
	lvn_log.Debug.Printf("Enter %s with filePath %s", utils.GetFuncName(), listOfIp)
	//outCreate,errCreate :=createCpsFile()
	stringIp := strings.Join(listOfIp, ",")
	var cmd []string
	cmd = append(cmd, fmt.Sprintf("ln_sys_conf_update.py system add firewall '{whitelist=%s}'", stringIp))
	GjefInfo.Command = makeCmd(cmd)
	lvn_log.Debug.Printf("Recieving %s", GjefInfo.Command)
	outGjef, errGjef := GjefInfo.makeRequest()
	if errGjef != nil {
		lvn_log.Err.Println("Error from makeRequest()", errGjef)
	}
	JobID := getJobID(outGjef)
	lvn_log.Debug.Printf("Job ID: %s", JobID)
	var errorList []string
	outResult, errResult := GjefInfo.getReply(JobID)
	if errResult != nil {
		lvn_log.Err.Println("Error:", errResult)
		return "", errorList, errResult
	}
	for k := 0; k < len(outResult); k++ {
		str := strings.TrimSpace(outResult[k].Stderr)
		if strings.Compare(str, "") == 0 {
			errorList = append(errorList, fmt.Sprintf("Success : %s", outResult[k].UUID))
			lvn_log.Info.Printf("Success : %s", outResult[k].UUID)

		} else {
			errorList = append(errorList, fmt.Sprintf("%s:\t%s", outResult[k].UUID, outResult[k].Stderr))
			lvn_log.Err.Printf("Failed : %s", outResult[k].UUID)
		}

	}

	lvn_log.Debug.Println("Returning from ", utils.GetFuncName(), "with values", "", errorList, errResult)
	return "", errorList, errResult
}
