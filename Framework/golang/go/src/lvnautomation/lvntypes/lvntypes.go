package lvntypes

//import (
	//"fmt"
//)

type BANDWIDTH float32

func (i BANDWIDTH) GetPlusOrMinusPercent(bw float32) (BANDWIDTH, BANDWIDTH) {

        //float_i := float32(i)
        //bw_percent := float_i * float32(bw)/100.0
        bw_percent := BANDWIDTH(bw) * i/BANDWIDTH(100)

        upper := i + bw_percent
        lower := i - bw_percent

	//fmt.Println(upper, lower)
        return upper, lower
}

func (i BANDWIDTH) WithinRange(max  BANDWIDTH, min BANDWIDTH) bool {
	//fmt.Println(max, min)
        return (i <= max && i >= min)
}

