package newsetuputils

import (
	//"fmt"
	"encoding/json"
	"io/ioutil"
	"strings"
)

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

/*
type PhyPoints struct {
	Name string `json:"name"`
	Iface string `json:"iface"`
}*/

type NodeInfo struct {
    Name string `json:"name"`
    MgmtIp string `json:"mgmt_ip"`
    SshPort string `json:"ssh_port"`
    SshUsername string `json:"ssh_username"`
    SshPassword string `json:"ssh_password"`
    NodeType string `json:"type"`
}

type Node struct {
	Name string `json:"name"`
	Iface string `json:"iface"`
	Via string `json:"via"`
}

type PhyPoint struct {
	Iface string `json:"iface"`
	Nodes []Node `json:"nodes"`
}

type Device struct {
	DeviceName string `json:"name"`
	Phypoints []PhyPoint `json:"phy_points"`
}

type Site struct {
    SiteName string `json:"site_name"`
    DeviceInfo Device `json:"device"`
    PhyPointsInfo []PhyPoint `json:"phy_points"`
}

type Route struct {
	SrcNw string `json:"source"`
	DestNw string `json:"destination"`
	Gateway string `json:"gateway"`
}

type Setup001 struct {
    Baseurl string `json:"base_url"`
    Master Credentials `json:"master"`
    Customerurl string `json:"customer_url"`
    SysAdmin Credentials `json:"sys_admin"`
    NetAdmin Credentials `json:"net_admin"`
    DefaultNwGrpName string `json:"default_nw_grp"`
    DefaultSecurityProfileName string `json:"default_security_profile"`
    DefaultEncryptionProfileName string `json:"default_encryption_profile"`
    Hub Site `json:"hub"`
    Spoke1 Site `json:"spoke1"`
    Spoke2 Site `json:"spoke2"`
    Nodes []NodeInfo `json:"nodes"`
    MgmtRoutes []Route `json:"mgmt_routes"`
}


func NewSetup001(setup_file string) *Setup001 {
    var setup Setup001
    file, err := ioutil.ReadFile(setup_file)
    if err != nil {
	    return nil
    }
    json.Unmarshal([]byte(file), &setup)
    //fmt.Println(setup)
    return &setup
}

func (r *Route) GetRouteInfo() (string, string, string) {
	return r.SrcNw, r.DestNw, r.Gateway
}

func (s *Setup001) GetMgmtRoutes() []Route {
	return s.MgmtRoutes
}

func (s *Setup001) SearchRoute(src, dest string) string {
	for _, route := range s.MgmtRoutes {
            if route.SrcNw == src && route.DestNw == dest {
		    return route.Gateway
	    }
	}
	return ""
}

func (s *Setup001) BaseUrl() string {
	return s.Baseurl
}

func (s *Setup001) MasterCredentials() (string, string) {
	return s.Master.Username, s.Master.Password
}

func (s *Setup001) CustomerUrl() string {
	return s.Customerurl
}

func (s *Setup001) SysAdminCredentials() (string, string) {
	return s.SysAdmin.Username, s.SysAdmin.Password
}

func (s *Setup001) NetAdminCredentials() (string, string) {
	return s.NetAdmin.Username, s.NetAdmin.Password
}

func (s *Setup001) DefaultNwGrp() string {
	return s.DefaultNwGrpName
}

func (s *Setup001) DefaultSecurityProfile() string {
	return s.DefaultSecurityProfileName
}

func (s *Setup001) DefaultEncryptionProfile() string {
	return s.DefaultEncryptionProfileName
}


func (s *Setup001) HubSiteName() string {
	return s.Hub.SiteName
}

func (s *Setup001) HubDeviceInfo() *Device {
	return &s.Hub.DeviceInfo
}

func (s *Setup001) HubDeviceName() string {
	return s.Hub.DeviceInfo.DeviceName
}


func (s *Setup001) S1SiteName() string {
	return s.Spoke1.SiteName
}

func (s *Setup001) S2SiteName() string {
	return s.Spoke2.SiteName
}

func (s *Setup001) S1DeviceName() string {
	return s.Spoke1.DeviceInfo.DeviceName
}

func (s *Setup001) S1DeviceInfo() *Device {
	return &s.Spoke1.DeviceInfo
}

func (s *Setup001) S2DeviceName() string {
	return s.Spoke2.DeviceInfo.DeviceName
}

func (s *Setup001) S2DeviceInfo() *Device {
	return &s.Spoke2.DeviceInfo
}

func (s *Setup001) GetNodeInfo(node_name string) (string, string, string, string, string) {
	for _, node_info := range s.Nodes {
		if node_info.Name == node_name {
			return node_info.MgmtIp, node_info.SshPort, node_info.SshUsername, node_info.SshPassword, node_info.NodeType
		}
	}
	return "", "", "", "", ""
}


func (d *Device) GetPhyInfo(phy_point_ifacename string) []Node {
	for _, phy_point := range d.Phypoints {
		if phy_point.Iface == phy_point_ifacename {
			return phy_point.Nodes
		}
	}
	return []Node{}
}

func (n *Node) IsViaDirect() bool {
	return strings.ToLower(strings.TrimSpace(n.Via)) == "direct"
}

func (n *Node) IsViaL2() bool {
	return strings.ToLower(strings.TrimSpace(n.Via)) == "l2"
}

func (n *Node) IsViaL3() bool {
	return strings.ToLower(strings.TrimSpace(n.Via)) == "l3"
}

func (n *Node) GetName() string {
	return strings.TrimSpace(n.Name)
}

func (n *Node) GetIface() string {
	return strings.TrimSpace(n.Iface)
}

/*
func main() {
	file := "new_setup_info.json"
	x := NewSetup001(file)
	//fmt.Println(x.BaseUrl())
	//fmt.Println(x.HubSiteName(), x.HubDeviceName())
	//fmt.Println(x.HubDeviceInfo().GetPhyInfo("eth0"))
	for _, i := range x.HubDeviceInfo().GetPhyInfo("eth0") {
		fmt.Println(i.IsViaDirect(), i.IsViaL2(), i.IsViaL3())
		fmt.Println(x.GetNodeInfo(i.GetName()))
	}
}
*/
