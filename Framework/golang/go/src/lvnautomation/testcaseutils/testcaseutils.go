package testcaseutils

import (
        "fmt"
        "runtime"
        "strings"
	"time"
	lvn_log "lvnautomation/lvnlog"
       )

var PULL_CS_INFO_SECONDS int = 30
func Get_testcase_name() (hier string, err error) {
	//fmt.Println(PrintFuncName())
        hier = ""
        if _, filepath, _, ok:= runtime.Caller(2); ok==true {
                fileInfo := strings.Split(filepath, "/")
                hier = fileInfo[len(fileInfo) - 1]
                //fmt.Println(hier[len(hier)-1])
                return hier,  nil
        }
        return hier, fmt.Errorf("Runtime caller returned Not Ok")
}

func FuncName() (funcName string){
    fpcs := make([]uintptr, 1)

    // Skip 2 levels to get the caller
    n := runtime.Callers(2, fpcs)
    if n == 0 {
        fmt.Println("MSG: NO CALLER")
    }

    caller := runtime.FuncForPC(fpcs[0] - 1)
    if caller == nil {
        fmt.Println("MSG CALLER WAS NIL")
    }

    // Print the name of the function
    //fmt.Println(caller.Name())
    funcName = caller.Name()
    return
}

func WaitForConfigPull() {
    lvn_log.Info.Printf("Waiting for %d sec for configs to get fetched in cloudports\n", PULL_CS_INFO_SECONDS)
    time.Sleep(time.Duration(PULL_CS_INFO_SECONDS) * 1000 * time.Millisecond)
}
