package utils

import (
	"strconv"
	"strings"
	"fmt"
	"runtime"
	"time"
	"net"
	"regexp"
	//lvntypes "lvnautomation/lvntypes"
)

func GetCurrentDateTimeForFile() string {
    return time.Now().Format("2006-01-02-15-04-05")
}

func ValidateIP(input string) (bool, error) {
	SplitIP := strings.Split(input, ".")
	if len(SplitIP) != 4 {
		return false, fmt.Errorf("Invalid IP length")
	}
	for _, val := range SplitIP {
		if octate, err1 := strconv.Atoi(val); err1 == nil {
			if octate > 255 || octate < 0 {
				return false, fmt.Errorf("Invalid IP")
			}
		} else {
			return false, fmt.Errorf("string conversion error")
		}
	}
	return true, nil
}

func GetFuncName() (funcName string){
   fpcs := make([]uintptr, 1)    // Skip 2 levels to get the caller
   n := runtime.Callers(2, fpcs)
   if n == 0 {
       fmt.Println("MSG: NO CALLER")
   }
   caller := runtime.FuncForPC(fpcs[0] - 1)
   if caller == nil {
       fmt.Println("MSG CALLER WAS NIL")
   }    // Print the name of the function
   //fmt.Println(caller.Name())
   funcName = caller.Name()
   //return funcName
   return ""
}


func GetUuidFromSlno(sl_no int) (bool, string) {

    result:=false
    uuid:= ""

    if sl_no > 65535 || sl_no < 0 {
        return result, uuid
    }
    uuid = "00:00:00:00:00:00:00:00:00:00:01:11:00:00:"

    sl_no_hex := fmt.Sprintf("%04x", sl_no)
    sl_no_hex_list := strings.Split(sl_no_hex, "")

    c:=1
    for _, v := range sl_no_hex_list {
        uuid=uuid+v
        c++
        if c%2!=0 {
            uuid=uuid+":"
        }
    }

    uuid=strings.Trim(uuid, ":")
    return result, uuid
}

func Get_ip_address_from_netaddr(network_addr string, no_of_ips int, excluded_ips ...string) ([]string, error) {
	ip, ipnet, err := net.ParseCIDR(network_addr)

	ip1 := ip
	if err != nil {
		return nil, err
	}

	var ips []string
	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {

		ips = append(ips, ip.String())

	}
	// remove network address and broadcast address
	lenIPs := len(ips)
	var finalIpList []string
	switch {
	case lenIPs < 2:
		return ips, nil

	default:
		for _, i := range ips[1 : len(ips)-1] {
			if !stringInSlice(i, excluded_ips) && i != ip1.String() {
				finalIpList = append(finalIpList, i)
			}
		}
	}

        /* if no of ips = -1, retrun all*/
        if no_of_ips >= 0 {
            if len(finalIpList) > no_of_ips {
                return finalIpList[0:no_of_ips], nil
            }
        }

	return finalIpList, nil
}


func IsIPaddrInNetwork(ip_addr string, network_addr string) (bool, error) {

	_, ipnet, err := net.ParseCIDR(network_addr)

	if err != nil {
		return false, err
	}

	return ipnet.Contains(net.ParseIP(ip_addr)), nil
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func NetMaskInt2String(mask int) string {
	return net.IP(net.CIDRMask(mask, 32)).String()
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

type UTILSINT int
type UTILSF32 float32

/*
func (i lvntypes.BANDWIDTH) GetPlusOrMinusPercent(bw float32) (lvntypes.BANDWIDTH, lvntypes.BANDWIDTH) {

	//float_i := float32(i)
	//bw_percent := float_i * float32(bw)/100.0
	bw_percent := lvntypes.BANDWIDTH(bw) * i/lvntypes.BANDWIDTH(100)

	upper := i + bw_percent
	lower := i - bw_percent

	return upper, lower
}
*/
func (i UTILSINT) GetPlusOrMinusPercent(bw int) (float32, float32) {

	float_i := float32(i)
	bw_percent := float_i * float32(bw)/100.0

	upper := float_i + bw_percent
	lower := float_i - bw_percent

	return upper, lower
}

func (i UTILSF32) GetPlusOrMinusPercent(bw int) (float32, float32) {

	float_i := float32(i)
	bw_percent := float32(i) * float32(bw)/100.0

	upper := float_i + bw_percent
	lower := float_i - bw_percent

	return upper, lower
}

func (i UTILSINT) GetPlusOrMinus(bw int) (int, int) {

	return int(i)+bw, int(i)-bw
}

func (i UTILSF32) GetPlusOrMinus(bw int) (float32, float32) {

	bw_f := float32(bw)
	return float32(i)+float32(bw_f), float32(i)-float32(bw_f)
}

/*
func (i lvntypes.BANDWIDTH) WithinRange(max  lvntypes.BANDWIDTH, min lvntypes.BANDWIDTH) bool {
	return (i <= max && i >= min)
}
*/
/*
func (i UTILSINT) WithinRange(max int, min int) bool {
	if i <= max && i >= min {
		return true
	} else {
		return false
	}
}

func (i UTILSF32) WithinRange(max UTILSF32, min UTILSF32) bool {
	if i <= max && i >= min {
		return true
	} else {
		return false
	}
}*/

func num2mask(num uint32) []uint64 {
    mask := (0xFFFFFFFF << (32 - num)) & 0xFFFFFFFF; //24 is the netmask
    var dmask uint64
    dmask = 32
    localmask := make([]uint64, 0, 4)
    for i := 1; i <= 4; i++{
        tmp := mask >> (dmask - 8) & 0xFF
        localmask = append(localmask, uint64(tmp))
        dmask -= 8
    }

    return localmask
}


func GetDeletedAddedStrings(prev []string, current []string) ([]string, []string) {

        x := make(map[string]struct{}, len(prev))
        y := make(map[string]struct{}, len(current))

        deleted := []string{}
        added   := []string{}

        for _, i := range prev {
                x[i] = struct{}{}
        }
        //o(n)

        for _, i := range current {
                y[i] = struct{}{}
        }
        //o(n)

        for _, i := range prev {
                if _, ok := y[i]; !ok {
                        deleted = append(deleted, i)
                }
        }
        //o(n)

        for _, i := range current {
                if _, ok := x[i]; !ok {
                        added = append(added, i)
                }
        }
        //o(n)
        // 4 x o(n)

        return deleted, added
}


func ParsePingOutput(op string) (status bool, tx int, rx int, loss int, time int, rtt_min float64, rtt_avg float64, rtt_max float64, rtt_mdev float64) {


    var err error
    status = false
    //re := regexp.MustCompile(`(\d+)\s+packets transmitted,\s+(\d+)\s+received,\s+(\d+)%\s+packet loss,\s+time\s+(\d+)ms.*rtt min/avg/max/mdev = ([0-9\.]+)/([0-9\.]+)/([0-9\.]+)/([0-9\.]+)\s+ms`)
    re := regexp.MustCompile(`([0-9]+)\s+packets transmitted,\s+(\d+)\s+received,\s+.*(\d+)%\s+packet loss,\s+time\s+(\d+)ms`)
    //re := regexp.MustCompile(`([0-9]+)\s+packets transmitted,\s+(\d+)\s+received,\s+(\d+)%\s+packet loss,\s+time\s+(\d+)ms\n*rtt\s+min\/avg\/max\/mdev\s+\=\s+([0-9\.]+)\/([0-9\.]+)\/([0-9\.]+)\/([0-9\.]+)\s+ms`)

    res := re.FindStringSubmatch(op)
    if len(res) == 5 {
	    if tx, err = strconv.Atoi(res[1]); err != nil {
		    return
	    }
	    if rx, err = strconv.Atoi(res[2]); err != nil {
		    return
	    }
	    if loss, err = strconv.Atoi(res[3]); err != nil {
		    return
	    }
	    if time, err = strconv.Atoi(res[4]); err != nil {
		    return
	    }
	    /*
	    if rtt_min, err = strconv.ParseFloat(res[5], 64); err != nil {
		    return
	    }
	    if rtt_avg, err = strconv.ParseFloat(res[6], 64); err != nil {
		    return
	    }
	    if rtt_max, err = strconv.ParseFloat(res[7], 64); err != nil {
		    return
	    }
	    if rtt_mdev, err = strconv.ParseFloat(res[8], 64); err != nil {
		    return
	    }*/
	    status = true
    }
    return
}

func ConvertStringToPktList (b string) ([]string ,  map[int][]string) {

    pkt_hexdump_list := []string{}
    pkt_hexdump_map := map[int][]string{}
    re1 := regexp.MustCompile(`(\d\d:\d\d:\d\d.\d\d\d\d\d\d).*?\n((\s*0x[0-9a-fA-F]{4}:(\s+[0-9a-fA-F]{2,4}){1,8}\n)*)`)
    re2 := regexp.MustCompile(`\s*0x[0-9a-fA-F]{4}:([0-9a-fA-F\s]*)\n`)
    re3 := regexp.MustCompile(`\s+`)

    pkt_info_list := re1.FindAllStringSubmatch(b, -1)

    var timestamp_match_index, pkt_hexdump_match_index int
    timestamp_match_index = 1
    pkt_hexdump_match_index = 2
    for idx, pkt_info := range pkt_info_list {
        timestamp := pkt_info[timestamp_match_index]
	pkt_hexdump_with_lineno := pkt_info[pkt_hexdump_match_index]
	pkt_hexdump_list_with_spaces := re2.FindAllStringSubmatch(pkt_hexdump_with_lineno, -1)
        pkt_hexdump := ""
        hexdump_with_space_index := 1
	for _, pkt_hexdump_with_spaces := range pkt_hexdump_list_with_spaces {
            pkt_hexdump  = pkt_hexdump  + re3.ReplaceAllString(pkt_hexdump_with_spaces[hexdump_with_space_index], "")
	}
	pkt_hexdump_list = append(pkt_hexdump_list, pkt_hexdump)
	pkt_hexdump_map[idx] = []string{timestamp, pkt_hexdump}
    }
    return pkt_hexdump_list, pkt_hexdump_map
}

/*
func ConvertStringToPktList (b string) []string {
    //file, err := os.Open(filename)
    //if err != nil {
    //    log.Fatal(err)
   // }
    //defer file.Close()


   //b, err := ioutil.ReadAll(file) 
   //re :=regexp.MustCompile(`(\d\d:\d\d:\d\d.\d\d\d\d\d\d(.*?)\n([0-9x\sa-fA-F\n\:]*)?)(\d\d:\d\d:\d\d.\d\d\d\d\d\d)?`)
   re :=regexp.MustCompile(`(\d\d:\d\d:\d\d.\d\d\d\d\d\d(.*?)\n([0-9x\sa-fA-F\n\:]*))(\d\d:\d\d:\d\d.\d\d\d\d\d\d)?`)
   re2 := regexp.MustCompile(`\d\d:\d\d:\d\d`)
   re3 := regexp.MustCompile(`\s+`)

   x := re.FindAllStringSubmatch(string(b), -1)
   //fmt.Println(len(x))

   pkts_string := []string{}
   for _, y := range x {
           hex2 := ""
           z :=  strings.TrimSpace( re2.Split(y[3], -1)[0])
           z1 := strings.Split(z, "\n")
           for _, i := range z1 {
                   hex2 = hex2 + strings.Join(re3.Split(strings.TrimSpace(i), -1)[1:], "")
           }
           pkts_string = append(pkts_string, hex2)
    }
    fmt.Println("length of pkts: ", len(pkts_string))
    return  pkts_string
}
*/

func ParseIpRouteShowOutput(op string) (map[string]string, map[string]string, map[string]string) {

	onlink_routes, default_route, routes := map[string]string{}, map[string]string{}, map[string]string{}
        op = strings.TrimSpace(op)
        route_list := strings.Split(op, "\n")

        re := regexp.MustCompile(`(\d+\.\d+\.\d+\.\d+/\d+).*link src\s+(\d+\.\d+\.\d+\.\d+)`)
        def_re := regexp.MustCompile(`(default) via (\d+\.\d+\.\d+\.\d+)`)
        route_re := regexp.MustCompile(`(\d+\.\d+\.\d+\.\d+/\d+) via (\d+\.\d+\.\d+\.\d+)`)

        for _, route := range route_list {

                if x := re.FindAllStringSubmatch(route, -1); len(x) > 0 {
                        onlink_routes[x[0][1]] = x[0][2]
			continue
                }
                if x := def_re.FindAllStringSubmatch(route, -1); len(x) > 0 {
                        default_route[x[0][1]] = x[0][2]
			continue
                }
                if x := route_re.FindAllStringSubmatch(route, -1); len(x) > 0 {
                        routes[x[0][1]] = x[0][2]
                }
        }

        return default_route, onlink_routes, routes
}
