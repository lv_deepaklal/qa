package lvnanalyser

import (
	//"errors"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"encoding/hex"
	//"net"
	//"fmt"
)

type AnalysePkt struct {
    Pkt string
    PktBytes []byte
    gopkt gopacket.Packet
}

//type GoPkt struct {
//    gopkt gopacket.Packet
//}

func NewPktAnalyser(pkt string) *AnalysePkt {
    //fmt.Println()
    var p AnalysePkt
    p.Pkt = pkt
    p.decode()
    //p.gopkt = GoPkt{gopkt: gopacket.NewPacket(p.PktBytes, layers.LayerTypeEthernet, gopacket.Default)}
    p.gopkt = gopacket.NewPacket(p.PktBytes, layers.LayerTypeEthernet, gopacket.Default)
    return &p
}

func (p *AnalysePkt) GetGoPkt() gopacket.Packet {
	return p.gopkt
}

func (p *AnalysePkt) decode() {
    if pkt_data, err :=  hex.DecodeString(p.Pkt); err == nil {
        p.PktBytes = pkt_data
    }
}

/* Working on gopkt */
func (p *AnalysePkt) IsLavelleTunnelPkt() (bool, *gopacket.Layer) {
    lvnLayer := p.gopkt.Layer(layers.LayerTypeLNTun)
    return (lvnLayer != nil), &lvnLayer
}

func (p *AnalysePkt) IsTcpPkt() (bool, *gopacket.Layer) {
    lvnLayer := p.gopkt.Layer(layers.LayerTypeTCP)
    return (lvnLayer != nil), &lvnLayer
}

func (p *AnalysePkt) IsIPv4Pkt() (bool, *gopacket.Layer) {
    lvnLayer := p.gopkt.Layer(layers.LayerTypeIPv4)
    return (lvnLayer != nil), &lvnLayer
}

func (p *AnalysePkt) IsGREPkt() (bool, *gopacket.Layer) {
    lvnLayer := p.gopkt.Layer(layers.LayerTypeGRE)
    return (lvnLayer != nil), &lvnLayer
}

func (p *AnalysePkt) IsICMPPkt() (bool, *gopacket.Layer) {
    lvnLayer := p.gopkt.Layer(layers.LayerTypeICMPv4)
    return (lvnLayer != nil), &lvnLayer
}

func (p *AnalysePkt) IsTCPPkt() (bool, *gopacket.Layer) {
    lvnLayer := p.gopkt.Layer(layers.LayerTypeTCP)
    return (lvnLayer != nil), &lvnLayer
}

func (p *AnalysePkt) IsUDPPkt() (bool, *gopacket.Layer) {
    lvnLayer := p.gopkt.Layer(layers.LayerTypeUDP)
    return (lvnLayer != nil), &lvnLayer
}

/* Working on analyser */
func (p *AnalysePkt) IsLavelleTunnelCtrlPkt() bool {
    if ok, lvnlayer := p.IsLavelleTunnelPkt(); ok {
        if lvn, err := (*lvnlayer).(*layers.LNTun); err {
            /* All pkts of controltype != 1 are control pkts */
            return (lvn.ControlType != 1)
		}
    }
    return false
}


func (p *AnalysePkt) IsLavelleTunnelDataPkt() bool {
    if ok, lvnlayer := p.IsLavelleTunnelPkt(); ok {
        if lvn, err := (*lvnlayer).(*layers.LNTun); err {
		    /* All pkts of controltype != 1 are control pkts */
            return (lvn.ControlType == 1)
		}
    }
	return false
}

func (p *AnalysePkt) GetLavelleTunnelLayer() (*layers.LNTun) {

    if res, l := p.IsLavelleTunnelPkt(); res {
        if l_data, err := (*l).(*layers.LNTun); err {
            return l_data
		}
	}
	return nil
}

func (p *AnalysePkt) GetIPLayer() (*layers.IPv4) {

    if res, l := p.IsIPv4Pkt(); res {
        if l_data, err := (*l).(*layers.IPv4); err  {
            return l_data
		}
	}
	return nil
}

func (p *AnalysePkt) GetICMPLayer() (*layers.ICMPv4) {
    if res, l := p.IsICMPPkt(); res {
        if l_data, err := (*l).(*layers.ICMPv4); err  {
            return l_data
        }
    }
    return nil
}

func (p *AnalysePkt) GetTCPLayer() (*layers.TCP) {
    if res, l := p.IsTCPPkt(); res {
        if l_data, err := (*l).(*layers.TCP); err  {
            return l_data
        }
    }
    return nil
}

func (p *AnalysePkt) GetUDPLayer() (*layers.UDP) {
    if res, l := p.IsUDPPkt(); res {
        if l_data, err := (*l).(*layers.UDP); err  {
            return l_data
        }
    }
    return nil
}

func (p *AnalysePkt) GetTcpPortNumbers() (bool, int, int) {
	if transport_layer := p.GetTCPLayer(); transport_layer != nil {
		return true, int(transport_layer.SrcPort), int(transport_layer.DstPort)
	}
	return false, 0, 0
}

func (p *AnalysePkt) GetUdpPortNumbers() (bool, int, int) {
	if transport_layer := p.GetUDPLayer(); transport_layer != nil {
		return true, int(transport_layer.SrcPort), int(transport_layer.DstPort)
	}
	return false, 0, 0
}

func (p *AnalysePkt) GetPortNumbers() (bool, int, int) {

    var res bool
    var src_port, dst_port int

    res, src_port, dst_port = p.GetTcpPortNumbers()
    if res {
	    return res, src_port, dst_port
    }

    return p.GetUdpPortNumbers()
}

func (p *AnalysePkt) IsLavelleTunnelFromTo(src_ip string, dst_ip string) bool {
    return (p.IsIPv4PktFromTo(src_ip, dst_ip) && p.GetLavelleTunnelLayer() != nil)
}

func (p *AnalysePkt) IsLavelleTunnelBetween(ip1 string, ip2 string) bool {
    return p.IsLavelleTunnelFromTo(ip1, ip2) || p.IsLavelleTunnelFromTo(ip2, ip1)
}

func (p *AnalysePkt) IsIPv4PktFromTo(src_ip string, dst_ip string) bool {
    if ip_l := p.GetIPLayer(); ip_l != nil {
         return (ip_l.SrcIP.String() == src_ip) && (ip_l.DstIP.String() == dst_ip)
    }
    return false
}

func (p *AnalysePkt) IsLavelleTunnelDataPktFromTo(tunnel_src_ip, tunnel_dst_ip, data_src_ip, data_dst_ip string) bool {

    /*if p.IsLavelleTunnelDataPkt() && p.IsLavelleTunnelFromTo(tunnel_src_ip, tunnel_dst_ip) {
        lavelle_tunnel := p.GetLavelleTunnelLayer()
        inner_data := lavelle_tunnel.LayerPayload()
        inner_pkt := gopacket.NewPacket(inner_data, lavelle_tunnel.NextLayerType(), gopacket.Default)
        inner_analysepkt := AnalysePkt{gopkt : inner_pkt}
        return inner_analysepkt.IsIPv4PktFromTo(data_src_ip, data_dst_ip)
    }
    return false
    */
    if analyse := p.GetLavelleTunnelDataPkt(tunnel_src_ip, tunnel_dst_ip); analyse != nil {
        return analyse.IsIPv4PktFromTo(data_src_ip, data_dst_ip)
    }
    return false
}

func (p *AnalysePkt) GetLavelleTunnelDataPkt(tunnel_src_ip, tunnel_dst_ip string) *AnalysePkt {

    if p.IsLavelleTunnelDataPkt() && p.IsLavelleTunnelFromTo(tunnel_src_ip, tunnel_dst_ip) {
        lavelle_tunnel := p.GetLavelleTunnelLayer()
        inner_data := lavelle_tunnel.LayerPayload()
        inner_pkt := gopacket.NewPacket(inner_data, lavelle_tunnel.NextLayerType(), gopacket.Default)
        inner_analysepkt := AnalysePkt{gopkt : inner_pkt}
        return &inner_analysepkt
    }
    return nil
}


/* GRE related */
func (p *AnalysePkt) GetGRETunnelLayer() (*layers.GRE) {

    if res, l := p.IsGREPkt(); res {
        if l_data, err := (*l).(*layers.GRE); err {
            return l_data
        }
    }
    return nil
}

func (p *AnalysePkt) IsGRETunnelFromTo(src_ip string, dst_ip string) bool {
    return (p.IsIPv4PktFromTo(src_ip, dst_ip) && p.GetGRETunnelLayer() != nil)
}

func (p *AnalysePkt) IsGRETunnelBetween(src_ip string, dst_ip string) bool {
    return p.IsGRETunnelFromTo(src_ip, dst_ip) || p.IsGRETunnelFromTo(dst_ip, src_ip)
}

func (p *AnalysePkt) GetGRETunnelDataPkt(tunnel_src_ip, tunnel_dst_ip string) (*AnalysePkt) {
    if is_gre_pkt, _ := p.IsGREPkt(); is_gre_pkt && p.IsGRETunnelFromTo(tunnel_src_ip, tunnel_dst_ip) {
        gre_tunnel := p.GetGRETunnelLayer()
        inner_data := gre_tunnel.LayerPayload()
        inner_pkt := gopacket.NewPacket(inner_data, gre_tunnel.NextLayerType(), gopacket.Default)
        inner_analysepkt := AnalysePkt{gopkt : inner_pkt}
        return &inner_analysepkt
    }
    return nil
}

func (p *AnalysePkt) IsGRETunnelDataPktFromTo(tunnel_src_ip, tunnel_dst_ip, data_src_ip, data_dst_ip string) bool {

    inner_analysepkt := p.GetGRETunnelDataPkt(tunnel_src_ip, tunnel_dst_ip)
    if inner_analysepkt != nil {
        return inner_analysepkt.IsIPv4PktFromTo(data_src_ip, data_dst_ip)
    }
    return false
}

/*
func (p *AnalysePkt) GetGRETunnelDataPktInfo() (src_ip string, dst_ip string, protcol int, icmp_type int, src_port int, dst_port int) {
    if gre_pkt := p.GetGRETunnelDataPkt(); gre_pkt != nil {
        if ip_l := gre_pkt.GetIPLayer(); ip_l != nil {
            src_ip = ip_l.SrcIP.String()
            dst_ip = ip_l.DstIP.String()
        }
    }
    return nil
}
*/
