package lvnanalyser

import (
    "fmt"
    //"github.com/google/gopacket"
    "strings"
    "github.com/google/gopacket/layers"
    "regexp"
    lvn_log "lvnautomation/lvnlog"
)

type GRETunnelInfo struct {
    pkt_count int
    src_ips map[string][]map[string]int
}

type LVNTunnelInfo struct {
}

var (
        icmp_proto string = "ICMPv4"
        tcp_proto string = "TCP"
        udp_proto string = "UDP"
        gre_proto string = "GRE"
        ip_proto string = "IPv4"
        echo_request string = "EchoRequest"
        echo_reply string = "EchoReply"
	icmp_dest_unreachable_port string = "DestinationUnreachable(Port)"
        ssh_port string = "22"
        http_port string = "80"
        https_port string = "443"
        lntun_port string = "7007"
        lntun_pkt_type_data string = "1"
        lntun_pkt_type_ctrl string = "2"

        lntun_ctrl_type_hbeat string = "1"
        lntun_ctrl_type_hbeatack string = "2"
        lntun_ctrl_type_rttprobe string = "6"
        lntun_ctrl_type_hshake string = "3"
)

type PlainTrafficInfo struct {

	//pkt_count int
	//src_ips map[string][]map[string]int

	//icmps map[string]
}

/* 
"srcip-dstip-proto-"
1.1.1.1-1.1.1.2-17-31016-7007-10.5.0.1-8.8.8.8-1-5000-80
1.1.1.1-1.1.1.2-47-10.5.0.1-8.8.8.8-1-5000-90
*/
type DumpAnalyser struct {

    pkts []string
    no_of_packets uint32
    stats map[string]int

    //src_ips map[string]int
    //dst_ips map[string]int
    src_dst_ips map[string]int
    src_dst_ips_proto map[string]int
    //proto map[string]int
    //tcp map[string]int
    //udp map[string]int
    //lntun map[string]int
    src_dst_ips_gre map[string]int
    src_dst_ips_gre_src_dst_ips map[string]int

    src_dst_ips_lntun map[string]int
    src_dst_ips_lntun_ctrl map[string]int
    src_dst_ips_lntun_data map[string]int

    /* 
    AnalysePlainTraffic bool
    AnalyseLNTunTraffic bool
    AnalyseGRETraffic bool

    IsLavelleTunnelPktsFound bool
    LavelleTunnelPktsCount uint32

    IsLavelleTunnelCtrlPktsFound bool
    LavelleTunnelCtrlPktsCount uint32

    IsLavelleTunnelDataPktsFound bool
    LavelleTunnelDataPktsCount uint32

    IsGreTunnelPktsFound bool
    GreTunnelPktsCount uint32

    ICMPPktsFound bool
    ICMPPktsCount uint32

    TCPPktsFound bool
    TCPPktsCount uint32

    UDPPktsFound bool
    UDPPktsCount uint32

    ICMP
    */
}

type emptystruct struct{}
func NewDumpAnalyser(pkts  []string) *DumpAnalyser {
    var d DumpAnalyser
    d.pkts = pkts
    d.no_of_packets = uint32(len(d.pkts))
    d.stats = make(map[string]int)
    d.analyse()
    return &d
}

func (d *DumpAnalyser) NoOfPkts() uint32 {
	return d.no_of_packets
}


func (d *DumpAnalyser) IcmpTrafficFromTo(src_ip, dst_ip string) int {
   return 0
}

func (d *DumpAnalyser) analyse() bool {

	KnownPorts  := make(map[int]emptystruct)
	KnownPorts[21] = emptystruct{}
	KnownPorts[22] = emptystruct{}
	KnownPorts[23] = emptystruct{}
	KnownPorts[53] = emptystruct{}
	KnownPorts[67] = emptystruct{}
	KnownPorts[68] = emptystruct{}
	KnownPorts[80] = emptystruct{}
	KnownPorts[123] = emptystruct{}
	KnownPorts[443] = emptystruct{}
	KnownPorts[502] = emptystruct{}
	KnownPorts[546] = emptystruct{}
	KnownPorts[547] = emptystruct{}
	KnownPorts[623] = emptystruct{}
	KnownPorts[636] = emptystruct{}
	KnownPorts[989] = emptystruct{}
	KnownPorts[990] = emptystruct{}
	KnownPorts[992] = emptystruct{}
	KnownPorts[993] = emptystruct{}
	KnownPorts[994] = emptystruct{}
	KnownPorts[995] = emptystruct{}
	KnownPorts[2152] = emptystruct{}
	KnownPorts[3784] = emptystruct{}
	KnownPorts[4789] = emptystruct{}
	KnownPorts[5005] = emptystruct{}
	KnownPorts[5006] = emptystruct{}
	KnownPorts[5060] = emptystruct{}
	KnownPorts[5061] = emptystruct{}
	KnownPorts[6081] = emptystruct{}
	KnownPorts[6343] = emptystruct{}
	KnownPorts[7007] = emptystruct{}

    for _, pkt := range d.pkts {
	    //fmt.Printf("Pkt [%d]", idx)
	    //fmt.Println("==========")
        pkt_analyser := NewPktAnalyser(pkt)
	go_pkt := pkt_analyser.GetGoPkt()

	key := []string{}
	for _, layer := range go_pkt.Layers() {
           if layer.LayerType() == layers.LayerTypeIPv4 {
		   ipv4 := layer.(*layers.IPv4)
	    //fmt.Printf("IPv4 pkt %s-->%s %s", ipv4.SrcIP.String(), ipv4.DstIP.String(), fmt.Sprintf("%s", ipv4.Protocol))
		   key = append(key, ipv4.SrcIP.String())
		   key = append(key, ipv4.DstIP.String())
		   key = append(key, fmt.Sprintf("%s", ipv4.Protocol))
		   ///ipv4.DstIP.String()
	   }

           if layer.LayerType() == layers.LayerTypeICMPv4 {
		   icmpv4 := layer.(*layers.ICMPv4)
	    //fmt.Println("ICMP pkt", icmpv4.TypeCode)
		   key = append(key, fmt.Sprintf("%s", icmpv4.TypeCode))
		   ///ipv4.DstIP.String()
	   }

           if layer.LayerType() == layers.LayerTypeTCP {
		   tcp := layer.(*layers.TCP)
		   src_port := int(tcp.SrcPort)
		   dst_port := int(tcp.DstPort)
		   if _, ok := KnownPorts[src_port]; ok {
		       key = append(key, fmt.Sprintf("%d", src_port))
	           } else if _, ok = KnownPorts[dst_port]; ok {
		       key = append(key, fmt.Sprintf("%d", dst_port))
	           } else {
		       key = append(key, fmt.Sprintf("%d", src_port))
		       key = append(key, fmt.Sprintf("%d", dst_port))
		   }
	   }

           if layer.LayerType() == layers.LayerTypeUDP {
		   udp := layer.(*layers.UDP)
		   src_port := int(udp.SrcPort)
		   dst_port := int(udp.DstPort)
		   if _, ok := KnownPorts[src_port]; ok {
		       key = append(key, fmt.Sprintf("%d", src_port))
	           } else if _, ok = KnownPorts[dst_port]; ok {
		       key = append(key, fmt.Sprintf("%d", dst_port))
	           } else {
		       key = append(key, fmt.Sprintf("%d", src_port))
		       key = append(key, fmt.Sprintf("%d", dst_port))
		   }
	   }

           if layer.LayerType() == layers.LayerTypeGRE {
	    //fmt.Println("GRE pkt")
		   gre := layer.(*layers.GRE)
                   key = append(key, fmt.Sprintf("%s", gre.Protocol))
                   //key = append(key, fmt.Sprintf("%s", gre.Protocol))
	   }

           if layer.LayerType() == layers.LayerTypeLNTun {
		   lntun := layer.(*layers.LNTun)
                   key = append(key, fmt.Sprintf("%d", int(lntun.PacketType)))
		   if lntun.LNTunCtrlMsg != nil {
                       key = append(key, fmt.Sprintf("%d", int(lntun.LNTunCtrlMsg.ControlType)))
		   }
	   }

	}

	if len(key) == 0 {
		key = []string{"others"}
	}

	key2 := strings.Join(key, "-")
	//fmt.Println(key2)
	if _, ok := d.stats[key2]; ok {
		d.stats[key2] += 1
	} else {
		d.stats[key2]  = 1
	}

	/*
	for _, layer := range go_pkt.Layers() {
            //layers = append(layers, layer.LayerType())
           if layer.LayerType() == layers.LayerTypeIPv4 {
		   ip_layers_count += 1
	   }

           if layer.LayerType() == layers.LayerTypeGRE {
		   gre_layers_count += 1
	   }

           if layer.LayerType() == layers.LayerTypeICMPv4 {
		   icmp_count += 1
	   }

           if layer.LayerType() == layers.LayerTypeLNTun {
		   lntun_count += 1
	   }

           if layer.LayerType() == layers.LayerTypeTCP {
		   tcp_count += 1
	   }

           if layer.LayerType() == layers.LayerTypeUDP {
		   udp_count += 1
	   }
	}

	if ip_layers_count == 1 {
	}

	if ip_layers_count == 2 {
		if gre_layers_count == 1 {

		}
	}*/

    }
    for k, v := range d.stats {
        //fmt.Println(k, v)
	lvn_log.Debug.Printf("Pkt %s, Count %d", k, v)
    }
    return true
}

func ConvertStringToPktList (b string) []string {
    /*file, err := os.Open(filename)
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()


   b, err := ioutil.ReadAll(file) */
   //re :=regexp.MustCompile(`(\d\d:\d\d:\d\d.\d\d\d\d\d\d(.*?)\n([0-9x\sa-fA-F\n\:]*)?)(\d\d:\d\d:\d\d.\d\d\d\d\d\d)?`)
   re :=regexp.MustCompile(`(\d\d:\d\d:\d\d.\d\d\d\d\d\d(.*?)\n([0-9x\sa-fA-F\n\:]*))(\d\d:\d\d:\d\d.\d\d\d\d\d\d)?`)
   re2 := regexp.MustCompile(`\d\d:\d\d:\d\d`)
   re3 := regexp.MustCompile(`\s+`)

   x := re.FindAllStringSubmatch(string(b), -1)
   //fmt.Println(len(x))

   pkts_string := []string{}
   for _, y := range x {
           hex2 := ""
           z :=  strings.TrimSpace( re2.Split(y[3], -1)[0])
           z1 := strings.Split(z, "\n")
           for _, i := range z1 {
                   hex2 = hex2 + strings.Join(re3.Split(strings.TrimSpace(i), -1)[1:], "")
           }
           pkts_string = append(pkts_string, hex2)
    }
    //fmt.Println("length of pkts: ", len(pkts_string))
    return  pkts_string
}


func (d *DumpAnalyser) EchoRequestBetween(src_ip, dst_ip string) int {
    key_list := []string{src_ip, dst_ip, icmp_proto, echo_request}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) EchoReplyBetween(src_ip, dst_ip string) int {
    key_list := []string{src_ip, dst_ip, icmp_proto, echo_reply}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) Http(src_ip, dst_ip string) int {
    key_list := []string{src_ip, dst_ip, tcp_proto, http_port}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        //fmt.Println(count)
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) Https(src_ip, dst_ip string) int {
    key_list := []string{src_ip, dst_ip, tcp_proto, https_port}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) GRETunnelDataEchoRequest(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, gre_proto, ip_proto, src_ip, dst_ip, icmp_proto, echo_request}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) GRETunnelDataEchoReply(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, gre_proto, ip_proto, src_ip, dst_ip, icmp_proto, echo_reply}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) GRETunnelUDPData(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string, src_port, dst_port int) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, gre_proto, ip_proto, src_ip, dst_ip, udp_proto, fmt.Sprintf("%d", src_port), fmt.Sprintf("%d", dst_port)}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) GRETunnelTCPData(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string, src_port, dst_port int) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, gre_proto, ip_proto, src_ip, dst_ip, tcp_proto, fmt.Sprintf("%d", src_port), fmt.Sprintf("%d", dst_port)}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) GRETunnelHttps(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, gre_proto, ip_proto, src_ip, dst_ip, tcp_proto, https_port}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}


func (d *DumpAnalyser) GRETunnelHttp(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, gre_proto, ip_proto, src_ip, dst_ip, tcp_proto, http_port}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) LNTunDataEchoRequest(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, udp_proto, lntun_port, lntun_pkt_type_data, src_ip, dst_ip, icmp_proto, echo_request}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) LNTunDataEchoReply(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, udp_proto, lntun_port, lntun_pkt_type_data, src_ip, dst_ip, icmp_proto, echo_reply}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) LNTunDataTCPData(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string, src_port int, dst_port int) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, udp_proto, lntun_port, lntun_pkt_type_data, src_ip, dst_ip, tcp_proto, fmt.Sprintf("%d", src_port), fmt.Sprintf("%d", dst_port)}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) LNTunDataHttps(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, udp_proto, lntun_port, lntun_pkt_type_data, src_ip, dst_ip, tcp_proto, https_port}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}


func (d *DumpAnalyser) LNTunDataHttp(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, udp_proto, lntun_port, lntun_pkt_type_data, src_ip, dst_ip, tcp_proto, http_port}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) LNTunDataSSH(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, udp_proto, lntun_port, lntun_pkt_type_data, src_ip, dst_ip, tcp_proto, ssh_port}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) LNTunDataUDPData(tunnel_src_ip, tunnel_dst_ip, src_ip, dst_ip string, src_port int, dst_port int) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, udp_proto, lntun_port, lntun_pkt_type_data, src_ip, dst_ip, udp_proto, fmt.Sprintf("%d", src_port), fmt.Sprintf("%d", dst_port)}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) LNTunHbeat(tunnel_src_ip, tunnel_dst_ip string) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, udp_proto, lntun_port, lntun_pkt_type_ctrl, lntun_ctrl_type_hbeat}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
	return 0
    }
}

func (d *DumpAnalyser) LNTunHbeatAck(tunnel_src_ip, tunnel_dst_ip string) int {
    key_list := []string{tunnel_src_ip, tunnel_dst_ip, udp_proto, lntun_port, lntun_pkt_type_ctrl, lntun_ctrl_type_hbeatack}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) LNTunHandshake(tunnel_src_ip, tunnel_dst_ip string) int {

    key_list := []string{tunnel_src_ip, tunnel_dst_ip, udp_proto, lntun_port, lntun_pkt_type_ctrl, lntun_ctrl_type_hshake}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}

func (d *DumpAnalyser) LNTunRTTProbe(tunnel_src_ip, tunnel_dst_ip string) int {

    key_list := []string{tunnel_src_ip, tunnel_dst_ip, udp_proto, lntun_port, lntun_pkt_type_ctrl, lntun_ctrl_type_rttprobe}
    key := strings.Join(key_list, "-")
    //fmt.Println(key)
    if count, ok := d.stats[key]; ok {
        return count
    } else {
        return 0
    }
}
