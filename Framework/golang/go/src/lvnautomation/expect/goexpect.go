package main

import (
	//"flag"
	"fmt"
	"log"
	"regexp"
	"time"

	"golang.org/x/crypto/ssh"

	//"google.golang.org/grpc/codes"

	"github.com/google/goexpect"
	"github.com/google/goterm/term"
)

const (
	timeout = 10 * time.Minute
	timeout1 = 10 * time.Second
)

//var (
//	addr  = flag.String("address", "", "address of telnet server")
//	user  = flag.String("user", "user", "username to use")
//	pass1 = flag.String("pass1", "pass1", "password to use")
//	pass2 = flag.String("pass2", "pass2", "alternate password to use")
//)
var (
	u string
	a string
	p1 string
	//p2 string
)

type sshClient struct {
	User string
	Pass string
	Host string
	Port int
	loginpasswordprompt string
	Prompt string
	RootUserPrompt string
	TimeoutSeconds int
	InSudoMode bool
	SudoPasswordPrompt string
}

func NewSSHClient() *sshClient {
    var sshclient sshClient
    sshclient.User = "lavelle"
    sshclient.Pass = "lavelle@3163"
    sshclient.Port = 23
    sshclient.loginpasswordprompt = `.*password:`
    sshclient.Prompt = `lavelle@lavelle\:\~\$`
    sshclient.RootUserPrompt= `root@lavelle`
    sshclient.TimeoutSeconds = 30
    sshclient.InSudoMode = false
    sshclient.SudoPasswordPrompt = `.*password`
}

func (n *NewSSHClient) timeout() time.Duration {
	return n.TimeoutSeconds * time.Second
}

func (n *NewSSHClient) Login() {

	sshClt, err := ssh.Dial("tcp", n.Host, &ssh.ClientConfig{
		User:            n.User,
		Auth:            []ssh.AuthMethod{ssh.Password(n.Pass)},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	})

	if err != nil {
		log.Fatalf("ssh.Dial(%q) failed: %v", n.Host, err)
	}

	e, _, err := expect.SpawnSSH(sshClt, timeout)

	if err != nil {
		log.Fatal(err)
	}

	o, _ := e.ExpectBatch([]expect.Batcher{
		&expect.BCas{[]expect.Caser{
			&expect.Case{R: regexp.MustCompile(n.Prompt), T: expect.OK()},
			&expect.Case{R: regexp.MustCompile(n.loginpasswordprompt), S: *pass1, T: expect.Next(), Rt: 1},
		}},
	}, n.timeout())
}

func (n *NewSSHClient) SudoMode() {

	if !n.InSudoMode {
		n.Send("sudo bash")
                o1, m1, e1 := e.Expect(regexp.MustCompile(n.SudoPasswordPrompt), n.timeout())
		//fmt.Println("====", o1, "====", m1)
        
		n.Send(n.Pass)
		o1, m1, e1 = e.Expect(regexp.MustCompile(n.RootUserPrompt), n.timeout())
		fmt.Println("====", o1, "====", m1)
		n.InSudoMode = true
	}
	sshClt.Close()
	e.Close()

}

func (n *NewSSHClient) ExitSudoMode() {

	if n.InSudoMode {
		n.Send("exit")
		o1, m1, e1 = e.Expect(regexp.MustCompile(`Connection closed`), n.timeout())
		fmt.Println(o1, m1)
		if e1 != nil {
		}
	}
}

func (n *NewSSHClient) Logout() {

	n.Send("exit")
	o1, m1, e1 = e.Expect(regexp.MustCompile(`Connection closed`), n.timeout())

	sshClt.Close()
	e.Close()

}

func (n *NewSSHClient) Send(s string) {
	e.Send("%s\n", s)
}

func (n *NewSSHClient) SendAndExpect(s string) {
	n.Send(s)
}


func main() {
	//flag.Parse()

	o, _ := e.ExpectBatch([]expect.Batcher{
		&expect.BCas{[]expect.Caser{
			&expect.Case{R: regexp.MustCompile(`lavelle@lavelle\:\~\$`), T: expect.OK()},
			&expect.Case{R: regexp.MustCompile(`.*password:`), S: *pass1, T: expect.Next(), Rt: 1},
		}},
	}, timeout)

	fmt.Println(o)
	c := 1
	var x string
	for c <=0 {
        c++
	fmt.Println(c)
	time.Sleep(10*time.Second)
	e.Send("ifconfig\n")
	x, _, _= e.Expect(regexp.MustCompile(`lavelle@lavelle\:\~\$`), timeout)
	}
	fmt.Println(x)

	e.Send("sudo bash\n")
	//o, _ = e.ExpectBatch([]expect.Batcher{
	//	&expect.BCas{[]expect.Caser{
	//		&expect.Case{R: regexp.MustCompile(`root@lavelle\:\~#`), T: expect.OK()},
	//		&expect.Case{R: regexp.MustCompile(`.*password`), S: *pass1, T: expect.Next(), Rt: 1},
//
//		}},
//	}, timeout1)
o1, m1, e1 := e.Expect(regexp.MustCompile(`.*password`), timeout)
	fmt.Println("====", o1, "====", m1)
	e.Send(fmt.Sprintf("%s\n", p1))
	o1, m1, e1 = e.Expect(regexp.MustCompile(`root@lavelle`), timeout)
	fmt.Println("====", o1, "====", m1)

	//fmt.Println(o1)
	//fmt.Println(m)
	e.Send("exit\n")
	o1, m1, e1 = e.Expect(regexp.MustCompile(`lavelle@lavelle\:\~\$`), timeout)
	fmt.Println("====", o1, "====", m1)
	e.Send("exit\n")
	o1, m1, e1 = e.Expect(regexp.MustCompile(`Connection closed`), timeout)
	fmt.Println("====", o1, "====", m1)
	if e1 != nil {
	}

	fmt.Println(term.Greenf("All done"))
}
