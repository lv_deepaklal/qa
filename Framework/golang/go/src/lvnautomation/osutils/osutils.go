/*
	func Ping(ip_address string,interface_Name string,pck_Count string,interval string)
	func DiskSpace()
	func Ifconfig(interface_name string)
	func ShowRoute()
	func AddRoute(network string, netmask int, gateway string)
	func Cat(file_name string)
	func Tail(file_name string, line string)
        func Top_proc()
	func GetCurrentDirectory()
	func AppendContentToFile(file string)
	func Makedir(file string)
	func MakeNestedDir(nesdirec_name string)

*/

package osutils

import (
	"fmt"
	"math"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"lvnautomation/utils"
	lvn_log "lvnautomation/lvnlog"

)

/*
func xmain() {

	ip := "8.8.8.8"
	ping_iface := ""
	count := "5"
	interval := ""
	//NPktTrans, NPktRcv, PktLoss, Time, MinRtt, AvgRtt, MaxRtt, dev :=
	out, Ptrans, Prcv, Ploss, MinRtt, AvgRtt, MaxRtt, Mdev, TotalTime, err_ping := Ping(ip, ping_iface, count, interval)
	if err_ping != nil {

		fmt.Println(out, err_ping)
		return
	}
	fmt.Println(out)
	fmt.Println("Packet Transmitter: ", Ptrans)
	fmt.Println("Packet Recieved: ", Prcv)
	fmt.Println("Packet Loss: ", Ploss)
	fmt.Println("Min RTT: ", MinRtt)
	fmt.Println("Max Rtt: ", MaxRtt)
	fmt.Println("Avg RTT: ", AvgRtt)
	fmt.Println("Std Dev: ", Mdev)
	fmt.Println("Total Time: ", TotalTime)
	//-----------------------------------------------------------
	iface := "wlp1s0"
	ip, mac, err_ifconfig := Ifconfig(iface)
	if err_ifconfig != nil {
		fmt.Println(err_ifconfig)
		return
	}
	//	fmt.Println(out)
	fmt.Println("IP Address: ", ip)
	fmt.Println("Mac Address: ", mac)

	//-------------------------------------------------------------
	result, err_showroute := ShowRoute()
	if err_showroute != nil {
		fmt.Println(result, err_showroute)
	}

	for x := range result {
		fmt.Println(result[x])
	}
	//--------------------------------------------------------------
	network := "171.11.12.1"
	netmask := 7
	gw := "10.0.0.1"
	out_addroute, err_addroute := AddRoute(network, netmask, gw)
	if err_addroute != nil {
		fmt.Println(err_addroute)
	}
	fmt.Println(out_addroute)

	//--------------------------------------------------------------

	file_name := "cat.go"
	out_cat, err_cat := cat(file_name)
	if err_cat != nil {
		fmt.Println(err_cat)
	}
	fmt.Println(out_cat)
	//--------------------------------------------------------------
	filename := "cat.go"
	NoLine := "5"
	out_tail, err_tail := tail(NoLine, filename)
	if err_tail != nil {
		fmt.Println(err_tail)
	}
	fmt.Println(out_tail)
	//..............................................................
	out_curdir,err_curdir := GetCurrentDirectory()
	if err_curdir != nil {
                fmt.Println(err_curdir)
        }
        fmt.Println(out_curdir)
	//..............................................................
	out_append,err_append := AppendContentToFile(content,file)
        if err_append != nil {
                fmt.Println(err_append)
        }
        fmt.Println(out_append)
        //..............................................................
	out_mkdir,err_mkdir := mkdir(direc_name)
        if err_mkdir != nil {
                fmt.Println(err_mkdir)
        }
        fmt.Println(out_mkdir)
        //...............................................................
        result_ds, err_ds := DiskSpace()
	if err_ds != nil {
		fmt.Println(result_ds, err_ds)
	}
	for x := range result_ds {
		fmt.Println(result_ds[x])
	}
}
*/

func Ping(ip string, count string, interval string) (string, string, string, string, string, string, string, string, string, error) {

	lvn_log.Debug.Println(utils.GetFuncName())
	lvn_log.Debug.Println(" Parameters: ip %s", ip)
	if _, err := utils.ValidateIP(ip); err != nil {
		return "", "", "", "", "", "", "", "", "ip validation fail", err

	}
	var flags = map[string]string{}
	flags["c"] = count
	flags["i"] = interval
	//fmt.Println("valid ip")
	args := []string{"ping", ip}
	for key, value := range flags {
		if value != "" {
			x := "-" + key + " " + value

			args = append(args, x)
		}
	}

	fmt.Println(args)
	lvn_log.Debug.Println(args)

	output, err3 := execute("ping", args)
	if err3 != nil {
	        lvn_log.Debug.Printf("Received from execute err %s", err3)
		return output, "", "", "", "", "", "", "", "", err3
	}
	lvn_log.Debug.Printf("Received from execute output %s", output)

	//fmt.Println("\n\n")
	//fmt.Println("------------------------------------------------")
	re := regexp.MustCompile(`([0-9]+)% packet loss`)
	PktLoss := re.Find([]byte(output))
	//fmt.Println(string(PktLoss))
	//fmt.Println("------------------------------------------------")
	re = regexp.MustCompile(`([0-9]+) received`)
	PktRcv := re.Find([]byte(output))
	//fmt.Println(string(PktRcv))
	//fmt.Println("------------------------------------------------")
	re = regexp.MustCompile(`([0-9]+) packets transmitted`)
	PktTrans := re.Find([]byte(output))
	//fmt.Println(string(PktTrans))
	//fmt.Println("------------------------------------------------")
	re = regexp.MustCompile(`min\/avg\/max\/mdev = ([.0-9]+)\/([.0-9]+)\/([.0-9]+)\/(.*)`)
	Rtt := re.Find([]byte(output))
	//fmt.Println(string(Rtt))
	//fmt.Println("------------------------------------------------")
	re = regexp.MustCompile(`time ([0-9]+ms)`)
	Time := re.Find([]byte(output))
	//fmt.Println(string(time))
	//fmt.Println("------------------------------------------------")

	PacketTranslitted := strings.Split(string(PktTrans), " ")
	PacketRecieved := strings.Split(string(PktRcv), " ")
	PacketLoss := strings.Split(string(PktLoss), " ")
	RTime := strings.Split(string(Rtt), " ")
	RoundTime := strings.Split(string(RTime[2]), "/")
	TTime := strings.Split(string(Time), " ")
	lvn_log.Debug.Printf("Returning output %s, PktTransnitted %s", output, PacketTranslitted[0])
	lvn_log.Debug.Printf("Exiting ping functon...")

	return output, PacketTranslitted[0], PacketRecieved[0], PacketLoss[0], RoundTime[0], RoundTime[1], RoundTime[2], RoundTime[3], TTime[1], nil
}

func DiskSpace() ([]map[string]string, error) {
	lvn_log.Debug.Println(utils.GetFuncName())
	args := []string{"df", "--output=source,pcent"}
        lvn_log.Debug.Println(args)
	out, err1 := execute("df", args)
	r := []map[string]string{}
	if err1 != nil {
		return r, err1
                lvn_log.Debug.Printf("Received from execute err %s", err1)
	}
        lvn_log.Debug.Printf("Received from execute output %s", out)
	//fmt.Println(out)
	pathtemp := strings.Split(string(out), "\n")
	//fmt.Println(pathtemp)
	//length := len(pathtemp)
	//a:= []string{}
	//for i :=0; i<length; i++ {
	//	if(pathtemp[i] != ""){
	//		a=append(a,pathtemp[i])
	//		}
	//}
	//lengtha:=len(a)
	//fmt.Println(lengtha)
	var diskspace = []map[string]string{}
	for k:=1;k<len(pathtemp)-1;k++{
		a:= strings.Fields(pathtemp[k])
	//	fmt.Println(a[0],a[1])
		x := map[string]string{
		"filesystem": "",
		"used": "",
		}
		x["filesystem"] = a[0]
		x["used"] = a[1]
		diskspace = append(diskspace, x)
		//fmt.Println(pathtemp[k])
	}
	lvn_log.Debug.Printf("Returning output %s, diskspace %s", out, diskspace)
        lvn_log.Debug.Printf("Exiting DiskSpace functon...")
	return diskspace, nil
}

func Ifconfig(iface string) (string, string, error) {

	lvn_log.Debug.Println(utils.GetFuncName())
        lvn_log.Debug.Println(" Parameters: iface %s",iface)
	args := []string{"ifconfig", iface}
	lvn_log.Debug.Println(args)
	cmd, err := execute("ifconfig", args)
	if err != nil {
		return "", "", err
		lvn_log.Debug.Printf("Received from execute err %s", err)
	}
	lvn_log.Debug.Printf("Received from execute output %s", cmd)
	//fmt.Println(cmd)
	re := regexp.MustCompile(`inet [0-9]+(\.[0-9]+)+`)
	iptemp := re.Find([]byte(cmd))
	//fmt.Printf(string(iptemp))

	re = regexp.MustCompile(`ether [a-fA-F0-9:]{17}|[a-fA-F0-9]{12}`)
	mactemp := re.Find([]byte(cmd))
	//fmt.Println(string(mactemp))

	ip := strings.Split(string(iptemp), " ")
	mac := strings.Split(string(mactemp), " ")
	lvn_log.Debug.Printf("Returning output %s, ip %s, mac %s", cmd, ip[1],mac[1])
        lvn_log.Debug.Printf("Exiting Ifconfig functon...")
	return ip[1], mac[1], nil
}


func ShowRoute() ([]map[string]string, error) {
	lvn_log.Debug.Println(utils.GetFuncName())
	args := []string{"route", "-n"}
	lvn_log.Debug.Println(args)
	out, err1 := execute("route", args)
	r := []map[string]string{}
	if err1 != nil {
		return r, err1    
		lvn_log.Debug.Printf("Received from execute err %s", err1)
	}
        lvn_log.Debug.Printf("Received from execute output %s", out)
	re := regexp.MustCompile(`[0-9]+(\.[0-9]+)+`)
	result := re.FindAllString(out, -1)
	length := len(result)
	//l := int(length / 3)
	var RouteInfo = []map[string]string{}
	j := 0
	for i := 0; i < length/3; i++ {
		x := map[string]string{
			"network": "",
			"netmask": "",
			"gateway": "",
	 	}
		x["network"] = result[j]
		x["netmask"] = result[j+1]
		x["gateway"] = result[j+2]
		j = j + 3
		RouteInfo = append(RouteInfo, x)
	}
        lvn_log.Debug.Printf("Returning output routeinfo %s", RouteInfo)
	lvn_log.Debug.Printf("Exiting ShowRoute functon...")
	return RouteInfo, nil
}

func AddRoute(nw string, msk int, gw string) (string, error) {

	lvn_log.Debug.Println(utils.GetFuncName())
        lvn_log.Debug.Println(" Parameters: nw %s, msk %s, gw %s",nw,msk,gw)
	if nw == "" || msk == 0 || gw == "" {
		return "", fmt.Errorf("Insufficient info")
	}

	if _, err1 := utils.ValidateIP(nw); err1 != nil {
		return "", fmt.Errorf("Ip validation fail")
	}
	if msk <= 0 || msk > 32 {
		return "", fmt.Errorf("netmask validation fail")
	}

	if _, err3 :=utils.ValidateIP(gw); err3 != nil {
		return "", fmt.Errorf("gateway validation fail")
	}
	dot_mask := mask_conv(msk)
	args := []string{"route", "add"}
	args = append(args, "-net", nw, "netmask", dot_mask, "gw", gw)
        lvn_log.Debug.Println(args)

	result, err4 := execute("route", args)
	if err4 != nil {

		return result, err4
		lvn_log.Debug.Printf("Received from execute err %s", err4)

	}
        
	lvn_log.Debug.Printf("Received from execute output %s", result)
        lvn_log.Debug.Printf("Exiting AddRoute functon...")
	return result, nil
}
func mask_conv(cidr int) string {
	if cidr < 0 || cidr > 32 {
		return "invalid length"
	}

	mask := ""
	for t := 0; t < 4; t++ {
		if cidr >= 8 {
			mask += "255."
		} else if cidr > 0 && cidr < 8 {
			dec := 255 - (math.Pow(2, 8-float64(cidr)) - 1)
			mask += strconv.Itoa(int(dec)) + "."

		} else {

			mask += "0."
			cidr = 0

		}
		cidr -= 8
	}
	mask = strings.TrimSuffix(mask, ".")

	return mask

}

func Cat(file string) (string, error) {
	lvn_log.Debug.Println(utils.GetFuncName())
        lvn_log.Debug.Println(" Parameters: file %s",file)
	args := []string{"cat"}
	args = append(args, file)
	lvn_log.Debug.Println(args)
	out, err := execute("cat", args)
	if err != nil {
		return out, err
		lvn_log.Debug.Printf("Received from execute err %s",err)
	}
	lvn_log.Debug.Printf("Received from execute output %s", out)
        lvn_log.Debug.Printf("Exiting cat functon...")
	return out, nil
}

func Tail(lines string, file string) (string, error) {
	lvn_log.Debug.Println(utils.GetFuncName())
        lvn_log.Debug.Println(" Parameters: lines %s,file %s",lines,file)
	args := []string{"tail"}
	l := "-"
	l = l + lines
	args = append(args, l, file)
	lvn_log.Debug.Println(args)
	out, err := execute("tail", args)
	if err != nil {
		return out, err
		lvn_log.Debug.Printf("Received from execute err %s",err)
	}
	lvn_log.Debug.Printf("Received from execute output %s", out)
        lvn_log.Debug.Printf("Exiting tail functon...")
	return out, nil
}
func Top_proc() ([]map[string]string, error) {
	lvn_log.Debug.Println(utils.GetFuncName())
        args := []string{"/bin/sh", "-c", "top -b -n 1 | awk '{print $9, $12}'"}
	lvn_log.Debug.Println(args)
        output, err := execute("/bin/sh", args)
        //r := []map[string]string{}
        if err != nil {
                return nil, err
		lvn_log.Debug.Printf("Received from execute err %s",err)
        }
	lvn_log.Debug.Printf("Received from execute output %s", output)
        //res := regexp.MustCompile(`[0-9]+.[0-9]+`)
        //result := res.FindAllString(output, -1)
        //result1 := regexp.MustCompile(`[0-9]+.[0-9]+`).Split(output, -1)
        pathtemp := strings.Split(string(output), "\n")
        length := len(pathtemp) - 1
        var TopPr = []map[string]string{}

        for i := 6; i < length; i++ {
                a := strings.Fields(pathtemp[i])
                x := map[string]string{}
                x[a[1]] = a[0]
                TopPr = append(TopPr, x)
        }
	lvn_log.Debug.Printf("Returning output topprocess %s", TopPr)
        lvn_log.Debug.Printf("Exiting top_proc functon...")

        return TopPr, nil
}


func GetCurrentDirectory()(string,error){
       lvn_log.Debug.Println(utils.GetFuncName())
       args:=[]string{"pwd"}
       lvn_log.Debug.Println(args)
       out,err :=execute("pwd",args)
       if err != nil {
                return out, err
		lvn_log.Debug.Printf("Received from execute err %s", err)
       }
       lvn_log.Debug.Printf("Received from execute output %s", out)
       lvn_log.Debug.Printf("Exiting GetCurrentDirectory functon...")
       return out, nil
}

func AppendContentToFile(file string, content string) (string,error){
       lvn_log.Debug.Println(utils.GetFuncName())
       lvn_log.Debug.Println(" Parameters: file %s",file)
       args:=[]string{"/bin/sh"}
       args=append(args,"-c",fmt.Sprintf(`echo "%s" >> %s`, content, file))
       lvn_log.Debug.Println(args)
       out, err := execute("/bin/sh",args)
       if err != nil {
                return out, err
		lvn_log.Debug.Printf("Received from execute err %s",err)
       }
       lvn_log.Debug.Printf("Received from execute output %s", out)
       lvn_log.Debug.Printf("Exiting AppendContentToFile functon...")
       return out, nil
}

func Makedir(direc_name string) (string,error){
       lvn_log.Debug.Println(utils.GetFuncName())
       lvn_log.Debug.Println(" Parameters: Directory_name %s",direc_name)
       args:=[]string{"mkdir"}
       args=append(args,direc_name)
       lvn_log.Debug.Println(args)
       out, err := execute("mkdir",args)
       if err != nil {
                return out, err
		lvn_log.Debug.Printf("Received from execute err %s",err)
       }
       lvn_log.Debug.Printf("Received from execute output %s", out)
       lvn_log.Debug.Printf("Exiting Makedir functon...")
       return out, nil
}

func MakeNestedDir(nesdirec_name string) (string,error){
       lvn_log.Debug.Println(utils.GetFuncName())
       lvn_log.Debug.Println(" Parameters: Directory_name %s",nesdirec_name)
       args:=[]string{"mkdir"}
       args=append(args,"-p",nesdirec_name)
       lvn_log.Debug.Println(args)
       out, err := execute("mkdir",args)
       if err != nil {
                return out, err
                lvn_log.Debug.Printf("Received from execute err %s",err)
       }
       lvn_log.Debug.Printf("Received from execute output %s", out)
       lvn_log.Debug.Printf("Exiting MakeNestedDir functon...")
       return out, nil
}

func ResolveHostname(hostname string) (bool, string) {

       var ip string
       args :=[]string{"dig", hostname}
       out, err := execute("dig", args)
       if err != nil {
                lvn_log.Debug.Printf("Received from execute err %s",err)
                return false, ip
       }
       re := regexp.MustCompile(fmt.Sprintf(`ANSWER SECTION:\n%s.\s+\d\s+IN\s+A\s+(\d+.\d+.\d+.\d+)\n`, hostname))
       //ip = string(re.Find([]byte(out)))
       ip2 := re.FindStringSubmatch(out)
       //fmt.Println(ip2, len(ip2), ip2[len(ip2)-1])
       if len(ip2) >  0 {
	       ip = ip2[len(ip2)-1]
       } else {
                return false, ip
       }
       //fmt.Println(out)
       return true, ip
}

func DeleteFile(file string) bool {

       lvn_log.Debug.Println(utils.GetFuncName())
       args:=[]string{"rm", "-rf", file}
       out, err := execute("rm", args)
       fmt.Println(out)
       if err != nil {
                return false
       }
       return true
}

func CopyFile(source_file string, dest_file string) bool {

       lvn_log.Debug.Println(utils.GetFuncName())
       args:=[]string{"cp", "-rf", source_file, dest_file}
       out, err := execute("cp", args)
       fmt.Println(out)
       if err != nil {
                return false
       }
       return true
}

func AddCustomerHostEntry(customer_url_prefix string, customer_url_domain string, master_webui_url string) bool {

    var res bool
    var cs_ip, insights_ip string
    res, cs_ip = ResolveHostname(strings.Split(master_webui_url, "//")[1])
    if !res {
	    return false
    }
    res, insights_ip = ResolveHostname(strings.Replace(strings.Split(master_webui_url, "//")[1], "webui", "insights", 1))
    if !res {
	    return false
    }
    base := fmt.Sprintf("%s.%s", customer_url_prefix, customer_url_domain)
    webui := fmt.Sprintf("%s-webui.%s", customer_url_prefix, customer_url_domain)
    webrt := fmt.Sprintf("%s-webrt.%s", customer_url_prefix, customer_url_domain)
    nbrs := fmt.Sprintf("%s-nbrs.%s", customer_url_prefix, customer_url_domain)
    repo := fmt.Sprintf("%s-repo.%s", customer_url_prefix, customer_url_domain)
    insights := fmt.Sprintf("%s-insights.%s", customer_url_prefix, customer_url_domain)

    content := fmt.Sprintf(`%s %s %s %s %s %s
%s %s`, cs_ip, base, webui, webrt, nbrs, repo, insights_ip, insights)

    _, err := AppendContentToFile("/etc/hosts", content)
    if err!=nil {
	    return false
    }
    return true

}

func execute(name string, args []string) (string, error) {

	cmd := exec.Command(name, "")
	cmd.Args = args
	out, err2 := cmd.CombinedOutput()
	if err2 != nil {
		fmt.Println(string(out))
		return string(out), fmt.Errorf("exec.Command fails")
	}

	return string(out), nil
	//return out, nil
}


func GenerateSshKey() (bool, error) {

//	ssh-keygen -t rsa -f /root/.ssh/id_rsa -N ""

        filename := "/root/.ssh/id_rsa"
       if _, err := os.Stat(filename); os.IsNotExist(err) {

           args :=[]string{"ssh-keygen", "-t", "rsa", "-f", filename, "-N", ""}
           out, err := execute("ssh-keygen", args)
           fmt.Println(out)
           if err != nil {
                    lvn_log.Debug.Printf("Received from execute err %s",err)
                    return false, nil
           }
           return true, nil
      }
      return false, nil
}

func Regenerate_ssh_key() (bool, error) {

        filename := "/root/.ssh/id_rsa"
        if !DeleteFile(filename) {
		return false, nil
	}
        return GenerateSshKey()

}


func ScpFromRemote(ip_addr string, username string, remote_file string, local_file string) (bool, error) {

    args :=[]string{"scp", fmt.Sprintf("%s@%s:%s", username, ip_addr, remote_file), local_file}
    out, err := execute("scp", args)
    fmt.Println(args)
    if err != nil {
             lvn_log.Debug.Printf("Received from execute err %s",err)
             return false, nil
    }
    lvn_log.Debug.Printf(out)
    return true, nil
}


func ScpFromCpe(ip_addr string, remote_file string, local_file string) (bool, error) {
	username := "lavelle"
	return ScpFromRemote(ip_addr, username, remote_file, local_file)
}


func  DigShort(domain string) (bool, error, []string) {
    cmd := []string{"dig", "+short", domain}
    output, err := execute("dig", cmd)
    if err != nil {
        return false, nil, []string{}
    }
    re := regexp.MustCompile(`^\d+\.\d+\.\d+\.\d+$`)
    ip_list := strings.Split(strings.TrimSpace(output), "\n")
    result := []string{}
    for _, ip := range ip_list {
        if re.MatchString(ip) {
            result = append(result, ip)
        }
    }
    return true, nil, result
}

func IpRouteShow() (map[string]string, map[string]string, map[string]string, error) {

    default_route := map[string]string{}
    onlink_routes := map[string]string{}
    routes := map[string]string{}

    cmd := []string{"ip", "-h", "route", "show"}
    output, err := execute("ip", cmd)
    if err != nil {
        return default_route, onlink_routes, routes, err
    }

    default_route, onlink_routes, routes = utils.ParseIpRouteShowOutput(output)
    return default_route, onlink_routes, routes, nil

}
