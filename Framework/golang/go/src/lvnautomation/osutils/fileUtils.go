package osutils

import (
	"os"
)


func CheckFileExists(filename string) (bool) {
    if _, err := os.Stat(filename); os.IsNotExist(err) {
        return false
    } else {
        return true
    }
}
