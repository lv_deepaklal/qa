package osutils

import (
	"fmt"
	scp "github.com/bramvdbogaerde/go-scp"
	"github.com/bramvdbogaerde/go-scp/auth"
	"golang.org/x/crypto/ssh"
	"os"
	host_utils "lvnautomation/hostutils"
)

//type Credentials struct{
//        Host string
//        Port string
//        User string
//        Pswd string
//}

//Credentials
func Scp(c *host_utils.Credentials, local_filename string, remote_filename string) {
	// Use SSH key authentication from the auth package
	// we ignore the host key in this example, please change this if you use this library
	clientConfig, _ := auth.PasswordKey(c.User, c.Pswd, ssh.InsecureIgnoreHostKey())

	// For other authentication methods see ssh.ClientConfig and ssh.AuthMethod

	// Create a new SCP client
	client := scp.NewClient(fmt.Sprintf("%s:%s", c.Host, c.Port), &clientConfig)

	// Connect to the remote server
	err := client.Connect()
	if err != nil {
		fmt.Println("Couldn't establish a connection to the remote server ", err)
		return
	}

	// Open a file
	//filename:= "/root/.ssh/id_rsa.pub"
	f, _ := os.Open(local_filename)

	// Close client connection after the file has been copied
	defer client.Close()

	// Close the file after it has been copied
	defer f.Close()

	// Finaly, copy the file over
	// Usage: CopyFile(fileReader, remotePath, permission)
	// Finaly, copy the file over
	// Usage: CopyFile(fileReader, remotePath, permission)

	err = client.CopyFile(f, remote_filename, "0666")

	if err != nil {
		fmt.Println("Error while copying file ", err)
	}
}
