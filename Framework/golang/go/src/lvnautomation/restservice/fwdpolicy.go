package restservice

import (

	"fmt"
	"net/http"
	//"errors"
	"encoding/json"
	//"regexp"
	"strconv"
	"strings"
	//"bytes"
	//"compress/gzip"
	//"io"

	log "lvnautomation/lvnlog"
	//"time"
	tc_utils "lvnautomation/testcaseutils"
)

/*
	FWD POLICY
*/

type WanInfo struct {
	CreatedAt              string `json:"created_at"`
	Customer_id            int    `json:"customer_id"`
	Direction              string `json:"direction"`
	Dst_network_group_id   int    `json:"dst_network_group_id"`
	Dst_network_group_name string `json:"dst_network_group_name"`
	Hex_id                 string `json:"hex_id"`
	Id                     int    `json:"id"`
	Name                   string `json:"name"`
	Output_direction       string `json:"output_direction"`
	Priority               int    `json:"priority"`
	Src_network_group_id   int    `json:"src_network_group_id"`
	Src_network_group_name string `json:"src_network_group_name"`
	Tag                    string `json:"tag"`
	UpdatedAt              string `json:"updated_at"`
	User_id                int    `json:"user_id"`
}

type CreateFwdResponse struct {
	Data struct {
		Wan_policies WanInfo `json:"wan_policy"`
	} `json:"data"`
	Message string `json:"message"`
}

func (c *CreateFwdResponse) GetName() string {
	return c.Data.Wan_policies.Name
}

func (c *CreateFwdResponse) GetHexId() string {
	return c.Data.Wan_policies.Hex_id
}

func (c *CreateFwdResponse) GetId() int {
	return c.Data.Wan_policies.Id
}

type FWDPOLICY struct {
	Wan_policies []WanInfo `json:"wan_policies"`
	Total_count  int       `json:"total_count"`
}

type FwdList struct{
	Wan_Policy []struct{
		Id int `json"id"`
		Name string `json"name"`
		Priority int `json"priority"`
		Create_at string `json"created_at"`
		Update_at string `json"updated_at"`
		Src_net_grp_id int `json"src_network_group_id"`
		Src_net_grp_name string `json"src_network_group_name"`
		Direction string `json"direction"`
		Cust_id int `json"customer_id"`
		User_id int `json"user_id"`
		Hex_id string `json"hex_id"`
		Tag string `json"tag"`
		Out_direction string `json"output_direction"`
		Dst_net_grp_id int `json"dst_network_group_id"`
		Dst_net_grp_name string `json:"dst_network_group_name"`
	}`json:"wan_policies"`
	Tot_count int `json:"total_count"`
}

type GetFwdListResponse struct {
          Wan_policies []WanInfo `json:"wan_policies"`
          Total_count int `json:"total_count"`
}

type DeleteFwdPolicyResponse struct{
	Msg string `json:"message"`
	Tot_count int `json:"total_count"`
	Reason string `json:"reason"`
	Data struct{
		Wan_Policy WanInfo `json:'wan_policy'`
	} `json:"data"`
}

type FwdPolAction struct{
                        Encapsulation string `json:"encapsulation"`
                        Gre_keepalive bool `json:"gre_keepalive"`
                        Gre_key  string `json:"gre_key"`
                        Next_hop string `json:"next_hop"`
                        //Secured_data string `json:"secured_data"` // bool for nexthop none, else string
                        Wan_type string `json:"wan_type"`
                        //Site string `json:"site"`
			VpnProfileId string `json:"vpn_profile_id"`
                        //NxtHopObjectValue string `json:"next_hop_object_value"` // int for site, string for customip
}

type SiteAction struct {
	FwdPolAction
	NxtHopObjectValue int `json:"next_hop_object_value"`
        Secured_data string `json:"secured_data"` // bool for nexthop none, else string
}

type DefaultAction struct {
	FwdPolAction
	NxtHopObjectValue string `json:"next_hop_object_value"`
        Secured_data string `json:"secured_data"` // bool for nexthop none, else string
}

type LocalAction struct {
	FwdPolAction
	NxtHopObjectValue string `json:"next_hop_object_value"`
        Secured_data bool `json:"secured_data"` // bool for nexthop none, else string
}

//bool, error, map[string]FwdInfo, map[string]FwdInfo

func (cs *Cloudstation) Get_FwdPolicyList() (bool, error, map[string]WanInfo, map[string]WanInfo) {

	if cs.Helper.policies.fwd.byName == nil || cs.Helper.policies.fwd.byHexId == nil {
		return cs.ForceGetFwdPolicyList()
	} else {
		return true, nil, cs.Helper.policies.fwd.byName, cs.Helper.policies.fwd.byHexId
	}
}

func (cs *Cloudstation) ForceGetFwdPolicyList() (bool, error, map[string]WanInfo, map[string]WanInfo) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	//var fwd_policy_list_wrt_name, fwd_policy_list_wrt_hexid map[string]WanInfo
	fwd_policy_list_wrt_name := make( map[string]WanInfo )
	fwd_policy_list_wrt_hexid := make( map[string]WanInfo )

	key := "get_fwdpolicy_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

	var fwdpol FWDPOLICY

	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//fmt.Println("Status Get_FwdPolicyList of page 1= ", resp.StatusCode)
	if resp.StatusCode != 200 {
		return false, nil, fwd_policy_list_wrt_name, fwd_policy_list_wrt_hexid
	}
	err_fwdpolicy := json.NewDecoder(resp.Body).Decode(&fwdpol)
	if err_fwdpolicy != nil {
		fmt.Println(err_fwdpolicy)
	}

	tot_entry := fwdpol.Total_count

        for _, fwd := range fwdpol.Wan_policies {
            fwd_policy_list_wrt_name[fwd.Name] = fwd
            fwd_policy_list_wrt_hexid[fwd.Hex_id] = fwd
         }

	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}

	i := 2
	for i <= tot_page {
		var fwdpol1 FWDPOLICY
		queryParam = map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		//fmt.Printf("Status Get_FwdPolicyList of page %d= %d\n", i, resp.StatusCode)
		if resp.StatusCode != 200 {
		    return false, nil, fwd_policy_list_wrt_name, fwd_policy_list_wrt_hexid
		}
		err_fwdpolicy := json.NewDecoder(resp.Body).Decode(&fwdpol1)
		if err_fwdpolicy != nil {
			fmt.Println(err_fwdpolicy)
		}

                for _, fwd := range fwdpol1.Wan_policies {
                    fwd_policy_list_wrt_name[fwd.Name] = fwd
                    fwd_policy_list_wrt_hexid[fwd.Hex_id] = fwd
                 }

		i++
	}
	//fmt.Println("--------------Got into the Verse for Getting the FWD POLICY LIST !--------------")
	//fmt.Println()
	cs.Helper.policies.fwd.byName = fwd_policy_list_wrt_name
	cs.Helper.policies.fwd.byHexId = fwd_policy_list_wrt_hexid

        return true, nil, fwd_policy_list_wrt_name, fwd_policy_list_wrt_hexid
}

func (cs *Cloudstation) Get_FwdHexId(fwd_name string, fwd_policy_list_wrt_name map[string]WanInfo) (string, bool) {

	/*
	res, _, fwd_policy_list_wrt_name, _ := cs.Get_FwdPolicyList()
	if !res {
		return "", false
	}
	var hex_id string
	var found_hex_id bool
	for i := 0; i < len(fwdpol_ls); i++ {
		for j := 0; j < 10; j++ {
			if fwdpol_ls[i].Wan_policies[j].Name == fwd_name {
				hex_id = fwdpol_ls[i].Wan_policies[j].Hex_id
				found_hex_id = true
				break
			}
		}
		if found_hex_id == true {
			break
		}
	}
	if found_hex_id == false {
		fmt.Println("No Hex Id found !")
		return "", false
	}
	return hex_id, true
	*/

	if fwdpolicy, ok := fwd_policy_list_wrt_name[fwd_name]; ok {
		return fwdpolicy.Hex_id, true
	} else {
		return "", false
	}
}

func (cs *Cloudstation) Get_FwdId(fwd_name string, fwd_policy_list_wrt_name map[string]WanInfo) (int, bool) {


	/*
	res, _, fwd_policy_list_wrt_name, _ := cs.Get_FwdPolicyList()
	if !res {
		return 0, false
	}
	var id int
	var found_id bool
	for i := 0; i < len(fwdpol_ls); i++ {
		for j := 0; j < 10; j++ {
			if fwdpol_ls[i].Wan_policies[j].Name == fwd_name {
				id = fwdpol_ls[i].Wan_policies[j].Id
				found_id = true
				break
			}
		}
		if found_id == true {
			break
		}
	}
	if found_id == false {
		fmt.Println("No Id found !")
		return 0, false
	}
	return id, true
	*/

	if fwdpolicy, ok := fwd_policy_list_wrt_name[fwd_name]; ok {
		return fwdpolicy.Id, true
	} else {
		return 0, false
	}
}

// For FWD POLICY attach and detach at network group

func (cs *Cloudstation) AttachFwdAtNwGrp(fwd_name string, nwgrp_name string) bool {

	var fwdhex_id string
	var err1 bool

	if res, _, x, _ := cs.Get_FwdPolicyList(); res {
	    fwdhex_id, err1 = cs.Get_FwdHexId(fwd_name, x)
	}

	if err1 == false {
		fmt.Println("Failed to fetch the Fwd Hex Id !")
		return false
	}

	nwgrphex_id, err2 := cs.Get_NwGrpHexId(nwgrp_name)
	if err2 == false {
		fmt.Println("Failed to fetch the NwGrp Hex Id !")
		return false
	}

	var attach_nwgrp_payload struct {
		Attached_to_id   string `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_nwgrp_payload.Attached_to_id = nwgrphex_id
	attach_nwgrp_payload.Attached_to_type = "NetworkGroup"

	key := "attach_fwd_at_networkgrp"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": fwdhex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_nwgrp_payload, cookie)
	//fmt.Println("Status AttachFwdAtNwGrp= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for ATTACHING FWD AT NETWORK_GROUP LEVEL-------------")
	//fmt.Println()
	return true
}

func (cs *Cloudstation) DetachFwdAtNwGrp(fwd_name string, nwgrp_name string) bool {

	var fwd_id int
	var err1 bool

	if res, _, x, _ := cs.Get_FwdPolicyList(); res {
	    fwd_id, err1 = cs.Get_FwdId(fwd_name, x)
	}

	if err1 == false {
		fmt.Println("Failed to fetch the Fwd Id !")
		return false
	}

	nwobject_id, err2 := cs.Get_NwGrpId(nwgrp_name)
	if err2 == false {
		fmt.Println("Failed to fetch the NwGrp Object Id !")
		return false
	}

	var detach_nwgrp_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_nwgrp_payload.Object_id = nwobject_id
	detach_nwgrp_payload.Object_type = "NetworkGroup"

	key := "detach_fwd_at_networkgrp"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(fwd_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_nwgrp_payload, cookie)
	//fmt.Println("Status DetachFwdAtNwGrp= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for DETACHING FWD AT NETWORK_GROUP LEVEL-------------")
	//fmt.Println()
	return true
}

// For FWD POLICY attach and detach at site

func (cs *Cloudstation) AttachFwdAtSite(fwd_name string, site_name string) bool {

	var fwdhex_id string
	var err1 bool

	if res, _, x, _ := cs.Get_FwdPolicyList(); res {
	    fwdhex_id, err1 = cs.Get_FwdHexId(fwd_name, x)
	}

	if err1 == false {
		fmt.Println("Failed to fetch the Fwd Hex Id !")
		return false
	}

	site_id, err2 := cs.Get_SiteId(site_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Site Id !")
		return false
	}

	var attach_site_payload struct {
		Attached_to_id   int    `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_site_payload.Attached_to_id = site_id
	attach_site_payload.Attached_to_type = "Site"

	key := "attach_fwd_at_site"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": fwdhex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_site_payload, cookie)
	//fmt.Println("Status AttachFwdAtSite= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for ATTACHING FWD AT SITE LEVEL-------------")
	//fmt.Println()
	return true
}

func (cs *Cloudstation) DetachFwdAtSite(fwd_name string, site_name string) bool {

	var fwd_id int
	var err1 bool

	if res, _, x, _ := cs.Get_FwdPolicyList(); res {
	    fwd_id, err1 = cs.Get_FwdId(fwd_name, x)
	}

	if err1 == false {
		fmt.Println("Failed to fetch the Fwd Id !")
		return false
	}

	siteobject_id, err2 := cs.Get_SiteId(site_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Site Object Id !")
		return false
	}

	var detach_site_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_site_payload.Object_id = siteobject_id
	detach_site_payload.Object_type = "Site"

	key := "detach_fwd_at_site"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(fwd_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_site_payload, cookie)
	//fmt.Println("Status DetachFwdAtSite= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for DETACHING FWD AT SITE LEVEL-------------")
	//fmt.Println()
	return true
}

// For FWD POLICY attach and detach at site profile

func (cs *Cloudstation) AttachFwdAtSiteProfile(fwd_name string, profile_name string) bool {

	var fwdhex_id string
	var err1 bool

	if res, _, x, _ := cs.Get_FwdPolicyList(); res {
	    fwdhex_id, err1 = cs.Get_FwdHexId(fwd_name, x)
	    if !err1 {
		    return err1
	    }
	}

	profile_id, err2 := cs.Get_ProfileId(profile_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Profile Id !")
		return false
	}

	var attach_profile_payload struct {
		Attached_to_id   int    `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_profile_payload.Attached_to_id = profile_id
	attach_profile_payload.Attached_to_type = "Profile"

	key := "attach_fwd_at_profile"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": fwdhex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_profile_payload, cookie)
	//fmt.Println("Status AttachFwdAtSiteProfile= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for ATTACHING FWD AT SITE PROFILE LEVEL-------------")
	//fmt.Println()
	return true
}

func (cs *Cloudstation) DetachFwdAtSiteProfile(fwd_name string, profile_name string) bool {

	var fwd_id int
	var err1 bool

	if res, _, x, _ := cs.Get_FwdPolicyList(); res {
	    fwd_id, err1 = cs.Get_FwdId(fwd_name, x)
	}

	if err1 == false {
		fmt.Println("Failed to fetch the Fwd Id !")
		return false
	}

	profileobject_id, err2 := cs.Get_ProfileId(profile_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Profile Object Id !")
		return false
	}

	var detach_profile_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_profile_payload.Object_id = profileobject_id
	detach_profile_payload.Object_type = "Profile"

	key := "detach_fwd_at_profile"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(fwd_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_profile_payload, cookie)
	//fmt.Println("Status DetachFwdAtSiteProfile= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for DETACHING FWD AT SITE PROFILE LEVEL-------------")
	//fmt.Println()
	return true
}



func (cs *Cloudstation) DeleteFwdPolicyByHexId(hex_id string) (bool,*DeleteFwdPolicyResponse) {

        var delresponse DeleteFwdPolicyResponse
        var deletefwd_payload struct {
        }

        key := "delete_fwd_policy"
        apis := getApis()
        api := apis[key]
	fmt.Println(hex_id)
        substitutedPath := substitutePath(apis[key], map[string]string{"policy_id": hex_id})
        //fmt.Println(substitutedPath)
        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, deletefwd_payload, cookie)
	err_delresponse:= json.NewDecoder(resp.Body).Decode(&delresponse)
        if err_delresponse != nil {
        fmt.Println(err_delresponse)
         }
         return true,&delresponse

        //fmt.Println("Status Delete_Fwd= ", resp.StatusCode)
        if resp.StatusCode != 200 {
                return false,&delresponse
 }
        //fmt.Println("--------------Got into the Verse for DELETING FWD-------------")
        //fmt.Println()
        return true,&delresponse
}

func (cs *Cloudstation) DeleteFwdPolicy(fwd_name string) (bool,*DeleteFwdPolicyResponse) {

        //var delresponse DeleteFwdPolicyResponse
	var fwd_hexid string
	var fwd_res WanInfo
	var ok bool
        err1, _, res2,_ := cs.Get_FwdPolicyList()
        //fmt.Println(fwd_id)
        if err1 == false {
                fmt.Println("Failed to fetch the Fwd Id !")
                return false, nil
        }
        //fwd_res:=res2[fwd_name]
        //fwd_id:=fwd_res.Hex_id
	if fwd_res, ok = res2[fwd_name]; !ok {
            _, _, res2,_ = cs.ForceGetFwdPolicyList()
	}

	if fwd_res, ok = res2[fwd_name]; !ok {
            return false, nil
	}
	fwd_hexid = fwd_res.Hex_id
	//fmt.Println(fwd_hexid)

	return cs.DeleteFwdPolicyByHexId(fwd_hexid)

	/*
        var deletefwd_payload struct {
        }

        key := "delete_fwd_policy"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"policy_id": fwd_id})
        //fmt.Println(substitutedPath)
        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, deletefwd_payload, cookie)
	err_delresponse:= json.NewDecoder(resp.Body).Decode(&delresponse)
        if err_delresponse != nil {
        fmt.Println(err_delresponse)
         }
         return true,&delresponse

        //fmt.Println("Status Delete_Fwd= ", resp.StatusCode)
        if resp.StatusCode != 200 {
                return false,&delresponse
 }
        //fmt.Println("--------------Got into the Verse for DELETING FWD-------------")
        //fmt.Println()
        return true,&delresponse
	*/
}

func (cs *Cloudstation) CreateForwardingPolicy(CreateFwdObj *CreateFwdInputs) (bool,*CreateFwdResponse) {

        var fwdresp CreateFwdResponse
        var CreateForwardingStruct struct{
                MATCHCRITERIA
		//MATCHCRITERIAv10_0_2
		AclAction string `json:"acl_action"`
                Name string `json:"name"`
                Priority int `json:"priority"`
                Tag string `json:"tag"`
                Output_direction string `json:"output_direction"`
                Wan_actions []interface{}`json:"wan_actions"`

		/* below are useless data  */
		BothName string `json:"both_name"`
		BothObj string `json:"both_object"`
		BothType string `json:"both_type"`
		Disabled bool `json:"disabled"`
		FromName string `json:"from_name"`
		FromObj string `json:"from_object"`
		FromType string `json:"from_type"`
		OwnerId string `json:"owner_id"`
		OwnerType string `json:"owner_type"`
		SecuredData bool `json:"secured_data"`
		ToName string `json:"to_name"`
		ToObj string `json:"to_object"`
		ToType string `json:"to_type"`
        }

     CreateForwardingStruct.Name = CreateFwdObj.Name
     CreateForwardingStruct.Priority = CreateFwdObj.Priority
     CreateForwardingStruct.Tag = CreateFwdObj.Tag
     CreateForwardingStruct.Output_direction = CreateFwdObj.Output_direction

     encryption_profile_id := ""// find it from val.encryption_profile

     for _, val := range CreateFwdObj.action {
             common_action := FwdPolAction{
                                Wan_type : val.wan_type,
                                Encapsulation : val.encapsulation,
				//Secured_data: val.secure_data,
                                Next_hop : val.next_hop,
                                //Site : val.site,
                                Gre_key : val.gre_key,
                                Gre_keepalive : val.gre_keepalive,
				VpnProfileId: encryption_profile_id,
	     }

	     if val.next_hop == "site" {
		     site_id, ok := cs.SitesByName()[val.site]
		     if !ok {
			     return false, &fwdresp
		     }
                y := SiteAction { NxtHopObjectValue : site_id.Id,
                                  Secured_data: val.secure_data,
				  FwdPolAction: common_action }
               CreateForwardingStruct.Wan_actions = append( CreateForwardingStruct.Wan_actions,y)
	     } else if strings.ToLower(val.next_hop) == "local" {
               var none_secure_data bool
               if val.secure_data == "" {
	           none_secure_data = false
               } else {
                   none_secure_data = true
               }
                y := LocalAction{  NxtHopObjectValue : val.custom_ip,
                                  Secured_data: none_secure_data,
				  FwdPolAction: common_action }
               CreateForwardingStruct.Wan_actions = append( CreateForwardingStruct.Wan_actions,y)
	     } else {
                y := DefaultAction{ NxtHopObjectValue : val.custom_ip,
                                  Secured_data: val.secure_data,
				FwdPolAction: common_action }
               CreateForwardingStruct.Wan_actions = append( CreateForwardingStruct.Wan_actions,y)
	     }
       }
     //match_cri, _ := cs.GetMatchCriteriaV10_0_2(CreateFwdObj.MatchCriteriaInput)
     //CreateForwardingStruct.MATCHCRITERIAv10_0_2 = *match_cri

     match_cri, _ := cs.GetMatchCriteria(CreateFwdObj.MatchCriteriaInput)
     CreateForwardingStruct.MATCHCRITERIA = *match_cri

     key:="create_fwd_policy"
    apis := getApis()
    api :=apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{})

    header := cs.Construct_headers(apis[key].headers.headers)
    //fmt.Println(header)
    var resp *http.Response

       // policyjson, _ := json.MarshalIndent(CreateForwardingStruct, "", "\t")
   //err := ioutil.WriteFile("output1.json", policyjson, 0644)
   //if err != nil {
   //fmt.Println(err)
   //}

 cookie := []*http.Cookie{cs.Cookies}
    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, CreateForwardingStruct, cookie)
    err_fwdresp:= json.NewDecoder(resp.Body).Decode(&fwdresp)
        if err_fwdresp != nil {
                fmt.Println(err_fwdresp)
         }
       //  return true,&fwdresp
    if !cs.Status_CS(resp.StatusCode) {
                return false,&fwdresp
        }

        if resp.StatusCode == 401 {
                resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, CreateForwardingStruct, cookie)
        }

        if resp.StatusCode != 200 {
                return false,&fwdresp
        }

	if cs.Helper.policies.fwd.byName == nil {
		cs.Helper.policies.fwd.byName = make(map[string]WanInfo)
	}
	if cs.Helper.policies.fwd.byHexId == nil {
		cs.Helper.policies.fwd.byHexId = make(map[string]WanInfo)
	}

	cs.Helper.policies.fwd.byName[fwdresp.Data.Wan_policies.Name] = fwdresp.Data.Wan_policies
	cs.Helper.policies.fwd.byHexId[fwdresp.Data.Wan_policies.Hex_id]= fwdresp.Data.Wan_policies

	if ok, json_data := convert_struct_to_string_via_json(fwdresp); ok {
	    log.Debug.Printf("create_fwd_policy_response : %s\n", json_data)
	}
        return true,&fwdresp
}
