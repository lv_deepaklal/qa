package restservice

import (
	"fmt"
	"net/http"
	"errors"
	"encoding/json"
	//"regexp"
	//"strconv"
	"strings"
	//"bytes"
	//"compress/gzip"
	//"io"

	log "lvnautomation/lvnlog"
	//"time"
	tc_utils "lvnautomation/testcaseutils"
)

type CreateCustomerResponse struct {
        Message string `json:"message"`
        Data struct {
                Customer CustomerInfo `json:"customer"`
                Net_admin struct {
                        Email string `json:"email"`
                        Password string `json:"password"`
                } `json:"net_admin"`
                Sys_admin struct {
                        Email string `json:"email"`
                        Password string `json:"password"`
                } `json:"sys_admin"`

        } `json:"data"`
}

type CustomerManager struct {
    Name string `json:"name"`
    Email string `json:"email"`
}

type CustomerManagerResponse struct {
    Managers []CustomerManager `json:"managers"`
}

func (cs *Cloudstation) GetListOfManagers() (bool, error, *CustomerManagerResponse) {

        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        var (
            //res bool
            //err error
            manager_response CustomerManagerResponse
            //resp *http.Response
            //payload EmptyHttpPayload
        )

        key := "get_list_of_managers"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{})

        header := cs.Construct_headers(apis[key].headers.headers)
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ := request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, map[string]interface{}{}, cookie)
        if resp.StatusCode != 200 {
            return false, nil, &manager_response
        }

        err12 := json.NewDecoder(resp.Body).Decode(&manager_response)
        //fmt.Println("manager repsne: ",manager_response)
        if err12 != nil {
            log.Err.Println(err12)
            return false, nil, &manager_response
        }
        if ok, json_data := convert_struct_to_string_via_json(manager_response); ok {
            log.Debug.Printf("response data: %s\n", json_data)
        }
        return true, nil, &manager_response
}

func (cs *Cloudstation) CreateCustomer(create_customer_input *createCustomerInput) (bool, error, *CreateCustomerResponse) {

        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        log.Info.Printf("Creating a customer: %+v\n", create_customer_input)
    var (
//        res bool
//err error
//manager_response *CustomerManagerResponse
create_customer_response CreateCustomerResponse
resp *http.Response
createCustomer struct {
       Name string `json:"name"`
       Sys_admin_email string `json:"email"`
       Net_admin_email string `json:"net_admin_email"`
       Manager CustomerManager `json:"manager"`
       Net_admin_emails []string `json:"net_admin_emails"`
       License_name string `json:"license_name"`
       Network_size int `json:"network_size"`
       Admin_assist_tickets int `json:"admin_assist_tickets"`
       License_key string `json:"license_key"`
       Domain string `json:"domain"`
       Url_prefix string `json:"url_prefix"`
       Url string `json:"url"`
}
         )

         res, _, manager_response := cs.GetListOfManagers()
         if !res {
            return false, nil, nil
         }

    for _, manager := range manager_response.Managers {
            //fmt.Println(manager)
        if strings.ToLower(create_customer_input.Managed_by)==strings.ToLower(manager.Name) {
            createCustomer.Manager = manager
            break
        }
    }

    createCustomer.Name = create_customer_input.Customer_name
    createCustomer.Sys_admin_email = create_customer_input.Sys_admin_email
    createCustomer.Net_admin_email = create_customer_input.Net_admin_email
    createCustomer.Net_admin_emails = []string{}
    createCustomer.License_name = create_customer_input.License_name
    createCustomer.Network_size = create_customer_input.Network_size
    createCustomer.Admin_assist_tickets = create_customer_input.Admin_assist_tickets
    createCustomer.License_key = create_customer_input.License_key
    createCustomer.Domain = create_customer_input.Domain
    createCustomer.Url_prefix = create_customer_input.Url_prefix
    createCustomer.Url = fmt.Sprintf("%s.%s", create_customer_input.Url_prefix, create_customer_input.Domain)

    key := "create_customer"
    apis := getApis()
    api := apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{})

                //filename := fmt.Sprintf("%s_%s.json", "post", strings.Replace(substitutedPath, "/", "_", -1))
                //file, _ := json.MarshalIndent(createCustomer, "", " ")
                //file, _ := json.Marshal(createCustomer)
                //_ = ioutil.WriteFile(filename, file, 0666)

    header := cs.Construct_headers(apis[key].headers.headers)
    cookie := []*http.Cookie{cs.Cookies}
    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, createCustomer, cookie)
    if resp.StatusCode != 200 {
        return false, nil, &create_customer_response
    }

    err12 := json.NewDecoder(resp.Body).Decode(&create_customer_response)
    if err12 != nil {
        log.Err.Println(err12)
        return false, nil, &create_customer_response
    }
        if ok, json_data := convert_struct_to_string_via_json(create_customer_response); ok {
            log.Debug.Printf("response data: %s\n", json_data)
        }
    return true, nil, &create_customer_response
}

type CustomerInfo struct {
        AdminAssistTickets int `json:"admin_assist_tickets"`
        //AdminEmail string `json:"admin_email"`
        Banner *string `json:"banner"`
        CreatedAt string `json:"created_at"`
        EnableInftMatch *string `json:"enable_intf_match"`
        EnableNotifications *string `json:"enable_notifications"`
        HexId string `json:"hex_id"`
        Id int `json:"id"`
        LicenseKey string `json:"license_key"`
        LicenseName string `json:"license_name"`
        Manager string `json:"manager"`
        ManagerEmail string `json:"manager_email"`
        Name string `json:"name"`
        NetworkSize int `json:"network_size"`
        Url string `json:"url"`
        UpdatedAt string `json:"updated_at"`
}

type GetCustomerListResponse struct {
        Customers []CustomerInfo `json:"customers"`
        DefaultDomain string `json:"default_domain"`
}


func (cs *Cloudstation) GetCustomerList() (bool, error, map[string]CustomerInfo) {

        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        var (
                customer_list_response GetCustomerListResponse
                resp *http.Response
                dummy struct {}
        )
    customer_info_wrt_name :=make(map[string]CustomerInfo)
    key := "get_customer_list"
    apis := getApis()
    api := apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{})

    header := cs.Construct_headers(apis[key].headers.headers)
    cookie := []*http.Cookie{cs.Cookies}
    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)

    if resp.StatusCode != 200 {
        return false, nil, customer_info_wrt_name
    }

    err12 := json.NewDecoder(resp.Body).Decode(&customer_list_response)

        if ok, json_data := convert_struct_to_string_via_json(customer_list_response); ok {
            log.Debug.Printf("response data: %s\n", json_data)
        }
    for _, customer := range customer_list_response.Customers {
            customer_info_wrt_name[customer.Name] = customer
    }
    if err12 != nil {
        log.Err.Println(err12)
        return false, nil, customer_info_wrt_name
    }

    return true, nil, customer_info_wrt_name
}

type DeleteCustomerResponse struct {
        Message string `json:"message"`
}

func (cs *Cloudstation) DeleteCustomer(customer_name string) (bool, error, *DeleteCustomerResponse) {

        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        log.Info.Printf("Deleting the customer: %s\n", customer_name)
    var (
         delete_customer_response DeleteCustomerResponse
         resp *http.Response
         dummy struct {}
         customer_id int
        )

    _, _, customer_name_map := cs.GetCustomerList()

    if customer_info, ok := customer_name_map[customer_name]; ok {
            customer_id = customer_info.Id
    } else {
        return false, nil, &delete_customer_response
    }

    key := "delete_customer"
    apis := getApis()
    api := apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{"customer_id": fmt.Sprintf("%d", customer_id)})

    header := cs.Construct_headers(apis[key].headers.headers)
    cookie := []*http.Cookie{cs.Cookies}
    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
    var err_reason error
    if resp.StatusCode != 200 {
        err_reason = errors.New(fmt.Sprintf("HTTP statuscode :%s", resp.StatusCode))
        log.Err.Println("Failed to delete the customer: %s\nReason: %s", customer_name, err_reason.Error())
        return false, err_reason, &delete_customer_response
    }

    err12 := json.NewDecoder(resp.Body).Decode(&delete_customer_response)
    if err12 != nil {
        log.Err.Println(err12)
        return false, nil, &delete_customer_response
    }

        if ok, json_data := convert_struct_to_string_via_json(delete_customer_response); ok {
            log.Debug.Printf("response data: %s\n", json_data)
        }

    log.Info.Printf("Successfully deleted the customer: %s\n", customer_name)
    return true, nil, &delete_customer_response
}

