package restservice

import (
	//"time"
	log "lvnautomation/lvnlog"
)

type CreateTrlPolicyInput struct {
                Name string
               // Net_grp_name string
               // Owner_id int
               // Owner_type string
                Pol_type string
                Priority int
               // Qos_action string
                Rate int
               // Tag string
                MatchCriteriaInput
}

func NewTrlInput() (bool, error, *CreateTrlPolicyInput) {
        var trlInput CreateTrlPolicyInput
	//fmt.Println(log.Log_date)
        var name string = "lvn_auto_" + log.Log_date
        trlInput.SetName(name)
        trlInput.SetPolicyType("flat_rate")
        trlInput.SetPriority(3)
        trlInput.SetRate(3)
        _, _, matchcriteria := NewMatchCriteriaInput()
        trlInput.MatchCriteriaInput = *matchcriteria
        return true, nil, &trlInput
}

func (trl *CreateTrlPolicyInput) SetPolicyType(pol_type string) (bool, error) {
        trl.Pol_type = pol_type
        return true, nil
}

func (trl *CreateTrlPolicyInput) SetName(name string) (bool, error) {
        trl.Name = name
        return true, nil
}

func (trl *CreateTrlPolicyInput) SetRate(rate int) (bool, error) {

	if rate < 0 {
		return false, nil
	}

        trl.Rate = rate
        return true, nil
}

func (trl *CreateTrlPolicyInput) SetPriority(priority int) (bool, error) {

        if priority >= 0 && priority <= 1000 {
                trl.Priority = priority
                return true, nil
        }
        return false, nil
}

func (trl *CreateTrlPolicyInput) SetMatchCriteria(m *MatchCriteriaInput) {
        trl.MatchCriteriaInput = *m
}
