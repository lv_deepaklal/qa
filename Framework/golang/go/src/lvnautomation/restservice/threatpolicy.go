package restservice

import (
	"fmt"
	"net/http"
	"errors"
	"encoding/json"
	log "lvnautomation/lvnlog"
	tc_utils "lvnautomation/testcaseutils"
)

type SecurityProfile struct {
    Antivirus *bool `json:"antivirus"`
    CreatedAt string `json:"created_at"`
    CustomerId int `json:"customer_id"`
    Id int `json:"Id"`
    Ips bool `json:"ips"`
    Name string `json:"name"`
    ProfileType string `json:"profile_type"`
    Repute *bool`json:"repute"`
    Sandbox *bool`json:"sandbox"`
    SslInspection *bool`json:"ssl_insp"`
    Tag string `json:"tag"`
    UpdatedAt string `json:"updated_at"`
    UserId int `json:"user_id"`
}

type GetSecurityProfilesResponse struct {
    SecurityProfiles []SecurityProfile `json:"security_profiles"`
    TotalCount int `json:"total_count"`
}

func (cs *Cloudstation) GetSecurityProfiles() (bool, error, map[string]SecurityProfile) {
    if len(cs.Helper.profiles.security.byName) == 0 {
	    return cs.ForceGetSecurityProfiles()
    }
    return true, nil, cs.Helper.profiles.security.byName
}

func (cs *Cloudstation) ForceGetSecurityProfiles() (bool, error, map[string]SecurityProfile) {
	//pi/v1/lavelle/security_profiles?key=&page=1&per_page=10&sort=

        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        var dummy struct {
        }
	var get_security_profiles_response GetSecurityProfilesResponse
	//security_profiles := []SecurityProfile{}
	security_profiles_wrt_name := make(map[string]SecurityProfile)

        key := "get_security_profiles_list"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{})

	page_no := 1
	retreivals_per_page := 100
	max_pages_to_avoid_loop := 10

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        queryParam := map[string]interface{}{
                "key":      "",
                "page":     page_no,
                "per_page": retreivals_per_page,
                "sort":     "",
                "user": map[string]interface{}{
                        "username":             cs.Username,
                        "id":                   cs.Id,
                        "role":                 cs.Role,
                        "allow_manage":         cs.AllowManage,
                        "customer_id":          cs.CustomerId,
                        "hex_id":               cs.HexId,
                        "customer_hex_id":      cs.CustomerHexId,
                        "enable_notifications": cs.EnableNotifications,
                },
        }

	for {

            queryParam["page"] = page_no

            resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
            if resp.StatusCode != 200 {
                    return false, errors.New(fmt.Sprintf("Response code %d", resp.StatusCode)), security_profiles_wrt_name
            }
            err_nwgrp := json.NewDecoder(resp.Body).Decode(&get_security_profiles_response)
            if err_nwgrp != nil {
                fmt.Println(err_nwgrp)
            }
            if ok, json_data := convert_struct_to_string_via_json(get_security_profiles_response); ok {
                log.Debug.Printf("response data: %s\n", json_data)
            }

            total_profiles := get_security_profiles_response.TotalCount
	    //security_profiles = append(security_profiles, get_security_profiles_response.SecurityProfiles...)

            for _, security_profile := range get_security_profiles_response.SecurityProfiles {
                security_profiles_wrt_name[security_profile.Name] = security_profile
            }

	    cs.Helper.profiles.security.byName = security_profiles_wrt_name

	    if len(security_profiles_wrt_name) == total_profiles {
		    break
	    }

	    if page_no == max_pages_to_avoid_loop {
		    break
	    }

	    page_no += 1
	}

        return true, nil, security_profiles_wrt_name
}

func (cs *Cloudstation) GetSecurityProfileId(profile_name string) (bool, int) {

    if res, _, security_profiles_wrt_name := cs.GetSecurityProfiles(); res {
	    //fmt.Println("sec prf wrt name", security_profiles_wrt_name)
	    //fmt.Println("proflne anme", profile_name)
        if profile, ok := security_profiles_wrt_name[profile_name]; ok {
		return true, profile.Id
        }
    }
    return false, 0

}


/*
func (cs *Cloudstation) DetachThreatPolicyAtNwGrp(threat_policy_name string, nwgrp_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        acl_id, err1 := cs.GetThreatPolicyId(threat_policy_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Threat Id !")
                return false
        }

        nwobject_id, err2 := cs.Get_NwGrpId(nwgrp_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the NwGrp Object Id !")
                return false
        }

        var detach_nwgrp_payload struct {
                Object_id   int    `json:"object_id"`
                Object_type string `json:"object_type"`
        }

        detach_nwgrp_payload.Object_id = nwobject_id
        detach_nwgrp_payload.Object_type = "NetworkGroup"

        key := "detach_threat_policy_at_networkgrp"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(acl_id)})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_nwgrp_payload, cookie)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}


func (cs *Cloudstation) DetachThreatPolicyPolicyFromSite(threat_policy_name string, site_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        acl_id, err1 := cs.GetThreatPolicyId(threat_policy_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Threat Id !")
                return false
        }

        siteobject_id, err2 := cs.Get_SiteId(site_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the Site Object Id !")
                return false
        }

        var detach_site_payload struct {
                Object_id   string    `json:"object_id"`
                Object_type string `json:"object_type"`
        }

        detach_site_payload.Object_id = strconv.Itoa(siteobject_id)
        detach_site_payload.Object_type = "Site"

        key := "detach_threat_policy_from_site"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(acl_id)})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_site_payload, cookie)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}


func (cs *Cloudstation) DetachThreatPolicyAtSiteProfile(threat_policy_name string, profile_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        acl_id, err1 := cs.GetThreatPolicyId(threat_policy_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Threat Id !")
                return false
        }

        profileobject_id, err2 := cs.Get_ProfileId(profile_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the Profile Object Id !")
                return false
        }

        var detach_profile_payload struct {
                Object_id   int    `json:"object_id"`
                Object_type string `json:"object_type"`
        }

        detach_profile_payload.Object_id = profileobject_id
        detach_profile_payload.Object_type = "Profile"

        key := "detach_threat_policy_at_profile"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(acl_id)})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_profile_payload, cookie)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}



func (cs *Cloudstation) DetachThreatPolicyAtNwGrpById(threat_policy_id int, nwgrp_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        acl_id, err1 := cs.GetThreatPolicyId(threat_policy_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Threat Id !")
                return false
        }

        nwobject_id, err2 := cs.Get_NwGrpId(nwgrp_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the NwGrp Object Id !")
                return false
        }

        var detach_nwgrp_payload struct {
                Object_id   int    `json:"object_id"`
                Object_type string `json:"object_type"`
        }

        detach_nwgrp_payload.Object_id = nwobject_id
        detach_nwgrp_payload.Object_type = "NetworkGroup"

        key := "detach_threat_policy_at_networkgrp"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": threat_policy_id})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_nwgrp_payload, cookie)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}


func (cs *Cloudstation) DetachThreatPolicyPolicyFromSiteById(threat_policy_id int, site_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        acl_id, err1 := cs.GetThreatPolicyId(threat_policy_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Threat Id !")
                return false
        }

        siteobject_id, err2 := cs.Get_SiteId(site_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the Site Object Id !")
                return false
        }

        var detach_site_payload struct {
                Object_id   string    `json:"object_id"`
                Object_type string `json:"object_type"`
        }

        detach_site_payload.Object_id = strconv.Itoa(siteobject_id)
        detach_site_payload.Object_type = "Site"

        key := "detach_threat_policy_from_site"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(threat_policy_id)})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_site_payload, cookie)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}


func (cs *Cloudstation) DetachThreatPolicyAtSiteProfileById(threat_policy_id int, profile_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        acl_id, err1 := cs.GetThreatPolicyId(threat_policy_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Threat Id !")
                return false
        }

        profileobject_id, err2 := cs.Get_ProfileId(profile_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the Profile Object Id !")
                return false
        }

        var detach_profile_payload struct {
                Object_id   int    `json:"object_id"`
                Object_type string `json:"object_type"`
        }

        detach_profile_payload.Object_id = profileobject_id
        detach_profile_payload.Object_type = "Profile"

        key := "detach_threat_policy_at_profile"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": threat_policy_id})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_profile_payload, cookie)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}
*/
