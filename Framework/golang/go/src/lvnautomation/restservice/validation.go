package restservice

import (
	"fmt"
	lvn_log "lvnautomation/lvnlog"
	"lvnautomation/utils"
	"strings"
	"time"
)

/* CheckValidPort Move to utils */
func CheckValidPort(port int) bool {
	return (port >= 0 && port <= 65535)
}

func CheckValidCondition(condition string) bool {
	return Contains([]string{"not", "or"}, strings.ToLower(condition))
}

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

type MatchCriteriaInput struct {
	direction       string
	srcNwgrp        string
	dstNwgrp        string
	apps            []string
	appCategories   []string
	customProtocols []struct {
		protocol  string
		condition string
	}

	from struct {
		subnet []string
		custom struct {
			ip []struct {
				ip        string
				condition string
			}
			port []struct {
				port      int
				condition string
			}
		}
	}
	to struct {
		subnet []string
		custom struct {
			ip []struct {
				ip        string
				condition string
			}
			port []struct {
				port      int
				condition string
			}
		}
	}
}

/*
	Match criteria related setters and getters
*/

func NewMatchCriteriaInput() (bool, error, *MatchCriteriaInput) {
	var matchCriteria MatchCriteriaInput

	matchCriteria.SetDirection("")
	matchCriteria.SetSrcNwGrp("")
	matchCriteria.SetDstNwGrp("")

	apps := []string{}
	matchCriteria.SetApps(apps)

	appcat := []string{}
	matchCriteria.SetAppCategories(appcat)

	matchCriteria.SetCustomProtocol("", "")

	subnetFrom := []string{}
	matchCriteria.SetFromSubnets(subnetFrom)

	matchCriteria.SetFromCustomIp("", "")
	matchCriteria.SetFromCustomPort(0, "")

	subnetTo := []string{}
	matchCriteria.SetToSubnets(subnetTo)

	matchCriteria.SetToCustomIp("", "")
	matchCriteria.SetToCustomPort(0, "")

	//fmt.Println(lvn_log.Debug)
	return true, nil, &matchCriteria
}

func (matchCriteria *MatchCriteriaInput) SetDirection(direction string) (bool, error) {
	direction_map := map[string]string{
		"from lan": "lan_to_wan",
		"from wan": "wan_to_lan",
		"any":      "-1",
	}

	direction = strings.ToLower(direction)

	if !Contains([]string{"from lan", "from wan", "any"}, direction) {
		return false, nil
	}
	matchCriteria.direction = direction_map[direction]
	return true, nil
}

func (matchCriteria *MatchCriteriaInput) SetDirectionAny() (bool, error) {
	return matchCriteria.SetDirection("any")
}

func (matchCriteria *MatchCriteriaInput) SetDirectionFromLan() (bool, error) {
	return matchCriteria.SetDirection("from lan")
}

func (matchCriteria *MatchCriteriaInput) SetDirectionFromWan() (bool, error) {
	return matchCriteria.SetDirection("from wan")
}

func (matchCriteria *MatchCriteriaInput) SetSrcNwGrp(nwgrp string) (bool, error) {
	matchCriteria.srcNwgrp = nwgrp
	return true, nil
}

func (matchCriteria *MatchCriteriaInput) SetSrcNwGrpInternet() (bool, error) {
	return matchCriteria.SetSrcNwGrp("Internet")
}

func (matchCriteria *MatchCriteriaInput) SetSrcNwGrpAnyEnterprise() (bool, error) {
	return matchCriteria.SetSrcNwGrp("Any Enterprise Network Group(s)")
}

func (matchCriteria *MatchCriteriaInput) SetDstNwGrp(nwgrp string) (bool, error) {
	matchCriteria.dstNwgrp = nwgrp
	return true, nil
}

func (matchCriteria *MatchCriteriaInput) SetDstNwGrpInternet() (bool, error) {
	return matchCriteria.SetDstNwGrp("Internet")
}

func (matchCriteria *MatchCriteriaInput) SetDstNwGrpAnyEnterprise() (bool, error) {
	return matchCriteria.SetDstNwGrp("Any Enterprise Network Group(s)")
}


func (matchCriteria *MatchCriteriaInput) SetApps(apps []string) (bool, error) {
	for _, values := range apps {
		matchCriteria.apps = append(matchCriteria.apps, values)
	}
	//fmt.Println("setting apps===", matchCriteria.apps)
	return true, nil
}

func (matchCriteria *MatchCriteriaInput) SetAppCategories(app_categories []string) (bool, error) {
	for _, values := range app_categories {
		matchCriteria.appCategories = append(matchCriteria.appCategories, values)
	}
	return true, nil
}

func (matchCriteria *MatchCriteriaInput) SetCustomProtocol(proto string, cond string) (bool, error) {
	type x struct {
		protocol  string
		condition string
	}
	y := x{
		protocol:  strings.ToLower(proto),
		condition: strings.ToUpper(cond),
	}

	if Contains([]string{"tcp", "udp", "icmp"}, strings.ToLower(proto)) && CheckValidCondition(cond) {
		matchCriteria.customProtocols = append(matchCriteria.customProtocols, y)
		return true, nil
	}
	return false, nil
}

func (matchCriteria *MatchCriteriaInput) SetFromSubnets(subnets []string) (bool, error) {
	for _, values := range subnets {
		matchCriteria.from.subnet = append(matchCriteria.from.subnet, values)
	}
	return true, nil
}

func (matchCriteria *MatchCriteriaInput) SetFromCustomIp(cust_ip string, cond string) (bool, error) {
	type x struct {
		ip        string
		condition string
	}
	y := x{
		ip:        cust_ip,
		condition: strings.ToUpper(cond),
	}
	if ok, _ := utils.ValidateIP(cust_ip); ok && CheckValidCondition(cond) {
		matchCriteria.from.custom.ip = append(matchCriteria.from.custom.ip, y)
		return true, nil
	}
	return false, nil
}

func (matchCriteria *MatchCriteriaInput) SetFromCustomPort(cust_port int, cond string) (bool, error) {
	type x struct {
		port      int
		condition string
	}
	y := x{
		port:      cust_port,
		condition: cond,
	}
	if CheckValidPort(cust_port) && CheckValidCondition(cond) {
		matchCriteria.from.custom.port = append(matchCriteria.from.custom.port, y)
		return true, nil
	}
	return false, nil
}

func (matchCriteria *MatchCriteriaInput) SetToSubnets(subnets []string) (bool, error) {
	for _, values := range subnets {
		matchCriteria.to.subnet = append(matchCriteria.to.subnet, values)
	}
	return true, nil
}

func (matchCriteria *MatchCriteriaInput) SetToCustomIp(cust_ip string, cond string) (bool, error) {
	type x struct {
		ip        string
		condition string
	}
	y := x{
		ip:        cust_ip,
		condition: strings.ToUpper(cond),
	}
	if ok, _ := utils.ValidateIP(cust_ip); ok && CheckValidCondition(cond) {
		matchCriteria.to.custom.ip = append(matchCriteria.to.custom.ip, y)
		return true, nil
	}
	return false, nil
}

func (matchCriteria *MatchCriteriaInput) SetToCustomPort(cust_port int, cond string) (bool, error) {
	type x struct {
		port      int
		condition string
	}
	y := x{
		port:      cust_port,
		condition: cond,
	}
	if CheckValidPort(cust_port) && CheckValidCondition(cond) {
		matchCriteria.to.custom.port = append(matchCriteria.to.custom.port, y)
		return true, nil
	}
	return false, nil
}

type CreateAclInputs struct {
	Name            string
	priority        int
	tag             string
	action          string
	connectionstate string
	MatchCriteriaInput
}

/*
	ACL related setters and getters
*/

func NewAclInput() (bool, error, *CreateAclInputs) {
	var aclInput CreateAclInputs
	var name string = "lvn_auto_" + time.Now().Format("2006-01-02-15-04-05")
	aclInput.SetAclName(name)
	aclInput.SetAclPriority(0)
	aclInput.SetAclAction("")
	aclInput.SetAclConnectionState("")
	_, _, matchcr := NewMatchCriteriaInput()
	aclInput.MatchCriteriaInput = *matchcr
	return true, nil, &aclInput
}

func (acl *CreateAclInputs) SetAclName(name string) (bool, error) {
	acl.Name = name
	return true, nil
}

func (acl *CreateAclInputs) SetAclPriority(priority int) (bool, error) {

	if priority >= 0 && priority <= 1000 {
		acl.priority = priority
		return true, nil
	}
	return false, nil
}

func (acl *CreateAclInputs) SetAclTag(tag string) (bool, error) {
	acl.tag = tag
	return true, nil
}

func (acl *CreateAclInputs) SetAclAction(action string) (bool, error) {
	action = strings.ToLower(action)

	actionmap := map[string]string{
		"allow": "Allow",
		"deny":  "Deny",
	}

	if !Contains([]string{"deny", "allow"}, action) {
		return false, nil
	}

	acl.action = actionmap[action]
	return true, nil
}

func (acl *CreateAclInputs) SetAclActionAllow() (bool, error) {
	return acl.SetAclAction("allow")
}

func (acl *CreateAclInputs) SetAclActionDeny() (bool, error) {
	return acl.SetAclAction("deny")
}

func (acl *CreateAclInputs) SetAclConnectionState(connection_state string) (bool, error) {
	connection_state = strings.ToLower(connection_state)

	connection_state_map := map[string]string{
		"any":      "Any",
		"new":      "New",
		"existing": "Existing",
	}

	if !Contains([]string{"any", "new", "existing"}, connection_state) {
		return false, nil
	}

	acl.connectionstate = connection_state_map[connection_state]
	return true, nil
}

func (acl *CreateAclInputs) SetAclConnectionStateAny() (bool, error) {
	return acl.SetAclConnectionState("any")
}

func (acl *CreateAclInputs) SetAclConnectionStateNew() (bool, error) {
	return acl.SetAclConnectionState("new")
}

func (acl *CreateAclInputs) SetAclConnectionStateExisting() (bool, error) {
	return acl.SetAclConnectionState("existing")
}

func (acl *CreateAclInputs) SetMatchCriteria(m *MatchCriteriaInput) {
	acl.MatchCriteriaInput = *m
}

func (acl *CreateAclInputs) GetAclPriority() int {
	return acl.priority
}

func (acl *CreateAclInputs) GetAclTag() string {
	return acl.tag
}

func (acl *CreateAclInputs) GetAclAction() string {
	return acl.action
}

func (acl *CreateAclInputs) GetAclConnectionState() string {
	return acl.connectionstate
}

/*type CreateQosInputs struct {
	Name           string
	priority       int
	tag            string
	dscp_code      int
	dscp_remarking bool
	qos_action     string
	MatchCriteriaInput
}*/

/*
QOS related setters and getters
*/

/*func NewQosInput() (bool, error, *CreateQosInputs) {
	var qosInput CreateQosInputs
	var name string = "lvn_auto_" + time.Now().Format("2006-01-02-15-04-05")
	var isBool bool
	qosInput.SetQosName(name)
	qosInput.SetQosPriority(0)
	qosInput.SetQosAction("")
	qosInput.SetQosDscpCode(0)
	qosInput.SetQosDscpRemarking(isBool)
	_, _, matchcr := NewMatchCriteriaInput()
	qosInput.MatchCriteriaInput = *matchcr
	return true, nil, &qosInput
}

func (qos *CreateQosInputs) SetQosName(name string) (bool, error) {
	qos.Name = name
	return true, nil
}


func (qos *CreateQosInputs) SetQosPriority(priority int) (bool, error) {
	if priority >= 0 && priority <= 1000 {
		qos.priority = priority
		return true, nil
	}
	return false, nil
}

func (qos *CreateQosInputs) SetQosAction(action string) (bool, error) {
	act = strings.ToLower(action)

	actionmap := map[string]string{
		"critical": "Critical",
		"high": "High",
		"medium": "Medium",
		"low": "Low",
	}

}*/

type createCustomerInput struct {
    Customer_name string
    Sys_admin_email string
    Net_admin_email string
    Url_prefix string
    Domain string
    Managed_by string
    License_name string
    Network_size int
    Admin_assist_tickets int
    License_key string
}

func NewCreateCustomerInput() (*createCustomerInput) {

    create_customer_input := createCustomerInput{
        Domain: "devcloudstation.io",
        Managed_by: "Deepak Lal",
        License_name: "cloudstation_pro",
        Network_size: 1000,
        Admin_assist_tickets: 1000,
        License_key: "",
    }

    return &create_customer_input
}


/*
* Match criteria related setters and getters
 */
type CreateSiteInput struct {
	Name   string
	User   string
	Is_hub bool
}

func NewSiteInput() (bool, error, *CreateSiteInput) {
	var Site CreateSiteInput
	Site.Name = ""
	Site.User = ""
	Site.Is_hub = false
	return true, nil, &Site
}

type DeviceInput struct {
	Authentication struct {
		Local struct {
			Password string
			Username string
		}
		Type string
	}
	hardwareAttachmentType string
	hardwarePort           string
	IsHardwareAttached     bool
	ModelType              string
	Name                   string
	snumber                string
	serialNumber           string
	SiteName               string
}

func NewDeviceInput() (bool, error, *DeviceInput) {
	var Device DeviceInput
	Device.Authentication.Local.Password = ""
	Device.Authentication.Local.Username = ""
	Device.Authentication.Type = "UUID"
	Device.IsHardwareAttached = true
	Device.SetHardwareAttachmentType("custom")
	Device.SetHardwarePort([]string{"eth0", "eth1", "eth2", "eth3"})
	Device.ModelType = ""
	Device.Name = ""
	Device.SetSerialNumber("")
	Device.SetUUID("")
	Device.SiteName = ""
	return true, nil, &Device
}

func (Device *DeviceInput) SetHardwareAttachmentType(Type string) (bool, error) {
	str := strings.ToLower(Type)
	if Device.IsHardwareAttached == false {
		if str == "generic_100m" {
			Device.hardwareAttachmentType = "generic_100m"
			return true, nil
		} else {
			return false, fmt.Errorf("Bad combination of HardwareAttachmentType and IsHardwareAttached")

		}
	} else {
		if str == "custom" || str == "" {
			Device.hardwareAttachmentType = "custom"
			return true, nil
		} else {
			return false, fmt.Errorf("Bad combination of HardwareAttachmentType and IsHardwareAttached")
		}
	}


}

func (Device *DeviceInput) SetHardwarePort(Port []string) (bool, error) {
	if Device.IsHardwareAttached == true {
		Device.hardwarePort = strings.Join(Port, ",")
		return true, nil
	} else {
		return false, fmt.Errorf("Band Conbimation of Hardwareport and IsHardwareAttached")
	}
}

func (Device *DeviceInput) GetSerialNumber() string {
	return Device.serialNumber
}

func (Device *DeviceInput) GetUUID() string {
	return Device.snumber
}

func (Device *DeviceInput) SetSerialNumber(Serial string) (bool, error) {
	if Device.GetUUID() == "" {
		Device.serialNumber = Serial
		return true, nil
	} else {
		return false, fmt.Errorf("Bad combination of UUID and Serial Number")
	}
}

func (Device *DeviceInput) SetUUID(UUID string) (bool, error) {
	if Device.GetSerialNumber() == "" {
		Device.snumber = UUID
		return true, nil
	} else {
		return false, fmt.Errorf("Bad combination of UUID and Serial Number")
	}
}

func (Device *DeviceInput) GetHardwareAttachmentType() string {
	return Device.hardwareAttachmentType
}

func (Device *DeviceInput) GetHardwarePort() string {
	return Device.hardwarePort
}

//		res, _, acl_input = rest_service.NewAclInput()
//		_, _, match_criteria_input := rest_service.NewMatchCriteriaInput()
type LdapInput struct{
		        Admin_pw string
			      Admin_usrname string
			      Bind_type string
			      Cmi string
			      Dist_name string
			      Enable bool
			      En_sec_conn bool
			      Ldap_name string
			      Server_ip string
			      Server_port int
}

type DhcpServerInput struct{
	CpeName string
	InterfaceName string
	Dns string
	StartIP string
	StopIP string
}

func NewDhcpServerInput() (bool, error, *DhcpServerInput){
	//lvn_log.Debug.Printf("New DHCP server input...\n")
	lvn_log.Debug.Println()
	var NewDhcp DhcpServerInput
	NewDhcp.CpeName = ""
	NewDhcp.InterfaceName ="lan0"
	NewDhcp.Dns = "8.8.8.8"
	NewDhcp.StartIP = ""
	NewDhcp.StopIP = ""
	return true, nil, &NewDhcp
}
