package restservice

import (
	"fmt"
	"net/http"
	//"errors"
	"encoding/json"
	//"regexp"
	"strconv"
	//"strings"
	//"bytes"
	//"compress/gzip"
	//"io"

	log "lvnautomation/lvnlog"
	//"time"
	//tc_utils "lvnautomation/testcaseutils"
)

type url_profile struct {
	Name string `json:"name"`
	CategoryType        string `json:"category_type"`
	Categories       *[]string `json:"categories"`
	DenyCategories    []string `json:"denyCategories"`
	Domains  	     *[]string `json:"domains"`
	PermitDomain      	string `json:"permitDomain"`
    PermitDomains 	  []Permittdomains `json:"permitDomains"`
	PermitUrls  	 *[]string `json:"permitUrls"`
	PermittedDomains  []string `json:"permittedDomains"`
	PermittedUrls  	 *[]string `json:"permittedUrls"`
	RejectDomain      	string `json:"rejectDomain"`
    RejectDomains 	  []Rejecttdomains `json:"rejectDomains"`
	RejectUrls  	 *[]string `json:"rejectUrls"`
	RejectedDomains   []string `json:"rejectedDomains"`
	RejectedUrls  	 *[]string `json:"rejectedUrls"`
	Urls 		 	 *[]string `json:"urls"`
}

/*type permitdomains struct{
	Name 	string `json:"name"`
	Ticked 	bool `json:"ticked"`
}

type rejectdomains struct{
	Name 	string `json:"name"`
	Ticked 	bool `json:"ticked"`
}
*/
type user struct {
    Customer_id int `json:"customer_id"`
    Id int `json:"id"`
}

func (cs *Cloudstation) CreateWebFilterInput(webfilterinputs *CreateWebFilterInput) bool {

	var webfilter_payload struct {
        UrlProfiles url_profile `json:"url_profiles"`
        User user `json:"user"`
    }

	webfilter_payload.UrlProfiles.Name         	= webfilterinputs.Name
	webfilter_payload.UrlProfiles.CategoryType 	= webfilterinputs.CategoryType
	webfilter_payload.UrlProfiles.Categories   	= webfilterinputs.Categories
	webfilter_payload.UrlProfiles.DenyCategories   = webfilterinputs.DenyCategories
	webfilter_payload.UrlProfiles.Domains     		= webfilterinputs.Domains
	webfilter_payload.UrlProfiles.PermitDomain 		= webfilterinputs.PermitDomain
	webfilter_payload.UrlProfiles.PermitUrls  		= webfilterinputs.PermitUrls
	webfilter_payload.UrlProfiles.PermittedDomains  = webfilterinputs.PermittedDomains
	webfilter_payload.UrlProfiles.PermittedUrls 	= webfilterinputs.PermittedUrls
	webfilter_payload.UrlProfiles.RejectDomain  	= webfilterinputs.RejectDomain
	webfilter_payload.UrlProfiles.RejectUrls     	= webfilterinputs.RejectUrls
	webfilter_payload.UrlProfiles.RejectedDomains 	= webfilterinputs.RejectedDomains
	webfilter_payload.UrlProfiles.RejectedUrls  	= webfilterinputs.RejectedUrls
	webfilter_payload.UrlProfiles.Urls  			= webfilterinputs.Urls
	webfilter_payload.User.Customer_id = cs.CustomerId
	webfilter_payload.User.Id = cs.Id


	for _, val := range webfilterinputs.PermitDomains {
/*		y := permitdomains{
			Name: val.Name,
			Ticked: true
		}
*/		webfilter_payload.UrlProfiles.PermitDomains = append(webfilter_payload.UrlProfiles.PermitDomains, val)
	}

	for _, val := range webfilterinputs.RejectDomains {
/*		y := rejectdomains{
			Name: val,
			Ticked: true
		}
*/		webfilter_payload.UrlProfiles.RejectDomains = append(webfilter_payload.UrlProfiles.RejectDomains, val)
	}



	//fmt.Println("************************", webfilter_payload, "***************************")

	key := "conf_webfilter"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	/*policyjson, _ := json.Marshal(Aclpol_payload)
	err := ioutil.WriteFile("output1.json", policyjson, 0644)
	//fmt.Println(err)*/

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, webfilter_payload, cookie)
	//fmt.Println("Status Conf_webfilter= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println()
	return true
}


type urlpro struct{
	Category_type string `json:"category_type"`
	DenyCategories []string `json:"denyCategories"`
	Hex_id string `json:"hex_id"`
	Id int `json:"id"`
	Name string `json:"name"`
	PermittedDomains []string `json:"permittedDomains"`
	PermittedUrls []string `json:"permittedUrls"`
	RejectedDomains []string `json:"rejectedDomains"`
	RejectedUrls []string `json:"rejectedUrls"`
	UpdatedAt string `json:"updated_at"`
}


type URLPOlICY struct{
	Url_profiles []urlpro `json:"url_profiles"`
	Total_count int `json:"total_count"`
}

func (cs *Cloudstation) GetUrlprofile() ([]URLPOlICY, bool) {

    var dummy struct {
	}

	key := "geturlprofile"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

	var urlpolicy_ls []URLPOlICY
	var urlpolicy URLPOlICY
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//fmt.Println(resp)
	//fmt.Println("Status Get_UrlPolicy of page 1= ", resp.StatusCode)
	if resp.StatusCode != 200 {
		return urlpolicy_ls, false
	}
	err_policy := json.NewDecoder(resp.Body).Decode(&urlpolicy)
	if err_policy != nil {
		fmt.Println(err_policy)
	}
	urlpolicy_ls = append(urlpolicy_ls, urlpolicy)

	tot_entry := urlpolicy.Total_count
	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}

	i := 2
	for i <= tot_page {
		var urlpolicy1 URLPOlICY
		queryParam = map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		//fmt.Println(resp)
		//fmt.Printf("Status Get_SiteList of page %d= %d\n", i, resp.StatusCode)
		if resp.StatusCode != 200 {
			return urlpolicy_ls, false
		}
		err_urlpolicy := json.NewDecoder(resp.Body).Decode(&urlpolicy1)
		if err_urlpolicy != nil {
			fmt.Println(err_urlpolicy)
		}
		urlpolicy_ls = append(urlpolicy_ls, urlpolicy1)
		i++
	}
	//fmt.Println()
	return urlpolicy_ls, true
}


func (cs *Cloudstation) GetUrlProfileId(urlprofile_name string) (int, bool) {
	urlprofile_ls, err1 := cs.GetUrlprofile()
	if err1 == false {
		fmt.Println("Failed to fetch the SITE List !", err1)
		return 0, false
	}

//	var urlpolicy_ls []URLPOlICY
	var urlprofile_id int
	var found_urlprofile_id bool

	for i := 0; i < len(urlprofile_ls); i++ {
		for j := 0; j < 10; j++ {
			if urlprofile_ls[i].Url_profiles[j].Name == urlprofile_name {
				urlprofile_id = urlprofile_ls[i].Url_profiles[j].Id
				found_urlprofile_id = true
				break
			}
		}
		if found_urlprofile_id == true {
			break
		}
	}
	if found_urlprofile_id == false {
		fmt.Println("No urlprofile Id found !")
		return 0, false
	}

	return urlprofile_id, true
}


//----------------ATTACHING URL POlICY-------------------------------


type UrlPolicies struct {
	CidrIp 			string 		`json:"cidr_ip"`
	CidrSubnet	    string 		`json:"cidr_subnet"`
	Cidrs       	[]Cidrs 	`json:"cidrs"`
	FromIp    		string 		`json:"from_ip"`
	IonId 			int 		`json:"ion_id"`
	Ip 				string 		`json:"ip"`
    IpRanges 		[]IpRanges 	`json:"ipRanges"`
	Ips  	 		[]Ips 		`json:"ips"`
	Name 			string 		`json:"name"`
	Priority 		int 		`json:"priority"`
	ProfileId 		int  		`json:"profile_id"`
    Profiles 		[]urlpro 	`json:"profiles"`
	SelectedCidrs 	[]string 	`json:"selectedCidrs"`
	SelectedIpRanges []string 	`json:"selectedIpRanges"`
	SelectedIps 	[]string 	`json:"selectedIps"`
	To_ip 			string 		`json:"to_ip"`
}
/*
type user struct {
    Customer_id int `json:"customer_id"`
    Id int `json:"id"`
}
*/
func (cs *Cloudstation) AttachWebFilterPolicyAtSite(site_name string, attwfpolicy *AttachUrlPolicies, profile_name string) bool {

	ion_id, err1 := cs.Get_SiteId(site_name)
	if err1 == false {
		fmt.Println("No Ion Id found !")
		return false
	}

	profile_id, err2 := cs.GetUrlProfileId(profile_name)
	if err2 == false {
		fmt.Println("No Profile Id found !")
		return false
	}
	var wfpolicy_payload struct {
        UrlPolicies UrlPolicies `json:"url_policies"`
        User user `json:"user"`
    }

    wfpolicy_payload.UrlPolicies.CidrIp 	= attwfpolicy.CidrIp
    wfpolicy_payload.UrlPolicies.CidrSubnet = attwfpolicy.CidrSubnet
    wfpolicy_payload.UrlPolicies.FromIp 	= attwfpolicy.FromIp
    wfpolicy_payload.UrlPolicies.IonId 		= ion_id
    wfpolicy_payload.UrlPolicies.Ip 		= attwfpolicy.Ip
    wfpolicy_payload.UrlPolicies.Name 		= attwfpolicy.Name
    wfpolicy_payload.UrlPolicies.Priority 	= attwfpolicy.Priority
    wfpolicy_payload.UrlPolicies.ProfileId 	= profile_id
    wfpolicy_payload.UrlPolicies.SelectedCidrs    = attwfpolicy.SelectedCidrs
    wfpolicy_payload.UrlPolicies.SelectedIpRanges = attwfpolicy.SelectedIpRanges
    wfpolicy_payload.UrlPolicies.SelectedIps 	  = attwfpolicy.SelectedIps
    wfpolicy_payload.UrlPolicies.To_ip = attwfpolicy.To_ip
    wfpolicy_payload.User.Customer_id = cs.CustomerId
	wfpolicy_payload.User.Id = cs.Id

	for _, val := range attwfpolicy.Cidrs {
		wfpolicy_payload.UrlPolicies.Cidrs = append(wfpolicy_payload.UrlPolicies.Cidrs, val)
	}

	for _, val := range attwfpolicy.IpRanges {
		wfpolicy_payload.UrlPolicies.IpRanges = append(wfpolicy_payload.UrlPolicies.IpRanges, val)
	}

	for _, val := range attwfpolicy.Ips {
		wfpolicy_payload.UrlPolicies.Ips = append(wfpolicy_payload.UrlPolicies.Ips, val)
	}

	/*urlprofile_ls, err3 := cs.GetUrlprofile()
	if err3 == false {
		//fmt.Println("Failed to fetch the SITE List !", err3)
		return false
	}

	for _, val := range urlprofile_ls {
		wfpolicy_payload.UrlPolicies.Profiles = append(wfpolicy_payload.UrlPolicies.Profiles, val)
	}
*/
	//fmt.Println("************************", wfpolicy_payload, "***************************")

	key := "conf_attachwfpolicy"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	/*policyjson, _ := json.Marshal(Aclpol_payload)
	err := ioutil.WriteFile("output1.json", policyjson, 0644)
	//fmt.Println(err)*/

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, wfpolicy_payload, cookie)
	//fmt.Println("Status conf_attachwfpolicy= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println()
	return true

}

func (cs *Cloudstation) DeleteWebFilter(urlprofile string) bool {
	profileid, err1 := cs.GetUrlProfileId(urlprofile)
	if err1 == false {
		log.Err.Println("Failed to fetch the PROFILE ID !", err1)
		return false
	}

	var deletecustom_payload struct {
	}
	key := "delete_webfilter"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(profileid)})
	//fmt.Println(substitutedPath)

	header := cs.Construct_headers(apis[key].headers.headers)
	//fmt.Println(header)
	//var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	_, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, deletecustom_payload, cookie)
	//fmt.Println("Status Del_App= ", resp.StatusCode)
	//fmt.Println(resp)
	/*if resp.StatusCode != 200 {
		return false
	}*/
	//fmt.Println("--------------Got into the Verse for DELETE WEBFILTER !--------------")
	//fmt.Println()
	return true
}

//-----------------------------GET ATTACHED WFPOLICY LIST-------------------------------

type atturl_policies struct{
	Hex_id string `json:"hex_id"`
	Id int `json:"id"`
	Name string `json:"name"`
	Priority int `json:"priority"`
	Site_id string `json:"site_id"`
	Url_profile_hex_id string `json:"url_profile_hex_id"`
	Url_profile_id int `json:"url_profile_id"`
	Url_profile_name string `json:"url_profile_name"`
}

type atturlmembers struct{
	Cidr []string `json:"cidr"`
	Ip []string `json:"ip"`
	IpRange []string `json:"ip_range"`
}

type ATTACHEDPOLICY struct{
	Mesasge string `json:"message"`
//	UrlMembers []atturlmembers `json:"url_members"`
	UrlPolicies []atturl_policies `json:"url_policies"`
}

func (cs *Cloudstation) GetAttachedWebFilterPolicyListAtSite(sitename string) ([]ATTACHEDPOLICY, bool) {

    var dummy struct {
	}

	var attachpolicy_ls []ATTACHEDPOLICY
	var attachpolicy ATTACHEDPOLICY

	siteid, err1 := cs.Get_SiteId(sitename)
	if err1 == false {
		fmt.Println("No Ion Id found !")
		return attachpolicy_ls, false
	}

	key := "get_attached_wfpolicy_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(siteid)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return attachpolicy_ls, false
	}
	err_policy := json.NewDecoder(resp.Body).Decode(&attachpolicy)
	if err_policy != nil {
		fmt.Println(err_policy)
	}
	attachpolicy_ls = append(attachpolicy_ls, attachpolicy)

	//fmt.Println()
	return attachpolicy_ls, true
}

//---------------------------GET ATTACHED WF_URLPROFILE ID---------------------------------

func (cs *Cloudstation) Get_attach_urlprofileid(attach_urlprofile_name string, sitename string) (int, bool) {
	attach_urlprofile_ls, err1 := cs.GetAttachedWebFilterPolicyListAtSite(sitename)
	if err1 == false {
		fmt.Println("Failed to fetch the SITE List !", err1)
		return 0, false
	}

//	var urlpolicy_ls []URLPOlICY
	var attach_urlprofile_id int
	var found_attach_urlprofile_id bool
	
	for i := 0; i < len(attach_urlprofile_ls); i++ {
		for j := 0; j < len(attach_urlprofile_ls[i].UrlPolicies); j++ {
			if attach_urlprofile_ls[i].UrlPolicies[j].Name == attach_urlprofile_name {
				attach_urlprofile_id = attach_urlprofile_ls[i].UrlPolicies[j].Id
				found_attach_urlprofile_id = true
				break
			}
		}
		if found_attach_urlprofile_id == true {
			break
		}
	}
	if found_attach_urlprofile_id == false {
		fmt.Println("No attach_urlprofile Id found !")
		return 0, false
	}

	return attach_urlprofile_id, true
}

//----------------------------- Detach WEBFILTER FROM SITE ----------------------------

func (cs *Cloudstation) DetachWebFilterFromSite(sitename string, urlprofile string) bool {

	attach_profileid, err1 := cs.Get_attach_urlprofileid(urlprofile, sitename)
	if err1 == false {
		fmt.Println("Failed to fetch the PROFILE ID !", err1)
		return false
	}

	var deletecustom_payload struct {
	}
	key := "detach_wfpolicy"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(attach_profileid)})
	//fmt.Println(substitutedPath)

	header := cs.Construct_headers(apis[key].headers.headers)
	//fmt.Println(header)
	//var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	_, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, deletecustom_payload, cookie)
	//fmt.Println("Status Del_App= ", resp.StatusCode)
	//fmt.Println(resp)
	/*if resp.StatusCode != 200 {
		return false
	}*/
	//fmt.Println("--------------Got into the Verse for DELETE WEBFILTER !--------------")
	//fmt.Println()
	return true
}

