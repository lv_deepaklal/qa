package restservice

import (
	"time"
)

type CreateWebFilterInput struct {
	Name       			string
	CategoryType        string
	Categories       *[]string
	DenyCategories    []string
	Domains  	     *[]string
	PermitDomain      	string
    PermitDomains 	  []Permittdomains
	PermitUrls  	 *[]string
	PermittedDomains  []string
	PermittedUrls  	 *[]string
	RejectDomain      	string
    RejectDomains 	  []Rejecttdomains
	RejectUrls  	 *[]string
	RejectedDomains   []string
	RejectedUrls  	 *[]string
	Urls 		 	 *[]string
}

type Permittdomains struct{
	Name 	string
	Ticked 	bool
}

type Rejecttdomains struct{
	Name 	string
	Ticked 	bool
}

type userr struct {
    Customer_id int `json:"customer_id"`
    Id int `json:"id"`
}

func (webfilter *CreateWebFilterInput) SetWebFiltetrName(name string) (bool, error) {
	webfilter.Name = name
	return true, nil
}

func (webfilter *CreateWebFilterInput) SetWFCategoryType(categorytype string) (bool, error) {
	if categorytype == "deny" || categorytype ==  "allow" {
		webfilter.CategoryType = categorytype
		return true, nil
	}
	return false, nil
}

func (webfilter *CreateWebFilterInput) SetWFDenyCategories(denycategories []string) (bool, error) {
	for _, values := range denycategories {
		webfilter.DenyCategories = append(webfilter.DenyCategories, values)
	}
	return true, nil
}

func (webfilter *CreateWebFilterInput) SetWFPermittedDomains(permitteddomains []string) (bool, error) {
	for _, values := range permitteddomains {
		webfilter.PermittedDomains = append(webfilter.PermittedDomains, values)
	}
	return true, nil
}

func (webfilter *CreateWebFilterInput) SetWFRejectedDomains(rejecteddomains []string) (bool, error) {
	for _, values := range rejecteddomains {
		webfilter.RejectedDomains = append(webfilter.RejectedDomains, values)
	}
	return true, nil
}

func (webfilter *CreateWebFilterInput) SetWFPermitDomains(pmitdomains []Permittdomains) (bool, error) {
	webfilter.PermitDomains = append(webfilter.PermitDomains, pmitdomains...)
	return true, nil
}


func (webfilter *CreateWebFilterInput) SetWFRejectDomains(rjctdomains []Rejecttdomains) (bool, error) {
	webfilter.RejectDomains = append(webfilter.RejectDomains, rjctdomains...)
	return true, nil
}

func NewWebFilterInput() (bool, error, *CreateWebFilterInput) {
	var webfilterInput CreateWebFilterInput
	var name string = "lvn_auto_" + time.Now().Format("2006-01-02-15-04-05")
	webfilterInput.SetWebFiltetrName(name)
	webfilterInput.SetWFCategoryType("deny")

	wfdenycat := []string{}
	webfilterInput.SetWFDenyCategories(wfdenycat)

	wfpermittedcat := []string{}
	webfilterInput.SetWFPermittedDomains(wfpermittedcat)

	wfrejectedcat := []string{}
	webfilterInput.SetWFRejectedDomains(wfrejectedcat)

/*	type S struct {
        Name := ""
        Ticked := true
	}
	var wfpermitdom S
	webfilterInput.SetWFPermitDomains(wfpermitdom)
	webfilterInput.SetWFRejectDomains(wfpermitdom)
*/
	return true, nil, &webfilterInput
}


type AttachUrlPolicies struct {
	CidrIp 			string
	CidrSubnet	    string
	Cidrs       	[]Cidrs
	FromIp    		string
	IonId 			int
	Ip 				string
    IpRanges 		[]IpRanges
	Ips  	 		[]Ips
	Name 			string
	Priority 		int
	ProfileId 		int
        Profiles 		[]Urlpro
	SelectedCidrs 	[]string
	SelectedIpRanges []string
	SelectedIps 	[]string
	To_ip 			string
}

type Cidrs struct{
	Name 	string
	Ticked 	bool
}

type IpRanges struct{
	Name 	string
	Ticked 	bool
}

type Ips struct{
	Name 	string
	Ticked 	bool
}

type Urlpro struct{
	Category_type string
	DenyCategories []string
	Hex_id string
	Id int
	Name string
	PermittedDomains []string
	PermittedUrls []string
	RejectedDomains []string
	RejectedUrls []string
	UpdatedAt string
}

func (attchurlpol *AttachUrlPolicies) SetAttachUrlPolicyName(name string) (bool, error) {
	attchurlpol.Name = name
	return true, nil
}

func (attchurlpol *AttachUrlPolicies) SetUrlPolicyPriority(pri int) (bool, error) {
	attchurlpol.Priority = pri
	return true, nil
}

func (attchurlpol *AttachUrlPolicies) SetCidrs(cidrsa []Cidrs) (bool, error) {
	attchurlpol.Cidrs = append(attchurlpol.Cidrs, cidrsa...)
	return true, nil
}

func (attchurlpol *AttachUrlPolicies) SetIpRanges(iprangesa []IpRanges) (bool, error) {
	attchurlpol.IpRanges = append(attchurlpol.IpRanges, iprangesa...)
	return true, nil
}

func (attchurlpol *AttachUrlPolicies) SetIps(ipsa []Ips) (bool, error) {
	attchurlpol.Ips = append(attchurlpol.Ips, ipsa...)
	return true, nil
}

func (attchurlpol *AttachUrlPolicies) SetSelectedCidrs(selcidrs []string) (bool, error) {
	for _, values := range selcidrs {
		attchurlpol.SelectedCidrs = append(attchurlpol.SelectedCidrs, values)
	}
	return true, nil
}

func (attchurlpol *AttachUrlPolicies) SetSelectedIpRanges(seliprange []string) (bool, error) {
	for _, values := range seliprange {
		attchurlpol.SelectedIpRanges = append(attchurlpol.SelectedIpRanges, values)
	}
	return true, nil
}

func (attchurlpol *AttachUrlPolicies) SetSelectedIps(selips []string) (bool, error) {
	for _, values := range selips {
		attchurlpol.SelectedIps = append(attchurlpol.SelectedIps, values)
	}
	return true, nil
}

func NewAttachwfpolicy() (bool, error, *AttachUrlPolicies) {
	var wfpolicy AttachUrlPolicies
	var name string = "lvn_auto_" + time.Now().Format("2006-01-02-15-04-05")
	wfpolicy.SetAttachUrlPolicyName(name)

	selcidr := []string{}
	wfpolicy.SetSelectedCidrs(selcidr)

	seliprange := []string{}
	wfpolicy.SetSelectedIpRanges(seliprange)

	selip := []string{}
	wfpolicy.SetSelectedIps(selip)

	return true, nil, &wfpolicy
}
