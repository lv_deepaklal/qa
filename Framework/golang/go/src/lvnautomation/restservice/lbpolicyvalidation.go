package restservice

import (
	"fmt"
)

type LBAction struct {
        Wan_interface string
        Wan_priority string
}

type CreateLbInputs struct {
    Name string
    priority int
    tag string
    Action []LBAction
    MatchCriteriaInput
}

func NewLbInput() (*CreateLbInputs) {
    var lbInput CreateLbInputs
    var name string
    lbInput.SetLbName(name)
    lbInput.SetLbPriority(0)
    lbInput.SetLbTag("")
    lbInput.Action = make([]LBAction, 0, 8)
    _,_, matchcr := NewMatchCriteriaInput()
    lbInput.MatchCriteriaInput = *matchcr
    return &lbInput
}


func (lb *CreateLbInputs) SetLbName(name string) {
    lb.Name = name
}

func (lb *CreateLbInputs) SetLbPriority(priority int) {

    if priority >= 0 && priority <= 1000 {
        lb.priority = priority
    } else {
        lb.priority = 0
    }
}

func (lb *CreateLbInputs) SetLbTag(tag string) {
    lb.tag = tag
}


func (lb *CreateLbInputs) GetLbPriority() int {
    return lb.priority
}

func (lb *CreateLbInputs) GetLbTag() string {
    return lb.tag
}

func (lb *CreateLbInputs) SetLbAction(wan string, priority int) {
    lb.Action = append(lb.Action, LBAction{Wan_interface: wan, Wan_priority: fmt.Sprintf("%d", priority)})
}

func (lb *CreateLbInputs) SetMatchCriteria(m *MatchCriteriaInput) {
        lb.MatchCriteriaInput = *m
}
