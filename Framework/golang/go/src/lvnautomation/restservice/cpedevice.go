package restservice

import (
	"fmt"
	"net/http"
	"errors"
	"encoding/json"
	log "lvnautomation/lvnlog"
	tc_utils "lvnautomation/testcaseutils"
)

type GetDeviceListResponseDeviceInfo struct {
        Cnid string `json:"cnid"`
        Id int `json:"id"`
        SerialNo string `json:"serial_no"`
        S_Number string `json:"s_number"`
        Name string `json:"name"`
        SiteId int `json:"site_id"`
        Site SiteInfo `json:"site"`
}

type GetDeviceListResponse struct {
        Cloudports []GetDeviceListResponseDeviceInfo `json:"cloudports"`
        TotalCount int `json:"total_count"`
}

func (cs *Cloudstation) AddDevice(Device *DeviceInput) (error, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var AddDevicePayload struct {
		Authentication struct {
			Local struct {
				Password string `json:"password"`
				Username string `json:"username"`
			} `json:"local"`
			Type string `json:"type"`
		} `json:"authentication"`
		HardwareAttachmentType string `json:"hardware_attachment_type"`
		HardwarePort           string `json:"hardware_port"`
		IsHardwareAttached     bool   `json:"is_hardware_attached"`
		ModelType              string `json:"model_type"`
		Name                   string `json:"name"`
		Snumber                string `json:"s_number"`
		SerialNumber           string `json:"serial_no"`
		SiteId                 int    `json:"site_id"`
	}

	AddDevicePayload.Authentication.Local.Username = Device.Authentication.Local.Username
	AddDevicePayload.Authentication.Local.Password = Device.Authentication.Local.Password
	AddDevicePayload.Authentication.Type = Device.Authentication.Type
	AddDevicePayload.HardwareAttachmentType =   Device.GetHardwareAttachmentType()
	AddDevicePayload.HardwarePort = Device.GetHardwarePort()
	AddDevicePayload.IsHardwareAttached = Device.IsHardwareAttached
	AddDevicePayload.ModelType = Device.ModelType
	AddDevicePayload.Name = Device.Name
	AddDevicePayload.Snumber = Device.GetUUID()
	AddDevicePayload.SerialNumber = Device.GetSerialNumber()
	isFound, errList, Siteid := cs.GetSiteID(Device.SiteName)
	//fmt.Println(Siteid)
	log.Debug.Printf(" Site ID : %d\n", Siteid)
	if isFound == false {
		//return errList, false
		log.Err.Println(errList, false)
	}
	AddDevicePayload.SiteId = Siteid

	key := "add_device"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
        header := cs.Construct_headers(apis[key].headers.headers)
	//fmt.Println(header)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{}

        payload_str, marshal_err := json.Marshal(AddDevicePayload)

        if marshal_err == nil {
            log.Debug.Printf("add device payload: %s\n", payload_str)
        } else {
            log.Debug.Printf("add device payload: %s\n", "Marshalling failed")
        }

	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, AddDevicePayload, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return errors.New(fmt.Sprintf("Response code %d", resp.StatusCode)), false
	}

	return nil, true
}

func (cs *Cloudstation) DeleteDevice(device_name string) (error, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}
	key := "delete_device"
	apis := getApis()
	api := apis[key]
	device_info := cs.DevicesByName()[device_name]
	//fmt.Println(device_info, device_info.Id)
	substitutedPath := substitutePath(apis[key], map[string]string{"cloudport_id": fmt.Sprintf("%d", device_info.Id)})
        header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return nil, false
	}

	return nil, true
}

func (cs *Cloudstation) ForceGetDeviceList() (bool, error, map[int]GetDeviceListResponseDeviceInfo, map[string]GetDeviceListResponseDeviceInfo, map[string]GetDeviceListResponseDeviceInfo, map[string][]GetDeviceListResponseDeviceInfo) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}
	key := "get_device_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
        page_no := 1
        retreivals_per_page := 100
        max_pages_to_avoid_loop := 10

	var device_list_resp  GetDeviceListResponse
	device_info := make(map[int]GetDeviceListResponseDeviceInfo)
	device_info_wrt_name := make(map[string]GetDeviceListResponseDeviceInfo)
	device_info_wrt_slno := make(map[string]GetDeviceListResponseDeviceInfo)
	device_info_wrt_sitename := make(map[string][]GetDeviceListResponseDeviceInfo)


        header := cs.Construct_headers(apis[key].headers.headers)
	//fmt.Println(header)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     page_no,
		"per_page": retreivals_per_page,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}


        for {

            queryParam["page"] = page_no

            resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
            if resp.StatusCode != 200 {
                    return false, nil, device_info, device_info_wrt_name, device_info_wrt_slno, device_info_wrt_sitename
            }

            err := json.NewDecoder(resp.Body).Decode(&device_list_resp)
            if err != nil {
                fmt.Println(err.Error())
            }
            if ok, json_data := convert_struct_to_string_via_json(device_list_resp); ok {
                log.Debug.Printf("response data: %s\n", json_data)
            }

	    tot_entry := device_list_resp.TotalCount
            //security_profiles = append(security_profiles, get_security_profiles_response.SecurityProfiles...)

            for _, cp := range device_list_resp.Cloudports {
                device_info[cp.Id] = cp
                device_info_wrt_name[cp.Name] = cp
                device_info_wrt_slno[cp.SerialNo] = cp
                device_info_wrt_sitename[cp.Site.Name] = append(device_info_wrt_sitename[cp.Site.Name], cp)
            }

            if len(device_info) == tot_entry {
                    break
            }

            if page_no == max_pages_to_avoid_loop {
                    break
            }

            page_no += 1
        }

    return true, nil, device_info, device_info_wrt_name, device_info_wrt_slno, device_info_wrt_sitename
}

func (cs *Cloudstation) GetDeviceList() (bool, error, map[int]GetDeviceListResponseDeviceInfo, map[string]GetDeviceListResponseDeviceInfo, map[string]GetDeviceListResponseDeviceInfo, map[string][]GetDeviceListResponseDeviceInfo) {

    if len(cs.DevicesByName()) == 0 {
        return cs.ForceGetDeviceList()
    }
    return true, nil, cs.DevicesById(), cs.DevicesByName(), cs.DevicesBySlNo(), cs.DevicesBySiteName()
}


func (cs *Cloudstation) Get_CpId(cp_name string) (int, bool) {

    res, _, _, _,_, _ := cs.ForceGetDeviceList()
    if !res {
        return 0, false
    }

    if cp_info, ok := cs.DevicesByName()[cp_name]; ok {
	    return cp_info.Id, true
    }
    return 0, false

}
