package restservice

import (
	"fmt"
	"errors"
	log "lvnautomation/lvnlog"
)

func (cs *Cloudstation) CleanupPoliciesOnSite(sitename string) (bool, error) {

    _, affecting_policy_response := cs.GetAffectingPolicyList(sitename)
    fmt.Println()

    //acl_site = []string{}
    //acl_siteprofile = []map[string]
    //acl_nwgrp = []string{}

    //qos_site = []string{}
    //qos_siteprofile = []string{}
    //qos_nwgrp = []string{}
    log.Debug.Println()

    /* 1. Cleanup Acl policy */
    for _, acl_policy := range affecting_policy_response.Acl_Policies {

	 fmt.Println(acl_policy.Inherit_frm.Name, acl_policy.Inherit_lvl)
        if acl_policy.Inherit_lvl == "Site" {
		if !cs.DetachAclPolicyFromSite(acl_policy.Rule.Name, sitename) {
			return false, errors.New("Failed to delete acl")
		}
	} else if acl_policy.Inherit_lvl == "Network Group" {
                if !cs.DetachAclAtNwGrp(acl_policy.Rule.Name, acl_policy.Inherit_frm.Name) {
			return false, errors.New("Failed to delete acl")
		}
	} else if acl_policy.Inherit_lvl == "Profile" {
                if !cs.DetachAclAtSiteProfile(acl_policy.Rule.Name, acl_policy.Inherit_frm.Name) {
			return false, errors.New("Failed to delete acl")
		}
	} else {
	}
    }

    for _, qos_policy := range affecting_policy_response.Qos_Policies {

        if qos_policy.Inherit_lvl == "Site" {
		cs.DetachQosAtSite(qos_policy.Rule.Name, sitename)
	} else if qos_policy.Inherit_lvl == "Network Group" {
                cs.DetachQosAtNwGrp(qos_policy.Rule.Name, qos_policy.Inherit_frm.Name)
	} else if qos_policy.Inherit_lvl == "Profile" {
                cs.DetachQosAtSiteProfile(qos_policy.Rule.Name, qos_policy.Inherit_frm.Name)
	} else {
	}
    }

    for _, trl_policy := range affecting_policy_response.TrafficRateLimiters {

        if trl_policy.Inherit_lvl == "Site" {
		cs.DetachTrlAtSite(trl_policy.Rule.Name, sitename)
	} else if trl_policy.Inherit_lvl == "Network Group" {
                cs.DetachTrlAtNwGrp(trl_policy.Rule.Name, trl_policy.Inherit_frm.Name)
	} else if trl_policy.Inherit_lvl == "Profile" {
                cs.DetachTrlAtSiteProfile(trl_policy.Rule.Name, trl_policy.Inherit_frm.Name)
	} else {
	}
    }

    for _, wan_policy := range affecting_policy_response.WanPolicies {

        if wan_policy.Inherit_lvl == "Site" {
		cs.DetachFwdAtSite(wan_policy.Rule.Name, sitename)
	} else if wan_policy.Inherit_lvl == "Network Group" {
                cs.DetachFwdAtNwGrp(wan_policy.Rule.Name, wan_policy.Inherit_frm.Name)
	} else if wan_policy.Inherit_lvl == "Profile" {
                cs.DetachFwdAtSiteProfile(wan_policy.Rule.Name, wan_policy.Inherit_frm.Name)
	} else {
	}
    }

    /*
    for _, threat_policy := range affecting_policy_response.Security_Policies {

        if threat_policy.Inherit_lvl == "Site" {
		cs.DetachThreatPolicyAtSite(threat_policy.Rule.Name, sitename)
	} else if threat_policy.Inherit_lvl == "Network Group" {
                cs.DetachThreatPolicyAtNwGrp(threat_policy.Rule.Name, threat_policy.Inherit_frm.Name)
	} else if threat_policy.Inherit_lvl == "Profile" {
                cs.DetachThreatPolicyAtSiteProfile(threat_policy.Rule.Name, threat_policy.Inherit_frm.Name)
	} else {
	}
    }*/


    return true, nil

}
