package restservice

import (
	"fmt"
	"net/http"
	"net/url"

	"errors"
	"encoding/json"
	"regexp"
	"strconv"
	"strings"

	//"encoding/hex"
	"bytes"
	"compress/gzip"
	"io"

	log "lvnautomation/lvnlog"
	"time"
	tc_utils "lvnautomation/testcaseutils"
	//"io/ioutil"
	//"unicode/utf8"
	//"crypto/tls"
)

type User struct {
	username, password string
}

type Customer struct {
	Url        string
	Sys_admins []User
	Net_admins []User
	Users      []User
}

type Site struct {
	Name   string
	Is_hub bool
	User   User
}

type Device struct {
	Name string
	Site Site
	Uuid string
}

type Cloudstation struct {
	Base_url            string
	Port                int
	Master              User
	Customers           []Customer
	Username            string
	Password            string
	Sites               []Site
	Devices             []Device
	LoggedIn            bool
	AccessToken         string
	Cookies             *http.Cookie
	Headers             map[string]interface{}
	CustomerHexId       string
	Id                  int
	Role                string
	AllowManage         bool
	CustomerId          int
	HexId               string
	EnableNotifications bool
	header              map[string]string

        InitialiseInfo     bool
	Helper             struct {
		site struct {
			byId     map[int]SiteInfo
			byName   map[string]SiteInfo
		}
		dnsproxy struct {
			byName    map[string]DnsProxy
		}

		device struct {
			byId     map[int]GetDeviceListResponseDeviceInfo
			byName   map[string]GetDeviceListResponseDeviceInfo
			bySlNo   map[string]GetDeviceListResponseDeviceInfo
			bySiteName map[string][]GetDeviceListResponseDeviceInfo
			IfaceListMap map[string]IFACE
			IfaceListMapCpId map[int]IFACE
		}
		ldap struct {
			byId map[int]LdapInfo
			byName map[string]LdapInfo
		}
		policy_objects struct {
			apps struct {
				byName map[string]AppInfo
			}
			app_categories struct {
				byName map[string]PolicyObjectAppCat
			}
			nw_grp struct {
				byName map[string]PolicyObjectNwGrp
			}
			subnets struct {
				byName map[string]PolicyObjectSubnets
			}
		}
		policies struct {
			fwd struct {
				byName map[string]WanInfo
				byHexId map[string]WanInfo
			}
			lb struct {
				//byCPEName map[string][]LbInfo
				byCPEName map[string]int
			}
		}
		profiles struct {
			security struct {
				byName map[string]SecurityProfile
			}
			vpn struct {
				byName map[string]VpnProfile
			}
		}
		nwgrp struct {
			byName map[string]NetworkGroupInfo
			nwgrplist []NETWORK_GRP
		}
	}

	FileserverDownload struct {
		url string
		cpe string
		filename string
	}
}


type gzreadCloser struct {
	*gzip.Reader
	io.Closer
}

func (gz gzreadCloser) Close() error {
	return gz.Closer.Close()
}

func StringClone(str string) (newstr string) {

	newbytes := make([]byte, len(str))
	copy(newbytes, []byte(str))
	return string(newbytes)

}

func (cs *Cloudstation) Download(cpe_name string, filename string) {

	//cs.Base_url
	request("GET", "deepak-webrt.devcloudstation.io/ftp/downloadWS?tid=986a0800&token=b2f87675dc4b83b4f0801d6b732c779b&file=index.html", "", map[string]interface{}{}, map[string]string{}, struct{}{}, []*http.Cookie{})
}

func request(method string, uri string, apiPath string, queryParams map[string]interface{}, header map[string]string, data interface{}, cookies []*http.Cookie) (result *http.Response, err error) {

	//log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	//fmt.Println(method, uri, apiPath, queryParams, header, data, cookies)
	/* 1. Create URL */
	baseurl, _ := url.Parse(uri)
	baseurl.Path += apiPath
	params := url.Values{}

	for queryParamKey, queryParamValue := range queryParams {
		//params.Add(queryParamKey, string(queryParamValue))
		params.Add(queryParamKey, fmt.Sprintf("%v", queryParamValue))
	}

	baseurl.RawQuery = params.Encode()

	completeUrl := baseurl.String()

	///* 3. Creating the http(s) request */
	var request *http.Request

	// bytesRepresentation, err = json.Marshal(map[string]interface{}{})

	//resp, _ = http.Post(baseurl, "application/x-www-form-urlencoded; charset=UTF-8", bytes.NewBuffer(bytesRepresentation))
	method = strings.ToUpper(method)

	//if len(data) != 0 {
	//jsonValue, err1 := json.Marshal(data)
	bytesRepresentation, err1 := json.Marshal(data)
	//bytesLength  := len(bytesRepresentation)
	//fmt.Sprintf(bytesLength)
	if err1 != nil {
		panic(err1)
	}

	var json_payload string
	if len(bytesRepresentation) > 0 {
		json_payload = string(bytesRepresentation)
		request, err = http.NewRequest(method, completeUrl, bytes.NewBuffer(bytesRepresentation))
	} else {
		json_payload = "{}"
		request, err = http.NewRequest(method, completeUrl, nil)
	}
	//}
	request.Close = true

	if err != nil {
		log.Err.Println(err)
		//fmt.Println("error in New request creation")
	        log.Err.Printf(" Error in http request creation\n")
		return
	}

	/* Setting the http header values*/
	for hdr_name, hdr_value := range header {
		if _, ok := request.Header[hdr_name]; ok {
			request.Header.Set(hdr_name, hdr_value)
		} else {
			request.Header.Add(hdr_name, hdr_value)
		}
	}

	//request.Header.Set("Content-Length", string(bytesLength))

	/* 2. Creating client*/
	var timeout time.Duration = 15

	for _, cookie := range cookies {
		request.AddCookie(cookie)
	}

	//for k, v := range request.Header {
	//    fmt.Println(k, v)
	//}

	//fmt.Println("===============>", request.Header.Get("Content-Length"))
	client := &http.Client{Timeout: time.Second * timeout}
	result, err = client.Do(request)
	//defer result.Body.Close()
	if err!=nil {
		//fmt.Println("error in doing request..")
	        log.Err.Printf(" Error in http request execution\n")
		fmt.Println(err)
		return result, err
	}
	log.Debug.Printf(" HTTP request %s %s Json: %s response %d\n", method, completeUrl, json_payload, result.StatusCode)
	resp := result

	//fmt.Println(resp.Header)
	//fmt.Println(resp.Header.Get("Content-Encoding"))
	if resp.Header.Get("Content-Encoding") == "gzip" {
		resp.Header.Del("Content-Length")
		zr, err2 := gzip.NewReader(resp.Body)
		if err2 != nil {
			fmt.Println(err)
			return
		}
		resp.Body = gzreadCloser{zr, resp.Body}

		/*
		var response_data bytes.Buffer
		response_body := io.TeeReader(resp.Body, &response_data)

		resp.Body = response_data
		//response_data, err := ioutil.ReadAll(response_body)
		if d, err := ioutil.ReadAll(response_body); err == nil {
                    log.Debug.Printf(" Response data: %s\n", string(d))
		} else {
                    log.Debug.Printf("Error in getting response data\n")
		}
		*/
	}

	return result, err
}

func convert_struct_to_string_via_json(v interface{}) (bool, string) {
    var json_payload string
    bytesRepresentation, err := json.Marshal(v)
    if err != nil {
	    return false, json_payload
    }
    if len(bytesRepresentation) > 0 {
        json_payload = string(bytesRepresentation)
	return true, json_payload
    }
    return false, json_payload
}

type rest_api_request struct {
	path   string
	method string
}

type rest_api_headers struct {
	headers map[string]string
}

type rest_api_params struct {
	params map[string]string
}

type rest_api_body struct {
	body map[string]string
}

type rest_details struct {
	request rest_api_request
	headers rest_api_headers
	params  rest_api_params
	body    rest_api_body
}

//var apis  map[string]rest_details

func getApis() (apis map[string]rest_details) {

	apis = map[string]rest_details{
		"login": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/sessions/login",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{
					"Content-Type":    "application/x-www-form-urlencoded; charset=UTF-8",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9",
					"Accept":          "application/json",
				},
			},
			params: rest_api_params{
				params: map[string]string{
					"otp_secret": "",
					"username":   "",
					"password":   "",
				},
			},
			body: rest_api_body{
				body: map[string]string{},
			},
		},

		"logout": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/sessions/logout",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{
					"Content-Type":    "application/x-www-form-urlencoded; charset=UTF-8",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9",
					"Accept":          "application/json",
				},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

                "get_cpelinks_mapping": rest_details{
                    request: rest_api_request{
                        path:   "api/v1/lavelle/cloudports/<device_id>/cloudport_links",
                        method: "GET",
                    },
                    headers: rest_api_headers{
                        headers: map[string]string{},
                    },
                    params: rest_api_params{},
                    body:   rest_api_body{},
                },

		"get_appcategories_list": rest_details{
			request: rest_api_request{
				path:   "api/v2/lavelle/application_categories",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"conf_customapp": rest_details{
			request: rest_api_request{
				path:   "/api/v2/lavelle/applications",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{}},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_app_list": rest_details{
			request: rest_api_request{
				path:   "api/v2/lavelle/applications",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"delete_app": rest_details{
			request: rest_api_request{
				path:   "/api/v2/lavelle/applications/<custom_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_cloudport_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_iface_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/interfaces/",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"conf_dhcpserver": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/dhcp_servers/",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_dhcpserver_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/dhcp_servers/",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"delete_dhcpserver": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/dhcp_servers/<custom_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"conf_dhcpoption": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/dhcp_options/",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_dhcpoption_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/dhcp_options/",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"delete_dhcpoption": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/dhcp_options/<custom_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"conf_mac_ip_binds": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/mac_ip_bindings",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_mac_ip_binds_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/mac_ip_bindings/",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"delete_mac_ip_binds": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/mac_ip_bindings/<custom_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"conf_staticroute": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/routes/",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_staticroute_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/routes/",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"delete_staticroute": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<device_id>/routes/<custom_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_policy_objects_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/policies/policy_objects",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"create_customer": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/customers",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
	        "get_customer_list": rest_details {
			request: rest_api_request{
				path:   "api/v1/lavelle/customers",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"delete_customer": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/customers/<customer_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"get_list_of_managers": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/customers/managers",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
                "create_site": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/sites",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"create_trl_policy": rest_details{
                        request: rest_api_request{
                                path:   "api/v1/lavelle/traffic_rate_limiter",
                                method: "POST",
                        },
                        headers: rest_api_headers{
                                headers: map[string]string{},
                        },
                        params: rest_api_params{},
                        body:   rest_api_body{},
                },

		"delete_trl_policy":  rest_details{
                        request: rest_api_request{
                                path:   "api/v1/lavelle/traffic_rate_limiter/<trl_policy_id>",
                                method: "DELETE",
                        },
                        headers: rest_api_headers{
                                headers: map[string]string{},
                        },
                        params: rest_api_params{},
                        body:   rest_api_body{},
                },

                "add_device": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

                "delete_site": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/sites/<site_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},


                "delete_device": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports/<cloudport_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

                "get_device_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/cloudports",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

                "get_site_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/sites",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
                "get_users_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/getUserListForCpeForm/1",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"conf_aclpolicy": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_aclpolicy_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"delete_aclpolicy": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

                "get_qospolicy_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/qos",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_trlpolicy_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/traffic_rate_limiter",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_fwdpolicy_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/wan_policies",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"create_fwd_policy": rest_details{
                       request: rest_api_request{
                               path:   "api/v1/lavelle/wan_policies",
                               method: "POST",
                       },
                       headers: rest_api_headers{
                               headers: map[string]string{},
                       },
                       params: rest_api_params{},
                       body:   rest_api_body{},
               },
	        "delete_fwd_policy":  rest_details{
                        request: rest_api_request{
                                path:   "api/v1/lavelle/wan_policies/<policy_id>",
                                method: "DELETE",
                        },
                        headers: rest_api_headers{
                                headers: map[string]string{},
                        },
                        params: rest_api_params{},
                        body:   rest_api_body{},
                },


		"get_aclpreview_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_networkgrp_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/network_groups",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_at_networkgrp": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_at_networkgrp": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/acls_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_acl_at_networkgrp": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_acl_at_networkgrp": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/acls_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"detach_threat_policy_at_networkgrp": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/security_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_qos_at_networkgrp": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/qos/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_qos_at_networkgrp": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/qos/<custom_id>/qos_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_trl_at_networkgrp": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/traffic_rate_limiter/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_trl_at_networkgrp": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/traffic_rate_limiter/<custom_id>/traffic_rate_limiter_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_fwd_at_networkgrp": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/wan_policies/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_fwd_at_networkgrp": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/wan_policies/<custom_id>/wan_policy_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_acl_at_site": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_acl_from_site": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/acls_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_threat_policy_from_site": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/security_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"attach_qos_at_site": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/qos/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_qos_at_site": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/qos/<custom_id>/qos_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_trl_at_site": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/traffic_rate_limiter/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_trl_at_site": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/traffic_rate_limiter/<custom_id>/traffic_rate_limiter_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_fwd_at_site": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/wan_policies/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_fwd_at_site": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/wan_policies/<custom_id>/wan_policy_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_acl_at_profile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_acl_at_profile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/acls_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"detach_threat_policy_at_profile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/security_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},


		"attach_qos_at_profile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/qos/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_qos_at_profile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/qos/<custom_id>/qos_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_trl_at_profile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/traffic_rate_limiter/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_trl_at_profile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/traffic_rate_limiter/<custom_id>/traffic_rate_limiter_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_fwd_at_profile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/wan_policies/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_fwd_at_profile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/wan_policies/<custom_id>/wan_policy_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_profile_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/profile",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"attach_at_profile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/attach_policy",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"detach_at_profile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/acls/<custom_id>/acls_relations",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"create_ldap":  rest_details{
                        request: rest_api_request{
                                path:   "api/v2/lavelle/ldap_servers",
                                method: "POST",
                        },
                        headers: rest_api_headers{
                                headers: map[string]string{},
                        },
                        params: rest_api_params{},
                        body:   rest_api_body{},
                },

                "get_ldap_list":  rest_details{
                        request: rest_api_request{
                                path:   "api/v2/lavelle/ldap_servers",
                                method: "GET",
                        },
                        headers: rest_api_headers{
                                headers: map[string]string{},
                        },
                        params: rest_api_params{},
                        body:   rest_api_body{},
                },
                "delete_ldap":  rest_details{
                        request: rest_api_request{
                                path:   "api/v2/lavelle/ldap_servers/<custom1_id>",
                                method: "DELETE",
                        },
                        headers: rest_api_headers{
                                headers: map[string]string{},
                        },
                        params: rest_api_params{},
                        body:   rest_api_body{},
                },
		"get_affecting_policy_list": rest_details{
                      request: rest_api_request{
                              path:   "api/v1/lavelle/nodes/<custom_uuid>/get_affecting_policies",
                              method: "GET",
                      },
                      headers: rest_api_headers{
                              headers: map[string]string{},
                      },
                      params: rest_api_params{},
                      body:   rest_api_body{},
              },
	      "conf_webfilter": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/url_profiles",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"geturlprofile": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/url_profiles",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"conf_attachwfpolicy": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/url_policies",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"delete_webfilter": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/url_profiles/<custom_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"get_networkgroup_info_by_hexid": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/network_groups/<nw_grp_hexid>",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"update_nw_grp": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/network_groups/<nw_grp_id>",
				method: "PUT",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},

		"get_vpn_profiles_list": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/vpn_profiles",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"get_security_profiles_list": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/security_profiles",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"get_attached_wfpolicy_list": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/sites/<custom_id>/url_policies",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"detach_wfpolicy": rest_details{
			request: rest_api_request{
				path:   "api/v1/lavelle/url_policies/<custom_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"create_lb_policy_on_cpe": rest_details{
                        request: rest_api_request{
                            path:   "api/v1/lavelle/lb_policy",
                            method: "POST",
                        },
                        headers: rest_api_headers{
                            headers: map[string]string{},
                        },
                        params: rest_api_params{},
                        body:   rest_api_body{},
                },

                "get_lbpolicy_on_device_list": rest_details{
                   request: rest_api_request{
                       path:   "api/v1/lavelle/cloudports/<custom_id>/lb_policy",
                       method: "GET",
                   },
                   headers: rest_api_headers{
                       headers: map[string]string{},
                   },
                   params: rest_api_params{},
                   body:   rest_api_body{},
                },

                "delete_lbpolicy_on_device": rest_details{
                    request: rest_api_request{
                        path:   "/api/v1/lavelle/lb_policy/<custom_id>",
                        method: "DELETE",
                    },
                    headers: rest_api_headers{
                        headers: map[string]string{},
                    },
                    params: rest_api_params{},
                    body:   rest_api_body{},
                },

                "attach_lbpolicy_on_device": rest_details{
                    request: rest_api_request{
                        path:   "/api/v1/lavelle/lb_policy/<policy_id>/attach_policy",
                        method: "POST",
                    },
                    headers: rest_api_headers{
                        headers: map[string]string{},
                    },
                    params: rest_api_params{},
                    body:   rest_api_body{},
                },

                "detach_lb_pol_from_device": rest_details{
                   request: rest_api_request{
                       path:   "api/v1/lavelle/lb_policy/<custom_id>/lb_policy_relations",
                       method: "POST",
                   },
                   headers: rest_api_headers{
                       headers: map[string]string{},
                   },
                   params: rest_api_params{},
                   body:   rest_api_body{},
                },
                "create_dns_proxy": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/dns_proxies",
				method: "POST",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
                "get_dnsproxy_list": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/dns_proxies",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
                "get_dnsproxy_info": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/dns_proxies/<dns_proxy_id>",
				method: "GET",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"delete_dns_proxy": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/dns_proxies/<dns_proxy_id>",
				method: "DELETE",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
		"update_site": rest_details{
			request: rest_api_request{
				path:   "/api/v1/lavelle/sites/<site_id>",
				method: "PUT",
			},
			headers: rest_api_headers{
				headers: map[string]string{},
			},
			params: rest_api_params{},
			body:   rest_api_body{},
		},
	}
	return apis
}

func substitutePath(api rest_details, pathSubstitutes map[string]string) (substitutedPath string) {
	//apis := getApis()

	/* 1. Substitute the values in the path*/
	substitutedPath = api.request.path

	var re *regexp.Regexp
	for k, v := range pathSubstitutes {
		re = regexp.MustCompile(fmt.Sprintf(`<%s>`, k))
		substitutedPath = re.ReplaceAllString(substitutedPath, v)
	}

	return substitutedPath
}

/*
"common_headers": map[string]string {
"Content-Type": "application/json; charset=UTF-8",
"Accept-Encoding": "gzip, deflate, br",
"Accept-Language": "en-US,en;q=0.9",
"Accept": "application/json",
"Authorization": None,
"Connection": "keep-alive",
"Host": self.host,
"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
"Referer": None,
"Origin": self.url,
*/

/*
var default_header = map[string]string{
	"Content-Type":    "application/json; charset=UTF-8",
	"Accept-Encoding": "gzip, deflate, br",
	"Accept-Language": "en-US,en;q=0.9",
	"Accept":          "application/json",
	"Connection":      "keep-alive",
	"User-Agent":      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
	"Referer":         "",
	//"Host":            "deepak-webui.devcloudstation.io",
	//"Origin":          "https://deepak-webui.devcloudstation.io",
}
*/
func (cs *Cloudstation) Construct_headers(updates map[string]string) (updated_header map[string]string) {

	//"Authorization": "",

	//fmt.Println("=============>", default_header)
	updated_header = make(map[string]string)
	for k, v := range cs.header {
		updated_header[k] = v
		//fmt.Println(k, v)
	}
	//updated_header["Host"] = strings.Split(cs.Base_url, "//")[1]
	//updated_header["Origin"] = cs.Base_url

	//fmt.Println("====>",updated_header)
	for k, v := range updates {
		updated_header[k] = v
	}
	return
}

func (cs *Cloudstation) Login() (error, bool, int) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	log.Info.Printf("Logging to the CS (%s) as User %s\n", cs.Base_url, cs.Username)

	//if cs.LoggedIn {
	//    return nil, true, 200
	//}

	key := "login"
	apis := getApis()
	api := apis[key]
	//fmt.Println(apis, key)
	//request("POST", GetApiPaths(cs.Base_url)["login"],
	substitutedPath := substitutePath(apis[key], map[string]string{})

	//myurl := Construct_url(cs.Base_url, key, map[string]string{})
	//fmt.Println(myurl)
	//cs.Headers =
	//cs.header = cs.Construct_headers(apis[key].headers.headers)

	header := map[string]string {
		"Content-Type"   : "application/json; charset=UTF-8",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-US,en;q=0.9",
		"Accept"         : "application/json",
		"Connection"     : "keep-alive",
		"User-Agent"     : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
		"Referer"        : "",
		"Host"           : strings.Split(cs.Base_url, "//")[1],
		"Origin"         : cs.Base_url,
	}

	cs.header = header
	header = cs.Construct_headers(apis[key].headers.headers)
	//params := apis[key].params.params
	//params["username"] = username
	//params["password"] = password

	//fmt.Println(header)
	//fmt.Println(params)
	queryParams := map[string]interface{}{
		"username":   cs.Username,
		"password":   cs.Password,
		"otp_secret": "",
	}
	var resp *http.Response
	var err error
	//fmt.Println(api.request.method, cs.Base_url, substitutedPath, queryParams, header)
	resp, err = request(api.request.method, cs.Base_url, substitutedPath, queryParams, header, map[string]interface{}{}, []*http.Cookie{})
	//fmt.Println("Status Login= ", resp.StatusCode)
	if err != nil {
		return err, false, resp.StatusCode
	}

	if resp.StatusCode != 200 {
		return err, false, resp.StatusCode
	}
	//resp, _ = request(apis[key].request.method, url, cs.header, apis[key].params.params, apis[key].body.body)
	//resp, _ = request(apis[key].request.method, url, cs.header, map[string]string{}, apis[key].params.params)
	// bytesRepresentation, err := json.Marshal(message)
	// bytesRepresentation, err = json.Marshal(map[string]interface{}{})

	//resp, _ = http.Post(baseurl, "application/x-www-form-urlencoded; charset=UTF-8", bytes.NewBuffer(bytesRepresentation))
	//resp, _ = http.Post(baseurl.String(), "application/x-www-form-urlencoded; charset=UTF-8", bytes.NewBuffer(bytesRepresentation))

	type LoginResponse struct {
		Message             string `json:"message"`
		Username            string `json:"username"`
		Id                  int    `json:"id"`
		Role                string `json:"role"`
		AllowManage         bool   `json:"allow_manage"`
		CustomerId          int    `json:"customer_id"`
		Timestamp           string `json:"timestamp"`
		UserEmail           string `json:"user_email"`
		HexId               string `json:"hex_id"`
		CustomerHexId       string `json:"customer_hex_id"`
		AccessToken         string `json:"access_token"`
		UserId              int    `json:"user_id"`
		EnableNotifications bool   `json:"enable_notifications"`
	}

	var login_response LoginResponse

	err12 := json.NewDecoder(resp.Body).Decode(&login_response)
	if err12 != nil {
		fmt.Println(err12)
		return nil, false, resp.StatusCode
	}

	//fmt.Println(login_response)

	if ok, json_data := convert_struct_to_string_via_json(login_response); ok {
	    log.Debug.Printf("login_response : %s\n", json_data)
	}
	/*
	   fmt.Println(login_response.Message)
	   fmt.Println(login_response.Username)
	   fmt.Println(login_response.Id)
	   fmt.Println(login_response.Role)
	   fmt.Println(login_response.AllowManage)
	   fmt.Println(login_response.CustomerId)
	   fmt.Println(login_response.Timestamp)
	   fmt.Println(login_response.UserEmail)
	   fmt.Println(login_response.HexId)
	   fmt.Println(login_response.CustomerHexId)
	   fmt.Println(login_response.AccessToken)
	   fmt.Println(login_response.UserId)
	   fmt.Println(login_response.EnableNotifications)
	*/
	//var myCookie string
	for _, cookie := range resp.Cookies() {
		if cookie.Name == "_lavelle_session" {
			//fmt.Println("Cookie: ", cookie.Value)
			cs.Cookies = cookie
			//myCookie = fmt.Sprintf("%s=%s", cookie.Name,cookie.Value)
		}
	}

	//fmt.Println(myCookie)

	cs.Id = login_response.Id
	cs.AllowManage = login_response.AllowManage
	cs.CustomerId = login_response.CustomerId
	cs.HexId = login_response.HexId
	cs.EnableNotifications = login_response.EnableNotifications

	cs.LoggedIn = true
	cs.AccessToken = StringClone(login_response.AccessToken)
	//fmt.Println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
	//fmt.Println(cs.AccessToken, login_response.AccessToken)
	//fmt.Println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
	cs.CustomerHexId = login_response.CustomerHexId
	//fmt.Println(resp.StatusCode)
	cs.header["Authorization"] = fmt.Sprintf("Bearer %s", cs.AccessToken)
	cs.header["Referer"] = fmt.Sprintf("%s/cid=%s", cs.Base_url, cs.CustomerHexId)
	//default_header["Cookie"] = myCookie

	//fmt.Println(default_header["Authorization"])
	//fmt.Println("login cs.LoggedIn ==", cs.LoggedIn)


	/* Collecting info */
	//if cs.InitialiseInfo {
	if  true {

           if  login_response.Role == "net-admin" || login_response.Role == "master" {

                 /* 1. Get all site info */
	        _, _, site_info_wrt_id, site_info_wrt_name := cs.SiteList()
	        //log.Info.Println(site_info_wrt_id)
	        //log.Info.Println(site_info_wrt_name)
                cs.Helper.site.byId = site_info_wrt_id
                cs.Helper.site.byName = site_info_wrt_name
                /* 2. Get all device info */
	        _, _, device_info_wrt_id, device_info_wrt_name, device_info_wrt_slno, device_info_wrt_sitename := cs.GetDeviceList()
	        cs.Helper.device.byId  = device_info_wrt_id
	        cs.Helper.device.byName  = device_info_wrt_name
	        cs.Helper.device.bySlNo= device_info_wrt_slno
	        cs.Helper.device.bySiteName= device_info_wrt_sitename

		//log.Info.Printf("================================\n")
		//log.Info.Println(cs.Helper.device.bySiteName["5078"])
		//for name, _ := range cs.Helper.device.bySiteName {
		//	log.Info.Println(name)
		//}
		//log.Info.Printf("================================\n")
		//log.Info.Println(device_info_wrt_sitename["5078"])
		//log.Info.Printf("================================\n")

	        cs.Helper.device.bySlNo= device_info_wrt_slno
	        cs.Helper.device.IfaceListMap = map[string]IFACE{}
	        cs.Helper.device.IfaceListMapCpId = map[int]IFACE{}
		cs.Helper.profiles.security.byName = map[string]SecurityProfile{}
		cs.Helper.profiles.vpn.byName = map[string]VpnProfile{}
		//cs.Helper.policies.lb.byCPEName = map[string]map[string]LbInfo{}
		cs.Helper.policies.lb.byCPEName = map[string]int{}
		cs.Helper.nwgrp.byName = map[string]NetworkGroupInfo{}
		cs.Helper.nwgrp.nwgrplist = []NETWORK_GRP{}
                cs.Get_PolicyObjectsList()
		cs.Helper.dnsproxy.byName = map[string]DnsProxy{}

	   } else if login_response.Role == "sys-admin" {
	   } else if login_response.Role == "user" {
	   } else if login_response.Role == "master" {
	   }
	}
	log.Info.Printf("Successfully logged into the CS (%s) as User %s\n", cs.Base_url, cs.Username)
	return nil, true, resp.StatusCode
}

func (cs *Cloudstation) SitesById()  map[int]SiteInfo {
	return cs.Helper.site.byId
}

func (cs *Cloudstation) SitesByName()  map[string]SiteInfo {
	//fmt.Println(cs.Helper.site.byName)
	return cs.Helper.site.byName
}

func (cs *Cloudstation) DevicesBySiteName()  map[string][]GetDeviceListResponseDeviceInfo {
	return cs.Helper.device.bySiteName
}

func (cs *Cloudstation) DevicesById()  map[int]GetDeviceListResponseDeviceInfo {
	return cs.Helper.device.byId
}

func (cs *Cloudstation) DevicesByName()  map[string]GetDeviceListResponseDeviceInfo {
	return cs.Helper.device.byName
}

func (cs *Cloudstation) DevicesBySlNo()  map[string]GetDeviceListResponseDeviceInfo {
	return cs.Helper.device.bySlNo
}

func (cs *Cloudstation) LdapById()  map[int]LdapInfo {
	return cs.Helper.ldap.byId
}

func (cs *Cloudstation) LdapByName()  map[string]LdapInfo {
	return cs.Helper.ldap.byName
}

func (cs *Cloudstation) PolicyObjAppCats()  map[string]PolicyObjectAppCat {
	return cs.Helper.policy_objects.app_categories.byName
}

func (cs *Cloudstation) PolicyObjApps()  map[string]AppInfo {
	return cs.Helper.policy_objects.apps.byName
}

func (cs *Cloudstation) PolicyObjSubnets()  map[string]PolicyObjectSubnets {
	return cs.Helper.policy_objects.subnets.byName
}

func (cs *Cloudstation) PolicyObjNwgroup()  map[string]PolicyObjectNwGrp {
	return cs.Helper.policy_objects.nw_grp.byName
}

func (cs *Cloudstation) Logout() (error, bool) {

	//fmt.Println("login cs.LoggedIn ==", cs.LoggedIn)
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	log.Info.Printf("Logout of the CS (%s) as User %s\n", cs.Base_url, cs.Username)
	if !cs.LoggedIn {
		return nil, true
	}

	key := "logout"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
	//fmt.Println(substitutedPath)

	header := cs.Construct_headers(apis[key].headers.headers)
	//fmt.Println(header)
	var resp *http.Response
	var err error
	resp, err = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, map[string]interface{}{}, []*http.Cookie{})
	//fmt.Println("Status Logout= ", resp.StatusCode)
	if err != nil {
		return err, false
	}

	if resp.StatusCode != 200 {
		return errors.New("status code not 200"), false
	}

	cs.LoggedIn = false
	return nil, true
}

func (cs *Cloudstation) Status_CS(StatusCode int) bool {

	//fmt.Println("Status Code= ", StatusCode)

	for i := 0; i < 3; i++ {
		//fmt.Println("==========================Status Code= ", i, "==>", StatusCode)
		if StatusCode == 200 {
			return true
		}
		if StatusCode == 401 {
			_, resp_bool, result := cs.Login() //error, bool, int
			//fmt.Println("RElogin result", resp_bool, result)
			if resp_bool == false {
				return false
			}
			StatusCode = result
		} else {
			return false
		}
	}
	return false
}

/* TO DO */
func (cs *Cloudstation) execRequest(key string, pathSubstitutes map[string]string, queryParams map[string]interface{}, data interface{}) (bool, error, *http.Response) {

	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], pathSubstitutes)
	header := cs.Construct_headers(apis[key].headers.headers)

	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParams, header, data, cookie)

	if !cs.Status_CS(resp.StatusCode) {
		return false, nil, resp
	}

	if resp.StatusCode == 401 {
		header := cs.Construct_headers(apis[key].headers.headers)
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, data, cookie)
	}

	if resp.StatusCode != 200 {
		return false, nil, resp
	}
	return true, nil, resp
}


//func (cs *Cloudstation) CreateCustomApp(AppName string, appcat_name string, MatchType string, Ip string, Port string, SrcIp string, SrcPort string, DstIp string, DstPort string, Hostname string, Protocol string, Sign string, Tag string) bool {

//Shubham's Get_CloudPorts function

type Cloudport struct {
    Customer struct {
    } `json:"customer"`
    Hardware struct {
	//Id int `json:"id"`
    } `json:"hardware"`
    Id int `json:"id"`
    Name string `json:"name"`
}

type CP_s struct {
	Cloudports []Cloudport `json:"cloudports"`
	Total_count int `json:"total_count"`
}

func (cs *Cloudstation) GetHubs() ([]SiteInfo, []GetDeviceListResponseDeviceInfo) {

	hub_sites := []SiteInfo{}
	hub_devices := []GetDeviceListResponseDeviceInfo{}

	for sitename, devices := range cs.Helper.device.bySiteName {
           if cs.Helper.site.byName[sitename].Is_hub {
		   hub_sites = append(hub_sites, cs.Helper.site.byName[sitename])
		   hub_devices = append(hub_devices, devices...)
	   }
	}
	return hub_sites, hub_devices
}

func (cs *Cloudstation) IsThisCPEHub(cpename string) (bool, error) {
    if cpeinfo , ok := cs.Helper.device.byName[cpename]; ok {
	    return cpeinfo.Site.Is_hub, nil
    }
    return false, errors.New("No cpe found")
}


func (cs *Cloudstation) IsThisSiteHub(sitename string) (bool, error) {
    if siteinfo , ok := cs.Helper.site.byName[sitename]; ok {
	    return siteinfo.Is_hub , nil
    }
    return false, errors.New("No site found")
}

/*
func (cs *Cloudstation) Get_CpList() (bool, error, []Cloudport, map[string]Cloudport) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}
	var cp_list []Cloudport = []Cloudport{}
	var cp_info_wrt_name map[string]Cloudport = map[string]Cloudport{}
	var get_cloudports_response CP_s
	key := "get_cloudport_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

        page_no := 1
        retreivals_per_page := 100
        max_pages_to_avoid_loop := 10

	header := cs.Construct_headers(apis[key].headers.headers)
	//fmt.Println(header)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     page_no,
		"per_page": retreivals_per_page,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

        for {

            queryParam["page"] = page_no

            resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
            if resp.StatusCode != 200 {
                    return false, errors.New(fmt.Sprintf("Response code %d", resp.StatusCode)), cp_list, cp_info_wrt_name
            }
            err_nwgrp := json.NewDecoder(resp.Body).Decode(&get_cloudports_response)
            if err != nil {
                fmt.Println(err)
            }
            if ok, json_data := convert_struct_to_string_via_json(get_cloudports_response); ok {
                log.Debug.Printf("response data: %s\n", json_data)
            }

            total_cpes := get_cloudports_response.Total_count
            cp_list = append(cp_list, get_cloudports_response.Cloudports...)

            for _, cloudport := range get_cloudports_response.Cloudports {
                cp_info_wrt_name[cloudport.Name] = cloudport
            }

            //cs.Helper.profiles.security.byName = security_profiles_wrt_name

            if len(cp_info_wrt_name) == total_profiles {
                    break
            }

            if page_no == max_pages_to_avoid_loop {
                    break
            }

            page_no += 1
        }
	return true, nil, cp_list, cp_info_wrt_name

}

func (cs *Cloudstation) OLDGet_CpList() ([]CP_s, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}
	key := "get_cloudport_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
	header := cs.Construct_headers(apis[key].headers.headers)
	//fmt.Println(header)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	var cp_list []CP_s
	var cp CP_s
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return cp_list, false
	}

	err_cp := json.NewDecoder(resp.Body).Decode(&cp)
	if err_cp != nil {
		log.Err.Println(err_cp)
	}
	if ok, json_data := convert_struct_to_string_via_json(cp); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}

	tot_entry := cp.Total_count

	cp_list = append(cp_list, cp)
	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}
	i := 2
	for i <= tot_page {
		queryParam = map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		if resp.StatusCode != 200 {
			return cp_list, false
		}
		err_cp = json.NewDecoder(resp.Body).Decode(&cp)
		if err_cp != nil {
			log.Err.Println(err_cp)
		}
	if ok, json_data := convert_struct_to_string_via_json(cp); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
		cp_list = append(cp_list, cp)
		i++
	}
	return cp_list, true
}
func (cs *Cloudstation) Get_CpId(cp_name string) (int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	res, err_device, cp_list, cp_info_wrt_name := cs.Get_CpList()
	if !res {
		log.Err.Printf("Failed to fetch the CP List ! %s", err_device.Error())
		return 0, false
	}

	i := 0
	j := 0
	var cp_id int
	var found_cp_id bool
	length := len(cp_ls)
	//fmt.Println("\n----------Number of Pages in which 10 CP's are present----------: ", length)
	//fmt.Println("\n----------Number of CloudPorts----------: ", cp_ls)
	for i < length {
		for j < 10 {
			if cp_ls[i].Cloudports[j].Name == cp_name {
				cp_id = cp_ls[i].Cloudports[j].Id
				found_cp_id = true
				break
			}
			j++
		}
		if found_cp_id == true {
			break
		}
		i++
	}
	if found_cp_id == false {
		log.Err.Printf("No Device found !")
		return 0, false
	}
	return cp_id, true
}

*/
type CloudportLinkInfo struct {
	Id int `json:"id"`
	Name string `json:"name"`
	PortType string `json:"port_type"`
}

type Interface struct {
    Id                  int `json:"id"`
    CloudportLink  CloudportLinkInfo `json:"cloudport_link"`
    Interface_addresses []struct {
	Id int `json:"id"`
	Ip string `json:"ip"`
	IpVersion int `json:"ip_version"`
	PrefixLength int `json:"prefix_length"`
    } `json:"interface_addresses"`
    NetworkMode string `json:"network_mode"`
    Name string `json:"name"`
    WanType *string `json:"wan_type"`
    Ipv4Gateway string `json:"ipv4_gateway"`
    Role string `json:"role"`
    Status string `json:"status"`
    Vlan int `json:"vlan"`
    PrimaryIp struct {
	    V4 string `json:"v4"`
	    V6 string `json:"v6"`
    } `json:"primary_ip"`
}

func (i *Interface) GetIPv4Gateway() string {
	return i.Ipv4Gateway
}

func (i *Interface) GetPrimaryV4Addr() (string, int) {
	if i.PrimaryIp.V4 != "" {
	    ip_info := strings.Split(i.PrimaryIp.V4, "/")
	    pref, err := strconv.Atoi(ip_info[1])
	    if err != nil {
		    return "", 0
	    }
	    return ip_info[0], pref
	} else {
		return "", 0
	}
}

type IFACE struct {
	Interfaces []Interface `json:"interfaces"`
}

func (i *IFACE) GetWans() []Interface {
    var wans []Interface
    for _, iface := range i.Interfaces {
	    if iface.Role == "wan" {
	       wans = append(wans, iface)
            }
    }
    return wans
}

func (i *Interface) IfaceName() string {
    return i.Name
}

func (i *Interface) IsUp() bool {
    return (strings.ToLower(i.Status) == "up")
}

func (i *IFACE) GetUpWans() []Interface {
    var wans []Interface
    for _, iface := range i.GetWans() {
	    if strings.ToLower(iface.Status) == "up" {
	       wans = append(wans, iface)
            }
    }
    return wans
}


func (i *IFACE) GetInetWans() []Interface {
	var inet_wans []Interface
	for _, wan :=  range i.GetWans() {
		if *wan.WanType == "internet" {
			inet_wans = append(inet_wans, wan)
		}
	}
	return inet_wans
}

func (i *IFACE) GetUpInetWans() []Interface {
	var inet_wans []Interface
	for _, wan :=  range i.GetInetWans() {
	    if strings.ToLower(wan.Status) == "up" {
	       inet_wans = append(inet_wans, wan)
            }
	}
	return inet_wans
}

func (i *IFACE) GetMplsWans() []Interface {
	var mpls_wans []Interface
	for _, wan :=  range i.GetWans() {
		if *wan.WanType == "mpls" {
			mpls_wans = append(mpls_wans, wan)
		}
	}
	return mpls_wans
}

func (i *IFACE) GetLans() []Interface {
    var lans []Interface
    for _, iface := range i.Interfaces {
	    if iface.Role == "lan" {
	       lans = append(lans, iface)
            }
    }
    return lans
}

func (i *IFACE) GetBondLans() []Interface {
    var blans []Interface
    lans := i.GetLans()
    for _, lan := range lans {
	    if lan.CloudportLink.PortType == "bondport" {
		    blans = append(blans, lan)
	    }
    }
    return blans
}

func (i *IFACE) GetPhyLans() []Interface {
    var phylans []Interface
    lans := i.GetLans()
    for _, lan := range lans {
	    if lan.CloudportLink.PortType == "phyport" {
		    phylans = append(phylans, lan)
	    }
    }
    return phylans
}

func (i *IFACE) GetUpPhyLans() []Interface {
    var phylans []Interface
    //lans := i.GetLans()
    for _, lan := range i.GetPhyLans() {
	    //fmt.Println("lan stat", lan.Status)
	    if lan.IsUp() {
		    phylans = append(phylans, lan)
	    }
    }
    return phylans
}

func (cs *Cloudstation) GetUpWansFromCpe(cp_name string) []Interface {
	if i, res := cs.Get_IfaceList(cp_name); res {
            return i.GetUpWans()
	}
	return []Interface{}
}

func (cs *Cloudstation) GetWansFromCpe(cp_name string) []Interface {
	if i, res := cs.Get_IfaceList(cp_name); res {
            return i.GetWans()
	}
	return []Interface{}
}

func (cs *Cloudstation) GetLansFromCpe(cp_name string) []Interface {
	if i, res := cs.Get_IfaceList(cp_name); res {
            return i.GetLans()
	}
	return []Interface{}
}

func (cs *Cloudstation) GetInetWansFromCpe(cp_name string) []Interface {
	if i, res := cs.Get_IfaceList(cp_name); res {
            return i.GetInetWans()
	}
	return []Interface{}
}

func (cs *Cloudstation) GetMplsWansFromCpe(cp_name string) []Interface {
	if i, res := cs.Get_IfaceList(cp_name); res {
            return i.GetMplsWans()
	}
	return []Interface{}
}

func (cs *Cloudstation) Get_IfaceListByCPEId(cp_id int) (IFACE, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var iface_ls IFACE
	var dummy struct {
	}

	key := "get_iface_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return iface_ls, false
	}
	err_iface := json.NewDecoder(resp.Body).Decode(&iface_ls)
	if err_iface == nil {
		log.Debug.Println(err_iface)
	}
	if ok, json_data := convert_struct_to_string_via_json(iface_ls); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}

	cs.Helper.device.IfaceListMapCpId[cp_id] = iface_ls
	return iface_ls, true

}

func (cs *Cloudstation) Get_IfaceList(cp_name string) (IFACE, bool) {

    cp_id, err := cs.Get_CpId(cp_name)
    if err == false {
        fmt.Println("Failed to fetch the CP Id !")
        return IFACE{}, false
    }

    cs.Helper.device.IfaceListMap[cp_name] = cs.Helper.device.IfaceListMapCpId[cp_id]
    return cs.Get_IfaceListByCPEId(cp_id)
}

/*
func (cs *Cloudstation) OLDGet_IfaceList(cp_name string) (IFACE, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var iface_ls IFACE
	var dummy struct {
	}

	var cp_id int
	var err bool

	if cp_info, ok := cs.Helper.device.byName[cp_name]; ok {
		cp_id = cp_info.Id
	} else {
		cp_id, err = cs.Get_CpId(cp_name)
		if err == false {
			log.Err.Printf("Failed to fetch the CP Id !")
			return iface_ls, false
		}
        }

	key := "get_iface_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return iface_ls, false
	}
	err_iface := json.NewDecoder(resp.Body).Decode(&iface_ls)
	if err_iface == nil {
		log.Debug.Println(err_iface)
	}
	if ok, json_data := convert_struct_to_string_via_json(iface_ls); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}

	cs.Helper.device.IfaceListMap[cp_name] = iface_ls
	return iface_ls, true

}
*/
func (cs *Cloudstation) Iface_Id(cp_name string, iface_name string) (int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	iface_ls, err1 := cs.Get_IfaceList(cp_name)
	if err1 == false {
		log.Err.Printf("Failed to fetch the Interface list !", err1)
		return 0, false
	}

	var iface_id int
	var found_iface_id bool
	length := len(iface_ls.Interfaces)
	for i := 0; i < length; i++ {
		if iface_ls.Interfaces[i].Name == iface_name {
			iface_id = iface_ls.Interfaces[i].Id
			found_iface_id = true
			break
		}
	}
	if found_iface_id == false {
		log.Err.Printf("No Device found !")
		return 0, false
	}

	return iface_id, true
}

func (cs *Cloudstation) Iface_addr_Id(cp_name string, iface_name string) (int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	iface_ls, err1 := cs.Get_IfaceList(cp_name)
	if err1 == false {
		log.Err.Printf("Failed to fetch the Interface list !", err1)
		return 0, false
	}

	var iface_addr_id int
	var found_iface_addr_id bool
	length := len(iface_ls.Interfaces)
	for i := 0; i < length; i++ {
		if iface_ls.Interfaces[i].Name == iface_name {
			for j := 0; j < length; j++ {
				iface_addr_id = iface_ls.Interfaces[i].Interface_addresses[j].Id
				found_iface_addr_id = true
				break
			}
		}
	}
	if found_iface_addr_id == false {
		log.Err.Printf("No Device found !")
		return 0, false
	}

	return iface_addr_id, true
}

func (cs *Cloudstation) Conf_Dhcpserver(DhcpInput DhcpServerInput) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())

	cp_id, err1 := cs.Get_CpId(DhcpInput.CpeName)
	if err1 == false {
		log.Err.Println("Failed to fetch the CP Id !")
		return false
	}

	iface_add_id, err2 := cs.Iface_addr_Id(DhcpInput.CpeName, DhcpInput.InterfaceName)
	if err2 == false {
		log.Err.Println("Failed to fetch the Iface Id !")
		return false
	}

	var dhcp_payload struct {
		Dns               string `json:"dns"`
		Interface_addr_id int    `json:"interface_address_id"`
		Start_ip          string `json:"start_ip"`
		Stop_ip           string `json:"stop_ip"`
	}

	dhcp_payload.Dns = DhcpInput.Dns
	dhcp_payload.Interface_addr_id = iface_add_id
	dhcp_payload.Start_ip = DhcpInput.StartIP
	dhcp_payload.Stop_ip = DhcpInput.StopIP

	key := "conf_dhcpserver"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dhcp_payload, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}
type DHCP_SERVER struct {
	Data struct {
		Dhcp_servers []struct {
			Dns       string `json:"dns"`
			Id        int    `json:"id"`
			Interface struct {
				Name string `json:"name"`
			} `json:"interface"`
		} `json:"dhcp_servers"`
	} `json:"data"`
	Message     string `json:"message"`
	Reason      string `json:"reason"`
	Total_Count int    `json:"total_count"`
}

func (cs *Cloudstation) Get_DhcpserverList(cp_name string) (DHCP_SERVER, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	var dh_ls DHCP_SERVER
	cp_id, err1 := cs.Get_CpId(cp_name)
	if err1 == false {
		log.Err.Printf("Failed to fetch the CP Id !\n")
		return dh_ls, false
	}

	key := "get_dhcpserver_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id)})
	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return dh_ls, false
	}
	err_dhcp := json.NewDecoder(resp.Body).Decode(&dh_ls)
	if err_dhcp != nil {
		log.Err.Printf(err_dhcp.Error())
	}
	if ok, json_data := convert_struct_to_string_via_json(dh_ls); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}

	return dh_ls, true
}

func (cs *Cloudstation) Get_DhcpserverId(cp_name string, iface_name string) (int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	server_ls, err_server := cs.Get_DhcpserverList(cp_name)
	if err_server == false {
		log.Err.Printf("Failed to fetch the Server list !", err_server)
		return 0, false
	}

	var server_id int
	var found_server_id bool
	length := len(server_ls.Data.Dhcp_servers)
	for i := 0; i < length; i++ {
		if server_ls.Data.Dhcp_servers[i].Interface.Name == iface_name {
			server_id = server_ls.Data.Dhcp_servers[i].Id
			found_server_id = true
			break
		}
	}
	if found_server_id == false {
		log.Err.Printf("No Server Id found !")
		return 0, false
	}
	//fmt.Printf("Added DHCP Server Id is: %v\n", server_id)
	//fmt.Println("--------------Got into the Verse for Getting the DHCP Server ID !--------------")
	return server_id, true
}

func (cs *Cloudstation) Del_Dhcpserver(cp_name string, iface_name string) bool {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	cp_id, err1 := cs.Get_CpId(cp_name)
	if err1 == false {
		log.Err.Printf("Failed to fetch the CP Id !")
		return false
	}

	serverid, err2 := cs.Get_DhcpserverId(cp_name, iface_name)
	if err2 == false {
		log.Err.Printf("Failed to fetch the created Server Id !")
		return false
	}

	var deletedhcp_payload struct {
	}

	key := "delete_dhcpserver"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id), "custom_id": strconv.Itoa(serverid)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, deletedhcp_payload, cookie)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}

func (cs *Cloudstation) Conf_Dhcpoption(cp_name string, code string, name string, data string, opt_type string) bool {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	cp_id, err1 := cs.Get_CpId(cp_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the CP Id !")
		return false
	}

	var dhcp_option_payload struct {
		Code        string `json:"code"`
		Data        string `json:"data"`
		Name        string `json:"name"`
		Option_type string `json:"option_type"`
	}

	dhcp_option_payload.Code = code
	dhcp_option_payload.Name = name
	dhcp_option_payload.Data = data
	dhcp_option_payload.Option_type = opt_type

	key := "conf_dhcpoption"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dhcp_option_payload, cookie)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}

type DHCP_OPTION struct {
	Data struct {
		Dhcp_options []struct {
			Code string `json:"code"`
			Data string `json:"data"`
			Id   int    `json:"id"`
			Name string `json:"name"`
		} `json:"dhcp_options"`
	} `json:"data"`
	Message     string `json:"message"`
	Reason      string `json:"reason"`
	Total_Count int    `json:"total_count"`
}

func (cs *Cloudstation) Get_DhcpoptionList(cp_name string) (DHCP_OPTION, bool) {
	var dummy struct {
	}

	var dh_ls DHCP_OPTION
	cp_id, err1 := cs.Get_CpId(cp_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the CP Id !\n")
		return dh_ls, false
	}

	key := "get_dhcpoption_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return dh_ls, false
	}
	err_dhcp := json.NewDecoder(resp.Body).Decode(&dh_ls)
	if err_dhcp != nil {
		fmt.Println(err_dhcp)
	}
	if ok, json_data := convert_struct_to_string_via_json(dh_ls); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}

	return dh_ls, true
}

func (cs *Cloudstation) Get_DhcpoptionId(cp_name string, op_name string) (int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	option_ls, err1 := cs.Get_DhcpoptionList(cp_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the Option list !", err1)
		return 0, false
	}

	var option_id int
	var found_option_id bool
	length := len(option_ls.Data.Dhcp_options)
	for i := 0; i < length; i++ {
		if option_ls.Data.Dhcp_options[i].Name == op_name {
			option_id = option_ls.Data.Dhcp_options[i].Id
			found_option_id = true
			break
		}
	}
	if found_option_id == false {
		log.Err.Println("No Option Id found !")
		return 0, false
	}
	//fmt.Printf("Added DHCP Server Id is: %v\n", server_id)
	//fmt.Println("--------------Got into the Verse for Getting the DHCP Option ID !--------------")
	return option_id, true
}

func (cs *Cloudstation) Del_Dhcpoption(cp_name string, op_name string) bool {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	cp_id, err1 := cs.Get_CpId(cp_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the CP Id !")
		return false
	}

	optionid, err2 := cs.Get_DhcpoptionId(cp_name, op_name)
	if err2 == false {
		log.Err.Println("Failed to fetch the created Option Id !", err2)
		return false
	}

	key := "delete_dhcpoption"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id), "custom_id": strconv.Itoa(optionid)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}

func (cs *Cloudstation) Conf_MacIpBindings(cp_name string, mac_name string, ip string, mac string) bool {

	cp_id, err1 := cs.Get_CpId(cp_name)
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	if err1 == false {
		log.Err.Println("Failed to fetch the CP Id !")
		return false
	}

	var mac_bind_payload struct {
		Host string `json:"host"`
		Ip   string `json:"ip"`
		Mac  string `json:"mac"`
	}

	mac_bind_payload.Host = mac_name
	mac_bind_payload.Ip = ip
	mac_bind_payload.Mac = mac

	key := "conf_mac_ip_binds"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id)})
	header := cs.Construct_headers(apis[key].headers.headers)

	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, mac_bind_payload, cookie)
	if resp.StatusCode != 200 {
		return false
	}

	return true
}

type MAC_IP struct {
	Data struct {
		Mac_ip_bindings []struct {
			Host string `json:"host"`
			Id   int    `json:"id"`
			Ip   string `json:"ip"`
			Mac  string `json:"mac"`
		} `json:"mac_ip_bindings"`
	} `json:"data"`
	Message     string `json:"message"`
	Reason      string `json:"reason"`
	Total_Count int    `json:"total_count"`
}

func (cs *Cloudstation) Get_MacIpBindingsList(cp_name string) (MAC_IP, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	var mac_ls MAC_IP
	cp_id, err1 := cs.Get_CpId(cp_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the CP Id !\n")
		return mac_ls, false
	}

	key := "get_mac_ip_binds_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id)})
	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return mac_ls, false
	}
	err_mac := json.NewDecoder(resp.Body).Decode(&mac_ls)
	if err_mac != nil {
		log.Err.Println(err_mac)
	}
	if ok, json_data := convert_struct_to_string_via_json(mac_ls); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}

	return mac_ls, true
}

func (cs *Cloudstation) Get_MacIpBindingsId(cp_name string, mac_name string) (int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	mac_ls, err1 := cs.Get_MacIpBindingsList(cp_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the MAC IP List !", err1)
		return 0, false
	}

	i := 0
	var mac_id int
	var found_mac_id bool
	length := len(mac_ls.Data.Mac_ip_bindings)
	for i < length {
		if mac_ls.Data.Mac_ip_bindings[i].Host == mac_name {
			mac_id = mac_ls.Data.Mac_ip_bindings[i].Id
			found_mac_id = true
			break
		}
		i++
	}
	if found_mac_id == false {
		log.Err.Println("No Mac Ip Bindings Id found !")
		return 0, false
	}
	//fmt.Printf("Added MAC IP BINDINGS Id is: %v\n", mac_id)
	return mac_id, true
}

func (cs *Cloudstation) Del_MacIpBindings(cp_name string, mac_name string) bool {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	cp_id, err1 := cs.Get_CpId(cp_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the CP Id !")
		return false
	}

	mac_id, err2 := cs.Get_MacIpBindingsId(cp_name, mac_name)
	if err2 == false {
		log.Err.Println("Failed to fetch the created MAC IP BINDINGS Id !")
		return false
	}

	var deletedhcp_payload struct {
	}

	key := "delete_mac_ip_binds"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id), "custom_id": strconv.Itoa(mac_id)})
	//fmt.Println(substitutedPath)

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, deletedhcp_payload, cookie)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}

func (cs *Cloudstation) Conf_StaticRoute(cp_name string, iface_name string, prefix string, next_hop string) bool {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	cp_id, err1 := cs.Get_CpId(cp_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the CP Id !")
		return false
	}
	iface_id, err2 := cs.Iface_Id(cp_name, iface_name)
	if err2 == false {
		log.Err.Println("Interface not found !")
		return false
	}

	var static_route_payload struct {
		Interface_id int    `json:"interface_id"`
		Nexthop      string `json:"nexthop"`
		Prefix       string `json:"prefix"`
		Route_type   string `json:"route_type"`
	}

	routetype := ""
	static_route_payload.Interface_id = iface_id
	static_route_payload.Nexthop = next_hop
	static_route_payload.Prefix = prefix
	static_route_payload.Route_type = routetype

	key := "conf_staticroute"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, static_route_payload, cookie)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}

type ROUTE struct {
	Routes []struct {
		Id        int `json:"id"`
		Interface struct {
			Id   int    `json:"id"`
			Name string `json:"name"`
		} `json:"interface"`
		Nexthop    string `json:"nexthop"`
		Prefix     string `json:"prefix"`
		Route_type string `json:"route_type"`
	} `json:"routes"`
}

func (cs *Cloudstation) Get_StaticRouteList(cp_name string) (ROUTE, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	var route_ls ROUTE
	cp_id, err1 := cs.Get_CpId(cp_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the CP Id !")
		return route_ls, false
	}

	key := "get_staticroute_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return route_ls, false
	}
	err_route := json.NewDecoder(resp.Body).Decode(&route_ls)
	if err_route != nil {
		fmt.Println(err_route)
	}
	if ok, json_data := convert_struct_to_string_via_json(route_ls); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	return route_ls, true

}

func (cs *Cloudstation) Get_StaticRouteId(cp_name string, pref string) (int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	route_ls, err1 := cs.Get_StaticRouteList(cp_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the  STATIC ROUTE List !", err1)
		return 0, false
	}

	i := 0
	var route_id int
	var found_route_id bool
	length := len(route_ls.Routes)
	for i < length {
		if route_ls.Routes[i].Prefix == pref {
			route_id = route_ls.Routes[i].Id
			found_route_id = true
			break
		}
		i++
	}
	if found_route_id == false {
		log.Err.Println("No STATIC ROUTE ID found !")
		return 0, false
	}
	return route_id, true
}

func (cs *Cloudstation) Del_StaticRoute(cp_name string, pref string) bool {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	cp_id, err1 := cs.Get_CpId(cp_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the CP Id !")
		return false
	}

	route_id, err2 := cs.Get_StaticRouteId(cp_name, pref)
	if err2 == false {
		log.Err.Println("Failed to fetch the created STATIC ROUTE Id !")
		return false
	}

	key := "delete_staticroute"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"device_id": strconv.Itoa(cp_id), "custom_id": strconv.Itoa(route_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}


/*
func (cs *Cloudstation) OldGetMatchCriteria(matchCriteria MatchCriteriaInput) (*MATCHCRITERIA, bool) {

	var M_criteria MATCHCRITERIA

	appcat_id, err1 := cs.Get_POAppCatId(matchCriteria.appCategories)
	if err1 == false {
		fmt.Println("Failed to fetch the App Category Id !", err1)
		return &M_criteria, false
	}

	appcatOnly_id, err2 := cs.Get_POAppCatOnlyId(matchCriteria.appCategories)
	if err2 == false {
		fmt.Println("Failed to fetch the App Category Id Only !", err2)
		return &M_criteria, false
	}

	app_id, err3 := cs.Get_POAppId(matchCriteria.apps)
	if err3 == false {
		fmt.Println("Failed to fetch the App Id !", err3)
		return M_criteria, false
	}

	appOnly_id, err4 := cs.Get_POAppOnlyId(matchCriteria.apps)
	if err4 == false {
		fmt.Println("Failed to fetch the App Id Only !", err4)
		return M_criteria, false
	}

	appHex_id, err5 := cs.Get_POAppHexId(matchCriteria.apps)
	if err5 == false {
		fmt.Println("Failed to fetch the App HexId !", err5)
		return M_criteria, false
	}

	srcnetgr_id, err6 := cs.Get_PONetGrpId(matchCriteria.srcNwgrp)
	if err6 == false {
		fmt.Println("Failed to fetch the SrcNwgrp Id !", err6)
		return M_criteria, false
	}

	dstnetgr_id, err7 := cs.Get_PONetGrpId(matchCriteria.dstNwgrp)
	if err7 == false {
		fmt.Println("Failed to fetch the DstNwgrp Id !", err7)
		return M_criteria, false
	}

	sub_id, err8 := cs.Get_POSubnetsId(matchCriteria.from.subnet)
	if err8 == false {
		fmt.Println("Failed to fetch the Sub Id !", err8)
		return M_criteria, false
	}

	sub_site_id, err9 := cs.Get_POSubnetsSiteId(matchCriteria.from.subnet)
	if err9 == false {
		fmt.Println("Failed to fetch the Sub Id !", err9)
		return M_criteria, false
	}

	sub_site_name, err10 := cs.Get_POSubnetsSiteName(matchCriteria.from.subnet)
	if err10 == false {
		fmt.Println("Failed to fetch the Sub Id !", err10)
		return M_criteria, false
	}

	sub_subnet, err11 := cs.Get_POSubnetsSubnet(matchCriteria.from.subnet)
	if err11 == false {
		fmt.Println("Failed to fetch the Sub Id !", err11)
		return M_criteria, false
	}

	sub_pref, err12 := cs.Get_POSubnetsPrefLen(matchCriteria.from.subnet)
	if err12 == false {
		fmt.Println("Failed to fetch the Sub Id !", err12)
		return M_criteria, false
	}

	sub_vlan, err13 := cs.Get_POSubnetsVlan(matchCriteria.from.subnet)
	if err13 == false {
		fmt.Println("Failed to fetch the Sub Id !", err13)
		return M_criteria, false
	}

	sub_id1, err14 := cs.Get_POSubnetsId(matchCriteria.to.subnet)
	if err14 == false {
		fmt.Println("Failed to fetch the Sub Id !", err14)
		return M_criteria, false
	}

	sub_site_id1, err15 := cs.Get_POSubnetsSiteId(matchCriteria.to.subnet)
	if err15 == false {
		fmt.Println("Failed to fetch the Sub Id !", err15)
		return M_criteria, false
	}

	sub_site_name1, err16 := cs.Get_POSubnetsSiteName(matchCriteria.to.subnet)
	if err16 == false {
		fmt.Println("Failed to fetch the Sub Id !", err16)
		return M_criteria, false
	}

	sub_subnet1, err17 := cs.Get_POSubnetsSubnet(matchCriteria.to.subnet)
	if err17 == false {
		fmt.Println("Failed to fetch the Sub Id !", err17)
		return M_criteria, false
	}

	sub_pref1, err18 := cs.Get_POSubnetsPrefLen(matchCriteria.to.subnet)
	if err18 == false {
		fmt.Println("Failed to fetch the Sub Id !", err18)
		return M_criteria, false
	}

	sub_vlan1, err19 := cs.Get_POSubnetsVlan(matchCriteria.to.subnet)
	if err19 == false {
		fmt.Println("Failed to fetch the Sub Id !", err19)
		return M_criteria, false
	}

	for i := 0; i < len(M_criteria.Both.Application_categories); i++ {
		M_criteria.Both.Application_categories[i].Addition_type = "OR"
		for j := 0; j < len(appcat_id); j++ {
			M_criteria.Both.Application_categories[i].Object.App_category_id = appcat_id[j]
		}
		M_criteria.Both.Application_categories[i].Object.Customer_id = cs.CustomerId
		for j := 0; j < len(appcatOnly_id); j++ {
			M_criteria.Both.Application_categories[i].Object.Id = appcatOnly_id[j]
		}
		for j := 0; j < len(matchCriteria.appCategories); j++ {
			M_criteria.Both.Application_categories[i].Object.Name = matchCriteria.appCategories[j]
		}
		M_criteria.Both.Application_categories[i].Object.Ticked = true
		M_criteria.Both.Application_categories[i].Object.User_id = cs.Id
	}

	for i := 0; i < len(M_criteria.Both.Applications); i++ {
		M_criteria.Both.Applications[i].Addition_type = "OR"
		for j := 0; j < len(app_id); j++ {
			M_criteria.Both.Applications[i].Object.Appid = app_id[j]
		}
		M_criteria.Both.Applications[i].Object.Customer_id = cs.CustomerId
		for j := 0; j < len(appHex_id); j++ {
			M_criteria.Both.Applications[i].Object.Hex_id = appHex_id[j]
		}
		for j := 0; j < len(appOnly_id); j++ {
			M_criteria.Both.Applications[i].Object.Id = appOnly_id[j]
		}
		for j := 0; j < len(matchCriteria.appCategories); j++ {
			M_criteria.Both.Application_categories[i].Object.Name = matchCriteria.appCategories[j]
		}
		M_criteria.Both.Applications[i].Object.Ticked = true
		M_criteria.Both.Applications[i].Object.User_id = cs.Id
	}

	for i := 0; i < len(M_criteria.Both.Customs); i++ {
		for j := 0; j < len(matchCriteria.customProtocols); j++ {
			M_criteria.Both.Customs[i].Addition_type = matchCriteria.customProtocols[j].condition
		}
		for j := 0; j < len(matchCriteria.customProtocols); j++ {
			M_criteria.Both.Customs[i].Object.Value = matchCriteria.customProtocols[j].protocol
		}
		M_criteria.Both.Customs[i].Object.Type = "CustomProtocol"
	}

	M_criteria.Direction = matchCriteria.direction
	M_criteria.Dst_network_group_id = dstnetgr_id

	for i := 0; i < len(M_criteria.From.Customs); i++ {
		for j := 0; j < len(matchCriteria.from.custom.ip); j++ {
			M_criteria.From.Customs[i].Addition_type = matchCriteria.from.custom.ip[j].condition
		}
		for j := 0; j < len(matchCriteria.from.custom.ip); j++ {
			M_criteria.From.Customs[i].Object.Value = matchCriteria.from.custom.ip[j].ip
		}
		M_criteria.From.Customs[i].Object.Type = "CustomIp"
	}

	for i := 0; i < len(M_criteria.From.Lan_subnets); i++ {
		M_criteria.From.Lan_subnets[i].Addition_type = "OR"
		for j := 0; j < len(sub_id); j++ {
			M_criteria.From.Lan_subnets[i].Object.Id = sub_id[j]
		}
		for j := 0; j < len(sub_site_id); j++ {
			M_criteria.From.Lan_subnets[i].Object.Site_id = sub_site_id[j]
		}
		for j := 0; j < len(sub_site_name); j++ {
			M_criteria.From.Lan_subnets[i].Object.Site_name = sub_site_name[j]
		}
		for j := 0; j < len(sub_subnet); j++ {
			M_criteria.From.Lan_subnets[i].Object.Subnet = sub_subnet[j]
		}
		for j := 0; j < len(matchCriteria.from.subnet); j++ {
			M_criteria.From.Lan_subnets[i].Object.Subnet_name = matchCriteria.from.subnet[j]
		}
		for j := 0; j < len(sub_pref); j++ {
			M_criteria.From.Lan_subnets[i].Object.Subnet_prefix_length = sub_pref[j]
		}
		M_criteria.From.Lan_subnets[i].Object.Ticked = true
		for j := 0; j < len(sub_vlan); j++ {
			M_criteria.From.Lan_subnets[i].Object.Vlan = sub_vlan[j]
		}
	}

	M_criteria.Src_network_group_id = srcnetgr_id

	for i := 0; i < len(M_criteria.To.Customs); i++ {
		for j := 0; j < len(matchCriteria.to.custom.ip); j++ {
			M_criteria.To.Customs[i].Addition_type = matchCriteria.to.custom.ip[j].condition
		}
		for j := 0; j < len(matchCriteria.to.custom.ip); j++ {
			M_criteria.To.Customs[i].Object.Value = matchCriteria.to.custom.ip[j].ip
		}
		M_criteria.To.Customs[i].Object.Type = "CustomIp"
	}

	for i := 0; i < len(M_criteria.To.Lan_subnets); i++ {
		M_criteria.To.Lan_subnets[i].Addition_type = "OR"
		for j := 0; j < len(sub_id1); j++ {
			M_criteria.To.Lan_subnets[i].Object.Id = sub_id1[j]
		}
		for j := 0; j < len(sub_site_id1); j++ {
			M_criteria.To.Lan_subnets[i].Object.Site_id = sub_site_id1[j]
		}
		for j := 0; j < len(sub_site_name1); j++ {
			M_criteria.To.Lan_subnets[i].Object.Site_name = sub_site_name1[j]
		}
		for j := 0; j < len(sub_subnet1); j++ {
			M_criteria.To.Lan_subnets[i].Object.Subnet = sub_subnet1[j]
		}
		for j := 0; j < len(matchCriteria.to.subnet); j++ {
			M_criteria.To.Lan_subnets[i].Object.Subnet_name = matchCriteria.to.subnet[j]
		}
		for j := 0; j < len(sub_pref1); j++ {
			M_criteria.To.Lan_subnets[i].Object.Subnet_prefix_length = sub_pref1[j]
		}
		M_criteria.To.Lan_subnets[i].Object.Ticked = true
		for j := 0; j < len(sub_vlan1); j++ {
			M_criteria.To.Lan_subnets[i].Object.Vlan = sub_vlan1[j]
		}
	}

	return M_criteria, true
}
*/

/*
		Customer struct {
			Id int `json:"id"`
			Name string `json:"name"`
			Manager string `json:"manager"`
			Hex_id string `json:"hex_id"`
			Url string `json:"url"`
			License_name string `json:"license_name"`
			Network_size int `json:"network_size"`
			Admin_assist_tickets int `json:"admin_assist_tickets"`
			License_key string `json:"license_key"`
		} `json:"customer"`
*/

/*
type CreateCustomerResponse struct {
	Message string `json:"message"`
	Data struct {
		Customer CustomerInfo `json:"customer"`
		Net_admin struct {
			Email string `json:"email"`
			Password string `json:"password"`
		} `json:"net_admin"`
		Sys_admin struct {
			Email string `json:"email"`
			Password string `json:"password"`
		} `json:"sys_admin"`

	} `json:"data"`
}

type CustomerManager struct {
    Name string `json:"name"`
    Email string `json:"email"`
}

type CustomerManagerResponse struct {
    Managers []CustomerManager `json:"managers"`
}

*/

//type EmptyHttpPayload  map[string]interface{}{}
//type EmptyApiPathSubstitute map[string]string
//type EmptyHttpRequestQueryParams  map[string]interface{}{}

/*
func (cs *Cloudstation) GetListOfManagers() (bool, error, *CustomerManagerResponse) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var (
	    //res bool
	    //err error
	    manager_response CustomerManagerResponse
            //resp *http.Response
	    //payload EmptyHttpPayload 
	)

        key := "get_list_of_managers"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{})

        header := cs.Construct_headers(apis[key].headers.headers)
        cookie := []*http.Cookie{cs.Cookies}
	resp, _ := request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, map[string]interface{}{}, cookie)
        if resp.StatusCode != 200 {
            return false, nil, &manager_response
        }

        err12 := json.NewDecoder(resp.Body).Decode(&manager_response)
	//fmt.Println("manager repsne: ",manager_response)
        if err12 != nil {
            log.Err.Println(err12)
            return false, nil, &manager_response
        }
	if ok, json_data := convert_struct_to_string_via_json(manager_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	return true, nil, &manager_response
}

func (cs *Cloudstation) CreateCustomer(create_customer_input *createCustomerInput) (bool, error, *CreateCustomerResponse) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	log.Info.Printf("Creating a customer: %+v\n", create_customer_input)
    var (
//        res bool
//err error
//manager_response *CustomerManagerResponse
create_customer_response CreateCustomerResponse
resp *http.Response
createCustomer struct {
       Name string `json:"name"`
       Sys_admin_email string `json:"email"`
       Net_admin_email string `json:"net_admin_email"`
       Manager CustomerManager `json:"manager"`
       Net_admin_emails []string `json:"net_admin_emails"`
       License_name string `json:"license_name"`
       Network_size int `json:"network_size"`
       Admin_assist_tickets int `json:"admin_assist_tickets"`
       License_key string `json:"license_key"`
       Domain string `json:"domain"`
       Url_prefix string `json:"url_prefix"`
       Url string `json:"url"`
}
         )

	 res, _, manager_response := cs.GetListOfManagers()
	 if !res {
	    return false, nil, nil
         }

    for _, manager := range manager_response.Managers {
	    //fmt.Println(manager)
        if strings.ToLower(create_customer_input.Managed_by)==strings.ToLower(manager.Name) {
            createCustomer.Manager = manager
	    break
        }
    }

    createCustomer.Name = create_customer_input.Customer_name
    createCustomer.Sys_admin_email = create_customer_input.Sys_admin_email
    createCustomer.Net_admin_email = create_customer_input.Net_admin_email
    createCustomer.Net_admin_emails = []string{}
    createCustomer.License_name = create_customer_input.License_name
    createCustomer.Network_size = create_customer_input.Network_size
    createCustomer.Admin_assist_tickets = create_customer_input.Admin_assist_tickets
    createCustomer.License_key = create_customer_input.License_key
    createCustomer.Domain = create_customer_input.Domain
    createCustomer.Url_prefix = create_customer_input.Url_prefix
    createCustomer.Url = fmt.Sprintf("%s.%s", create_customer_input.Url_prefix, create_customer_input.Domain)


    key := "create_customer"
    apis := getApis()
    api := apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{})

		//filename := fmt.Sprintf("%s_%s.json", "post", strings.Replace(substitutedPath, "/", "_", -1))
		//file, _ := json.MarshalIndent(createCustomer, "", " ")
		//file, _ := json.Marshal(createCustomer)
	        //_ = ioutil.WriteFile(filename, file, 0666)

    header := cs.Construct_headers(apis[key].headers.headers)
    cookie := []*http.Cookie{cs.Cookies}
    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, createCustomer, cookie)
    if resp.StatusCode != 200 {
        return false, nil, &create_customer_response
    }

    err12 := json.NewDecoder(resp.Body).Decode(&create_customer_response)
    if err12 != nil {
	log.Err.Println(err12)
	return false, nil, &create_customer_response
    }
	if ok, json_data := convert_struct_to_string_via_json(create_customer_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
    return true, nil, &create_customer_response
}

type CustomerInfo struct {
	AdminAssistTickets int `json:"admin_assist_tickets"`
	//AdminEmail string `json:"admin_email"`
	Banner *string `json:"banner"`
	CreatedAt string `json:"created_at"`
	EnableInftMatch *string `json:"enable_intf_match"`
	EnableNotifications *string `json:"enable_notifications"`
	HexId string `json:"hex_id"`
	Id int `json:"id"`
	LicenseKey string `json:"license_key"`
	LicenseName string `json:"license_name"`
	Manager string `json:"manager"`
	ManagerEmail string `json:"manager_email"`
	Name string `json:"name"`
	NetworkSize int `json:"network_size"`
	Url string `json:"url"`
	UpdatedAt string `json:"updated_at"`
}

type GetCustomerListResponse struct {
	Customers []CustomerInfo `json:"customers"`
	DefaultDomain string `json:"default_domain"`
}

func (cs *Cloudstation) GetCustomerList() (bool, error, map[string]CustomerInfo) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var (
		customer_list_response GetCustomerListResponse
		resp *http.Response
		dummy struct {}
	)
    customer_info_wrt_name :=make(map[string]CustomerInfo)
    key := "get_customer_list"
    apis := getApis()
    api := apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{})

    header := cs.Construct_headers(apis[key].headers.headers)
    cookie := []*http.Cookie{cs.Cookies}
    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)

    if resp.StatusCode != 200 {
        return false, nil, customer_info_wrt_name
    }

    err12 := json.NewDecoder(resp.Body).Decode(&customer_list_response)

	if ok, json_data := convert_struct_to_string_via_json(customer_list_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
    for _, customer := range customer_list_response.Customers {
	    customer_info_wrt_name[customer.Name] = customer
    }
    if err12 != nil {
	log.Err.Println(err12)
	return false, nil, customer_info_wrt_name
    }

    return true, nil, customer_info_wrt_name
}

type DeleteCustomerResponse struct {
	Message string `json:"message"`
}

func (cs *Cloudstation) DeleteCustomer(customer_name string) (bool, error, *DeleteCustomerResponse) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	log.Info.Printf("Deleting the customer: %s\n", customer_name)
    var (
         delete_customer_response DeleteCustomerResponse
         resp *http.Response
	 dummy struct {}
	 customer_id int
        )

    _, _, customer_name_map := cs.GetCustomerList()

    if customer_info, ok := customer_name_map[customer_name]; ok {
	    customer_id = customer_info.Id
    } else {
        return false, nil, &delete_customer_response
    }

    key := "delete_customer"
    apis := getApis()
    api := apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{"customer_id": fmt.Sprintf("%d", customer_id)})

    header := cs.Construct_headers(apis[key].headers.headers)
    cookie := []*http.Cookie{cs.Cookies}
    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
    var err_reason error
    if resp.StatusCode != 200 {
        err_reason = errors.New(fmt.Sprintf("HTTP statuscode :%s", resp.StatusCode))
        log.Err.Println("Failed to delete the customer: %s\nReason: %s", customer_name, err_reason.Error())
        return false, err_reason, &delete_customer_response
    }

    err12 := json.NewDecoder(resp.Body).Decode(&delete_customer_response)
    if err12 != nil {
	log.Err.Println(err12)
	return false, nil, &delete_customer_response
    }

	if ok, json_data := convert_struct_to_string_via_json(delete_customer_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}

    log.Info.Printf("Successfully deleted the customer: %s\n", customer_name)
    return true, nil, &delete_customer_response
}

*/

/*
func (cs *Cloudstation) CreateAclPolicy(aclinputs *CreateAclInputs) (bool, *CreateAclResponse) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var Aclpol_payload struct {
		Name       string `json:"name"`
		Priority   int    `json:"priority"`
		Tag        string `json:"tag"`
		Acl_action string `json:"acl_action"`
		Conn_state string `json:"conn_state"`
		MATCHCRITERIA
	}

	Aclpol_payload.Name = aclinputs.Name
	Aclpol_payload.Priority = aclinputs.GetAclPriority()
	Aclpol_payload.Tag = aclinputs.GetAclTag()
	Aclpol_payload.Acl_action = aclinputs.GetAclAction()
	Aclpol_payload.Conn_state = aclinputs.GetAclConnectionState()
	matchcr, _ := cs.GetMatchCriteria(aclinputs.MatchCriteriaInput)
	Aclpol_payload.MATCHCRITERIA = *matchcr

	key := "conf_aclpolicy"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	var acl_response CreateAclResponse
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, Aclpol_payload, cookie)
	//fmt.Println(resp)

	if resp.StatusCode != 200 {
		return false, &acl_response
	}

	err_acl := json.NewDecoder(resp.Body).Decode(&acl_response)
	if err_acl != nil {
		log.Err.Println(err_acl)
	}
	if ok, json_data := convert_struct_to_string_via_json(acl_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	return true, &acl_response
}
*/


type UsersList struct {
	Users []struct {
		AllowManage bool `json:"allow_manage"`
		Customer    struct {
			Name string `json:"name"`
		} `json:"customer"`
		CustomerId int    `json:"customer_id"`
		Email      string `json:"email"`
		Id         int    `json:"id"`
		Role       string `json:"role"`
	} `json:"users"`
	Message string `json:"message"`
}

func (cs *Cloudstation) GetUserList() (UsersList, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var ListUser UsersList
	var dummy struct {
	}
	key := "get_users_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

        header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//resp, _ = http.Post(fmt.Sprintf("%s/%s", cs.Base_url, substitutedPath),  )
	//fmt.Println(err)
	if resp.StatusCode != 200 {
		return ListUser, false
	}

	err_user := json.NewDecoder(resp.Body).Decode(&ListUser)
	if err_user != nil {
		log.Err.Println(err_user)
	}
	if ok, json_data := convert_struct_to_string_via_json(ListUser); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	//fmt.Println("--------------Got into the Verse for Getting the CP List !--------------")
	return ListUser, true
}

func (cs *Cloudstation) GetUserId(User string) (bool, error, int) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	List, errList := cs.GetUserList()
	var isFound bool
	if errList == false {
		return isFound, fmt.Errorf("Unable to fetch Users List"), 0
	}

	var userId int
	for i := 0; i < len(List.Users); i++ {
		if List.Users[i].Email == User {
			isFound = true
			userId = List.Users[i].Id
			break
		}
	}
	if isFound == false {
		return isFound, fmt.Errorf("User Not Found"), 0
	}
	//fmt.Println("UserFound")
	return isFound, nil, userId

}

type map_place interface{}

/*

type CreateSiteResponse struct {
	Message string `json:"message"`
	Site SiteInfo  `json:"site"`
}

func (cs *Cloudstation) CreateSite(Site *CreateSiteInput) (error, bool, *CreateSiteResponse) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var placeinfo map_place
	var create_site_response CreateSiteResponse
	var createSitePayload struct {
		Name                 string    `json:"name"`
		Location             string    `json:"location"`
		Tags                 string    `json:"tags"`
		Is_hub               bool      `json:"is_hub"`
		Profile_id           string    `json:"profile_id"`
		User_id              int       `json:"user_id"`
		Enable_mac_filtering bool      `json:"enable_mac_filtering"`
		Dns_proxy_id         string    `json:"dns_proxy_id"`
		Dns_proxy_enable     bool      `json:"dns_proxy_enable"`
		Snmp_mapping_id      string    `json:"snmp_mapping_id"`
		Snmp_mapping_enable  bool      `json:"snmp_config_enable"`
		Auto_upgrade         bool      `json:"auto_upgrade"`
		Place                map_place `json:"place"`
		Place_id             string    `json:"place_id"`
	}

    	createSitePayload.Name = Site.Name
	createSitePayload.Location = "Bangalore, Karnataka, India"
	createSitePayload.Tags = ""
	createSitePayload.Is_hub = Site.Is_hub
	createSitePayload.Profile_id = ""
	createSitePayload.Dns_proxy_id = ""
	createSitePayload.Dns_proxy_enable = false
	createSitePayload.Snmp_mapping_id = ""
	createSitePayload.Snmp_mapping_enable = false
	createSitePayload.Auto_upgrade = false
	createSitePayload.Place = placeinfo
	createSitePayload.Place_id = "ChIJbU60yXAWrjsR4E9-UejD3_g"
	isFound, errList, userId := cs.GetUserId(Site.User)
	if isFound == false {
		return errList, false, &create_site_response
	}
	createSitePayload.User_id = userId
	key := "create_site"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
        header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{}
        //payload_str, marshal_err := json.Marshal(createSitePayload)

        //if marshal_err == nil {
        //    log.Debug.Printf("create site payload: %s\n", payload_str)
        //} else {
        //    log.Debug.Printf("create site payload: %s\n", "Marshalling failed")
        //}

    	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, createSitePayload, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return nil, false, &create_site_response
	}

	err_site := json.NewDecoder(resp.Body).Decode(&create_site_response)
	if err_site != nil {
		fmt.Println(err_site)
	}

	if ok, json_data := convert_struct_to_string_via_json(create_site_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
       cs.Helper.site.byId[create_site_response.Site.Id] = create_site_response.Site
       cs.Helper.site.byName[create_site_response.Site.Name] = create_site_response.Site

	return nil, true, &create_site_response
}

type SiteInfo struct {
		AutoUpgrade          bool   `json:"auto_upgrade"`
		CloudportUrlConfig   string `json:"cloudport_url_config"`
		CreatedAt            string `json:"created_at"`
		CustomerdID          int    `json:"customer_id"`
		Dns_proxy_enable     bool   `json:"dns_proxy_enable"`
		Dns_proxy_id         int    `json:"dns_proxy_id"`
		Enable_mac_filtering bool   `json:"enable_mac_filtering"`
		HexId                string `json:"hex_id"`
		Id                   int    `json:"id"`
		Is_hub               bool   `json:"is_hub"`
		Latitude             string `json:"latitude"`
		location             string `json:"location"`
		Longitude            string `josn:"longitude"`
		Name                 string `json:"name"`
		PlaceId              string `json:"place_id"`
		ProfileId            int    `json:"profile_id"`
		Snmp_mapping_enable  int    `json:"snmp_config_enable"`
		Snmp_mapping_id      string `json:"snmp_mapping_id"`
		Tags                 string `json:"tags"`
		UpdatedAt           string `json:"updated_at"`
		UrlFiltering         bool   `json:"url_filtering"`
		UrlProfileId         int    `json:"url_profile_id"`
		UserId               int    `json:"user_id"`
}


type GetSiteListResponse struct {
	Site []SiteInfo `json:"sites"`
	TotalCount int `json:"total_count"`
}


type GetDeviceListResponseDeviceInfo struct {
	Cnid string `json:"cnid"`
        Id int `json:"id"`
	SerialNo string `json:"serial_no"`
	S_Number string `json:"s_number"`
	Name string `json:"name"`
	SiteId int `json:"site_id"`
        Site SiteInfo `json:"site"`
}

type GetDeviceListResponse struct {
	Cloudports []GetDeviceListResponseDeviceInfo `json:"cloudports"`
	TotalCount int `json:"total_count"`
}



func (cs *Cloudstation) GetDeviceList() (bool, error, map[int]GetDeviceListResponseDeviceInfo, map[string]GetDeviceListResponseDeviceInfo, map[string]GetDeviceListResponseDeviceInfo, map[string][]GetDeviceListResponseDeviceInfo) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}
	key := "get_device_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
        header := cs.Construct_headers(apis[key].headers.headers)
	//fmt.Println(header)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

      	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)

	var device_list_resp  GetDeviceListResponse
	device_info := make(map[int]GetDeviceListResponseDeviceInfo)
	device_info_wrt_name := make(map[string]GetDeviceListResponseDeviceInfo)
	device_info_wrt_slno := make(map[string]GetDeviceListResponseDeviceInfo)
	device_info_wrt_sitename := make(map[string][]GetDeviceListResponseDeviceInfo)

	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false, nil, device_info, device_info_wrt_name, device_info_wrt_slno, device_info_wrt_sitename
	}

	err_site := json.NewDecoder(resp.Body).Decode(&device_list_resp)
	if err_site != nil {
		fmt.Println(err_site)
	}
	if ok, json_data := convert_struct_to_string_via_json(device_list_resp); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}

	tot_entry := device_list_resp.TotalCount

	for _, cp := range device_list_resp.Cloudports {
	    //device_list = append(device_list, cp)
	    device_info[cp.Id] = cp
	    device_info_wrt_name[cp.Name] = cp
	    device_info_wrt_slno[cp.SerialNo] = cp
	    //fmt.Println(cp.Site.Name)
	    device_info_wrt_sitename[cp.Site.Name] = append(device_info_wrt_sitename[cp.Site.Name], cp)
	}

	//fmt.Println(SiteList)
	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}
	i := 2
	for i <= tot_page {
		//var SingleSite1 SiteInfoList
		queryParam = map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}

		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		if resp.StatusCode != 200 {

		//	return false, nil, device_info
		return false, nil, device_info, device_info_wrt_name, device_info_wrt_slno, device_info_wrt_sitename
		}
		err_site = json.NewDecoder(resp.Body).Decode(&device_list_resp)
		if err_site != nil {
			log.Err.Println(err_site)
		}
	if ok, json_data := convert_struct_to_string_via_json(device_list_resp); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
		//fmt.Println(SingleSite)
		//SiteList = append(SiteList, SingleSite1)
		for _, cp := range device_list_resp.Cloudports {
		    //device_list = append(device_list, cp)
		    device_info[cp.Id] = cp
		    device_info_wrt_name[cp.Name] = cp
		    device_info_wrt_slno[cp.SerialNo] = cp
	            device_info_wrt_sitename[cp.Site.Name] = append(device_info_wrt_sitename[cp.Site.Name], cp)
		}
		//fmt.Println(SiteList)
		i++
	}

	//fmt.Println(SiteList)
	//fmt.Println("--------------Got into the Verse for Getting the CP List !--------------")
	//return  true, nil, device_info

		return true, nil, device_info, device_info_wrt_name, device_info_wrt_slno, device_info_wrt_sitename
}
*/
/*
func (cs *Cloudstation) SiteList() (bool, error, map[int]SiteInfo, map[string]SiteInfo) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	//log.Debug.Printf("FUNC: %s exit\n", tc_utils.FuncName())
	var dummy struct {
	}
	key := "get_site_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
        header := cs.Construct_headers(apis[key].headers.headers)
	//fmt.Println(header)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

      	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//resp, _ = http.Post(fmt.Sprintf("%s/%s", cs.Base_url, substitutedPath),  )
	//fmt.Println(err)
	//var SiteList []SiteInfoList
	//var SingleSite SiteInfoList

	var get_site_list_response GetSiteListResponse
	site_info := make(map[int]SiteInfo)
	site_info_wrt_name := make(map[string]SiteInfo)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false, nil, site_info, site_info_wrt_name
	}

	err_site := json.NewDecoder(resp.Body).Decode(&get_site_list_response)
	if err_site != nil {
		log.Err.Println(err_site)
	}
	if ok, json_data := convert_struct_to_string_via_json(get_site_list_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	tot_entry := get_site_list_response.TotalCount
	log.Debug.Printf("    Total Site Count: %d\n", tot_entry)

	//SiteList = append(SiteList, SingleSite)
	for _, site := range get_site_list_response.Site {
            site_info_wrt_name[site.Name] = site
            site_info[site.Id] = site
	}
	//fmt.Println(SiteList)
	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}
	i := 2
	for i <= tot_page {
		//var SingleSite1 SiteInfoList
		queryParam = map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}

		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		if resp.StatusCode != 200 {

		//	return site_info, false
		return false, nil, site_info, site_info_wrt_name
		}
		err_site = json.NewDecoder(resp.Body).Decode(&get_site_list_response)
		if err_site != nil {
			log.Err.Println(err_site)
		}
	if ok, json_data := convert_struct_to_string_via_json(get_site_list_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
		//fmt.Println(SingleSite)
		//SiteList = append(SiteList, SingleSite1)
		for _, site := range get_site_list_response.Site {
                    site_info[site.Id] = site
                    site_info_wrt_name[site.Name] = site
		}
		//fmt.Println(SiteList)
		i++
	}

	//fmt.Println(SiteList)
	//fmt.Println("--------------Got into the Verse for Getting the CP List !--------------")
	//return site_info, true

	//fmt.Println(site_info)
	//fmt.Println(site_info_wrt_name)
	return true, nil, site_info, site_info_wrt_name
}




func (cs *Cloudstation) GetSiteID(SiteName string) (bool, error, int) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	//_, _, _, site_info_wrt_name  := cs.SiteList()
	//fmt.Println(Sitelist)
	//var isFound bool
	//var siteId int
	if siteinfo, ok := cs.SitesByName()[SiteName]; ok {
		return true, nil, siteinfo.Id
	}
	return false, nil, 0
}

func (cs *Cloudstation) AddDevice(Device *DeviceInput) (error, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var AddDevicePayload struct {
		Authentication struct {
			Local struct {
				Password string `json:"password"`
				Username string `json:"username"`
			} `json:"local"`
			Type string `json:"type"`
		} `json:"authentication"`
		HardwareAttachmentType string `json:"hardware_attachment_type"`
		HardwarePort           string `json:"hardware_port"`
		IsHardwareAttached     bool   `json:"is_hardware_attached"`
		ModelType              string `json:"model_type"`
		Name                   string `json:"name"`
		Snumber                string `json:"s_number"`
		SerialNumber           string `json:"serial_no"`
		SiteId                 int    `json:"site_id"`
	}

	AddDevicePayload.Authentication.Local.Username = Device.Authentication.Local.Username
	AddDevicePayload.Authentication.Local.Password = Device.Authentication.Local.Password
	AddDevicePayload.Authentication.Type = Device.Authentication.Type
	AddDevicePayload.HardwareAttachmentType =   Device.GetHardwareAttachmentType()
	AddDevicePayload.HardwarePort = Device.GetHardwarePort()
	AddDevicePayload.IsHardwareAttached = Device.IsHardwareAttached
	AddDevicePayload.ModelType = Device.ModelType
	AddDevicePayload.Name = Device.Name
	AddDevicePayload.Snumber = Device.GetUUID()
	AddDevicePayload.SerialNumber = Device.GetSerialNumber()
	isFound, errList, Siteid := cs.GetSiteID(Device.SiteName)
	//fmt.Println(Siteid)
	log.Debug.Printf(" Site ID : %d\n", Siteid)
	if isFound == false {
		//return errList, false
		log.Err.Println(errList, false)
	}
	AddDevicePayload.SiteId = Siteid

	key := "add_device"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
        header := cs.Construct_headers(apis[key].headers.headers)
	//fmt.Println(header)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{}

        payload_str, marshal_err := json.Marshal(AddDevicePayload)

        if marshal_err == nil {
            log.Debug.Printf("add device payload: %s\n", payload_str)
        } else {
            log.Debug.Printf("add device payload: %s\n", "Marshalling failed")
        }

	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, AddDevicePayload, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return nil, false
	}

	return nil, true
}

*/


/*
func (cs *Cloudstation) DeleteDevice(device_name string) (error, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}
	key := "delete_device"
	apis := getApis()
	api := apis[key]
	device_info := cs.DevicesByName()[device_name]
	//fmt.Println(device_info, device_info.Id)
	substitutedPath := substitutePath(apis[key], map[string]string{"cloudport_id": fmt.Sprintf("%d", device_info.Id)})
        header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return nil, false
	}

	return nil, true
}

type AclInfo struct {
		Acl_action             string `json:"acl_action"`
		Conn_state             string `json:"conn_state"`
		CreatedAt              string `json:"created_at"`
		Customer_id            int    `json:"customer_id"`
		Direction              string `json:"direction"`
		Dst_network_group_id   int    `json:"dst_network_group_id"`
		Dst_network_group_name string `json:"dst_network_group_name"`
		Hex_id                 string `json:"hex_id"`
		Id                     int    `json:"id"`
		Name                   string `json:"name"`
		Priority               int    `json:"priority"`
		Src_network_group_id   int    `json:"src_network_group_id"`
		Src_network_group_name string `json:"src_network_group_name"`
		Tag                    string `json:"tag"`
		UpdatedAt              string `json:"updated_at"`
		User_id                int    `json:"user_id"`
}

type ACLPOLICY struct {
	Acls []AclInfo  `json:"acls"`
	Total_count int `json:"total_count"`
}

type CreateAclResponse struct {
	Data struct {
		Acls AclInfo `json:"acls"`
	} `json:"data"`
	Message string `json:"message"`
}

func (cs *Cloudstation) Get_AclPolicyList() ([]ACLPOLICY, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	key := "get_aclpolicy_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

	var aclpol_ls []ACLPOLICY
	var aclpol ACLPOLICY
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return aclpol_ls, false
	}
	err_aclpolicy := json.NewDecoder(resp.Body).Decode(&aclpol)
	if err_aclpolicy != nil {
		fmt.Println(err_aclpolicy)
	}
	if ok, json_data := convert_struct_to_string_via_json(aclpol); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	aclpol_ls = append(aclpol_ls, aclpol)

	tot_entry := aclpol.Total_count
	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}

	i := 2
	for i <= tot_page {
		var aclpol1 ACLPOLICY
		queryParam = map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		if resp.StatusCode != 200 {
			return aclpol_ls, false
		}
		err_aclpolicy := json.NewDecoder(resp.Body).Decode(&aclpol1)
		if err_aclpolicy != nil {
			log.Err.Println(err_aclpolicy)
		}
	if ok, json_data := convert_struct_to_string_via_json(aclpol1); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
		aclpol_ls = append(aclpol_ls, aclpol1)
		i++
	}
	return aclpol_ls, true
}

func (cs *Cloudstation) Get_AclHexId(acl_name string) (string, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	aclpol_ls, err1 := cs.Get_AclPolicyList()
	if err1 == false {
		log.Err.Println("Failed to fetch the ACL POLICY List !", err1)
		return "", false
	}

	var hex_id string
	var found_hex_id bool
	for i := 0; i < len(aclpol_ls); i++ {
		for j := 0; j < 10; j++ {
			if aclpol_ls[i].Acls[j].Name == acl_name {
				hex_id = aclpol_ls[i].Acls[j].Hex_id
				found_hex_id = true
				break
			}
		}
		if found_hex_id == true {
			break
		}
	}
	if found_hex_id == false {
		log.Err.Println("No Hex Id found !")
		return "", false
	}
	return hex_id, true
}

func (cs *Cloudstation) DeleteAclPolicy(acl_name string) bool {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	hex_id, err1 := cs.Get_AclHexId(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the Hex Id !")
		return false
	}

	var dummy struct {
	}

	key := "delete_aclpolicy"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": hex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		log.Err.Println("The Policy is been Attached !")
		return false
	}
	return true
}

type ACLpreviewLIST struct {
	Acls struct {
		Hex_id string `json:"hex_id"`
		Id     int    `json:"id"`
		Name   string `json:"name"`
	} `json:"acls"`
	Status string `json:"status"`
}

func (cs *Cloudstation) Get_AclPreviewList(acl_name string) (ACLpreviewLIST, bool) {


	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var aclpreview_ls ACLpreviewLIST
	var dummy struct {
	}

	aclhex_id, err1 := cs.Get_AclHexId(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the Acl Hex Id !", err1)
		return aclpreview_ls, false
	}

	key := "get_aclpreview_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": aclhex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return aclpreview_ls, false
	}
	err_aclpreview := json.NewDecoder(resp.Body).Decode(&aclpreview_ls)
	if err_aclpreview == nil {
		fmt.Println(err_aclpreview)
	}
	if ok, json_data := convert_struct_to_string_via_json(aclpreview_ls); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	return aclpreview_ls, true
}

func (cs *Cloudstation) Get_AclId(acl_name string) (int, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	aclpreview_ls, err1 := cs.Get_AclPreviewList(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the ACL PREVIEW List !", err1)
		return 0, false
	}

	var id int
	var found_id bool
	if aclpreview_ls.Acls.Name == acl_name {
		id = aclpreview_ls.Acls.Id
		found_id = true
	}
	if found_id == false {
		log.Err.Println("No Id found !")
		return 0, false
	}
	//fmt.Println("--------------Got into the Verse for Getting the ACL ID from preview !--------------")
	return id, true
}
*/
// Attaching and Detaching the Policy at Network Group Level
type NetworkGroupInfo struct {
	Hex_id                string  `json:"hex_id"`
	Id                    int     `json:"id"`
	Location              *string `json:"location"`
	N_id                  int     `json:"n_id"`
	Name                  string  `json:"name"`
	Next_hub              string  `json:"next_hop"`
	Security_profile_id   int     `json:"security_profile_id"`
	Security_profile_name string  `json:"security_profile_name"`
	Subnets               string  `json:"subnets"`
	Tag                   *string `json:"tag"`
	Vpn                   *string `json:"vpn"`
	Vpn_profile_id        int     `json:"vpn_profile_id"`
	Vpn_profile_name      string  `json:"vpn_profile_name"`
}

type NETWORK_GRP struct {
	Network_groups []NetworkGroupInfo `json:"network_groups"`
	Total_count int `json:"total_count"`
}

func (cs *Cloudstation) Get_NetworkGroupList() ([]NETWORK_GRP, map[string]NetworkGroupInfo, bool) {

    if len(cs.Helper.nwgrp.byName) == 0 {
	    return cs.ForceGet_NetworkGroupList()
    }

    return cs.Helper.nwgrp.nwgrplist, cs.Helper.nwgrp.byName, true

}

func (cs *Cloudstation) ForceGet_NetworkGroupList() ([]NETWORK_GRP, map[string]NetworkGroupInfo, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	key := "get_networkgrp_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
	nw_grp_info_wrt_name := make(map[string]NetworkGroupInfo)

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

	var nwgrp_ls []NETWORK_GRP
	var nwgrp NETWORK_GRP
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return nwgrp_ls, nw_grp_info_wrt_name, false
	}
	err_nwgrp := json.NewDecoder(resp.Body).Decode(&nwgrp)
	if err_nwgrp != nil {
		fmt.Println(err_nwgrp)
	}
	if ok, json_data := convert_struct_to_string_via_json(nwgrp); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	nwgrp_ls = append(nwgrp_ls, nwgrp)

	for _, nw_grp_info := range nwgrp.Network_groups {
		nw_grp_info_wrt_name[nw_grp_info.Name] = nw_grp_info
	}

	tot_entry := nwgrp.Total_count
	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}

	i := 2
	for i <= tot_page {
		var nwgrp1 NETWORK_GRP
		queryParam := map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		//fmt.Println(resp)
		if resp.StatusCode != 200 {
			return nwgrp_ls, nw_grp_info_wrt_name, false
		}
		err_nwgrp = json.NewDecoder(resp.Body).Decode(&nwgrp1)
		if err_nwgrp != nil {
			log.Err.Println(err_nwgrp)
		}
		for _, nw_grp_info := range nwgrp1.Network_groups {
			nw_grp_info_wrt_name[nw_grp_info.Name] = nw_grp_info
		}
	if ok, json_data := convert_struct_to_string_via_json(nwgrp1); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
		nwgrp_ls = append(nwgrp_ls, nwgrp1)
		i++
	}
	cs.Helper.nwgrp.nwgrplist = nwgrp_ls
	cs.Helper.nwgrp.byName = nw_grp_info_wrt_name

	return nwgrp_ls, nw_grp_info_wrt_name, true
}

func (cs *Cloudstation) Get_NwGrpHexId(nwgrp_name string) (string, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	nwgrp_ls, _, err1 := cs.Get_NetworkGroupList()
	if err1 == false {
		log.Err.Println("Failed to fetch the NETWORK GROUP List !", err1)
		return "", false
	}

	var hex_id string
	var found_hex_id bool
	for i := 0; i < len(nwgrp_ls); i++ {
		for j := 0; j < len(nwgrp_ls[i].Network_groups); j++ {
			if nwgrp_ls[i].Network_groups[j].Name == nwgrp_name {
				hex_id = nwgrp_ls[i].Network_groups[j].Hex_id
				found_hex_id = true
				break
			}
		}
		if found_hex_id == true {
			break
		}
	}
	if found_hex_id == false {
		log.Err.Println("No Hex Id found !")
		return "", false
	}
	return hex_id, true
}

func (cs *Cloudstation) Get_NwGrpId(nwgrp_name string) (int, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	nwgrp_ls, _, err1 := cs.Get_NetworkGroupList()
	if err1 == false {
		log.Err.Println("Failed to fetch the NETWORK GROUP List !", err1)
		return 0, false
	}

	var id int
	var found_id bool
	for i := 0; i < len(nwgrp_ls); i++ {
		for j := 0; j < len(nwgrp_ls[i].Network_groups); j++ {
			if nwgrp_ls[i].Network_groups[j].Name == nwgrp_name {
				id = nwgrp_ls[i].Network_groups[j].Id
				found_id = true
				break
			}
		}
		if found_id == true {
			break
		}
	}
	if found_id == false {
		log.Err.Println("No Hex Id found !")
		return 0, false
	}

	return id, true
}
/*
func (cs *Cloudstation) AttachAclAtNwGrp(acl_name string, nwgrp_name string) bool {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	aclhex_id, err1 := cs.Get_AclHexId(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the Acl Hex Id !")
		return false
	}

	nwgrphex_id, err2 := cs.Get_NwGrpHexId(nwgrp_name)
	if err2 == false {
		log.Err.Println("Failed to fetch the NwGrp Hex Id !")
		return false
	}

	var attach_nwgrp_payload struct {
		Attached_to_id   string `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_nwgrp_payload.Attached_to_id = nwgrphex_id
	attach_nwgrp_payload.Attached_to_type = "NetworkGroup"

	key := "attach_at_networkgrp"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": aclhex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_nwgrp_payload, cookie)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}

func (cs *Cloudstation) DetachAclAtNwGrp(acl_name string, nwgrp_name string) bool {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	acl_id, err1 := cs.Get_AclId(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the Acl Id !")
		return false
	}

	nwobject_id, err2 := cs.Get_NwGrpId(nwgrp_name)
	if err2 == false {
		log.Err.Println("Failed to fetch the NwGrp Object Id !")
		return false
	}

	var detach_nwgrp_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_nwgrp_payload.Object_id = nwobject_id
	detach_nwgrp_payload.Object_type = "NetworkGroup"

	key := "detach_at_networkgrp"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(acl_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_nwgrp_payload, cookie)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}
*/
// Attaching and Detaching the Policy at Site Level

type SITE struct {
	Sites []struct {
		AutoUpgrade          bool   `json:"auto_upgrade"`
		CloudportUrlConfig   string `json:"cloudport_url_config"`
		CreatedAt            string `json:"created_at"`
		CustomerdID          int    `json:"customer_id"`
		Dns_proxy_enable     bool   `json:"dns_proxy_enable"`
		Dns_proxy_id         int    `json:"dns_proxy_id"`
		Enable_mac_filtering bool   `json:"enable_mac_filtering"`
		HexId                string `json:"hex_id"`
		Id                   int    `json:"id"`
		Is_hub               bool   `json:"is_hub"`
		Latitude             string `json:"latitude"`
		Location             string `json:"location"`
		Longitude            string `josn:"longitude"`
		Name                 string `json:"name"`
		PlaceId              string `json:"place_id"`
		ProfileId            int    `json:"profile_id"`
		Snmp_mapping_enable  int    `json:"snmp_config_enable"`
		Snmp_mapping_id      string `json:"snmp_mapping_id"`
		Tags                 string `json:"tags"`
		UpdatedAt            string `json:"updated_at"`
		UrlFiltering         bool   `json:"url_filtering"`
		UrlProfileId         int    `json:"url_profile_id"`
		UserId               int    `json:"user_id"`
	} `json:"sites"`
	Total_count int `json:"total_count"`
}

func (cs *Cloudstation) Get_SiteList() ([]SITE, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	key := "get_site_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

	var site_ls []SITE
	var site SITE
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return site_ls, false
	}
	err_site := json.NewDecoder(resp.Body).Decode(&site)
	if err_site != nil {
		log.Err.Println(err_site)
	}
	if ok, json_data := convert_struct_to_string_via_json(site); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	site_ls = append(site_ls, site)

	tot_entry := site.Total_count
	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}

	i := 2
	for i <= tot_page {
		var site1 SITE
		queryParam = map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		//fmt.Println(resp)
		if resp.StatusCode != 200 {
			return site_ls, false
		}
		err_site = json.NewDecoder(resp.Body).Decode(&site1)
		if err_site != nil {
			log.Err.Println(err_site)
		}
	if ok, json_data := convert_struct_to_string_via_json(site1); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
		site_ls = append(site_ls, site1)
		i++
	}
	return site_ls, true
}

func (cs *Cloudstation) Get_SiteId(site_name string) (int, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	site_ls, err1 := cs.Get_SiteList()
	if err1 == false {
		log.Err.Println("Failed to fetch the SITE List !", err1)
		return 0, false
	}

	var site_id int
	var found_site_id bool
	for i := 0; i < len(site_ls); i++ {
		for j := 0; j < 10; j++ {
			if site_ls[i].Sites[j].Name == site_name {
				site_id = site_ls[i].Sites[j].Id
				found_site_id = true
				break
			}
		}
		if found_site_id == true {
			break
		}
	}
	if found_site_id == false {
		log.Err.Println("No Site Id found !")
		return 0, false
	}

	return site_id, true
}
/*
func (cs *Cloudstation) AttachAclPolicyAtSite(acl_name string, site_name string) bool {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	aclhex_id, err1 := cs.Get_AclHexId(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the Acl Hex Id !")
		return false
	}

	site_id, err2 := cs.Get_SiteId(site_name)
	if err2 == false {
		log.Err.Println("Failed to fetch the Site Id !")
		return false
	}

	var attach_site_payload struct {
		Attached_to_id   int    `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_site_payload.Attached_to_id = site_id
	attach_site_payload.Attached_to_type = "Site"

	key := "attach_at_site"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": aclhex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_site_payload, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}

func (cs *Cloudstation) DetachAclPolicyAtSite(acl_name string, site_name string) bool {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	acl_id, err1 := cs.Get_AclId(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the Acl Id !")
		return false
	}

	siteobject_id, err2 := cs.Get_SiteId(site_name)
	if err2 == false {
		log.Err.Println("Failed to fetch the Site Object Id !")
		return false
	}

	var detach_site_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_site_payload.Object_id = siteobject_id
	detach_site_payload.Object_type = "Site"

	key := "detach_at_site"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(acl_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_site_payload, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}
*/

// Attaching and Detaching the Policy at Site Profile Level

type PROFILE struct {
	Profiles []struct {
		Customer_id int    `json:"customer_id"`
		Hex_id      string `json:"hex_id"`
		Id          int    `json:"id"`
		Name        string `json:"name"`
		User_id     int    `json:"user_id"`
	} `json:"profiles"`
	Total_count int `json:"total_count"`
}

func (cs *Cloudstation) Get_ProfileList() ([]PROFILE, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	key := "get_profile_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

	var profile_ls []PROFILE
	var profile PROFILE
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return profile_ls, false
	}
	err_profile := json.NewDecoder(resp.Body).Decode(&profile)
	if err_profile != nil {
		log.Err.Println(err_profile)
	}
	if ok, json_data := convert_struct_to_string_via_json(profile); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	profile_ls = append(profile_ls, profile)

	tot_entry := profile.Total_count
	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}

	i := 2
	for i <= tot_page {
		var profile1 PROFILE
		queryParam = map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		//fmt.Println(resp)
		if resp.StatusCode != 200 {
			return profile_ls, false
		}
		err_profile = json.NewDecoder(resp.Body).Decode(&profile1)
		if err_profile != nil {
			log.Err.Println(err_profile)
		}
	if ok, json_data := convert_struct_to_string_via_json(profile1); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
		profile_ls = append(profile_ls, profile1)
		i++
	}
	return profile_ls, true
}

func (cs *Cloudstation) Get_ProfileId(profile_name string) (int, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	profile_ls, err1 := cs.Get_ProfileList()
	if err1 == false {
		log.Err.Println("Failed to fetch the SITE PROFILE List !", err1)
		return 0, false
	}

	var profile_id int
	var found_profile_id bool
	for i := 0; i < len(profile_ls); i++ {
		for j := 0; j < 10; j++ {
			if profile_ls[i].Profiles[j].Name == profile_name {
				profile_id = profile_ls[i].Profiles[j].Id
				found_profile_id = true
				break
			}
		}
		if found_profile_id == true {
			break
		}
	}
	if found_profile_id == false {
		log.Err.Println("No Profile Id found !")
		return 0, false
	}

	return profile_id, true
}

/*
func (cs *Cloudstation) AttachAclAtSiteProfile(acl_name string, profile_name string) bool {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	aclhex_id, err1 := cs.Get_AclHexId(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the Acl Hex Id !")
		return false
	}

	profile_id, err2 := cs.Get_ProfileId(profile_name)
	if err2 == false {
		log.Err.Println("Failed to fetch the Profile Id !")
		return false
	}

	var attach_profile_payload struct {
		Attached_to_id   int    `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_profile_payload.Attached_to_id = profile_id
	attach_profile_payload.Attached_to_type = "Profile"

	key := "attach_at_profile"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": aclhex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_profile_payload, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}
func (cs *Cloudstation) DetachAclAtSiteProfile(acl_name string, profile_name string) bool {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	acl_id, err1 := cs.Get_AclId(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the Acl Id !")
		return false
	}

	profileobject_id, err2 := cs.Get_ProfileId(profile_name)
	if err2 == false {
		log.Err.Println("Failed to fetch the Profile Object Id !")
		return false
	}

	var detach_profile_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_profile_payload.Object_id = profileobject_id
	detach_profile_payload.Object_type = "Profile"

	key := "detach_at_profile"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(acl_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_profile_payload, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}
*/
/*
type CreateLdapResponse struct{
	Msg string `json:"message"`
	Total_count int `json:"total_count"`
	Data struct{
		Ldap_server struct{
			Id int `json:"id"`
			Name string `json:"name"`
			Server_id string `json:"server_id"`
			Server_port int `json:"server_port"`
			Cmi string `json:"common_name_identifier"`
			Dist_name string `json:"distinguished_name"`
			Bind_type string `json:"bind_type"`
			En_sec_conn bool `json:"enable_secure_connection"`
			Cust_id int `json:"customer_id"`
			Creat_at string `json:"created_at"`
			Updated_at string `json:"updated_at"`
			Enable bool `json:"enable"`
			Admin_usrname string `json:"admin_username"`
			Admin_pw string `json:"admin_password"`
			Ldap_status string `json:"ldap_status"`
			Last_check string `json:"last_checked"`
		}`json:"ldap_server"`
	}`json:"data"`
}
func (cs *Cloudstation) CreateLdap(LdapObj LdapInput) (bool,*CreateLdapResponse){
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        var createldapresp CreateLdapResponse
        var CreateLdapStruct struct{
               Admin_pw string `json:"admin_password"`
                Admin_usrname string `json:"admin_username"`
                Bind_type string `json:"bind_type"`
                Cmi string `json:"common_name_identifier"`
                Dist_name string `json:"distinguished_name"`
                Enable bool `json:"enable"`
                En_sec_conn bool `json:"enable_secure_connection"`
                Ldap_name string `json:"name"`
                Server_ip string `json:"server_ip"`
                Server_port int `json:"server_port"`
        }
        CreateLdapStruct.Admin_pw = LdapObj.Admin_pw
        CreateLdapStruct.Admin_usrname = LdapObj.Admin_usrname
        CreateLdapStruct.Bind_type = LdapObj.Bind_type
        CreateLdapStruct.Cmi = LdapObj.Cmi
        CreateLdapStruct.Dist_name = LdapObj.Dist_name
        CreateLdapStruct.Enable = LdapObj.Enable
        CreateLdapStruct.En_sec_conn = LdapObj.En_sec_conn
        CreateLdapStruct.Ldap_name = LdapObj.Ldap_name
        CreateLdapStruct.Server_ip = LdapObj.Server_ip
        CreateLdapStruct.Server_port = LdapObj.Server_port

    key := "create_ldap"
    apis := getApis()
    api :=apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{})

    header := cs.Construct_headers(apis[key].headers.headers)
    //fmt.Println(header)
    var resp *http.Response
    cookie := []*http.Cookie{cs.Cookies}
    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, CreateLdapStruct, cookie)

    if !cs.Status_CS(resp.StatusCode) {
                return false,&createldapresp
        }

        if resp.StatusCode == 401 {
                resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, CreateLdapStruct, cookie)
        }

        if resp.StatusCode != 200 {
                return false,&createldapresp
        }

    err_createldapresp:= json.NewDecoder(resp.Body).Decode(&createldapresp)
    if err_createldapresp != nil {
               log.Err.Println(err_createldapresp)
         }
	if ok, json_data := convert_struct_to_string_via_json(createldapresp); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
        return true,&createldapresp
}

type LdapInfo struct {
                        Id int `json:"id"`
                        Name string `json:"name"`
                        Server_ip string `json:"server_ip"`
                        Server_port int `json:"server_port"`
                        Cmi string `json:"common_name_identifier"`
			Dist_name string `json:"distinguished_name"`
                        Bind_type string `json:"bind_type"`
                        En_sec_conn bool `json:"enable_secure_connection"`
                        Cust_id int `json:"customer_id"`
                        Creat_at string `json:"created_at"`
                        Updated_at string `json:"updated_at"`
                        Enable bool `json:"enable"`
                        Admin_usrname string `json:"admin_username"`
                        Admin_pw string `json:"admin_password"`
                        Ldap_status string `json:"ldap_status"`
                        Last_check string `json:"last_checked"`

}


type GetLdapListResponse struct {
        Msg string `json:"message"`
        Total_count int `json:"total_count"`
        Data struct{
                Ldap_servers []LdapInfo `json:"ldap_servers"`
        }`json:"data"`
}
func (cs *Cloudstation) GetLdapList() (bool, error, map[int]LdapInfo, map[string]LdapInfo) {

//        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        //log.Debug.Printf("FUNC: %s exit\n", tc_utils.FuncName())
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        var dummy struct {
        }
        key := "get_ldap_list"
        apis := getApis()
         api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{})
        header := cs.Construct_headers(apis[key].headers.headers)
        //fmt.Println(header)
	var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        queryParam := map[string]interface{}{
                "key":      "",
                "ldap_server_type":  1,
                "page":     10,
                "per_page": "",
        }
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
        //resp, _ = http.Post(fmt.Sprintf("%s/%s", cs.Base_url, substitutedPath),  )
        //fmt.Println(err)
        //var LdapList []LdapInfoList
        //var SingleLdap LdapInfoList

        var get_ldap_list_response GetLdapListResponse
        ldap_info := make(map[int]LdapInfo)
        ldap_info_wrt_name := make(map[string]LdapInfo)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false, nil, ldap_info, ldap_info_wrt_name
        }

        err_ldap := json.NewDecoder(resp.Body).Decode(&get_ldap_list_response)
        if err_ldap != nil {
                log.Err.Println(err_ldap)
        }
	if ok, json_data := convert_struct_to_string_via_json(get_ldap_list_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
        tot_entry := get_ldap_list_response.Total_count
       // log.Debug.Printf("    Total Site Count: %d\n", tot_entry)
        //SiteList = append(LdapList, LdapSite)
//      fmt.Println(tot_entry)
//      fmt.Println(get_ldap_list_response)
        for _, ldap := range get_ldap_list_response.Data.Ldap_servers {
                //fmt.Println(ldap.Name)
		 ldap_info_wrt_name[ldap.Name] = ldap
            ldap_info[ldap.Id] = ldap
        }
        //fmt.Println(LdapList)
        tot_page := 0
        if tot_entry%10 == 0 {
                tot_page = tot_entry / 10
        } else {
                tot_page = (tot_entry / 10) + 1
        }
        i:=2
        for i <= tot_page {
                //var SingleLdap1 LdapInfoList
                queryParam = map[string]interface{}{
                        "key":      "",
                        "ldap_server_type":  1,
                        "page":     i,
                        "per_page": "",
                }

                resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
                if resp.StatusCode != 200 {

                //      return ldap_info, false
                return false, nil, ldap_info, ldap_info_wrt_name
                }
                err_ldap = json.NewDecoder(resp.Body).Decode(&get_ldap_list_response)
                if err_ldap != nil {
                        log.Err.Println(err_ldap)
                }
	if ok, json_data := convert_struct_to_string_via_json(get_ldap_list_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
                //fmt.Println(SingleLdap)
                //SiteList = append(LdapList, SingleLdap1)
                for _, ldap := range get_ldap_list_response.Data.Ldap_servers {
                         ldap_info[ldap.Id] = ldap
			 ldap_info_wrt_name[ldap.Name] = ldap
                }
                //fmt.Println(LdapList)
                i++
        }

        //fmt.Println(LdapList)
        //fmt.Println("--------------Got into the Verse for Getting the LDAP List !--------------")
        //return ldap_info, true

        //fmt.Println(ldap_info)
        //fmt.Println(ldap_info_wrt_name)
        return true, nil, ldap_info, ldap_info_wrt_name
}

func (cs *Cloudstation) DeleteLdapConfig(ldap_name string) (bool,*CreateLdapResponse) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        var delresp CreateLdapResponse
	//var ldap_info LdapInfo
	var ldap_id int
        err1, _,_,res2 := cs.GetLdapList()
        if err1 == false {
                log.Err.Println("Failed to fetch the Ldap Id !")
                return false,&delresp
        }

	if ldap_info, ok := res2[ldap_name]; ok {
		ldap_id = ldap_info.Id
	} else {
                return false,&delresp
	}
        //ldap_res:=res2[ldap_name]
        //ldap_id:=ldap_res.Id

        var deleteldap_payload struct {
        }

        key := "delete_ldap"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom1_id": strconv.Itoa(ldap_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, deleteldap_payload, cookie)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false,&delresp
        }

        err_delresp:= json.NewDecoder(resp.Body).Decode(&delresp)
        if err_delresp != nil {
                fmt.Println(err_delresp)
         }
	if ok, json_data := convert_struct_to_string_via_json(delresp); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
        return true,&delresp
}*/

type Inherited struct{
       Id int `json:"id"`
       Loc string `json:"location"`
       Latitude string `json:"latitude"`
       Longitude string `json:"longitude"`
       Place_id string `json:"place_id"`
       Name string `json:"name"`
       Hex_id string `json:"hex_id"`
       Auto_upgrade bool `json:"auto_upgrade"`
       Is_hub bool `json:"is_hub"`
       Url_filter bool `json:"url_filtering"`
       Url_prof_id int `json:"url_profile_id"`
       Cloudprt_url_conf string `json:"cloudport_url_config"`
       Intnt_withot_nat string `json:"internet_without_nat"`
       User_id int `json:"user_id"`
       Cust_id int `json:"customer_id"`
       Create_at string `json:"created_at"`
       Update_at string `json:"updated_at"`
       Profile_id int `json:"profile_id"`
       Tags string `json:"tags"`
       Dns_proxy_id int `json:"dns_proxy_id"`
       Dns_proxy_enable bool `json:"dns_proxy_enable"`
       Snmp_map_id string `json:"snmp_mapping_id"`
       Snmp_conf_enable int `json:"snmp_config_enable"`
       Enable_mac_filter string `json:"enable_mac_filter"`
}
type GetAffectingPolicyListResponse struct {
       Acl_Policies []struct{
             Rule struct{
               Id int `json:"id"`
               Name string `json:"name"`
               Acl_action string `json:"acl_action"`
               Create_at string `json:"created_at"`
               Update_at string `json:"updated_at"`
               Priority int `json:"priority"`
               Src_net_grp_id int `json:"scr_network_group_id"`
               Src_net_group_name string `json:"src_network_group_name"`
               Cust_id int `json:"customer_id"`
               User_id int `json:"user_id"`
               Hex_id string `json:"hex_id"`
               Direction string `json:"direction"`
               Tag string `json:"tag"`
               Dst_net_grp_id int `json:"dst_network_group_id"`
               Dst_net_grp_name string `json:"dst_network_group_name"`
               Conn_state string `json:"conn_state"`
       }`json:"rule"`
               Inherit_lvl string `json:"inheritance_level"`
               Inherit_frm Inherited `json:"inherited_from"`
       }`json:"acl_policies"`
       Qos_Policies []struct{
               Rule struct{
               Id int `json:"id"`
               Name string `json:"name"`
               Qos_action string `json:"qos_action"`
               Create_at string `json:"created_at"`
               Update_at string `json:"updated_at"`
               Priority int `json:"priority"`
               Src_net_grp_id int `json:"scr_network_group_id"`
               Src_net_group_name string `json:"src_network_group_name"`
               Cust_id int `json:"customer_id"`
               User_id int `json:"user_id"`
               Hex_id string `json:"hex_id"`
               Direction string `json:"direction"`
               Tag string `json:"tag"`
               Dst_net_grp_id int `json:"dst_network_group_id"`
               Dst_net_grp_name string `json:"dst_network_group_name"`
               Dscp_code int `json:"dscp_code"`
               Dscp_remake bool `json:"dscp_remarking"`
       }`json:"rule"`
               Inherit_lvl string `json:"inheritance_level"`
               Inherit_frm Inherited `json:"inherited_from"`
               Traf_rate_lim []string `json:"traffic_rate_limiters"`
               Lb_policy []string `json:"lb_policies"`
               Nat_policy []string `json:"nat_policies"`
       }`json:"qos_policies"`
       Security_Policies []struct{
             Rule struct{
              Id int `json:"id"`
               Hex_id string `json:"hex_id"`
               Name string `json:"name"`
               Priority int `json:"priority"`
               Tag string `json:"tag"`
               Dst_net_grp_id int `json:"dst_network_group_id"`
               Src_net_grp_id int `json:"src_network_group_id"`
               Direction string `json:"direction"`
               Security_prof_id int `json:"security_profile_id"`
               Create_at string `json:"created_at"`
               Update_at string `json:"updated_at"`
               User_id int `json:"user_id"`
               Cust_id int `json:"customer_id"`
       }`json:"rule"`
 Inherit_lvl string `json:"inheritance_level"`

               Inher_frm struct{
                       Id int `json:"id"`
                       Name string `json:"name"`
                       Loc string `json:"location"`
                       Vpn string `json:"vpn"`
                       Create_at string `json:"created_at"`
                       Update_at string `json:"updated_at"`
                       Tag string `json:"tag"`
                       Encap string `json:"encapsulation"`
                       Nxt_hop string `json:"next_hop"`
                       Ib_enable string `json:"ib_enable"`
                       Vpn_prof_id string `json:"vpn_profile_id"`
                       Secure_data string `json:"secured_data"`
                       User_id int `json:"user_id"`
                       Cust_id int `json:"customer_id"`
                       Hex_id string `json:"hex_id"`
                       N_id int `json:"n_id"`
       Security_prof_id int `json:"security_profile_id"`
               }`json:"inherited_from"`
       }`json:"security_policies"`

       TrafficRateLimiters []struct {
	       Inherit_lvl string `json:"inheritance_level"`
	       Inherit_frm struct {
		       Id int `json:"id"`
		       Name string `json:"name"`
	       } `json:"inherited_from"`
	       Rule struct {
                  Id int `json:"id"`
                  Name string `json:"name"`
	       } `json:"rule"`
       } `json:"traffic_rate_limiters"`

       WanPolicies []struct {
	       Inherit_lvl string `json:"inheritance_level"`
	       Inherit_frm struct {
		       Id int `json:"id"`
		       Name string `json:"name"`
	       } `json:"inherited_from"`
	       Rule struct {
                  Id int `json:"id"`
                  Name string `json:"name"`
	       } `json:"rule"`
       } `json:"wan_policies"`
}

func (cs *Cloudstation) GetAffectingPolicyList(site_name string) (bool, *GetAffectingPolicyListResponse) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
       var affpolicy_ls GetAffectingPolicyListResponse
       var uuid string
       if device_info, ok:=  cs.Helper.device.bySiteName[site_name]; ok {
	       uuid = device_info[0].S_Number
	       //for _, per_device_info :=  range device_info {
	//	     uuid =   per_device_info.S_Number
	  //     }
       } else {
	       return false, &affpolicy_ls
       }
//	uuid,err1:= cs.Get_Uuid(Uuid_name)
       var dummy struct {
       }
       key := "get_affecting_policy_list"
       apis := getApis()
       api := apis[key]
       //substitutedPath := substitutePath(apis[key], map[string]string{"custom_uuid": strconv.Itoa(uuid)})
       substitutedPath := substitutePath(apis[key], map[string]string{"custom_uuid":  uuid})
       header := cs.Construct_headers(apis[key].headers.headers)
       var resp *http.Response
       cookie := []*http.Cookie{cs.Cookies}
       resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
       if resp.StatusCode != 200 {
               return false, &affpolicy_ls
       }
       err_affpolicy := json.NewDecoder(resp.Body).Decode(&affpolicy_ls)
       if err_affpolicy != nil {
               log.Err.Println(err_affpolicy)
       }
	if ok, json_data := convert_struct_to_string_via_json(affpolicy_ls); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
       return true, &affpolicy_ls
}


type CPELinkMapping struct {
    BondPorts []struct {
        ChildPort []struct {
            Name string `json:"name"`
        } `json:"child_ports"`
        Interfaces []struct {
            LogicalPortName string `json:"name"`
        } `json:"interfaces"`
    } `json:"bond_ports"`
    CloudportLinks []struct {
        //CloudportId      int    `json:"cloudport_id"`
        PhysicalPortName string `json:"name"`
        //PhysicalPortId   int    `json:"id"`
        Interfaces []struct {
            //LogicalPortId   int    `json:"id"`
            LogicalPortName string `json:"name"`
        } `json:"interfaces"`
        PortType string `json:"port_type"`
    } `json:"cloudport_links"`
}


func (cs *Cloudstation) GetCpeLinkMap(CPEName string) (map[string][]string, bool) {
    var dummy struct {
    }

    var cpeLinkmap CPELinkMapping
    LogicalToPhysicalPort := make(map[string][]string)
    isList, _, _, namemap, _, _ := cs.GetDeviceList()
    if !isList {
        log.Debug.Printf("Failed to fetch device list")
        return LogicalToPhysicalPort, false
    }
    devid := namemap[CPEName].Id
    //fmt.Println(devid)
    key := "get_cpelinks_mapping"
    apis := getApis()
    api := apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{"device_id": fmt.Sprintf("%d", devid)})

    header := cs.Construct_headers(apis[key].headers.headers)
    var resp *http.Response
    cookie := []*http.Cookie{cs.Cookies}
    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
    //fmt.Println(resp)
    //fmt.Println("Status = ", resp.StatusCode)
    if resp.StatusCode != 200 {
        return LogicalToPhysicalPort, false
    }
    err_info := json.NewDecoder(resp.Body).Decode(&cpeLinkmap)
    if err_info != nil {
        fmt.Println(err_info)
    }
    //fmt.Println(cpeLinkmap, "*******************")

    for i := 0; i < len(cpeLinkmap.BondPorts); i++ {
        for j := 0; j < len(cpeLinkmap.BondPorts[i].ChildPort); j++ {
            LogicalToPhysicalPort[cpeLinkmap.BondPorts[i].Interfaces[0].LogicalPortName] = append(LogicalToPhysicalPort[cpeLinkmap.BondPorts[i].Interfaces[0].LogicalPortName], cpeLinkmap.BondPorts[i].ChildPort[j].Name)
        }
    }
    for i := 0; i < len(cpeLinkmap.CloudportLinks); i++ {
        if cpeLinkmap.CloudportLinks[i].PortType == "bondport" {
            continue
        }
        for j := 0; j < len(cpeLinkmap.CloudportLinks[i].Interfaces); j++ {
            LogicalToPhysicalPort[cpeLinkmap.CloudportLinks[i].Interfaces[j].LogicalPortName] = append(LogicalToPhysicalPort[cpeLinkmap.CloudportLinks[i].Interfaces[j].LogicalPortName], cpeLinkmap.CloudportLinks[i].PhysicalPortName)
        }
    }
    return LogicalToPhysicalPort, true
}

