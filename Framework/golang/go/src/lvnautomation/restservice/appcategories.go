package restservice

import (
	"fmt"
	"net/http"
	"encoding/json"
	log "lvnautomation/lvnlog"
	tc_utils "lvnautomation/testcaseutils"

)

type APPCAT struct {
	Application_categories []struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
	} `json:"application_categories"`
}

func (cs *Cloudstation) Get_AppCategoriesList() (APPCAT, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var appcat_ls APPCAT
	var dummy struct {
	}

	key := "get_appcategories_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	//for k, v := range header {
	//		fmt.Println(k, v)
	//	}
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	//fmt.Println("Status Get_UnAuth= ", resp.StatusCode)
	//fmt.Println(resp)

	if !cs.Status_CS(resp.StatusCode) {
		return appcat_ls, false
	}

	if resp.StatusCode == 401 {
		//for k, v := range header {
		//	fmt.Println(k, v)
		//}
		//cookie2 := []*http.Cookie{cs.Cookies}
		header := cs.Construct_headers(apis[key].headers.headers)
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
		//fmt.Println(resp.StatusCode)
		//fmt.Println(resp)
	}

	if resp.StatusCode != 200 {
		return appcat_ls, false
	}
	err_appcat := json.NewDecoder(resp.Body).Decode(&appcat_ls)
	if err_appcat != nil {
		fmt.Println(err_appcat)
	}
	if ok, json_data := convert_struct_to_string_via_json(appcat_ls); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	return appcat_ls, true
}

func (cs *Cloudstation) Get_AppCategoriesId(appcat_name string) (int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	appcat_ls, err1 := cs.Get_AppCategoriesList()
	if err1 == false {
		fmt.Println("Failed to fetch the APP List !", err1)
		return 0, false
	}

	var appcat_id int
	var found_appcat_id bool
	length := len(appcat_ls.Application_categories)
	for i := 0; i < length; i++ {
		if appcat_ls.Application_categories[i].Name == appcat_name {
			appcat_id = appcat_ls.Application_categories[i].Id
			found_appcat_id = true
			break
		}
	}
	if found_appcat_id == false {
		log.Err.Printf("No AppCategory found !")
		return 0, false
	}
	return appcat_id, true
}
