package restservice

import (
	"fmt"
	"net/http"
	"errors"
	"encoding/json"
	log "lvnautomation/lvnlog"
	tc_utils "lvnautomation/testcaseutils"
)

type VpnProfile struct {
    AntiReplay bool `json:"anti_replay"`
    AuInterval *int `json:"auinterval"`
    AuthType string `json:"auth_type"`
    CreatedAt string `json:"created_at"`
    CustomerId int `json:"customer_id"`
    DataIntegrity string `json:"data_integrity"`
    Description *string `json:"description"`
    EncryptionMode string `json:"encryption_mode"`
    EncryptionStandard string `json:"encryption_standard"`
    HexId string `json:"hex_id"`
    Id int `json:"id"`
    Name string `json:"name"`
    Refresh string `json:"refresh"`
    RefreshInterval int `json:"refresh_interval"`
    Tag *string `json:"tag"`
    UpdatedAt string `json:"updated_at"`
    UserId int `json:"user_id"`
    Windowsize string `json:"window_size"`
}

type GetVpnProfilesResponse struct {
    Total_count int `json:"total_count"`
    VpnProfiles []VpnProfile `json:"vpn_profiles"`
}

func (cs *Cloudstation) GetVpnProfiles() (bool, error, map[string]VpnProfile) {
    if len(cs.Helper.profiles.vpn.byName) == 0 {
	    return cs.ForceGetVpnProfiles()
    }
    return true, nil, cs.Helper.profiles.vpn.byName
}

func (cs *Cloudstation) ForceGetVpnProfiles() (bool, error, map[string]VpnProfile) {
	//pi/v1/lavelle/security_profiles?key=&page=1&per_page=10&sort=

        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        var dummy struct {
        }
	var get_vpn_profiles_response GetVpnProfilesResponse
	//security_profiles := []VpnProfile{}
	vpn_profiles_wrt_name := make(map[string]VpnProfile)

        key := "get_vpn_profiles_list"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{})

	page_no := 1
	retreivals_per_page := 100
	max_pages_to_avoid_loop := 10

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        queryParam := map[string]interface{}{
                "key":      "",
                "page":     page_no,
                "per_page": retreivals_per_page,
                "sort":     "",
                "user": map[string]interface{}{
                        "username":             cs.Username,
                        "id":                   cs.Id,
                        "role":                 cs.Role,
                        "allow_manage":         cs.AllowManage,
                        "customer_id":          cs.CustomerId,
                        "hex_id":               cs.HexId,
                        "customer_hex_id":      cs.CustomerHexId,
                        "enable_notifications": cs.EnableNotifications,
                },
        }

	for {

            queryParam["page"] = page_no

            resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
            if resp.StatusCode != 200 {
                    return false, errors.New(fmt.Sprintf("Response code %d", resp.StatusCode)), vpn_profiles_wrt_name
            }
            err_nwgrp := json.NewDecoder(resp.Body).Decode(&get_vpn_profiles_response)
            if err_nwgrp != nil {
                fmt.Println(err_nwgrp)
            }
            if ok, json_data := convert_struct_to_string_via_json(get_vpn_profiles_response); ok {
                log.Debug.Printf("response data: %s\n", json_data)
            }

            total_profiles := get_vpn_profiles_response.Total_count
	    //vpn_profiles = append(vpn_profiles, get_vpn_profiles_response.VpnProfiles...)

            for _, vpn_profile := range get_vpn_profiles_response.VpnProfiles {
                vpn_profiles_wrt_name[vpn_profile.Name] = vpn_profile
            }

	    cs.Helper.profiles.vpn.byName = vpn_profiles_wrt_name

	    if len(vpn_profiles_wrt_name) == total_profiles {
		    break
	    }

	    if page_no == max_pages_to_avoid_loop {
		    break
	    }

	    page_no += 1
	}

        return true, nil, vpn_profiles_wrt_name
}

func (cs *Cloudstation) GetVpnProfileId(profile_name string) (bool, int) {

    if res, _, vpn_profiles_wrt_name := cs.GetVpnProfiles(); res {
        if profile, ok := vpn_profiles_wrt_name[profile_name]; ok {
		return true, profile.Id
        }
    }
    return false, 0
}
