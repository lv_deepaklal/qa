package restservice

import (
	"fmt"
	"net/http"
	//"errors"
	"encoding/json"
	//"regexp"
	"strconv"
	//"strings"
	//"bytes"
	//"compress/gzip"
	//"io"

	log "lvnautomation/lvnlog"
	//"time"
	tc_utils "lvnautomation/testcaseutils"
)

/*
	QOS POLICY
*/

type QosInfo struct {
	CreatedAt              string `json:"created_at"`
	Customer_id            int    `json:"customer_id"`
	Direction              string `json:"direction"`
	Dscp_code              int    `json:"dscp_code"`
	Dscp_remarking         bool   `json:"dscp_remarking"`
	Dst_network_group_id   int    `json:"dst_network_group_id"`
	Dst_network_group_name string `json:"dst_network_group_name"`
	Hex_id                 string `json:"hex_id"`
	Id                     int    `json:"id"`
	Name                   string `json:"name"`
	Priority               int    `json:"priority"`
	Src_network_group_id   int    `json:"src_network_group_id"`
	Src_network_group_name string `json:"src_network_group_name"`
	Tag                    string `json:"tag"`
	UpdatedAt              string `json:"updated_at"`
	User_id                int    `json:"user_id"`
}

type CreateQosResponse struct {
	Data struct {
            Qos QosInfo `json:"qos"`
	} `json:"data"`
	Message string `json:"message"`
}

type QOSPOLICY struct {
	Qos         []QosInfo `json:"qos"`
	Total_count int       `json:"total_count"`
}

func (cs *Cloudstation) Get_QosPolicyList() ([]QOSPOLICY, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	key := "get_qospolicy_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

	var qospol_ls []QOSPOLICY
	var qospol QOSPOLICY
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	fmt.Println("Status Get_QosPolicyList of page 1= ", resp.StatusCode)
	if resp.StatusCode != 200 {
		return qospol_ls, false
	}
	err_qospolicy := json.NewDecoder(resp.Body).Decode(&qospol)
	if err_qospolicy != nil {
		fmt.Println(err_qospolicy)
	}
	qospol_ls = append(qospol_ls, qospol)

	tot_entry := qospol.Total_count
	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}

	i := 2
	for i <= tot_page {
		var qospol1 QOSPOLICY
		queryParam = map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		fmt.Printf("Status Get_QosPolicyList of page %d= %d\n", i, resp.StatusCode)
		if resp.StatusCode != 200 {
			return qospol_ls, false
		}
		err_qospolicy := json.NewDecoder(resp.Body).Decode(&qospol1)
		if err_qospolicy != nil {
			fmt.Println(err_qospolicy)
		}
		qospol_ls = append(qospol_ls, qospol1)
		i++
	}
	//fmt.Println("--------------Got into the Verse for Getting the QOS POLICY LIST !--------------")
	//fmt.Println()
	return qospol_ls, true
}

func (cs *Cloudstation) Get_QosHexId(qos_name string, qospol_ls []QOSPOLICY) (string, bool) {
	var hex_id string
	var found_hex_id bool
	for i := 0; i < len(qospol_ls); i++ {
		for j := 0; j < 10; j++ {
			if qospol_ls[i].Qos[j].Name == qos_name {
				hex_id = qospol_ls[i].Qos[j].Hex_id
				found_hex_id = true
				break
			}
		}
		if found_hex_id == true {
			break
		}
	}
	if found_hex_id == false {
		fmt.Println("No Hex Id found !")
		return "", false
	}
	return hex_id, true
}

func (cs *Cloudstation) Get_QosId(qos_name string, qospol_ls []QOSPOLICY) (int, bool) {
	var id int
	var found_id bool
	fmt.Println("qosname", qos_name)
	for i := 0; i < len(qospol_ls); i++ {
		for j := 0; j < len(qospol_ls[i].Qos); j++ {
			if qospol_ls[i].Qos[j].Name == qos_name {
				id = qospol_ls[i].Qos[j].Id
				found_id = true
				break
			}
		}
		if found_id == true {
			break
		}
	}
	if found_id == false {
		fmt.Println("No Id found !")
		return 0, false
	}
	return id, true
}

// For QOS POLICY attach and detach at network group

func (cs *Cloudstation) AttachQosAtNwGrp(qos_name string, nwgrp_name string) bool {
	x, _ := cs.Get_QosPolicyList()

	qoshex_id, err1 := cs.Get_QosHexId(qos_name, x)
	if err1 == false {
		fmt.Println("Failed to fetch the Qos Hex Id !")
		return false
	}

	nwgrphex_id, err2 := cs.Get_NwGrpHexId(nwgrp_name)
	if err2 == false {
		fmt.Println("Failed to fetch the NwGrp Hex Id !")
		return false
	}

	var attach_nwgrp_payload struct {
		Attached_to_id   string `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_nwgrp_payload.Attached_to_id = nwgrphex_id
	attach_nwgrp_payload.Attached_to_type = "NetworkGroup"

	key := "attach_qos_at_networkgrp"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": qoshex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_nwgrp_payload, cookie)
	fmt.Println("Status AttachQosAtNwGrp= ", resp.StatusCode)
	fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for ATTACHING QOS AT NETWORK_GROUP LEVEL-------------")
	//fmt.Println()
	return true
}

func (cs *Cloudstation) DetachQosAtNwGrp(qos_name string, nwgrp_name string) bool {
	x, _ := cs.Get_QosPolicyList()

	qos_id, err1 := cs.Get_QosId(qos_name, x)
	if err1 == false {
		fmt.Println("Failed to fetch the Qos Id !")
		return false
	}

	nwobject_id, err2 := cs.Get_NwGrpId(nwgrp_name)
	if err2 == false {
		fmt.Println("Failed to fetch the NwGrp Object Id !")
		return false
	}

	var detach_nwgrp_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_nwgrp_payload.Object_id = nwobject_id
	detach_nwgrp_payload.Object_type = "NetworkGroup"

	key := "detach_qos_at_networkgrp"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(qos_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_nwgrp_payload, cookie)
	fmt.Println("Status DetachQosAtNwGrp= ", resp.StatusCode)
	fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for DETACHING QOS AT NETWORK_GROUP LEVEL-------------")
	//fmt.Println()
	return true
}

// For QOS POLICY attach and detach at site

func (cs *Cloudstation) AttachQosAtSite(qos_name string, site_name string) bool {
	x, _ := cs.Get_QosPolicyList()

	qoshex_id, err1 := cs.Get_QosHexId(qos_name, x)
	if err1 == false {
		fmt.Println("Failed to fetch the Qos Hex Id !")
		return false
	}

	site_id, err2 := cs.Get_SiteId(site_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Site Id !")
		return false
	}

	var attach_site_payload struct {
		Attached_to_id   int    `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_site_payload.Attached_to_id = site_id
	attach_site_payload.Attached_to_type = "Site"

	key := "attach_qos_at_site"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": qoshex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_site_payload, cookie)
	fmt.Println("Status AttachQosAtSite= ", resp.StatusCode)
	fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for ATTACHING QOS AT SITE LEVEL-------------")
	//fmt.Println()
	return true
}

func (cs *Cloudstation) DetachQosAtSite(qos_name string, site_name string) bool {
	x, _ := cs.Get_QosPolicyList()
	fmt.Println("length of qospolciy list ", len(x))

	qos_id, err1 := cs.Get_QosId(qos_name, x)
	fmt.Println(qos_id)

	if err1 == false {
		fmt.Println("Failed to fetch the Qos Id !")
		return false
	}

	siteobject_id, err2 := cs.Get_SiteId(site_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Site Object Id !")
		return false
	}

	var detach_site_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_site_payload.Object_id = siteobject_id
	detach_site_payload.Object_type = "Site"

	key := "detach_qos_at_site"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(qos_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_site_payload, cookie)
	fmt.Println("Status DetachQosAtSite= ", resp.StatusCode)
	fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for DETACHING QOS AT SITE LEVEL-------------")
	//fmt.Println()
	return true
}

// For QOS POLICY attach and detach at site profile

func (cs *Cloudstation) AttachQosAtSiteProfile(qos_name string, profile_name string) bool {
	x, _ := cs.Get_QosPolicyList()

	qoshex_id, err1 := cs.Get_QosHexId(qos_name, x)
	if err1 == false {
		fmt.Println("Failed to fetch the Qos Hex Id !")
		return false
	}

	profile_id, err2 := cs.Get_ProfileId(profile_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Profile Id !")
		return false
	}

	var attach_profile_payload struct {
		Attached_to_id   int    `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_profile_payload.Attached_to_id = profile_id
	attach_profile_payload.Attached_to_type = "Profile"

	key := "attach_qos_at_profile"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": qoshex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_profile_payload, cookie)
	fmt.Println("Status AttachQosAtSiteProfile= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for ATTACHING QOS AT SITE PROFILE LEVEL-------------")
	//fmt.Println()
	return true
}

func (cs *Cloudstation) DetachQosAtSiteProfile(qos_name string, profile_name string) bool {
	x, _ := cs.Get_QosPolicyList()

	qos_id, err1 := cs.Get_QosId(qos_name, x)
	if err1 == false {
		fmt.Println("Failed to fetch the Qos Id !")
		return false
	}

	profileobject_id, err2 := cs.Get_ProfileId(profile_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Profile Object Id !")
		return false
	}

	var detach_profile_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_profile_payload.Object_id = profileobject_id
	detach_profile_payload.Object_type = "Profile"

	key := "detach_qos_at_profile"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(qos_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_profile_payload, cookie)
	fmt.Println("Status DetachQosAtSiteProfile= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for DETACHING QOS AT SITE PROFILE LEVEL-------------")
	//fmt.Println()
	return true
}
