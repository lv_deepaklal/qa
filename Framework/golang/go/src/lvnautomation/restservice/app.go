package restservice

import (
	"fmt"
	"net/http"
	"encoding/json"
	"strconv"
	log "lvnautomation/lvnlog"
	tc_utils "lvnautomation/testcaseutils"
)

type Apps struct {
    Id   int    `json:"id"`
    Name string `json:"name"`
}

type APPs struct {
	Data struct {
		Applications []Apps `json:"applications"`
	} `json:"data"`
	Message     string `json:"message"`
	Reason      string `json:"reason"`
	Total_Count int    `json:"total_count"`
}


type AppInfo struct {
	App_id      int     `json:"app_id"`
	AppCatId    *int    `json:"application_category_id"`
	CreatedAt   string  `json:"created_at"`
	Customer_id int     `json:"customer_id"`
	Default     bool    `json:"default"`
	Dscp        *string `json:"dscp"`
	Dst_ip      *string `json:"dst_ip"`
	Dst_port    *string `json:"dst_port"`
	Hex_id      string  `json:"hex_id"`
	Hostname    *string `json:"hostname"`
	Id          int     `json:"id"`
	Ip          *string `json:"ip"`
	Match_type  *string `json:"match_type"`
	Name        string  `json:"name"`
	Port        *string `json:"port"`
	Protocol    *string `json:"protocol"`
	Signature   *string `json:"signature"`
	Src_ip      *string `json:"src_ip"`
	Src_port    *string `json:"src_port"`
	Tag         *string `json:"tag"`
	Tcp_port    *string `json:"tcp_port"`
	Udp_port    *string `json:"udp_port"`
	UpdatedAt   string  `json:"updated_at"`
	User_id     int     `json:"user_id"`
}

type CreateCustomAppResponse struct {
	Data struct {
            Applications AppInfo `json:"application"`
	} `json:"data"`
	Message string `json:"message"`

	/*
	//AppInfo 

	App_id      int     `json:"app_id"`
	Customer_id int     `json:"customer_id"`
	//Dscp        *string `json:"dscp"`
	//Dst_ip      *string `json:"dst_ip"`
	//Dst_port    *string `json:"dst_port"`
	Hex_id      string  `json:"hex_id"`
	//Hostname    *string `json:"hostname"`
	Id          int     `json:"id"`
	//Ip          *string `json:"ip"`
	//Match_type  *string `json:"match_type"`
	Name        string  `json:"name"`
	//Port        *string `json:"port"`
	//Protocol    *string `json:"protocol"`
	//Signature   *string `json:"signature"`
	//Src_ip      *string `json:"src_ip"`
	//Src_port    *string `json:"src_port"`
	//Tag         *string `json:"tag"`
	//Tcp_port    *string `json:"tcp_port"`
	//Udp_port    *string `json:"udp_port"`
	User_id     int     `json:"user_id"`
	//AppCatId int `json:"application_category_id"`
	//CreatedAt string `json:"created_at"`
	//Default bool `json:"default"`
	//UpdatedAt string `json:"updated_at"`
	*/

}

func (cs *Cloudstation) CreateCustomApp(customAppInput *CreateCustomAppInputs) (bool, error, *CreateCustomAppResponse){

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var create_custom_app_response CreateCustomAppResponse
	appcat_id, err1 := cs.Get_AppCategoriesId(customAppInput.AppCategory)
	if err1 == false {
		log.Err.Printf("Failed to fetch the AppCategory ID !", err1)
		return false, nil, &create_custom_app_response
	}

	var customapp_payload struct {
		Name      string `json:"name"`
		AppCatId  int    `json:"application_category_id"`
		Ip        string `json:"ip"`
		Port      string `json:"port"`
		MatchType string `json:"match_type"`
		SrcIp     string `json:"src_ip"`
		SrcPort   string `json:"src_port"`
		DstIp     string `json:"dst_ip"`
		DstPort   string `json:"dst_port"`
		Hostname  string `json:"hostname"`
		Protocol  string `json:"protocol"`
		Sign      string `json:"signature"`
		Tag       string `json:"tag"`
	}

	//fmt.Printf("=====customAppInput.IdentificationType===%s\n",customAppInput.IdentificationType)
	customapp_payload.Name = customAppInput.Name
	customapp_payload.AppCatId = appcat_id
	customapp_payload.MatchType = customAppInput.IdentificationType
	customapp_payload.Ip = customAppInput.ip
	customapp_payload.Port = customAppInput.port
	customapp_payload.SrcIp = customAppInput.sourceIp
	customapp_payload.SrcPort = customAppInput.sourcePort
	customapp_payload.DstIp = customAppInput.destinationIp
	customapp_payload.DstPort = customAppInput.destinationPort
	customapp_payload.Hostname = customAppInput.Hostname
	customapp_payload.Protocol = customAppInput.Protocol
	customapp_payload.Sign = customAppInput.Signature
	customapp_payload.Tag = customAppInput.Tag


	key := "conf_customapp"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
	header := cs.Construct_headers(apis[key].headers.headers)

	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, customapp_payload, cookie)
	//fmt.Println("Status CreateCustomApp= ", resp.StatusCode)
	//fmt.Println(resp)

	if !cs.Status_CS(resp.StatusCode) {
		return false, nil, &create_custom_app_response
	}

	if resp.StatusCode == 401 {
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, customapp_payload, cookie)
	}

	if resp.StatusCode != 200 {
		return false, nil, &create_custom_app_response
	}

	err_app := json.NewDecoder(resp.Body).Decode(&create_custom_app_response)
	if err_app != nil {
		fmt.Println(err_app)
	}
	return true, nil, &create_custom_app_response
}

func (cs *Cloudstation) Get_AppList() (map[string]Apps, bool) {

	var all_apps map[string]Apps

	if default_apps, res := cs.GetDefaultAppList(); res {
	    //all_apps = append(all_apps, default_apps...)
	    for k, v := range default_apps {
		    all_apps[k] = v
	    }
	} else {
            return all_apps, res
        }

	if custom_apps, res := cs.GetCustomAppList(); res {
	    //all_apps = append(all_apps, custom_apps...)
	    for k, v := range custom_apps {
		    all_apps[k] = v
	    }
	} else {
            return all_apps, res
        }
        return all_apps, true
}

func (cs *Cloudstation) GetDefaultAppList() (map[string]Apps, bool) {
	return cs.GetSpecificAppList("default")
}

func (cs *Cloudstation) GetCustomAppList() (map[string]Apps, bool) {
	return cs.GetSpecificAppList("custom")
}

func (cs *Cloudstation) GetSpecificAppList(app_type string) (map[string]Apps, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	app_list_wrt_name := make(map[string]Apps)

	key := "get_app_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"app_type": app_type,
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
	}

	//var app_list []APPs
	var app APPs
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return app_list_wrt_name, false
	}
	err_app := json.NewDecoder(resp.Body).Decode(&app)
	if ok, json_data := convert_struct_to_string_via_json(app); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	if err_app != nil {
		fmt.Println(err_app)
	}
	//app_list = append(app_list, app)

	for _, indiv_app := range app.Data.Applications {
		app_list_wrt_name[indiv_app.Name]  = indiv_app
	}

	tot_entry := app.Total_Count
	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}

	i := 2
	for i <= tot_page {
		queryParam = map[string]interface{}{
			"app_type": app_type,
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
		}
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		//fmt.Println(resp)
		if resp.StatusCode != 200 {
                   return app_list_wrt_name, false
		}
		err_app := json.NewDecoder(resp.Body).Decode(&app)
	if ok, json_data := convert_struct_to_string_via_json(app); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
		if err_app != nil {
			fmt.Println(err_app)
		}
		//app_list = append(app_list, app)
		for _, indiv_app := range app.Data.Applications {
			app_list_wrt_name[indiv_app.Name]  = indiv_app
		}
		i++
	}
    return app_list_wrt_name, false
}

func (cs *Cloudstation) Get_AppId(app_name string) (int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	apps, err1 := cs.Get_AppList()

	if err1 {
		return 0, false
	}

	if app, ok := apps[app_name]; ok {
		return app.Id, true
	}
	return 0, false
}

func (cs *Cloudstation) Del_App(app_name string) bool {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	appid, err1 := cs.Get_AppId(app_name)
	if err1 == false {
		log.Err.Printf("Failed to fetch the APP ID !", err1)
		return false
	}

	return cs.DeleteCustomAppById(appid)
}

func (cs *Cloudstation) DeleteCustomAppById(appid int) bool {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	/*
	appid, err1 := cs.Get_AppId(app_name)
	if err1 == false {
		log.Err.Printf("Failed to fetch the APP ID !", err1)
		return false
	}*/

	var deletecustom_payload struct {
	}
	key := "delete_app"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(appid)})
	//fmt.Println(substitutedPath)

	header := cs.Construct_headers(apis[key].headers.headers)
	//fmt.Println(header)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, deletecustom_payload, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	return true
}
