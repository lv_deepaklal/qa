package restservice

import (
        "fmt"
	"errors"
	"net/http"
	"encoding/json"
	log "lvnautomation/lvnlog"
	tc_utils "lvnautomation/testcaseutils"
)

type DnsProxy struct {
        CreatedAt string `json:"created_at"`
	CustomerId int `json:"customer_id"`
	Id int `json:"id"`
	Name string `json:"name"`
        PrimaryServer string `json:"primary_server"`
	PrimaryServerIPVersion int `json:"primary_server_ip_version"`
	SecondaryServer string `json:"secondary_server"`
	SecondaryServerIPVersion int `json:"secondary_server_ip_version"`
	UpdatedAt string `json:"updated_at"`
	UserId int `json:"user_id"`
}

type CreateDnsProxyResponse struct {
    Data struct {
        Dnsproxy DnsProxy `json:"dns_proxy"`
    } `json:"data"`
    Message string `json:"message"`
}

type GetDnsProxyListResponse struct {
    Dnsproxies []DnsProxy `json:"dns_proxies"`
    TotalCount int `json:"total_count"`
}

type DnsProxyEntryPayload struct {
    Name string `json:"name"`
    Fqdn string `json:"fqdn"`
    PublicIp string `json:"public_ip"`
    PrivateIp string `json:"private_ip"`
}

type DnsProxyEntryForUpdate struct {
    CreatedAt string `json:"created_at"`
    DnsProxyId int `json:"dns_proxy_id"`
    Id int `json:"id"`
    UpdatedAt string `json:"updated_at"`
    IpVersion int `json:"ip_version"`
    DnsProxyEntryPayload
}

type UpdateDnsProxy struct {
       DnsProxy
       DnsProxyEntries []DnsProxyEntryForUpdate `json:"dns_proxy_entries"`
}

type GetDnsProxyResponse struct {
    DnsproxyInfo struct {
        UpdateDnsProxy
    } `json:"dns_proxy"`
}

//get /api/v1/lavelle/dns_proxies - get dns porxy list

func (cs *Cloudstation) AttachDnsproxyAtSite(site_name string, dns_proxy_name string) (bool, error, *UpdateSiteResponse) {

	var (
           res bool
	   err error
	   dns_proxy_wrt_name map[string]DnsProxy
	   dns_proxy_info DnsProxy
	)

	res, err, dns_proxy_wrt_name = cs.GetDnsProxyList()
	if !res {
        return res, err, nil
	}

	dns_proxy_info, res = dns_proxy_wrt_name[dns_proxy_name]
	if !res {
        return res, errors.New(fmt.Sprintf("Failed to get the dns proxy name %s", dns_proxy_name)), nil
	}

    if site_info, ok := cs.SitesByName()[site_name]; ok {
		site_info.Dns_proxy_enable = true
		site_info.Dns_proxy_id = dns_proxy_info.Id
        u := UpdateSiteInfo{
		    SiteInfo: site_info,
		}
        return cs.UpdateSite(u)
	}
	return false, errors.New(fmt.Sprintf("Failed to get the site info %s", site_name)), nil
}

/*
type UpdateSiteResponse struct {
    Message string `json:"message"`
    Site SiteInfo `json:"site"`
}

func (cs *Cloudstation) UpdateSite(site_info_payload UpdateSiteInfo) (bool, error, *UpdateSiteResponse) {

    key := "update_site"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"site_id": fmt.Sprintf("%d", site_info_payload.Id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	var update_site_response UpdateSiteResponse
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, site_info_payload, cookie)

	if resp.StatusCode != 200 {
		return false, errors.New(fmt.Sprintf("Response code %d", resp.StatusCode)), &update_site_response
	}
    err_site := json.NewDecoder(resp.Body).Decode(&update_site_response)
	if err_site != nil {
		log.Err.Println(err_site)
	}

	if ok, json_data := convert_struct_to_string_via_json(update_site_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}

	return true, nil, &update_site_response
}
*/

func (cs *Cloudstation) DetachDnsproxyFromSite(site_name string, dns_proxy_name string) (bool, error, *UpdateSiteResponse) {

	res, err, dns_proxy_wrt_name := cs.GetDnsProxyList()
	if !res {
        return res, err, nil
	}

	dns_proxy_info, res := dns_proxy_wrt_name[dns_proxy_name]
	if !res {
            return res, errors.New(fmt.Sprintf("Failed to get the dns proxy name %s", dns_proxy_name)), nil
	}

        if site_info, ok := cs.SitesByName()[site_name]; ok {
           site_info.Dns_proxy_enable = false
           site_info.Dns_proxy_id = dns_proxy_info.Id
           u := UpdateSiteInfo{
		    SiteInfo: site_info,
                }
           return cs.UpdateSite(u)
	}

	return false, errors.New(fmt.Sprintf("Failed to get the site info %s", site_name)), nil
}

func(cs *Cloudstation) CreateDnsProxy(c *CreateDnsProxyInput) (bool, error, *CreateDnsProxyResponse) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())

	var create_dns_proxy_response CreateDnsProxyResponse

	var create_dns_proxy_payload struct {
            Name string `json:"name"`
	    PrimaryServer string `json:"primary_server"`
	    SecondaryServer string `json:"secondary_server"`
            DnsProxyEntries []DnsProxyEntryPayload `json:"dns_proxy_entries"`
        }

	create_dns_proxy_payload.Name = c.Name
	create_dns_proxy_payload.PrimaryServer = c.PrimaryServer
	create_dns_proxy_payload.SecondaryServer = c.SecondaryServer
	for _, dns_proxy_entry := range c.DnsProxyEntries {
		create_dns_proxy_payload.DnsProxyEntries = append(create_dns_proxy_payload.DnsProxyEntries, DnsProxyEntryPayload{Name: dns_proxy_entry.Name,
		Fqdn: dns_proxy_entry.Fqdn,
		PublicIp : dns_proxy_entry.PublicIp,
		PrivateIp : dns_proxy_entry.PrivateIp,
	})
        }
	//create_dns_proxy_payload.DnsProxyEntries = c.DnsProxyEntries

	key := "create_dns_proxy"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, create_dns_proxy_payload, cookie)
	//fmt.Println(resp)

	if resp.StatusCode != 200 {
		return false, errors.New(fmt.Sprintf("Response code %d", resp.StatusCode)), &create_dns_proxy_response
	}

	err_dns := json.NewDecoder(resp.Body).Decode(&create_dns_proxy_response)
	if err_dns != nil {
		log.Err.Println(err_dns)
	}

	cs.Helper.dnsproxy.byName[create_dns_proxy_response.Data.Dnsproxy.Name] = create_dns_proxy_response.Data.Dnsproxy

	if ok, json_data := convert_struct_to_string_via_json(create_dns_proxy_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	return true, nil, &create_dns_proxy_response
}

func (cs *Cloudstation) GetDnsProxyList() (bool, error, map[string]DnsProxy) {

    if len(cs.Helper.dnsproxy.byName) == 0 {
	    return cs.ForceGetDnsProxyList()
	}
	return true, nil, cs.Helper.dnsproxy.byName
}

func(cs *Cloudstation) ForceGetDnsProxyList() (bool, error, map[string]DnsProxy) {

    log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
    var dummy struct {}

	var get_dnsproxy_list_response GetDnsProxyListResponse
	//security_profiles := []SecurityProfile{}
	dns_proxy_wrt_name := make(map[string]DnsProxy)

    key := "get_dnsproxy_list"
    apis := getApis()
    api := apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{})

	page_no := 1
	retreivals_per_page := 100
	max_pages_to_avoid_loop := 10

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        queryParam := map[string]interface{}{
                "key":      "",
                "page":     page_no,
                "per_page": retreivals_per_page,
                "sort":     "",
                "user": map[string]interface{}{
                        "username":             cs.Username,
                        "id":                   cs.Id,
                        "role":                 cs.Role,
                        "allow_manage":         cs.AllowManage,
                        "customer_id":          cs.CustomerId,
                        "hex_id":               cs.HexId,
                        "customer_hex_id":      cs.CustomerHexId,
                        "enable_notifications": cs.EnableNotifications,
                },
        }

	for {

            queryParam["page"] = page_no

            resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
            if resp.StatusCode != 200 {
                    return false, errors.New(fmt.Sprintf("Response code %d", resp.StatusCode)), dns_proxy_wrt_name
            }
            err_nwgrp := json.NewDecoder(resp.Body).Decode(&get_dnsproxy_list_response)
            if err_nwgrp != nil {
                fmt.Println(err_nwgrp)
            }
            if ok, json_data := convert_struct_to_string_via_json(get_dnsproxy_list_response); ok {
                log.Debug.Printf("response data: %s\n", json_data)
            }

            total_profiles := get_dnsproxy_list_response.TotalCount
	    //security_profiles = append(security_profiles, get_security_profiles_response.SecurityProfiles...)

            for _, dns_proxy := range get_dnsproxy_list_response.Dnsproxies {
                dns_proxy_wrt_name[dns_proxy.Name] = dns_proxy
            }

	    cs.Helper.dnsproxy.byName = dns_proxy_wrt_name

	    if len(dns_proxy_wrt_name) == total_profiles {
		    break
	    }

	    if page_no == max_pages_to_avoid_loop {
		    break
	    }

	    page_no += 1
	}

        return true, nil, dns_proxy_wrt_name
}


func(cs *Cloudstation) DeleteDnsProxyById(dns_id int) (bool, error) {

	var dummy struct {
	}
     //delete api/v1/lavelle/dns_proxies/3
	key := "delete_dns_proxy"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"dns_proxy_id": fmt.Sprintf("%d", dns_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	//fmt.Println(resp)

	if resp.StatusCode != 200 {
		return false, errors.New(fmt.Sprintf("Response code %d", resp.StatusCode))
	}

	return true, nil

}

func (cs *Cloudstation) DeleteDnsProxy(dns_proxy_name string) (bool, error) {

    if res, _, dns_wrt_name := cs.GetDnsProxyList(); res {
	if dns_proxy_info, ok := dns_wrt_name[dns_proxy_name]; ok {
        return cs.DeleteDnsProxyById(dns_proxy_info.Id)
	}
    }

    return false, errors.New(fmt.Sprintf("Failed to get the dns proxy name %s", dns_proxy_name))
}

func (cs *Cloudstation) GetDnsProxyInfoById(dns_proxy_id int) (bool, error, *GetDnsProxyResponse) {

    var dummy struct {}

    var get_dns_proxy_response GetDnsProxyResponse
    key := "get_dnsproxy_info"
    apis := getApis()
    api := apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{"dns_proxy_id" : fmt.Sprintf("%d", dns_proxy_id)})

    header := cs.Construct_headers(apis[key].headers.headers)
    var resp *http.Response
    cookie := []*http.Cookie{cs.Cookies}
    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)

    if resp.StatusCode != 200 {
	return false, errors.New(fmt.Sprintf("Response code %d", resp.StatusCode)), &get_dns_proxy_response
    }

    err_dns := json.NewDecoder(resp.Body).Decode(&get_dns_proxy_response)
    if err_dns != nil {
	log.Err.Println(err_dns)
    }

    if ok, json_data := convert_struct_to_string_via_json(get_dns_proxy_response); ok {
        log.Debug.Printf("response data: %s\n", json_data)
    }
    return true, nil, &get_dns_proxy_response
}

func (cs *Cloudstation) GetDnsProxyInfo(dns_proxy_name string) (bool, error, *GetDnsProxyResponse) {

    if ok, _, dns_proxy_wrt_name := cs.GetDnsProxyList(); ok {
        if dns_proxy_info, ok :=  dns_proxy_wrt_name[dns_proxy_name]; ok {
		return cs.GetDnsProxyInfoById(dns_proxy_info.Id)
	}
    }
    return false, errors.New(fmt.Sprintf("Failed to get dns proxy info for %s", dns_proxy_name)), nil
}

/*
func(cs *Cloudstation) UpdateDnsProxy(dns_proxy_name string, c *CreateDnsProxyInput) (bool, error, *CreateDnsProxyResponse) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())

	var update_dns_proxy_payload UpdateDnsProxy
	var update_dns_proxy_response CreateDnsProxyResponse

	res, err, get_dns_proxy_response := cs.GetDnsProxyInfo(c.Name)
	if !res {
		return res, err, &update_dns_proxy_response
	}

	update_dns_proxy_payload = get_dns_proxy_response.DnsproxyInfo.UpdateDnsProxy


	update_dns_proxy_payload.DnsProxy.Name = c.Name
	update_dns_proxy_payload.DnsProxy.PrimaryServer = c.PrimaryServer
	update_dns_proxy_payload.DnsProxy.SecondaryServer = c.SecondaryServer

	for _, dns_proxy_entry := range c.DnsProxyEntries {
		update_dns_proxy_payload.DnsProxyEntries = append(create_dns_proxy_payload.DnsProxyEntries, DnsProxyEntryPayload{Name: dns_proxy_entry.Name,
		Fqdn: dns_proxy_entry.Fqdn,
		PublicIp : dns_proxy_entry.PublicIp,
		PrivateIp : dns_proxy_entry.PrivateIp,
	})
        }
	//create_dns_proxy_payload.DnsProxyEntries = c.DnsProxyEntries

	key := "create_dns_proxy"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, create_dns_proxy_payload, cookie)
	//fmt.Println(resp)

	if resp.StatusCode != 200 {
		return false, errors.New(fmt.Sprintf("Response code %d", resp.StatusCode)), &create_dns_proxy_response
	}

	err_dns := json.NewDecoder(resp.Body).Decode(&create_dns_proxy_response)
	if err_dns != nil {
		log.Err.Println(err_dns)
	}

	cs.Helper.dnsproxy.byName[create_dns_proxy_response.Data.Dnsproxy.Name] = create_dns_proxy_response.Data.Dnsproxy

	if ok, json_data := convert_struct_to_string_via_json(create_dns_proxy_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	return true, nil, &create_dns_proxy_response
}
*/
