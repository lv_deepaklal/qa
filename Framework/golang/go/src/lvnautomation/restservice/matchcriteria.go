package restservice

import (
	"fmt"
	"net/http"
	"encoding/json"
	"strconv"
	//"encoding/hex"
	//"io"

	log "lvnautomation/lvnlog"
	tc_utils "lvnautomation/testcaseutils"
	//"io/ioutil"
	//"unicode/utf8"
	//"crypto/tls"
)

type appcat_object struct {
	Ticked          bool   `json:"ticked"`
	PolicyObjectAppCat
}
type PolicyObjectAppCat struct {
	App_category_id int     `json:"app_category_id"`
	Category        *string `json:"category"`
	Customer_id     int     `json:"customer_id"`
	Id              int     `json:"id"`
	Name            string  `json:"name"`
	User_id         int     `json:"user_id"`
}

type PolicyObjectNwGrp struct {
	Customer_id         int     `json:"customer_id"`
	Encapsulation       string  `json:"encapsulation"`
	Hex_id              string  `json:"hex_id"`
	Ib_enabled          string  `json:"ib_enabled"`
	Id                  int     `json:"id"`
	Location            *string `json:"location"`
	N_id                int     `json:"n_id"`
	Name                string  `json:"name"`
	Next_hop            string  `json:"next_hop"`
	Secured_data        string  `json:"secured_data"`
	Security_profile_id int     `json:"security_profile_id"`
	Tag                 *string `json:"tag"`
	User_id             int     `json:"user_id"`
	Vpn                 *string `json:"vpn"`
	Vpn_profile_id      int     `json:"vpn_profile_id"`
}

type PolicyObjectSubnets struct {
			Id                   int    `json:"id"`
			Site_id              int    `json:"site_id"`
			Site_name            string `json:"site_name"`
			Subnet               string `json:"subnet"`
			Subnet_name          string `json:"subnet_name"`
			Subnet_prefix_length int    `json:"subnet_prefix_length"`
			Vlan                 int    `json:"vlan"`
}

type PolicyObjectSites struct {
	Hex_id *string `json:"hex_id"`
	Id     int     `json:"id"`
	Name   string  `json:"name"`
}

type PolicyObjectIons struct {
	Hex_id *string `json:"hex_id"`
	Id     int     `json:"id"`
	Name   string  `json:"name"`
}

type POLICY_OBJECTS struct {
	Policy_objects struct {
		Application_categories []PolicyObjectAppCat `json:"application_categories"`
		Applications []AppInfo `json:"applications"`
		Ions []PolicyObjectIons `json:"ions"`
		Network_groups  []PolicyObjectNwGrp `json:"network_groups"`
		Sites []PolicyObjectSites `json:"sites"`
		Subnets []PolicyObjectSubnets  `json:"subnets"`
	} `json:"policy_objects"`
}

type appcat struct {
	Addition_type string        `json:"addition_type"`
	Object        appcat_object `json:"object"`
}

type app_object struct {
    AppInfo
    Ticked     bool    `json:"ticked"`
}

type app struct {
	Addition_type string     `json:"addition_type"`
	Object        app_object `json:"object"`
}
type custom_object struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}
type custom struct {
	Addition_type string        `json:"addition_type"`
	Object        custom_object `json:"object"`
}
type lansubnets_object struct {
	PolicyObjectSubnets
	Ticked               bool   `json:"ticked"`
}
type lansubnets struct {
	Addition_type string            `json:"addition_type"`
	Object        lansubnets_object `json:"object"`
}


type both struct {
	Application_categories []appcat `json:"application_categories"`
	Applications           []app    `json:"applications"`
	Customs                []custom `json:"customs"`
	Lan_subnets            []lansubnets `json:"lan_subnets"`
}
type from struct {
	Application_categories []appcat     `json:"application_categories"`
	Applications           []app        `json:"applications"`
	Customs                []custom     `json:"customs"`
	Lan_subnets            []lansubnets `json:"lan_subnets"`
}
type to struct {
	Application_categories []appcat     `json:"application_categories"`
	Applications           []app        `json:"applications"`
	Customs                []custom     `json:"customs"`
	Lan_subnets            []lansubnets `json:"lan_subnets"`
}

type MATCHCRITERIA struct {
	Both                 both   `json:"both"`
	Direction            string `json:"direction"`
	Dst_network_group_id int    `json:"dst_network_group_id"`
	From                 from   `json:"from"`
	//Network_group_name   string `json:"network_group_name"`
	Src_network_group_id int `json:"src_network_group_id"`
	To                   to  `json:"to"`
}

type MATCHCRITERIAv10_0_2 struct {
	Direction            string `json:"direction"`
	Dst_network_group_id int    `json:"dst_network_group_id"`
	Src_network_group_id int `json:"src_network_group_id"`

	/* optional fields */

	Both                 *both   `json:"both,omitempty"`
	From                 *from   `json:"from,omitempty"`
	//Network_group_name   string `json:"network_group_name"`
	To                   *to  `json:"to,omitempty"`
}


func (cs *Cloudstation) Get_PolicyObjectsList() (POLICY_OBJECTS, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var policyob_ls POLICY_OBJECTS
	var dummy struct {
	}

	key := "get_policy_objects_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//fmt.Println(resp)

	if resp.StatusCode != 200 {
		return policyob_ls, false
	}
	err_policyob := json.NewDecoder(resp.Body).Decode(&policyob_ls)
	if err_policyob != nil {
		fmt.Println(err_policyob)
	}
	if ok, json_data := convert_struct_to_string_via_json(policyob_ls); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}

	app_name_map := make(map[string]AppInfo)
	app_cat_name_map := make(map[string]PolicyObjectAppCat)
	nw_grp_map := make(map[string]PolicyObjectNwGrp)
	subnet_map := make(map[string]PolicyObjectSubnets)

	for _, nw_grp_info := range policyob_ls.Policy_objects.Network_groups {
		nw_grp_map[nw_grp_info.Name] = nw_grp_info
	}

	for _, subnet_info := range policyob_ls.Policy_objects.Subnets {
		subnet_map[subnet_info.Subnet_name]  = subnet_info
	}

	for _, app := range policyob_ls.Policy_objects.Applications {
		//fmt.Println(app)
		//cs.Helper.policy_objects.apps.byName[app.Name] = app
		app_name_map[app.Name]=app
	}

	for _, app_cat := range policyob_ls.Policy_objects.Application_categories {
            app_cat_name_map[app_cat.Name] = app_cat
	}

        cs.Helper.policy_objects.apps.byName  =  app_name_map
        cs.Helper.policy_objects.app_categories.byName  =  app_cat_name_map
        cs.Helper.policy_objects.nw_grp.byName  =  nw_grp_map
        cs.Helper.policy_objects.subnets.byName  =  subnet_map

	return policyob_ls, true
}

type OLDMATCHCRITERIA struct {
	Both struct {
		Application_categories []struct {
			Addition_type string `json:"addition_type"`
			Object        struct {
				App_category_id int    `json:"app_category_id"`
				Category        *int   `json:"category"`
				Customer_id     int    `json:"customer_id"`
				Id              int    `json:"id"`
				Name            string `json:"name"`
				Ticked          bool   `json:"ticked"`
				User_id         int    `json:"user_id"`
			} `json:"object"`
		} `json:"application_categories"`
		Applications []struct {
			Addition_type string `json:"addition_type"`
			Object        struct {
				Appid                   int  `json:"app_id"`
				Application_category_id *int `json:"application_category_id"`
				Customer_id             int  `json:"customer_id"`
				//Default                 bool    `json:"default"`
				Dscp       *string `json:"dscp"`
				Dst_ip     *string `json:"dst_ip"`
				Dst_port   *int    `json:"dst_port"`
				Hex_id     string  `json:"hex_id"`
				Hostname   *string `json:"hostname"`
				Id         int     `json:"id"`
				Ip         *string `json:"ip"`
				Match_type *string `json:"match_type"`
				Name       string  `json:"name"`
				Port       *int    `json:"port"`
				Protocol   *string `json:"protocol"`
				Signature  *string `json:"signature"`
				Src_ip     *string `json:"src_ip"`
				Src_port   *int    `json:"src_port"`
				Tag        *string `json:"tag"`
				Tcp_port   *int    `json:"tcp_port"`
				Ticked     bool    `json:"ticked"`
				Udp_port   *int    `json:"udp_port"`
				User_id    int     `json:"user_id"`
			} `json:"object"`
		} `json:"applications"`
		Customs []struct {
			Addition_type string `json:"addition_type"`
			Object        struct {
				Type  string `json:"type"`
				Value string `json:"value"`
			} `json:"object"`
		} `json:"customs"`
		Lan_subnets []struct {
		} `json:"lan_subnets"`
	} `json:"both"`
	Direction            string `json:"direction"`
	Dst_network_group_id int    `json:"dst_network_group_id"`
	From                 struct {
		Application_categories []struct {
		} `json:"application_categories"`
		Applications []struct {
		} `json:"applications"`
		Customs []struct {
			Addition_type string `json:"addition_type"`
			Object        struct {
				Type  string `json:"type"`
				Value string `json:"value"`
			} `json:"object"`
		} `json:"Customs"`
		Lan_subnets []struct {
			Addition_type string `json:"addition_type"`
			Object        struct {
				Id                   int    `json:"id"`
				Site_id              int    `json:"site_id"`
				Site_name            string `json:"site_name"`
				Subnet               string `json:"subnet"`
				Subnet_name          string `json:"subnet_name"`
				Subnet_prefix_length int    `json:"subnet_prefix_length"`
				Ticked               bool   `json:"ticked"`
				Vlan                 int    `json:"vlan"`
			} `json:"object"`
		} `json:"lan_subnets"`
	} `json:"from"`
	//Network_group_name   string `json:"network_group_name"`
	Src_network_group_id int `json:"src_network_group_id"`
	To                   struct {
		Application_categories []struct {
		} `json:"application_categories"`
		Applications []struct {
		} `json:"applications"`
		Customs []struct {
			Addition_type string `json:"addition_type"`
			Object        struct {
				Type  string `json:"type"`
				Value string `json:"value"`
			} `json:"object"`
		} `json:"Customs"`
		Lan_subnets []struct {
			Addition_type string `json:"addition_type"`
			Object        struct {
				Id                   int    `json:"id"`
				Site_id              int    `json:"site_id"`
				Site_name            string `json:"site_name"`
				Subnet               string `json:"subnet"`
				Subnet_name          string `json:"subnet_name"`
				Subnet_prefix_length int    `json:"subnet_prefix_length"`
				Ticked               bool   `json:"ticked"`
				Vlan                 int    `json:"vlan"`
			} `json:"object"`
		} `json:"lan_subnets"`
	} `json:"to"`
}

func (cs *Cloudstation) GetMatchCriteria(matchCriteria MatchCriteriaInput) (*MATCHCRITERIA, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var M_criteria MATCHCRITERIA

	_, res := cs.Get_PolicyObjectsList()
	if !res {
		return nil, false
	}

	// both -> application categories
        M_criteria.Both.Application_categories = []appcat{}

	app_cats_not_configured := []string{}

	for _, app_cat_name := range matchCriteria.appCategories {
            if app_cat_info, ok := cs.PolicyObjAppCats()[app_cat_name]; ok {
		y := appcat{
			Addition_type: "OR",
			Object: appcat_object{
                                PolicyObjectAppCat : app_cat_info,
				Ticked:          true,
			},
		}
		M_criteria.Both.Application_categories = append(M_criteria.Both.Application_categories, y)
            } else {
                app_cats_not_configured = append(app_cats_not_configured, app_cat_name)
	    }
	}

	if len(app_cats_not_configured) > 0 {
		return nil, false
	}

	// both -> applications

	apps_not_configured := []string{}
        M_criteria.Both.Applications =  []app{}
	for _, app_name := range matchCriteria.apps {
		if app_info, ok := cs.PolicyObjApps()[app_name]; ok {
			y := app{
				Addition_type: "OR",
				Object: app_object{
					AppInfo :  app_info,
					Ticked:      true,
				},
			}
			M_criteria.Both.Applications = append(M_criteria.Both.Applications, y)
		} else {
			apps_not_configured = append(apps_not_configured, app_name)
		}
	}
	if len(apps_not_configured) > 0 {
		return nil, false
	}

	// both -> customs

        M_criteria.Both.Customs = []custom{}
	for _, val := range matchCriteria.customProtocols {
		y := custom{
			Addition_type: val.condition,
			Object: custom_object{
				Type:  "CustomProtocol",
				Value: val.protocol,
			},
		}
		M_criteria.Both.Customs = append(M_criteria.Both.Customs, y)
	}

	// both -> lan subnets always empty list
        M_criteria.Both.Lan_subnets = []lansubnets{}

	M_criteria.Direction = matchCriteria.direction
	var ok bool

	var dstnwgrp_info, srcnwgrp_info PolicyObjectNwGrp 
	srcnwgrp_info, ok = cs.PolicyObjNwgroup()[matchCriteria.srcNwgrp]
	if !ok {
		return nil, false
	}
	M_criteria.Src_network_group_id = srcnwgrp_info.Id

	dstnwgrp_info, ok = cs.PolicyObjNwgroup()[matchCriteria.dstNwgrp]
	if !ok {
		return nil, false
	}
	M_criteria.Dst_network_group_id = dstnwgrp_info.Id

	// from -> application categories always empty
        M_criteria.From.Application_categories = []appcat{}

	// from -> applications always empty
        M_criteria.From.Applications = []app{}

	// from -> customs for ip

	for _, val := range matchCriteria.from.custom.ip {
		y := custom{
			Addition_type: val.condition,
			Object: custom_object{
				Type:  "CustomIp",
				Value: val.ip,
			},
		}
		M_criteria.From.Customs = append(M_criteria.From.Customs, y)
	}

	// from -> customs for port

	for _, val := range matchCriteria.from.custom.port {
		y := custom{
			Addition_type: val.condition,
			Object: custom_object{
				Type:  "CustomPort",
				Value: strconv.Itoa(val.port),
			},
		}
		M_criteria.From.Customs = append(M_criteria.From.Customs, y)
	}

	// from -> lan_subnets

	from_subnets_not_included := []string{}
	for _, subnet_name := range matchCriteria.from.subnet {
		if subnet_info, ok := cs.PolicyObjSubnets()[subnet_name]; ok {
			y := lansubnets{
				Addition_type: "OR",
				Object: lansubnets_object{
					PolicyObjectSubnets : subnet_info,
					Ticked:               true,
				},
			}
			M_criteria.From.Lan_subnets = append(M_criteria.From.Lan_subnets, y)
		} else {
			from_subnets_not_included = append(from_subnets_not_included, subnet_name)
		}
	}

	if len(from_subnets_not_included) > 0 {
		return nil, false
	}

	// to -> application categories always empty
        M_criteria.To.Application_categories = []appcat{}

	// to -> applications always empty
        M_criteria.To.Applications = []app{}

	// to -> customs for ip

	for _, val := range matchCriteria.to.custom.ip {
		y := custom{
			Addition_type: val.condition,
			Object: custom_object{
				Type:  "CustomIp",
				Value: val.ip,
			},
		}
		M_criteria.To.Customs = append(M_criteria.To.Customs, y)
	}

	// to -> customs for port

	for _, val := range matchCriteria.to.custom.port {
		y := custom{
			Addition_type: val.condition,
			Object: custom_object{
				Type:  "CustomPort",
				Value: strconv.Itoa(val.port),
			},
		}
		M_criteria.To.Customs = append(M_criteria.To.Customs, y)
	}

	// to -> lan_subnets
	to_subnets_not_included := []string{}
	for _, subnet_name := range matchCriteria.to.subnet {
		if subnet_info, ok := cs.PolicyObjSubnets()[subnet_name]; ok {
			y := lansubnets{
				Addition_type: "OR",
				Object: lansubnets_object{
					PolicyObjectSubnets : subnet_info,
					Ticked:               true,
				},
			}
		        M_criteria.To.Lan_subnets = append(M_criteria.To.Lan_subnets, y)
		} else {
			to_subnets_not_included = append(to_subnets_not_included, subnet_name)
		}
	}

	if len(to_subnets_not_included) > 0 {
		return nil, false
	}

	return &M_criteria, true
}

/*
func (cs *Cloudstation) GetMatchCriteriaV10_0_2(matchCriteria MatchCriteriaInput) (*MATCHCRITERIAv10_0_2, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var M_criteria MATCHCRITERIAv10_0_2

	x, _ := cs.Get_PolicyObjectsList()

	appcat_id, err1 := cs.Get_POAppCatId(matchCriteria.appCategories, x)
	if err1 == false {
		log.Err.Println("Failed to fetch the App Category Id !", err1)
		return &M_criteria, false
	}

	appcatOnly_id, err2 := cs.Get_POAppCatOnlyId(matchCriteria.appCategories, x)
	if err2 == false {
		log.Err.Println("Failed to fetch the App Category Id Only !", err2)
		return &M_criteria, false
	}

	app_id, err3 := cs.Get_POAppId(matchCriteria.apps, x)
	if err3 == false {
		log.Err.Println("Failed to fetch the App Id !", err3)
		return &M_criteria, false
	}

	appOnly_id, err4 := cs.Get_POAppOnlyId(matchCriteria.apps, x)
	if err4 == false {
		log.Err.Println("Failed to fetch the App Id Only !", err4)
		return &M_criteria, false
	}

	appHex_id, err5 := cs.Get_POAppHexId(matchCriteria.apps, x)
	if err5 == false {
		log.Err.Println("Failed to fetch the App HexId !", err5)
		return &M_criteria, false
	}

	srcnetgr_id, err6 := cs.Get_PONetGrpId(matchCriteria.srcNwgrp, x)
	if err6 == false {
		log.Err.Println("Failed to fetch the SrcNwgrp Id !", err6)
		return &M_criteria, false
	}

	dstnetgr_id, err7 := cs.Get_PONetGrpId(matchCriteria.dstNwgrp, x)
	if err7 == false {
		log.Err.Println("Failed to fetch the DstNwgrp Id !", err7)
		return &M_criteria, false
	}

	sub_id, err8 := cs.Get_POSubnetsId(matchCriteria.from.subnet, x)
	if err8 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err8)
		return &M_criteria, false
	}

	sub_site_id, err9 := cs.Get_POSubnetsSiteId(matchCriteria.from.subnet, x)
	if err9 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err8)
		return &M_criteria, false
	}

	sub_site_name, err10 := cs.Get_POSubnetsSiteName(matchCriteria.from.subnet, x)
	if err10 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err10)
		return &M_criteria, false
	}

	sub_subnet, err11 := cs.Get_POSubnetsSubnet(matchCriteria.from.subnet, x)
	if err11 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err11)
		return &M_criteria, false
	}

	sub_pref, err12 := cs.Get_POSubnetsPrefLen(matchCriteria.from.subnet, x)
	if err12 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err12)
		return &M_criteria, false
	}

	sub_vlan, err13 := cs.Get_POSubnetsVlan(matchCriteria.from.subnet, x)
	if err13 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err13)
		return &M_criteria, false
	}

	sub_id1, err14 := cs.Get_POSubnetsId(matchCriteria.to.subnet, x)
	if err14 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err14)
		return &M_criteria, false
	}

	sub_site_id1, err15 := cs.Get_POSubnetsSiteId(matchCriteria.to.subnet, x)
	if err15 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err15)
		return &M_criteria, false
	}

	sub_site_name1, err16 := cs.Get_POSubnetsSiteName(matchCriteria.to.subnet, x)
	if err16 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err16)
		return &M_criteria, false
	}

	sub_subnet1, err17 := cs.Get_POSubnetsSubnet(matchCriteria.to.subnet, x)
	if err17 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err17)
		return &M_criteria, false
	}

	sub_pref1, err18 := cs.Get_POSubnetsPrefLen(matchCriteria.to.subnet, x)
	if err18 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err18)
		return &M_criteria, false
	}

	sub_vlan1, err19 := cs.Get_POSubnetsVlan(matchCriteria.to.subnet, x)
	if err19 == false {
		log.Err.Println("Failed to fetch the Sub Id !", err19)
		return &M_criteria, false
	}

	// both -> application categories

        //M_criteria.Both = nil
        b := both{}
	for i, val := range matchCriteria.appCategories {
		y := appcat{
			Addition_type: "OR",
			Object: appcat_object{
				App_category_id: appcat_id[i],
				Customer_id:     cs.CustomerId,
				Id:              appcatOnly_id[i],
				Name:            val,
				Ticked:          true,
				User_id:         cs.Id,
			},
		}
                b.Application_categories = append(b.Application_categories, y)
	}

	// both -> applications

	for i, val := range matchCriteria.apps {
		y := app{
			Addition_type: "OR",
			Object: app_object{
				App_id:      app_id[i],
				Customer_id: cs.CustomerId,
				Hex_id:      appHex_id[i],
				Id:          appOnly_id[i],
				Name:        val,
				Ticked:      true,
				User_id:     cs.Id,
			},
		}
		//M_criteria.Both.Applications = append(M_criteria.Both.Applications, y)
		b.Applications = append(b.Applications, y)
	}

	// both -> customs

	for _, val := range matchCriteria.customProtocols {
		y := custom{
			Addition_type: val.condition,
			Object: custom_object{
				Type:  "CustomProtocol",
				Value: val.protocol,
			},
		}
		//M_criteria.Both.Customs = append(M_criteria.Both.Customs, y)
		b.Customs = append(b.Customs, y)
	}

       if !reflect.DeepEqual(b,  both{}) {
            M_criteria.Both = &b
	}

	M_criteria.Direction = matchCriteria.direction
	M_criteria.Dst_network_group_id = dstnetgr_id

	// from -> application categories

	frm := from{}
	for i, val := range matchCriteria.appCategories {
		y := appcat{
			Addition_type: "OR",
			Object: appcat_object{
				App_category_id: appcat_id[i],
				Customer_id:     cs.CustomerId,
				Id:              appcatOnly_id[i],
				Name:            val,
				Ticked:          true,
				User_id:         cs.Id,
			},
		}
		//M_criteria.From.Application_categories = append(M_criteria.From.Application_categories, y)
		frm.Application_categories = append(frm.Application_categories, y)
	}

	// from -> applications

	for i, val := range matchCriteria.apps {
		y := app{
			Addition_type: "OR",
			Object: app_object{
				App_id:      app_id[i],
				Customer_id: cs.CustomerId,
				Hex_id:      appHex_id[i],
				Id:          appOnly_id[i],
				Name:        val,
				Ticked:      true,
				User_id:     cs.Id,
			},
		}
		//M_criteria.From.Applications = append(M_criteria.From.Applications, y)
		frm.Applications = append(frm.Applications, y)
	}

	// from -> customs for ip

	for _, val := range matchCriteria.from.custom.ip {
		y := custom{
			Addition_type: val.condition,
			Object: custom_object{
				Type:  "CustomIp",
				Value: val.ip,
			},
		}
		//M_criteria.From.Customs = append(M_criteria.From.Customs, y)
		frm.Customs = append(frm.Customs, y)
	}

	// from -> customs for port

	for _, val := range matchCriteria.from.custom.port {
		y := custom{
			Addition_type: val.condition,
			Object: custom_object{
				Type:  "CustomPort",
				Value: strconv.Itoa(val.port),
			},
		}
		//M_criteria.From.Customs = append(M_criteria.From.Customs, y)
		frm.Customs = append(frm.Customs, y)
	}

	// from -> lan_subnets

	for i, val := range matchCriteria.from.subnet {
		y := lansubnets{
			Addition_type: "OR",
			Object: lansubnets_object{
				Id:                   sub_id[i],
				Site_id:              sub_site_id[i],
				Site_name:            sub_site_name[i],
				Subnet:               sub_subnet[i],
				Subnet_name:          val,
				Subnet_prefix_length: sub_pref[i],
				Ticked:               true,
				Vlan:                 sub_vlan[i],
			},
		}
		//M_criteria.From.Lan_subnets = append(M_criteria.From.Lan_subnets, y)
		frm.Lan_subnets = append(frm.Lan_subnets, y)
	}

	//if frm != from{} {
        if !reflect.DeepEqual(frm,  from{}) {
            M_criteria.From = &frm
	}

	M_criteria.Src_network_group_id = srcnetgr_id

	// to -> application categories

	tto := to{}
	for i, val := range matchCriteria.appCategories {
		y := appcat{
			Addition_type: "OR",
			Object: appcat_object{
				App_category_id: appcat_id[i],
				Customer_id:     cs.CustomerId,
				Id:              appcatOnly_id[i],
				Name:            val,
				Ticked:          true,
				User_id:         cs.Id,
			},
		}
		//M_criteria.To.Application_categories = append(M_criteria.To.Application_categories, y)
		tto.Application_categories = append(tto.Application_categories, y)
	}

	// to -> applications

	for i, val := range matchCriteria.apps {
		y := app{
			Addition_type: "OR",
			Object: app_object{
				App_id:      app_id[i],
				Customer_id: cs.CustomerId,
				Hex_id:      appHex_id[i],
				Id:          appOnly_id[i],
				Name:        val,
				Ticked:      true,
				User_id:     cs.Id,
			},
		}
		//M_criteria.To.Applications = append(M_criteria.To.Applications, y)
		tto.Applications = append(tto.Applications, y)
	}

	// to -> customs for ip

	for _, val := range matchCriteria.to.custom.ip {
		y := custom{
			Addition_type: val.condition,
			Object: custom_object{
				Type:  "CustomIp",
				Value: val.ip,
			},
		}
		//M_criteria.To.Customs = append(M_criteria.To.Customs, y)
		tto.Customs = append(tto.Customs, y)
	}

	// to -> customs for port

	for _, val := range matchCriteria.to.custom.port {
		y := custom{
			Addition_type: val.condition,
			Object: custom_object{
				Type:  "CustomPort",
				Value: strconv.Itoa(val.port),
			},
		}
		//M_criteria.To.Customs = append(M_criteria.To.Customs, y)
		tto.Customs = append(tto.Customs, y)
	}

	// to -> lan_subnets

	for i, val := range matchCriteria.to.subnet {
		y := lansubnets{
			Addition_type: "OR",
			Object: lansubnets_object{
				Id:                   sub_id1[i],
				Site_id:              sub_site_id1[i],
				Site_name:            sub_site_name1[i],
				Subnet:               sub_subnet1[i],
				Subnet_name:          val,
				Subnet_prefix_length: sub_pref1[i],
				Ticked:               true,
				Vlan:                 sub_vlan1[i],
			},
		}
		//M_criteria.To.Lan_subnets = append(M_criteria.To.Lan_subnets, y)
		tto.Lan_subnets = append(tto.Lan_subnets, y)
	}
	//if to != To{} {
        if !reflect.DeepEqual(tto,  to{}) {
            M_criteria.To = &tto
	}
	return &M_criteria, true
}
*/
/*
func (cs *Cloudstation) Get_POAppCatId(appcat_name []string, policyob_ls POLICY_OBJECTS) ([]int, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length_appcat_name := len(appcat_name)
	var appcat_id_list []int

	length_appcat := len(policyob_ls.Policy_objects.Application_categories)

	var appcat_id int
	var found_appcat_id bool
	for i := 0; i < length_appcat_name; i++ {
		for j := 0; j < length_appcat; j++ {
			if policyob_ls.Policy_objects.Application_categories[j].Name == appcat_name[i] {
				appcat_id = policyob_ls.Policy_objects.Application_categories[j].App_category_id
				found_appcat_id = true
				break
			}
		}
		appcat_id_list = append(appcat_id_list, appcat_id)
		if found_appcat_id == false {
			log.Err.Println("No Index Id for App Category found !")
			return appcat_id_list, false
		}
	}
	//fmt.Println("--------------Got into the Verse for APPCAT ID for Application Category-------------")
	//fmt.Println()
	return appcat_id_list, true
}

func (cs *Cloudstation) Get_POAppCatOnlyId(appcat_name []string, policyob_ls POLICY_OBJECTS) ([]int, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length_appcat_name := len(appcat_name)
	var appcatOnly_id_list []int

	length_appcat := len(policyob_ls.Policy_objects.Application_categories)

	var appcatOnly_id int
	var found_appcatOnly_id bool
	for i := 0; i < length_appcat_name; i++ {
		for j := 0; j < length_appcat; j++ {
			if policyob_ls.Policy_objects.Application_categories[j].Name == appcat_name[i] {
				appcatOnly_id = policyob_ls.Policy_objects.Application_categories[j].Id
				found_appcatOnly_id = true
				break
			}
		}
		appcatOnly_id_list = append(appcatOnly_id_list, appcatOnly_id)
		if found_appcatOnly_id == false {
			log.Err.Println("No Index Id for App Category found !")
			return appcatOnly_id_list, false
		}
	}
	//fmt.Println("--------------Got into the Verse for APPCAT ONLY ID for Application Category-------------")
	//fmt.Println()
	return appcatOnly_id_list, true
}

func (cs *Cloudstation) Get_POAppId(app_name []string, policyob_ls POLICY_OBJECTS) ([]int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length_app_name := len(app_name)
	var app_id_list []int

	length_app := len(policyob_ls.Policy_objects.Applications)

	var app_id int
	var found_app_id bool
	for i := 0; i < length_app_name; i++ {
		for j := 0; j < length_app; j++ {
			if policyob_ls.Policy_objects.Applications[j].Name == app_name[i] {
				app_id = policyob_ls.Policy_objects.Applications[j].App_id
				found_app_id = true
				break
			}
		}
		app_id_list = append(app_id_list, app_id)
		if found_app_id == false {
			log.Err.Println("No Index Id for App found !")
			return app_id_list, false
		}
	}
	//fmt.Println("--------------Got into the Verse for APP ID for Application-------------")
	//fmt.Println()
	return app_id_list, true
}

func (cs *Cloudstation) Get_POAppOnlyId(app_name []string, policyob_ls POLICY_OBJECTS) ([]int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length_app_name := len(app_name)
	var appOnly_id_list []int

	length_app := len(policyob_ls.Policy_objects.Applications)

	var appOnly_id int
	var found_appOnly_id bool
	for i := 0; i < length_app_name; i++ {
		for j := 0; j < length_app; j++ {
			if policyob_ls.Policy_objects.Applications[j].Name == app_name[i] {
				appOnly_id = policyob_ls.Policy_objects.Applications[j].Id
				found_appOnly_id = true
				break
			}
		}
		appOnly_id_list = append(appOnly_id_list, appOnly_id)
		if found_appOnly_id == false {
			log.Err.Println("No Index Id for App found !")
			return appOnly_id_list, false
		}
	}
	//fmt.Println("--------------Got into the Verse for APP ONLY ID for Application-------------")
	//fmt.Println()
	return appOnly_id_list, true
}

func (cs *Cloudstation) Get_POAppHexId(app_name []string, policyob_ls POLICY_OBJECTS) ([]string, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length_app_name := len(app_name)
	var appHex_id_list []string

	length_app := len(policyob_ls.Policy_objects.Applications)

	var appHex_id string
	var found_appHex_id bool
	for i := 0; i < length_app_name; i++ {
		for j := 0; j < length_app; j++ {
			if policyob_ls.Policy_objects.Applications[j].Name == app_name[i] {
				appHex_id = policyob_ls.Policy_objects.Applications[j].Hex_id
				found_appHex_id = true
				break
			}
		}
		appHex_id_list = append(appHex_id_list, appHex_id)
		if found_appHex_id == false {
			log.Err.Println("No Index Id for App found !")
			return appHex_id_list, false
		}
	}
	//fmt.Println("--------------Got into the Verse for HEX ID for Application-------------")
	//fmt.Println()
	return appHex_id_list, true
}

func (cs *Cloudstation) Get_PONetGrpId(netgr_name string, policyob_ls POLICY_OBJECTS) (int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length := len(policyob_ls.Policy_objects.Network_groups)
	var netgr_id int
	var found_netgr_id bool
	for i := 0; i < length; i++ {
		if policyob_ls.Policy_objects.Network_groups[i].Name == netgr_name {
			netgr_id = policyob_ls.Policy_objects.Network_groups[i].Id
			found_netgr_id = true
			break
		}
	}
	if found_netgr_id == false {
		log.Err.Println("No Index Id for App found !")
		return 0, false
	}
	//fmt.Println("--------------Got into the Verse for NETWORK GROUP ID for Network Groups-------------")
	//fmt.Println()
	return netgr_id, true
}

func (cs *Cloudstation) Get_POSubnetsId(sub_name []string, policyob_ls POLICY_OBJECTS) ([]int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length_sub_name := len(sub_name)
	var sub_id_list []int

	length := len(policyob_ls.Policy_objects.Subnets)
	var sub_id int
	var found_sub_id bool
	for i := 0; i < length_sub_name; i++ {
		for j := 0; j < length; j++ {
			if policyob_ls.Policy_objects.Subnets[j].Subnet_name == sub_name[i] {
				sub_id = policyob_ls.Policy_objects.Subnets[j].Id
				found_sub_id = true
				break
			}
		}
		sub_id_list = append(sub_id_list, sub_id)
		if found_sub_id == false {
			log.Err.Println("No Index Id for Subnet found !")
			return sub_id_list, false
		}
	}
	//fmt.Println("--------------Got into the Verse for ID for Subnets-------------")
	//fmt.Println()
	return sub_id_list, true
}

func (cs *Cloudstation) Get_POSubnetsSiteId(sub_name []string, policyob_ls POLICY_OBJECTS) ([]int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length_sub_name := len(sub_name)
	var subsite_id_list []int

	length := len(policyob_ls.Policy_objects.Subnets)
	var subsite_id int
	var found_subsite_id bool
	for i := 0; i < length_sub_name; i++ {
		for j := 0; j < length; j++ {
			if policyob_ls.Policy_objects.Subnets[j].Subnet_name == sub_name[i] {
				subsite_id = policyob_ls.Policy_objects.Subnets[j].Site_id
				found_subsite_id = true
				break
			}
		}
		subsite_id_list = append(subsite_id_list, subsite_id)
		if found_subsite_id == false {
			log.Err.Println("No Index Id for Subnet found !")
			return subsite_id_list, false
		}
	}
	//fmt.Println("--------------Got into the Verse for SITE ID for Subnets-------------")
	//fmt.Println()
	return subsite_id_list, true
}

func (cs *Cloudstation) Get_POSubnetsSiteName(sub_name []string, policyob_ls POLICY_OBJECTS) ([]string, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length_sub_name := len(sub_name)
	var subsite_name_list []string

	length := len(policyob_ls.Policy_objects.Subnets)
	var subsite_name string
	var found_subsite_name bool
	for i := 0; i < length_sub_name; i++ {
		for j := 0; j < length; j++ {
			if policyob_ls.Policy_objects.Subnets[j].Subnet_name == sub_name[i] {
				subsite_name = policyob_ls.Policy_objects.Subnets[j].Site_name
				found_subsite_name = true
				break
			}
		}
		subsite_name_list = append(subsite_name_list, subsite_name)
		if found_subsite_name == false {
			log.Err.Println("No Index Id for Subnet found !")
			return subsite_name_list, false
		}
	}
	//fmt.Println("--------------Got into the Verse for SITE NAME for Subnets-------------")
	//fmt.Println()
	return subsite_name_list, true
}

func (cs *Cloudstation) Get_POSubnetsSubnet(sub_name []string, policyob_ls POLICY_OBJECTS) ([]string, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length_sub_name := len(sub_name)
	var sub_subnet_list []string

	length := len(policyob_ls.Policy_objects.Subnets)
	var sub_subnet string
	var found_sub_subnet bool
	for i := 0; i < length_sub_name; i++ {
		for j := 0; j < length; j++ {
			if policyob_ls.Policy_objects.Subnets[j].Subnet_name == sub_name[i] {
				sub_subnet = policyob_ls.Policy_objects.Subnets[j].Subnet
				found_sub_subnet = true
				break
			}
		}
		sub_subnet_list = append(sub_subnet_list, sub_subnet)
		if found_sub_subnet == false {
			log.Err.Println("No Index Id for Subnet found !")
			return sub_subnet_list, false
		}
	}
	//fmt.Println("--------------Got into the Verse for SUBNET for Subnets-------------")
	//fmt.Println()
	return sub_subnet_list, true
}

func (cs *Cloudstation) Get_POSubnetsPrefLen(sub_name []string, policyob_ls POLICY_OBJECTS) ([]int, bool) {


	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length_sub_name := len(sub_name)
	var sub_pref_list []int

	length := len(policyob_ls.Policy_objects.Subnets)
	var sub_pref int
	var found_sub_pref bool
	for i := 0; i < length_sub_name; i++ {
		for j := 0; j < length; j++ {
			if policyob_ls.Policy_objects.Subnets[j].Subnet_name == sub_name[i] {
				sub_pref = policyob_ls.Policy_objects.Subnets[j].Subnet_prefix_length
				found_sub_pref = true
				break
			}
		}
		sub_pref_list = append(sub_pref_list, sub_pref)
		if found_sub_pref == false {
			log.Err.Println("No Index Id for Subnet found !")
			return sub_pref_list, false
		}
	}
	//fmt.Println("--------------Got into the Verse for PREFIX LENGTH for Subnets-------------")
	//fmt.Println()
	return sub_pref_list, true
}

func (cs *Cloudstation) Get_POSubnetsVlan(sub_name []string, policyob_ls POLICY_OBJECTS) ([]int, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	length_sub_name := len(sub_name)
	var sub_vlan_list []int

	length := len(policyob_ls.Policy_objects.Subnets)
	var sub_vlan int
	var found_sub_vlan bool
	for i := 0; i < length_sub_name; i++ {
		for j := 0; j < length; j++ {
			if policyob_ls.Policy_objects.Subnets[j].Subnet_name == sub_name[i] {
				sub_vlan = policyob_ls.Policy_objects.Subnets[j].Vlan
				found_sub_vlan = true
				break
			}
		}
		sub_vlan_list = append(sub_vlan_list, sub_vlan)
		if found_sub_vlan == false {
			log.Err.Println("No Index Id for Subnet found !")
			return sub_vlan_list, false
		}
	}
	//fmt.Println("--------------Got into the Verse for VLAN ID for Subnets-------------")
	//fmt.Println()
	return sub_vlan_list, true
}
*/

/*
type appcat_object struct {
	App_category_id int    `json:"app_category_id"`
	Customer_id     int    `json:"customer_id"`
	Id              int    `json:"id"`
	Name            string `json:"name"`
	Ticked          bool   `json:"ticked"`
	User_id         int    `json:"user_id"`
}
*/

/*
type app_object struct {

	//AppInfo

	App_id      int `json:"app_id"`
	Customer_id int `json:"customer_id"`
	//Default                 bool    `json:"default"`
	Dscp       *string `json:"dscp"`
	Dst_ip     *string `json:"dst_ip"`
	Dst_port   *int    `json:"dst_port"`
	Hex_id     string  `json:"hex_id"`
	Hostname   *string `json:"hostname"`
	Id         int     `json:"id"`
	Ip         *string `json:"ip"`
	Match_type *string `json:"match_type"`
	Name       string  `json:"name"`
	Port       *int    `json:"port"`
	Protocol   *string `json:"protocol"`
	Signature  *string `json:"signature"`
	Src_ip     *string `json:"src_ip"`
	Src_port   *int    `json:"src_port"`
	Tag        *string `json:"tag"`
	Tcp_port   *int    `json:"tcp_port"`
	Ticked     bool    `json:"ticked"`
	Udp_port   *int    `json:"udp_port"`
	User_id    int     `json:"user_id"`
}*/
