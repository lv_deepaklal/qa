package restservice

import (
	"fmt"
	"net/http"
	//"errors"
	"encoding/json"
	//"regexp"
	"strconv"
	//"strings"
	//"bytes"
	//"compress/gzip"
	//"io"

	log "lvnautomation/lvnlog"
	//"time"
	//tc_utils "lvnautomation/testcaseutils"
)

/*
	TRL POLICY
*/

type TrlInfo struct {
	CreatedAt              string `json:"created_at"`
	Customer_id            string `json:"customer_id"`
	Direction              string `json:"direction"`
	Dst_network_group_id   int    `json:"dst_network_group_id"`
	Dst_network_group_name string `json:"dst_network_group_name"`
	Id                     int    `json:"id"`
	Name                   string `json:"name"`
	Pol_id                 int    `json:"pol_id"`
	Pol_type               string `json:"pol_type"`
	Priority               int    `json:"priority"`
	Src_network_group_id   int    `json:"src_network_group_id"`
	Src_network_group_name string `json:"src_network_group_name"`
	Tag                    string `json:"tag"`
	UpdatedAt              string `json:"updated_at"`
	User_id                string `json:"user_id"`
        Rate                   int    `json:"rate"`
}

type CreateTRLInput struct{
        Message string `json:"message"`
        Tot_count int `json:"total_count"`
        Reason string `json:"reason"`
        Data struct{
                Traf_rate_lim TrlInfo `json:"traffic_rate_limiter"`
        }`json:"data"`
}

type CreateTrlResponse struct {
	Data struct {
		Traffic_rate_limiter TrlInfo `json:"traffic_rate_limiter"`
	} `json:"data"`
	Message string `json:"message"`
}

type TRLPOLICY struct {
	Traffic_rate_limiter []TrlInfo `json:"traffic_rate_limiter"`
	Total_count          int       `json:"total_count"`
}

type GetTrlListResponse struct {
          Traffic_rate_limiter []TrlInfo `json:"traffic_rate_limiter"`
          Total_count int `json:"total_count"`
}

func (cs *Cloudstation) Get_TrlPolicyList() (bool, error, map[string]TrlInfo, map[int]TrlInfo) {
	var dummy struct {
	}

	key := "get_trlpolicy_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})
	trl_info_wrt_name := make(map[string]TrlInfo)
	trl_info_wrt_id   := make( map[int]TrlInfo)

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

	var trlpol_ls []TRLPOLICY
	var trlpol TRLPOLICY
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//fmt.Println("Status Get_TrlPolicyList of page 1= ", resp.StatusCode)
	if resp.StatusCode != 200 {
		return false, nil, trl_info_wrt_name, trl_info_wrt_id
	}
	err_trlpolicy := json.NewDecoder(resp.Body).Decode(&trlpol)
	if err_trlpolicy != nil {
		fmt.Println(err_trlpolicy)
	}
	trlpol_ls = append(trlpol_ls, trlpol)

	tot_entry := trlpol.Total_count

	for _, trlpol_indiv := range trlpol.Traffic_rate_limiter {
		trl_info_wrt_name[trlpol_indiv.Name] = trlpol_indiv
		trl_info_wrt_id[trlpol_indiv.Id] = trlpol_indiv
	}

	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}

	i := 2
	for i <= tot_page {
		var trlpol1 TRLPOLICY
		queryParam = map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		//fmt.Printf("Status Get_TrlPolicyList of page %d= %d\n", i, resp.StatusCode)
		if resp.StatusCode != 200 {
		   return false, nil, trl_info_wrt_name, trl_info_wrt_id
		}
		err_trlpolicy := json.NewDecoder(resp.Body).Decode(&trlpol1)
		if err_trlpolicy != nil {
			fmt.Println(err_trlpolicy)
		}
		for _, trlpol_indiv := range trlpol1.Traffic_rate_limiter {
			trl_info_wrt_name[trlpol_indiv.Name] = trlpol_indiv
			trl_info_wrt_id[trlpol_indiv.Id] = trlpol_indiv
		}
		trlpol_ls = append(trlpol_ls, trlpol1)
		i++
	}
	//fmt.Println("--------------Got into the Verse for Getting the TRL POLICY LIST !--------------")
	//fmt.Println()
        return true, nil, trl_info_wrt_name, trl_info_wrt_id
}


func (cs *Cloudstation) Get_TrlId(trl_name string, trl_wrt_name map[string]TrlInfo) (int, bool) {

	if trl_info, ok := trl_wrt_name[trl_name]; ok {
		return trl_info.Id, true
	} else {
		return 0, false
	}
	/*
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var id int
	var found_id bool
	for i := 0; i < len(trlpol_ls); i++ {
		for j := 0; j < 10; j++ {
			if trlpol_ls[i].Traffic_rate_limiter[j].Name == trl_name {
				id = trlpol_ls[i].Traffic_rate_limiter[j].Id
				found_id = true
				break
			}
		}
		if found_id == true {
			break
		}
	}
	if found_id == false {
		fmt.Println("No Id found !")
		return 0, false
	}
	return id, true
	*/
}

// For TRL POLICY attach and detach at network group

func (cs *Cloudstation) AttachTrlAtNwGrp(trl_name string, nwgrp_name string) bool {

	var trl_id int
	var err1 bool

	if res, _, trl_policy_wrt_name, _ := cs.Get_TrlPolicyList(); res {
	    trl_id, err1 = cs.Get_TrlId(trl_name, trl_policy_wrt_name)
	}

	//trl_id, err1 := cs.Get_TrlId(trl_name, x)
	if err1 == false {
		fmt.Println("Failed to fetch the Trl Id !")
		return false
	}

	nwgrphex_id, err2 := cs.Get_NwGrpHexId(nwgrp_name)
	if err2 == false {
		fmt.Println("Failed to fetch the NwGrp Hex Id !")
		return false
	}

	var attach_nwgrp_payload struct {
		Attached_to_id   string `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_nwgrp_payload.Attached_to_id = nwgrphex_id
	attach_nwgrp_payload.Attached_to_type = "NetworkGroup"

	key := "attach_trl_at_networkgrp"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(trl_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_nwgrp_payload, cookie)
	//fmt.Println("Status AttachTrlAtNwGrp= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for ATTACHING TRL AT NETWORK_GROUP LEVEL-------------")
	//fmt.Println()
	return true
}

func (cs *Cloudstation) DetachTrlAtNwGrp(trl_name string, nwgrp_name string) bool {

	var trl_id int
	var err1 bool

	if res, _, trl_policy_wrt_name, _ := cs.Get_TrlPolicyList(); res {
	    trl_id, err1 = cs.Get_TrlId(trl_name, trl_policy_wrt_name)
	}
	if err1 == false {
		fmt.Println("Failed to fetch the Trl Id !")
		return false
	}

	nwobject_id, err2 := cs.Get_NwGrpId(nwgrp_name)
	if err2 == false {
		fmt.Println("Failed to fetch the NwGrp Object Id !")
		return false
	}

	var detach_nwgrp_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_nwgrp_payload.Object_id = nwobject_id
	detach_nwgrp_payload.Object_type = "NetworkGroup"

	key := "detach_trl_at_networkgrp"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(trl_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_nwgrp_payload, cookie)
	//fmt.Println("Status DetachTrlAtNwGrp= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for DETACHING TRL AT NETWORK_GROUP LEVEL-------------")
	//fmt.Println()
	return true
}

// For TRL POLICY attach and detach at site

func (cs *Cloudstation) AttachTrlAtSite(trl_name string, site_name string) bool {

	var trl_id int
	var err1 bool

	if res, _, trl_policy_wrt_name, _ := cs.Get_TrlPolicyList(); res {
	    trl_id, err1 = cs.Get_TrlId(trl_name, trl_policy_wrt_name)
	}
	if err1 == false {
		fmt.Println("Failed to fetch the Trl Id !")
		return false
	}

	site_id, err2 := cs.Get_SiteId(site_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Site Id !")
		return false
	}

	var attach_site_payload struct {
		Attached_to_id   int    `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_site_payload.Attached_to_id = site_id
	attach_site_payload.Attached_to_type = "Site"

	key := "attach_trl_at_site"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(trl_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_site_payload, cookie)
	//fmt.Println("Status AttachTrlAtSite= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for ATTACHING TRL AT SITE LEVEL-------------")
	//fmt.Println()
	return true
}

func (cs *Cloudstation) DetachTrlAtSite(trl_name string, site_name string) bool {
	var trl_id int
	var err1 bool

	if res, _, trl_policy_wrt_name, _ := cs.Get_TrlPolicyList(); res {
	    trl_id, err1 = cs.Get_TrlId(trl_name, trl_policy_wrt_name)
	}
	if err1 == false {
		fmt.Println("Failed to fetch the Trl Id !")
		return false
	}

	siteobject_id, err2 := cs.Get_SiteId(site_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Site Object Id !")
		return false
	}

	var detach_site_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_site_payload.Object_id = siteobject_id
	detach_site_payload.Object_type = "Site"

	key := "detach_trl_at_site"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(trl_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_site_payload, cookie)
	//fmt.Println("Status DetachTrlAtSite= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for DETACHING TRL AT SITE LEVEL-------------")
	//fmt.Println()
	return true
}

// For TRL POLICY attach and detach at site profile

func (cs *Cloudstation) AttachTrlAtSiteProfile(trl_name string, profile_name string) bool {

	var trl_id int
	var err1 bool

	if res, _, trl_policy_wrt_name, _ := cs.Get_TrlPolicyList(); res {
	    trl_id, err1 = cs.Get_TrlId(trl_name, trl_policy_wrt_name)
	}
	if err1 == false {
		fmt.Println("Failed to fetch the Trl Id !")
		return false
	}

	profile_id, err2 := cs.Get_ProfileId(profile_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Profile Id !")
		return false
	}

	var attach_profile_payload struct {
		Attached_to_id   int    `json:"attached_to_id"`
		Attached_to_type string `json:"attached_to_type"`
	}

	attach_profile_payload.Attached_to_id = profile_id
	attach_profile_payload.Attached_to_type = "Profile"

	key := "attach_trl_at_profile"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(trl_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_profile_payload, cookie)
	//fmt.Println("Status AttachTrlAtSiteProfile= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for ATTACHING TRL AT SITE PROFILE LEVEL-------------")
	//fmt.Println()
	return true
}

func (cs *Cloudstation) DetachTrlAtSiteProfile(trl_name string, profile_name string) bool {

	var trl_id int
	var err1 bool

	if res, _, trl_policy_wrt_name, _ := cs.Get_TrlPolicyList(); res {
	    trl_id, err1 = cs.Get_TrlId(trl_name, trl_policy_wrt_name)
	}
	if err1 == false {
		fmt.Println("Failed to fetch the Trl Id !")
		return false
	}

	profileobject_id, err2 := cs.Get_ProfileId(profile_name)
	if err2 == false {
		fmt.Println("Failed to fetch the Profile Object Id !")
		return false
	}

	var detach_profile_payload struct {
		Object_id   int    `json:"object_id"`
		Object_type string `json:"object_type"`
	}

	detach_profile_payload.Object_id = profileobject_id
	detach_profile_payload.Object_type = "Profile"

	key := "detach_trl_at_profile"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(trl_id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_profile_payload, cookie)
	//fmt.Println("Status DetachTrlAtSiteProfile= ", resp.StatusCode)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return false
	}
	//fmt.Println("--------------Got into the Verse for DETACHING TRL AT SITE PROFILE LEVEL-------------")
	//fmt.Println()
	return true
}

/*
type TrlInfo struct {
                        Id int `json:"id"`
                        Name string `json:"name"`
                        Pol_type string `json:"pol_type"`
                        Pol_id int `json:"pol_id"`
                        //Rate int `json:"rate"`
                        Cust_id string `json:"customer_id"`
                        Creat_at string `json:"created_at"`
                        Updated_at string `json:"updated_at"`
                        Src_nw_group_id int `json:"src_network_group_id"`
                        Src_nw_group_name string `json:"src_network_group_name"`
                        Priority int `json:"priority"`
                        Direction string `json:"direction"`
                        Tag string `json:"tag"`
                        Dst_nw_group_id int `json:"dst_network_group_id"`
                        Dst_nw_group_name string `json:"dst_network_group_name"`
}

*/

type DeleteTrlPolicyResponse struct {
        Msg string `json:"message"`
        Total_count int `json:"total_count"`
        Data struct{
                Traffic_rate_limiter TrlInfo `json:"traffic_rate_limiter"`
        }`json:"data"`
}

func (cs *Cloudstation) DeleteTrlPolicy(trl_name string) (bool,*DeleteTrlPolicyResponse) {

        var delresp DeleteTrlPolicyResponse
        err1, _, res2,_ := cs.Get_TrlPolicyList()
        trl_res:=res2[trl_name]
        trl_id:=trl_res.Id
        //fmt.Println(trl_id)
        if err1 == false {
                fmt.Println("Failed to fetch the Trl Id !")
                return false,&delresp
        }

        var deletetrl_payload struct {
        }

        key := "delete_trl_policy"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"trl_policy_id": strconv.Itoa(trl_id)})
        //fmt.Println(substitutedPath)
        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, deletetrl_payload, cookie)

        //fmt.Println("Status DeleteTrlPolicy= ", resp.StatusCode)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false,&delresp
        }

        err_delresp:= json.NewDecoder(resp.Body).Decode(&delresp)
        if err_delresp != nil {
                fmt.Println(err_delresp)
         }
         return true,&delresp
        //fmt.Println("--------------Got into the Verse for DELETING TRL-------------")
        //fmt.Println()
        return true,&delresp
}



func (cs *Cloudstation) CreateTRLPolicy(trlinputs *CreateTrlPolicyInput) (bool, error, *CreateTrlResponse) {

        var TRLresponse CreateTrlResponse
        var trl_policy_payload struct {
                Name string `json:"name"`
                Net_grp_name *string `json:"network_group_name"`
                Owner_id *int `json:"owner_id"`
                Owner_type *string `json:"owner_type"`
                Pol_type string `json:"pol_type"`
                Priority int `json:"priority"`
                Qos_action *string `json:"qos_action"`
                Rate int `json:"rate"`
                Tag *string `json:"tag"`
                MATCHCRITERIA
        }
        trl_policy_payload.Name = trlinputs.Name
//        trl_policy_payload.Net_grp_name = trlinputs.Net_grp_name
 //       trl_policy_payload.Owner_id = trlinputs.Owner_id
   //     trl_policy_payload.Owner_type = trlinputs.Owner_type
        trl_policy_payload.Pol_type = trlinputs.Pol_type
        trl_policy_payload.Priority = trlinputs.Priority
    //    trl_policy_payload.Tag = trlinputs.Tag
    //    trl_policy_payload.Qos_action = trlinputs.Qos_action
        trl_policy_payload.Rate = trlinputs.Rate
        matchcriteria, _ := cs.GetMatchCriteria(trlinputs.MatchCriteriaInput)
        trl_policy_payload.MATCHCRITERIA = *matchcriteria
        //fmt.Println("************************", trl_policy_payload, "***************************")
        key := "create_trl_policy"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{})
        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, trl_policy_payload, cookie)
        //fmt.Println("Status Create_TRL_policy= ", resp.StatusCode)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false,  nil, &TRLresponse
        }

        err_create_resp:= json.NewDecoder(resp.Body).Decode(&TRLresponse)
        if err_create_resp != nil {
                log.Err.Println(err_create_resp)
         }
        return true,  nil, &TRLresponse
}
