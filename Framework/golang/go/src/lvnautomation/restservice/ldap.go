package restservice

import (
	"fmt"
	"net/http"
	//"errors"
	"encoding/json"
	//"regexp"
	"strconv"
	//"strings"
	//"bytes"
	//"compress/gzip"
	//"io"

	log "lvnautomation/lvnlog"
	//"time"
	tc_utils "lvnautomation/testcaseutils"
)

type CreateLdapResponse struct{
        Msg string `json:"message"`
        Total_count int `json:"total_count"`
        Data struct{
                Ldap_server struct{
                        Id int `json:"id"`
                        Name string `json:"name"`
                        Server_id string `json:"server_id"`
                        Server_port int `json:"server_port"`
                        Cmi string `json:"common_name_identifier"`
                        Dist_name string `json:"distinguished_name"`
                        Bind_type string `json:"bind_type"`
                        En_sec_conn bool `json:"enable_secure_connection"`
                        Cust_id int `json:"customer_id"`
                        Creat_at string `json:"created_at"`
                        Updated_at string `json:"updated_at"`
                        Enable bool `json:"enable"`
                        Admin_usrname string `json:"admin_username"`
                        Admin_pw string `json:"admin_password"`
                        Ldap_status string `json:"ldap_status"`
                        Last_check string `json:"last_checked"`
                }`json:"ldap_server"`
        }`json:"data"`
}

func (cs *Cloudstation) CreateLdap(LdapObj LdapInput) (bool,*CreateLdapResponse){
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        var createldapresp CreateLdapResponse
        var CreateLdapStruct struct{
               Admin_pw string `json:"admin_password"`
                Admin_usrname string `json:"admin_username"`
                Bind_type string `json:"bind_type"`
                Cmi string `json:"common_name_identifier"`
                Dist_name string `json:"distinguished_name"`
                Enable bool `json:"enable"`
                En_sec_conn bool `json:"enable_secure_connection"`
                Ldap_name string `json:"name"`
                Server_ip string `json:"server_ip"`
                Server_port int `json:"server_port"`
        }
        CreateLdapStruct.Admin_pw = LdapObj.Admin_pw
        CreateLdapStruct.Admin_usrname = LdapObj.Admin_usrname
        CreateLdapStruct.Bind_type = LdapObj.Bind_type
        CreateLdapStruct.Cmi = LdapObj.Cmi
        CreateLdapStruct.Dist_name = LdapObj.Dist_name
        CreateLdapStruct.Enable = LdapObj.Enable
        CreateLdapStruct.En_sec_conn = LdapObj.En_sec_conn
        CreateLdapStruct.Ldap_name = LdapObj.Ldap_name
        CreateLdapStruct.Server_ip = LdapObj.Server_ip
        CreateLdapStruct.Server_port = LdapObj.Server_port
    key := "create_ldap"
    apis := getApis()
    api :=apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{})

    header := cs.Construct_headers(apis[key].headers.headers)
    //fmt.Println(header)
    var resp *http.Response
    cookie := []*http.Cookie{cs.Cookies}
    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, CreateLdapStruct, cookie)

    if !cs.Status_CS(resp.StatusCode) {
                return false,&createldapresp
        }

        if resp.StatusCode == 401 {
                resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, CreateLdapStruct, cookie)
        }

        if resp.StatusCode != 200 {
                return false,&createldapresp
        }

    err_createldapresp:= json.NewDecoder(resp.Body).Decode(&createldapresp)
    if err_createldapresp != nil {
               log.Err.Println(err_createldapresp)
         }
        if ok, json_data := convert_struct_to_string_via_json(createldapresp); ok {
            log.Debug.Printf("response data: %s\n", json_data)
        }
        return true,&createldapresp
}


type LdapInfo struct {
                        Id int `json:"id"`
                        Name string `json:"name"`
                        Server_ip string `json:"server_ip"`
                        Server_port int `json:"server_port"`
                        Cmi string `json:"common_name_identifier"`
                        Dist_name string `json:"distinguished_name"`
                        Bind_type string `json:"bind_type"`
                        En_sec_conn bool `json:"enable_secure_connection"`
                        Cust_id int `json:"customer_id"`
                        Creat_at string `json:"created_at"`
                        Updated_at string `json:"updated_at"`
                        Enable bool `json:"enable"`
                        Admin_usrname string `json:"admin_username"`
                        Admin_pw string `json:"admin_password"`
                        Ldap_status string `json:"ldap_status"`
                        Last_check string `json:"last_checked"`

}


type GetLdapListResponse struct {
        Msg string `json:"message"`
        Total_count int `json:"total_count"`
        Data struct{
                Ldap_servers []LdapInfo `json:"ldap_servers"`
        }`json:"data"`
}

func (cs *Cloudstation) GetLdapList() (bool, error, map[int]LdapInfo, map[string]LdapInfo) {

//        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        //log.Debug.Printf("FUNC: %s exit\n", tc_utils.FuncName())
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        var dummy struct {
        }
        key := "get_ldap_list"
        apis := getApis()
         api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{})
        header := cs.Construct_headers(apis[key].headers.headers)
        //fmt.Println(header)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        queryParam := map[string]interface{}{
                "key":      "",
                "ldap_server_type":  1,
                "page":     10,
                "per_page": "",
        }
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
        //resp, _ = http.Post(fmt.Sprintf("%s/%s", cs.Base_url, substitutedPath),  )
        //fmt.Println(err)
        //var LdapList []LdapInfoList
        //var SingleLdap LdapInfoList
        var get_ldap_list_response GetLdapListResponse
        ldap_info := make(map[int]LdapInfo)
        ldap_info_wrt_name := make(map[string]LdapInfo)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false, nil, ldap_info, ldap_info_wrt_name
        }

        err_ldap := json.NewDecoder(resp.Body).Decode(&get_ldap_list_response)
        if err_ldap != nil {
                log.Err.Println(err_ldap)
        }
        if ok, json_data := convert_struct_to_string_via_json(get_ldap_list_response); ok {
            log.Debug.Printf("response data: %s\n", json_data)
        }
        tot_entry := get_ldap_list_response.Total_count
       // log.Debug.Printf("    Total Site Count: %d\n", tot_entry)
        //SiteList = append(LdapList, LdapSite)
//      fmt.Println(tot_entry)
//      fmt.Println(get_ldap_list_response)
        for _, ldap := range get_ldap_list_response.Data.Ldap_servers {
                //fmt.Println(ldap.Name)
                 ldap_info_wrt_name[ldap.Name] = ldap
            ldap_info[ldap.Id] = ldap
        }
        tot_page := 0
        if tot_entry%10 == 0 {
                tot_page = tot_entry / 10
        } else {
                tot_page = (tot_entry / 10) + 1
        }
        i:=2
        for i <= tot_page {
                //var SingleLdap1 LdapInfoList
                queryParam = map[string]interface{}{
                        "key":      "",
                        "ldap_server_type":  1,
                        "page":     i,
                        "per_page": "",
                }

                resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
                if resp.StatusCode != 200 {

                //      return ldap_info, false
                return false, nil, ldap_info, ldap_info_wrt_name
                }
                err_ldap = json.NewDecoder(resp.Body).Decode(&get_ldap_list_response)
                if err_ldap != nil {
                        log.Err.Println(err_ldap)
                }
        if ok, json_data := convert_struct_to_string_via_json(get_ldap_list_response); ok {
            log.Debug.Printf("response data: %s\n", json_data)
        }
                //fmt.Println(SingleLdap)
                //SiteList = append(LdapList, SingleLdap1)
                for _, ldap := range get_ldap_list_response.Data.Ldap_servers {
                         ldap_info[ldap.Id] = ldap
                         ldap_info_wrt_name[ldap.Name] = ldap
                }
                //fmt.Println(LdapList)
                i++
        }

        //fmt.Println(LdapList)
        //fmt.Println("--------------Got into the Verse for Getting the LDAP List !--------------")
        //return ldap_info, true

        //fmt.Println(ldap_info)
        //fmt.Println(ldap_info_wrt_name)*/
        return true, nil, ldap_info, ldap_info_wrt_name
}

func (cs *Cloudstation) DeleteLdapConfig(ldap_name string) (bool,*CreateLdapResponse) {

        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        var delresp CreateLdapResponse
        //var ldap_info LdapInfo
        var ldap_id int
        err1, _,_,res2 := cs.GetLdapList()
        if err1 == false {
                log.Err.Println("Failed to fetch the Ldap Id !")
                return false,&delresp
        }

        if ldap_info, ok := res2[ldap_name]; ok {
                ldap_id = ldap_info.Id
        } else {
                return false,&delresp
        }
        //ldap_res:=res2[ldap_name]
        //ldap_id:=ldap_res.Id

        var deleteldap_payload struct {
        }

        key := "delete_ldap"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom1_id": strconv.Itoa(ldap_id)})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, deleteldap_payload, cookie)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false,&delresp
        }
        err_delresp:= json.NewDecoder(resp.Body).Decode(&delresp)
        if err_delresp != nil {
                fmt.Println(err_delresp)
         }
        if ok, json_data := convert_struct_to_string_via_json(delresp); ok {
            log.Debug.Printf("response data: %s\n", json_data)
        }
        return true,&delresp
}

