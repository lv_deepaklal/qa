package restservice

import (
	"errors"
	"fmt"
	"net/http"
	"encoding/json"
	log "lvnautomation/lvnlog"
	//tc_utils "lvnautomation/testcaseutils"
)

type GetNwGrpInfoReponseNwGrp struct {
	CreatedAt string `json:"created_at"`
	CustomerId int `json:"customer_id"`
	Encapsulation string `json:"encapsulation"`
	HexId string `json:"hex_id"`
	AllowInternet string `json:"ib_enabled"`
	Id int `json:"id"`
	Location string `json:"location"`
	N_Id int `json:"n_id"`
	Name string `json:"name"`
	NextHop string `json:"next_hop"`
	SecuredData string `json:"secured_data"`
	SecurityProfileId int `json:"security_profile_id"`
	Tag string `json:"tag"`
	UpdatedAt string `json:"updated_at"`
	UserId int `json:"user_id"`
	Vpn string `json:"vpn"`
	VpnProfileId *int `json:"vpn_profile_id"`
}

type GetNwGrpInfoResponseLanSubnets struct {
    Id int `json:"id"`
    SiteId int `json:"site_id"`
    Subnet string `json:"subnet"`
    SubnetName string `json:"subnet_name"`
    SubnetPrefixLength int `json:"subnet_prefix_length"`
    Vlan int `json:"vlan"`
}

type UpdateNwGroup struct {
    GetNwGrpInfoReponseNwGrp
    Subnets []int `json:"subnets"`
}

/*
type VpnProfile struct {
    AntiReplay bool `json:"anti_replay"`
    AuInterval *int `json:"auinterval"`
    AuthType string `json:"auth_type"`
    CreatedAt string `json:"created_at"`
    CustomerId int `json:"customer_id"`
    DataIntegrity string `json:"data_integrity"`
    Description *string `json:"description"`
    EncryptionMode string `json:"encryption_mode"`
    EncryptionStandard string `json:"encryption_standard"`
    HexId string `json:"hex_id"`
    Id int `json:"id"`
    Name string `json:"name"`
    Refresh string `json:"refresh"`
    RefreshInterval int `json:"refresh_interval"`
    Tag *string `json:"tag"`
    UpdatedAt string `json:"updated_at"`
    UserId int `json:"user_id"`
    Windowsize string `json:"window_size"`
}
*/
type GetNwGrpInfoResponse struct {
    Available_lan_subnets []GetNwGrpInfoResponseLanSubnets `json:"available_lan_subnets"`
    Nw_grp GetNwGrpInfoReponseNwGrp `json:"network_group"`
    // selected_lan_subnets value will have ticked: true, but ignoring since we dont require it
    Selected_lan_subnets []GetNwGrpInfoResponseLanSubnets `json:"selected_lan_subnets"`
    Vpn_profiles []VpnProfile `json:"vpn_profiles"`
}

func (g *GetNwGrpInfoResponse) SelectedLansubnets() []GetNwGrpInfoResponseLanSubnets {
	return g.Selected_lan_subnets
	//return []GetNwGrpInfoResponseLanSubnets{}
}

func (g *GetNwGrpInfoResponse) GetNwGrpInfo() GetNwGrpInfoReponseNwGrp {
	return g.Nw_grp
	//return GetNwGrpInfoReponseNwGrp{}
}

func (g *GetNwGrpInfoReponseNwGrp) GetEncapsulation() string {
	return g.Encapsulation
}

func (g *GetNwGrpInfoReponseNwGrp) IsInternetAllowed() bool {
	return g.AllowInternet == "true"
}

func (g *GetNwGrpInfoReponseNwGrp) IsEncapsulationUdp() bool {
	return g.Encapsulation == "lntun"
}

func (g *GetNwGrpInfoReponseNwGrp) IsNextHopHub() bool {
	return g.NextHop == "hub"
}

func (g *GetNwGrpInfoReponseNwGrp) IsVPNEnabled() bool {
	return g.SecuredData == "true"
}

func (g *GetNwGrpInfoReponseNwGrp) GetVpnProfileId() int {
    return *g.VpnProfileId
}

func (cs *Cloudstation) FetchNwGrpInfoByHexId(nw_grp_hexid string) (bool, *GetNwGrpInfoResponse) {
	//get api/v1/lavelle/network_groups/be0569b5d78c73424d3063dc7b02aa34
        var dummy struct {
        }
	var nwgrp GetNwGrpInfoResponse

        key := "get_networkgroup_info_by_hexid"
        apis := getApis()
        api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"nw_grp_hexid": nw_grp_hexid})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{}

        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
        if resp.StatusCode != 200 {
                return false, &nwgrp
        }

        err_nwgrp := json.NewDecoder(resp.Body).Decode(&nwgrp)
        if err_nwgrp != nil {
                fmt.Println(err_nwgrp)
        }
	//fmt.Println("hwfrp_by-hexid", nwgrp)
        if ok, json_data := convert_struct_to_string_via_json(nwgrp); ok {
            log.Debug.Printf("response data: %s\n", json_data)
        }
	return true, &nwgrp
}

func (cs *Cloudstation) FetchNwGrpInfo(nw_grp_name string) (bool, *GetNwGrpInfoResponse) {

    var nw_grp_hexid string
    var res bool
    if nw_grp_hexid, res = cs.Get_NwGrpHexId(nw_grp_name); !res {
	    return false, nil
    }
    return cs.FetchNwGrpInfoByHexId(nw_grp_hexid)
}

func (cs *Cloudstation) UpdateNetworkGroup(update_nw_grp *UpdateNwGrpInput) (bool, error) {

    var res bool

    var nw_grp_name, nw_grp_hexid string
    var nw_grp_id int
    var get_nw_grp_info_response *GetNwGrpInfoResponse
    var set_nw_grp_info GetNwGrpInfoReponseNwGrp

    if update_nw_grp  == nil {
	    return false, errors.New(fmt.Sprintf("Input provided for update nw group is nil"))
    }

    nw_grp_name = update_nw_grp.Name
    //put pi/v1/lavelle/network_groups/<nw_grp_id>
    nw_grp_id, res = cs.Get_NwGrpId(nw_grp_name)

    if !res {
	    return false, errors.New(fmt.Sprintf("Failed to fetch the ID for NW group %s", nw_grp_name))
    }

    nw_grp_hexid, res = cs.Get_NwGrpHexId(nw_grp_name)
    if !res {
	    return false, errors.New(fmt.Sprintf("Failed to fetch the Hex ID for NW group %s", nw_grp_name))
    }

    res, get_nw_grp_info_response = cs.FetchNwGrpInfoByHexId(nw_grp_hexid)
    set_nw_grp_info = get_nw_grp_info_response.Nw_grp

    set_nw_grp_info.Encapsulation = update_nw_grp.encapsulation
    set_nw_grp_info.NextHop = update_nw_grp.nextHop
    set_nw_grp_info.SecuredData = update_nw_grp.secureData
    set_nw_grp_info.AllowInternet = update_nw_grp.allowInternetAccess

    var vpn_id int

    if set_nw_grp_info.SecuredData == "true" {
        res, vpn_id = cs.GetVpnProfileId(update_nw_grp.encryption_profile_name)
	if !res {
		return false, errors.New(fmt.Sprintf("Failed to get VPN ID for %s", update_nw_grp.encryption_profile_name))
	}
	set_nw_grp_info.VpnProfileId = &vpn_id
    }

    res, set_nw_grp_info.SecurityProfileId = cs.GetSecurityProfileId(update_nw_grp.threatProtectionProfile)
    if !res {
	    return false, errors.New(fmt.Sprintf("Failed to get Threat profile id for %s", update_nw_grp.threatProtectionProfile))
    }

    subnets := get_nw_grp_info_response.Available_lan_subnets
    subnet_info_map := make(map[string]GetNwGrpInfoResponseLanSubnets)

    subnet_list := []int{}
    selected_subnet_list_all := []int{}

    for _, subnet := range subnets {
        subnet_info_map[subnet.SubnetName] =  subnet
	selected_subnet_list_all = append(selected_subnet_list_all, subnet.Id)
    }

    //fmt.Println("subnet_info_map", subnet_info_map)

    if len(update_nw_grp.subnetList) == 0 {
        update_nw_grp.subnet_decoder = "all"
    }

    //fmt.Println("update_nw_grp.subnet_decoder", update_nw_grp.subnet_decoder)
    //fmt.Println("update_nw_grp.subnetList", update_nw_grp.subnetList)
    if update_nw_grp.subnet_decoder == "specific" {
        for _, selected_subnet := range update_nw_grp.subnetList {
	    if x, ok := subnet_info_map[selected_subnet]; ok {
		subnet_list = append(subnet_list, x.Id)
	    }
	}
    } else {
	    subnet_list = selected_subnet_list_all
    }

    //fmt.Println("subnet_list", subnet_list)
    // put api/v1/lavelle/network_groups/1
    key := "update_nw_grp"
    apis := getApis()
    api := apis[key]
    substitutedPath := substitutePath(apis[key], map[string]string{"nw_grp_id": fmt.Sprintf("%d", nw_grp_id)})

    header := cs.Construct_headers(apis[key].headers.headers)
    var resp *http.Response
    cookie := []*http.Cookie{cs.Cookies}
    queryParam := map[string]interface{}{}
    payload := UpdateNwGroup{GetNwGrpInfoReponseNwGrp: set_nw_grp_info, Subnets: subnet_list}

    resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, payload, cookie)
    if resp.StatusCode != 200 {
        return false,  errors.New(fmt.Sprintf("Response code %d", resp.StatusCode))
    }

    return true, nil
}
