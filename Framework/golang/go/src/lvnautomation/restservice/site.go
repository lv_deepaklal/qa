package restservice

import (
	"fmt"
	"net/http"
	"errors"
	"encoding/json"
	//"regexp"
	//"strconv"
	//"strings"
	//"bytes"
	//"compress/gzip"
	//"io"

	log "lvnautomation/lvnlog"
	//"time"
	tc_utils "lvnautomation/testcaseutils"
)

type CreateSiteResponse struct {
        Message string `json:"message"`
        Site SiteInfo  `json:"site"`
}

type UpdateSiteResponse struct {
    Message string `json:"message"`
    Site SiteInfo `json:"site"`
}

type UpdateSiteInfo struct {
    SiteInfo
    InetWONat *bool `json:"internet_without_nat"`
}

func (cs *Cloudstation) CreateSite(Site *CreateSiteInput) (error, bool, *CreateSiteResponse) {

        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        var placeinfo map_place
        var create_site_response CreateSiteResponse
        var createSitePayload struct {
                Name                 string    `json:"name"`
                Location             string    `json:"location"`
                Tags                 string    `json:"tags"`
                Is_hub               bool      `json:"is_hub"`
                Profile_id           string    `json:"profile_id"`
                User_id              int       `json:"user_id"`
                Enable_mac_filtering bool      `json:"enable_mac_filtering"`
                Dns_proxy_id         string    `json:"dns_proxy_id"`
                Dns_proxy_enable     bool      `json:"dns_proxy_enable"`
                Snmp_mapping_id      string    `json:"snmp_mapping_id"`
                Snmp_mapping_enable  bool      `json:"snmp_config_enable"`
                Auto_upgrade         bool      `json:"auto_upgrade"`
                Place                map_place `json:"place"`
                Place_id             string    `json:"place_id"`
        }

        createSitePayload.Name = Site.Name
        createSitePayload.Location = "Bangalore, Karnataka, India"
        createSitePayload.Tags = ""
        createSitePayload.Is_hub = Site.Is_hub
        createSitePayload.Profile_id = ""
        createSitePayload.Dns_proxy_id = ""
        createSitePayload.Dns_proxy_enable = false
        createSitePayload.Snmp_mapping_id = ""
        createSitePayload.Snmp_mapping_enable = false
        createSitePayload.Auto_upgrade = false
        createSitePayload.Place = placeinfo
        createSitePayload.Place_id = "ChIJbU60yXAWrjsR4E9-UejD3_g"
        isFound, errList, userId := cs.GetUserId(Site.User)
        if isFound == false {
                return errList, false, &create_site_response
        }
        createSitePayload.User_id = userId
        key := "create_site"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{})
        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        queryParam := map[string]interface{}{}
        //payload_str, marshal_err := json.Marshal(createSitePayload)

        //if marshal_err == nil {
        //    log.Debug.Printf("create site payload: %s\n", payload_str)
        //} else {
        //    log.Debug.Printf("create site payload: %s\n", "Marshalling failed")
        //}

        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, createSitePayload, cookie)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return nil, false, &create_site_response
        }

       log.Debug.Printf("Created site1\n")
        err_site := json.NewDecoder(resp.Body).Decode(&create_site_response)
        if err_site != nil {
                fmt.Println(err_site)
        }
       log.Debug.Printf("Created site2, %d %s\n", create_site_response.Site.Id, create_site_response.Site.Name)

       //if ok, json_data := convert_struct_to_string_via_json(create_site_response); ok {
          //  log.Debug.Printf("response data: %s\n", json_data)
       // }
       log.Debug.Printf("//cs.Helper.site.byId %s\n", cs.Helper.site.byId)
       log.Debug.Printf("//cs.Helper.site.byId %s\n", cs.Helper.site.byName)
       cs.Helper.site.byId[create_site_response.Site.Id] = create_site_response.Site
       cs.Helper.site.byName[create_site_response.Site.Name] = create_site_response.Site
       log.Debug.Printf("Created site\n")

        return nil, true, &create_site_response
}


type SiteInfo struct {
                AutoUpgrade          bool   `json:"auto_upgrade"`
                CloudportUrlConfig   string `json:"cloudport_url_config"`
                CreatedAt            string `json:"created_at"`
                CustomerdID          int    `json:"customer_id"`
                Dns_proxy_enable     bool   `json:"dns_proxy_enable"`
                Dns_proxy_id         int    `json:"dns_proxy_id"`
                Enable_mac_filtering bool   `json:"enable_mac_filtering"`
                HexId                string `json:"hex_id"`
                Id                   int    `json:"id"`
                Is_hub               bool   `json:"is_hub"`
                Latitude             string `json:"latitude"`
                location             string `json:"location"`
                Longitude            string `josn:"longitude"`
                Name                 string `json:"name"`
                PlaceId              string `json:"place_id"`
                ProfileId            int    `json:"profile_id"`
                Snmp_mapping_enable  int    `json:"snmp_config_enable"`
                Snmp_mapping_id      string `json:"snmp_mapping_id"`
                Tags                 string `json:"tags"`
                UpdatedAt           string `json:"updated_at"`
                UrlFiltering         bool   `json:"url_filtering"`
                UrlProfileId         int    `json:"url_profile_id"`
                UserId               int    `json:"user_id"`
}


type GetSiteListResponse struct {
        Site []SiteInfo `json:"sites"`
        TotalCount int `json:"total_count"`
}


func (cs *Cloudstation) SiteList() (bool, error, map[int]SiteInfo, map[string]SiteInfo) {

        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        //log.Debug.Printf("FUNC: %s exit\n", tc_utils.FuncName())
        var dummy struct {
        }
        key := "get_site_list"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{})
        header := cs.Construct_headers(apis[key].headers.headers)
        //fmt.Println(header)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        queryParam := map[string]interface{}{
                "key":      "",
                "page":     1,
                "per_page": 10,
                "sort":     "",
                "user": map[string]interface{}{
                        "username":             cs.Username,
                        "id":                   cs.Id,
                        "role":                 cs.Role,
                        "allow_manage":         cs.AllowManage,
                        "customer_id":          cs.CustomerId,
                        "hex_id":               cs.HexId,
                        "customer_hex_id":      cs.CustomerHexId,
                        "enable_notifications": cs.EnableNotifications,
                },
        }

        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
        var get_site_list_response GetSiteListResponse
        site_info := make(map[int]SiteInfo)
        site_info_wrt_name := make(map[string]SiteInfo)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false, nil, site_info, site_info_wrt_name
        }

        err_site := json.NewDecoder(resp.Body).Decode(&get_site_list_response)
        if err_site != nil {
                log.Err.Println(err_site)
        }
        //if ok, json_data := convert_struct_to_string_via_json(get_site_list_response); ok {
          //  log.Debug.Printf("response data: %s\n", json_data)
        //}
        tot_entry := get_site_list_response.TotalCount
        log.Debug.Printf("    Total Site Count: %d\n", tot_entry)

        //SiteList = append(SiteList, SingleSite)
        for _, site := range get_site_list_response.Site {
            site_info_wrt_name[site.Name] = site
            site_info[site.Id] = site
        }
        tot_page := 0
        if tot_entry%10 == 0 {
                tot_page = tot_entry / 10
        } else {
                tot_page = (tot_entry / 10) + 1
        }
        i := 2
        for i <= tot_page {
                //var SingleSite1 SiteInfoList
                queryParam = map[string]interface{}{
                        "key":      "",
                        "page":     i,
                        "per_page": 10,
                        "sort":     "",
                        "user": map[string]interface{}{
                                "username":             cs.Username,
                                "id":                   cs.Id,
                                "role":                 cs.Role,
                                "allow_manage":         cs.AllowManage,
                                "customer_id":          cs.CustomerId,
                                "hex_id":               cs.HexId,
                                "customer_hex_id":      cs.CustomerHexId,
                                "enable_notifications": cs.EnableNotifications,
                        },
                }

                resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
                if resp.StatusCode != 200 {

                //      return site_info, false
                return false, nil, site_info, site_info_wrt_name
                }
                err_site = json.NewDecoder(resp.Body).Decode(&get_site_list_response)
                if err_site != nil {
                        log.Err.Println(err_site)
                }
        //if ok, json_data := convert_struct_to_string_via_json(get_site_list_response); ok {
         //   log.Debug.Printf("response data: %s\n", json_data)
        //}
                //fmt.Println(SingleSite)
                //SiteList = append(SiteList, SingleSite1)
                for _, site := range get_site_list_response.Site {
                    site_info[site.Id] = site
                    site_info_wrt_name[site.Name] = site
                }
                //fmt.Println(SiteList)
                i++
        }

        //fmt.Println(SiteList)
        //fmt.Println("--------------Got into the Verse for Getting the CP List !--------------")
        //return site_info, true

        //fmt.Println(site_info)
        //fmt.Println(site_info_wrt_name)
        return true, nil, site_info, site_info_wrt_name
}

func (cs *Cloudstation) GetSiteID(SiteName string) (bool, error, int) {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        if siteinfo, ok := cs.SitesByName()[SiteName]; ok {
                return true, nil, siteinfo.Id
        }
        return false, nil, 0
}

func (cs *Cloudstation) UpdateSite(site_info_payload UpdateSiteInfo) (bool, error, *UpdateSiteResponse) {

    key := "update_site"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"site_id": fmt.Sprintf("%d", site_info_payload.Id)})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	var update_site_response UpdateSiteResponse
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, site_info_payload, cookie)

	if resp.StatusCode != 200 {
		return false, errors.New(fmt.Sprintf("Response code %d", resp.StatusCode)), &update_site_response
	}
    err_site := json.NewDecoder(resp.Body).Decode(&update_site_response)
	if err_site != nil {
		log.Err.Println(err_site)
	}

	if ok, json_data := convert_struct_to_string_via_json(update_site_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}

	return true, nil, &update_site_response
}

func (cs *Cloudstation) DeleteSite(site_name string) (error, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}
	key := "delete_site"
	apis := getApis()
	api := apis[key]
	site_info := cs.SitesByName()[site_name]
	substitutedPath := substitutePath(apis[key], map[string]string{"site_id": fmt.Sprintf("%d", site_info.Id)})
        header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	//fmt.Println(resp)
	if resp.StatusCode != 200 {
		return nil, false
	}

	return nil, true
}

