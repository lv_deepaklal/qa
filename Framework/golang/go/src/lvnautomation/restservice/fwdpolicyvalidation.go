package restservice

import (
	"strings"
	"time"
)

type CreateFwdInputs struct {
        Name string
        Priority int
        Output_direction string
        action []CreateFwdPolicyAction
        Tag string
        MatchCriteriaInput
}

type CreateFwdPolicyAction struct {
        wan_type string
        encapsulation string
        next_hop string
        custom_ip string
        site string
        gre_key string
        gre_keepalive bool
	secure_data string
	encryption_profile string
}


func (fwd *CreateFwdInputs) CheckFwdAction (wan_type string, encapsulation string, next_hop string) bool {

    wan_type_res :=  Contains([]string{"mpls", "broadband", "any"}, strings.ToLower(wan_type))
    encap_res := Contains([]string{"udp tunnel", "ipsec", "gre", "none"}, strings.ToLower(encapsulation))
    hop_res := Contains([]string{"resolve", "site", "hub", "custom", "local", "local", "local breakout"}, strings.ToLower(next_hop))
    return  wan_type_res && encap_res && hop_res
}

func CheckforUdpTunnel(encap string) (string, bool){
        if encap == "udp tunnel" {
                //encap = "lntun"
                return "lntun", true
        } else {
                return "", false
        }
}

func(fwd *CreateFwdInputs) CheckValidFwdCombinations(encapsulation string, next_hop string, site string, custom_ip string, gre_key string, gre_keepalive bool) (bool){
         res, _ := CheckforUdpTunnel(encapsulation)
    if(res == "lntun") {
        if(next_hop == "site" && site == " "){
            return false
        } else if (next_hop == "custom" && custom_ip  == " ") {
            return false
        } else {
           return  true
        }
    }
   if(encapsulation == "gre") {
        if(next_hop == "site" && site == " "){
            return false
        } else if (next_hop == "custom" && custom_ip  == " " && gre_key == " ") {
            return false
        } else {
           return  true
        }
    }
    //if(encapsulation == "ipsec") {
//          if(
        return true
}

func(fwd *CreateFwdInputs) AddFwdpolicyAction(a *CreateFwdPolicyAction) {
	fwd.action = append(fwd.action, *a)
}

func NewCreateFwdPolicyAction() *CreateFwdPolicyAction {
	var f CreateFwdPolicyAction
	return &f
}

func (f *CreateFwdPolicyAction) SetWantype(wan_type string) bool {

    if Contains([]string{"mpls", "internet", "any"}, strings.ToLower(wan_type)) {
	    f.wan_type = strings.ToLower(wan_type)
	    return true
    }
    return false
}

func (f *CreateFwdPolicyAction) SetWantypeMpls() bool {
	return f.SetWantype("mpls")
}

func (f *CreateFwdPolicyAction) SetWantypeBroadband() bool {
	return f.SetWantype("internet")
}

func (f *CreateFwdPolicyAction) SetWantypeAny() bool {
	return f.SetWantype("any")
}

func (f *CreateFwdPolicyAction) SetEncapsulation(encapsulation string) bool  {
    if Contains([]string{"lntun", "ipsec", "gre", "none"}, strings.ToLower(encapsulation)) {
	    f.encapsulation = strings.ToLower(encapsulation)
	    return true
    }
    return false
}

func (f *CreateFwdPolicyAction) SetEncapsUdptunnel() bool {
	return f.SetEncapsulation("lntun")
}

func (f *CreateFwdPolicyAction) SetEncapsIpsec(encryption_profile string) bool {
	if strings.ToLower(f.next_hop) == "custom" {
		return false
	}
	if f.SetEncapsulation("ipsec") {
		return f.SetEncryptionProfile(encryption_profile)
	}
	return false
}

func (f *CreateFwdPolicyAction) SetEncapsGre() bool {
	f.secure_data = ""
	f.encryption_profile = ""
	return f.SetEncapsulation("gre")
}
func (f *CreateFwdPolicyAction) SetEncapsNone() bool {
	f.secure_data = ""
	f.encryption_profile = ""
	return f.SetEncapsulation("none")
}

func (f *CreateFwdPolicyAction) SetNextHop(next_hop string) bool {
    if Contains([]string{"resolve", "site", "hub", "custom", "local", "local-breakout"}, strings.ToLower(next_hop)) {
	    f.next_hop = strings.ToLower(next_hop)
	    return true
    }
    return false
}

func (f *CreateFwdPolicyAction) SetNextHopResolve() bool {

	f.custom_ip = ""
	f.gre_key = ""
	f.gre_keepalive = false
	f.site = ""
	if strings.ToLower(f.encapsulation) == "ipsec" {
		// resovle with ipsec not supoted
		return false
	}
	return f.SetNextHop("resolve")
}

func (f *CreateFwdPolicyAction) SetNextHopSite(site_name string) bool {
	if f.SetNextHop("site") {
		f.custom_ip = ""
		f.gre_key = ""
		f.gre_keepalive = false
             f.site =  strings.ToUpper(site_name)
	     return true
	}
	return false
}

func (f *CreateFwdPolicyAction) SetNextHopLocal() bool {
	f.custom_ip = ""
	f.gre_key = ""
	f.gre_keepalive = false
	f.site = ""
	return f.SetNextHop("local")
}

func (f *CreateFwdPolicyAction) SetNextHopLocalBreakout() bool {
	f.custom_ip = ""
	f.gre_key = ""
	f.gre_keepalive = false
	f.site = ""
	return f.SetNextHop("local-breakout")
}

func (f *CreateFwdPolicyAction) SetNextHopHub() bool {
	f.custom_ip = ""
	f.gre_key = ""
	f.gre_keepalive = false
	f.site = ""
	return f.SetNextHop("hub")
}

func (f *CreateFwdPolicyAction) SetNextHopCustomIp(ip string) bool {
	if f.SetNextHop("custom") {
	        f.site = ""
		if strings.ToLower(f.encapsulation) == "gre" {
		} else {
	            f.gre_key = ""
	            f.gre_keepalive = false
		}
		f.custom_ip = ip
		return true
	}
	return false
}

func (f *CreateFwdPolicyAction) SetGreKey(key string) bool {
	if f.SetNextHop("custom")  && strings.ToLower(f.encapsulation) == "gre" {
		f.gre_key = strings.ToUpper(key)
		return true
	}
	return false
}

func (f *CreateFwdPolicyAction) SetGreKeepAlive(keepalive bool) bool {
	if f.SetNextHop("custom")  && strings.ToLower(f.encapsulation) == "gre" {
		f.gre_keepalive = keepalive
		return false
	}
	return false
}

func (f *CreateFwdPolicyAction) SetEncryptionProfile(encryption_profile string) bool {

	if strings.ToLower(f.encapsulation) == "none" || strings.ToLower(f.encapsulation) == "gre" {
		return false
	}
	f.secure_data = "true"
	f.encryption_profile = encryption_profile
	return true
}

func (fwd *CreateFwdInputs) SetFwdActions(wan_type string, encapsulation string, next_hop string, custom_ip string, gre_key string, site string, gre_keepalive bool) bool {
        y := CreateFwdPolicyAction{
                                                wan_type: strings.ToLower(wan_type),
                                                encapsulation: strings.ToUpper(encapsulation),
                                                next_hop: strings.ToLower(next_hop),
                                                custom_ip: strings.ToUpper(custom_ip),
                                                site: strings.ToUpper(site),
                                                gre_key: strings.ToUpper(gre_key),
                                                gre_keepalive: gre_keepalive,
                                        }
    action := fwd.CheckFwdAction(wan_type,encapsulation,next_hop)
    combination := fwd.CheckValidFwdCombinations(encapsulation,next_hop,custom_ip,gre_key,site,gre_keepalive)
    if action==true && combination == true {
        fwd.action = append(fwd.action, y)
        return true
    }
    return false
}

func NewFwdInput() (bool, error, *CreateFwdInputs) {
    var fwdInput CreateFwdInputs
    var name string = "lvn_auto_" + time.Now().Format("2006-01-02-15-04-05")
    fwdInput.SetPriority(0)
    fwdInput.SetName(name)
    fwdInput.SetFwdActions("mpls","udp", "gre", "custom", "50.6.7.8", "6001", false)
    _, _, matchCriteria := NewMatchCriteriaInput()
    fwdInput.MatchCriteriaInput = *matchCriteria
    return true, nil, &fwdInput
}

func (fwd *CreateFwdInputs) SetMatchCriteria(matchCriteria *MatchCriteriaInput) {
     fwd.MatchCriteriaInput = *matchCriteria
}

func (fwd *CreateFwdInputs) SetName (Name string) (bool, error) {
    fwd.Name = Name
        return true, nil
}

func (fwd *CreateFwdInputs) SetPriority (Priority int) (bool, error) {
    if Priority >=  0 && Priority <=1000 {
        fwd.Priority  = Priority
        return true, nil
    }
        return false, nil
}

func (fwd *CreateFwdInputs) SetWantypeInternet() (bool, error) {
	return fwd.SetOutputDirection("to lan")
}


func (fwd *CreateFwdInputs) SetOutputDirectionLan() (bool, error) {
	return fwd.SetOutputDirection("to lan")
}

func (fwd *CreateFwdInputs) SetOutputDirectionWan() (bool, error) {
	return fwd.SetOutputDirection("to wan")
}

func (fwd *CreateFwdInputs) SetOutputDirection (direction string) (bool, error) {
        if direction == "to lan"{
            fwd.Output_direction = "lan"
    } else if direction == "to wan"{
                    fwd.Output_direction = "wan"
            } else {
                    return false,nil
            }

        return false, nil
}

