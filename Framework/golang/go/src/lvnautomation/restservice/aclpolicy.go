package restservice

import (
	"fmt"
	"net/http"
	//"errors"
	"encoding/json"
	//"regexp"
	"strconv"
	//"strings"
	//"bytes"
	//"compress/gzip"
	//"io"

	log "lvnautomation/lvnlog"
	//"time"
	tc_utils "lvnautomation/testcaseutils"
)

func (cs *Cloudstation) CreateAclPolicy(aclinputs *CreateAclInputs) (bool, *CreateAclResponse) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var Aclpol_payload struct {
		Name       string `json:"name"`
		Priority   int    `json:"priority"`
		Tag        string `json:"tag"`
		Acl_action string `json:"acl_action"`
		Conn_state string `json:"conn_state"`
		MATCHCRITERIA
	}

	Aclpol_payload.Name = aclinputs.Name
	Aclpol_payload.Priority = aclinputs.GetAclPriority()
	Aclpol_payload.Tag = aclinputs.GetAclTag()
	Aclpol_payload.Acl_action = aclinputs.GetAclAction()
	Aclpol_payload.Conn_state = aclinputs.GetAclConnectionState()
	matchcr, _ := cs.GetMatchCriteria(aclinputs.MatchCriteriaInput)
	Aclpol_payload.MATCHCRITERIA = *matchcr

	key := "conf_aclpolicy"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	var acl_response CreateAclResponse
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, Aclpol_payload, cookie)
	//fmt.Println(resp)

	if resp.StatusCode != 200 {
		return false, &acl_response
	}

	err_acl := json.NewDecoder(resp.Body).Decode(&acl_response)
	if err_acl != nil {
		log.Err.Println(err_acl)
	}
	if ok, json_data := convert_struct_to_string_via_json(acl_response); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	return true, &acl_response
}
type AclInfo struct {
		Acl_action             string `json:"acl_action"`
		Conn_state             string `json:"conn_state"`
		CreatedAt              string `json:"created_at"`
		Customer_id            int    `json:"customer_id"`
		Direction              string `json:"direction"`
		Dst_network_group_id   int    `json:"dst_network_group_id"`
		Dst_network_group_name string `json:"dst_network_group_name"`
		Hex_id                 string `json:"hex_id"`
		Id                     int    `json:"id"`
		Name                   string `json:"name"`
		Priority               int    `json:"priority"`
		Src_network_group_id   int    `json:"src_network_group_id"`
		Src_network_group_name string `json:"src_network_group_name"`
		Tag                    string `json:"tag"`
		UpdatedAt              string `json:"updated_at"`
		User_id                int    `json:"user_id"`
}

type ACLPOLICY struct {
	Acls []AclInfo  `json:"acls"`
	Total_count int `json:"total_count"`
}

type CreateAclResponse struct {
	Data struct {
		Acls AclInfo `json:"acls"`
	} `json:"data"`
	Message string `json:"message"`
}

func (cs *Cloudstation) Get_AclPolicyList() ([]ACLPOLICY, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var dummy struct {
	}

	key := "get_aclpolicy_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	queryParam := map[string]interface{}{
		"key":      "",
		"page":     1,
		"per_page": 10,
		"sort":     "",
		"user": map[string]interface{}{
			"username":             cs.Username,
			"id":                   cs.Id,
			"role":                 cs.Role,
			"allow_manage":         cs.AllowManage,
			"customer_id":          cs.CustomerId,
			"hex_id":               cs.HexId,
			"customer_hex_id":      cs.CustomerHexId,
			"enable_notifications": cs.EnableNotifications,
		},
	}

	var aclpol_ls []ACLPOLICY
	var aclpol ACLPOLICY
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return aclpol_ls, false
	}
	err_aclpolicy := json.NewDecoder(resp.Body).Decode(&aclpol)
	if err_aclpolicy != nil {
		fmt.Println(err_aclpolicy)
	}
	if ok, json_data := convert_struct_to_string_via_json(aclpol); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	aclpol_ls = append(aclpol_ls, aclpol)

	tot_entry := aclpol.Total_count
	tot_page := 0
	if tot_entry%10 == 0 {
		tot_page = tot_entry / 10
	} else {
		tot_page = (tot_entry / 10) + 1
	}

	i := 2
	for i <= tot_page {
		var aclpol1 ACLPOLICY
		queryParam = map[string]interface{}{
			"key":      "",
			"page":     i,
			"per_page": 10,
			"sort":     "",
			"user": map[string]interface{}{
				"username":             cs.Username,
				"id":                   cs.Id,
				"role":                 cs.Role,
				"allow_manage":         cs.AllowManage,
				"customer_id":          cs.CustomerId,
				"hex_id":               cs.HexId,
				"customer_hex_id":      cs.CustomerHexId,
				"enable_notifications": cs.EnableNotifications,
			},
		}
		resp, _ = request(api.request.method, cs.Base_url, substitutedPath, queryParam, header, dummy, cookie)
		if resp.StatusCode != 200 {
			return aclpol_ls, false
		}
		err_aclpolicy := json.NewDecoder(resp.Body).Decode(&aclpol1)
		if err_aclpolicy != nil {
			log.Err.Println(err_aclpolicy)
		}
	if ok, json_data := convert_struct_to_string_via_json(aclpol1); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
		aclpol_ls = append(aclpol_ls, aclpol1)
		i++
	}
	return aclpol_ls, true
}

func (cs *Cloudstation) Get_AclHexId(acl_name string) (string, bool) {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	aclpol_ls, err1 := cs.Get_AclPolicyList()
	if err1 == false {
		log.Err.Println("Failed to fetch the ACL POLICY List !", err1)
		return "", false
	}

	var hex_id string
	var found_hex_id bool
	for i := 0; i < len(aclpol_ls); i++ {
		for j := 0; j < len(aclpol_ls[i].Acls); j++ {
			if aclpol_ls[i].Acls[j].Name == acl_name {
				hex_id = aclpol_ls[i].Acls[j].Hex_id
				found_hex_id = true
				break
			}
		}
		if found_hex_id == true {
			break
		}
	}
	if found_hex_id == false {
		log.Err.Println("No Hex Id found !")
		return "", false
	}
	return hex_id, true
}

func (cs *Cloudstation) DeleteAclPolicy(acl_name string) bool {

	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	hex_id, err1 := cs.Get_AclHexId(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the Hex Id !")
		return false
	}

	var dummy struct {
	}

	key := "delete_aclpolicy"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": hex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		log.Err.Println("The Policy is been Attached !")
		return false
	}
	return true
}

type ACLpreviewLIST struct {
	Acls struct {
		Hex_id string `json:"hex_id"`
		Id     int    `json:"id"`
		Name   string `json:"name"`
	} `json:"acls"`
	Status string `json:"status"`
}

func (cs *Cloudstation) Get_AclPreviewList(acl_name string) (ACLpreviewLIST, bool) {


	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	var aclpreview_ls ACLpreviewLIST
	var dummy struct {
	}

	aclhex_id, err1 := cs.Get_AclHexId(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the Acl Hex Id !", err1)
		return aclpreview_ls, false
	}

	key := "get_aclpreview_list"
	apis := getApis()
	api := apis[key]
	substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": aclhex_id})

	header := cs.Construct_headers(apis[key].headers.headers)
	var resp *http.Response
	cookie := []*http.Cookie{cs.Cookies}
	resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, dummy, cookie)
	if resp.StatusCode != 200 {
		return aclpreview_ls, false
	}
	err_aclpreview := json.NewDecoder(resp.Body).Decode(&aclpreview_ls)
	if err_aclpreview == nil {
		fmt.Println(err_aclpreview)
	}
	if ok, json_data := convert_struct_to_string_via_json(aclpreview_ls); ok {
	    log.Debug.Printf("response data: %s\n", json_data)
	}
	return aclpreview_ls, true
}

func (cs *Cloudstation) Get_AclId(acl_name string) (int, bool) {
	log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
	aclpreview_ls, err1 := cs.Get_AclPreviewList(acl_name)
	if err1 == false {
		log.Err.Println("Failed to fetch the ACL PREVIEW List !", err1)
		return 0, false
	}

	var id int
	var found_id bool
	if aclpreview_ls.Acls.Name == acl_name {
		id = aclpreview_ls.Acls.Id
		found_id = true
	}
	if found_id == false {
		log.Err.Println("No Id found !")
		return 0, false
	}
	//fmt.Println("--------------Got into the Verse for Getting the ACL ID from preview !--------------")
	return id, true
}

func (cs *Cloudstation) AttachAclAtNwGrp(acl_name string, nwgrp_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        aclhex_id, err1 := cs.Get_AclHexId(acl_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Acl Hex Id !")
                return false
        }

        nwgrphex_id, err2 := cs.Get_NwGrpHexId(nwgrp_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the NwGrp Hex Id !")
                return false
        }

        var attach_nwgrp_payload struct {
                Attached_to_id   string `json:"attached_to_id"`
                Attached_to_type string `json:"attached_to_type"`
        }

        attach_nwgrp_payload.Attached_to_id = nwgrphex_id
        attach_nwgrp_payload.Attached_to_type = "NetworkGroup"

        key := "attach_acl_at_networkgrp"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": aclhex_id})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_nwgrp_payload, cookie)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}


func (cs *Cloudstation) DetachAclAtNwGrp(acl_name string, nwgrp_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        acl_id, err1 := cs.Get_AclId(acl_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Acl Id !")
                return false
        }

        nwobject_id, err2 := cs.Get_NwGrpId(nwgrp_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the NwGrp Object Id !")
                return false
        }

        var detach_nwgrp_payload struct {
                Object_id   int    `json:"object_id"`
                Object_type string `json:"object_type"`
        }

        detach_nwgrp_payload.Object_id = nwobject_id
        detach_nwgrp_payload.Object_type = "NetworkGroup"

        key := "detach_acl_at_networkgrp"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(acl_id)})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_nwgrp_payload, cookie)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}


func (cs *Cloudstation) AttachAclPolicyAtSite(acl_name string, site_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        aclhex_id, err1 := cs.Get_AclHexId(acl_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Acl Hex Id !")
                return false
        }

        site_id, err2 := cs.Get_SiteId(site_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the Site Id !")
                return false
        }

        var attach_site_payload struct {
                Attached_to_id   string    `json:"attached_to_id"`
                Attached_to_type string `json:"attached_to_type"`
        }

        attach_site_payload.Attached_to_id = strconv.Itoa(site_id)
        attach_site_payload.Attached_to_type = "Site"

        key := "attach_acl_at_site"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": aclhex_id})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_site_payload, cookie)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}


func (cs *Cloudstation) DetachAclPolicyFromSite(acl_name string, site_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        acl_id, err1 := cs.Get_AclId(acl_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Acl Id !")
                return false
        }

        siteobject_id, err2 := cs.Get_SiteId(site_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the Site Object Id !")
                return false
        }

        var detach_site_payload struct {
                Object_id   string    `json:"object_id"`
                Object_type string `json:"object_type"`
        }

        detach_site_payload.Object_id = strconv.Itoa(siteobject_id)
        detach_site_payload.Object_type = "Site"

        key := "detach_acl_from_site"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(acl_id)})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_site_payload, cookie)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}


func (cs *Cloudstation) AttachAclAtSiteProfile(acl_name string, profile_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        aclhex_id, err1 := cs.Get_AclHexId(acl_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Acl Hex Id !")
                return false
        }

        profile_id, err2 := cs.Get_ProfileId(profile_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the Profile Id !")
                return false
        }

        var attach_profile_payload struct {
                Attached_to_id   int    `json:"attached_to_id"`
                Attached_to_type string `json:"attached_to_type"`
        }

        attach_profile_payload.Attached_to_id = profile_id
        attach_profile_payload.Attached_to_type = "Profile"

        key := "attach_acl_at_profile"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": aclhex_id})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, attach_profile_payload, cookie)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}


func (cs *Cloudstation) DetachAclAtSiteProfile(acl_name string, profile_name string) bool {
        log.Debug.Printf("FUNC: %s entered\n", tc_utils.FuncName())
        acl_id, err1 := cs.Get_AclId(acl_name)
        if err1 == false {
                log.Err.Println("Failed to fetch the Acl Id !")
                return false
        }

        profileobject_id, err2 := cs.Get_ProfileId(profile_name)
        if err2 == false {
                log.Err.Println("Failed to fetch the Profile Object Id !")
                return false
        }

        var detach_profile_payload struct {
                Object_id   int    `json:"object_id"`
                Object_type string `json:"object_type"`
        }

        detach_profile_payload.Object_id = profileobject_id
        detach_profile_payload.Object_type = "Profile"

        key := "detach_acl_at_profile"
        apis := getApis()
        api := apis[key]
        substitutedPath := substitutePath(apis[key], map[string]string{"custom_id": strconv.Itoa(acl_id)})

        header := cs.Construct_headers(apis[key].headers.headers)
        var resp *http.Response
        cookie := []*http.Cookie{cs.Cookies}
        resp, _ = request(api.request.method, cs.Base_url, substitutedPath, map[string]interface{}{}, header, detach_profile_payload, cookie)
        //fmt.Println(resp)
        if resp.StatusCode != 200 {
                return false
        }
        return true
}

