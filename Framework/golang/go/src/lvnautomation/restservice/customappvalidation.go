package restservice

import (
	//"fmt"
	//"time"
        "strings"
	log "lvnautomation/lvnlog"
	"strconv"
)

type CreateCustomAppInputs struct {

        Name string
        AppCategory string
        IdentificationType string
        Hostname string
        Tag string
        ip string
        sourceIp string
        destinationIp string
        port string
        sourcePort string
        destinationPort string
        Protocol string
        Signature string

}


func NewCustomApp() (*CreateCustomAppInputs) {
	var custom_app CreateCustomAppInputs
        custom_app.Name = "lvn_auto_" + log.Log_date
	custom_app.AppCategory = "Enterprise"
	custom_app.SetIdentificationTypeHostname()
	custom_app.Hostname = "abcd.efgh.ijk"
	return &custom_app
}

func (custom_app *CreateCustomAppInputs) SetIdentificationTypeHostname() {

	custom_app.setIdentificationType("hostname")
	custom_app.Signature = ""
	custom_app.ip = ""
	custom_app.sourceIp = ""
	custom_app.destinationIp = ""

	custom_app.port = ""
	custom_app.destinationPort = ""
	custom_app.sourcePort = ""
	custom_app.Protocol = ""
}

func (custom_app *CreateCustomAppInputs) SetIdentificationTypeNetworkTuple() {
	custom_app.setIdentificationType("tuple")
	custom_app.Hostname = ""
	custom_app.Signature = ""
}

func (custom_app *CreateCustomAppInputs) SetIdentificationTypeRawPattern() {
	custom_app.setIdentificationType("raw_pattern")


}

func (custom_app *CreateCustomAppInputs) setIdentificationType(identification_type string) bool {
    identification_type = strings.ToLower(identification_type)
    if Contains([]string{"hostname", "tuple", "raw_pattern"}, identification_type) {
	    custom_app.IdentificationType = identification_type
	    return true
    } else {
	    return false
    }
}

func (custom_app *CreateCustomAppInputs) SetPort(port int) bool {
	if CheckValidPort(port) {
	    custom_app.sourcePort = ""
            custom_app.destinationPort = ""
	    custom_app.port = strconv.Itoa(port)
	    return true
	} else {
		return false
	}

}

func (custom_app *CreateCustomAppInputs) SetSourcePort(port int) bool {
	if CheckValidPort(port) {
	    custom_app.sourcePort = strconv.Itoa(port)
	    custom_app.port =  ""
	    return true
	} else {
	    return false
	}
}

func (custom_app *CreateCustomAppInputs) SetDestinationPort(port int) bool {
	if CheckValidPort(port) {
	    custom_app.destinationPort = strconv.Itoa(port)
	    custom_app.port =  ""
	    return true
	} else {
		return false
	}
}

func (custom_app *CreateCustomAppInputs) SetIP(ip string) {
	    custom_app.sourceIp = ""
            custom_app.destinationIp = ""
	    custom_app.ip = ip
}

func (custom_app *CreateCustomAppInputs) SetSourceIP(ip string) {
	    custom_app.sourceIp = ip
	    custom_app.ip =  ""
}

func (custom_app *CreateCustomAppInputs) SetDestinationIP(ip string) {
	    custom_app.destinationIp = ip
	    custom_app.ip = ""
}
