package restservice

import (
	"fmt"
	"strings"
)

type UpdateNwGrpInput struct {
    Name string
    Tag string
    threatProtectionProfile string
    subnetList []string
    subnet_decoder string
    encapsulation string
    nextHop string
    secureData string
    allowInternetAccess string
    encryption_profile_name string
}

type SubnetInfo struct {
	SiteName string
	Subnet string
	Netmask string
	Vlan int
}

func NewSubNetInfo(site_name string, subnet string, netmask string, vlan int) *SubnetInfo {

    site_name = strings.TrimSpace(site_name)
    subnet = strings.TrimSpace(subnet)
    netmask = strings.TrimSpace(netmask)
    /* vlan 4095 is internet */
    if site_name == "" || subnet == "" || netmask == "" || vlan < 0 || vlan > 4094 {
	    return nil
    }
    return &SubnetInfo{SiteName: site_name, Subnet: subnet, Netmask: netmask, Vlan: vlan}
}

func NewUpdateNwGrpInput(nw_grp_name string, default_encryption_profile string, default_threatprotection_profile string) *UpdateNwGrpInput {
    var u UpdateNwGrpInput
    nw_grp_name = strings.TrimSpace(nw_grp_name)
    default_encryption_profile = strings.TrimSpace(default_encryption_profile)
    default_threatprotection_profile = strings.TrimSpace(default_threatprotection_profile)

    if nw_grp_name == "" || default_encryption_profile == "" || default_threatprotection_profile == "" {
	    return nil
    }
    u.Name =  nw_grp_name
    u.EnableInternetAccess()
    u.DisableSecureData()
    u.SetNextHopHub()
    u.SetEncapsulationUdp()
    u.SubnetsAll()
    u.SetThreatProtectionProfile(default_threatprotection_profile)
    u.encryption_profile_name = default_encryption_profile
    return &u
}

func (u *UpdateNwGrpInput) SubnetsAll() {
	u.subnet_decoder = "all"
}


func (u *UpdateNwGrpInput) Subnets(subnet_list []SubnetInfo) {
	u.subnet_decoder = "specific"
	for _, subnet_info := range subnet_list {
		subnet_name := fmt.Sprintf("%s__%s__%s__%d", subnet_info.SiteName, subnet_info.Subnet, subnet_info.Netmask, subnet_info.Vlan)
		u.subnetList = append(u.subnetList, subnet_name)
	}
}

func (u *UpdateNwGrpInput) EnableInternetAccess() {
	u.allowInternetAccess = "true"
}

func (u *UpdateNwGrpInput) EnableSecureData(encryption_profile_name ...string) {
	if u.encapsulation == "lntun" {
	    u.secureData = "true"
	    if len(encryption_profile_name) == 1 {
	        u.encryption_profile_name = encryption_profile_name[0]
	    }
	}
}

func (u *UpdateNwGrpInput) SetEncryptionProfile(encryption_profile_name string) {
	if u.encapsulation == "lntun" && u.secureData == "true" {
	    //u.secureData = "true"
	    u.encryption_profile_name = encryption_profile_name
	}
}

func (u *UpdateNwGrpInput) SetThreatProtectionProfile(profile_name string) {
	u.threatProtectionProfile = profile_name
}

func (u *UpdateNwGrpInput) DisableInternetAccess() {
	u.allowInternetAccess = "false"
}

func (u *UpdateNwGrpInput) DisableSecureData() {
	u.secureData = "false"
}

func (u *UpdateNwGrpInput) SetNextHopHub() {
	u.nextHop = "hub"
}

func (u *UpdateNwGrpInput) SetNextHopResolve() {
	u.nextHop = "resolve"
}

func (u *UpdateNwGrpInput) SetEncapsulationUdp() {
	u.encapsulation = "lntun"
}

func (u *UpdateNwGrpInput) SetEncapsulationGre() {
	u.DisableSecureData()
	u.encapsulation = "gre"
}

func (u *UpdateNwGrpInput) SetEncapsulationIPsec() {
	u.DisableSecureData()
	u.encapsulation = "ipsec"
}
