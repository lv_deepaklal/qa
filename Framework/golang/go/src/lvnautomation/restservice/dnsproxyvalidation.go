package restservice

type DnsProxyEntry struct {
        Name string
	Fqdn string
	PublicIp string
	PrivateIp string
}

type CreateDnsProxyInput struct {
    Name string
    PrimaryServer string
    SecondaryServer string
    DnsProxyEntries []DnsProxyEntry
}

type UpdateDnsProxyInput struct {
    CreateDnsProxyInput
    PrevName string
}

func NewUpdateDnsProxyInput(prev_name string) *UpdateDnsProxyInput {
    var u UpdateDnsProxyInput
    u.PrevName = prev_name
    return &u
}

func (u *UpdateDnsProxyInput) SetName(name string) {
    u.CreateDnsProxyInput.Name = name
}

func (u *UpdateDnsProxyInput) SetPrimaryServer(primary_server string) {
    u.CreateDnsProxyInput.PrimaryServer = primary_server
}

func (u *UpdateDnsProxyInput) SetSecondaryServer(secondary_server string) {
    u.CreateDnsProxyInput.SecondaryServer = secondary_server
}

/*
func (u *UpdateDnsProxyInput) UpdateDnsProxyEntry(name string) {
    u.CreateDnsProxyInput.Name = name
}
*/

func NewDnsProxyEntry(name string, fqdn string, public_ip string) *DnsProxyEntry {
    var d DnsProxyEntry
	d.Name = name
	d.Fqdn = fqdn
	d.PublicIp = public_ip
	//d.PrivateIp = private_ip
	return &d
}

func (d *DnsProxyEntry) SetPrivateIP(private_ip string) {
    d.PrivateIp = private_ip
}

func NewCreateDnsProxy(name string, primary_server string) *CreateDnsProxyInput {
   var d CreateDnsProxyInput
   d.Name = name
   d.PrimaryServer = primary_server
   return &d
}


func (d *CreateDnsProxyInput) SetSecondaryServer(secondary_server string) {
    d.SecondaryServer = secondary_server
}

func (d *CreateDnsProxyInput) AppendDnsProxyEntry(p *DnsProxyEntry) {
    d.DnsProxyEntries = append(d.DnsProxyEntries, *p)
}
