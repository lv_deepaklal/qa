Folder go - has libraries to be placed in GOROOT or GOPATH
Folder Ginkgo - Testcases

Ginkgo reference: https://onsi.github.io/ginkgo/

Execution
=========
gingko
ginkgo --focus=<testcasename>
