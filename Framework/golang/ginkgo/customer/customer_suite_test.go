package customer_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
	"fmt"
        "strings"
        tc_utils "lvnautomation/testcaseutils"
        os_utils "lvnautomation/osutils"
        host_utils "lvnautomation/hostutils"
        lvn_log "lvnautomation/lvnlog"
	lvn_reports "lvnautomation/lvnreports"
)

func TestGinkgofw(t *testing.T) {
	//fmt.Printf("%+v\n", t)
	//fmt.Println("name==",t.Name())
	RegisterFailHandler(Fail)
	var r []Reporter
	r = append(r, lvn_reports.New())
	//RunSpecs(t, "Lavelle automation Suite")
        RunSpecsWithDefaultAndCustomReporters(t, "Lavelle automation suitr", r)
}
var tc_name string = ""
var testcase_name, test_purpose string
var step_no int
var tc_result string = "PASS"
var tc_result_reason string = ""

func initialise() {
        //fmt.Println(utils.GetFuncName())
        var err error
	step_no = 0
        if tc_name, err = tc_utils.Get_testcase_name(); err!=nil {
                fmt.Println("error getting name")
        } else {
        tc_name = strings.Split(tc_name, ".go")[0]
        lvn_log.New(tc_name)
        //fmt.Println()
        lvn_log.Info.Printf("############################################################################")
	lvn_log.Info.Printf("## Testcase Name: %s", tc_name)
        lvn_log.Info.Printf("############################################################################")
        }
}



func step(format string, v ...interface{}) {

	step_no++
        //lvn_log.Info.Printf(" ")
        lvn_log.Info.Printf("****************************************************************************")
	//lvn_log.Info.Printf(format, v...)
	//fmt.Println(format)
	format = "STEP %d: " + format
	//fmt.Println(format)
	new_v := append([]interface{}{step_no}, v...)
	lvn_log.Info.Printf(format, new_v...)
        lvn_log.Info.Printf("****************************************************************************")
}

func substep(format string, v ...interface{}) {

	//lvn_log.Info.Printf(" ")
	lvn_log.Info.Printf(format, v...)
	//lvn_log.Info.Printf(" ")

}

type cleanup  func() bool
func cleanup_handler(f cleanup) bool {

    lvn_log.Info.Printf("############################################################################")
    lvn_log.Info.Printf("###                     START OF CLEANUP                                 ###")
    lvn_log.Info.Printf("############################################################################")

    res:= f()

    lvn_log.Info.Printf("############################################################################")
    lvn_log.Info.Printf("###                     END OF CLEANUP                                 #####")
    lvn_log.Info.Printf("############################################################################")

    return res
}

var _ = BeforeSuite(func() {
	fmt.Println("")
})

var _ = AfterSuite(func() {
	fmt.Println("")
})


func EnablePasswordLessSshOnCpeFromLocal(ip string) bool {

    os_utils.GenerateSshKey()

    // Username Password hardcoded since using standard user/password everywhere
    cpe := host_utils.Credentials{Host:ip, Port:"22", User:"lavelle", Pswd:"lavelle@3163"}
    cpe.Chmod("/home/lavelle/.ssh/", "700")
    //cpe.Chmod("/home/lavelle/.ssh/authorized_keys", "666")

    local_file  := "/root/.ssh/id_rsa.pub"
    remote_file2 := "/home/lavelle/.ssh/authorized_keys"
    remote_file := "/home/lavelle/authorized_keys"

    os_utils.Scp(&cpe, local_file, remote_file)
    cpe.ExecCopyFromHost(remote_file, remote_file2)

    return true
}
