package customer_test

import (

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        //tc_utils "lvnautomation/testcaseutils"
        setup_utils "lvnautomation/setuputils"
        os_utils "lvnautomation/osutils"

        //"lvn_automation/utils"
	"fmt"
	//"strings"

)



//testcase_name = "create_customer_test"
var _ = Describe("Verifying the Customer creation", func() {

	var (
		master_url string
		master_login,  master_password string
		cs rest_service.Cloudstation
		customer_name, sys_admin_email, net_admin_email string
		url_prefix, domain, url string
		create_customer_input = rest_service.NewCreateCustomerInput()
		error_reason string = ""
		setup *setup_utils.Setup001
                cleanup func() bool = func() bool {

                        lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
                        _, res, _ := cs.Login()
                        if !res {
                              return res
                        }
                        lvn_log.Info.Printf("Successfully logged into the CS\n")

                        res, _, _ = cs.DeleteCustomer(customer_name)

                        cs.Logout()
                        return true
                }

	)

	BeforeEach(func() {

                /* Fetching info from setupfile */
		var res bool
		setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"
		res, setup = setup_utils.NewSetup001(setup_filename)
		if !res {

			fmt.Println(setup)
		}
                master_url = setup.Cloudstation.Base_url
		master_login =  setup.Cloudstation.Master.Username
		master_password = setup.Cloudstation.Master.Password

                /* Fetching info from setupfile - done */

		customer_name = "Customer22"
		sys_admin_email = "cs22-sysadmin@lavelle.com"
		net_admin_email = "cs22-netadmin@lavelle.com"
		url_prefix = "customer22"
		domain = "devcloudstation.io"
		url = fmt.Sprintf("https://%s-webui.%s", url_prefix, domain)

                cs= rest_service.Cloudstation{
                        Base_url: master_url,
			Username: master_login,
			Password: master_password,
                }
		create_customer_input.Customer_name = customer_name
		create_customer_input.Sys_admin_email= sys_admin_email
		create_customer_input.Net_admin_email=net_admin_email
		create_customer_input.Url_prefix=url_prefix
		create_customer_input.Domain=domain
                initialise()


	})

	Context("create_customer_test", func() {

                //lvn_log.Info.
		It("create_customer_test", func() {
			step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			error_reason = fmt.Sprintf("Failed to login to the CS %s\n", cs.Base_url)
			Expect(res).Should(BeTrue())

			step("Create a customer (%s) with sys-admin (%s) and net-admin (%s) and url %s.%s\n",customer_name, sys_admin_email, net_admin_email, url_prefix, domain)
			res, _, create_customer_response := cs.CreateCustomer(create_customer_input)
			error_reason = fmt.Sprintf("Failed to create customer %s\n", customer_name)
			Expect(res).Should(BeTrue())
		        cs.Logout()

			step("Adding host entry for customer url in this machine\n")
			os_utils.CopyFile("/etc/hosts", "/etc/hosts.bkp")
			add_host_entry := os_utils.AddCustomerHostEntry(url_prefix, domain, master_url)
			if add_host_entry {
				lvn_log.Info.Println("Customer host entry added successfully")
			}

			step("Logging to the CS via customer url %s as sys-admin (%s)\n", url, sys_admin_email)
			error_reason = fmt.Sprintf("Failed to login to the CS via customer url %s as sys-admin (%s)\n", url, sys_admin_email)

			fmt.Println(create_customer_response.Data.Sys_admin.Password)
			sys_admin_session := rest_service.Cloudstation {
				Base_url: url,
				Username: sys_admin_email,
				Password: create_customer_response.Data.Sys_admin.Password,
			}

			_, res, _ = sys_admin_session.Login()
			Expect(res).Should(BeTrue())
			sys_admin_session.Logout()

			step("Logging to the CS via customer url %s as net-admin (%s)\n", url, net_admin_email)
			error_reason = fmt.Sprintf("Failed to login to the CS via customer url %s as net-admin (%s)\n", url, net_admin_email)
			net_admin_session := rest_service.Cloudstation {
				Base_url: url,
				Username: net_admin_email,
				Password: create_customer_response.Data.Net_admin.Password,
			}

			_, res, _ = net_admin_session.Login()
			Expect(res).Should(BeTrue())
			net_admin_session.Logout()
			os_utils.CopyFile("/etc/hosts.bkp", "/etc/hosts")

			//cs.Logout()
			//fmt.Println("Testcase completed")
		})

	})
        //url := "https://deepak-webui.devcloudstation.io"
        //cs:= rest_service.Cloudstation{
        //        Base_url: url,
        //}
        //cs.Login("deepaklal-net-admin@lavellenetworks.com", "password")
	AfterEach(func() {
		cleanup_handler(cleanup)
		//if "" != error_reason {
		//	lvn_log.Err.Printf(error_reason)
		//}
	})

})
