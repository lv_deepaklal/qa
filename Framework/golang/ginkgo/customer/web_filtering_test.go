package customer_test

import (
. "github.com/onsi/ginkgo"
        . "github.com/onsi/gomega"
        lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        tc_utils "lvnautomation/testcaseutils"
        utils "lvnautomation/utils"
        setup_utils "lvnautomation/setuputils"
        host_utils "lvnautomation/hostutils"
        "fmt"
	"strings"

)

//testcase_name = "web_filtering_test"
var _ = Describe("Verifying the Web Filtering", func() {

	var (
		url string
		login,  password string
		cs rest_service.Cloudstation
		res bool
		
		//device_name string

		//device_input *rest_service.DeviceInput
		//site_input  *rest_service.CreateSiteInput

		err error
		//tc_name string
		error_reason string = ""

		setup *setup_utils.Setup001
		
		customer_index int
                net_admin_index int
                host_index int

		ssh_host, ssh_username, ssh_password, ssh_port_no string

		wget_url, wget_folder string

		WebFilter *rest_service.CreateWebFilterInput
		AtttachWFPolicy	*rest_service.AttachUrlPolicies
		siteName string
		ProfileName  string
		cattype string
		dnycat []string
		pmtdoms  []string
		rjtdoms  []string
		permdom []rest_service.Permittdomains
		rjctdom  []rest_service.Rejecttdomains
		cidrs  []string
	//	ipranges  []string
		ips  []string
		cidrss  []rest_service.Cidrs
		iprangess  []rest_service.IpRanges
		ipss  []rest_service.Ips
		PolicyName  string
		//Priority int
		SingleIP string
		SingleIPfrompool string
		SingleIPfromsubnet string
		NumberofNetwork int
		NetworkIP string
		IpList []string
		IpListToUseAsSlice []string
		IpListToUse string
		IpListNw []string
		 IpTocheck []string
		wget_timeout int
		Netmask string

                cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")
			res = cs.DetachWebFilterFromSite(siteName, PolicyName)
			res =cs.DeleteWebFilter(ProfileName)
			//res = cs.DetachAclPolicyAtSite(acl_name, site_name)

			cs.Logout()
			return true
                }
	)

	BeforeEach(func() {

                /* Fetching info from setupfile */
		
		setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"
		res, setup = setup_utils.NewSetup001(setup_filename)
		if !res {

			fmt.Println(setup)
		}

                customer_index=0
		net_admin_index=0
		host_index=2

                url = setup.Cloudstation.Customer[customer_index].Url
		login = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Username
		password = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Password
                /* Fetching info from setupfile - done */

				/*Initialising Input*/
	_, _, WebFilter = rest_service.NewWebFilterInput()
	ProfileName = "Test_demo"
	cattype = "allow"
	dnycat = []string{"Homestyle", "Humour", "Instant Messaging"}
	pmtdoms = []string{"google.com", "youtube.com", "try.sh"}
	rjtdoms = []string{"gmail.com", "sample.hi", "gsd.daa"}

	_,_= WebFilter.SetWebFiltetrName(ProfileName)
	_,_= WebFilter.SetWFCategoryType(cattype)
	_,_= WebFilter.SetWFDenyCategories(dnycat)
	_,_= WebFilter.SetWFPermittedDomains(pmtdoms)
	_,_= WebFilter.SetWFRejectedDomains(rjtdoms)
	
	
	for i := 0; i < len(pmtdoms); i++ {
		n := rest_service.Permittdomains{Name: pmtdoms[i], Ticked: true}
		permdom = append(permdom, n)
	}

	
	for i := 0; i < len(rjtdoms); i++ {
		n := rest_service.Rejecttdomains{Name: rjtdoms[i], Ticked: true}
		rjctdom = append(rjctdom, n)
	}

	_,_= WebFilter.SetWFPermitDomains(permdom)
	_,_= WebFilter.SetWFRejectDomains(rjctdom)

				/*Input for attaching URL Filter*/
	_, _, AtttachWFPolicy = rest_service.NewAttachwfpolicy()
	cidrs = []string{"10.5.0.100/16"}
	//ipranges = []string{"192.168.57.30-192.168.57.33", "192.168.57.35-192.168.57.39"}
	ips = []string{"10.5.0.40","10.5.0.32", "10.5.0.33"}
	
	SingleIP = ips[0]
		NumberofNetwork = 10
		NetworkIP = "10.5.0.3/16"
		IpList,_=utils.Get_ip_address_from_netaddr(NetworkIP,NumberofNetwork)
	SingleIPfrompool = IpList[3]
		IpListToUse = IpList[0]+"-"+IpList[len(IpList)-1]
		IpListToUseAsSlice = append(IpListToUseAsSlice,IpListToUse)
		IpListNw,_=utils.Get_ip_address_from_netaddr(cidrs[0],NumberofNetwork)
	SingleIPfromsubnet = IpListNw[7]
	IpTocheck = append(IpTocheck,SingleIP,SingleIPfrompool,SingleIPfromsubnet)
	Netmask = "255.255.0.0"
	siteName ="5022"
	Priority := 11


	for i := 0; i<len(cidrs) ;i++ {
		n := rest_service.Cidrs{Name: cidrs[i], Ticked: true}
		cidrss = append(cidrss,n)
	} 	

	for i := 0; i<len(IpListToUseAsSlice) ;i++ {
		n := rest_service.IpRanges{Name: IpListToUseAsSlice[i], Ticked: true}
		iprangess = append(iprangess,n)
	} 

	for i := 0; i<len(ips) ;i++ {
		n := rest_service.Ips{Name: ips[i], Ticked: true}
		ipss = append(ipss,n)
	}
	PolicyName = "wonderla"	
	_,_= AtttachWFPolicy.SetAttachUrlPolicyName(PolicyName) 

	_,_= AtttachWFPolicy.SetUrlPolicyPriority(Priority) 
	_,_= AtttachWFPolicy.SetSelectedCidrs(cidrs)
	_,_= AtttachWFPolicy.SetSelectedIpRanges( IpListToUseAsSlice)	
	_,_= AtttachWFPolicy.SetSelectedIps(ips)	
	
	
	_,_= AtttachWFPolicy.SetCidrs(cidrss)	
	_,_= AtttachWFPolicy.SetIpRanges( iprangess)	
	_,_= AtttachWFPolicy.SetIps(ipss)

                ssh_host =setup.Cloudstation.Customer[customer_index].Hosts[host_index].Mgmt_ip
                ssh_username =setup.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_UN
                ssh_password = setup.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_pwd
                ssh_port_no = setup.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_port

		wget_url = "www.google.com"
		wget_folder = "/home/lavelle/"
		wget_timeout = 10
                cs= rest_service.Cloudstation{
                        Base_url: url,
                        Username: login,
                        Password: password,
                        InitialiseInfo: true,
                }

                initialise()

	})

	Context("web_filtering_test", func() {

                //lvn_log.Info.
		It("web_filtering_test", func() {

			//fmt.Println("&&&&&&&&&&&&&&&&&&&&&&&&&",IpTocheck)
			step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			error_reason = fmt.Sprintf("Failed to login to the CS %s\n", cs.Base_url)
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf ("Successfully logged into the CS\n")
			host_credentials := host_utils.Credentials{Host: ssh_host, Port:ssh_port_no, User: ssh_username, Pswd: ssh_password}
			//fmt.Println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&",host_credentials)
			step("Create URL Profile\n")
			res= cs.CreateWebFilterInput(WebFilter)
			error_reason = fmt.Sprintf("Failed to create the URL Profile\n")
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully created the URL Profile\n")

			wget_op:=""
			
			
			step("Verify the URL is accessible from the site before attaching URL Filterling\n")
				host_credentials.AddDefaultRoute("10.5.0.1")
			res, _, wget_op = host_credentials.ExecWgetFromHost(wget_url, wget_folder, wget_timeout)
			error_reason = fmt.Sprintf("URL is not accessible from the site\n")
			Expect(res).Should(BeTrue())
                        if err!=nil {
                        }
			lvn_log.Info.Printf("URL is accessible from the site\n")

			step("Attach the URL Filter \n")
                        res = cs.AttachWebFilterPolicyAtSite(siteName,AtttachWFPolicy,ProfileName)
			error_reason = fmt.Sprintf("Failed to attach the URL Filter\n")
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully attached the URL Filter\n")
			tc_utils.WaitForConfigPull()
			var HostPhyInterface string
			//tc_utils.WaitForConfigPull()
			for i := 0; i < len(setup.Cloudstation.Customer[customer_index].Devices); i++ {
               		 if setup.Cloudstation.Customer[customer_index].Devices[i].Name == siteName {
				HostPhyInterface = setup.Cloudstation.Customer[customer_index].Devices[i].IfaceMap[0].Host_iface

                       	 }
               	 }
			step("Verify the application is not accessible from the site after URL Filter\n")
			fail_cnt := 0
			fail_ip_list := []string{}
			 for i:=0;i<len(IpTocheck);i++{
				wget_op,err=host_credentials.ExecAddIpFromHost(HostPhyInterface,IpTocheck[i],Netmask)
				if err!=nil{
				//	fmt.Println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&error")
					error_reason="Failed to set ip on Host's interface"
				}
				host_credentials.AddDefaultRoute("10.5.0.1")
			res, err, wget_op = host_credentials.ExecWgetFromHost(wget_url, wget_folder, wget_timeout)

			if res == false && err == nil {
				lvn_log.Info.Printf("Site is blocked for the IP %s\n", IpTocheck[i])
			} else {
				fail_cnt = fail_cnt + 1
				fail_ip_list = append(fail_ip_list, IpTocheck[i])
			}
			//error_reason = fmt.Sprintf("Application is still accessible from the site after applying URL Filter for %s\n", IpTocheck[i])
			//error_reason = fmt.Sprintf("Application is not accessible from the site after applying URL Filter\n")
		}
		fail_reas := "All configurated IPs are blocked"
		if fail_cnt > 0 {
				lvn_log.Err.Printf("Site is not blocked for the IPs %s\n", strings.Join(fail_ip_list, ", "))
				fail_reas = fmt.Sprintf("Site is not blocked for the IPs %s\n", strings.Join(fail_ip_list, ", "))
		}

			step("Detach the URL Filter\n")
                        res = cs.DetachWebFilterFromSite(siteName,PolicyName)
			error_reason = fmt.Sprintf("Failed to Detach the URL Filter\n")
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully Detach the URL Filter \n")

			tc_utils.WaitForConfigPull()

			step("Verify the application is accessible from the site after detaching URL Filter\n")
		        host_credentials.AddDefaultRoute("10.5.0.1")
			res, err, wget_op = host_credentials.ExecWgetFromHost(wget_url, wget_folder, wget_timeout)
			error_reason = fmt.Sprintf("Application is not accessible from the site\n")
			//Expect(res).Should(BeTrue())
			//	Exepct(res).Should(BeFalse())
			if res == true && err == nil {
			lvn_log.Info.Printf("Application is accessible from the site\n")
		        }

			step("Logout from the CS\n")
			_, res = cs.Logout()
			error_reason = fmt.Sprintf("Failed to logout from the CS\n")
			//Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully logged out from the CS\n")
			lvn_log.Info.Println(wget_op)
		if fail_cnt > 0 {
			Expect("All configurated IPs are blocked").Should(Equal(fail_reas))
		}
		})

	})
	AfterEach(func() {
		//cleanup()
		cleanup_handler(cleanup)
		//if "" != error_reason {
		//	lvn_log.Err.Printf(error_reason)
		//}
	})

})
