package customer_test

import (

	. "github.com/onsi/ginkgo"
	//. "github.com/onsi/gomega"
	//ginkgotypes "github.com/onsi/ginkgo/types"
	lvn_log "lvnautomation/lvnlog"
        //rest_service "lvnautomation/restservice"
        //tc_utils "lvnautomation/testcaseutils"
        //utils "lvnautomation/utils"
	setup_utils "lvnautomation/setuputils"
	host_utils "lvnautomation/hostutils"
        //lvntypes "lvnautomation/lvntypes"
	"fmt"
	//"time"

)

var _ = Describe("Verify the maximum custom app creation on CS", func() {

	var (
		//err error
		//tc_name string
		error_reason string = ""

		setup *setup_utils.Setup001

                cleanup func() bool = func() bool {

			return true
                }
	)

	BeforeEach(func() {

                /* Fetching info from setupfile */
		var res bool
		setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"
		res, setup = setup_utils.NewSetup001(setup_filename)
		if !res {

			fmt.Println(setup)
		}

                initialise()


	})

	Context("trl", func() {

                //lvn_log.Info.
		It("debug", func() {

			/* 1. Login to the CS as net-admin */
			/* 2. Get default app list and custom app list */
			/* 3. For max custom apps supported, create a custom app */

                        cpe1 := host_utils.Credentials{Host:  "192.168.10.21",
                                                               Port: "22",
                                                               User: "lavelle",
                                                               Pswd: "lavelle@3163"}

                        fmt.Println(cpe1)
			//cpe1.StartDebugOnCpe([]string{"lan0", "lan1"}, []string{"wan0"}, true, true, true)

			//time.Sleep(5 * time.Second)
			//cpe1.CollectDebugInfoFromCpe()
			//cpe1.EnableRootSSH()
			//cpe1.DisableRootSSH()

		})

	})
	AfterEach(func() {
		//cleanup()
		cleanup_handler(cleanup)
		if "" != error_reason {
			lvn_log.Err.Printf(error_reason)
		}
	})

})
