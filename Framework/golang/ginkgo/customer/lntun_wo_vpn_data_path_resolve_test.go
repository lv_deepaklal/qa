package customer_test

import (

	. "github.com/onsi/ginkgo"
	//. "github.com/onsi/gomega"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        tc_utils "lvnautomation/testcaseutils"
        utils "lvnautomation/utils"
	setup_utils "lvnautomation/newsetuputils"
	//matcher "lvnautomation/lvnmatcher"
	lvnanalyser "lvnautomation/lvnanalyser"
	host_utils "lvnautomation/hostutils"
	//os_utils "lvnautomation/osutils"
	"fmt"
	//"time"
	"strings"
	"sync"

)

var _ = Describe("Verifying the data path of enterprise traffic in full mesh topology using LNTun", func() {

	var (
		res bool
		err error

		error_reason string
		setup *setup_utils.Setup001

		url string
		login,  password string
		network_group_name string
		cs rest_service.Cloudstation

		hub_site_name, spoke1_site_name, spoke2_site_name string
		hub_cpe_name, spoke1_cpe_name, spoke2_cpe_name string
		hub_cpe_info, spoke1_cpe_info, spoke2_cpe_info *setup_utils.Device
		internet_ip string
		enterprise_ip string
		ping_count int


		default_nw_grp_name string
                default_encryption_profile_name string
                default_threat_protection_profile_name string
                nw_grp_wrt_name map[string]rest_service.NetworkGroupInfo

		spoke1_ssh_session *host_utils.Credentials
		//spoke2_ssh_session *host_utils.Credentials

                //ssh_host, ssh_port, ssh_username, ssh_password, node_type string
		ssh_host string

                cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")

			cs.CleanupPoliciesOnSite(spoke1_site_name)

			cs.Logout()
			return true
                }
	)

	BeforeEach(func() {

		initialise()
                /* Fetching info from setupfile */
		setup_filename := "/home/lavelle/deepak/ginkgo/new_setup_info.json"
		setup = setup_utils.NewSetup001(setup_filename)
		if setup == nil {
			FailTest("Failed to read the setup file")
		}

                internet_ip = "8.8.4.4"
                ping_count = 5

                url = setup.CustomerUrl()
		network_group_name = setup.DefaultNwGrp()
		login, password = setup.NetAdminCredentials()
                /* Fetching info from setupfile - done */

                cs= rest_service.Cloudstation{
                        Base_url: url,
                        Username: login,
                        Password: password,
                        InitialiseInfo: true,
                }

                hub_site_name = setup.HubSiteName()
                hub_cpe_name  = setup.HubDeviceName()
                hub_cpe_info  = setup.HubDeviceInfo()

                spoke1_site_name = setup.S1SiteName()
                spoke1_cpe_name  = setup.S1DeviceName()
                spoke1_cpe_info  = setup.S1DeviceInfo()

                spoke2_site_name = setup.S2SiteName()
                spoke2_cpe_name  = setup.S2DeviceName()
                spoke2_cpe_info  = setup.S2DeviceInfo()


		default_nw_grp_name = setup.DefaultNwGrp()
                default_encryption_profile_name = setup.DefaultEncryptionProfile()
                default_threat_protection_profile_name = setup.DefaultSecurityProfile()

		cleanup_handler(cleanup)
	})

	Context("lntun_wo_vpn_data_path_resolve_test", func() {

                //lvn_log.Info.
		It("lntun_wo_vpn_data_path_resolve_test", func() {

			/* step 1: login to cs  */
			step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			//expected_reason = "Successfully logged into the CS"
			_, res, _ = cs.Login()
			if !res {
                           error_reason = fmt.Sprintf("Failed to login to the CS %s as %s", cs.Base_url, cs.Username)
			   FailTest(error_reason)
			}
			defer LogoutFromCloudstation(&cs)
			lvn_log.Info.Println("Successfully logged into the CS %s as %s", cs.Base_url, cs.Username)

			_, nw_grp_wrt_name, res = cs.Get_NetworkGroupList()
			if !res {
				FailTest("Failed to fetch the network group list")
			}
			//var default_nw_grp_info rest_service.NetworkGroupInfo
                        if _, res = nw_grp_wrt_name[default_nw_grp_name]; !res {
				FailTest(fmt.Sprintf("Default network group (%s) info not present in the CS", default_nw_grp_name))
			}

			var nw_grp_info_response *rest_service.GetNwGrpInfoResponse
			if res, nw_grp_info_response = cs.FetchNwGrpInfo(default_nw_grp_name); !res {
				FailTest(fmt.Sprintf("Failed to fetch the Network group info %s from the CS", default_nw_grp_name))
			}


			var sites_info map[string]rest_service.SiteInfo
			sites_info = cs.SitesByName()

			site_list := []string{hub_site_name, spoke1_site_name, spoke2_site_name}
			sites_not_present := []string{}
			for _, site := range site_list {
                            if _, ok := sites_info[site]; !ok {
				    sites_not_present  = append(sites_not_present, site)
			    }
			}

			if len(sites_not_present) > 0 {
				FailTest(fmt.Sprintf("Failed to fetch the site(s) %s info from the CS", strings.Join(sites_not_present, ",")))
			}

			sites_id_to_selected_lan_subnets_map := map[int]struct{}{}
			//fmt.Println(sites_id_to_selected_lan_subnets_map)
			for _, selected_lan_subnet := range  nw_grp_info_response.SelectedLansubnets() {
				sites_id_to_selected_lan_subnets_map[selected_lan_subnet.SiteId] = struct{}{}
			}

			sites_presented_in_default_nw_grp := []string{}
			for _, site := range site_list {
			    if _, ok := sites_id_to_selected_lan_subnets_map[sites_info[site].Id];  !ok {
				    sites_presented_in_default_nw_grp = append(sites_presented_in_default_nw_grp, site)
			    }
			}

			if len(sites_presented_in_default_nw_grp) > 0 {
				FailTest(fmt.Sprintf("Site(s) %s not present in default nw group %s", strings.Join(sites_presented_in_default_nw_grp, ","), default_nw_grp_name))
			}

                        var spoke1_iface_to_ip_addr_map, spoke2_iface_to_ip_addr_map map[string][]string
			var include_bond_iface bool = false
                        spoke1_iface_to_ip_addr_map, err = SelectLanFromCPE(&cs, spoke1_cpe_name, include_bond_iface)
			if err != nil {
				FailTest(err.Error())
			}

                        spoke2_iface_to_ip_addr_map, err = SelectLanFromCPE(&cs, spoke2_cpe_name, include_bond_iface)
			if err != nil {
				FailTest(err.Error())
			}

			var spoke1_cpe_lan_iface, spoke1_cpe_lan_iface_primary_v4_addr, spoke1_cpe_lan_iface_primary_v4_addr_mask string
			var spoke2_cpe_lan_iface, spoke2_cpe_lan_iface_primary_v4_addr string

			for k, v := range spoke1_iface_to_ip_addr_map {
                            spoke1_cpe_lan_iface, spoke1_cpe_lan_iface_primary_v4_addr, spoke1_cpe_lan_iface_primary_v4_addr_mask = k, v[0], v[1]
			    break //First iface
			}
                        lvn_log.Info.Printf("Active lan on CPE %s on site %s is %s (%s)", spoke1_cpe_name, spoke1_site_name, spoke1_cpe_lan_iface, spoke1_cpe_lan_iface_primary_v4_addr)

			for k, v := range spoke2_iface_to_ip_addr_map {
                            spoke2_cpe_lan_iface, spoke2_cpe_lan_iface_primary_v4_addr, _ = k, v[0], v[1]
			    break //First iface
			}
                        lvn_log.Info.Printf("Active lan on CPE %s on site %s is %s (%s)", spoke2_cpe_name, spoke2_site_name, spoke2_cpe_lan_iface, spoke2_cpe_lan_iface_primary_v4_addr)


	                var host_phy_iface_map, host_name_map map[string]string
			var host_session_map map[string]host_utils.Credentials
			spoke1_cpe_lan_iface_list := []string{spoke1_cpe_lan_iface}
			res, err, _, host_phy_iface_map, host_name_map, host_session_map = GetLanInfoOfCPE(setup, &cs, spoke1_site_name, spoke1_cpe_name, spoke1_cpe_info, spoke1_cpe_lan_iface_list)
			if !res {
				FailTest(err.Error())
			}

			HostName := host_name_map[spoke1_cpe_lan_iface]
			HostPhyInterface := host_phy_iface_map[spoke1_cpe_lan_iface]
			spoke1_host_ssh_session := host_session_map[spoke1_cpe_lan_iface]
			ssh_host = spoke1_host_ssh_session.Host

                        /* Get host information of the site */
                        substep("Configuring IP address on the host %s interface %s", HostName, HostPhyInterface)
                        var HostPhyInterfaceIP []string
                        HostPhyInterfaceIP, err = utils.Get_ip_address_from_netaddr(fmt.Sprintf("%s/%s", spoke1_cpe_lan_iface_primary_v4_addr, spoke1_cpe_lan_iface_primary_v4_addr_mask), 1, spoke1_cpe_lan_iface_primary_v4_addr)
                        if err != nil {
                                FailTest(fmt.Sprintf("Failed to get ip address from the network address %s/%s", spoke1_cpe_lan_iface_primary_v4_addr, spoke1_cpe_lan_iface_primary_v4_addr_mask))
                        }
                        _, err = spoke1_host_ssh_session.ExecAddIpFromHost(HostPhyInterface,  HostPhyInterfaceIP[0], spoke1_cpe_lan_iface_primary_v4_addr)
                        if err != nil {
                                error_reason = fmt.Sprintf("Failed to set Ip address %s on the interface %s on host %s(%s)", HostPhyInterfaceIP[0], HostPhyInterface, HostName, ssh_host)
                                FailTest(error_reason)
                        }
                        lvn_log.Info.Printf("Successfully set Ip address %s on the interface %s on host %s(%s)", HostPhyInterfaceIP[0], HostPhyInterface, HostName, ssh_host)

                       if err = EnsureSpecificRouteToAutomationServerOnHostSetup1(&spoke1_host_ssh_session, setup); err != nil {
			       FailTest(err.Error())
                       }

                        spoke1_host_ssh_session.AddDefaultRoute(spoke1_cpe_lan_iface_primary_v4_addr)

			enterprise_ip = spoke2_cpe_lan_iface_primary_v4_addr

			substep("Create SSH session to CPE %s", spoke1_site_name)
                        spoke1_ssh_session = SSHToCPE(&cs, spoke1_cpe_name)
                        spoke1_ssh_session.StartDebugOnCpe([]string{}, []string{}, true, true, true)
	                defer spoke1_ssh_session.CollectDebugInfoFromCpe()

                        spoke1_inet_wans := cs.GetInetWansFromCpe(spoke1_cpe_name)
                        spoke2_inet_wans := cs.GetInetWansFromCpe(spoke2_cpe_name)
                        hub_inet_wans := cs.GetInetWansFromCpe(hub_cpe_name)

                        tcpdumps := make(map[string]*host_utils.Tcpdump)
                        spoke1_inet_primary_ipv4 := make(map[string]string)
                        spoke2_inet_primary_ipv4 := make(map[string]string)
                        hub_inet_primary_ipv4 := make(map[string]string)

			spoke1_valid_inet_ips := []string{}
			spoke2_valid_inet_ips := []string{}
			hub_valid_inet_ips := []string{}

                        for _, wan_iface := range spoke1_inet_wans {
                                t := host_utils.NewTcpDump()
                                t.Iface = wan_iface.Name
                                t.Write_to_file = fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s_1.txt", lvn_log.LogFilename, wan_iface.Name, lvn_log.Log_date)
                                t.EnableRedirectToFile()
				t.SetFilter("icmp or port 7007")
                                tcpdumps[wan_iface.Name] = t
                                spoke1_inet_primary_ipv4[wan_iface.Name], _ = wan_iface.GetPrimaryV4Addr()

				if spoke1_inet_primary_ipv4[wan_iface.Name] != "" {
					spoke1_valid_inet_ips = append(spoke1_valid_inet_ips, spoke1_inet_primary_ipv4[wan_iface.Name])
				}
                        }

                        for _, wan_iface := range hub_inet_wans {
                                hub_inet_primary_ipv4[wan_iface.Name], _ = wan_iface.GetPrimaryV4Addr()
				if hub_inet_primary_ipv4[wan_iface.Name] != "" {
					hub_valid_inet_ips = append(hub_valid_inet_ips, hub_inet_primary_ipv4[wan_iface.Name])
				}
                        }

                        for _, wan_iface := range spoke2_inet_wans {
                                spoke2_inet_primary_ipv4[wan_iface.Name], _ = wan_iface.GetPrimaryV4Addr()
				if spoke2_inet_primary_ipv4[wan_iface.Name] != "" {
					spoke2_valid_inet_ips = append(spoke2_valid_inet_ips, spoke2_inet_primary_ipv4[wan_iface.Name])
				}
                        }

                        for _, tcpdump := range tcpdumps {
                            spoke1_ssh_session.StartTcpdump(tcpdump)
                        }

                        /*############################################
                          ########## PART 1 ##########################
                          ############################################*/
                        step("Update the default network group %s to have LNTun and Next hop Hub", default_nw_grp_name)
			update_nw_grp_inputs := rest_service.NewUpdateNwGrpInput(default_nw_grp_name, default_encryption_profile_name, default_threat_protection_profile_name)
			update_nw_grp_inputs.SetNextHopHub()
			update_nw_grp_inputs.SetEncapsulationUdp()

			res, err = cs.UpdateNetworkGroup(update_nw_grp_inputs)

			if !res {
				FailTest(err.Error())
			}

                        tc_utils.WaitForConfigPull()


			step("Send enterprise traffic to %s from the host %s(%s)", enterprise_ip, HostName, ssh_host)
			var ping_output_enterprise map[string]interface{}
			ping_output_enterprise, err = spoke1_host_ssh_session.ExecPingFromHost(enterprise_ip, ping_count)
			if err != nil {
				error_reason = fmt.Sprintf("Failed to execute ping %s from Host %s, reason: %s", enterprise_ip, ssh_host, err.Error())
				lvn_log.Err.Println(error_reason)
			}
			if error_reason != "" {
				FailTest(error_reason)
			}
                        lvn_log.Info.Printf("Successfully sent enterprise traffic to %s from the host %s(%s)", enterprise_ip, HostName, ssh_host)

			if loss, ok := (ping_output_enterprise["loss"]).(int); ok {
				if loss > 5 {
				FailTest(fmt.Sprintf("Ping loss more than 5% for enterprise traffic %d%", loss))
			        }
			} else {
				FailTest("Assertion error in ping output for enterprise traffic")
			}

			var w2 sync.WaitGroup
                        for _, tcpdump := range tcpdumps {
                            w2.Add(1)
                            go spoke1_ssh_session.StopTcpdump(tcpdump, &w2)
                        }
                        w2.Wait()

                        analysers := make(map[string]lvnanalyser.DumpAnalyser)
                        for wan_name, tcpdump := range tcpdumps {
                            analysers[wan_name]  = *lvnanalyser.NewDumpAnalyser(tcpdump.CapturedPkts)
                        }

			total_plain_enterprise_traffic, total_lntun_enterprise_traffic := 0, 0

                        for wan, analyser := range analysers {

                                if spoke1_inet_primary_ipv4[wan] != "" {
					plain_enterprise_traffic := analyser.EchoRequestBetween(spoke1_inet_primary_ipv4[wan], enterprise_ip)
					plain_enterprise_traffic = plain_enterprise_traffic + analyser.EchoRequestBetween(HostPhyInterfaceIP[0], enterprise_ip)
                                        if plain_enterprise_traffic > 0 {
                                            lvn_log.Err.Printf("Enterprise traffic from %s to %s is sent (Pkts: %d) on interface %s as plain traffic", HostPhyInterfaceIP[0], enterprise_ip, plain_enterprise_traffic, wan)
                                        } else {
                                            lvn_log.Info.Printf("Enterprise traffic from %s to %s is not sent (Pkts: %d) on interface %s as plain traffic", HostPhyInterfaceIP[0], enterprise_ip, plain_enterprise_traffic, wan)
                                        }



			                for _, hub_valid_inet_ip := range hub_valid_inet_ips {
                                                lntun_enterprise_traffic := analyser.LNTunDataEchoRequest(spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip, HostPhyInterfaceIP[0], enterprise_ip)

                                                total_plain_enterprise_traffic = total_plain_enterprise_traffic + plain_enterprise_traffic
                                                total_lntun_enterprise_traffic = total_lntun_enterprise_traffic + lntun_enterprise_traffic

						if lntun_enterprise_traffic > 0 {
                                                    lvn_log.Info.Printf("Enterprise traffic from %s to %s is sent (Pkts: %d) on LNTun %s-%s", HostPhyInterfaceIP[0], enterprise_ip, lntun_enterprise_traffic, spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip)
						} else {
                                                    lvn_log.Err.Printf("Enterprise traffic from %s to %s is not sent (Pkts: %d) on LNTun %s-%s", HostPhyInterfaceIP[0], enterprise_ip, lntun_enterprise_traffic, spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip)
						}

			                }

                                }
                        }

                        error_reason = ""
			error_reason_list := []string{}
			if total_plain_enterprise_traffic > 0 {
				error_reason_list = append(error_reason_list, "Plain enterprise traffic observed")
			}

			if total_lntun_enterprise_traffic == 0 {
				error_reason_list = append(error_reason_list, "No enterprise traffic in LNTun observed")
			}

			if len(error_reason_list) > 0 {
				error_reason = strings.Join(error_reason_list, ",")
			}

			if error_reason != "" {
				FailTest(error_reason)
			}

                        /*############################################
                          ########## PART 2 ##########################
                          ############################################*/

                        step("Update the default network group %s to have LNTun and Next hop Resolve", default_nw_grp_name)
			update_nw_grp_inputs.SetNextHopResolve()
			res, err = cs.UpdateNetworkGroup(update_nw_grp_inputs)

			if !res {
				FailTest(err.Error())
			}

                        tc_utils.WaitForConfigPull()

                        for wan_name, tcpdump := range tcpdumps {
                            tcpdump.Write_to_file = fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s_2.txt", lvn_log.LogFilename, wan_name, lvn_log.Log_date)
                        }

                        for _, tcpdump := range tcpdumps {
                            spoke1_ssh_session.StartTcpdump(tcpdump)
                        }

			step("Send enterprise traffic to %s from the host %s(%s)", enterprise_ip, HostName, ssh_host)
			ping_output_enterprise, err = spoke1_host_ssh_session.ExecPingFromHost(enterprise_ip, ping_count)
			if err != nil {
				error_reason = fmt.Sprintf("Failed to execute ping %s from Host %s, reason: %s", enterprise_ip, ssh_host, err.Error())
				lvn_log.Err.Println(error_reason)
			}
			if error_reason != "" {
				FailTest(error_reason)
			}
                        lvn_log.Info.Printf("Successfully sent enterprise traffic to %s from the host %s(%s)", enterprise_ip, HostName, ssh_host)

			if loss, ok := (ping_output_enterprise["loss"]).(int); ok {
				if loss > 5 {
				FailTest(fmt.Sprintf("Ping loss more than 5% for enterprise traffic %d%", loss))
			        }
			} else {
				FailTest("Assertion error in ping output for enterprise traffic")
			}

			var w4 sync.WaitGroup
                        for _, tcpdump := range tcpdumps {
                            w4.Add(1)
                            go spoke1_ssh_session.StopTcpdump(tcpdump, &w4)
                        }
                        w4.Wait()

                        for wan_name, tcpdump := range tcpdumps {
                            analysers[wan_name]  = *lvnanalyser.NewDumpAnalyser(tcpdump.CapturedPkts)
                        }
			total_plain_enterprise_traffic, total_lntun_enterprise_traffic  = 0,0
			total_lntun_enterprise_traffic_via_hub := 0

                        for wan, analyser := range analysers {

                                if spoke1_inet_primary_ipv4[wan] != "" {
					plain_enterprise_traffic := analyser.EchoRequestBetween(spoke1_inet_primary_ipv4[wan], enterprise_ip)
                                        if plain_enterprise_traffic > 0 {
                                            lvn_log.Err.Printf("Enterprise traffic from %s to %s is sent (Pkts: %d) on interface %s as plain traffic", HostPhyInterfaceIP[0], enterprise_ip, plain_enterprise_traffic, wan)
                                        } else {
                                            lvn_log.Debug.Printf("Enterprise traffic from %s to %s is not sent (Pkts: %d) on interface %s as plain traffic", HostPhyInterfaceIP[0], enterprise_ip, plain_enterprise_traffic, wan)
                                        }



                                        total_plain_enterprise_traffic = total_plain_enterprise_traffic + plain_enterprise_traffic
			                for _, hub_valid_inet_ip := range hub_valid_inet_ips {
                                                lntun_enterprise_traffic_via_hub := analyser.LNTunDataEchoRequest(spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip, HostPhyInterfaceIP[0], enterprise_ip)

                                                total_lntun_enterprise_traffic_via_hub = total_lntun_enterprise_traffic_via_hub + lntun_enterprise_traffic_via_hub

						if lntun_enterprise_traffic_via_hub > 0 {
                                                    lvn_log.Err.Printf("Enterprise traffic from %s to %s is sent (Pkts: %d) on LNTun %s-%s", HostPhyInterfaceIP[0], enterprise_ip, lntun_enterprise_traffic_via_hub, spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip)
						} else {
                                                    lvn_log.Info.Printf("Enterprise traffic from %s to %s is not sent (Pkts: %d) on LNTun %s-%s", HostPhyInterfaceIP[0], enterprise_ip, lntun_enterprise_traffic_via_hub, spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip)
						}

			                }

			                for _, spoke2_valid_inet_ip := range spoke2_valid_inet_ips {
                                                lntun_enterprise_traffic := analyser.LNTunDataEchoRequest(spoke1_inet_primary_ipv4[wan], spoke2_valid_inet_ip, HostPhyInterfaceIP[0], enterprise_ip)

                                                total_lntun_enterprise_traffic = total_lntun_enterprise_traffic + lntun_enterprise_traffic

						if lntun_enterprise_traffic > 0 {
                                                    lvn_log.Info.Printf("Enterprise traffic from %s to %s is sent (Pkts: %d) on LNTun %s-%s", HostPhyInterfaceIP[0], enterprise_ip, lntun_enterprise_traffic, spoke1_inet_primary_ipv4[wan], spoke2_valid_inet_ip)
						} else {
                                                    lvn_log.Err.Printf("Enterprise traffic from %s to %s is not sent (Pkts: %d) on LNTun %s-%s", HostPhyInterfaceIP[0], enterprise_ip, lntun_enterprise_traffic, spoke1_inet_primary_ipv4[wan], spoke2_valid_inet_ip)
						}

			                }

                                }
                        }
                        /* analysis finished */
                        error_reason = ""
			error_reason_list = []string{}
			if total_plain_enterprise_traffic > 0 {
                            error_reason = fmt.Sprintf("Enterprise traffic is sent as plain traffic")
			    error_reason_list = append(error_reason_list, error_reason)
			}

			if total_lntun_enterprise_traffic == 0 {
                            error_reason = fmt.Sprintf("Enterprise traffic is not sent directly to the other CPE as resolve")
			    error_reason_list = append(error_reason_list, error_reason)
			}

			if total_lntun_enterprise_traffic_via_hub > 0 {
                            error_reason = fmt.Sprintf("Enterprise traffic is sent directly to the hub")
			    error_reason_list = append(error_reason_list, error_reason)
			}

			if len(error_reason_list) > 0 {
				FailTest(strings.Join(error_reason_list, ","))
			}

		})

	})
	AfterEach(func() {
		cleanup_handler(cleanup)
	})

})
