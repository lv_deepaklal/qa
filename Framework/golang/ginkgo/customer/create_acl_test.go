package customer_test

import (

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        //tc_utils "lvnautomation/testcaseutils"
        //utils "lvnautomation/utils"
	setup_utils "lvnautomation/setuputils"
	//host_utils "lvnautomation/hostutils"
	"fmt"

)

var _ = Describe("Verifying the ACL policy creation alone", func() {

	var (
		url string
		login,  password string
		cs rest_service.Cloudstation
		//device_name string

		//device_input *rest_service.DeviceInput
		//site_input  *rest_service.CreateSiteInput

		err error
		//tc_name string
		step_no int = 1
		error_reason string = ""

		setup *setup_utils.Setup001
		acl_input *rest_service.CreateAclInputs
		acl_response *rest_service.CreateAclResponse


                cleanup func() bool = func() bool {

			//lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			//_, res, _ := cs.Login()
			//if !res {
			//	return res
			//}
			//lvn_log.Info.Printf("Successfully logged into the CS\n")

			//res = cs.DetachAclPolicyAtSite(acl_name, site_name)

			//cs.Logout()
			return true
                }
	)

	BeforeEach(func() {

                /* Fetching info from setupfile */
		var res bool
		setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"
		res, setup = setup_utils.NewSetup001(setup_filename)
		if !res {

			fmt.Println(setup)
		}

                customer_index:=0
		net_admin_index:=0

                url = setup.Cloudstation.Customer[customer_index].Url
		login = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Username
		password = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Password
                /* Fetching info from setupfile - done */

		res, _, acl_input = rest_service.NewAclInput()
		_, _, match_criteria_input := rest_service.NewMatchCriteriaInput()

		acl_action := "Deny"
		acl_connection_state := "New"
		acl_direction := "from lan"
		acl_src_nwgrp := "Default_NetworkGroup_2"
		acl_dst_nwgrp := "Internet"
		acl_apps := []string{"gmail"}

	        acl_input.SetAclAction(acl_action)
	        acl_input.SetAclConnectionState(acl_connection_state)

	        match_criteria_input.SetDirection(acl_direction)
                match_criteria_input.SetSrcNwGrp(acl_src_nwgrp)
	        match_criteria_input.SetDstNwGrp(acl_dst_nwgrp)
	        match_criteria_input.SetApps(acl_apps)
	        acl_input.SetMatchCriteria(match_criteria_input)


                cs= rest_service.Cloudstation{
                        Base_url: url,
                        Username: login,
                        Password: password,
                        InitialiseInfo: true,
                }

                initialise()

	})

	Context("waste", func() {

                //lvn_log.Info.
		It("madhur", func() {
			step("STEP %d: Logging to the CS %s as %s\n", step_no, cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			error_reason = fmt.Sprintf("Failed to login to the CS %s\n", cs.Base_url)
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf ("Successfully logged into the CS\n")

			step_no++
			step("STEP %d: Create ACL policy (%s)\n", step_no)
			res, acl_response = cs.CreateAclPolicy(acl_input)
                        acl_name := acl_response.Data.Acls.Name
			fmt.Println(acl_name)
			error_reason = fmt.Sprintf("Failed to create the ACL policy\n")
                        if err!=nil {
                        }
			lvn_log.Info.Printf("Successfully created the ACL policy\n")

			step_no++
			step("STEP %d: Logout from the CS\n", step_no)
			_, res = cs.Logout()
			error_reason = fmt.Sprintf("Failed to logout from the CS\n")
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully logged out from the CS\n")
		})

	})
	AfterEach(func() {
		//cleanup()
		cleanup_handler(cleanup)
		if "" != error_reason {
			lvn_log.Err.Printf(error_reason)
		}
	})

})
