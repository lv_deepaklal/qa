package customer_test

import (

	. "github.com/onsi/ginkgo"
	//. "github.com/onsi/gomega"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        //tc_utils "lvnautomation/testcaseutils"
        //utils "lvnautomation/utils"
	setup_utils "lvnautomation/setuputils"
	//host_utils "lvnautomation/hostutils"
	"fmt"

)

var _ = Describe("Verifying the ACL creation", func() {

	var (
		url string
		login,  password string
		cs rest_service.Cloudstation

		error_reason string = ""
		customer *setup_utils.CustomerInfo

		setup *setup_utils.Setup001


                cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")
			cs.CleanupPoliciesOnSite("5078")

			//res = cs.DetachAclPolicyAtSite(acl_name, site_name)

			cs.Logout()
			return true
                }
	)

	BeforeEach(func() {

                /* Fetching info from setupfile */
		var res bool
		setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"
		res, setup = setup_utils.NewSetup001(setup_filename)
		if !res {

			fmt.Println(setup)
		}

                customer_index:=0
		net_admin_index:=0
		//host_index:=2

		customer = &setup.GetCustomers()[customer_index]
                url = customer.GetURL()
		login = customer.GetNetAdminCredentials()[net_admin_index].Username
		password = customer.GetNetAdminCredentials()[net_admin_index].Password
                /* Fetching info from setupfile - done */
                cs= rest_service.Cloudstation{
                        Base_url: url,
                        Username: login,
                        Password: password,
                        InitialiseInfo: true,
                }

                initialise()

	})

	Context("cleanup_policy", func() {

                //lvn_log.Info.
		It("create_acl", func() {

		})

	})
	AfterEach(func() {
		//cleanup()
		cleanup_handler(cleanup)
		if "" != error_reason {
			lvn_log.Err.Printf(error_reason)
		}
	})

})
