package customer_test

import (

	. "github.com/onsi/ginkgo"
	//. "github.com/onsi/gomega"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        tc_utils "lvnautomation/testcaseutils"
        utils "lvnautomation/utils"
	setup_utils "lvnautomation/newsetuputils"
	//matcher "lvnautomation/lvnmatcher"
	lvnanalyser "lvnautomation/lvnanalyser"
	host_utils "lvnautomation/hostutils"
	//os_utils "lvnautomation/osutils"
	"fmt"
	//"time"
	"sync"

)

//testcase_name = "rib_test"
var _ = Describe("Verifying the RIB on a spoke", func() {

	var (
		res bool
		err error

		//expected_reason string
		//actual_reason string

		error_reason string
		setup *setup_utils.Setup001


		url string
		login,  password string
		network_group_name string
		cs rest_service.Cloudstation

		fwdInput *rest_service.CreateFwdInputs
		fwdAction *rest_service.CreateFwdPolicyAction
		fwd_policy_name string
		match_criteria_input *rest_service.MatchCriteriaInput

		hub_site_name, spoke1_site_name string
		hub_cpe_name, spoke1_cpe_name string
		hub_cpe_info, spoke1_cpe_info *setup_utils.Device
		internet_ip string
		ping_count int

		spoke1_ssh_session *host_utils.Credentials

                ssh_host, ssh_port, ssh_username, ssh_password, node_type string

		create_fwd_repsonse *rest_service.CreateFwdResponse

                cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")

			cs.CleanupPoliciesOnSite(spoke1_site_name)
			cs.DeleteFwdPolicy(fwd_policy_name)

			cs.Logout()
			return true
                }
	)

	BeforeEach(func() {

		initialise()
                /* Fetching info from setupfile */
		setup_filename := "/home/lavelle/deepak/ginkgo/new_setup_info.json"
		setup = setup_utils.NewSetup001(setup_filename)
		if setup == nil {
			FailTest("Failed to read the setup file")
		}

                internet_ip = "8.8.4.4"
                ping_count = 5

                url = setup.CustomerUrl()
		network_group_name = setup.DefaultNwGrp()
		login, password = setup.NetAdminCredentials()
                /* Fetching info from setupfile - done */

		fwd_policy_name = "lvn_auto_fwd_policy"
	        _, _, match_criteria_input = rest_service.NewMatchCriteriaInput()
                _, _, fwdInput = rest_service.NewFwdInput()
                fwdAction = rest_service.NewCreateFwdPolicyAction()
                fwdInput.SetName(fwd_policy_name)

                cs= rest_service.Cloudstation{
                        Base_url: url,
                        Username: login,
                        Password: password,
                        InitialiseInfo: true,
                }

                hub_site_name = setup.HubSiteName()
                hub_cpe_name  = setup.HubDeviceName()
                hub_cpe_info  = setup.HubDeviceInfo()

                spoke1_site_name = setup.S1SiteName()
                spoke1_cpe_name  = setup.S1DeviceName()
                spoke1_cpe_info  = setup.S1DeviceInfo()

		cleanup_handler(cleanup)
	})

	Context("rib_test", func() {

                //lvn_log.Info.
		It("rib_test", func() {

			/* step 1: login to cs  */
			step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			//expected_reason = "Successfully logged into the CS"
			_, res, _ = cs.Login()
			if !res {
                           error_reason = fmt.Sprintf("Failed to login to the CS %s as %s", cs.Base_url, cs.Username)
			   FailTest(error_reason)
			}
			defer LogoutFromCloudstation(&cs)
			lvn_log.Info.Println("Successfully logged into the CS %s as %s", cs.Base_url, cs.Username)


                        /* Get list of active lan interfaces from CPE  */
                        substep("Get list of active lan interfaces from CPE %s", spoke1_cpe_name)
                        var cpe_ifaces rest_service.IFACE
                        cpe_ifaces, res = cs.Get_IfaceList(spoke1_cpe_name)
                        if !res {
                                FailTest(fmt.Sprintf("Unable to get interface list from the CPE %s", spoke1_cpe_name))
                        }

                        cpe_lans := cpe_ifaces.GetUpPhyLans()
                        if len(cpe_lans) == 0 {
                            error_reason = fmt.Sprintf("Unable to find active lans on CPE %s", spoke1_cpe_name)
                            FailTest(error_reason)
                        }

                        lan_iface, lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask := "", "", 0
                        for _, lan := range cpe_lans {
                                if ipv4_addr, ipv4_mask := lan.GetPrimaryV4Addr();  ipv4_addr != "" {
                                    lan_iface = lan.IfaceName()
                                    lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask = ipv4_addr, ipv4_mask
                                    break
                            }
                        }
                        if lan_iface == "" || lan_iface_primary_v4_addr == "" {
                            FailTest(fmt.Sprintf("No active lans found on the site %s (cpe %s) with IPv4 address", spoke1_site_name, spoke1_cpe_name))
                        }
                        lvn_log.Info.Printf("Active lan on CPE %s on site %s is %s (%s)", spoke1_cpe_name, spoke1_site_name, lan_iface, lan_iface_primary_v4_addr)


                        /* Get physical interface of the lan interface on CPEe */
                        substep("Get physical interface of the lan interface %s on CPE %s", lan_iface, spoke1_cpe_name)
                        Linkmap, _ := cs.GetCpeLinkMap(spoke1_cpe_name)
                        ReqCpePhyport := Linkmap[lan_iface][0]
                        if ReqCpePhyport == "" {
                                FailTest(fmt.Sprintf("Unable to get the physical interface for %s", lan_iface))
                        }

                        /* Get host information of the site */
                        substep("Get host information of the site %s", spoke1_site_name)
                        HostPhyInterface := ""
                        HostName := ""
                        node_type = ""
                        for _, node := range spoke1_cpe_info.GetPhyInfo(ReqCpePhyport) {
                            if node.IsViaL2() {
                                ssh_host, ssh_port, ssh_username, ssh_password, node_type = setup.GetNodeInfo(node.GetName())
                                if node_type == "host" {
                                        HostPhyInterface = node.GetIface()
                                        HostName = node.GetName()
                                        break
                                }
                            }
                        }
                        if HostName == "" || HostPhyInterface == "" {
                            FailTest(fmt.Sprintf("No physical hosts found for the site %s", spoke1_site_name))
                        }
                        lvn_log.Info.Printf("The physical host for the site %s is %s (interface: %s)", spoke1_site_name, HostName, HostPhyInterface)

                        spoke1_host_ssh_session := host_utils.Credentials{Host: ssh_host, Port:ssh_port, User: ssh_username, Pswd: ssh_password}

                        /* Get host information of the site */
                        substep("Configuring IP address on the host %s interface %s", HostName, HostPhyInterface)
                        var HostPhyInterfaceIP []string
                        HostPhyInterfaceIP, err = utils.Get_ip_address_from_netaddr(fmt.Sprintf("%s/%d", lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask), 1, lan_iface_primary_v4_addr)
                        if err != nil {
                                FailTest(fmt.Sprintf("Failed to get ip address from the network address %s/%d", lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask))
                        }
                        _, err = spoke1_host_ssh_session.ExecAddIpFromHost(HostPhyInterface,  HostPhyInterfaceIP[0], utils.NetMaskInt2String(lan_iface_primary_v4_addr_mask))
                        if err != nil {
                                error_reason = fmt.Sprintf("Failed to set Ip address %s on the interface %s on host %s(%s)", HostPhyInterfaceIP[0], HostPhyInterface, HostName, ssh_host)
                                FailTest(error_reason)
                        }
                        lvn_log.Info.Printf("Successfully set Ip address %s on the interface %s on host %s(%s)", HostPhyInterfaceIP[0], HostPhyInterface, HostName, ssh_host)


                       if err = EnsureSpecificRouteToAutomationServerOnHostSetup1(&spoke1_host_ssh_session, setup); err != nil {
			       FailTest(err.Error())
                       }

                        spoke1_host_ssh_session.AddDefaultRoute(lan_iface_primary_v4_addr)

			substep("Create SSH session to CPE %s", spoke1_site_name)
                        spoke1_ssh_session = SSHToCPE(&cs, spoke1_cpe_name)
                        spoke1_ssh_session.StartDebugOnCpe([]string{}, []string{}, true, true, true)
	                defer spoke1_ssh_session.CollectDebugInfoFromCpe()


                        spoke1_inet_wans := cs.GetInetWansFromCpe(spoke1_cpe_name)
                        hub_inet_wans := cs.GetInetWansFromCpe(hub_cpe_name)

                        tcpdumps := make(map[string]*host_utils.Tcpdump)
                        //lvn_log.Info.Println("inet wans", spoke1_inet_wans)
                        spoke1_inet_primary_ipv4 := make(map[string]string)
                        hub_inet_primary_ipv4 := make(map[string]string)

			spoke1_valid_inet_ips := []string{}
			hub_valid_inet_ips := []string{}

                        for _, wan_iface := range spoke1_inet_wans {
                                t := host_utils.NewTcpDump()
                                t.Iface = wan_iface.Name
                                t.Write_to_file = fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s_1.txt", lvn_log.LogFilename, wan_iface.Name, lvn_log.Log_date)
                                t.EnableRedirectToFile()
				t.SetFilter("icmp or port 7007")
                                tcpdumps[wan_iface.Name] = t
                                spoke1_inet_primary_ipv4[wan_iface.Name], _ = wan_iface.GetPrimaryV4Addr()

				if spoke1_inet_primary_ipv4[wan_iface.Name] != "" {
					spoke1_valid_inet_ips = append(spoke1_valid_inet_ips, spoke1_inet_primary_ipv4[wan_iface.Name])
				}
                        }

                        for _, wan_iface := range hub_inet_wans {
                                hub_inet_primary_ipv4[wan_iface.Name], _ = wan_iface.GetPrimaryV4Addr()
				if hub_inet_primary_ipv4[wan_iface.Name] != "" {
					hub_valid_inet_ips = append(hub_valid_inet_ips, hub_inet_primary_ipv4[wan_iface.Name])
				}
                        }

                        for _, tcpdump := range tcpdumps {
                            spoke1_ssh_session.StartTcpdump(tcpdump)
                        }

			var ping_output map[string]interface{}
			ping_output, err = spoke1_host_ssh_session.ExecPingFromHost(internet_ip, ping_count)
			if err != nil {
				FailTest(fmt.Sprintf("Failed to execute ping %s from Host %s, reason: %s", internet_ip, ssh_host, err.Error()))
			}

			if loss, ok := (ping_output["loss"]).(int); ok {
				if loss > 5 {
				FailTest(fmt.Sprintf("Ping loss more than 5% %d%", loss))
			        }
			} else {
				FailTest("Assertion error in ping output")
			}

			var w2, w4 sync.WaitGroup
                        for _, tcpdump := range tcpdumps {
                            w2.Add(1)
                            go spoke1_ssh_session.StopTcpdump(tcpdump, &w2)
                        }
                        w2.Wait()

                        analysers := make(map[string]lvnanalyser.DumpAnalyser)
                        for wan_name, tcpdump := range tcpdumps {
                            analysers[wan_name]  = *lvnanalyser.NewDumpAnalyser(tcpdump.CapturedPkts)
                        }

                        res = true
                        total_plain_icmp_pkts := 0
                        for wan, analyser := range analysers {
                                //lvn_log.Info.Printf("wna iface: %s wna ip %s", wan, spoke1_inet_primary_ipv4[wan])
                                if spoke1_inet_primary_ipv4[wan] != "" {
                                        pkt_counts := analyser.EchoRequestBetween(spoke1_inet_primary_ipv4[wan], internet_ip)
                                        total_plain_icmp_pkts = total_plain_icmp_pkts + pkt_counts
                                        if pkt_counts > 0 {
                                            lvn_log.Info.Printf("PING ICMP Echorequest traffic from %s to %s is sent (Pkts: %d) on interface %s", spoke1_inet_primary_ipv4[wan], internet_ip, pkt_counts, wan)
                                        } else {
                                            lvn_log.Info.Printf("PING ICMP Echorequest traffic from %s to %s is not sent (Pkts: %d) on interface %s", spoke1_inet_primary_ipv4[wan], internet_ip, pkt_counts, wan)
                                        }
                                }
                        }

			//lvn_log.Info.Printf("No of inet wans on CPE %s is %d", spoke1_cpe_name, len(spoke1_inet_primary_ipv4))
			//lvn_log.Info.Printf("No of inet wans on CPE %s is %d", hub_cpe_name, len(hub_inet_primary_ipv4))
			//lvn_log.Info.Printf("Valid IPs on inet links of CPE %s: %s", spoke1_cpe_name, spoke1_valid_inet_ips)
			//lvn_log.Info.Printf("Valid IPs on inet links of CPE %s: %s", hub_cpe_name, hub_valid_inet_ips)

			/* FWD policy testcase*/
			step("Create a Forwarding policy for Remote Internet breakout %s", fwd_policy_name)
	                match_criteria_input.SetDirectionFromLan()
                        match_criteria_input.SetSrcNwGrp(network_group_name)
	                match_criteria_input.SetDstNwGrpInternet()

			fwdInput.SetOutputDirectionWan()
			fwdAction.SetWantypeBroadband()
                        fwdAction.SetEncapsUdptunnel()
			fwdAction.SetNextHopHub()
			fwdInput.AddFwdpolicyAction(fwdAction)
	                fwdInput.SetMatchCriteria(match_criteria_input)
			res, create_fwd_repsonse = cs.CreateForwardingPolicy(fwdInput)

			if !res {
                           error_reason = fmt.Sprintf("Failed to Create Forwarding policy %s", fwd_policy_name)
			   FailTest(error_reason)
			}
			lvn_log.Info.Printf("Successfully created the Forwarding policy %s", fwd_policy_name)

			step("Attach the Forwarding policy %s on site %s", fwd_policy_name, spoke1_site_name)
			if !cs.AttachFwdAtSite(fwd_policy_name, spoke1_site_name) {
                            error_reason = fmt.Sprintf("Failed to attach the forwarding policy %s on site %s", fwd_policy_name, spoke1_site_name)
                            FailTest(error_reason)
			}
			lvn_log.Info.Printf("Successfully attached the Forwarding policy %s on site %s", fwd_policy_name, spoke1_site_name)
			tc_utils.WaitForConfigPull()

                        for wan_iface_name, tcpdump := range tcpdumps {
                                tcpdump.Write_to_file = fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s_2.txt", lvn_log.LogFilename, wan_iface_name, lvn_log.Log_date)
                        }

                        for _, tcpdump := range tcpdumps {
                            spoke1_ssh_session.StartTcpdump(tcpdump)
                        }

			_, err = spoke1_host_ssh_session.ExecPingFromHost(internet_ip, ping_count)
			if err != nil {
				FailTest(fmt.Sprintf("Failed to execute ping %s from Host %s, reason: %s", internet_ip, ssh_host, err.Error()))
			}

                        for _, tcpdump := range tcpdumps {
                            w4.Add(1)
                            go spoke1_ssh_session.StopTcpdump(tcpdump, &w4)
                        }
                        w4.Wait()

                        for wan_name, tcpdump := range tcpdumps {
                            analysers[wan_name]  = *lvnanalyser.NewDumpAnalyser(tcpdump.CapturedPkts)
                        }

                        res = true
                        total_plain_icmp_pkts = 0
			substep("Checking the ping traffic passed via the inet inteface as plain traffic")
                        for wan, analyser := range analysers {
                                if spoke1_inet_primary_ipv4[wan] != "" {
                                        pkt_counts := analyser.EchoRequestBetween(spoke1_inet_primary_ipv4[wan], internet_ip)
                                        total_plain_icmp_pkts = total_plain_icmp_pkts + pkt_counts
                                        if pkt_counts > 0 {
                                            lvn_log.Info.Printf("PING ICMP Echorequest traffic from %s to %s is sent (Pkts: %d) on interface %s", spoke1_inet_primary_ipv4[wan], internet_ip, pkt_counts, wan)
                                        } else {
                                            lvn_log.Info.Printf("PING ICMP Echorequest traffic from %s to %s is not sent (Pkts: %d) on interface %s", spoke1_inet_primary_ipv4[wan], internet_ip, pkt_counts, wan)
                                        }
                                }
                        }

			total_lntun_icmp_pkts := 0
			substep("Checking the ping traffic passed via the inet paths as LNTun traffic")

                        for wan, analyser := range analysers {
                                if spoke1_inet_primary_ipv4[wan] != "" {
			          for _, hub_valid_inet_ip := range hub_valid_inet_ips {
                                              pkt_counts := analyser.LNTunDataEchoRequest(spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip, HostPhyInterfaceIP[0], internet_ip)
                                              total_lntun_icmp_pkts =  total_lntun_icmp_pkts + pkt_counts
                                              if pkt_counts > 0 {
                                                  lvn_log.Info.Printf("PING ICMP Echorequest traffic from %s to %s is sent (Pkts: %d) on LNTun path %s-%s on wan %s", HostPhyInterfaceIP[0], internet_ip, pkt_counts, spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip, wan)
                                              } else {
                                                  lvn_log.Info.Printf("PING ICMP Echorequest traffic from %s to %s is not sent (Pkts: %d) on LNTun path %s-%s on wan %s", HostPhyInterfaceIP[0], internet_ip, pkt_counts, spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip, wan)
                                              }
			          }
                                }
                        }

			error_reason = ""
			if total_plain_icmp_pkts > 0 {
				error_reason = "Internet traffic is leaked on local inet interface."
			}

			if total_lntun_icmp_pkts == 0 {
				error_reason =  error_reason + "No internet traffic is sent on LNTUN."
			}

			if total_plain_icmp_pkts > 0 || total_lntun_icmp_pkts == 0 {
				FailTest(error_reason)
			}

			step("Detach the Forwarding policy %s from the site %s", fwd_policy_name, spoke1_site_name)
			res = cs.DetachFwdAtSite(fwd_policy_name, hub_site_name)
			if !res {
				error_reason = fmt.Sprintf("Failed to detach the forwarding policy  %s from the site %s", fwd_policy_name, spoke1_site_name)
				FailTest(error_reason)
			}
			lvn_log.Info.Printf("Successfully detached the Forwarding policy %s on site %s", fwd_policy_name, spoke1_site_name)

		})

	})
	AfterEach(func() {
		cleanup_handler(cleanup)
	})

})
