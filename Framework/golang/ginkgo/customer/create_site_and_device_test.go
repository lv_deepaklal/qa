package customer_test

import (

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        //tc_utils "lvnautomation/testcaseutils"
        utils "lvnautomation/utils"
	setup_utils "lvnautomation/setuputils"
	"fmt"

)


//testcase_name = "create_site_and_device_test"
var _ = Describe("Verifying the Creation of site and device", func() {

	var (
		master_url string
		master_login,  master_password string
		cs rest_service.Cloudstation
		site_name, site_user string

		device_name, device_hw_type, device_uuid string
                device_ports []string
                device_attached bool
                device_slno int
                uuid_res bool

		device_input *rest_service.DeviceInput
		site_input  *rest_service.CreateSiteInput

		err error
		//tc_name string
		error_reason string = ""

		setup *setup_utils.Setup001

                cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")

			//lvn_log.Info.Printf("Get list of sites\n")
			//res, _, _, site_info_wrt_name := cs.SiteList()
			//fmt.Println(len(site_list))
			//fmt.Println(site_info_wrt_id[])

			//de:= cs.DevicesByName()
			//lvn_log.Info.Println(de)
			//lvn_log.Info.Println(de[device_name])
			//lvn_log.Info.Println(de[device_name].Name)
			lvn_log.Info.Printf("Delete the device %s\n", device_name)
			if d, ok:= cs.DevicesByName()[device_name]; ok {
				//lvn_log.Info.Println("device name ",d.Name)
				cs.DeleteDevice(d.Name)
			}

			lvn_log.Info.Printf("Delete the site %s\n", site_name)
			if s, ok:= cs.SitesByName()[site_name]; ok {
				//fmt.Println(s.Name)
				//lvn_log.Info.Println("site name ",s.Name)
				cs.DeleteSite(s.Name)
			}
			
			//error_reason = fmt.Sprintf("Failed to create the site (%s)\n", site_name)
			lvn_log.Info.Printf("Delete the site %s\n", site_name)
			cs.Logout()
			return true
                }
	)

	BeforeEach(func() {

                /* Fetching info from setupfile */
		var res bool
		setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"
		res, setup = setup_utils.NewSetup001(setup_filename)
		if !res {

			fmt.Println(setup)
		}
                master_url = setup.Cloudstation.Base_url
		master_login =  setup.Cloudstation.Master.Username
		master_password = setup.Cloudstation.Master.Password

                customer_index:=0
		sys_admin_index :=0

		site_name = "Site101"
		site_user = setup.Cloudstation.Customer[customer_index].Credentials.Sysadmin[sys_admin_index].Username
                /* Fetching info from setupfile - done */

		device_name = "Device101"
                device_hw_type = "generic_100m"
                device_ports = []string{"eth1", "eth2", "eth3", "eth4"}
                device_attached = true
                device_slno = 5001
                uuid_res, device_uuid = utils.GetUuidFromSlno(device_slno)
                //fmt.Println(device_uuid)


		_, _, site_input = rest_service.NewSiteInput()
		site_input.Name = site_name
		site_input.User = site_user

		_, _, device_input =  rest_service.NewDeviceInput()


                device_input.Name = device_name
                device_input.Authentication.Type = "UUID"
                device_input.SiteName = site_name
                device_input.SetHardwareAttachmentType(device_hw_type)
                device_input.IsHardwareAttached = device_attached
                device_input.SetHardwarePort(device_ports)
                device_input.SetUUID(device_uuid)
                cs= rest_service.Cloudstation{
                        Base_url: master_url,
			Username: master_login,
			Password: master_password,
			InitialiseInfo: true,
                }

                initialise()
		cleanup_handler(cleanup)

               //if tc_name, err = tc_utils.Get_testcase_name(); err!=nil {
               //    fmt.Println("error getting name")
               //} else {
               //    lvn_log.New(tc_name)
               //}

	})

	Context("create_site_and_device_test", func() {

                //lvn_log.Info.
		It("create_site_and_device_test", func() {
			step("Logging to the CS %s as %s\n",  cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			error_reason = fmt.Sprintf("Failed to login to the CS %s\n", cs.Base_url)
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully logged into the CS\n")

			step("Create a site (%s)\n",  site_name)
			err, res, _ = cs.CreateSite(site_input)
			//fmt.Println(err, res)
			error_reason = fmt.Sprintf("Failed to create the site (%s)\n", site_name)
			Expect(res).Should(BeTrue())
                        if err!=nil {
                        }
			lvn_log.Info.Printf("Successfully created the site (%s)\n", site_name)

			step("Attach the device (%d, UUID: %s) with site (%s)\n",  device_slno, device_uuid, site_name)
                        _, res = cs.AddDevice(device_input)
			error_reason = fmt.Sprintf("Failed to attach the device (%d)\n", device_slno)
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully attached the device (%d) at site (%s)\n", device_slno, site_name)

			step("Logout from the CS\n")
			_, res = cs.Logout()
			error_reason = fmt.Sprintf("Failed to logout from the CS\n")
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully logged out from the CS\n")
		})

	})
	AfterEach(func() {
		//cleanup()
		cleanup_handler(cleanup)
		if "" != error_reason {
			lvn_log.Err.Printf(error_reason)
		}
	})

})
