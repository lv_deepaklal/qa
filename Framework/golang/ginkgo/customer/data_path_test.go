package customer_test

import (

	. "github.com/onsi/ginkgo"
	//. "github.com/onsi/gomega"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        //tc_utils "lvnautomation/testcaseutils"
        utils "lvnautomation/utils"
	setup_utils "lvnautomation/newsetuputils"
	//matcher "lvnautomation/lvnmatcher"
	lvnanalyser "lvnautomation/lvnanalyser"
	host_utils "lvnautomation/hostutils"
	//os_utils "lvnautomation/osutils"
	"fmt"
	//"time"
	"strings"
	"sync"

)

//testcase_name = "data_path_test"
var _ = Describe("Verifying the data path of internet and enterprise trafic", func() {

	var (
		res bool
		err error

		error_reason string
		setup *setup_utils.Setup001

		url string
		login,  password string
		network_group_name string
		cs rest_service.Cloudstation

		hub_site_name, spoke1_site_name string
		hub_cpe_name, spoke1_cpe_name string
		hub_cpe_info, spoke1_cpe_info *setup_utils.Device
		internet_ip string
		enterprise_ip string
		ping_count int


		default_nw_grp_name string
                default_encryption_profile_name string
                default_threat_protection_profile_name string
                nw_grp_wrt_name map[string]rest_service.NetworkGroupInfo

		spoke1_ssh_session *host_utils.Credentials

                ssh_host, ssh_port, ssh_username, ssh_password, node_type string

                cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")

			cs.CleanupPoliciesOnSite(spoke1_site_name)

			cs.Logout()
			return true
                }
	)

	BeforeEach(func() {

		initialise()
                /* Fetching info from setupfile */
		setup_filename := "/home/lavelle/deepak/ginkgo/new_setup_info.json"
		setup = setup_utils.NewSetup001(setup_filename)
		if setup == nil {
			FailTest("Failed to read the setup file")
		}

                internet_ip = "8.8.4.4"
                ping_count = 5

                url = setup.CustomerUrl()
		network_group_name = setup.DefaultNwGrp()
		login, password = setup.NetAdminCredentials()
                /* Fetching info from setupfile - done */

                cs= rest_service.Cloudstation{
                        Base_url: url,
                        Username: login,
                        Password: password,
                        InitialiseInfo: true,
                }

                hub_site_name = setup.HubSiteName()
                hub_cpe_name  = setup.HubDeviceName()
                hub_cpe_info  = setup.HubDeviceInfo()

                spoke1_site_name = setup.S1SiteName()
                spoke1_cpe_name  = setup.S1DeviceName()
                spoke1_cpe_info  = setup.S1DeviceInfo()


		default_nw_grp_name = setup.DefaultNwGrp()
                default_encryption_profile_name = setup.DefaultEncryptionProfile()
                default_threat_protection_profile_name = setup.DefaultSecurityProfile()

		cleanup_handler(cleanup)
	})

	Context("data_path_test", func() {

                //lvn_log.Info.
		It("data_path_test", func() {

			/* step 1: login to cs  */
			step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			//expected_reason = "Successfully logged into the CS"
			_, res, _ = cs.Login()
			if !res {
                           error_reason = fmt.Sprintf("Failed to login to the CS %s as %s", cs.Base_url, cs.Username)
			   FailTest(error_reason)
			}
			defer LogoutFromCloudstation(&cs)
			lvn_log.Info.Println("Successfully logged into the CS %s as %s", cs.Base_url, cs.Username)

			_, nw_grp_wrt_name, res = cs.Get_NetworkGroupList()
			if !res {
				FailTest("Failed to fetch the network group list")
			}
			//var default_nw_grp_info rest_service.NetworkGroupInfo
                        if _, res = nw_grp_wrt_name[default_nw_grp_name]; !res {
				FailTest(fmt.Sprintf("Default network group (%s) info not present in the CS", default_nw_grp_name))
			}

			var nw_grp_info_response *rest_service.GetNwGrpInfoResponse
			if res, nw_grp_info_response = cs.FetchNwGrpInfo(default_nw_grp_name); !res {
				FailTest(fmt.Sprintf("Failed to fetch the Network group info %s from the CS", default_nw_grp_name))
			}


			hub_site_id, res := cs.SitesByName()[hub_site_name]
			if !res {
				FailTest(fmt.Sprintf("Failed to fetch the site %s info from the CS", hub_site_name))
			}

			spoke1_site_id, res := cs.SitesByName()[spoke1_site_name]
			if !res {
				FailTest(fmt.Sprintf("Failed to fetch the site %s info from the CS", spoke1_site_name))
			}

			sites_presented_in_default_nw_grp := false
			sites_id_map := map[int]struct{}{}
			fmt.Println(sites_id_map)
			for _, selected_lan_subnet := range  nw_grp_info_response.SelectedLansubnets() {
				sites_id_map[selected_lan_subnet.SiteId] = struct{}{}
			}

			if _, ok := sites_id_map[hub_site_id.Id];  ok {
			if _, ok := sites_id_map[spoke1_site_id.Id];  ok {
			    sites_presented_in_default_nw_grp = true
			}
			}

			if !sites_presented_in_default_nw_grp {
				FailTest(fmt.Sprintf("Sites %s not present in default nw group %s", strings.Join([]string{hub_site_name, spoke1_site_name}, ","), default_nw_grp_name))
			}

			update_nw_grp_inputs := rest_service.NewUpdateNwGrpInput(default_nw_grp_name, default_encryption_profile_name, default_threat_protection_profile_name)
			update_nw_grp_inputs.EnableInternetAccess()
			update_nw_grp_inputs.SetNextHopHub()
			update_nw_grp_inputs.SetEncapsulationUdp()
			update_nw_grp_inputs.DisableSecureData()

			res, err = cs.UpdateNetworkGroup(update_nw_grp_inputs)

			if !res {
				FailTest(err.Error())
			}

                        /* Get list of active lan interfaces from CPE  */
                        substep("Get list of active lan interfaces from CPE %s", spoke1_cpe_name)
                        var spoke1_cpe_ifaces, hub_cpe_ifaces rest_service.IFACE
                        spoke1_cpe_ifaces, res = cs.Get_IfaceList(spoke1_cpe_name)
                        if !res {
                                FailTest(fmt.Sprintf("Unable to get interface list from the CPE %s", spoke1_cpe_name))
                        }

                        spoke1_cpe_lans := spoke1_cpe_ifaces.GetUpPhyLans()
                        if len(spoke1_cpe_lans) == 0 {
                            error_reason = fmt.Sprintf("Unable to find active lans on CPE %s", spoke1_cpe_name)
                            FailTest(error_reason)
                        }

                        lan_iface, lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask := "", "", 0
                        for _, lan := range spoke1_cpe_lans {
                                if ipv4_addr, ipv4_mask := lan.GetPrimaryV4Addr();  ipv4_addr != "" {
                                    lan_iface = lan.IfaceName()
                                    lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask = ipv4_addr, ipv4_mask
                                    break
                            }
                        }
                        if lan_iface == "" || lan_iface_primary_v4_addr == "" {
                            FailTest(fmt.Sprintf("No active lans found on the site %s (cpe %s) with IPv4 address", spoke1_site_name, spoke1_cpe_name))
                        }
                        lvn_log.Info.Printf("Active lan on CPE %s on site %s is %s (%s)", spoke1_cpe_name, spoke1_site_name, lan_iface, lan_iface_primary_v4_addr)


                        /* Get physical interface of the lan interface on CPEe */
                        substep("Get physical interface of the lan interface %s on CPE %s", lan_iface, spoke1_cpe_name)
                        Linkmap, _ := cs.GetCpeLinkMap(spoke1_cpe_name)
                        ReqCpePhyport := Linkmap[lan_iface][0]
                        if ReqCpePhyport == "" {
                                FailTest(fmt.Sprintf("Unable to get the physical interface for %s", lan_iface))
                        }

                        /* Get host information of the site */
                        substep("Get host information of the site %s", spoke1_site_name)
                        HostPhyInterface := ""
                        HostName := ""
                        node_type = ""
                        for _, node := range spoke1_cpe_info.GetPhyInfo(ReqCpePhyport) {
                            if node.IsViaL2() {
                                ssh_host, ssh_port, ssh_username, ssh_password, node_type = setup.GetNodeInfo(node.GetName())
                                if node_type == "host" {
                                        HostPhyInterface = node.GetIface()
                                        HostName = node.GetName()
                                        break
                                }
                            }
                        }
                        if HostName == "" || HostPhyInterface == "" {
                            FailTest(fmt.Sprintf("No physical hosts found for the site %s", spoke1_site_name))
                        }
                        lvn_log.Info.Printf("The physical host for the site %s is %s (interface: %s)", spoke1_site_name, HostName, HostPhyInterface)

                        spoke1_host_ssh_session := host_utils.Credentials{Host: ssh_host, Port:ssh_port, User: ssh_username, Pswd: ssh_password}

                        /* Get host information of the site */
                        substep("Configuring IP address on the host %s interface %s", HostName, HostPhyInterface)
                        var HostPhyInterfaceIP []string
                        HostPhyInterfaceIP, err = utils.Get_ip_address_from_netaddr(fmt.Sprintf("%s/%d", lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask), 1, lan_iface_primary_v4_addr)
                        if err != nil {
                                FailTest(fmt.Sprintf("Failed to get ip address from the network address %s/%d", lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask))
                        }
                        _, err = spoke1_host_ssh_session.ExecAddIpFromHost(HostPhyInterface,  HostPhyInterfaceIP[0], utils.NetMaskInt2String(lan_iface_primary_v4_addr_mask))
                        if err != nil {
                                error_reason = fmt.Sprintf("Failed to set Ip address %s on the interface %s on host %s(%s)", HostPhyInterfaceIP[0], HostPhyInterface, HostName, ssh_host)
                                FailTest(error_reason)
                        }
                        lvn_log.Info.Printf("Successfully set Ip address %s on the interface %s on host %s(%s)", HostPhyInterfaceIP[0], HostPhyInterface, HostName, ssh_host)


                       if err = EnsureSpecificRouteToAutomationServerOnHostSetup1(&spoke1_host_ssh_session, setup); err != nil {
			       FailTest(err.Error())
                       }

                        spoke1_host_ssh_session.AddDefaultRoute(lan_iface_primary_v4_addr)

                        /* Get lan ip of the hub cpe */
                        substep("Get list of active lan interfaces from CPE %s", hub_cpe_name)
                        hub_cpe_ifaces, res = cs.Get_IfaceList(hub_cpe_name)
                        if !res {
                                FailTest(fmt.Sprintf("Unable to get interface list from the CPE %s", hub_cpe_ifaces))
                        }

                        hub_cpe_lans := hub_cpe_ifaces.GetUpPhyLans()
                        if len(hub_cpe_lans) == 0 {
                            error_reason = fmt.Sprintf("Unable to find active lans on CPE %s", hub_cpe_lans)
                            FailTest(error_reason)
                        }

                        hub_lan_iface_primary_v4_addr := ""
                        for _, lan := range hub_cpe_lans {
                                if ipv4_addr, _ := lan.GetPrimaryV4Addr();  ipv4_addr != "" {
                                    hub_lan_iface_primary_v4_addr = ipv4_addr
                                    break
                            }
                        }

                        if hub_lan_iface_primary_v4_addr == "" {
                            FailTest(fmt.Sprintf("No active lans found on the site %s (cpe %s) with IPv4 address", hub_site_name, hub_cpe_name))
                        }
                        lvn_log.Info.Printf("Active lan IP on CPE %s on site %s is %s", hub_cpe_name, hub_site_name, hub_lan_iface_primary_v4_addr)
			enterprise_ip = hub_lan_iface_primary_v4_addr


			substep("Create SSH session to CPE %s", spoke1_site_name)
                        spoke1_ssh_session = SSHToCPE(&cs, spoke1_cpe_name)
                        spoke1_ssh_session.StartDebugOnCpe([]string{}, []string{}, true, true, true)
	                defer spoke1_ssh_session.CollectDebugInfoFromCpe()


                        spoke1_inet_wans := cs.GetInetWansFromCpe(spoke1_cpe_name)
                        hub_inet_wans := cs.GetInetWansFromCpe(hub_cpe_name)

                        tcpdumps := make(map[string]*host_utils.Tcpdump)
                        spoke1_inet_primary_ipv4 := make(map[string]string)
                        hub_inet_primary_ipv4 := make(map[string]string)

			spoke1_valid_inet_ips := []string{}
			hub_valid_inet_ips := []string{}

                        for _, wan_iface := range spoke1_inet_wans {
                                t := host_utils.NewTcpDump()
                                t.Iface = wan_iface.Name
                                t.Write_to_file = fmt.Sprintf("/home/lavelle/file-server-files/%s_%s_%s_1.txt", lvn_log.LogFilename, wan_iface.Name, lvn_log.Log_date)
                                t.EnableRedirectToFile()
				t.SetFilter("icmp or port 7007")
                                tcpdumps[wan_iface.Name] = t
                                spoke1_inet_primary_ipv4[wan_iface.Name], _ = wan_iface.GetPrimaryV4Addr()

				if spoke1_inet_primary_ipv4[wan_iface.Name] != "" {
					spoke1_valid_inet_ips = append(spoke1_valid_inet_ips, spoke1_inet_primary_ipv4[wan_iface.Name])
				}
                        }

                        for _, wan_iface := range hub_inet_wans {
                                hub_inet_primary_ipv4[wan_iface.Name], _ = wan_iface.GetPrimaryV4Addr()
				if hub_inet_primary_ipv4[wan_iface.Name] != "" {
					hub_valid_inet_ips = append(hub_valid_inet_ips, hub_inet_primary_ipv4[wan_iface.Name])
				}
                        }

                        for _, tcpdump := range tcpdumps {
                            spoke1_ssh_session.StartTcpdump(tcpdump)
                        }



			step("Send internet traffic to %s and enterprise traffic to %s from the host %s(%s)", internet_ip, enterprise_ip, HostName, ssh_host)
			var ping_output_internet, ping_output_enterprise map[string]interface{}
			ping_output_internet,   err = spoke1_host_ssh_session.ExecPingFromHost(internet_ip, ping_count)
			error_reason = ""
			if err != nil {
				error_reason = fmt.Sprintf("Failed to execute ping %s from Host %s, reason: %s", internet_ip, ssh_host, err.Error())
				lvn_log.Err.Println(error_reason)
			}

			ping_output_enterprise, err = spoke1_host_ssh_session.ExecPingFromHost(enterprise_ip, ping_count)
			if err != nil {
				error_reason = fmt.Sprintf("Failed to execute ping %s from Host %s, reason: %s", enterprise_ip, ssh_host, err.Error())
				lvn_log.Err.Println(error_reason)
			}
			if error_reason != "" {
				FailTest(error_reason)
			}
                        lvn_log.Err.Printf("Successfully sent internet traffic to %s and enterprise traffic to %s from the host %s(%s)", internet_ip, enterprise_ip, HostName, ssh_host)

                        step("Verify that the internet traffic is sent only as plain and enterprise traffic is sent only via LNTun")

			if loss, ok := (ping_output_internet["loss"]).(int); ok {
				if loss > 5 {
				FailTest(fmt.Sprintf("Ping loss more than 5% for internet traffic %d%", loss))
			        }
			} else {
				FailTest("Assertion error in ping output for internet traffic")
			}


			if loss, ok := (ping_output_enterprise["loss"]).(int); ok {
				if loss > 5 {
				FailTest(fmt.Sprintf("Ping loss more than 5% for enterprise traffic %d%", loss))
			        }
			} else {
				FailTest("Assertion error in ping output for enterprise traffic")
			}

			var w2 sync.WaitGroup
                        for _, tcpdump := range tcpdumps {
                            w2.Add(1)
                            go spoke1_ssh_session.StopTcpdump(tcpdump, &w2)
                        }
                        w2.Wait()

                        analysers := make(map[string]lvnanalyser.DumpAnalyser)
                        for wan_name, tcpdump := range tcpdumps {
                            analysers[wan_name]  = *lvnanalyser.NewDumpAnalyser(tcpdump.CapturedPkts)
                        }

			total_plain_internet_traffic, total_plain_enterprise_traffic, total_lntun_internet_traffic, total_lntun_enterprise_traffic := 0, 0, 0, 0

                        for wan, analyser := range analysers {

                                if spoke1_inet_primary_ipv4[wan] != "" {
					plain_internet_traffic :=  analyser.EchoRequestBetween(spoke1_inet_primary_ipv4[wan], internet_ip)
					plain_enterprise_traffic := analyser.EchoRequestBetween(spoke1_inet_primary_ipv4[wan], enterprise_ip)
					plain_enterprise_traffic = plain_enterprise_traffic + analyser.EchoRequestBetween(HostPhyInterfaceIP[0], enterprise_ip)
                                        if plain_internet_traffic > 0 {
                                            lvn_log.Info.Printf("Internet traffic from %s to %s is sent (Pkts: %d) on interface %s", spoke1_inet_primary_ipv4[wan], internet_ip, plain_internet_traffic, wan)
                                        } else {
                                            lvn_log.Err.Printf("Internet traffic from %s to %s is not sent (Pkts: %d) on interface %s", spoke1_inet_primary_ipv4[wan], internet_ip, plain_internet_traffic, wan)
                                        }

                                        if plain_enterprise_traffic > 0 {
                                            lvn_log.Err.Printf("Enterprise traffic from %s to %s is sent (Pkts: %d) on interface %s as plain traffic", HostPhyInterfaceIP[0], enterprise_ip, plain_enterprise_traffic, wan)
                                        } else {
                                            lvn_log.Debug.Printf("Enterprise traffic from %s to %s is not sent (Pkts: %d) on interface %s as plain traffic", HostPhyInterfaceIP[0], enterprise_ip, plain_enterprise_traffic, wan)
                                        }



			                for _, hub_valid_inet_ip := range hub_valid_inet_ips {
                                                lntun_internet_traffic := analyser.LNTunDataEchoRequest(spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip, HostPhyInterfaceIP[0], internet_ip)
                                                lntun_enterprise_traffic := analyser.LNTunDataEchoRequest(spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip, HostPhyInterfaceIP[0], enterprise_ip)

                                                total_plain_internet_traffic = total_plain_internet_traffic + plain_internet_traffic
                                                total_plain_enterprise_traffic = total_plain_enterprise_traffic + plain_enterprise_traffic
                                                total_lntun_internet_traffic = total_lntun_internet_traffic + lntun_internet_traffic
                                                total_lntun_enterprise_traffic = total_lntun_enterprise_traffic + lntun_enterprise_traffic
						if lntun_internet_traffic > 0 {
                                                    lvn_log.Err.Printf("Internet traffic from %s to %s is sent (Pkts: %d) on LNTun %s-%s", HostPhyInterfaceIP[0], internet_ip, lntun_internet_traffic, spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip)
						} else {
                                                    lvn_log.Debug.Printf("Internet traffic from %s to %s is sent (Pkts: %d) on LNTun %s-%s", HostPhyInterfaceIP[0], internet_ip, lntun_internet_traffic, spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip)
						}

						if lntun_enterprise_traffic > 0 {
                                                    lvn_log.Err.Printf("Enterprise traffic from %s to %s is sent (Pkts: %d) on LNTun %s-%s", HostPhyInterfaceIP[0], enterprise_ip, lntun_enterprise_traffic, spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip)
						} else {
                                                    lvn_log.Debug.Printf("Enterprise traffic from %s to %s is sent (Pkts: %d) on LNTun %s-%s", HostPhyInterfaceIP[0], enterprise_ip, lntun_enterprise_traffic, spoke1_inet_primary_ipv4[wan], hub_valid_inet_ip)
						}

			                }



                                }
                        }

                        error_reason = ""
			error_reason_list := []string{}
			if total_plain_internet_traffic == 0 {
				error_reason_list = append(error_reason_list, "No plain interet traffic observed")
			}
			if total_plain_enterprise_traffic > 0 {
				error_reason_list = append(error_reason_list, "Plain enterprise traffic observed")
			}

			if total_lntun_internet_traffic > 0 {
				error_reason_list = append(error_reason_list, "Internet traffic in LNTun observed")
			}

			if total_lntun_enterprise_traffic == 0 {
				error_reason_list = append(error_reason_list, "No enterprise traffic in LNTun observed")
			}

			if len(error_reason_list) > 0 {
				error_reason = strings.Join(error_reason_list, ",")
			}

			if error_reason != "" {
				FailTest(error_reason)
			}
		})

	})
	AfterEach(func() {
		cleanup_handler(cleanup)
	})

})
