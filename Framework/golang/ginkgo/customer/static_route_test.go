package customer_test

import (

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        tc_utils "lvnautomation/testcaseutils"
        setup_utils "lvnautomation/setuputils"
        host_utils "lvnautomation/hostutils"
        //os_utils "lvnautomation/osutils"
	//"time"

        //"lvn_automation/utils"
	"fmt"
	//"strings"

)



var _ = Describe("Verifying the Customer creation", func() {

	var (
	        url string
                login,  password string
		cs rest_service.Cloudstation

		ssh_user, ssh_pass, ssh_host, ssh_port string
		ssh_user2, ssh_pass2, ssh_host2, ssh_port2 string

		src_host_index, dst_host_index int

		error_reason string = ""
		setup *setup_utils.Setup001
                cleanup func() bool = func() bool {

                        lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
                        _, res, _ := cs.Login()
                        if !res {
                              return res
                        }
                        lvn_log.Info.Printf("Successfully logged into the CS\n")

                        cs.Logout()
                        return true
                }

	)

	BeforeEach(func() {

                /* Fetching info from setupfile */
		var res bool
		setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"
		res, setup = setup_utils.NewSetup001(setup_filename)
		if !res {

			fmt.Println(setup)
		}

                customer_index:=0
                net_admin_index:=0

                url = setup.Cloudstation.Customer[customer_index].Url
                login = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Username
                password = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Password

		_, ssetup.GetHostDetails(customer_index, src_host_index)


                /* Fetching info from setupfile - done */


		cs= rest_service.Cloudstation{
                        Base_url: url,
                        Username: login,
                        Password: password,
                        InitialiseInfo: true,
                }

                initialise()


	})

	Context("", func() {

                //lvn_log.Info.
		It("static_route_test", func() {
			step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			error_reason = fmt.Sprintf("Failed to login to the CS %s\n", cs.Base_url)
			Expect(res).Should(BeTrue())

			cpe_name := "5078"
			iface := "lan0"
			prefix := "10.20.0.0/16"
			nxt_hop := "10.3.0.5"

			step("Create a static route\n")
			cs.Conf_StaticRoute(cpe_name, iface, prefix, nxt_hop)
			tc_utils.WaitForConfigPull()

			ssh_host = ""
			ssh_port = "22"
			ssh_user = "root"
			ssh_pass = "lavelle@3163"
			dest_host := host_utils.NewHost(ssh_host, ssh_port, ssh_user, ssh_pass)
			src_host  := host_utils.NewHost(ssh_host2, ssh_port2, ssh_user2, ssh_pass2)

			dest_host.EnableIPFwding()
		        //dest_host.DisableIPFwding()

			src_host.ExecPingFromHost("10.20.0.1", 5)

			step("Delete the static route\n")
			cs.Del_StaticRoute(cpe_name, prefix)
			cs.Logout()
			fmt.Println("Testcase completed")
		})

	})
        //url := "https://deepak-webui.devcloudstation.io"
        //cs:= rest_service.Cloudstation{
        //        Base_url: url,
        //}
        //cs.Login("deepaklal-net-admin@lavellenetworks.com", "password")
	AfterEach(func() {
		cleanup_handler(cleanup)
		//if "" != error_reason {
		//	lvn_log.Err.Printf(error_reason)
		//}
	})

})
