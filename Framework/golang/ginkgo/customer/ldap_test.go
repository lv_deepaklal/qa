package customer_test

import (
	lvn_log "lvnautomation/lvnlog"
	rest_service "lvnautomation/restservice"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	//utils "lvnautomation/utils"
	"fmt"
	setup_utils "lvnautomation/setuputils"
	"time"
)

//testcase_name = "ldap_test"
var _ = Describe("Verifying LDAP User", func() {

	var (
		url                      string
		login, password          string
		ldap_user, ldap_password string
		cs, cs_ldap              rest_service.Cloudstation
		//device_name string

		//device_input *rest_service.DeviceInput
		//site_input  *rest_service.CreateSiteInput

		err error
		//tc_name string
		error_reason string = ""
		LdapObj      rest_service.LdapInput

		setup *setup_utils.Setup001

		cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")

			res,_ = cs.DeleteLdapConfig("testing123")

			cs.Logout()
			return true
		}
	)

	BeforeEach(func() {

		/* Fetching info from setupfile */
		var res bool
		setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"
		res, setup = setup_utils.NewSetup001(setup_filename)
		if !res {

			fmt.Println(setup)
		}

		customer_index := 0
		sys_admin_index := 0
		//ldap_user = "deepaklal"
		ldap_user = "qa"
		ldap_password = "P@ssw0rd"

		url = setup.Cloudstation.Customer[customer_index].Url
		login = setup.Cloudstation.Customer[customer_index].Credentials.Sysadmin[sys_admin_index].Username
		password = setup.Cloudstation.Customer[customer_index].Credentials.Sysadmin[sys_admin_index].Password
		/* Fetching info from setupfile - done */

		cs = rest_service.Cloudstation{
			Base_url:       url,
			Username:       login,
			Password:       password,
			InitialiseInfo: true,
		}

		cs_ldap = rest_service.Cloudstation{
			Base_url:       url,
			Username:       ldap_user,
			Password:       ldap_password,
			InitialiseInfo: true,
		}
		/*Initialing Ldap Structure */
		/*
		LdapObj.Admin_pw = "13@lavelle"
		LdapObj.Admin_usrname = "CN=Administrator,CN=Users,DC=xpedition,DC=io"
		LdapObj.Bind_type = "simple"
		LdapObj.Cmi = "cn"
		LdapObj.Dist_name = "cn=Users,dc=xpedition,dc=io"
		LdapObj.Enable = true
		LdapObj.En_sec_conn = false
		LdapObj.Ldap_name = "testing123"
		LdapObj.Server_ip = "106.51.78.82"
		LdapObj.Server_port = 389
                */
		LdapObj.Admin_pw = "Qa@lavelle@1112"
		LdapObj.Admin_usrname = "CN=Administrator,CN=Users,DC=win2k12,DC=lavellenw"
		LdapObj.Bind_type = "simple"
		LdapObj.Cmi = "cn"
		LdapObj.Dist_name = "CN=Users,DC=win2k12,DC=lavellenw"
		LdapObj.Enable = true
		LdapObj.En_sec_conn = false
		LdapObj.Ldap_name = "testing123"
		LdapObj.Server_ip = "106.51.78.82"
		LdapObj.Server_port = 389
		initialise()

	})

	Context("ldap_test", func() {

		//lvn_log.Info.
		It("ldap_test", func() {
			step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			error_reason = fmt.Sprintf("Failed to login to the CS %s\n", cs.Base_url)
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully logged into the CS\n")

			step("Create LDAP configuration\n")
			//var create_ldap_response *rest_service.CreateLdapResponse
			res, _ = cs.CreateLdap(LdapObj)
			error_reason = fmt.Sprintf("Failed to create the LDAP configuration\n")
			Expect(res).Should(BeTrue())
			//Expect(create_ldap_response.Data.Ldap_server.Ldap_status).Should(Equal("connected"))
			if err != nil {
			}
			lvn_log.Info.Printf("Successfully created the LDAP configuration\n")

			step("Logout from the CS as %s\n", cs.Username)
			_, res = cs.Logout()
			error_reason = fmt.Sprintf("Failed to logout from the CS\n")
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully logged out from the CS\n")
			time.Sleep( 10 * time.Second)

			step("Logging to the CS %s as %s\n", cs_ldap.Base_url, ldap_user)
			_, res, _ = cs_ldap.Login()
			error_reason = fmt.Sprintf("Failed to login to the CS %s as LDAP user\n", cs_ldap.Base_url)
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully logged into the CS as LDAP user\n")

			step("Logout from the CS as LDAP user\n")
			_, res = cs_ldap.Logout()
			error_reason = fmt.Sprintf("Failed to logout from the CS\n")
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully logged out from the CS\n")
		})

	})
	AfterEach(func() {
		//cleanup()
		cleanup_handler(cleanup)
		//if "" != error_reason {
		//	lvn_log.Err.Printf(error_reason)
		//}
	})

})
