package customer_test

import (

	. "github.com/onsi/ginkgo"
	//. "github.com/onsi/gomega"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        tc_utils "lvnautomation/testcaseutils"
        utils "lvnautomation/utils"
	setup_utils "lvnautomation/newsetuputils"
	//matcher "lvnautomation/lvnmatcher"
	//lvnanalyser "lvnautomation/lvnanalyser"
	host_utils "lvnautomation/hostutils"
	//os_utils "lvnautomation/osutils"
	"fmt"
	"strings"
	//"time"
	//"sync"

)

//testcase_name = "dns_proxy_test"
var _ = Describe("Verify the DNS proxy functionality on a site", func() {

	var (
		res bool
		err error

		error_reason string
		setup *setup_utils.Setup001

		url string
		login,  password string
		network_group_name string
		cs rest_service.Cloudstation

		fwdInput *rest_service.CreateFwdInputs
		fwdAction *rest_service.CreateFwdPolicyAction
		fwd_policy_name string
		match_criteria_input *rest_service.MatchCriteriaInput
		dns_app []string

		hub_site_name string
		hub_cpe_name string
		hub_cpe_info *setup_utils.Device


		dns_proxy *rest_service.CreateDnsProxyInput
		dns_proxy_name string
		dns_primary_server string
		dns_proxy_entry_map map[string]string
		create_dns_proxy_response *rest_service.CreateDnsProxyResponse

                //ssh_host, ssh_port, ssh_username, ssh_password, node_type string

		//create_fwd_repsonse *rest_service.CreateFwdResponse

                cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")

                        cs.DetachDnsproxyFromSite(hub_site_name, dns_proxy_name)
                        cs.DeleteDnsProxy(dns_proxy_name)
			cs.CleanupPoliciesOnSite(hub_site_name)
			cs.DeleteFwdPolicy(fwd_policy_name)

			cs.Logout()
			return true
                }
	)

	BeforeEach(func() {

		initialise()
                /* Fetching info from setupfile */
		setup_filename := "/home/lavelle/deepak/ginkgo/new_setup_info.json"
		setup = setup_utils.NewSetup001(setup_filename)
		if setup == nil {
			FailTest("Failed to read the setup file")
		}

                url = setup.CustomerUrl()
		network_group_name = setup.DefaultNwGrp()
		login, password = setup.NetAdminCredentials()
                /* Fetching info from setupfile - done */

		fwd_policy_name = "lvn_auto_fwd_policy"
	        _, _, match_criteria_input = rest_service.NewMatchCriteriaInput()
                _, _, fwdInput = rest_service.NewFwdInput()
                fwdAction = rest_service.NewCreateFwdPolicyAction()

                cs= rest_service.Cloudstation{
                        Base_url: url,
                        Username: login,
                        Password: password,
                        InitialiseInfo: true,
                }

                hub_site_name = setup.HubSiteName()
                hub_cpe_name  = setup.HubDeviceName()
                hub_cpe_info  = setup.HubDeviceInfo()

                dns_app = []string{"dns"}
                dns_proxy_name = "lvn_auto_dns_proxy"
                dns_primary_server = "8.8.8.8"
		dns_proxy_entry_map = map[string]string{
                    "mail.google.com": "8.8.8.8",
                    "drive.google.com": "8.8.8.8",
                    "facebook.com": "8.8.4.4",
                    "youtube.com": "105.1.1.2",
	        }
                dns_proxy = rest_service.NewCreateDnsProxy(dns_proxy_name, dns_primary_server)

                for fqdn, public_ip := range dns_proxy_entry_map {
                    dns_proxy.AppendDnsProxyEntry(rest_service.NewDnsProxyEntry(fmt.Sprintf("lvn_auto_%s", fqdn), fqdn, public_ip))
                }

                match_criteria_input.SetDirectionFromLan()
                match_criteria_input.SetSrcNwGrp(network_group_name)
                match_criteria_input.SetDstNwGrpInternet()
                match_criteria_input.SetDstNwGrpInternet()
                match_criteria_input.SetApps(dns_app)

                fwdInput.SetName(fwd_policy_name)
                fwdInput.SetOutputDirectionLan()
                fwdAction.SetEncapsNone()
                fwdAction.SetWantypeBroadband()
                fwdAction.SetNextHopLocal()
                fwdInput.AddFwdpolicyAction(fwdAction)
                fwdInput.SetMatchCriteria(match_criteria_input)


		cleanup_handler(cleanup)
	})

	Context("dns_proxy_test", func() {

                //lvn_log.Info.
		It("dns_proxy_test", func() {

                        defer PrintTCResult()
			/* step 1: login to cs  */
			step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ = cs.Login()
			if !res {
                           error_reason = fmt.Sprintf("Failed to login to the CS %s as %s", cs.Base_url, cs.Username)
			   FailTest(error_reason)
			}
			defer LogoutFromCloudstation(&cs)
			lvn_log.Info.Println("Successfully logged into the CS %s as %s", cs.Base_url, cs.Username)

			sites_list := []string{hub_site_name}
			substep("Check sites %s present in the CS", strings.Join(sites_list, ","))
			res, err = CheckSitesPresentInCS(&cs, sites_list)
			if !res {
				FailTest(err.Error())
			}

                        var hub_iface_to_ip_addr_map map[string][]string
			var include_bond_iface bool = false
                        hub_iface_to_ip_addr_map, err = SelectLanFromCPE(&cs, hub_cpe_name, include_bond_iface)
			if err != nil {
				FailTest(err.Error())
			}

			hub_cpe_lan_iface, hub_cpe_lan_iface_primary_v4_addr, hub_cpe_lan_iface_primary_v4_addr_mask := "", "", ""
			for k, v := range hub_iface_to_ip_addr_map {
                            hub_cpe_lan_iface, hub_cpe_lan_iface_primary_v4_addr, hub_cpe_lan_iface_primary_v4_addr_mask = k, v[0], v[1]
			    break //First iface
			}
                        lvn_log.Info.Printf("Active lan on CPE %s on site %s is %s (%s)", hub_cpe_name, hub_site_name, hub_cpe_lan_iface, hub_cpe_lan_iface_primary_v4_addr)


	                var host_phy_iface_map, host_name_map map[string]string
			var host_session_map map[string]host_utils.Credentials
			hub_cpe_lan_iface_list := []string{hub_cpe_lan_iface}
			res, err, _, host_phy_iface_map, host_name_map, host_session_map = GetLanInfoOfCPE(setup, &cs, hub_site_name, hub_cpe_name, hub_cpe_info, hub_cpe_lan_iface_list)
			if !res {
				FailTest(err.Error())
			}

			HostName := host_name_map[hub_cpe_lan_iface]
			HostPhyInterface := host_phy_iface_map[hub_cpe_lan_iface]
			hub_host_ssh_session := host_session_map[hub_cpe_lan_iface]
			ssh_host := hub_host_ssh_session.Host

                        /* Get host information of the site */
                        substep("Configuring IP address on the host %s interface %s", HostName, HostPhyInterface)
                        var HostPhyInterfaceIP []string
                        HostPhyInterfaceIP, err = utils.Get_ip_address_from_netaddr(fmt.Sprintf("%s/%s", hub_cpe_lan_iface_primary_v4_addr, hub_cpe_lan_iface_primary_v4_addr_mask), 1, hub_cpe_lan_iface_primary_v4_addr)
                        if err != nil {
                                FailTest(fmt.Sprintf("Failed to get ip address from the network address %s/%s", hub_cpe_lan_iface_primary_v4_addr, hub_cpe_lan_iface_primary_v4_addr_mask))
                        }
                        _, err = hub_host_ssh_session.ExecAddIpFromHost(HostPhyInterface,  HostPhyInterfaceIP[0], hub_cpe_lan_iface_primary_v4_addr)
                        if err != nil {
                                error_reason = fmt.Sprintf("Failed to set Ip address %s on the interface %s on host %s(%s)", HostPhyInterfaceIP[0], HostPhyInterface, HostName, ssh_host)
                                FailTest(error_reason)
                        }
                        lvn_log.Info.Printf("Successfully set Ip address %s on the interface %s on host %s(%s)", HostPhyInterfaceIP[0], HostPhyInterface, HostName, ssh_host)

                       if err = EnsureSpecificRouteToAutomationServerOnHostSetup1(&hub_host_ssh_session, setup); err != nil {
			       FailTest(err.Error())
                       }

                       hub_host_ssh_session.AddDefaultRoute(hub_cpe_lan_iface_primary_v4_addr)

		       step("Create DNS proxy %s on the CS", dns_proxy_name)
                       res, err, create_dns_proxy_response = cs.CreateDnsProxy(dns_proxy)
                       if !res {
                           FailTest(err.Error())
                       }
                       lvn_log.Info.Printf("Successfully created DNS proxy %s", dns_proxy_name)

		       step("Attach DNS proxy %s on the site %s", dns_proxy_name, hub_site_name)
                       res, err, _ = cs.AttachDnsproxyAtSite(hub_site_name, dns_proxy_name)
                       if !res {
                           FailTest(err.Error())
                       }
                       lvn_log.Info.Printf("Successfully attached DNS proxy %s on the site %s", dns_proxy_name, hub_site_name)

		       step("Create Forwarding policy %s on the CS", fwd_policy_name)
                       res, _ = cs.CreateForwardingPolicy(fwdInput)
		       if !res {
                           error_reason = fmt.Sprintf("Failed to create Forwarding policy %s", fwd_policy_name)
			   FailTest(error_reason)
		       }
                       lvn_log.Info.Printf("Successfully created Forwarding policy %s", fwd_policy_name)

                       step("Attach the Forwarding policy %s on site %s", fwd_policy_name, hub_site_name)
                       if !cs.AttachFwdAtSite(fwd_policy_name, hub_site_name) {
                           error_reason = fmt.Sprintf("Failed to attach the forwarding policy %s on site %s", fwd_policy_name, hub_site_name)
                           FailTest(error_reason)
                       }
                       lvn_log.Info.Printf("Successfully attached the Forwarding policy %s on site %s", fwd_policy_name, hub_site_name)

                       tc_utils.WaitForConfigPull()

		       resolved_ip := ""
		       failed_hosts := []string{}

		       step("Resolve the created dns entries from the host %s", ssh_host)
		       for url, public_ip := range dns_proxy_entry_map {
                           if resolved_ip, err = hub_host_ssh_session.DnsResolveHost(url); err == nil && resolved_ip == public_ip {
                               lvn_log.Info.Printf("Host entry %s resolved to IP address %s", url, resolved_ip)
			   } else {
				   failed_hosts = append(failed_hosts, url)
			   }
		       }

		       if len(failed_hosts) > 0 {
			       FailTest(fmt.Sprintf("Failed to resolve the host entry %s", strings.Join(failed_hosts, ",")))
		       }

		})

	})
	AfterEach(func() {
		cleanup_handler(cleanup)
	})

})
