package customer_test

import (

	. "github.com/onsi/ginkgo"
	//. "github.com/onsi/gomega"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        tc_utils "lvnautomation/testcaseutils"
        //utils "lvnautomation/utils"
	setup_utils "lvnautomation/newsetuputils"
	host_utils "lvnautomation/hostutils"
	lvnanalyser "lvnautomation/lvnanalyser"
	os_utils "lvnautomation/osutils"
	utils "lvnautomation/utils"
	"fmt"
//	"time"
	"sync"
	//"strings"

)

//testcase_name = "acl_policy_test"
var _ = Describe("Verifying the ACL creation", func() {

	var (
		res bool
		url string
		login,  password string
		cs rest_service.Cloudstation
		acl_name string
		//hub_cpe_name string
                //customer_index int
		//net_admin_index int

		//device_input *rest_service.DeviceInput
		//site_input  *rest_service.CreateSiteInput

		err error
		//tc_name string
		error_reason string = ""
		//customer *setup_utils.CustomerInfo

		setup *setup_utils.Setup001
		acl_input *rest_service.CreateAclInputs

		ssh_host, ssh_username, ssh_password, ssh_port string
		hub_site_name, hub_cpe_name string
		hub_cpe_info *setup_utils.Device

		wget_url, wget_folder string
		wget_timeout int
		cpe1 *host_utils.Credentials

                cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")

			cs.CleanupPoliciesOnSite(hub_site_name)
			cs.DeleteAclPolicy(acl_name)

			cs.Logout()
			//panic("...")
			return true
                }
	)

	BeforeEach(func() {

                initialise()

                /* Fetching info from setupfile */
		setup_filename := "/home/lavelle/deepak/ginkgo/new_setup_info.json"
		setup = setup_utils.NewSetup001(setup_filename)
		if setup == nil {
			//fmt.Println(setup)
			FailTest("Failed to load the setup")
		}

                url = setup.CustomerUrl()
		login, password = setup.NetAdminCredentials()
                /* Fetching info from setupfile - done */

		res, _, acl_input = rest_service.NewAclInput()
		_, _, match_criteria_input := rest_service.NewMatchCriteriaInput()

		acl_action := "Deny"
		acl_connection_state := "New"
		acl_direction := "from lan"
		acl_src_nwgrp := "Default_NetworkGroup_2"
		acl_dst_nwgrp := "Internet"
		acl_apps := []string{"gmail"}
		acl_name = "lvn_auto_acl"


		acl_input.Name = acl_name
	        acl_input.SetAclAction(acl_action)
	        acl_input.SetAclConnectionState(acl_connection_state)

	        match_criteria_input.SetDirection(acl_direction)
                match_criteria_input.SetSrcNwGrp(acl_src_nwgrp)
	        match_criteria_input.SetDstNwGrp(acl_dst_nwgrp)
	        match_criteria_input.SetApps(acl_apps)
	        acl_input.SetMatchCriteria(match_criteria_input)


                //ssh_host =setup.Cloudstation.Customer[customer_index].Hosts[host_index].Mgmt_ip
                //ssh_username =setup.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_UN
                //ssh_password = setup.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_pwd
                //ssh_port = setup.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_port

		wget_url = "mail.google.com"
		wget_folder = "/home/lavelle/"
		wget_timeout = 10
                cs= rest_service.Cloudstation{
                        Base_url: url,
                        Username: login,
                        Password: password,
                        InitialiseInfo: true,
                }

                hub_site_name = setup.HubSiteName()
		hub_cpe_name  = setup.HubDeviceName()
		hub_cpe_info  = setup.HubDeviceInfo()

		cleanup_handler(cleanup)

	})

	Context("acl_policy_test", func() {

		It("acl_policy_test", func() {

			/* step 1: login to cs  */
			step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
			        error_reason = fmt.Sprintf("Failed to login to the CS %s\n", cs.Base_url)
				FailTest(error_reason)
			}
			defer LogoutFromCloudstation(&cs)
			lvn_log.Info.Printf("Successfully logged into the CS\n")




			/* Get list of active lan interfaces from CPE  */
			substep("Get list of active lan interfaces from CPE %s", hub_cpe_name)
			var cpe_ifaces rest_service.IFACE
			cpe_ifaces, res = cs.Get_IfaceList(hub_cpe_name)
			if !res {
				FailTest(fmt.Sprintf("Unable to get interface list from the CPE %s", hub_cpe_name))
			}

			cpe_lans := cpe_ifaces.GetUpPhyLans()
			if len(cpe_lans) == 0 {
                            error_reason = fmt.Sprintf("Unable to find active lans on CPE %s", hub_cpe_name)
                            FailTest(error_reason)
			}

			lan_iface, lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask := "", "", 0
			for _, lan := range cpe_lans {
				if ipv4_addr, ipv4_mask := lan.GetPrimaryV4Addr();  ipv4_addr != "" {
				    lan_iface = lan.IfaceName()
				    lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask = ipv4_addr, ipv4_mask
				    break
			    }
			}
			if lan_iface == "" || lan_iface_primary_v4_addr == "" {
			    FailTest(fmt.Sprintf("No active lans found on the site %s (cpe %s) with IPv4 address", hub_site_name, hub_cpe_name))
			}
			lvn_log.Info.Printf("Active lan on CPE %s on site %s is %s (%s)", hub_cpe_name, hub_site_name, lan_iface, lan_iface_primary_v4_addr)


			/* Get physical interface of the lan interface on CPEe */
			substep("Get physical interface of the lan interface %s on CPE %s", lan_iface, hub_cpe_name)
			Linkmap, _ := cs.GetCpeLinkMap(hub_cpe_name)
                        ReqCpePhyport := Linkmap[lan_iface][0]
			if ReqCpePhyport == "" {
				FailTest(fmt.Sprintf("Unable to get the physical interface for %s", lan_iface))
			}
			lvn_log.Info.Printf("The physical interface for the lan interface %s on CPE %s is %s", lan_iface, hub_cpe_name, ReqCpePhyport)

			/* Get host information of the site */
			substep("Get host information of the site %s", hub_site_name)
	                HostPhyInterface := ""
			HostName := ""
			node_type := ""
			for _, node := range hub_cpe_info.GetPhyInfo(ReqCpePhyport) {
                            if !node.IsViaL3() {
                                ssh_host, ssh_port, ssh_username, ssh_password, node_type = setup.GetNodeInfo(node.GetName())
				if node_type == "host" {
                                        HostPhyInterface = node.GetIface()
				        HostName = node.GetName()
				        break
				}
                            }
                        }
			if HostName == "" || HostPhyInterface == "" {
			    FailTest(fmt.Sprintf("No physical hosts found for the site %s", hub_site_name))
		        }
			lvn_log.Info.Printf("The physical host for the site %s is %s (interface: %s)", hub_site_name, HostName, HostPhyInterface)

                        host_session := host_utils.Credentials{Host: ssh_host, Port:ssh_port, User: ssh_username, Pswd: ssh_password}

                        //host_session.ExecRemoveDhcpIpFromHost(HostPhyInterface)
                        //host_session.ExecDhClientFromHost(HostPhyInterface)

			/* Get host information of the site */
			substep("Configuring IP address on the host %s interface %s", HostName, HostPhyInterface)
			var HostPhyInterfaceIP []string
			HostPhyInterfaceIP, err = utils.Get_ip_address_from_netaddr(fmt.Sprintf("%s/%d", lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask), 1, lan_iface_primary_v4_addr)
			if err != nil {
				FailTest(fmt.Sprintf("Failed to get ip address from the network address %s/%d", lan_iface_primary_v4_addr, lan_iface_primary_v4_addr_mask))
			}
		        _, err = host_session.ExecAddIpFromHost(HostPhyInterface,  HostPhyInterfaceIP[0], utils.NetMaskInt2String(lan_iface_primary_v4_addr_mask))
			if err != nil {
				error_reason = fmt.Sprintf("Failed to set Ip address %s on the interface %s on host %s", HostPhyInterfaceIP[0], HostPhyInterface, HostName)
				FailTest(error_reason)
			}
			lvn_log.Info.Printf("Successfully set Ip address %s on the interface %s on host %s", HostPhyInterfaceIP[0], HostPhyInterface, HostName)
		        host_session.AddDefaultRoute(lan_iface_primary_v4_addr)

			cpe1 = SSHToCPE(&cs, hub_cpe_name)
			cpe1.StartDebugOnCpe([]string{}, []string{}, true, true, true)
	                defer cpe1.CollectDebugInfoFromCpe()


			tcpdumps := make(map[string]*host_utils.Tcpdump)
			inet_wans := cs.GetInetWansFromCpe(hub_cpe_name)
		        lvn_log.Info.Println("inet wans", inet_wans)
			inet_primary_ipv4 := make(map[string]string)
			for _, wan_iface := range inet_wans {
				t := host_utils.NewTcpDump()
				t.Iface = wan_iface.Name
				t.Write_to_file = fmt.Sprintf("/home/lavelle/file-server-files/%s.txt", wan_iface.Name)
				t.EnableRedirectToFile()
				//tcpdumps = append(tcpdumps, t)
				tcpdumps[wan_iface.Name] = t
				inet_primary_ipv4[wan_iface.Name], _ = wan_iface.GetPrimaryV4Addr()
			}
                        //var w sync.WaitGroup
			var w1, w2 sync.WaitGroup
			for _, tcpdump := range tcpdumps {
                            //go cpe1.ExecTcpdump(tcpdump)
			    cpe1.StartTcpdump(tcpdump)
                            //w.Add(1)
                            //go func(tcpdump *host_utils.Tcpdump) {
                            //    defer w.Done()
                            //    cpe1.ExecTcpdump(tcpdump)
		            //}
			    //time.Sleep(3 * time.Second)
			}
			//w.Wait()


			step("Verify the application is accessible from the site before ACL rule\n")
			url_ips := []string{}
			res, _, url_ips = os_utils.DigShort(wget_url)
			if !res {
                            FailTest(fmt.Sprintf("Unable to resolve name for %s", wget_url))
			}
			if len(url_ips) == 0 {
                            FailTest(fmt.Sprintf("No IPs resolved for %s", wget_url))
			}

			res, err, _ = host_session.ExecWgetFromHost(url_ips[0], wget_folder, wget_timeout)

			for _, tcpdump := range tcpdumps {
                            w1.Add(1)
                            go cpe1.StopTcpdump(tcpdump, &w1)
			    //time.Sleep(3 * time.Second)
			}
			w1.Wait()
			    //time.Sleep(10 * time.Second)


			//analysers := []lvnanalyser.DumpAnalyser{}
			analysers := make(map[string]lvnanalyser.DumpAnalyser)
			for wan_name, tcpdump := range tcpdumps {
                            analysers[wan_name]  = *lvnanalyser.NewDumpAnalyser(tcpdump.CapturedPkts)
			}

			res = true
			total_pkts := 0
			for wan, analyser := range analysers {
				//lvn_log.Info.Printf("wna iface: %s wna ip %s", wan, inet_primary_ipv4[wan])
				if inet_primary_ipv4[wan] != "" {
				        pkt_counts := analyser.Http(inet_primary_ipv4[wan], url_ips[0])
					total_pkts = total_pkts + pkt_counts
					if pkt_counts > 0 {
                                            lvn_log.Info.Printf("HTTP traffic from %s to %s is sent (Pkts: %d) on interface %s", inet_primary_ipv4[wan], url_ips[0], pkt_counts, wan)
					} else {
                                            lvn_log.Info.Printf("HTTP traffic from %s to %s is not sent (Pkts: %d) on interface %s", inet_primary_ipv4[wan], url_ips[0], pkt_counts, wan)
					}
				}
			}

                        if err!=nil {
				FailTest(err.Error())
                        }
			/* Expecting any interface to send the traffic so just checking the sum */

                       if total_pkts == 0 || !res {
			        error_reason = fmt.Sprintf("Application is not accessible from the site\n")
				FailTest(error_reason)
			}

			lvn_log.Info.Printf("Application is accessible from the site\n")

			step("Create ACL policy %s", acl_name)
			res, _ = cs.CreateAclPolicy(acl_input)
			if res {
			lvn_log.Info.Printf("Successfully created the ACL policy %s\n", acl_name)
		        } else {
			error_reason = fmt.Sprintf("Failed to create the ACL policy\n")
			FailTest(error_reason)
                        }

			substep("Get ovs dumpflow details for ACL before attaching the ACL policy")
			if _, err = cpe1.GetOvsDumpFlowsForAcl(); err != nil {
                            lvn_log.Err.Printf("Failed to get the ACL dump flows\n")
			}

			//AttachAclPolicyAtSite(acl_input, hub_site_name)
			step("Attach the ACL policy %s at site %s - to deny the application\n", acl_name, hub_site_name)
                        res = cs.AttachAclPolicyAtSite(acl_name, hub_site_name)
			if !res {
			    error_reason = fmt.Sprintf("Failed to attach the ACL policy\n")
			    FailTest(error_reason)
			}
			lvn_log.Info.Printf("Successfully attached the ACL policy\n")

			tc_utils.WaitForConfigPull()

			substep("Get ovs dumpflow details for ACL after attaching the ACL policy")
			if _, err = cpe1.GetOvsDumpFlowsForAcl(); err != nil {
                            lvn_log.Err.Printf("Failed to get the ACL dump flows\n")
			}

			for _, tcpdump := range tcpdumps {
			    go cpe1.StartTcpdump(tcpdump)
			}

			step("Verify the application is not accessible from the site after ACL rule\n")
			res, err, _ = host_session.ExecWgetFromHost(url_ips[0], wget_folder, wget_timeout)

			for _, tcpdump := range tcpdumps {
			    w2.Add(1)
                            go cpe1.StopTcpdump(tcpdump, &w2)
			}
			w2.Wait()

			for wan_name, tcpdump := range tcpdumps {
                            analysers[wan_name]  = *lvnanalyser.NewDumpAnalyser(tcpdump.CapturedPkts)
			}

                        total_pkts = 0
			for wan, analyser := range analysers {
				//lvn_log.Info.Printf("wna iface: %s wna ip %s", wan, inet_primary_ipv4[wan])
				if inet_primary_ipv4[wan] != "" {
					pkt_counts := analyser.Http(inet_primary_ipv4[wan], url_ips[0])
					total_pkts = total_pkts + pkt_counts
					//pkt_counts_list = append(pkt_counts_list, pkt_counts)
					if pkt_counts > 0 {
                                            lvn_log.Info.Printf("HTTP traffic from %s to %s is sent (Pkts: %d) on interface %s", inet_primary_ipv4[wan], url_ips[0], pkt_counts, wan)
					} else {
                                            lvn_log.Info.Printf("HTTP traffic from %s to %s is not sent (Pkts: %d) on interface %s", inet_primary_ipv4[wan], url_ips[0], pkt_counts, wan)
					}
				}
			}


                        if err!=nil {
				FailTest(err.Error())
                        }

			substep("Get ovs dumpflow details for ACL after sending the traffic")
			if _, err = cpe1.GetOvsDumpFlowsForAcl(); err != nil {
                            lvn_log.Err.Printf("Failed to get the ACL dump flows\n")
			}

			/* Expecting no interface to send the traffic so just checking the sum */
                       if total_pkts != 0 || res {
			        error_reason = fmt.Sprintf("Application is still accessible from the site after the ACL rule - deny\n")
				FailTest(error_reason)
			}

			step("Detach the ACL policy %s at site %s", acl_name, hub_site_name)
                        res = cs.DetachAclPolicyFromSite(acl_name, hub_site_name)

			if !res {
			error_reason = fmt.Sprintf("Failed to detach the ACL policy %s from site %s\n", acl_name, hub_site_name)
			FailTest(error_reason)
		        }
			lvn_log.Info.Printf("Successfully detached the ACL policy %s from site %s\n", acl_name, hub_site_name)

			/*
			tc_utils.WaitForConfigPull()

			step("Verify the application is accessible from the site after detaching ACL rule\n")
			res, err, wget_op = host_session.ExecWgetFromHost(url_ips[0], wget_folder, wget_timeout)

			error_reason = fmt.Sprintf("Application is not accessible from the site\n")
			lvn_log.Info.Printf("Application is accessible from the site\n")
			*/

		})

	})
	AfterEach(func() {
		//cleanup()
	        //cpe1.CollectDebugInfoFromCpe()
		cleanup_handler(cleanup)
		if "" != error_reason {
			lvn_log.Err.Printf(error_reason)
		}
	})

})
