package customer_test

import (

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	//ginkgotypes "github.com/onsi/ginkgo/types"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        tc_utils "lvnautomation/testcaseutils"
        //utils "lvnautomation/utils"
	setup_utils "lvnautomation/setuputils"
	host_utils "lvnautomation/hostutils"
        lvntypes "lvnautomation/lvntypes"
	"fmt"
	"time"

)

//testcase_name = "trl_policy_test"
//test_purpose = "Verify the TRL policy at site level"
var _ = Describe(test_purpose, func() {

	var (
		url string
		login,  password string
		cs rest_service.Cloudstation
		site_name string
		trl_name string
		//device_name string

		//device_input *rest_service.DeviceInput
		//site_input  *rest_service.CreateSiteInput

		err error
		//tc_name string
		error_reason string = ""

		setup *setup_utils.Setup001
		trl_input *rest_service.CreateTrlPolicyInput

		iperf_server_host, iperf_server_ssh_username, iperf_server_ssh_password, iperf_server_ssh_port string
		iperf_client_host, iperf_client_ssh_username, iperf_client_ssh_password, iperf_client_ssh_port string

                iperf_port_no int
                is_udp_mode bool
                duration_sec int
                no_of_sessions int
                bandwidth lvntypes.BANDWIDTH
		rate_kbps lvntypes.BANDWIDTH

		trl_response *rest_service.CreateTrlResponse


                cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")

			//res = cs.DetachAclPolicyAtSite(acl_name, site_name)

			cs.Logout()
			return true
                }
	)

	BeforeEach(func() {

                /* Fetching info from setupfile */
		var res bool
		setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"
		res, setup = setup_utils.NewSetup001(setup_filename)
		if !res {

			fmt.Println(setup)
		}

                customer_index:=0
		net_admin_index:=0
		iperf_server_host_index:=0
		iperf_client_host_index:=2
		iperf_client_ip := "10.5.0.8"

                url = setup.Cloudstation.Customer[customer_index].Url
		login = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Username
		password = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Password
                /* Fetching info from setupfile - done */
                initialise()

		res, _, trl_input = rest_service.NewTrlInput()
		_, _, match_criteria_input := rest_service.NewMatchCriteriaInput()

		rate_kbps = lvntypes.BANDWIDTH(150)
		trl_direction := "from lan"
		trl_src_nwgrp := "Default_NetworkGroup_2"
		trl_dst_nwgrp := "Default_NetworkGroup_2"

		trl_input.SetRate(int(rate_kbps))

	        match_criteria_input.SetDirection(trl_direction)
                match_criteria_input.SetSrcNwGrp(trl_src_nwgrp)
	        match_criteria_input.SetDstNwGrp(trl_dst_nwgrp)
	        match_criteria_input.SetFromCustomIp(iperf_client_ip, "OR")
	        trl_input.SetMatchCriteria(match_criteria_input)

                iperf_server_host        =setup.Cloudstation.Customer[customer_index].Hosts[iperf_server_host_index].Mgmt_ip
                iperf_server_ssh_username=setup.Cloudstation.Customer[customer_index].Hosts[iperf_server_host_index].Ssh_UN
                iperf_server_ssh_password= setup.Cloudstation.Customer[customer_index].Hosts[iperf_server_host_index].Ssh_pwd
                iperf_server_ssh_port    = setup.Cloudstation.Customer[customer_index].Hosts[iperf_server_host_index].Ssh_port

                iperf_client_host        =setup.Cloudstation.Customer[customer_index].Hosts[iperf_client_host_index].Mgmt_ip
                iperf_client_ssh_username=setup.Cloudstation.Customer[customer_index].Hosts[iperf_client_host_index].Ssh_UN
                iperf_client_ssh_password= setup.Cloudstation.Customer[customer_index].Hosts[iperf_client_host_index].Ssh_pwd
                iperf_client_ssh_port    = setup.Cloudstation.Customer[customer_index].Hosts[iperf_client_host_index].Ssh_port

                iperf_port_no = 5001
                is_udp_mode   = true
                duration_sec  = 10
                no_of_sessions = 1
                bandwidth = rate_kbps + lvntypes.BANDWIDTH(100) //Sending at higher rate for TRL enforcement 


                cs= rest_service.Cloudstation{
                        Base_url: url,
                        Username: login,
                        Password: password,
                        InitialiseInfo: true,
                }


	})

	Context("trl_policy_test", func() {

                //lvn_log.Info.
		It("trl_policy_test", func() {
			step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			error_reason = fmt.Sprintf("Failed to login to the CS %s\n", cs.Base_url)
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf ("Successfully logged into the CS\n")

			iperf_server := host_utils.Credentials{Host: iperf_server_host,
			                                       Port: iperf_server_ssh_port,
							       User: iperf_server_ssh_username,
							       Pswd: iperf_server_ssh_password}

			iperf_client := host_utils.Credentials{Host: iperf_client_host,
			                                       Port: iperf_client_ssh_port,
							       User: iperf_client_ssh_username,
							       Pswd: iperf_client_ssh_password}

			cpe1 := host_utils.Credentials{Host:  "192.168.10.21",
			                                       Port: "22",
							       User: "lavelle",
							       Pswd: "lavelle@3163"}

                        cpe1.StartDebugOnCpe([]string{}, []string{}, true, true, true)
			step("Create TRL policy\n")
			if res, _, trl_response = cs.CreateTRLPolicy(trl_input); !res {
				fmt.Println("error in creatring trl polucy")
			}

                        trl_name = trl_response.Data.Traffic_rate_limiter.Name
			//fmt.Println(trl_name)
			error_reason = fmt.Sprintf("Failed to create the TRL policy\n")
                        if err!=nil {
                        }
			lvn_log.Info.Printf("Successfully created the TRL policy %s\n", trl_name)

			//acl_name = "DropGmail"
			site_name = "5022"
			step("Verify that before applying the TRL policy, there is no restriction in traffic rate\n")

			iperf_server_ip_connected_to_cpe_lan := "10.3.0.5"

		        iperf_server_config, iperf_client_config := host_utils.NewIperfTest(iperf_port_no, is_udp_mode, duration_sec, no_of_sessions, int(bandwidth))
                        iperf_server.StartIperfServer(iperf_server_config)
                        time.Sleep(1 * time.Second)
                        iperf_client.StartIperfClient(iperf_server_ip_connected_to_cpe_lan, iperf_client_config)

                        time.Sleep(time.Duration(duration_sec) * time.Second)

                        iperf_server.StopIperfServer()
                        op, _ := iperf_client.StopIperfClient()
                        observed_bandwidth := lvntypes.BANDWIDTH(op)
			lvn_log.Info.Printf("Observed bandwidth : %f\n", observed_bandwidth)

                        if !observed_bandwidth.WithinRange(bandwidth.GetPlusOrMinusPercent(1.0)) {
                            lvn_log.Err.Printf("Traffic passed across the sites are not within expected rate. Observed: %f Kbps, Expected: %f Kbps\n", observed_bandwidth, bandwidth)
			    res  = false
                        } else {
			    lvn_log.Info.Printf("The rate expected and observed are within acceptable range\n")
			    res  = true
			}
			//Expect(res).Should(BeTrue())

			go cpe1.GetOvsDumpFlowsEvery(1)
			step("Attach the TRL policy at site level - to limit the rate\n")
                        res = cs.AttachTrlAtSite(trl_name, site_name)
			error_reason = fmt.Sprintf("Failed to attach the TRL policy\n")
			lvn_log.Info.Printf("Successfully attached the TRL policy\n")

			tc_utils.WaitForConfigPull()

			step("Verify the application is rate limited after applying TRL rule\n")
                        iperf_server.StartIperfServer(iperf_server_config)
                        time.Sleep(1 * time.Second)
                        iperf_client.StartIperfClient(iperf_server_ip_connected_to_cpe_lan, iperf_client_config)

                        time.Sleep(time.Duration(duration_sec) * time.Second)

                        iperf_server.StopIperfServer()
                        op, _ = iperf_client.StopIperfClient()
                        observed_bandwidth = lvntypes.BANDWIDTH(op)
			success_reason := "Traffic is limited after applying TRL policy"
			actual_reason := "Traffic is limited after applying TRL policy"
			lvn_log.Info.Printf("Observed bandwidth : %f kbps\n", observed_bandwidth)
                        if observed_bandwidth.WithinRange(rate_kbps.GetPlusOrMinusPercent(1.0)) {
                            fmt.Println("After applying TRL policy the rate is actually limited")
                        } else {
				actual_reason = fmt.Sprintf("Traffic is not limited after applying TRL policy. Expected : %f kbps, Observed: %f kbps", rate_kbps, observed_bandwidth)
			}

			op2, _ := cpe1.StopOvsDumpFlows()
			fmt.Println(op2)

			step("Detach the TRL policy at site level\n")
                        res = cs.DetachTrlAtSite(trl_name, site_name)
                        cs.DeleteTrlPolicy(trl_name)
			lvn_log.Info.Printf("Successfully detached the TRL policy\n")

			tc_utils.WaitForConfigPull()

			step("Verify the application is not rate limited after detaching TRL rule\n")

                        iperf_server.StartIperfServer(iperf_server_config)
                        time.Sleep(1 * time.Second)
                        iperf_client.StartIperfClient(iperf_server_ip_connected_to_cpe_lan, iperf_client_config)

                        time.Sleep(time.Duration(duration_sec) * time.Second)

                        iperf_server.StopIperfServer()
                        op, _ = iperf_client.StopIperfClient()
                        observed_bandwidth = lvntypes.BANDWIDTH(op)
			lvn_log.Info.Printf("Observed bandwidth : %f kbps\n", observed_bandwidth)
                        if observed_bandwidth.WithinRange(bandwidth.GetPlusOrMinusPercent(1.0)) {
                            fmt.Println("Normally traffic is passing through without TRL policy")
                        }
			cpe1.CollectDebugInfoFromCpe()

			step("Logout from the CS\n")
			_, res = cs.Logout()
			error_reason = fmt.Sprintf("Failed to logout from the CS\n")
			lvn_log.Info.Printf("Successfully logged out from the CS\n")
			Expect(actual_reason).Should(Equal(success_reason))
		})

	})
	AfterEach(func() {
		//cleanup()
		cleanup_handler(cleanup)
		if "" != error_reason {
			lvn_log.Err.Printf(error_reason)
		}
	})

})
