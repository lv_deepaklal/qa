package customer_test

import (

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        tc_utils "lvnautomation/testcaseutils"
        //utils "lvnautomation/utils"
	setup_utils "lvnautomation/setuputils"
	host_utils "lvnautomation/hostutils"
	"fmt"

)

var _ = Describe("Verifying the DHCP Configuraetion", func() {

	var (
		url string
		login,  password string
		cs rest_service.Cloudstation
		
		 i int
		 j int
		HostPhyInterface string
		ip string
		err error
		
		
		error_reason string = ""

		setup *setup_utils.Setup001
		dhcpInput *rest_service.DhcpServerInput

		ssh_host, ssh_username, ssh_password, ssh_port_no string
                host_credentials host_utils.Credentials
	
	
		customer_index int
		host_index int
		net_admin_index int
                cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")
			_,err = host_credentials.ExecRemoveIpFromHost("p4p1")

			res = cs.Del_Dhcpserver("5078", "lan0")

			cs.Logout()
			return true
                }
	)
	BeforeEach(func() {

				/* Fetching info from setupfile */
		var res bool
		//setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"
		setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"

		res, setup = setup_utils.NewSetup001(setup_filename)
		if !res {

			fmt.Println(setup)
		}

		customer_index=0
		net_admin_index=0
		host_index=0

				url = setup.Cloudstation.Customer[customer_index].Url
		login = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Username
		password = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Password
				/* Fetching info from setupfile - done */

		res, _, dhcpInput = rest_service.NewDhcpServerInput()
		dhcpInput.CpeName = "5078"
		dhcpInput.InterfaceName = "lan0"
		dhcpInput.Dns = "8.8.8.8,8.8.4.4"
		dhcpInput.StartIP = "10.3.0.1"
		dhcpInput.StopIP = "10.3.0.240"
				ssh_host =setup.Cloudstation.Customer[customer_index].Hosts[host_index].Mgmt_ip
				ssh_username =setup.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_UN
				ssh_password = setup.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_pwd
				ssh_port_no = setup.Cloudstation.Customer[customer_index].Hosts[host_index].Ssh_port

			
				cs= rest_service.Cloudstation{
						Base_url: url,
						Username: login,
						Password: password,
						InitialiseInfo: true,
				}
			
		
		
		
				initialise()

})

Context("", func() {

		//lvn_log.Info.
It("dhcp", func() {
	step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
	_, res, _ := cs.Login()
	error_reason = fmt.Sprintf("Failed to login to the CS %s\n", cs.Base_url)
	Expect(res).Should(BeTrue())
	lvn_log.Info.Printf ("Successfully logged into the CS\n")
	host_credentials = host_utils.Credentials{Host: ssh_host, Port:ssh_port_no, User: ssh_username, Pswd: ssh_password}
	 
	step("Configuring DHCP Service \n")
				res = cs.Conf_Dhcpserver(*dhcpInput)
	error_reason = fmt.Sprintf("Failed to Configure DHCP Server\n")
	Expect(res).Should(BeTrue())
				if err!=nil {
				}
	lvn_log.Info.Printf("Successfully configure DHCP Server\n")
	tc_utils.WaitForConfigPull()


	var Linkmap map[string][]string
	Linkmap,res=cs.GetCpeLinkMap(dhcpInput.CpeName)
	ReqCpePhyport := Linkmap[dhcpInput.InterfaceName][0]

	for i = 0; i < len(setup.Cloudstation.Customer[customer_index].Devices); i++ {
		if setup.Cloudstation.Customer[customer_index].Devices[i].Name == dhcpInput.CpeName {
			for j = 0; j < len(setup.Cloudstation.Customer[customer_index].Devices[i].IfaceMap); j++ {
				if setup.Cloudstation.Customer[customer_index].Devices[i].IfaceMap[j].Cpe == ReqCpePhyport {
					HostPhyInterface = setup.Cloudstation.Customer[customer_index].Devices[i].IfaceMap[j].Host_iface
				}
			}
		}
	}


	step("Removing IP for interface\n")
				_,err = host_credentials. ExecRemoveIpFromHost(HostPhyInterface)
	if err!=nil{
	error_reason = fmt.Sprintf("Unable to remove ip from host's interface\n")
	}
/*	Expect(res).Should(BeTrue())
				if err!=nil {
				}
				*/
	lvn_log.Info.Printf("Remove ip from host's interface\n")

	
	step("Executing dhclient\n")
				_,err= host_credentials.ExecDhClientFromHost(HostPhyInterface)
	if  err!=nil {
	error_reason = fmt.Sprintf("Failed to execute dhclient on host\n")
	}
//      Expect(res).Should(BeTrue())
	lvn_log.Info.Printf("Successfully executed dhclient on host\n")

	
	
	step("Getting ip from the interface\n")
				ip,_,err = host_credentials.ExecIfconfigFromHost(HostPhyInterface)
	if err!=nil{
	error_reason = fmt.Sprintf("Failed to get the ip from host's interface\n")
	}
/*	Expect(res).Should(BeFalse())
				if err!=nil {
				}
				*/
//	error_reason = fmt.Sprintf("Got ip f\n")

	
	step("Logout from the CS\n")
	_, res = cs.Logout()
	error_reason = fmt.Sprintf("Failed to logout from the CS\n")
	Expect(res).Should(BeTrue())
	lvn_log.Info.Printf("Successfully logged out from the CS\n")
	
})
})
AfterEach(func() {
                //cleanup()
		cleanup_handler(cleanup)
                //if "" != error_reason {
                //      lvn_log.Err.Printf(error_reason)
                //}
        })

})


