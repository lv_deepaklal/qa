package customer_test

import (

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	//ginkgotypes "github.com/onsi/ginkgo/types"
	lvn_log "lvnautomation/lvnlog"
        rest_service "lvnautomation/restservice"
        //tc_utils "lvnautomation/testcaseutils"
        //utils "lvnautomation/utils"
	setup_utils "lvnautomation/setuputils"
	//host_utils "lvnautomation/hostutils"
        //lvntypes "lvnautomation/lvntypes"
	"fmt"
	//"time"

)

//testcase_name = "maximum_custom_app"
//test_purpose = "Verify the maximum custom app creation on CS"
var _ = Describe("Verify the maximum custom app creation on CS", func() {

	var (
		url string
		login,  password string
		cs rest_service.Cloudstation

		max_apps_supported int

		//err error
		//tc_name string
		error_reason string = ""

		setup *setup_utils.Setup001

                cleanup func() bool = func() bool {

			lvn_log.Info.Printf("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			if !res {
				return res
			}
			lvn_log.Info.Printf("Successfully logged into the CS\n")

			//res = cs.DetachAclPolicyAtSite(acl_name, site_name)

			cs.Logout()
			return true
                }
	)

	BeforeEach(func() {

                /* Fetching info from setupfile */
		var res bool
		setup_filename := "/home/lavelle/deepak/ginkgo/setup_info.json"
		res, setup = setup_utils.NewSetup001(setup_filename)
		if !res {

			fmt.Println(setup)
		}
		max_apps_supported = 2048

                customer_index:=0
		net_admin_index:=0

                url = setup.Cloudstation.Customer[customer_index].Url
		login = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Username
		password = setup.Cloudstation.Customer[customer_index].Credentials.Netadmin[net_admin_index].Password
                /* Fetching info from setupfile - done */
                initialise()

                cs= rest_service.Cloudstation{
                        Base_url: url,
                        Username: login,
                        Password: password,
                        InitialiseInfo: true,
                }


	})

	Context("maximum_custom_apps_test", func() {

                //lvn_log.Info.
		It("maximum_custom_apps_test", func() {

			/* 1. Login to the CS as net-admin */
			/* 2. Get default app list and custom app list */
			/* 3. For max custom apps supported, create a custom app */

			step("Logging to the CS %s as %s\n", cs.Base_url, cs.Username)
			_, res, _ := cs.Login()
			error_reason = fmt.Sprintf("Failed to login to the CS %s\n", cs.Base_url)
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf ("Successfully logged into the CS\n")

			no_of_custom_apps_to_be_supported := max_apps_supported

			if apps_wrt_name, ok := cs.GetDefaultAppList(); ok {
                            no_of_custom_apps_to_be_supported = no_of_custom_apps_to_be_supported - len(apps_wrt_name) - 1 //Hidden DNS app
			} else {
			}

			if apps_wrt_name, ok := cs.GetCustomAppList(); ok {
                            no_of_custom_apps_to_be_supported = no_of_custom_apps_to_be_supported - len(apps_wrt_name)
			} else {
			}

			lvn_log.Info.Printf("No. of custom apps expected to be supported: %d\n", no_of_custom_apps_to_be_supported)

			apps_created := []int{}
			var i int
			//no_of_custom_apps_to_be_supported = 2


			custom_app_input := rest_service.NewCustomApp()
			custom_app_input.SetIdentificationTypeNetworkTuple()
			port_no := 65534

			for i=0; i<no_of_custom_apps_to_be_supported; i++ {
			        custom_app_input.SetPort(port_no - i)
			        custom_app_input.Name = fmt.Sprintf("custom_app_test_%d", i+1)
				if res, _, create_app_resp := cs.CreateCustomApp(custom_app_input); res {
					fmt.Println(create_app_resp)
					apps_created = append(apps_created, create_app_resp.Data.Applications.Id)
				} else {
					break
				}
			}

			if i+1 <no_of_custom_apps_to_be_supported {
				lvn_log.Err.Printf("No of apps created: %d, but expected count: %d\n", i+1, no_of_custom_apps_to_be_supported)
			} else {
				lvn_log.Info.Printf("No of apps created: %d\n", i+1)
			}

			for _, app_id := range apps_created {
			      cs.DeleteCustomAppById(app_id)
			}

			step("Logout from the CS\n")
			_, res = cs.Logout()
			error_reason = fmt.Sprintf("Failed to logout from the CS\n")
			Expect(res).Should(BeTrue())
			lvn_log.Info.Printf("Successfully logged out from the CS\n")
		})

	})
	AfterEach(func() {
		//cleanup()
		cleanup_handler(cleanup)
		if "" != error_reason {
			lvn_log.Err.Printf(error_reason)
		}
	})

})
