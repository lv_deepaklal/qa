package customer_test

import (
	. "github.com/onsi/gomega"
	"fmt"
	"regexp"
	"errors"
        "strings"
	"strconv"
        //tc_utils "lvnautomation/testcaseutils"
        os_utils "lvnautomation/osutils"
        host_utils "lvnautomation/hostutils"
        lvn_log "lvnautomation/lvnlog"
	//lvn_reports "lvnautomation/lvnreports"
	rest_service "lvnautomation/restservice"
	//setup_utils "lvnautomation/setuputils"
	new_setup_utils "lvnautomation/newsetuputils"
	matcher "lvnautomation/lvnmatcher"
	utils "lvnautomation/utils"
)

func SSHToCPE(cs *rest_service.Cloudstation, cpe_name string) (*host_utils.Credentials) {

	var cpe_ifaces rest_service.IFACE
	var res bool
	var err error
	var cpe_inet_wans []rest_service.Interface
	var cpe_session *host_utils.Credentials

	cpe_ifaces, res = cs.Get_IfaceList(cpe_name)
	if !res {
		return cpe_session
	}
        cpe_inet_wans  = cpe_ifaces.GetInetWans()

	cpe_inet_ip_addr := []string{}

	for _, cpe_inet_wan := range cpe_inet_wans  {
            addr, _ := cpe_inet_wan.GetPrimaryV4Addr()
	    if addr != "" {
		    cpe_inet_ip_addr = append(cpe_inet_ip_addr, addr)
	    }
	}

	cpe_connected := false

	for _, host_addr := range cpe_inet_ip_addr {
            lvn_log.Debug.Printf("Trying to connect to CPE %s using address %s\n", cpe_name, host_addr)
            cpe_session = &host_utils.Credentials{Host:  host_addr,
                                           Port: "22",
                                           User: "lavelle",
                                           Pswd: "lavelle@3163"}
            cpe_session.Persistent = true
	    _, err = cpe_session.Connect()
	    if err == nil {
	            cpe_connected = true
		    break
	    }
	}

	if cpe_connected {
		lvn_log.Debug.Printf("Successfully connected to CPE %s\n", cpe_name)
	}
	return cpe_session
}

func ValidateInetPath(cs *rest_service.Cloudstation, cpe1 string, cpe2 string) (bool, error, [][]string, [][]string) {
    return ValidatePath(cs, cpe1, cpe2, "inet")
}

func ValidateMplsPath(cs *rest_service.Cloudstation, cpe1 string, cpe2 string) (bool, error, [][]string, [][]string) {
    return ValidatePath(cs, cpe1, cpe2, "mpls")
}

func ValidatePath(cs *rest_service.Cloudstation, cpe1 string, cpe2 string, wantype string) (bool, error, [][]string, [][]string) {


	lvn_log.Debug.Printf("Validating the paths between %s and %s", cpe1, cpe2)
	var cpe1_is_hub, cpe2_is_hub bool
	var err error
	var res bool
	var get_hub bool = false
	var cpe1_ifaces, cpe2_ifaces rest_service.IFACE
	var up_paths [][]string
	var down_paths [][]string

	if cpe1_is_hub, err = cs.IsThisCPEHub(cpe1); err != nil {
		return false, errors.New(fmt.Sprintf("Failed to get info about %s\n", cpe1)), up_paths, down_paths
	}

	if cpe2_is_hub, err = cs.IsThisCPEHub(cpe2); err != nil {
		return false, errors.New(fmt.Sprintf("Failed to get info about %s\n", cpe2)), up_paths, down_paths
	}

	if (cpe1_is_hub && cpe2_is_hub) {
		// Both are hub, wrong scenario
		return false, errors.New(fmt.Sprintf("Both %s and %s cannot be hub\n", cpe2)), up_paths, down_paths
	}

	if !(cpe1_is_hub || cpe2_is_hub) {
		// Both are not hub, so find hub
           get_hub = true
	}
	fmt.Println(get_hub)

	cpe1_ifaces, res = cs.Get_IfaceList(cpe1)
	if !res {
		return false, errors.New(fmt.Sprintf("Failed to get Iface details of CPE %s\n", cpe1)), up_paths, down_paths
	}

	cpe2_ifaces, res = cs.Get_IfaceList(cpe2)
	if !res {
		return false, errors.New(fmt.Sprintf("Failed to get Iface details of CPE %s\n", cpe2)), up_paths, down_paths
	}

	var cpe1_wans []rest_service.Interface
	var cpe2_wans []rest_service.Interface
//	var cpe1_inet_wans []rest_service.Interface
//	var cpe2_inet_wans []rest_service.Interface

	if wantype == "inet" {

		cpe1_wans  = cpe1_ifaces.GetInetWans()
		cpe2_wans  = cpe2_ifaces.GetInetWans()

		//cpe1_inet_wans = cpe1_wans
		//cpe2_inet_wans = cpe2_wans

	} else if wantype == "mpls" {

		cpe1_wans  = cpe1_ifaces.GetMplsWans()
		cpe2_wans  = cpe2_ifaces.GetMplsWans()

		//cpe1_inet_wans  = cpe1_ifaces.GetInetWans()
		//cpe2_inet_wans  = cpe2_ifaces.GetInetWans()

	} else {
	}

	cpe1_ip_addr := []string{}
	cpe2_ip_addr := []string{}

	// IP address for Paths
	for _, cpe1_wan := range cpe1_wans  {
            addr, _ := cpe1_wan.GetPrimaryV4Addr()
	    if addr != "" {
		    cpe1_ip_addr = append(cpe1_ip_addr, addr)
	    }
	}

	for _, cpe2_wan := range cpe2_wans  {
            addr, _ := cpe2_wan.GetPrimaryV4Addr()
	    if addr != "" {
		    cpe2_ip_addr = append(cpe2_ip_addr, addr)
	    }
	}

	// INET  IP address for connecting to CPE
	cpe_session := SSHToCPE(cs, cpe1)

	if cpe_session == nil {

            lvn_log.Debug.Printf("Unable to connect to CPE %s\n", cpe1)
	    cpe_session = SSHToCPE(cs, cpe2)

	    // Swapping the IP address
	    temp := cpe2_ip_addr
	    cpe1_ip_addr = cpe2_ip_addr
	    cpe2_ip_addr = temp
	}

	// Unable to open session to the inet interface
	if cpe_session == nil {
		return false, errors.New(fmt.Sprintf("Unable to connect to both CPEs %s, %s\n", cpe1, cpe2)), up_paths, down_paths
	}

	// Always use cpe1 to connect to check path status. For consistency
	var e error
	res, up_paths, down_paths = cpe_session.IsPathsUp(cpe1_ip_addr, cpe2_ip_addr)
	cpe_session.Close()

	if !res && len(down_paths) > 0 {
           var down_path_info []string
           for _, down_path := range down_paths {
               down_path_info = append(down_path_info, strings.Join(down_path, "-"))
	   }
           e = errors.New(fmt.Sprintf("Paths are down: %s", strings.Join(down_path_info, ", ")))
	}

	if len(up_paths) == 0 && len(down_paths) == 0 {
		e = errors.New(fmt.Sprintf("No path between %s and %s\n", cpe1, cpe2))
	}
	return res, e, up_paths, down_paths
}

func OLDValidatePath(cs *rest_service.Cloudstation, cpe1 string, cpe2 string, wantype string) (bool, error, [][]string, [][]string) {


	lvn_log.Debug.Printf("Validating the paths between %s and %s", cpe1, cpe2)
	var cpe1_is_hub, cpe2_is_hub bool
	var err error
	var res bool
	var get_hub bool = false
	var cpe1_ifaces, cpe2_ifaces rest_service.IFACE
	var up_paths [][]string
	var down_paths [][]string

	if cpe1_is_hub, err = cs.IsThisCPEHub(cpe1); err != nil {
		return false, errors.New(fmt.Sprintf("Failed to get info about %s\n", cpe1)), up_paths, down_paths
	}

	if cpe2_is_hub, err = cs.IsThisCPEHub(cpe2); err != nil {
		return false, errors.New(fmt.Sprintf("Failed to get info about %s\n", cpe2)), up_paths, down_paths
	}

	if (cpe1_is_hub && cpe2_is_hub) {
		// Both are hub, wrong scenario
		return false, errors.New(fmt.Sprintf("Both %s and %s cannot be hub\n", cpe2)), up_paths, down_paths
	}

	if !(cpe1_is_hub || cpe2_is_hub) {
		// Both are not hub, so find hub
           get_hub = true
	}
	fmt.Println(get_hub)

	cpe1_ifaces, res = cs.Get_IfaceList(cpe1)
	if !res {
		return false, errors.New(fmt.Sprintf("Failed to get Iface details of CPE %s\n", cpe1)), up_paths, down_paths
	}

	cpe2_ifaces, res = cs.Get_IfaceList(cpe2)
	if !res {
		return false, errors.New(fmt.Sprintf("Failed to get Iface details of CPE %s\n", cpe2)), up_paths, down_paths
	}

	var cpe1_wans []rest_service.Interface
	var cpe2_wans []rest_service.Interface
	var cpe1_inet_wans []rest_service.Interface
	var cpe2_inet_wans []rest_service.Interface

	if wantype == "inet" {

		cpe1_wans  = cpe1_ifaces.GetInetWans()
		cpe2_wans  = cpe2_ifaces.GetInetWans()

		cpe1_inet_wans = cpe1_wans
		cpe2_inet_wans = cpe2_wans

	} else if wantype == "mpls" {

		cpe1_wans  = cpe1_ifaces.GetMplsWans()
		cpe2_wans  = cpe2_ifaces.GetMplsWans()

		cpe1_inet_wans  = cpe1_ifaces.GetInetWans()
		cpe2_inet_wans  = cpe2_ifaces.GetInetWans()

	} else {
	}

	cpe1_ip_addr := []string{}
	cpe2_ip_addr := []string{}

	// IP address for Paths
	for _, cpe1_wan := range cpe1_wans  {
            addr, _ := cpe1_wan.GetPrimaryV4Addr()
	    if addr != "" {
		    cpe1_ip_addr = append(cpe1_ip_addr, addr)
	    }
	}

	for _, cpe2_wan := range cpe2_wans  {
            addr, _ := cpe2_wan.GetPrimaryV4Addr()
	    if addr != "" {
		    cpe2_ip_addr = append(cpe2_ip_addr, addr)
	    }
	}

	// INET  IP address for connecting to CPE
	cpe1_inet_ip_addr := []string{}
	cpe2_inet_ip_addr := []string{}

	for _, cpe1_inet_wan := range cpe1_inet_wans  {
            addr, _ := cpe1_inet_wan.GetPrimaryV4Addr()
	    if addr != "" {
		    cpe1_inet_ip_addr = append(cpe1_inet_ip_addr, addr)
	    }
	}
	for _, cpe2_inet_wan := range cpe2_inet_wans  {
            addr, _ := cpe2_inet_wan.GetPrimaryV4Addr()
	    if addr != "" {
		    cpe2_inet_ip_addr = append(cpe2_inet_ip_addr, addr)
	    }
	}

	cpe_connected := false

	var cpe_session host_utils.Credentials

	for _, host_addr := range cpe1_inet_ip_addr {
            lvn_log.Debug.Printf("Trying to connect to CPE %s using address %s\n", cpe1, host_addr)
            cpe_session = host_utils.Credentials{Host:  host_addr,
                                           Port: "22",
                                           User: "lavelle",
                                           Pswd: "lavelle@3163"}
            cpe_session.Persistent = true
	    _, err = cpe_session.Connect()
	    if err == nil {
	            cpe_connected = true
		    break
	    }
	}

	if !cpe_connected {

            lvn_log.Debug.Printf("Unable to connect to CPE %s\n", cpe1)
	    for _, host_addr := range cpe2_inet_ip_addr {
                lvn_log.Debug.Printf("Trying to connect to CPE %s using address %s\n", cpe2, host_addr)
                cpe_session = host_utils.Credentials{Host:  host_addr,
                                           Port: "22",
                                           User: "lavelle",
                                           Pswd: "lavelle@3163"}
                cpe_session.Persistent = true
	        _, err = cpe_session.Connect()
	        if err == nil {
	            cpe_connected = true
		    break
	        }
	    }

	    // Swapping the IP address
	    temp := cpe2_ip_addr
	    cpe1_ip_addr = cpe2_ip_addr
	    cpe2_ip_addr = temp
	}

	// Unable to open session to the inet interface
	if !cpe_connected {
		return false, errors.New(fmt.Sprintf("Unable to connect to both CPEs %s, %s\n", cpe1, cpe2)), up_paths, down_paths
	}

	// Always use cpe1 to connect to check path status. For consistency
	var e error
	res, up_paths, down_paths = cpe_session.IsPathsUp(cpe1_ip_addr, cpe2_ip_addr)
	cpe_session.Close()

	if !res && len(down_paths) > 0 {
           var down_path_info []string
           for _, down_path := range down_paths {
               down_path_info = append(down_path_info, strings.Join(down_path, "-"))
	   }
           e = errors.New(fmt.Sprintf("Paths are down: %s", strings.Join(down_path_info, ", ")))
	}

	if len(up_paths) == 0 && len(down_paths) == 0 {
		e = errors.New(fmt.Sprintf("No path between %s and %s\n", cpe1, cpe2))
	}
	return res, e, up_paths, down_paths
}

func IsAnyInetPathUpBetween(cs *rest_service.Cloudstation, cpe1 string, cpe2 string) bool {
	_, _, up_paths, _:= ValidateInetPath(cs, cpe1, cpe2)

	return len(up_paths) > 0
}

func AreAllInetPathsUpBetween(cs *rest_service.Cloudstation, cpe1 string, cpe2 string) bool {
	res, _, _, down_paths := ValidateInetPath(cs, cpe1, cpe2)
	return !(len(down_paths) > 0) && res
}

func IsInetReachableFromCPE(cs *rest_service.Cloudstation, cpe_name string) bool {

	ip := "8.8.8.8"
	//cpes_name := cs.Helper.device.bySiteName[site_name]
	cpe_session := SSHToCPE(cs, cpe_name)
	res, _ := cpe_session.IsReachableFromLanNamespace(ip)
	//fmt.Println("IsInetReachableFromCPE... res", res)
	if !res {
            lvn_log.Err.Printf("Internet is not reachable from the CPE %s\n", cpe_name)
        }
	cpe_session.Close()
	return res
}

func IsInetReachableFromSite(cs *rest_service.Cloudstation, site_name string) bool {

	cpes_name := cs.DevicesBySiteName()[site_name]
	return IsInetReachableFromCPE(cs, cpes_name[0].Name)
}

func FindOvsRulesHit(prev []string, next []string) (bool, []bool, []int, []int, []string) {

	is_hit := []bool{}
	pkts_count := []int{}
	bytes_count := []int{}
	rules_hit := []string{}

	if len(prev) != len(next) {
		return false, is_hit, pkts_count, bytes_count, rules_hit
	}

	var prev_pkts, prev_bytes, next_pkts, next_bytes int
	var err error
	//n_packets=3861, n_bytes=4803108
	re := regexp.MustCompile(`n_packets=(\d+),\s+n_bytes=(\d+)`)

	// Matching 1 to 1 assuming rule is not changed

	for  i:=0; i<len(prev); i++ {
           prev_data := re.FindAllStringSubmatch(prev[i], -1)
	   next_data := re.FindAllStringSubmatch(next[i], -1)

	   if len(prev_data) == 1 && len(next_data) == 1 {
		   if len(prev_data[0]) == 3 && len(next_data[0]) == 3 {
                       if prev_pkts, err = strconv.Atoi(prev_data[0][1]); err != nil {
			       continue
		       }
                       if prev_bytes, err = strconv.Atoi(prev_data[0][2]); err != nil {
			       continue
		       }
                       if next_pkts, err = strconv.Atoi(next_data[0][1]); err != nil {
			       continue
		       }
                       if next_bytes, err = strconv.Atoi(next_data[0][2]); err != nil {
			       continue
		       }

		       //diff_pkts := next_pkts - prev_pkts
		       if next_pkts - prev_pkts > 0 {
			       is_hit = append(is_hit, true)
			       pkts_count = append(pkts_count, next_pkts - prev_pkts)
			       bytes_count = append(bytes_count, next_bytes - prev_bytes)
			       rules_hit = append(rules_hit, next[i])
		       }
		   }

	   } else {
		   continue
	   }
	}
	return true, is_hit, pkts_count, bytes_count, rules_hit
}

func AttachAclPolicy(cs *rest_service.Cloudstation) {
	/*
    res, acl_response := cs.CreateAclPolicy(acl_input)
                        acl_name = acl_response.Data.Acls.Name
                        fmt.Println(acl_name)
                        error_reason = fmt.Sprintf("Failed to create the ACL policy\n")
                        if err!=nil {
                        }
                        lvn_log.Info.Printf("Successfully created the ACL policy\n")
*/
}

func GetAddedDeletedOvsFlows(prev []string, current []string) ([]string, []string) {
	return utils.GetDeletedAddedStrings(prev, current)
}

func LogoutFromCloudstation(cs *rest_service.Cloudstation) {
	step("Logout from the CS\n")
	err, res := cs.Logout()
	if res {
            lvn_log.Info.Printf("Successfully logged out from the CS")
	} else {
           lvn_log.Err.Printf("Failed logging out from the CS. (reason: %s)", err.Error())
	}
}

/*
func GetSSHHandlerForHost(host *setuputils.Hostinfo) *host_utils.Credentials {
    return &host_utils.Credentials {
                Host: host.Mgmt_ip,
                Port: host.Ssh_port,
                User: host.Ssh_UN,
                Pswd: host.Ssh_pwd,
           }
}
*/

func PrintTCResult() {
    if tc_result == "FAIL" {
        lvn_log.Err.Printf("####################################################################")
	lvn_log.Err.Printf("# TESTCASE FAILED - %s", tc_result_reason)
        lvn_log.Err.Printf("####################################################################")
    } else {
        lvn_log.Err.Printf("####################################################################")
	lvn_log.Err.Printf("#                     TESTCASE PASSED                              #")
        lvn_log.Err.Printf("####################################################################")
    }
}

func FailTest(reason string) {
	tc_result = "FAIL"
	tc_result_reason = strings.TrimSpace(reason)
	if tc_result_reason == "" {
		tc_result_reason = "<Reason not provided>"
	}

        lvn_log.Err.Printf(tc_result_reason)
	Expect(fmt.Sprintf("(STEP %d) %s", step_no, tc_result_reason)).Should(matcher.LvnAutoMatcherObj(""))
}

func EnsureSpecificRouteToAutomationServerOnHostSetup1(host_session *host_utils.Credentials, setup *new_setup_utils.Setup001) (error) {

	var default_route, onlink_routes, routes map[string]string
	var _, onlink_routes_on_spoke1_host, routes_on_spoke1_host map[string]string
	var err error
	ssh_host := host_session.Host
	var is_ip_in_nw bool

	substep("Getting the routes on the automation server")
	default_route, onlink_routes, routes, err = os_utils.IpRouteShow()
	if err != nil {
		return errors.New("Failed to get the route on automation server")
	}

	substep("Getting the routes on the host machine %s", host_session.Host)
	_, onlink_routes_on_spoke1_host, routes_on_spoke1_host, err = host_session.IpRouteShow()
	if err != nil {
		return errors.New(fmt.Sprintf("Failed to get the route information from the host %s", host_session.Host))
	}

	/* Check specifiv reverse route to automation server is present in the remote host, else add it */
	/* select source nw on automation server */


	host_and_automation_server_onlink := false
	for route, _ := range onlink_routes {
	    /* Automation server and host machines are on same network. No route to be added */
            if is_ip_in_nw, err = utils.IsIPaddrInNetwork(ssh_host, route); err == nil && is_ip_in_nw {
                   lvn_log.Debug.Printf("Host %s and Automation server on same network %s. No route needed", ssh_host, route)
		   host_and_automation_server_onlink = true
		   return nil
	    }
	    if err != nil {
		    return err
	    }
	}

	if !host_and_automation_server_onlink {

		gw := ""
		/* Checking specific gateway for remote host on automation server */
		for route, gway := range routes {
                    if is_ip_in_nw, err = utils.IsIPaddrInNetwork(ssh_host, route); err == nil && is_ip_in_nw {
			   gw = gway
                           lvn_log.Debug.Printf("Automation server has route to Host machine network %s via %s",  route, gw)
			   break
		    }
	            if err != nil {
	                    return err
	            }
		}

		/* If no specific gateway for remote host, use default gateway*/
		if gw == "" {
                    lvn_log.Debug.Printf("Automation server has default route to Host machine network")
                    gw = default_route["default"]
		}

		/* From the default gw IP, finding the nw addr on automation server */
		nw_used_from_automation_server_to_host := ""
                for route, local_iface_ip := range onlink_routes {
                    if is_ip_in_nw, err = utils.IsIPaddrInNetwork(gw, route); err == nil && is_ip_in_nw {
                        nw_used_from_automation_server_to_host = route
                        lvn_log.Debug.Printf("Automation server connects to Host network via local interface %s", local_iface_ip)
			break
		    }
	            if err != nil {
	                    return err
	            }
		}

		/* Finding the mgmt nw address of the host */
		host_mgmt_nw_addr := ""
		for route, _ := range onlink_routes_on_spoke1_host {
                    if is_ip_in_nw, err = utils.IsIPaddrInNetwork(ssh_host, route); err == nil && is_ip_in_nw {
                        host_mgmt_nw_addr = route
                        lvn_log.Debug.Printf("Mgmt network of the host %s is %s", ssh_host, host_mgmt_nw_addr)
			break
		    }
	            if err != nil {
	                    return err
	            }
		}

		/* Check specific route to automation server on host machine */
		for route, gw := range routes_on_spoke1_host {
			if route == nw_used_from_automation_server_to_host {
				/* route already present */
                                lvn_log.Debug.Printf("Host %s has already route to Automation server network %s via %s", ssh_host, route, gw)
				return nil
			}
		}

		/* Find the gw from host machine nw to automation server nw */
	        gateway_for_host_to_automation_server := setup.SearchRoute(host_mgmt_nw_addr, nw_used_from_automation_server_to_host)
		if gateway_for_host_to_automation_server != "" {
                    net_addr_info := strings.Split(nw_used_from_automation_server_to_host, "/")
		    netmask_int, _ := strconv.Atoi(net_addr_info[1])
                    _, err = host_session.AddRoute(net_addr_info[0], netmask_int, gateway_for_host_to_automation_server)
		    if err != nil {
			    return errors.New(fmt.Sprintf("Error adding route (%s via %s) in the host machine %s", nw_used_from_automation_server_to_host, gateway_for_host_to_automation_server, host_session.Host))
		    }

		    /* Delete the default route on the host machine. Since internet data need to be sent to CPE lan ip as default route */
                    _, err = host_session.DeleteDefaultRoute()
		    if err != nil {
			    return errors.New(fmt.Sprintf("Failed to delete the default route on host machine %s", host_session.Host))
		    }
		}

	} else {
		return errors.New(fmt.Sprintf(""))
	}
	return nil
}

/* 
 ************************************************************************************
 ***                                 SelectLanFromCPE                            
 ************************************************************************************
 * Purpose: From the given CPE, select active(status=up) lan interface along with
 *          its L3 information (addr, mask)
 ************************************************************************************
*/
func SelectLanFromCPE(cs *rest_service.Cloudstation, cpe_name string, bond bool) (map[string][]string, error) {

    /* 
     * 1. Get iface list from the cpe
     * 2. From the iface list, select only lans that are active (status=up)
     * 3. From the active lans, get ipv4 address, mask of first active lan interface
    */

    var (
	    error_reason string
	    res bool
    )
    iface_to_ip_addr_map := map[string][]string{}

    /* Get list of active lan interfaces from CPE  */
    substep("Get list of active lan interfaces from CPE %s", cpe_name)
    var cpe_ifaces rest_service.IFACE
    cpe_ifaces, res = cs.Get_IfaceList(cpe_name)
    if !res {
            return iface_to_ip_addr_map, errors.New(fmt.Sprintf("Unable to get interface list from the CPE %s", cpe_name))
    }

    cpe_lans := cpe_ifaces.GetUpPhyLans()
    if len(cpe_lans) == 0 {
        error_reason = fmt.Sprintf("Unable to find active lans on CPE %s", cpe_name)
        return iface_to_ip_addr_map, errors.New(error_reason)
    }

    iface, iface_primary_v4_addr, iface_primary_v4_addr_mask := "", "", 0
    for _, lan := range cpe_lans {
            if ipv4_addr, ipv4_mask := lan.GetPrimaryV4Addr();  ipv4_addr != "" {
                iface = lan.IfaceName()
                iface_primary_v4_addr, iface_primary_v4_addr_mask = ipv4_addr, ipv4_mask
                break
        }
    }

    if iface == "" || iface_primary_v4_addr == "" {
        return iface_to_ip_addr_map, errors.New(fmt.Sprintf("No active lans found on the cpe (%s) with IPv4 address", cpe_name))
    }

    //lvn_log.Info.Printf("Active lan on CPE %s on site %s is %s (%s)", spoke1_cpe_name, spoke1_site_name, lan_iface, lan_iface_primary_v4_addr)
    iface_to_ip_addr_map[iface] = []string{iface_primary_v4_addr, fmt.Sprintf("%d", iface_primary_v4_addr_mask)}
    return iface_to_ip_addr_map, nil
}




/* 
 ************************************************************************************
 ***                             GetLanInfoOfCPE
 ************************************************************************************
 * Purpose: From the given CPE and lan iface, get the host connected to the
 *          lan iface via switch/L2
 ************************************************************************************
*/
func GetLanInfoOfCPE(setup *new_setup_utils.Setup001, cs *rest_service.Cloudstation, site_name string, cpe_name string, cpe_info *new_setup_utils.Device, lan_iface_list []string) (bool, error, map[string][]string, map[string]string, map[string]string, map[string]host_utils.Credentials) {

    var (
	    ok bool
	    reason string
    )

    /* 
     * 1. Get logical (lan, wan) to physical interface (eth0, p3p1) mapping of CPE 
     * 2. From the map, get the physical interface of all given logical interfaces (lan_iface_list)
     * 3. From the setup information about CPE(cpe_info), find the remote Host connected via switch
    */
    /* Get physical interface of the lan interface on CPEe */
    substep("Get physical interface of the lan interfaces(%s) on CPE %s", strings.Join(lan_iface_list, ","), cpe_name)
    Linkmap, _ := cs.GetCpeLinkMap(cpe_name)
    ReqCpePhyport := map[string][]string{}
    HostPhyInterface := map[string]string{}
    HostName := map[string]string{}
    HostSession := map[string]host_utils.Credentials{}
    missed_ifaces := []string{}

    for _, lan_iface := range lan_iface_list {
        if ReqCpePhyport[lan_iface], ok = Linkmap[lan_iface]; !ok {
		missed_ifaces = append(missed_ifaces, lan_iface)
	}
    }

    if len(ReqCpePhyport) !=  len(lan_iface_list) {
            //FailTest(fmt.Sprintf("Unable to get the physical interface for %s", strings.Join(missed_ifaces, ",")))
	    reason = fmt.Sprintf("Unable to get the physical interface for %s", strings.Join(missed_ifaces, ","))
	    return false, errors.New(reason), ReqCpePhyport, HostPhyInterface, HostName, HostSession
    }

    /* Get host information of the site */
    missed_ifaces = []string{}
    substep("Get host information of the site %s", site_name)

    for k, v := range ReqCpePhyport {
        /* non bond ifaces */
        if len(v) == 1 {
            for _, node := range cpe_info.GetPhyInfo(v[0]) {
                if node.IsViaL2() {
                    ssh_host, ssh_port, ssh_username, ssh_password, node_type := setup.GetNodeInfo(node.GetName())
                    if node_type == "host" {
                            HostPhyInterface[k] = node.GetIface()
                            HostName[k] = node.GetName()
			    HostSession[k] = host_utils.Credentials{Host: ssh_host, Port:ssh_port, User: ssh_username, Pswd: ssh_password}
                            break
                    } else {
		        missed_ifaces = append(missed_ifaces, k)
		    }
                } else {
		        missed_ifaces = append(missed_ifaces, k)
		}
            }
        } else {
		missed_ifaces = append(missed_ifaces, k)
	}
    }

    if len(HostName) != len(lan_iface_list) || len(HostPhyInterface) != len(lan_iface_list) {
        reason = fmt.Sprintf("No physical hosts found for the site %s for all requested lan ifaces", site_name)
	return false, errors.New(reason), ReqCpePhyport, HostPhyInterface, HostName, HostSession
    }
    //lvn_log.Info.Printf("The physical host for the site %s is %s (interface: %s)", site_name, HostName, HostPhyInterface)
    for k, v := range HostName {
       lvn_log.Info.Printf("The physical host for the site %s is %s (interface: %s)", site_name, v, HostPhyInterface[k])
    }
    return true, nil, ReqCpePhyport, HostPhyInterface, HostName, HostSession
}

/* 
 ************************************************************************************
 ***                            CheckSitesPresentInCS
 ************************************************************************************
 * Purpose: From the given site list, check all sites are actually
 *          configured/present in the CS
 ************************************************************************************
*/
func CheckSitesPresentInCS(cs *rest_service.Cloudstation, site_list []string) (bool, error) {

    /* 
     * 1. Get the list of sites from the CS
     * 2. Check all the sites in the site_list are present in the CS
    */
    var sites_info map[string]rest_service.SiteInfo
    sites_info = cs.SitesByName()

    sites_not_present := []string{}
    for _, site := range site_list {
        if _, ok := sites_info[site]; !ok {
	    sites_not_present  = append(sites_not_present, site)
        }
    }

    if len(sites_not_present) > 0 {
	return false, errors.New(fmt.Sprintf("Failed to fetch the site(s) %s info from the CS", strings.Join(sites_not_present, ",")))
    }
    return true, nil
}
